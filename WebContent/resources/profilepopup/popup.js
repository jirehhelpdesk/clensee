function openOffersDialog() {
	$('#overlay').fadeIn('fast', function() {
		$('#boxpopup').css('display','block');
        $('#boxpopup').animate({'left':'30%'},500);
    });
}


function closeOffersDialog(prospectElementID) {
	$('#overlay').fadeOut('fast', function() {
		$('#boxpopup').css('display','none');
        $('#boxpopup').animate({'left':'30%'},500);
    });
}
