<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>
<script src="resources/kyc_scripts/jquery-ui-10.js"></script>
<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>



<script>

function viewKycDocumnt(fileName)
{
	$.ajax({
		type : "get",				
		url : "viewKycDoc.html",
		data: "fileName="+fileName,
		success : function(response) {
		
			document.getElementById('kycDocPopUplight').style.display='block';
	        document.getElementById('kycDocPopUpfade').style.display='block';
	        $("#kycDocPopUpDisplay").html(response);
	        			
		},		
	});		 		 		 		
	
}

</script>

</head>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>   
   <body>
   
   <c:set var="history" value="${viewdetails}"/>         	          	
           <% String history = (String)pageContext.getAttribute("history");%>                                      
       
               <%
               System.out.println("jvb  "+history);
               String filename ="";
               String docname="";
               String date="";
               String des="";
               
                        if(history.contains(","))
                        {
            	           String kycarray[] = history.split(",");           			  
            			   filename=kycarray[0];docname=kycarray[1];date=kycarray[2];des=kycarray[3];
                        }
               String historyname[] = filename.split("_");                                  
                 %>
                 
                  <div id="kycdetails">   
               
                     <%                              
                         String user =(String)session.getAttribute("Ind_id");	
                      %>
                     
                    <%if(!filename.contains("_")){ %>
                     
	                    <div  class="kycprofilephoto">                                    
	                    <img src="resources/kyc_images/dummyUploadDocuments.png"   width="200" height="150" /></img>
						</div>
					
					<%} else {%>
					
						<div  class="kycprofilephoto">
						
							<%if(filename.contains("pdf")) {%>
							
							      <img src="resources/images/preview_not_available.jpg"   width="200" height="150" /></img>
							
							<%}else{ %>
							
	                   		      <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+filename+"&docCategory=KYC_DOCUMENT"%>"   width="200" height="150" /></img>                                                 
	                      
	                         <%} %>
	                    </div>
                    
                    <%} %>
						 <div class="kycdetailsprofilstext">
						 <table>
						 <tr>
						 
						 <td style="width:130px;"><b>Proof of Identity</b></td>
						 <td style="width:5px;">:
						 </td>
						  <td style="width:300px;">
						  <c:out value="<%=docname%>"></c:out>  
						 </td>
						
						
						 </tr>
						 <tr>
						  <td style="width:130px;"> <b>Description</b></td>
						<td style="width:5px;">:
						 </td>
						 <td style="width:300px;"><c:out value="<%=date%>"></c:out>
						 </td>
						 </tr>
						 <tr>
						 
						  <td style="width:130px;"> <b>Date of Upload</b></td>
						  <td style="width:5px;"> :</td>
						  <td style="width:300px;">
						  <c:out value="<%=des%>"></c:out></td>
						 </tr>
						 
						 
						 </table>
                           
                         
                         
                         </div>  
                             
                      <div  class="kycdetailbutton">
                      
                      
                      <%if(filename.contains("_")) {%> 
                      
                       		<%if(filename.contains("pdf")) {%> 
                       		
                       		       <a href="download.html?fileName=<%=filename%>"><button class="documentdownload"></button></a><br><br>
			                  
                       		 <%}else {%>
			                      
			                      <a onclick="viewKycDocumnt('<%=filename%>')"><button class="documentviewpan"></button></a><br><br>
			                      <a href="download.html?fileName=<%=filename%>"><button class="documentdownload"></button></a><br><br>
			                  
			                  <%}%>    
                      <%} %>
                      
                      <a href="kychistory.html?fileName=<%=historyname[0]%>"><button class="documenthistory"></button></a><br><br>
                      
                      </div>
                                           
          </div> 
	</body>
    </html>