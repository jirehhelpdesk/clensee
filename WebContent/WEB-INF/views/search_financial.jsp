<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>
<script src="resources/kyc_scripts/jquery-ui-10.js"></script>
<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>


<script>
$(function() {
  $( "#datepicker" ).datepicker();
});
</script>

<!-- Script for Validation -->
<script type="text/javascript">

function ValidateFinancial()
{	
	var flag = 'true';
	
	
	if(document.getElementById("fromYearId").value=="Select Year")
	{
	   flag = 'false';
	   $("#yearDiv").show();
	   $("#yearDiv").html("Select Financial year !");
	}
	if(document.getElementById("fromYearId").value!="Select Year")
	{
		$("#yearDiv").hide();
	}
	 
		
	if(document.getElementById("financialfileId").value=="")
		  {
		     flag = 'false';
		     $("#fileDiv").show();
		     $("#fileDiv").html("Upload a financial document !");
		  }
	if(document.getElementById("financialfileId").value!="")
		{
		   $("#fileDiv").hide();
		}
	
	
			if (flag == 'true') 
			{
								
				$.ajax
			    ({
					type : "post",				
					url : "validateFileStorage.html",						
					success : function(response) {
						
						if(response=="withinStorage")
							{
								 var formData = new FormData($("#FinancialForm")[0]);
						            
								 formData.append("toYear",document.getElementById("toYearId").value);
								 formData.append("docMode",document.getElementById("docModeId").value);
								 
									$.ajax({
										type : "POST",
										url : "financialDocument.html",
										data : formData,
										async: false,
										success : function(response) {
						                    
											alert("Financial Document added Successfully .");
											$("#viewUsers1").html(response);
						
											
										},
										cache: false,
							 	        contentType: false,
							 	        processData: false,
										error : function() {
											alert('Error while fetching response');
										}
									});
									$("#titleBar").html("<h2><span>FINANCIAL DOCUMENT</span></h2>");
							}
						else
							{
							      alert("Failed due to storage limitation,to Upload more document migrate your Plan!");
							}
					},
					
				});		 		 		 		
				
				
			}
	}
		
function getNextYear()
{
	$("#finalyearDiv").html("");
	
	if(document.getElementById("fromYearId").value!="Select Year")
		{
	       document.getElementById("toYearId").value = +document.getElementById("fromYearId").value + 1;
		}
	if(document.getElementById("fromYearId").value=="Select Year")
		{
		   document.getElementById("toYearId").value = "";
		}
	
	var fyear = document.getElementById("fromYearId").value;
	var tyear = document.getElementById("toYearId").value;
	
	var inrfyear1 = +fyear + 1;
	var inrfyear2 = +fyear + 2;
		
	if(document.getElementById("fromYearId").value!="Select Year")
		{
			$("#finalyearDiv").show();
			$("#finalyearDiv").append("Financial Year "+fyear+"-"+tyear);
			$("#finalyearDiv").append("<br>Assesment Year "+inrfyear1+"-"+inrfyear2);
			$("#yearDiv").hide();
		}
	else
		{
		    $("#finalyearDiv").hide();
			$("#yearDiv").show();
			$("#yearDiv").html("Please a Select Year !");
		}
}

function getFinancialDetails(picname)
{	
	$.ajax
    ({
		type : "post",				
		url : "financial/Details.html",	
		data : "docName="+picname, 
		success : function(response) {
			
			 $("#viewUsers1")
				.html(response);
							
		},
		
	});		 		 		 		
     $("#titleBar").html("<h2><span>FINANCIAL DETAILS</span></h2>");
}

function getHistoryDetails(picName)
{	
	$.ajax
    ({
		type : "post",				
		url : "financialhistory.html",	
		data : "docName="+picName, 
		success : function(response) {
			
			 $("#viewUsers1")
				.html(response);
							
		},
		
	});		 		 		 		
     $("#titleBar").html("<h2><span>FINANCIAL HISTORY</span></h2>");
}


function generateFinancialMode(id)
{
	var docType = document.getElementById(id).value;
	var mode = docType.split(":");
	var values = "";
	if(mode[1]=="Both")
		{
		    values += "Document Mode:<select name=\"docMode\" id=\"docModeId\" >";
		    values +=  "<option value=\"Select Mode\">Select Mode</option>";
		    values +=  "<option value=\"Original\">Original</option>";
		    values +=  "<option value=\"Revised\">Revised</option>";
		    values += "</select>";
		    $("#docmodecompId").html(values);
		}
	if(mode[1]=="Original")
	{
	    values += "Document Mode:<input type=\"text\" name=\"docMode\" id=\"docModeId\" value=\"Original\" disabled=\"true\" />";
	    $("#docmodecompId").html(values);	    
	}
	if(mode[1]=="Revised")
	{
		values += "Document Mode:<input type=\"text\" name=\"docMode\" id=\"docModeId\" value=\"Revised\" disabled=\"true\" />";
		$("#docmodecompId").html(values);
	}
    
}

</script>
<!-- End of Script for Validation -->

<script> 
$(document).ready(function(){
  $("#financialAddDocumentFlip").click(function(){
    $("#financialAddDocumentPannel").slideToggle("slow");
  });
});
</script>
 

</head>
<body>	 

         <c:set var="financialHierarchy" value="${financial}"/>
         
         <c:set var="docName" value="${docName}"/>
         
         <% String fin_heirarchy =(String)pageContext.getAttribute("financialHierarchy");
            
            String heirarchyArray[] = fin_heirarchy.split(",");
            
            
            String docName =(String)pageContext.getAttribute("docName");
            
            int year = (Integer)request.getAttribute("year");
            
            String financialData = (String)request.getAttribute("financialData");
            
           
          %>         
		 

	<div id="financialAddDocumentFlip" ><button>Add Financial Document</button></div>
    <div id="financialAddDocumentPannel">
        
        <div style="width: 600px;">
          <form name="FinancialForm" id="FinancialForm" enctype="multipart/form-data">
          
             <ul>
                 <li>
		             From Year:<select id="fromYearId" name="fromYear" onchange="getNextYear()">
		                                <option  value="Select Year">Select Year</option>
		                                <%for(int i=2009;i<year+1;i++){%>
		                                <option value="<%=i%>"><%=i%></option>
		               				    <%} %>
		               				    </select>
		             To Year:<input type="text"   name="toYear" id="toYearId"  value="" disabled="disabled"/>
		             <div id="yearDiv" class="error" style="display:none"></div>
		             <div id="finalyearDiv" class="success" style="display:none;"></div>
		           </li>
		         
		         
			             <li >
			                   Financial Document Type:<select name="docType" id="docTypeId" onchange="generateFinancialMode(this.id)">
			                                                         <option  value="Select Type">Select Type</option>
			                                                          <% for(int j=0;j<heirarchyArray.length;j++){%>
			                                                          <option value="<%=heirarchyArray[j]%>"><%=heirarchyArray[j].split(":")[0]%></option>
										               				  <%} %>
										               		        </select>			                   
			             </li>      
		                   <li>
		                   
		                         <div id="docmodecompId"> 
		                        
		                        
		                        </div>
			                  	                   
			             </li> 	
		                   	     	                   		                                      
		                 
		           		          
		           <li >
			                   Financial Document:<input type="file" name="upl" id="financialfileId"/>
			                   <div id="fileDiv" class="error" style="display:none;margin-left: 283px;margin-top: -22px;"></div>
			       </li> 
		            
		           <li >
		           <input type="Reset" value="Reset" "/><input type="button" value="Save" onclick="ValidateFinancial()"/> 
		           </li>             		  
              </ul>
        
         </form>
        </div>
    
    </div>
    	
	 <div id="center-body-div">
						
	   <div  id="panel-1">
	
	     <%for(int i=year+1;i>=2010;i--){%>
		        
		        <% int from = i-1;
		           int to = i;
		          		           
		          String yearDiff = from+"-"+to; %>
		          
		         <h3 align="left"><b>Financial Year&nbsp;:&nbsp;<%=yearDiff%></b></h3> 
		         		
			          <div class='CSSTableGenerator'>			                			                  
			                
						          						           
						              
			                  <%if(financialData.contains("&&"))
			                  { 			                    
			                	  String finDataArray[] = financialData.split("/");
			                	  
			                	  for(int f=0;f<finDataArray.length;f++)
			                	  {
				                	  if(finDataArray[f].split("&&")[0].equals(yearDiff))
				                	   {						                 
				                		  %>	
				                		        		  					                  						                  
						                  <div class="financialdocumentview">
						                  <ul>
						                  <li style="width:330px;background-color:#eee;">Document Type-<%=finDataArray[f].split("&&")[1]%></li>
						                  <li style="width:155px;background-color:#eee;">Document Mode-<%=finDataArray[f].split("&&")[2]%></li>
						                  <li style="width:155px;background-color:#eee;">Attached On-<%=finDataArray[f].split("&&")[3]%></li>					                  
						                  <li style="width:60px;background-color:#eee;"><a href="#" onclick="getFinancialDetails('<%=finDataArray[f].split("&&")[4]%>')">View</a></li>
						                  <li style="width:60px;background-color:#eee;"><a href="#" onclick="getHistoryDetails('<%=finDataArray[f].split("&&")[4]%>')">History</a></li>
						                  </ul>
						          	     </div>
						               							                  
						                  						                  				                   						                 			                        			                       			                	 
			                	 <% } else { %>
			                	 			                	       			                	 
			                	 <% } //End Of Else
			                	 
			                        }  //End of For
			                	  			                	  
			                	 } %>
			                	 
			              
			               
			          </div>
		  
          <%} %>
          
       </div> 

	</div>
	
	  
</body>
</html>