<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<script>

function organiseInd(ind_id,status)
{
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				$("#waitingdivId").show();
				
					$.ajax({
						type : "Post",
						url : "changestatus.html",	
						data: "indid="+ind_id+"&status="+status,
						success : function(response) {
							
							$("#waitingdivId").hide();
							
							viewindividualsection();
							
						},						
					});	
				}
			else
				{
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	});
	
	
}

</script>


</head>
<body>



<div id="History" style="display: inline-table;
    overflow: visible; margin-top: 25px;">

<c:if test="${!empty indDetails}">
		
	<table class='CSSTableGenerator' align="left" cellspacing="0" align="center" width="600px" style="width: 98% !important;">
		<tr>
		    <th height="10" style="color: #00b6f5;">Serial No</th>			
			<th height="10" style="color: #00b6f5;">Name</th>
			<th height="10" style="color: #00b6f5;">Email Id</th>
			<th height="10" style="color: #00b6f5;">Mobile No</th>
			<th height="10" style="color: #00b6f5;">Last Updated</th>
			<th height="10" style="color: #00b6f5;">Status</th>
		</tr>
        <%int i=0; %>
		<c:forEach items="${indDetails}" var="det">					 
			 
			    <c:set var="indStatus" value="${det.admin_control}"/>
				<%String indStatus = (String)pageContext.getAttribute("indStatus");%>
				
				<%if(indStatus.equals("1")){%>
						
						 <tr style="background-color:;">
			    			    
						    <td height="10" width="120"><c:out value="<%=++i%>"/></td>				
							<td width="230"><c:out value="${det.firstname}"/> <c:out value="${det.middlename}"/> <c:out value="${det.lastname}"/></td>
							<td width="270"><c:out value="${det.emailid}"/></td>
							<td width="270"><c:out value="${det.mobileno}"/></td>
							<td width="270"><c:out value="${det.cr_date}"/></td>	
								
							<td align="center" width="210"><a href="#" onClick="organiseInd('${det.ind_id}','<%=indStatus%>')"><button id="viewhistory">ACTIVE</button></a></td>
						    				
			            </tr>							
						
			    <%}else { %>
			    
			            <tr style="background-color:#Cff;">
			    			    
						    <td height="10" width="120"><c:out value="<%=++i%>"/></td>				
							<td width="230"><c:out value="${det.firstname}"/> <c:out value="${det.middlename}"/> <c:out value="${det.lastname}"/></td>
							<td width="270"><c:out value="${det.emailid}"/></td>
							<td width="270"><c:out value="${det.mobileno}"/></td>
							<td width="270"><c:out value="${det.cr_date}"/></td>	
							
			    		    <td align="center" width="210"><a href="#" onClick="organiseInd('${det.ind_id}','<%=indStatus%>')"><button id="viewhistory">BLOCKED</button></a></td>
			    
			           </tr>
			    <%} %>
			    
			    
			
			 		
		</c:forEach>		
	</table>	
		
</c:if>


<c:if test="${empty indDetails}">
		
	<h1>No Data Found !</h1>
		
</c:if>
</div>



</body>
</html>