 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Documents</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>
<!-- Employee Date Picker  -->

<link rel="stylesheet" type="text/css" href="/css/result-light.css">

<script type='text/javascript' src="resources/onlineresources/code.jquery.com.jquery-1.9.1.js"></script>
<script type='text/javascript' src="resources/onlineresources/code.jquery.com.ui.1.10.3.jqury-ui.js"></script>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
$(".datepick2").datepicker({'dateFormat': 'd/m/y',maxDate:"0d",changeMonth: true,
	changeYear: true});
$("#datepick_id").datepicker({'dateFormat': 'd/m/y',maxDate:"0d",changeMonth: true,
	changeYear: true});

});  

</script>

<!-- End Of Employee Date Picker -->


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">
<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<!-- <link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" /> -->
<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
 
<script src="resources/onlineresources/code.jquery.com.jquery-1.10.2.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}



</style>








<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}



.smalldivstyle
{
    border-color: #ccc;
    border-style: solid;
    border-width: 3px;
    float: left;
    height: 140px;
    margin-bottom: 10px;
    margin-left: 164px;
    margin-top: 14px;
    overflow-y: scroll;
    width: 500px;
    display:none;
}

</style>



<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{
border: 2px solid #03b2ee;
    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}
</style>
 
<script>

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewDocumentInPopUp(fileName)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({
		type : "get",				
		url : "viewEmployeeDocument.html",
		data: "fileName="+fileName,
		success : function(response) {
		
			document.getElementById('waitlight').style.display='none';
	        document.getElementById('waitfade').style.display='none';
	       
			document.getElementById('kycDocPopUplight').style.display='block';
	        document.getElementById('kycDocPopUpfade').style.display='block';
	        $("#kycDocPopUpDisplay").html(response);
	        			
		},		
	});		 		 		 		
	
}

</script> 
 


<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>








<script type="text/javascript">
function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
		
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		    
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length==0)
	    {
		 	$('#searchhomepage').html("");
		 	$('.topprofilestyle').hide();
			 $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
		 	
	    }
	 if(id.length<3)
	    {
			$('#searchhomepage').html("");
			$('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();	
			$('.topprofilestyle').hide();
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}
</script>





<script>

/* All Scripts for MyDocument pannel  */

	
	function myDocuments2(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 window.location = "search_financialDoc.html";
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});		 		 		 				
	    }
	
	function myDocuments4(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>EMPLOYEMENT DETAILS</span></h2>");
	}
	
	
		
		/* <!-- End of code for noneditable to editabel for document-->  */
 	 	 	 	 		 
</script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<!-- Script and CSS for Employement Documents -->

<script type="text/javascript">

function empViewDetails(data)
{
	var info = data.split("?");
	var url = info[0];
	var docid = info[1];
			
	$.ajax
    ({
		type : "post",				
		url : url,		
		data : "docId="+docid,
		success : function(response) {
									
			 $("#viewUsers1")
				.html(response);
							
		},
		error : function() {
			alert('Error while fetching response');
		}
	});	
	
	$("#titleBar").html("<h2><span>Employment Details</span></h2>");
}

function backtoMain(url)
{
   window.location = url;	
}


function empHistory()
{
	var backval = document.getElementById("backId").value;
	
	window.location = "employement_history.html?cmyName="+backval;
	
}


function viewDocumentInPopUpfromHistory(fileName)
{
	 $.ajax({
				type : "post",
				url : "checksessionforIndividual.html",
				success : function(response) {
					
					if(response=="Exist")
						{
								document.getElementById('waitlight').style.display='block';
							    document.getElementById('waitfade').style.display='block';
							   
								$.ajax({
									type : "get",				
									url : "viewEmployeeDocument.html",
									data: "fileName="+fileName,
									success : function(response) {
									
										document.getElementById('waitlight').style.display='none';
								        document.getElementById('waitfade').style.display='none';
								       
										document.getElementById('kycDocPopUplight').style.display='block';
								        document.getElementById('kycDocPopUpfade').style.display='block';
								        
								        $("#kycDocPopUpDisplay").html(response);
								        			
									},		
								});		 		 		 		
						
						}
					else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	 
	
}






/* Main Header Actions  */

function gotoProfile()
{
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoMyDocument()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoSharingManager()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoStore()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoAlerts()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
}

function gotoPlans()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
}

function gotoSetings()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}


function hideAddvertise()
{
    $("#ProfileHeadingDiv").hide();	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}


function hideAddvertise()
{
   $("#ProfileHeadingDiv").fadeOut(90000);	
}
</script>
 
 <!-- End Of  -->
 
</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocumentsfocus"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 
 <!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
            
            
            
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
             		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
							  <ul>
							     <li id="Hi-B-1"><a href="myDocumentsMain.html"  id="Hi-B-1" ><span>KYC Documents</span></a></li>
								 <li id="academic_document"><a href="getacademicdetails.html"  id="Hi-B-2" ><span>Academic Documents</span></a></li>															
								 <li id="employee_document"><a href="employeeDetails.html"  id="employee_documentactive"><span>Employment Documents</span></a></li>
								 <li id="financial_document"><a href="#"  id="Hi-B-3" onclick="myDocuments2('search_financialDoc.html')"><span>Financial Documents</span></a></li>
							 </ul>
						   </div>    		
					</div>
					
					 </div>
					
					<div class="rightside">
					
						
						
					  <!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

					<div id="ProfileHeadingDiv" style="display: block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>

					<!-- Advertisment Div for Entire Page  -->
					   
					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span><%=(String)session.getAttribute("cmyName")%>'s history details as on <%=request.getAttribute("historyDate")%></span>								   
							</h2>
						 </div>
						 
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
						
						    									
									   <% String empDetails = (String)request.getAttribute("employeeDetails"); 
									      
									      System.out.println("Emp Details String "+empDetails);
									   
									     String data[] =  empDetails.split("@@@");
									     
									     String empdata[] = data[0].split("&&");
									     
									     String empdocname[] = data[1].split(",");
									     
									     
									   %>
									   
									   <input type="hidden" value="<%=empdata[0]%>" id="backId"/>
									   
									      <div align="center" class="employeedetails" >
									           <div align="right" style="  height: 30px;padding-top: 10px;width: 93%;"><input class="backbuttonindex_histy" type="button" value="Back" onclick="empHistory()" /></div>
									      <div id="viewempdeatls">
									     
									          <table >
									
									            <tr>
									            <td width="120">
									                    <b>Organization name </b></td>
									                    <td width="10">:</td>
									                    <td>
									                    <c:out value="<%=empdata[0]%>"></c:out></td>
									            </tr>
									            
									            <tr>
									              <td>
									            <b>Job designation </b></td>
									              <td>
									            :</td>
									              <td>
									              <c:out value="<%=empdata[1]%>"></c:out></td>
									            </tr>
									
									            <tr class="profiletextscroll" >
									              <td>
									            <b>Job summary </b></td>
									              <td>:</td>
									                <td>
									              <c:out value="<%=empdata[2]%>"></c:out></td>
									            </tr>
									
									            <tr>
									            <td>
									            <b>Job duration </b></td>
									            <td>:</td>
									            <td>
									             <b>form</b><c:out value="<%=empdata[3]%>"></c:out> <b>to</b> <c:out value="<%=empdata[4]%>"></c:out>
									             </td>
									            </tr>
									         
									         </table>
									        </div> 
									          
									       <div id="ImageSlider" class="kycemployeedetailimgafter"> 
									       <div class="employeedocumentdetails">
									       <span></span>
									          <h1>Employment Documents </h1> 
									          </div>
									          <div class="employmentdocument">
									          <div  style="float:left; position: relative; ">
									                     
														 <div id="ShowOfferDiv" style="display:block;margin-top: 10px; margin-left: 5px;">
														 <ul>
														 <li class="kyc_emp_his_viewinner">
														 <b>Offer letter</b>
														 
														 <%if(!empdocname[0].equals("No Document")){ %>
														 
														 		<%if(!empdocname[0].contains("jpeg") && !empdocname[0].contains("jpg") && !empdocname[0].contains("png") && !empdocname[0].contains("gif")) {%>
														 		
															 		  <%if(empdocname[0].contains("pdf")) {%>
																																																			                        
											                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
											                            		
											                        <%}else if(empdocname[0].contains("doc")) {%>
											                        
											                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
											                        
											                        <%}else if(empdocname[0].contains("docx")) {%>
											                        
											                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
											                        
											                        <%}else if(empdocname[0].contains("xls")) {%>
											                        
											                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
											                         
											                        <%}else { %>
										                        
										                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
										                        
										                           <%} %>		
																 
																 <br>							 	         
														         <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[0]%>"></a>
														         
														 		
														 		<%} else {%>
														 		
														 		<%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[0]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img> --%>
																 <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+empdocname[0]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
																 
																 <br>
																 <a class="mydocumentviewpan" onclick="viewDocumentInPopUpfromHistory('<%=empdocname[0]%>')"></a>		         
														         <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[0]%>"></a>
														         
														 		<%} %>
														 		
														
														 <%} else{%>
														 
														 <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
														 
														 
														 <%} %>		
																 
														 		
																 
														 </li>		
														 </ul>
														 </div>
											 </div>
											 
											 <div  style="float:left; position: relative;">
											 
														<div id="ShowHikeDiv" style="display:block;margin-top: 10px;"> 
														<ul>
														
														<li class="kyc_emp_his_viewinner">
														<b>Hike letter</b>
														
														<%if(!empdocname[1].equals("No Document")){ %>
														
														     <%if(!empdocname[1].contains("jpeg") && !empdocname[1].contains("jpg") && !empdocname[1].contains("png") && !empdocname[1].contains("gif")) {%>
														 		
														 		   <%if(empdocname[1].contains("pdf")) {%>
																																																			                        
											                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
											                            		
											                        <%}else if(empdocname[1].contains("doc")) {%>
											                        
											                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
											                        
											                        <%}else if(empdocname[1].contains("docx")) {%>
											                        
											                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
											                        
											                        <%}else if(empdocname[1].contains("xls")) {%>
											                        
											                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
											                         
											                        <%}else { %>
										                        
										                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
										                        
										                           <%} %>		
																 
																 <br>							 	         
														         <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[1]%>"> </a>
														         					 		
														 		<%} else {%>
														 		
														         <%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[1]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img> --%>
														         
														         <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+empdocname[1]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
														         
														         <br>
														        
														         <a class="mydocumentviewpan" onclick="viewDocumentInPopUpfromHistory('<%=empdocname[1]%>')"> </a>
												         
												                 <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[1]%>"></a>	
												                  
												                  <%} %>
												                  
												         <%} else {%>
												         
												                 <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
												                 
												         <%} %>
												           
														</li>		
														</ul>
														</div>
									        </div>
											 
											 <div  style="float:left; position: relative;">
											  
														<div id="ShowExpDiv" style="display:block;margin-top: 10px;" >
														<ul>
														
														<li class="kyc_emp_his_viewinner">
														<b>Experience letter </b>
														     <%if(!empdocname[2].equals("No Document")){ %>
														     
														     		<%if(!empdocname[2].contains("jpeg") && !empdocname[2].contains("jpg") && !empdocname[2].contains("png") && !empdocname[2].contains("gif")) {%>
														 		
																 		   <%if(empdocname[2].contains("pdf")) {%>
																																																					                        
													                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
													                            		
													                        <%}else if(empdocname[2].contains("doc")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(empdocname[2].contains("docx")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(empdocname[2].contains("xls")) {%>
													                        
													                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
													                         
													                        <%}else { %>
												                        
												                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
												                        
												                           <%} %>		
																 
																 		 <br>							 	         
														                 <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[2]%>"> </a>
														         					 		
														 		  <%} else {%>
														     		
														                <%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[2]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img> --%>
														         
														         			<img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+empdocname[2]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
														         
														                <br><a class="mydocumentviewpan"  onclick="viewDocumentInPopUpfromHistory('<%=empdocname[2]%>')"></a>
												                         <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[2]%>"> </a>
												                     
												                     <%} %>
												                       
												              <%} else{%>
												              
												                      <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
												                      
												               <%} %> 
														</li>					
														</ul>
														</div>		
											</div>
											 
											<div  style="float:left; position: relative;">		 
														<div id="ShowRelDiv" style="display:block;margin-top: 10px;">
														<ul> 
														
														 <li class="kyc_emp_his_viewinner">
														 <b>Reliving letter</b>
														 <%if(!empdocname[3].equals("No Document")){ %>
														 
														          <%if(!empdocname[3].contains("jpeg") && !empdocname[3].contains("jpg") && !empdocname[3].contains("png") && !empdocname[3].contains("gif")) {%>
														 		
														 		 		  <%if(empdocname[3].contains("pdf")) {%>
																																																					                        
													                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
													                            		
													                        <%}else if(empdocname[3].contains("doc")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(empdocname[3].contains("docx")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(empdocname[3].contains("xls")) {%>
													                        
													                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
													                         
													                        <%}else { %>
												                        
												                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
												                        
												                           <%} %>		
																 		 <br>							 	         
														                 <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[3]%>"> </a>
														         					 		
														 		  <%} else {%>
														 		  
														 		       <%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[3]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img> --%>
														                    
														                    <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+empdocname[3]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
														                    
															           <br><a  class="mydocumentviewpan" onclick="viewDocumentInPopUpfromHistory('<%=empdocname[3]%>')"> </a>
													                       <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[3]%>"> </a>
													                     
														 		  <%} %>
														 		  
														            
												          <%} else{%>
												          
												                   <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
												                   
												           <%} %> 
												           
														</li>		
														</ul>
														</div>
											</div>
											
											<div  style="float:left; position: relative;">							
														<div id="ShowFormDiv" style="display:block;margin-top: 10px;">
														<ul>
														
														 <li class="kyc_emp_his_viewinner">
														 <b>Form 16</b>
														 <%if(!empdocname[4].equals("No Document")){ %>
														 
														           <%if(!empdocname[4].contains("jpeg") && !empdocname[4].contains("jpg") && !empdocname[4].contains("png") && !empdocname[4].contains("gif")) {%>
														 		
														 		 		 <%if(empdocname[4].contains("pdf")) {%>
																																																					                        
													                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
													                            		
													                        <%}else if(empdocname[4].contains("doc")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(empdocname[4].contains("docx")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(empdocname[4].contains("xls")) {%>
													                        
													                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
													                         
													                        <%}else { %>
												                        
												                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
												                        
												                           <%} %>		
																 		 <br>							 	         
														                 <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[4]%>"> </a>
														         					 		
														 		  <%} else {%>
														 		  
														 		        <%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[4]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img> --%>
														                      
														                      <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+empdocname[4]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
														                      
															            <br><a class="mydocumentviewpan" onclick="viewDocumentInPopUpfromHistory('<%=empdocname[4]%>')"> </a>
													                        <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=empdocname[4]%>"> </a>
													                      					 		  
														 		  <%} %>
														            
												            <%} else{%>
												            
												                      <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
												                      
												             <%} %>
												             
														 </li>					
														 </ul>
														 </div>
														 
											</div>
											
										
										 </div>
										 
											<div id="empsalaryslip">
											 <div class="employeedocumentdetails">
									       <span></span>
											  <h1>SalarySlips </h1> 
											  </div>
											  <div class="kyc_emp_his_viewinner_to">
									     <div id="PayslipSlider" class="epmloyeedocimgdet"> 
									       	
									       	<div  style="float: left; position: relative; margin-top: 10px;">
												      <ul>
												          
									         <%if(empdocname.length>5) { 
					                             
									        	 
									        	 
									        	 
									        	 String monthNameArray[] = {"January", "February", "March", "April", "May",
				    	    				                				"June", "July", "August", "September", "October",
				    	    				                				"November", "December"};
									        	 
									        	 String SalarySlipNames = "";    
									        	 String monthyearName = "";    
								
									        	 for(int p=5;p<empdocname.length;p++)
												 {      
									        		 SalarySlipNames += empdocname[p] + ",";
												 } 
									        	 
									        	 if(SalarySlipNames.length()>0)
									        	 {
									        		 SalarySlipNames = SalarySlipNames.substring(0, SalarySlipNames.length()-1);
									        		 
									        		 String slipNamesArray[] = SalarySlipNames.split(",");
									        	     for(int sn=0;sn<slipNamesArray.length;sn++)
									        	     {
									        	    	 int i = slipNamesArray[sn].split("_")[0].lastIndexOf(" ");
									        	    	 String tempValue = slipNamesArray[sn].split("_")[0].substring(i+1);
									        	    	 
									        	    	 monthyearName += tempValue + ",";
									        	     }
									        	 }
									        	 
									        	 if(monthyearName.length()>0)
									        	 {
									        		 monthyearName = monthyearName.substring(0, monthyearName.length()-1);
									        	 }
									        	 
									        	 
									        	 //  Sorted logics
									        	 
									        	 String sortedMonthYearName = "";
									        	 
									        	 if(SalarySlipNames.length()>0)
										    	     {
										    	    	
										    	    	 String monthYearArray[] = monthyearName.split(","); 
										    	    	 
										    	    	 for(int mn=0;mn<monthYearArray.length;mn++)    // outer monthYear array
										    	    	 {
										    	    		 String monthValue = monthYearArray[mn].split("-")[0];
										    	    		 int outerindex = 0;
							    	    					 int innerindex = 0;
										    	    				 for(int imn=0;imn<monthYearArray.length;imn++)    // inner monthYear array
													    	    	 {
										    	    					 String imonthValue = monthYearArray[imn].split("-")[0];
											    	    				 for(int imName=0;imName<monthNameArray.length;imName++)
													    	    		 {
											    	    					 if(monthValue.equals(monthNameArray[imName]))
											    	    					 {
											    	    						 outerindex = imName;
											    	    					 }
											    	    					 if(imonthValue.equals(monthNameArray[imName]))
											    	    					 {
											    	    						 innerindex = imName;
											    	    					 }
													    	    		 }
											    	    				 if(outerindex>innerindex)
											    	    				 {
											    	    					 String temp = monthYearArray[mn];
											    	    					 monthYearArray[mn] = monthYearArray[imn];
												    	    				 monthYearArray[imn] = temp;
											    	    				 }
													    	    	 }
										    	    																														    	    		 
										    	    	 }
										    	    	 
										    	    	 
										    	    	 for(int k=0;k<monthYearArray.length;k++)
										    	    	 {																															    	    		 
										    	    		 int yearValue = Integer.parseInt(monthYearArray[k].split("-")[1]);
										    	    		 for(int l=0;l<monthYearArray.length;l++)
										    	    		 {
										    	    			 int tempyearValue = Integer.parseInt(monthYearArray[l].split("-")[1]);
										    	    			 if(yearValue>tempyearValue)
										    	    			 {
										    	    				 String temp = monthYearArray[k];
										    	    				 monthYearArray[k] = monthYearArray[l];
										    	    				 monthYearArray[l] = temp;
										    	    			 }
										    	    		 }																																    	    		 
										    	    	 }
										    	    	
										    	    	 for(int my=0;my<monthYearArray.length;my++)
										    	    	 {
										    	    		 sortedMonthYearName += monthYearArray[my] + ",";																														    	    		 
										    	    	 }
										    	    	 
										    	    	 if(sortedMonthYearName.length()>0)
										    	    	 {
										    	    		 sortedMonthYearName = sortedMonthYearName.substring(0, sortedMonthYearName.length()-1);
										    	    	 }
										    	    	 
										    	     }
										     
									        	    System.out.println("month-year Sorted Names="+sortedMonthYearName);
									        	 
									        		 String monthName[] = sortedMonthYearName.split(","); 
											    	 String salSlipArray1[] = SalarySlipNames.split(","); 
												     String sortedSlipName = "";
												     for(int sort=0;sort<monthName.length;sort++)
									    	    	 {
												    	 for(int sorti=0;sorti<salSlipArray1.length;sorti++)
										    	    	 {
												    		 if(salSlipArray1[sorti].contains(monthName[sort]))
												    		 {
												    			 sortedSlipName += salSlipArray1[sorti] + ",";	
												    		 }																										    	    		 
										    	    	 }																												    	    		 
									    	    	 }
												     
												     if(sortedSlipName.length()>0)
												     {
												    	 sortedSlipName =sortedSlipName.substring(0, sortedSlipName.length()-1);
												     }
												     
												     String salSlipArray[] = sortedSlipName.split(","); 
												     
									        	 
									        	 // End of sorted logics
									        	 
									        	 
									        	 System.out.println("Salary slips Sorted Names="+sortedSlipName);
									        	      
												for(int p=0;p<salSlipArray.length;p++)
											   	{
												      %>      
														
													      <li class="panviewinner_acad">
													      <%if(!salSlipArray[p].contains("jpeg") && !salSlipArray[p].contains("jpg") && !salSlipArray[p].contains("png") && !salSlipArray[p].contains("gif")) {%>
														 		
														 		 		  <%if(salSlipArray[p].contains("pdf")) {%>
																																																					                        
													                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
													                            		
													                        <%}else if(salSlipArray[p].contains("doc")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(salSlipArray[p].contains("docx")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(salSlipArray[p].contains("xls")) {%>
													                        
													                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
													                         
													                        <%}else { %>
												                        
												                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
												                        
												                           <%} %>		
																 		
																 		 <br>		
																 		 					 	         
														                 <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=salSlipArray[p]%>"> </a>
														         					 		
														 		  <%} else {%>
														 		  
														 		  
																	      <%=salSlipArray[p].split("_")[0]%> <br>
																	      
																	      <%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+salSlipArray[p]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img> --%>
																	      
																	      <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+salSlipArray[p]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
																	      
																	      <br><a class="mydocumentviewpan" onclick="viewDocumentInPopUpfromHistory('<%=salSlipArray[p]%>')"> </a>
																	      <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=salSlipArray[p]%>"> </a>
																	       		
													       		  <%} %>
													       				      
													      </li>
												     
											 
											   <%}
												  
												  } else {%>
												  			   
												             <h1>Salary slips are not added in this record !</h1>
												      		 	       					  		  			  
												  <%} %>
											 
											  </ul>
												       			
											 </div>
											 
										  </div>
									        </div>
									        </div>
									   
									           
									     
									     </div>                            
								
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

  
	
	
	
	
	
	
	    
</div> <!--end of right div  -->

	</div>

</div>
	
<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>					
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>
<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
								
								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->


    <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
		
	
	<!-- Document Pop up Div -->
	
	<div id="kycDocPopUplight">
	
	<div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		         
		  		
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:0; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF Document Pop up Div -->	


<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->



	
</body>
</html>
     	