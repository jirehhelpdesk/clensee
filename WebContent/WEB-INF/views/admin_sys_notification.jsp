<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>System Notification</title>

<script>
function notifyToAll()
{
	$("#waitingdivId").show();
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var formData = new FormData($("#notifyForm")[0]);						
					    $.ajax({
					        url: "notifyToAll.html",
					        type: 'POST',
					        data: formData,
					        //async: false,
					        success: function (data) {          
					            
					        	$("#waitingdivId").hide();
					        	
					        	if(data=="success")
					        		{
					        			alert("Admin Notification has sent successfully.");
					        		}
					        	else
					        		{
					        			alert("Admin Notification has failed,try again !");
					        		}					        						        	
					        },
					        cache: false,
					        contentType: false,
					        processData: false
					    });	
				}
			else
				{
				$("#waitingdivId").hide();
				     $("#sessionlight").show();
			         $("#sessionfade").show();
				}
		}
	});
	
	
	
}
</script>

</head>
<body>
<h1>System Notification:</h1><br>
<div id="notifydivId" style="display:block;margin-left:74px;margin-top:32px;width:725px;background-color: #fff;border: 2px solid #00b6f5;border-radius: 14px;">
	       
	       <form name="notifyForm" id="notifyForm" >
	       
			      <ul style="text-align:left;width:665px;">
			      
					       <li>
					              <input type="hidden"  value="System"   name="notification_type" />
					              <input type="hidden" id="" value="System" name="notification_to" />
					           To:<input type="text" id="indToNameValue" value="All KYC User(Individual & Utilizer)" style="width:250px;height:20px;" readonly/>
					       </li>
					       
					       <li>    
					           Notification Subject:
					           <input type="text" name="notification_subject" maxlength="90" style="width:652px;height:20px;"/>
					       </li>
					       
					       <li>    			           
					           Notification Message:
					           <textarea name="notification_message" style="width:652px;height:60px;">
					           
					           </textarea>
			               </li>	
			               
			               <li>    			           
					            <input type="button" value="Notify" style="align:right;" onclick="notifyToAll()"/>
			               </li>               
	              </ul> 
	            
	       </form>
	       	        
	 </div>


</body>
</html>