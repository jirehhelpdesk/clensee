<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>About Us</title>

<style>

</style>

</head>
<body>

	
		
		    <div class="popheaderstyle"><h1>About Us</h1></div>

            <h3 style="font-size: 24px ! important;">Welcome to Clensee !</h3>
            
			<p class="popuptext">
									First of its kind service, Clensee (www.clensee.com) is the product of "Wired Document Processing Pvt Ltd"
						designed in a dynamic fashion to provide wide range of services to all the Individuals and Utilizers in
						modern era. We at Clensee aim at sharing documents paperless, instantaneous, secure, economical, nonreputable
						and eco-friendly for all our users.
			</p>			
						
						<h3>Individuals</h3>
						
						<p class="popuptext">
						This application helps the individuals to design their profile to make it handy and share the information to
						any one. It also helps in keeping their most important and often shared documents (KYC documents,
						Academic documents, Employment documents, Financial documents and much more to come) in an
						organized fashion that keeps them handy to share with friends, families and registered Utilizers at any time.
						</p>
						
						<h3>Utilizers</h3>
						
						<p class="popuptext">
						This application helps the Utilizers to receive the documents from the individuals in a never ever easier way
						and get the latest information of individuals when ever individuals update the information / documents in
						Clensee. It also helps in Record keeping of the customer details.
						Our portal/platform enhances the customer convenience and greatly increases business efficiency across
						sectors and streamlines the process of on-boarding new customers. It simplifies the process of linking
						existing customer accounts to their respective documents in an easy yet secure manner. Proposed to
						integrate with the sector regulators for extending online-documentation to their respective sectors.
						</p>	
			
	

</body>
</html>