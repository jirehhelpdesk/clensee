<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>kycindex</title>
<link href="resources/css/kycstyle.css" rel="stylesheet" type="text/css" />

</head>
<body>

    <div class="container">
		<div class="top_line">
		</div>
			<header class="header">
			<div class="header_inner">
				<div class="logo">
				</div>
			  <div class="search-bar">
				 <div id="tfheader">
						<form id="tfnewsearch" method="get" action="#">
						 <input  type="text" id="tfq" class="tftextinput2" name="q" size="21" maxlength="120" value="Search our website"/>
                        
						 <input type='image' alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.jpg'/>
						 
							</form>
							<div class="tfclear"></div>
				 </div>     
				</div>
                <div class="tfclear"></div>
					 <div class=" mainmenu">
							 <ul class="main_menu">
                             
								<li><a class="individual" href="individual.html"></a></li>
								<li><a class="corporate" href="corporate.html"></a></li>
								<li><a class="utiliser" href="utiliser.html"></a></li>
							   
							 </ul>   
						</div>       
				
				 </div>

			</header>
				<div class="banner_center">
					<div class="slider_iamge"></div>
					 <div class="slider_buttons">    
					 <ul id="image_slider">
							<li><a href="#"><img src="resources/images/slider_btn_hover.png" width="11" height="11"/></a></li>
							<li><a href="#"><img src="resources/images/slider_btn.png" width="11" height="11" /></a></li>
							<li><a href="#"><img src="resources/images/slider_btn.png" width="11" height="11" /></a></li>
							<li><a href="#"><img src="resources/images/slider_btn.png" width="11" height="11" /></a></li>
						   
						</ul>     
					</div>
                    
				</div>
				
		<!------center End-------->
		<footer class="footer">
		   
		   <div class="footer_center">
           <div class="footer_topline"></div>
				<div class="bottom-icons">    
				 <ul id="bottom-icons-img">
						<li ><a href="#"><img src="resources/images/why_kyc_icon.png" width="61" height="43" /></a>
                        <h2>WHY KYC</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing.</p>
                        </li>
						<li class="bottom-icons-leftline" ><a href="#"><img src="resources/images/kyc_store_icon.png" width="61" height="43" /></a>
                        <h2>KYC STORE</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing.</p>
                        </li>
						<li class="bottom-icons-leftline"><a href="#"><img src="resources/images/kyc_answers_icon.png" width="61" height="43" />  </a>
                        <h2>KYC ANSWERS</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing.</p>
                        </li>
						<li class="bottom-icons-leftline"><a href="#"><img src="resources/images/meet_members_icon.png" width="61" height="43" /></a>
                        <h2>MEET MEMBERS</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing.</p>
                        </li>
					   
					</ul>     
				</div>
			</div>
				<div class="footer_copyrights">
                <div class="copyrights">    
					 <ul >
                     		<li class="footer_space"></li>
							<li class="footer_space01"><p>Design by:<span> <a href="http://jirehsol.com/">Jireh </a></span></p>
                            <p> © All rightacs Reserved KYC 2014- 2015</p>
                            </li>
							<li class="footer_space02"> <p><a href="#">About Us</a> |<a href="#"> Term and Conditions</a> |<a href="#"> Privacy Policy</a> | <a href="#">Contact Us</a></p></li>
							<li class="footer_space03"> 
                            
                            <div class="four columns">
                                <ul class="social_media">
                                    <li>
                                    <a class="google_plus" href="https://plus.google.com/"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://www.facebook.com/"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="http://www.linkedin.com/company/1872121?trk=tyah"></a>
                                    </li>
                                    <li>
                                    <a class="rss" href="http://feeder.co/"></a>
                                    </li>
                                </ul>
                              </div>
                            
                            </li>
						   
						</ul>     
					</div>
                
				</div>
		   
			<div class="footer_line">
			</div>

		 </footer>

    </div>
<!------container End-------->
</body>
</html>
