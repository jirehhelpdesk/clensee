<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<html>
<head>
<link rel="stylesheet" href="resources/css/kyc_admin_style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script>
function categoryPlanNames(category)
{
	if(category!="Select Category")
	   {
		
		  if(category=="Utilizer")
			  {
			      $("#utilizerplanDomain").show();
			  }
		  else
			  {
			  	  $("#utilizerplanDomain").hide();
			   	  $("#planNameSpanId").hide();		
			  	
				  	$.ajax({  
					    type : "Post",   
					    url : "get/plan/name/bycategory.html", 
					    data :"planCategory="+category,	     	     	     
					    success : function(response) 
					    {  		    		    		    	
					    	if(response!="No Data")
						   	   {
					    		 
						   		   var planName = response.split(",");
							   	   var values = "<table style=\"width:95%\"><tr><th>S.NO</th><th>PLAN NAME</th><th>UPDATE</th><th>REMOVE</th></tr>";
								   for ( var i = 0,j=1; i < planName.length;i++,j++) {
										
									   values += "<tr><td>" + j
												+ "</td><td>" + planName[i]
												+ "</td><td>" + "<input type=\"button\" value=\"Edit\" id=\""+planName[i]+"\" onclick=\"updatePlan(this.id)\"/>"
												+ "</td><td>" + "<input type=\"button\" value=\"Delete\" id=\""+planName[i]+"\" onclick=\"deletePlan(this.id)\"/>"														
												+ "</td></tr>";
									}
								   values += "</table>";
							   		   				   		   
						   		   $("#planNameSpanId").show();	   		   		   		   
						   		   $("#planNameSpanId").html(values);	
						   		   
						   	   }	
					    	else
					    		{	    		   
					    		  	 alert("For "+category+" no plans created !");   
					    		  	 $("#planNameSpanId").hide();				    		  	 
					    		}
					    },  		   
				    });  
			  	  
			  }
			
			
	   }
}

function getPlanAsperCatDomain(planDomain)
{
  	if(planDomain!="Select Plan Domain")
  		{
	  		$.ajax({  
			    type : "Post",   
			    url : "getplanviaDomainforutilizer.html", 
			    data :"planDomain="+planDomain,	     	     	     
			    success : function(response) 
			    {  		    		    		    	
			    	if(response!="No Data")
				   	   {
			    		 
				   		   var planName = response.split(",");
					   	   var values = "<table style=\"width:95%\"><tr><th>S.NO</th><th>PLAN NAME</th><th>UPDATE</th><th>REMOVE</th></tr>";
						   for ( var i = 0,j=1; i < planName.length;i++,j++) {
								
							   values += "<tr><td>" + j
										+ "</td><td>" + planName[i]
										+ "</td><td>" + "<input type=\"button\" value=\"Edit\" id=\""+planName[i]+"\" onclick=\"updatePlan(this.id)\"/>"
										+ "</td><td>" + "<input type=\"button\" value=\"Delete\" id=\""+planName[i]+"\" onclick=\"deletePlan(this.id)\"/>"														
										+ "</td></tr>";
							}
						   values += "</table>";
					   		   				   		   
				   		   $("#planNameSpanId").show();	   		   		   		   
				   		   $("#planNameSpanId").html(values);	
				   		   
				   	   }	
			    	else
			    		{	    		   
			    		  	 alert("For "+planDomain+" Domain no plans created !");   
			    		  	 $("#planNameSpanId").hide();				    		  	 
			    		}
			    },  		   
		    });  						
  		}
  	else
  		{
  		    $("#utilizerplanDomain").hide();
  		}
}


function deletePlan(planName)
{	 	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
					var planType = document.getElementById("plan_categoryId").value;	      
					
					$.ajax({  
				 		     type : "Post",   
				 		     url : "deletePlan.html", 
				 		     data :"planName="+planName+"&planType="+planType,	     	     	     
				 		     success : function(response) 
				 		     {  
				 		    	 alert(planName+" successfully Deleted from the category !");
				 		    	 categoryPlanNames(planType);
				 		     },  		    
				 		}); 
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
 }



function updatePlan(planName)
{  	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
							
							var planCategory = document.getElementById("plan_categoryId").value;	
							var planDomain = "";	
							  
							if(planCategory=="Individual")
							{
								planDomain = "Default";
							}
							else
							{
								planDomain = document.getElementById("plan_domainId").value;	
							}
							
							   $.ajax({  
							    	  
							     	     type : "Post",   
							     	     url : "editplandetail.html", 
							     	     data :"plan_name="+planName+"&plan_type="+planCategory+"&planDomain="+planDomain,	     	     	     
							     	     success : function(response) 
							     	     {  
							     	    	
							     	    	var planDetails = response.split("#");
							     	    	var featureDetails = planDetails[0].split(",");
							     	    	var planFeatureIds = planDetails[1].split(",");
							     	    	
							     	    	var values = "<h1>"+planName+" Plan Details:</h1><br>";
							     	    	values += "<b>Plan Price:</b><input type=\"text\" value=\""+planDetails[2]+"\" name=\"plan_price\" id=\"plan_price\" /><input type=\"hidden\" value=\""+planName+"\" name=\"plan_name\" id=\"plan_nameId\" /><br>";
							     	        values += "<table style=\"width:95%\"><tr><th>FEATURE NAME</th><th>FEATURE DATE</th><th>FEATURE PRICE</th><th>FEATURE STATUS</th></tr>";
											for ( var i = 0;i<featureDetails.length;i = 4 + i) 
											{	
												var flag = 'true';
												
												for(var j=0;j<planFeatureIds.length;j++)
													{
													   if(planFeatureIds[j]==featureDetails[0 + i])
														   {
														       flag = 'false';	
															   values += "<tr><td>" + featureDetails[1 + i]										
																		+ "</td><td>" + featureDetails[2 + i]
																		+ "</td><td>" + featureDetails[3 + i]									
																		+ "</td><td>" + "<input type=\"checkbox\" id=\"selectedfeature\"  name=\"selectedfeature\" checked=\"true\" value=\""+featureDetails[0 + i]+"\">"																													
																		+ "</td></tr>";
														   }						   					  
												    }
												
												 if(flag=='true')
												   {
													   values += "<tr><td>" + featureDetails[1 + i]										
																+ "</td><td>" + featureDetails[2 + i]
																+ "</td><td>" + featureDetails[3 + i]									
																+ "</td><td>" + "<input type=\"checkbox\" id=\"selectedfeature\"  name=\"selectedfeature\"  value=\""+featureDetails[0 + i]+"\">"																													
																+ "</td></tr>";
												   }
											 }
											
											   values += "</table>";
										   	   			   		   
									   		   $("#featureDetailsDivId").show();	   		   		   		   
									   		   $("#featureDetailsDivId").html(values);	
									   		   
							     	     },  		      	    
							     });           
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
       
   
}

function reOrganizePlan()
{
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var click = [];
						$("#selectedfeature:checked").each(function() {
					        click.push($(this).val());
					    });
						 	
						var plan_type = document.getElementById("plan_categoryId").value;	      
						var plan_Name = document.getElementById("plan_nameId").value;
						var plan_price = document.getElementById("plan_price").value;
						var planDomain = document.getElementById("plan_domainId").value;
							
						  $.ajax({
								type : "post",				
								url : "reOrganizePlan.html",				
								data :"plan_type="+plan_type+"&plan_Name="+plan_Name+"&plan_price="+plan_price+"&fatures_id="+click+"&planDomain="+planDomain,	  
								success : function(response) {
									
									if(response=="success")
										{
										      alert(plan_Name+" plan Details successfully updated !");
										      $("#featureDetailsDivId").hide();	   	
										}
									else
										{
										      alert("Problem occured due to some reason,Please try again !");
										      $("#featureDetailsDivId").hide();	 
										}
								},
								
							});		
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
		
}

</script>

</head>
<body>

		<div>
        <b>Select Category:</b>        
                <select name="category_name" id="plan_categoryId" onchange="categoryPlanNames(this.value)">
                          <option value="Select Category">Select Category</option>
                          <option value="Individual">Individual</option>
                          <option value="Utilizer">Utilizer</option>
                          <option value="Corporate">Corporate</option>
                </select>               		
		</div>
		
		<div id="utilizerplanDomain" style="display:none;">
        <b>Select Plan Domain:</b>        
                <select name="category_name" id="plan_domainId" onchange="getPlanAsperCatDomain(this.value)">
                          <option value="Select Plan Domain">Select Plan Domain</option>
                          <option value="Recruiter">Recruiter</option>
				          <option value="Banks">Banks</option>
				          <option value="Insurance">Insurance</option>
				          <option value="Travel Agent">Travel Agent</option>
				          <option value="Govt-Department">Govt-Department</option>
				          <option value="Telecommunication">Telecommunication</option>
				          <option value="Money Transfer Agents">Money Transfer Agents</option>
				          <option value="Hotel and Lodging">Hotel & Lodging</option>
				          <option value="Corporate">Corporate</option>
				          <option value="Other">Other</option>
                </select>               		
		</div>


        <div id="planNameSpanId" class="datagrid" style="display:none;width:100%;">
               	
		</div>
		
	     <div id="featureDetailsDivId" class="datagrid" style="display:none;width:100%;">   
	            
	     </div>
	     
		 	
	     <div id="Utiliser" align="center">
	     <input type="button" id="edit" onclick="reOrganizePlan()" value="Update"> 
	     </div>
	 
</body>
</html>