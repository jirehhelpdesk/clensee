<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<style>
.liAlign
{
  float:left;
  margin-left: 15px;
  height: 33px;
}

.textStyle
{
    background-color: #ccc;
    height: 27px;
    margin-top: 10px;
    width: 180px;
}

.buttonStart
{
    height: 33px;
    margin-top: 9px;
    width: 65px;
}


#IndividualDetailsAsPerData
{
   height: auto;
   margin-top: 100px;
   width: auto;
}
</style>


<script>

function getThisUserDetails()
{
    var flag = 'true';
    
	var year = document.getElementById("yearValue").value;
	var searchValue = document.getElementById("individualId").value;
	var searchType = document.getElementById("searchType").value;
	
	if(year=="Select Year")
	{
		flag='false';
		alert('Please select year.')
	}
	if(searchValue=="")
	{
		flag='false';
		alert('Please enter a search value.')
	}
	if(searchType=="Search Individual")
	{
		flag='false';
		alert('Please select search type.')
	}
	
	if(flag=='true')
		{
					$("#waitingdivId").show();
					
					$.ajax({  
					     type : "Post",   
					     url : "getProfileDetailsForEncrypt.html", 	
					     data: "registerYear="+year+"&searchType="+searchType+"&searchValue="+searchValue,
					     success : function(response) 
					     {  
					    	 if(response.indexOf("<img")>0)
					    		 {
					    		 	$("#waitingdivId").hide();
						    	 	$("#IndividualDetailsAsPerData").html(response);  	    	
					    		 }
					    	 else
					    		 {
					    		    $("#waitingdivId").hide();
						    	 	$("#IndividualDetailsAsPerData").html("<h1 style=\"margin-left: 200px;\">Individual not exist under the year of "+year+" and id "+indId+"</h1>");
					    		 }
					    	 													
					     },  
					        		    
			      });
		}
	
	
}

function encryptThisUserFile()
{
	 var flag = 'true';
	    
		var year = document.getElementById("yearValue").value;
		var indId = document.getElementById("individualId").value;
		var directoryValue = document.getElementById("directoryValue").value;
		var searchType = document.getElementById("searchType").value;
		
		if(year=="Select Year")
			{
			    flag='false';
			}
		
		if(indId=="")
			{
				flag='false';
			}
		
		if(directoryValue=="Select Directory")
			{
		    	flag='false';
			}
		if(searchType=="Search Individual")
			{
				flag='false';
				alert('Please select search type.')
			}
		
		if(flag=='true')
			{
						$("#waitingdivId").show();
						
						$.ajax({  
						     type : "Post",   
						     url : "encryptThisUserFiles.html", 	
						     data: "registerYear="+year+"&searchType="+searchType+"&individualId="+indId+"&directoryValue="+directoryValue,
						     success : function(response) 
						     {  
						    	 $("#waitingdivId").hide();
						    	 
						    	 alert("As per your selected user documents got encrypted successfully.");
						    	 managefileScheduler();	
						    	 
						     },  
						        		    
				      });
			}	
		
}

function ShowAllOption()
{	
	var allClensee = document.getElementById("allClensee").checked;
	document.getElementById('indClensee').checked = false; 
	
}

function ShowIndOption()
{	
	var indClensee = document.getElementById("indClensee").checked;
	document.getElementById('allClensee').checked = false; 
	
}

function selectForSearch(searchType)
{
   if(searchType=="Indid")
   {
	   document.getElementById("individualId").placeholder = "Enter Individual UniquiId";
   }
   else if(searchType=="Kycid")
    {
	     document.getElementById("individualId").placeholder = "Enter Individual Kyc Id";
	   }
   else if(searchType=="EmailId")
	   {
	     document.getElementById("individualId").placeholder = "Enter Individual Email id";
	   }
   else
	   {
	      document.getElementById("individualId").placeholder = "Enter Search Value";
	   }
}

</script>

</head>

<body>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>


<% String yearList = (String)request.getAttribute("yearList");
   
   String yearListArray[] = yearList.split(",");

%>




<div id="encryptOfIndDoc" style="margin-left: -28px;">
     
     <ul>
         
          <li class="liAlign">
               <select id="yearValue" style="width:93px ! important;">
               			
               			<option value="Select Year">Select Year</option>
               			
               			<%for(int j=0;j<yearListArray.length;j++){%>
               			
               				<option value="<%=yearListArray[j]%>"><%=yearListArray[j]%></option>
                        
                        <%}%>
                        
               </select>
               
         </li>
          
         <li class="liAlign">         	
         	    
         	    <select id="searchType"  style="width:131px ! important;" onchange="selectForSearch(this.value)"> 
               			
               			<option value="Search Individual">Search Individual</option>
               			<option value="Indid">By Individual Unique Id</option>
               			<option value="Kycid">By Kyc Id</option>              			
               			<option value="EmailId">By Email Id</option>
               			
                </select>   
                     
         </li> 
          
         <li class="liAlign"> 
         
              <input type="text" class="textStyle" id="individualId" placeholder="Enter Search Value"  onchange="getThisUserDetails()"/>        
       
         </li>
          
        
         <li class="liAlign">         	
         	    
         	    <select id="directoryValue" style="width:140px ! important;">
               			
               			<option value="Select Directory">Select Directory</option>
               			<option value="KYC_DOCUMENT">Kyc Documents</option>
               			<option value="ACADEMIC_DOC">Academic Documents</option>
               			<option value="Employement_DOC">Employment Documents</option>
               			<option value="Financial_DOC">Financial Documents</option>
               			<option value="All Documents">All Documents</option>
                       
               </select>   
                     
         </li>
          
          
         <li class="liAlign">
              
              <input class="buttonStart" type="button" value="Start"  onclick="encryptThisUserFile()" />
         
         </li>
          
     </ul>
      
</div>


   
   <div   id="IndividualDetailsAsPerData">
   
   
   </div>

</body>
</html>