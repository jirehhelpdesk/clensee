<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>${regname.firstname}'s Profile</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />
<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>
<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/onlineresources/code.jquery.com.jquery-1.10.2.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<link rel="stylesheet" href="resources/documentResources/kyc_document_css.css" />



<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}


</style>



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 33%;
    width: 800px;
    z-index: 1002;
}

</style>



<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}



</script>


<!-- <script>

$(document).ready(function(){

if(!Modernizr.input.placeholder){

	$('[placeholder]').focus(function() {
	  var input = $(this);
	  if (input.val() == input.attr('placeholder')) {
		input.val('');
		input.removeClass('placeholder');
	  }
	}).blur(function() {
	  var input = $(this);
	  if (input.val() == '' || input.val() == input.attr('placeholder')) {
		input.addClass('placeholder');
		input.val(input.attr('placeholder'));
	  }
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
	  $(this).find('[placeholder]').each(function() {
		var input = $(this);
		if (input.val() == input.attr('placeholder')) {
		  input.val('');
		}
	  })
	});

}

});
</script>
 -->


<script>

function showHideDiv1(id){
	

	myTextBox1 = document.getElementById("b6");
	value=myTextBox1.value;
	
	  var obj = document.getElementById(id);
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	    } else if(obj.style.display=="block"){
	      obj.style.display='none';
	    }
	}


</script>

<script>
 $(document).ready(function () {
        $("#dialog").dialog({ autoOpen: false });
 
          
                return false;
           
    });
</script>

<script type="text/javascript">

function viewPro1(){
		
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
        
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearchprofilefromUtilizer.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
}


function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function aboutUsfadeout()
{
	document.getElementById('aboutUslight').style.display='none';
	document.getElementById('aboutUsfade').style.display='none';
}


</script>





<script>

function divvalue()
{
	
	document.getElementById("landpagedescription").value=document.getElementById("editsumdiv").innerHTML ;
	alert(""+document.getElementById("landpagedescription").value);
}

/*home page*/

function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/

</script>
         
 <script>
function openbrowse()
 
 { 
	 
	 var obj = document.getElementById('openbrowse');
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	      document.getElementById("removeaddphoto").style.display="block";
	     
	    }	  
 }
</script>
 
<script src="resources/texteditor/nicEdit.js" type="text/javascript"></script>

<script>

/* Main Header Actions  */


function redirectToLoginIndividual()
{
	window.location = "utisignOut.html";	
}

function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}


</script>

<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}


</style>

<script>

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewKycDocumnt(fileName)
{	
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	
	    var indiId = document.getElementById("indId").value;
	    
		$.ajax({
			type : "post",				
			url : "viewDocFromUtilizer.html",
			data: "fileName="+fileName+"&individualId="+indiId,
			success : function(response) {
			
				document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
	
				document.getElementById('kycDocPopUplight').style.display='block';
		        document.getElementById('kycDocPopUpfade').style.display='block';
		        $("#kycDocPopUpDisplay").html(response);
		        			
			},		
		});		 		 		 					
}


function kycHistory(compType)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';	       
    
    var individualId = document.getElementById("indId").value;
    
	 $.ajax({
			type : "post",				
			url : "kychistoryfromUti.html",				
			data :"fileName="+compType+"&extp="+individualId,	  
			success : function(response) {
			
				document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';	       
			    
			    document.getElementById('kycHistoryLight').style.display='block';
				document.getElementById('kycHistoryFade').style.display='block';
				$("#kycHistoryDisplay").html(response);
			    
							
			},
		
		});		 		
}

function fadeOutHistoryDiv()
{
	document.getElementById('kycHistoryLight').style.display='none';
	document.getElementById('kycHistoryFade').style.display='none';
}

function showProfileAgain()
{	
	window.location = document.getElementById("redirectUrl").value;	
}

function showKycDocuments()
{
	var individualId = document.getElementById("indId").value;
	//window.location = "viewKycDocumentsFrmUti.html";	
	window.location = "redirectviewKycDocumentsFrmUti.html?accgiver="+individualId;	
}

function showAcademicDocuments()
{
	var individualId = document.getElementById("indId").value;
	//window.location = "viewAcademicDocumentsFrmUti.html";	
	window.location = "redirectviewAcademicDocumentsFrmUti.html?accgiver="+individualId;	
}

function showEmpDocuments()
{
	var individualId = document.getElementById("indId").value;
	//window.location = "viewEmpDocumentsFrmUti.html";	
	window.location = "redirectviewEmpDocumentsFrmUti.html?accgiver="+individualId;	
}

function showFinancialDocuments()
{
	var individualId = document.getElementById("indId").value;
	//window.location = "viewfindocumentsfrmUti.html";	
	window.location = "redirectviewfindocumentsfrmUti.html?accgiver="+individualId;	
}

function profilePicHistory()
{
	var individualId = document.getElementById("indId").value;
	 $.ajax({
			type : "post",
			url : "getIndProfilePicturesHistory.html",	
			data: "indvidualId="+individualId,
			success : function(response) {		
						
			    $("#content").html(response);
			
			},
			
		});
}

function activeSidebutton()
{
	document.getElementById("kycActive").style.background="-moz-linear-gradient(center top , #f4f4f4, #e9e9e9) repeat scroll 0 0 rgba(0, 0, 0, 0)";
	document.getElementById("kycActive").style.color="#05b7f5";
	document.getElementById("kycActive").style.border="1px solid #ccc";
}

</script>

<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
</head>

<body onload="hideAddvertise(),activeSidebutton()" onclick="hidesearchDiv()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };

    String regVisibility = (String)request.getAttribute("regVisibility");
    String basicVisibility = (String)request.getAttribute("basicVisibility");
    String familyVisibility = (String)request.getAttribute("familyVisibility");
    String socialVisibility = (String)request.getAttribute("socialVisibility");
    
    
    String profileAccess = (String)request.getAttribute("profileAccessDetails");
    String otherAccess = (String)request.getAttribute("otherAccess");
    
  
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("tempIdasdgthfmUti")%>"/>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		
		<%
		   String noOfAwaitedDocuments = (String)request.getAttribute("noOfAwaitedDocuments");
		   String noOfRecivedDocuments = (String)request.getAttribute("noOfRecivedDocuments");
		%>
		
		
		<div class="header_inner">
		
		  <a href="utimyProfile.html"><div class="logo"></div></a>

			<div class="tfclear"></div>
			<div class="mainmenu" style="margin-right:181px;">
				
				<ul class="uti_main_menu">
					
					<li id="myMenus1"><a class="myprofilefocus" href="utimyProfile.html"> <br>
							<br> My Profile
					</a></li>
					
					<li id="myMenus2" style="width:140px">
					     
					        <a class="mydocuments" href="reciveddocuments.html" > <br>
							<br> Received Documents
					        </a>
					     
					        <%if(!noOfAwaitedDocuments.equals("0")){ %>
												  
							     <div class="noti_bubble_right"><%=noOfAwaitedDocuments%></div>
							     
							 <%} %>
					     
					 </li>
					
					<li id="myMenus4"><a class="myplans" href="utilizerPlan.html"> <br> <br>
							My Plans					
					</a></li>
					
					<li id="myMenus6"><a  class="alearticons" href="utilizeralerts.html"><br>
							<br> Alerts
					</a>
					</li>	
					
					<li id="myMenus5"><a class="mysettings" href="utilizerSettings.html"  > <br> <br>
							My Settings					
					</a>
					</li>
					
				</ul>
				

			</div>

		</div>
		
 <!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=(String)request.getAttribute("utiFirstName")%></em></a>
                 <div style="clear:both"></div>
           <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <span class="topprofilestyle" style="display: none;"></span>		
						<div id="searchicon" style="display:none;">
																
							 <div id="searchhomepage" style="display:block;">
							
							 </div>
													
				        </div>
				        
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="utisignOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			      			
					<div class="leftside">
					
					<div id="textBox">
					<c:if test="${!empty profilePicInfo.profile_picture_name}">
	 
						 <%
	 						 	String filename1 = (String) session.getAttribute("profilepic");
	 						 	int searchedIndId = (Integer)request.getAttribute("searched_Indid");
						        String user0 = Integer.toString(searchedIndId);
	 					 %> 
	 					   
	 					  <c:set var="profilePicture"  value="${profilePicInfo.profile_picture_name}"/>
	 					  
	 					  <%String profilePicture = (String) pageContext.getAttribute("profilePicture");%>
										
						 <div class="user_photoupload view view-first" id="user_photoupload" style="height:224px ! important;margin-top:22px ! important;">
					    
					     <span> 
					     <img src="${pageContext.request.contextPath}<%="/previewprofileOther.html?fileName="+profilePicture+"&docCategory=Profile_Photo&year="+session.getAttribute("created_year")+"&indId="+user0+""%>" /></img>
					     </span>						   
						     
						     <div id="profileHover" class="mask">                       
	                               	                                	                                
	                               <form name="editbasicdetail" id="editbasicdetail"  method="POST" enctype="multipart/form-data">
	                                                                        
		                               <p style="float:left"><!-- --></p>
		                               <div id="profilePhotoDiv" style="float: left;margin-left: -5px;margin-top: -69px;width: 100px;display:none;"><a  class="info" style="height: 16px;margin-left: 0;margin-right: -7px;margin-top: 11px;width: 72px;" onclick="uploadbasicdetail1()">Save</a></div>		                               
		                               <p style="float:left;margin-top:5px;margin-left:8px;"><a href="#" class="info" onclick="openOffersDialog();profilePicHistory();">History</a>
		                               </p>
		                               
	                              </form>    
	                                                        														
						     </div>
						     
						 </div>
						   
						 <div class="user_photo">
							 <p><strong><c:out value="${regname.firstname}"></c:out></strong> <br>
								 <span>
									 <c:if test="${!empty basicdetails.present_address}">
									 <c:set var="presentplace"  value="${basicdetails.present_address}"/>
												<%
													String myVariable = (String) pageContext
																	.getAttribute("presentplace");
															String[] ary = myVariable.split("##");
															String str = ary[4];
															str = str.substring(0, str.length());
												%>
									 <%=str%>
									 </c:if>
								 </span>			 
							 </p>
						 </div>
				</c:if>	
									    
				
				<div id="overlay" ></div>
					
					<div id="boxpopup" class="box">
						
						<a onclick="closeOffersDialog('boxpopup');" class="boxclose"></a>
						 <div class="popheaderstyle"><h1 align="left">Previous Profile Pictures</h1></div>
						<div id="content">
						    
						</div>
					</div>
					            
                <div class="user_display">
                <div id="closepic" style="display: block;">
                
                
                <c:if test="${empty profilePicInfo.profile_picture_name}">
                                
                <div class="user_photoupload view view-first" id="user_photoupload">
	 
				<span id="userphoto"><img src="resources/kyc_images/Koala.jpg" width="196" height="222" /></span>
					 
			   </div> 
						   
			 <div class="user_photo">
					<p><strong><c:out value="${regHisname.first_name}"></c:out></strong> <br>
					<span>
					<c:if test="${!empty basicdetails.present_address}">
					<c:set var="presentplace"  value="${basicdetails.present_address}"/>
									<%
										String str = "";
												String myVariable = (String) pageContext
														.getAttribute("presentplace");
												String[] ary = myVariable.split("##");
												str = ary[4];
												str = str.substring(0, str.length());
									%>
						<%=str%>
					</c:if>
					</span>
					</p>
			  </div>
                              	
		   </c:if>		          	 					
	      </div>
	      </div>		
	      
	         		     	         		      	         		                  								 				 																			
						<div class="myaccounts">
							<h2>
							<span>
							<img src="resources/kyc_images/contact_info.png" width="25" height="26" />
							Contact Info</span>
							</h2>							
							<div class="account_titel"><img src="resources/images/info_mainid_icons.png" /><span>E-mail address</span></div>
							<ul>
							<li>
							<table>
							<tbody>
							<tr>
							<td>							
							<span id="emailid">
							<%if(regVisibility.charAt(0)=='1') {%>
							
							<c:out value="${regHisname.email_id}"></c:out>
							
							<%}else { %>
							
									<%if(profileAccess.contains("REGISTRATION-Email Id")) {%>
									
									    <c:out value="${regHisname.email_id}"></c:out>
									
									<%}else{ %>
									
									    <c:out value="Restricted"></c:out>
									
									<%} %>
							<%} %>
							</span>							
							</td>
							</tr>
							</tbody>
							</table>
							</li>
							</ul>
							<div class="mobileaccount_titel"><img src="resources/images/info_mobile_icons.png" /><span>Mobile Number</span></div>
							<ul>
							<li>
							<table>
							<tbody>
							<tr>
							<td><span id="mobileno">
							<%if(regVisibility.charAt(1)=='1') {%>
							<c:out value="${regHisname.mobile_no}"></c:out>
							<%}else { %>
							
									<%if(profileAccess.contains("REGISTRATION-Mobile No")) {%>
									
									    <c:out value="${regHisname.mobile_no}"></c:out>
									
									<%}else{ %>
									
									    <c:out value="Restricted"></c:out>
									
									<%} %>

							<%} %>
							
							</span></td>
							</tr>
							</tbody>
							</table>
							</li>
							</ul>
							
							<div class="present_addressaccount_titel"><img src="resources/images/info_presentaddress_icons.png" /><span>Present Address</span></div>
							<ul>
							<li>
							<div id="presentaddressget" >
								<span id="PresentAddID" style="color:#2d2d2d;">
								<c:if test="${!empty basicdetails.present_address}">
								<%-- <c:out  value="${basicdetails.present_address}" ></c:out> --%>
								<c:set var="present_address"  value="${basicdetails.present_address}" ></c:set>
								
								<%
																	String present_address = (String) pageContext
																				.getAttribute("present_address");
																		String pre_addressAry[] = present_address.split("##");
																%>
								<%if(basicVisibility.charAt(8)=='1') {%>
											
								           <table class="textwidth">
											<tr><td><%=pre_addressAry[0]%>,<%=pre_addressAry[3]%>,<%=pre_addressAry[2]%>,
											         <%=pre_addressAry[1]%>,<%=pre_addressAry[1]%>,<%=pre_addressAry[7]%>,
											         <%=pre_addressAry[4]%>,<%=pre_addressAry[5]%>,<%=pre_addressAry[6]%>,
											         <%=pre_addressAry[8]%></td></tr>
											</table>
								
								<%}else{ %>
								
								     <%if(profileAccess.contains("BASIC-Present Address")) {%>
									
									    	<table class="textwidth">
											<tr><td><%=pre_addressAry[0]%>,<%=pre_addressAry[3]%>,<%=pre_addressAry[2]%>,
											         <%=pre_addressAry[1]%>,<%=pre_addressAry[1]%>,<%=pre_addressAry[7]%>,
											         <%=pre_addressAry[4]%>,<%=pre_addressAry[5]%>,<%=pre_addressAry[6]%>,
											         <%=pre_addressAry[8]%></td></tr>
											</table>
									
									<%}else{ %>
									
									      Restricted as per the visibility !
									
									<%} %>
							      
								<%} %>
								
								</c:if>
								
								<c:if test="${empty basicdetails.present_address}">																
										Not Filled Yet !
								</c:if>
								
								</span>														
							</div>
							</li>
							</ul>
							</div>
							
							<c:if test="${!empty basicdetails.languages}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/languages_known_icon.png" width="25" height="26" />Languages Known</span>
							</h2>
							<ul>
							<li>
							<div id="languageDiv">
								           
								           <%if(basicVisibility.charAt(6)=='1') {%>			
								                 <c:out value="${basicdetails.languages}"></c:out>					                 								                 					           
								           <%}else{ %> 
								                 <c:out value="Language is Restricted"></c:out>			
								           <%}%>
								           
								</div>	
							</li>
							</ul>
							</div>
							</c:if>
							
							<c:if test="${empty basicdetails.languages}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/languages_known_icon.png" width="25" height="26" />Languages Known</span>
							</h2>
							<ul>
							<li>
							<div id="languageDiv">Not Filled Yet !</div>
							</li>
							</ul>
							</div>
							</c:if>
							
							
							<c:if test="${empty socialaccvalue}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/social_account_icon.png" width="25" height="26" />Social Account </span>
							</h2>
							<ul>
							<li>
							<img src="resources/kyc_images/face_book_icon.png" width="17" height="17" /><div id="websitelinkname">Not Available</div>
							</li>
							<li>
							<img src="resources/kyc_images/twitter_icon.png" width="17" height="17" /><div id="twitterlinkname">Not Available</div>
							</li>
							<li>
							<img src="resources/kyc_images/linkedin_icon.png" width="17" height="17" /><div id="linkedlnlinkname">Not Available</div>
							</li>							
							</ul>
							</div>
							</c:if>	
							
							<c:if test="${!empty socialaccvalue}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/social_account_icon.png" width="25" height="26" />Social Account </span>
							</h2>
							<ul>
							<li>
							<img src="resources/kyc_images/face_book_icon.png" width="17" height="17" /><div id="websitelinkname">
							<%if(socialVisibility.charAt(0)=='1') {%>
							
									<c:if test="${!empty socialaccvalue.socialwebsitelink}">
												<a href="${socialaccvalue.socialwebsitelink}" target="_blank">
													<c:out value="facebook connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.socialwebsitelink}">									
										<c:out value="facebook not connected"></c:out>						
									</c:if>	
									
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							<li>
							<img src="resources/kyc_images/twitter_icon.png" width="17" height="17" /><div id="twitterlinkname">
							<%if(socialVisibility.charAt(1)=='1') {%>
									
									<c:if test="${!empty socialaccvalue.sociallinkedlnlink}">
												<a href="${socialaccvalue.sociallinkedlnlink}" target="_blank">
													<c:out value="twitter connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.sociallinkedlnlink}">									
										<c:out value="twitter not connected"></c:out>						
									</c:if>	
								
								
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							<li>
							<img src="resources/kyc_images/linkedin_icon.png" width="17" height="17" /><div id="linkedlnlinkname">
							<%if(socialVisibility.charAt(2)=='1') {%>
							
							        <c:if test="${!empty socialaccvalue.socialtwitterlink}">
												<a href="${socialaccvalue.socialtwitterlink}" target="_blank">
													<c:out value="linkedin connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.socialtwitterlink}">									
										<c:out value="linkedin not connected"></c:out>						
									</c:if>	
																		
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							
							</ul>
							</div>
							</c:if>													
												                
					</div>
					
					 </div>
					
					<div class="rightside">
					
					 <input type="hidden" id="redirectUrl" value="<%=(String)session.getAttribute("reloadUrl")%>" />

                     <input type="hidden" id="indId" value="<%=session.getAttribute("tempIdasdgthfmUti")%>" />
	                 
	                 
					<!-- Advertisment Div for Entire Page  -->
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
						   %>
                                  <div id="ProfileHeadingDiv" class="utiProfileHeadingDiv" style="display:block;">
									
										<marquee behavior="scroll" scrollamount="2" direction="left">
											   <h1 class="rightsideheader">
											       
											        <%=utiAdvertise%>
											        
											   </h1>
									     </marquee>
							        </div>	
							       
							        		
					   <!-- Advertisment Div for Entire Page  -->
					   
					   
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
							       <b></b>
								   <span><c:out value="${regname.firstname}'s Kyc Documents"></c:out></span>
								   <c:set var="member_date"  value="${regname.cr_date}"/>
		                              <%
		                              	Date member_date = (Date) pageContext.getAttribute("member_date");
		                              	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		                              	String reportDate = df.format(member_date);
		                              	String dateAry1[] = reportDate.split(" ");
		                              	String dateAry2[] = dateAry1[0].split("-");
		                              	int month = Integer.parseInt(dateAry2[1]);
		                              	String year = dateAry2[0];
		                              %>							
								    <p>Member since <%=months[month - 1] + " " + year%></p>
							</h2>
						 </div>
						 
					
<div class="module-body">
	
	
	<!-- side tab button div -->
					
							<div class="sidetabdiv">
							<ul>
							
							    <li class="tab-defaultbtn">
								     <input class="tab-defaultinput" id="ProfileActive" type="button" value="Profile" onclick="showProfileAgain()"/>							
								</li>
							
							<%if(otherAccess.contains("KYC:")){ %>
							
								<li class="tab-defaultbtn">
								<input class="tab-defaultinputActive" id="kycActive" type="button" value="KYC" onclick="showKycDocuments()"/>							
								</li>
								
						    <%} %>
						    
						    
						    <%if(otherAccess.contains("ACADEMIC:")){ %>
						    
								<li class="tab-defaultbtn">
								<input class="tab-defaultinput" id="academicActive" type="button" value="Academic" onclick="showAcademicDocuments()">							
								</li>
								
							<%} %>
							
							<%if(otherAccess.contains("EMPLOYEMENT:")){ %>
							
								<li class="tab-defaultbtn">
								<input class="tab-defaultinput" id="empActive" type="button" value="Employment" onclick="showEmpDocuments()">							
								</li>
								
							<%}else if(otherAccess.contains("Employment:")) {%>
							
								<li class="tab-defaultbtn">
								<input class="tab-defaultinput" id="empActive" type="button" value="Employment" onclick="showEmpDocuments()">							
								</li>
								
							<%} %>
							
							<%if(otherAccess.contains("FINANCIAL:")){ %>
							
								<li class="tab-defaultbtn">
								<input class="tab-defaultinput" id="financialActive" type="button" value="Financial" onclick="showFinancialDocuments()">							
								</li>
								
							<%}else if(otherAccess.contains("Financial:")) {%>	
													
								<li class="tab-defaultbtn">
								<input class="tab-defaultinput" id="financialActive" type="button" value="Financial" onclick="showFinancialDocuments()">							
								</li>
								
							<%} %>
							
							</ul>
							</div>		
							
							
	<!-- side tab button div END-->	
	
						
	<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
											
				  <div id="center-body-div">
	  						
							<div class="centerbodydiv">
									    
									    <%String zipStatus = (String)session.getAttribute("allDocumentNames");  %>
										 
										<%if(zipStatus.contains("_")){ %> 
										   
									        <div class="showcodepage">
										        <ul>
										            <li>
										                <table>
										                <tr>
										                <td>
											            <img src="resources/kyc_images/kyczipicon.jpg" height="50px" width="50px" /></td>
											            
											            
												        <td>
												       
												        <form  id="downloadAllFormId" action="downlaodalldocumentsinzipForUtilizer.html" method="post">
												    
												     
												            <input type="hidden" value="<%=(String)session.getAttribute("allDocumentNames")%>" name="exttemp" /> 
												            <input type="hidden" value="<%=session.getAttribute("tempIdasdgthfmUti")%>" name="extp" /> 
												            <input type="hidden" value="KycDocuments" name="codeName" />
												      
												            <input type="submit" value="Click here to download latest Kyc documents in zip." />
												            
												         </form>
												         
												            </td>
												            </tr>
												            </table>
												        
												        
											        </li>
											    </ul>
											</div> 
									       
									        <%}else{ %>
										  
										         <div class="makeinHide">
										         
													  <div class="showcodepage">
													        <ul>
													            <li>
													                <table>
														                <tr>
															                <td>
																            <img src="resources/kyc_images/kyczipicon.jpg" height="50px" width="50px" /></td>
																            
																            
																	           <td>
																	        
																	            <input type="submit" style="cursor: not-allowed;" value="Click here to download all latest Academic documents in zip." />
																	          
																	            </td>
																          </tr>
															       </table>
															        
															        
														        </li>
														    </ul>
														</div>
										  
										         </div>
										  <%} %>
									       
									       
									       
									       
									       
									       
									        
									          <c:set var="comp" value="${accordNames}"/>  
									          <c:set var="kycdatas" value="${kycdatas}"/>     
									                     
									         <% 
									         String comp = (String)pageContext.getAttribute("comp");                        
									         String component[] = comp.split(",");        
									         String kycdatas =(String)pageContext.getAttribute("kycdatas");          
									         StringBuilder strKYC= new StringBuilder();
									         String strKYCDATA="";
									         
									         if(kycdatas.contains("/"))
									         {
									        	 String kycFieldNames[] = kycdatas.split("/");
									        	 for(int d=0;d<kycFieldNames.length;d++)
									        	 {
									        		 strKYC.append(kycFieldNames[d]+"%"); 
									        	 }
									        	 strKYCDATA=strKYC.toString();									        	
									         }
									              
									         %>              
									              
										 
										 <%  
										     String pannelWithId = (String)request.getAttribute("pannelWithId");
										     String pannelWithIdArray[] = pannelWithId.split(",");		  
										  %>  
										  	  	          	          	          	                                          
									      <div  id="panel-1">  	         	          	
									           <%                                          
									           for(int l=0,q=0;l<component.length;l++,q++)        	   
									        	  { 
									        	  
									        	     String kycId = "";
									        	  %>	
									        	  	
									        	  	<%for(int k=0;k<pannelWithIdArray.length;k++) {
									        	  	      
									        	  		if(pannelWithIdArray[k].contains(component[l]))
									        	  		{
									        	  			String splitWithId[] = pannelWithIdArray[k].split("-");
									        	  			kycId = splitWithId[1];
									        	  		}
									        	  	} %>
									        
									        <%if(!kycId.equals("")) {%>	  
									        
									        <h3 align="left" onclick="visibleAccordian('<%=component[l].replaceAll(" ", "")%>Accordian')"><%=component[l]%> : <b><%=kycId%></b></h3>  
									        
									        <%}else{ %>
									        
									        <h3 align="left" onclick="visibleAccordian('<%=component[l].replaceAll(" ", "")%>Accordian')"><%=component[l]%></h3> 
									        
									        <%} %>						             
									        
									      <div height="50px" id="<%=component[l].replaceAll(" ", "")%>Accordian" style="display:none;">
									      
									               <%
									               String filename ="";
									               String docname="";
									               String filenameback="";
									               String date="";
									               String des="";
									               int k = 0;
									               
									               if(strKYCDATA.contains("%"))
									               {
									            	   String kycarray [] = strKYCDATA.split("%");
									            	   for(int z=0;z<kycarray.length;z++)
									            	   {            		               
									            		   if(kycarray[z].contains(component[l]))
									            		   {           			   
									            			   String str = kycarray[z].substring(kycarray[z].lastIndexOf("$")+1, kycarray[z].length());
									            			  
									            			   String strkycarray[] = str.split(",");
									            			   filename=strkycarray[0];filenameback=strkycarray[1];docname=strkycarray[2];date=strkycarray[3];des=strkycarray[4];
									            		   }
									            	   }
									               }
									                                   
									                 %>
									                 
									           
									      <div id="kycdocumentpage" class="kycdocumentpan">   
									          
									        
									          <table border="0"  cellpadding="1" cellspacing="6" align="center">               
									                <tr>                                                                                      
									                    
									                      <%if(docname.isEmpty()){%>
									                    
									                             <td height="150" width="300" colspan="3" >
									                                
									                                		<div class="alert-box notice" style="width:250px;margin-left: 240px;">
																				
																				 Documents are not uploaded yet.
																				 
																			</div>
									                                									                               
									                             </td>
									                             
									                      <%}else{ %>
									                      
										                          <% if(!filename.contains("_"))
										                        	  {%>
												                            <td height="150" width="300" colspan="3" >
												                             <div  class="panviewinner">
												                             												                             
												                                		<img src="resources/images/noDocumentFound.png"   width="250" height="150" /></img>									                    									                              
												                                
												                                        <h1>Front View</h1>
												                                        </div>
												                             </td>
										                             
										                             <%}else{ %>
										                             
										                                 <%if(filename.contains("pdf")) {%>
									                                      
										                                        <td height="150" width="300" colspan="3" >
													                             	 <div  class="panviewinner">										                             														                             
												                                             <img src="resources/images/PNA_PDF.png"  width="250" height="150" /></img>
												                                       												                                           
												                                            <a title="Download Document" href="downloadDocfromUtilizer.html?fileName=<%=filename%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"><button class="mydocumentdownloadpan"></button></a>
												                                       
												                                         <h1>Front View</h1>
												                                           </div>
													                             </td>
													                             
									                                     <%}else{%>
				                                     
																	        <!-- Front Side -->	
																	         <td height="150" width="300" colspan="3" >
												                                                               	
													                                     <div  class="panviewinner">  
																		                       
																		                         <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+filename+"&docCategory=KYC_DOCUMENT&extp="+session.getAttribute("tempIdasdgthfmUti")%>" width="250" height="150" /></img>											                                                                   
																		                 
																		                    																	                 
																		                         <a title="View Document" onclick="viewKycDocumnt('<%=filename%>','<%=date%>','<%=des%>')"><button class="mydocumentviewpan"></button></a>
																		                         <a title="Download Document" href="downloadDocfromUtilizer.html?fileName=<%=filename%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"><button class="mydocumentdownloadpan"></button></a>
																                      
																                             
																                  		    <h1>Front View</h1>
																                  </div>
												                             </td>
												                             
										                    	          <%}%>
										                             
										                             <%} %>
										                             
										                             
										                             
										                             
										                             
										                             <%if(!filenameback.contains("_"))
										                        	  {%>
												                            <td height="150" width="300" colspan="3" >
												                              <div  class="panviewinner">
												                                 		
												                                		<img src="resources/images/noDocumentFound.png"  width="250" height="150" /></img>									                    									                              
												                                
												                                		<h1>Back View</h1>
												                                		</div>
												                             </td>
										                             
										                             <%}else{ %>
										                             
										                                 <%if(filenameback.contains("pdf")) {%>
									                                      
										                                        <td height="150" width="300" colspan="3" >
													                              <div  class="panviewinner">
													                                 
													                                
												                                        <img src="resources/images/PNA_PDF.png"   width="250" height="150" /></img>
												                                    
												                                    
												                                              <a title="Download Document" href="downloadDocfromUtilizer.html?fileName=<%=filenameback%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"><button class="mydocumentdownloadpan"></button></a>
												                                     
												                                        
												                                  		<h1>Back View</h1>
												                                  		</div>
												                                   
													                             </td>
													                             
									                                     <%}else{%>
				                                     
																	        <!-- Back Side -->	
																	         <td height="150" width="300" colspan="3" >
												                             				
												                             		 <div  class="panviewinner">										                                                                   													                                        								                    
																		                       
																		                        <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+filenameback+"&docCategory=KYC_DOCUMENT&extp="+session.getAttribute("tempIdasdgthfmUti")%>" width="250" height="150" /></img>											                                                                   
																		                 
																				                    <a  title="View Document" onclick="viewKycDocumnt('<%=filenameback%>','<%=date%>','<%=des%>')"><button  class="mydocumentviewpan"></button></a>
																				                    <a title="Download Document" href="downloadDocfromUtilizer.html?fileName=<%=filenameback%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"><button  class="mydocumentdownloadpan"></button></a>
																		                      
																	                       
																                               <h1>Back View</h1>
																                               </div>
																                  
												                             </td>
												                             
										                    	          <%}%>
										                             
										                             <%} %>
									                               
									                      <%} %>
									                      
									                      
									                      
									                      
									                     <td height="150" width="300" colspan="3" >
									                
									                             <%				                   
												                    if(date.equals("Captured in Registration")){ %>
												                    												                   
												                          
												                   
												                   <%} else if(date.equals("")){ %>
												                   
												                        
												                   
												                    <%}else{ %>
												                    
												                    
												                          <ul class="panviewinner_button">                                        
												                                                                      
												                         <li>
												                         <input class="historyinner_button" type="button" style="margin-right: 57px;width: 90px !important;" value="History"  onclick ="kycHistory('<%=component[l]%>')" />
												                        
												                         </li>
												                       </ul>      
												                    
												                    
												              <%} %>   
											                       
								                   	            
									</td>															
													
														                                              
												                
											                                
									               </tr>               
									            
									            </table>
									        
									          </div>
									      
									       </div>
									      
									    <% } %>
									    
									  </div>
									 
								  </div>
                             
                             </div>
                             
                             
					
					<!-- End of Required Division for viewUser1 not go beyond  -->	
						
			</div>
		</div>	
	</div>	<!-- End of -->	

	<%-- <div id="center-body-div" style="display:block   margin: 0 auto; width: 100%;">
<div  id="panel-1" >

 <h3 align="left"><b>Registration Details</b></h3> 
         
<div  style="display:block" id="indexinputbutton">
              
        <c:if test="${!empty regHisname}">  
      
       <form id="showRegDetails"></form>
                                
       <form id="editregistration"  name="regform" >
       
       <div class="displayregidetails">
										
											<ul>
												<li class="form-row" style=" float: left;height: auto;width: 100%;">
																																	
												<input class="firstname" style="width: 102px;float: left;margin-right: 6px;" type="text"
														value="${regHisname.first_name}" name="firstname"
														id="reg1" disabled="true"
														
														onchange="myFunctionreg(this.id)" placeholder="First Name" onkeyup="validateRegDetailsEach()"/>
														
														
															<input class="middlename"style="width: 113px;float: left;margin-right: 6px;" type="text"
														value="${regHisname.middle_name}" name="middlename" placeholder="Middle Name"
														id="reg0" disabled="true"
														
														onchange="myFunctionreg(this.id)"  onkeyup="validateRegDetailsEach()"/>
													<input class="lasstname"style="width: 113px;float: left;margin-right: 6px;" type="text"
														value="${regHisname.last_name}" name="lastname" id="reg3" placeholder="Lastname"
														disabled="true" 
														onchange="myFunctionreg(this.id)"  onkeyup="validateRegDetailsEach()"/>
											
												
												</li>
												<li class="form-row" >
												<%if(regVisibility.charAt(0)=='1') {%>
												<input class="mailidicons" style="clear: both;
                                                  float: left;
                                                  margin-top: 5px;
                                                  position: relative;
                                                  text-align: left;
                                                  width: 375px;" type="text"
												value="${regHisname.email_id}" name="emailid" id="reg4" placeholder="Email-id" disabled="true" />
														
											    <%}else { %>
											    
											    			<%if(profileAccess.contains("REGISTRATION-Email Id")) {%>
									
									   							<input class="mailidicons" style="clear: both;
				                                                  float: left;
				                                                  margin-top: 5px;
				                                                  position: relative;
				                                                  text-align: left;
				                                                  width: 375px;" type="text"
																  value="${regHisname.email_id}" name="emailid" id="reg4" placeholder="Email-id" disabled="true" />
																	
									
															<%}else{ %>
									
									   							 <input class="mailidicons" style="clear: both;
				                                                  float: left;
				                                                  margin-top: 5px;
				                                                  position: relative;
				                                                  text-align: left;
				                                                  width: 375px;" type="text"
																  value="Email Id Restricted" name="emailid" id="reg4" placeholder="Email-id" disabled="true" />
																	
									
															 <%} %>
															   
											    <%} %>
												</li>

												<li class="form-row">
												
												<%if(regVisibility.charAt(1)=='1') {%>	
													<input class="mobileicons"style=" clear: both;
														    float: left;
														    margin-top: 5px;
														    position: relative;
														    text-align: left;
														    width: 375px;" type="text" type="text"
														value="${regHisname.mobile_no}" name="mobileno" id="reg5" placeholder="Mobile No"
														disabled="true"	/>
												<%} else { %>
												
															<%if(profileAccess.contains("REGISTRATION-Mobile No")) {%>
									
											                       <input class="mobileicons"style=" clear: both;
																    float: left;
																    margin-top: 5px;
																    position: relative;
																    text-align: left;
																    width: 375px;" type="text" type="text"
																    value="${regHisname.mobile_no}" name="mobileno" id="reg5" placeholder="Mobile No"
																    disabled="true"	/>
																
									 						<%}else{ %>
									 						
																	 <input class="mobileicons"style=" clear: both;
																			    float: left;
																			    margin-top: 5px;
																			    position: relative;
																			    text-align: left;
																			    width: 375px;" type="text" type="text"
																			value="Mobile.no Restricted" name="mobileno" id="reg5" placeholder="Mobile No"
																			disabled="true"	/>
																	
															<%} %>
																																		
												<%} %>
												
												</li>
												
												<c:set var="indgender"  value="${regHisname.gender}"/>
												 <%
										            String indgender = (String)pageContext.getAttribute("indgender");										          
										         %>
         
          										<%if(indgender.equals("MALE")) {%>
          										
			          										<li class="form-row" >
																												
																<input class="gendericons" disabled="true" style=" clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;" type="text" type="text"
																	value="${regHisname.gender}" id="genderId" name="gender"  placeholder="Gender"/>												
															</li>
															
														   <li class="form-row" style=" clear: both;">
																										
															<div class="register-switch">
														      <input type="radio" name="sex" value="MALE" id="sex_m" class="register-switch-input" disabled="true" onclick="genderValue()">
														      <label for="sex_m" class="register-switch-malelabel">&nbsp;</label>														      
														    </div>
														   
														  </li>	
          										
          										
          										<%}else {%>
          										
			          										<li class="form-row" >
																												
																<input class="gendericons" disabled="true" style=" clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;" type="text" type="text"
																	value="${regHisname.gender}" id="genderId" name="gender"  placeholder="Gender"/>												
															</li>
															
														    <li class="form-row" style=" clear: both;">
																										
																<div class="register-switch">													      
															      <input type="radio" name="sex" value="FEMALE" id="sex_f" class="register-switch-input" disabled="true" onclick="genderValue()">
															      <label for="sex_f" class="register-switch-femalelabel">&nbsp;</label>														      
															    </div>
														   
														    </li>	
          										
          										      
          										<%} %>
													
  										              <%if(profileAccess.contains("REGISTRATION")) {%>
          										        
          										          <li id="indexinputbutton"class="form-row" style="clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 445px;" >
																	
																<input type="button" value="History" style="width:100px" onclick="reghistory('<%=(Integer)request.getAttribute("searched_Indid")%>')" class="historybutton" />
																																																
														  </li>
														  
													<%} %>
 </ul>	
									
 </div>
	</form> 
        </c:if> 
     
 </div>
  
  <c:if test="${empty basicdetails}">           
   
  <h3 align="left"><b>Basic Details</b></h3>
      
   <div id="indexinputbutton">
      
   <form name="savebasic" id="savebasic"  enctype="multipart/form-data">
        
   		<div id ="familydetailalign">
		 <ul>
		 <li>
		
		 <input class="mailidicons"   id="street" style="width:160px;float: left;margin-right: 6px;"  type="text" value="Not Available" name="Emailalternative" title="Alternativ Email Id" disabled="true" />		 		
		 <input class="dateofbirthicons" style="width:160px;float: left;margin-right: 6px;" value="Not Available" title="Date Of Birth"  type="text"  name="DOB"  id="b8" disabled="true" />		
		
		 </li>
		  
		  <li>
		  
		  <input  class="telofficeicons"  placeholder="Tel-Office" style="width:160px;float: left;margin-right: 6px;" value="Not Available" name="Teloff" id="b4" title="Office Tele-Phone No." disabled="true" />		  		  
		  <input class="telresidentialicons" value="Not Available" placeholder="Tel-Residence" style="width:160px;float: left;margin-right: 6px;" value="Not Available" title="Rsidence Tele-Phone No" name="Telres" id="b5"  disabled="true" />
		 
		  </li>
		  
		   <li>
		  
		   <input  class="nationalityicons"  placeholder="Nationality" style="width:160px;float: left;margin-right: 6px;" value="Not Available" name="Nationality" title="Nationality" id="b3" disabled="true" />		   		   
		   <input  class="matrialstatusicons"  placeholder="Marital Status" style="width:160px;float: left;margin-right: 6px;" value="Not Available" name="Matrialstatus" title="Marital Status" id="b2" disabled="true" />
		   
		   </li>
		   
		    <li>
		    	    
		    <input class="languagesicons"  placeholder="Language Known" style="width:160px;float: left;margin-right: 6px;" value="Not Available" name="Languages" title="Language known" id="b6" disabled="true" />		    		    		   
		    <input class="hobbiesicons"  placeholder="Hobbies" style="width:160px;float: left;margin-right: 6px;" value="Not Available" title="Hobbies" name="Hobbies" id="b7" disabled="true" />
		    
		    </li>
		    
		     <li></li>
		 </ul>
		 </div>
					
			<div id="permanentaddressdetails">
			<h1>
			<img src="resources/kyc_images/permanentaddress_iconsone.png" />
			<span>Permanent Address</span>
			</h1>
			<ul>
			<li>
			
			<input id="perdoor" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available"  placeholder="Door" title="Door Number" type="text"/>
			
			<input id="permandal" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Street" title="Street Name" type="text" />
									
			<input id="perzone" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Area" title="Area Name" type="text" />
			
			</li>
			
			<li>
			
			<input id="perlandmark" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Landmark" title="Near By Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
			
			<input id="perpincode" disabled="true" name="pincode" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Pincode" title="Pincode" type="text" maxlength="6"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="percity" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
						
			</li>
			
			<li>
			
			<input id="perdistrict" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Avilable" placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perstate" disabled="true" name="state" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			
			<input id="percountry" disabled="true" name="country" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Country" title="Country Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			</li>
					
			</ul>	
			 						
			</div>	
						
  </form>
 </div>
 
 </c:if>
  
  
  <c:if test="${!empty basicdetails}">           
   
  <h3 align="left"><b>Basic Details</b></h3>
      
   <div id="indexinputbutton">
    
   <form id="showBasicHistory" ></form>
      
   <form name="savebasic" id="savebasic"  enctype="multipart/form-data">
        
   		<div id ="familydetailalign">
		 <ul>
		 <li>
		 <%if(basicVisibility.charAt(0)=='1') {%>
		 <input class="mailidicons"   id="street" style="width:160px;float: left;margin-right: 6px;"  type="text" value="${basicdetails.emailalternative}" name="Emailalternative" disabled="true" />
		 <%}if(basicVisibility.charAt(0)=='0') {%>
		 
		      <%if(profileAccess.contains("BASIC-Alternate Email Id")) {%>
			  
			         <input class="mailidicons"   id="street" style="width:160px;float: left;margin-right: 6px;"  type="text" value="${basicdetails.emailalternative}" name="Emailalternative" disabled="true" />
			   
			  <%}else{ %>					
					
					 <input class="mailidicons"   id="street" style="width:160px;float: left;margin-right: 6px;"  type="text" value="Alternativ emailId Restricted" name="Emailalternative" disabled="true" />
		       
		       <%} %>
		       
		 <%} %>
		 
		 <%if(basicVisibility.charAt(1)=='1') {%>
		   <input class="dateofbirthicons" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.DOB}"  type="text"  name="DOB"  id="b8" disabled="true" />
		 <%} if(basicVisibility.charAt(1)=='0') { %>
		 
		       <%if(profileAccess.contains("BASIC-Date of Birth")) {%>
			  
			         <input class="dateofbirthicons" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.DOB}"  type="text"  name="DOB"  id="b8" disabled="true" />
			  
			  <%}else{ %>					
					
					  <input class="dateofbirthicons" style="width:160px;float: left;margin-right: 6px;" value="DOB is Retricted"  type="text"  name="DOB"  id="b8" disabled="true" />
		
		       <%} %>
		       
		 <%} %>
		 </li>
		  <li>
		  <%if(basicVisibility.charAt(2)=='1') {%>
		  <input  class="telofficeicons"  placeholder="Tel-Office" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.teloff}" name="Teloff" id="b4" disabled="true" />
		  <%}if(basicVisibility.charAt(2)=='0') { %>
		  
			      <%if(profileAccess.contains("BASIC-Office Contact no")) {%>
				  
				         <input  class="telofficeicons"  placeholder="Tel-Office" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.teloff}" name="Teloff" id="b4" disabled="true" />
		  
				  <%}else{ %>					
						
						 <input  class="telofficeicons"  placeholder="Tel-Office" style="width:160px;float: left;margin-right: 6px;" value="Office No Restricted" name="Teloff" id="b4" disabled="true" />
		 
			       <%} %>
		  			
		  <%} %>
		  <%if(basicVisibility.charAt(3)=='1') {%>
		  <input class="telresidentialicons" value="" placeholder="Tel-Residence" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.telres}" name="Telres" id="b5"  disabled="true" />
		  <%} if(basicVisibility.charAt(3)=='0') {%>
		  
		           <%if(profileAccess.contains("BASIC-Residence Contact no")) {%>
				  
				         <input class="telresidentialicons"  placeholder="Tel-Residence" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.telres}" name="Telres" id="b5"  disabled="true" />
		  
				  <%}else{ %>					
						
						 <input class="telresidentialicons"  placeholder="Tel-Residence" style="width:160px;float: left;margin-right: 6px;" value="Residence No Restricted" name="Telres" id="b5"  disabled="true" />
		  
			       <%} %>
		  
		   <%} %>
		  </li>
		   <li>
		   <%if(basicVisibility.charAt(4)=='1') {%>
		   <input  class="nationalityicons"  placeholder="Nationality" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.nationality}" name="Nationality" id="b3" disabled="true" />
		   <%}if(basicVisibility.charAt(4)=='0') { %>
		   <input  class="nationalityicons"  placeholder="Nationality" style="width:160px;float: left;margin-right: 6px;" value="Nationality Restricted" name="Nationality" id="b3" disabled="true" />
		   <%} %>
		   <%if(basicVisibility.charAt(5)=='1') {%>
		   <input  class="matrialstatusicons"  placeholder="Marital Status" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.matrialstatus}" name="Matrialstatus" id="b2" disabled="true" />
		   <%}if(basicVisibility.charAt(5)=='0') { %>
		            
		           <%if(profileAccess.contains("BASIC-Marital Status")) {%>
				  
				         <input  class="matrialstatusicons"  placeholder="Marital Status" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.matrialstatus}" name="Matrialstatus" id="b2" disabled="true" />
		   
				  <%}else{ %>					
												  
		                 <input  class="matrialstatusicons"  placeholder="Marital Status" style="width:160px;float: left;margin-right: 6px;" value="Marital Status Restricted" name="Matrialstatus" id="b2" disabled="true" />
		   
			       <%} %>
		            
		   <%} %>
		   </li>
		    <li>
		    <%if(basicVisibility.charAt(6)=='1') {%>
		    <input class="languagesicons"  placeholder="Language Known" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.languages}" name="Languages" id="b6" disabled="true" />
		    <%}if(basicVisibility.charAt(6)=='0') {%>
		                <input class="languagesicons"  placeholder="Language Known" style="width:160px;float: left;margin-right: 6px;" value="Language is Restricted" name="Languages" id="b6" disabled="true" />
		    <%} %>
		    
		    <%if(basicVisibility.charAt(7)=='1') {%>
		    <input class="hobbiesicons"  placeholder="Hobbies" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.hobbies}" name="Hobbies" id="b7" disabled="true" />
		    <%} if(basicVisibility.charAt(7)=='0') { %> 
		    <input class="hobbiesicons"  placeholder="Hobbies" style="width:160px;float: left;margin-right: 6px;" value="Hobbies Restricted" name="Hobbies" id="b7" disabled="true" />
		    <%} %>
		    </li>
		     <li></li>
		 </ul>
		 </div>
			
			<%if(basicVisibility.charAt(9)=='1') {%>
			
			<div id="permanentaddressdetails">
			<h1>
			<img src="resources/kyc_images/permanentaddress_iconsone.png" />
			<span>Permanent Address</span>
			</h1>
			<ul>
			<li>
			
			<input id="perdoor" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[0];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Door" title="Door Number" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="permandal" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
           String[] ary1=myVariable1.split("##");
           String str = ary1[3];
           
         %>
        value="<%=str%>" </c:if> placeholder="Street" title="Street Name" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perzone" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
           String[] ary1=myVariable1.split("##");
           String str = ary1[2];
           
         %>
        value="<%=str%>" </c:if> placeholder="Area" title="Area Name" type="text"   onchange='ReviewDefectsper(length);' />
			</li>
			
			<li>
			
			<input id="perlandmark" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}" />
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[1];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Landmark" title="Near By Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
			
			<input id="perpincode" disabled="true" name="pincode" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[7];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Pincode" title="Pincode" type="text" maxlength="6"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="percity" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[4];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
						
			</li>
			
			<li>
			
			<input id="perdistrict" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[5];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perstate" disabled="true" name="state" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           System.out.println("myVariable1"+myVariable1);
           String[] ary1=myVariable1.split("##");
           String str = ary1[6];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			
			<input id="percountry" disabled="true" name="country" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
          String[] ary1=myVariable1.split("##");
          String str = ary1[8];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Country" title="Country Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			</li>
			
			<%if(profileAccess.contains("BASIC")){ %>	
				
				<li>	
					<input style="width:100px" type="button" value="History" class="historybutton" onclick="basicHistory('<%=(Integer)request.getAttribute("searched_Indid")%>')" />
					
				</li>	
				
			<%} %>
			
			</ul>	
			 						
			</div>	
			
			<%} else{%>
			
			     <%if(profileAccess.contains("BASIC-Permanent Address")) {%>
				  
						 <div id="permanentaddressdetails">
						 
									<h1>
									<img src="resources/kyc_images/permanentaddress_iconsone.png" />
									<span>Permanent Address</span>
									</h1>
									<ul>
									<li>
									
									<input id="perdoor" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						           
						           String[] ary1=myVariable1.split("##");
						           String str = ary1[0];
						           //str = str.substring(0,str.length()-1);
						         %>
						        value="<%=str%>" </c:if> placeholder="Door" title="Door Number" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
									
									<input id="permandal" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						          
						           String[] ary1=myVariable1.split("##");
						           String str = ary1[3];
						           
						         %>
						        value="<%=str%>" </c:if> placeholder="Street" title="Street Name" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
									
												
									<input id="perzone" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						          
						           String[] ary1=myVariable1.split("##");
						           String str = ary1[2];
						           
						         %>
						        value="<%=str%>" </c:if> placeholder="Area" title="Area Name" type="text"   onchange='ReviewDefectsper(length);' />
									</li>
									
									<li>
									
									<input id="perlandmark" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}" />
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						           
						           String[] ary1=myVariable1.split("##");
						           String str = ary1[1];
						           //str = str.substring(0,str.length()-1);
						         %>
						        value="<%=str%>" </c:if> placeholder="Landmark" title="Near By Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
									
									<input id="perpincode" disabled="true" name="pincode" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						           
						           String[] ary1=myVariable1.split("##");
						           String str = ary1[7];
						           //str = str.substring(0,str.length()-1);
						         %>
						        value="<%=str%>" </c:if> placeholder="Pincode" title="Pincode" type="text" maxlength="6"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
									
									<input id="percity" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						           
						           String[] ary1=myVariable1.split("##");
						           String str = ary1[4];
						           //str = str.substring(0,str.length()-1);
						         %>
						        value="<%=str%>" </c:if> placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
												
									</li>
									
									<li>
									
									<input id="perdistrict" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						           
						           String[] ary1=myVariable1.split("##");
						           String str = ary1[5];
						           //str = str.substring(0,str.length()-1);
						         %>
						        value="<%=str%>" </c:if> placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
									
												
									<input id="perstate" disabled="true" name="state" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						           System.out.println("myVariable1"+myVariable1);
						           String[] ary1=myVariable1.split("##");
						           String str = ary1[6];
						           //str = str.substring(0,str.length()-1);
						         %>
						        value="<%=str%>" </c:if> placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
									
									
									<input id="percountry" disabled="true" name="country" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
						        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
								 <%
						         String myVariable1 = (String)pageContext.getAttribute("divide1");
						          
						          String[] ary1=myVariable1.split("##");
						          String str = ary1[8];
						           //str = str.substring(0,str.length()-1);
						         %>
						        value="<%=str%>" </c:if> placeholder="Country" title="Country Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
									</li>
										
											<%if(profileAccess.contains("BASIC")){ %>	
					
												<li>	
													<input style="width:100px" type="button" value="History" class="historybutton" onclick="basicHistory('<%=(Integer)request.getAttribute("searched_Indid")%>')" />
													
												</li>	
												
											<%} %>		
									</ul>	
									 						
									</div>	
				  <%}else{ %>					
												  
		                        <div id="permanentaddressdetails">
								<h1>
								<img src="resources/kyc_images/permanentaddress_iconsone.png" />
								<span>Permanent Address</span>
								</h1>
								<ul>
								<li>
								
								<input id="perdoor" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Door" title="Door Number" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
								
								<input id="permandal" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility"placeholder="Street" title="Street Name" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
								
											
								<input id="perzone" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Area" title="Area Name" type="text"   onchange='ReviewDefectsper(length);' />
								</li>
								
								<li>
								
								<input id="perlandmark" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Landmark" title="Near By Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
								
								<input id="perpincode" disabled="true" name="pincode" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Pincode" title="Pincode" type="text" maxlength="6"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
								
								<input id="percity" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
											
								</li>
								
								<li>
								
								<input id="perdistrict" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
														
								<input id="perstate" disabled="true" name="state" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
											
								<input id="percountry" disabled="true" name="country" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Country" title="Country Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
								</li>
									
									<%if(profileAccess.contains("BASIC")){ %>	
					
												<li>	
													<input style="width:100px" type="button" value="History" class="historybutton" onclick="basicHistory('<%=(Integer)request.getAttribute("searched_Indid")%>')" />
													
												</li>	
												
									<%} %>		
										
								</ul>	
								 						
								</div>	
			       <%} %>
			       
			
			<%} %>      
  </form>
 </div>
 
 </c:if>
 
  <c:if test="${!empty familyvalue}">  
  <h3 align="left"><b>Family Details</b></h3> 
  <div id="indexinputbutton">
  
  <div id ="familydetailalign">
  
  <% if(!planName.equals("Basic")) { %>

     <form id="showfamilydetail"></form>
     
     <form id="editfamilydetail" name="familyform">
     
		 <ul>
		
				<li class="form-row">
				     
				    <%if(familyVisibility.charAt(0)=='1') {%>
				    <input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name" value="${familyvalue.fathername}" name="Fathername" id="fname" disabled="true" />
				    <%}else{ %>
				    
				            <%if(profileAccess.contains("FAMILY-Father Name")) {%>
				             
				                 <input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name" value="${familyvalue.fathername}" name="Fathername" id="fname" disabled="true" />
				    
				            <%}else { %>
				            
				                 <input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name" value="Restricted as per Visibility" name="Fathername" id="fname" disabled="true" />
				   
				            <%} %> 
				             
				    <%} %>
				
				<%if(familyVisibility.charAt(1)=='1') {%>
			       <input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" type="text" value="${familyvalue.mothername}" name="Mothername" id="mname" disabled="true" />
				   <%}else{ %>
				             
				             <%if(profileAccess.contains("FAMILY-Mother Name")) {%>
				             
				                 <input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" type="text" value="${familyvalue.mothername}" name="Mothername" id="mname" disabled="true" />
				   
				            <%}else { %>
				            
				                   <input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" type="text" value="Restricted as per Visibility" name="Mothername" id="mname" disabled="true" />
				 
				            <%} %> 
				             
				  <%} %>
				   
				</li>
			
				<li class="form-row">
				    
				    <%if(familyVisibility.charAt(2)=='1') {%>
					<input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name"type="text"  name="Brothername" value="${familyvalue.brothername}" id="bname" disabled="true" />
					<%}else{ %>
					         
					        <%if(profileAccess.contains("FAMILY-Sibling Name")) {%>
				             
				                 <input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name"type="text"  name="Brothername" value="${familyvalue.brothername}" id="bname" disabled="true" />
					
				            <%}else { %>
				            
				                 <input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name"type="text"  name="Brothername" value="Restricted as per Visibility" id="bname" disabled="true" />
				            
				            <%} %> 
					         
					         
					<%} %>
					
					<%if(familyVisibility.charAt(3)=='1') {%>
					<input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name"  type="text" value="${familyvalue.spousename}" name="Spousename" id="sname" disabled="true" />
					<%}else { %>
					          
					        <%if(profileAccess.contains("FAMILY-Spouse Name")) {%>
				             
				                <input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name"  type="text" value="${familyvalue.spousename}" name="Spousename" id="sname" disabled="true" />
					
				            <%}else { %>
				            
				                <input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name"  type="text" value="Restricted as per Visibility" name="Spousename" id="sname" disabled="true" />
				           
				            <%} %>  
					          
					          
					<%} %>
										    
				</li>
				
				    
			    <li class="form-row">
			        
			        <%if(familyVisibility.charAt(4)=='1') {%>
			    	<input class="horoscopeicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Horoscopee"  type="text"  name="HoroscopeInformation" value="${familyvalue.horoscopeInformation}" id="hinfo" disabled="true" />
			    	<%}else {%>
			    	<input class="horoscopeicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Horoscopee"  type="text"  name="HoroscopeInformation" value="Restricted as per Visibility" id="hinfo" disabled="true" />
			    	<%} %>
			    	
			    	<%if(familyVisibility.charAt(5)=='1') {%>
			    	<input class="foodprefericons"  style="width:160px;float: left;margin-right: 6px;" placeholder="Food Preferred"  type="text"  name="Foodpreferred" value="${familyvalue.foodpreferred}" id="foodpreferred" disabled="true" />	 
			        <%} else {%>
			        <input class="foodprefericons"  style="width:160px;float: left;margin-right: 6px;" placeholder="Food Preferred"  type="text"  name="Foodpreferred" value="Restricted as per Visibility" id="foodpreferred" disabled="true" />
			        <%} %>
			    </li>
			    
			    
			   <%if(profileAccess.contains("FAMILY")){ %>	
					 
				  <li>  
				    
				      <input type="button" id="familyHistorybuttonId" style="width:100px;" value="History" class="historybutton" onclick="familyHistory('<%=(Integer)request.getAttribute("searched_Indid")%>')"/> 	
				      				
			      </li>
			  
			  <%} %>
		 		 			          
	    </ul>
	
	   </form>
	   
	 <%}else { %>
	
	
	      <ul>
		
				<li class="form-row">
					<input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name" value="Upgrade Plan to view" name="Fathername" id="fname" disabled="true" />
					<input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" type="text" value="Upgrade Plan to view" name="Mothername" id="mname" disabled="true" />				   	
				</li>
							   
				<li class="form-row">
					<input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name"type="text"  name="Brothername" value="Upgrade Plan to view" id="bname" disabled="true" />
					<input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name"  type="text" value="Upgrade Plan to view" name="Spousename" id="sname" disabled="true" />
						    
				</li>
				
			    <li class="form-row">
			    	<input class="horoscopeicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Horoscopee"  type="text"  name="HoroscopeInformation" value="Upgrade Plan to view" id="hinfo" disabled="true" />
			    	<input class="foodprefericons"  style="width:160px;float: left;margin-right: 6px;" placeholder="Food Preferred"  type="text"  name="Foodpreferred" value="Upgrade Plan to view" id="foodpreferred" disabled="true" />	 
			    </li>
		 		 
		 		<li> 
		 		     <input type="button" style="width:100px" id="familyHistorybuttonId" value="History" class="historybutton" onClick="familyHistory('<%=(Integer)request.getAttribute("searched_Indid")%>')"/>		 		    	                       
	            </li>			          
	     </ul>
	
	  <%} %>
	  
	</div> 
      
     </div>
	</c:if>
  
   <c:if test="${empty familyvalue}">  
  <h3 align="left"><b>Family Details</b></h3> 
  <div id="indexinputbutton">
												
	<div id ="familydetailalign">
		 	
	      <ul>
		
				<li class="form-row">
					<input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name" value="Not Available" name="Fathername" id="fname" disabled="true" />
					<input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" type="text" value="Not Available" name="Mothername" id="mname" disabled="true" />
				   	
				</li>
							    
				<li class="form-row">
					<input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name"type="text"  name="Brothername" value="Not Available" id="bname" disabled="true" />
					<input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name"  type="text" value="Not Available" name="Spousename" id="sname" disabled="true" />
						    
				</li>
				    			   
			    <li class="form-row">
			    	<input class="horoscopeicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Horoscopee"  type="text"  name="HoroscopeInformation" value="Not Available" id="hinfo" disabled="true" />
			    	<input class="foodprefericons"  style="width:160px;float: left;margin-right: 6px;" placeholder="Food Preferred"  type="text"  name="Foodpreferred" value="Not Available" id="foodpreferred" disabled="true" />	 
			    </li>
		 		 			          
	     </ul>

	</div> 
      
     </div>
	</c:if>
	
	<c:if test="${empty socialaccvalue}">
	<h3 align="left"><b>Social Account Details</b></h3>
		  		  
		  <div id="indexinputbutton">
		  
		  <div id="SocialSuccess" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:#05b7f5;color:#fff;display:none;font-size:16px;">Social Account Saved Successfully!</div>
		  
		  
		 <form  method="post" id="editsocialdetail">
		 
		<div id="socialbuttonsicon">
		
				<ul>
				
						<li id="social-facebook ">
						<input disabled="true" class="facebookicons" style="width:375px;"  type="text" value="Not Available" placeholder="Give your Facebook Url Link"  title="Give your Facebook Url Link" name="Socialwebsitelink"  id="websitelink" />
						</li>
						
						<li id="social-facebook ">
						<input disabled="true" class="linkedinicons" style="width:375px;"  type="text" value="Not Available" placeholder="Give your Linkedin Url Link" title="Give your Linkedin Url Link" name="Socialtwitterlink"  id="twitterlink" />
						</li>
						
						<li id="social-facebook ">
						<input disabled="true" class="twittericons" style="width:375px;"  type="text" value="Not Available" placeholder="Give your twitter Url Link" title="Give your twitter Url Link" name="Sociallinkedlnlink"  id="linkedlnlink" />
						</li>
									
				</ul>
				
		</div> 
		
		
		</form>
		  </div>
	</c:if>
	
	
	
	
	
	
	
        </div> 
	   </div> --%>
	    
        			 <!--end of right div  -->
	</div>

</div>
					
		<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="utiContactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>



<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">

 								<div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
									
                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->
	
	
	
  <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	         
  	<!-- kycDoc Pop up Div -->
	
	<div id="kycDocPopUplight">
		
		 <div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		         
		       	
			<div id="kycDocPopUpDisplay" style=" border: 2px solid #03b2ee;
			    border-radius: 5px;
			    color: #555;
			    margin: 0 auto;
			    padding:0;
			    text-align: left;
			    width: auto;"> 
					              
					       
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF kycDoc Pop up Div -->	                           
                             
                             
                             
                            
                             
<!-- History POPUP -->
	
	<div id="kycHistoryLight" class="kycHistoryLightcss" style="display:none;">
			
			<div class="colsebutton" onclick="fadeOutHistoryDiv()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		 
		    <div id="kycHistoryDisplay" class="kycHistoryDisplaycss"> 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="kycHistoryFade" class="kycHistoryFadecss" onclick="fadeOutHistoryDiv()"></div> 	
	
<!-- EOF  History POPUP -->
	
	
	
	
	
	
	
</body>
</html>
     	