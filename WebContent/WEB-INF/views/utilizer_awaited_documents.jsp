<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 
<%@ page import="java.text.SimpleDateFormat"%>  

<body>


<div style="text-align:left;color:#7c7c7c;"><p><i>On clicking on Accept button user will be notified and you can see in the Viewed document section.</i></p></div>


        <c:if test="${!empty awaitedData}">
        
		        <table class='CSSTableGenerator' style="width:860px">
		        
		        
		           <tr>
		               <!-- <th>S.NO</th>   -->            
		               <th>Name</th>
		               <th>Access Date</th>
		               <th>Description</th>
		               <th>Accept Details</th>
		          </tr>
		          
		        <%int i=0; %>
					
					<c:forEach items="${awaitedData}" var="det">	
					
						   <tr>
				                 <%-- <td><c:out value="<%=++i%>"/></td>  --%>               
				                 <td style="width:180px;"><c:out value="${det.giver_name}"/></td>
				                 <td style="width:130px;"><c:out value="${det.access_given_date}"/></td>
				                 <td style="width:300px;"><c:out value="${det.access_description}"/></td>
				                 <td style="width:130px;"><button href="#" onclick="acceptDoc('${det.access_giver}','${det.access_given_date}')">Accept</button></td>
												
						  </tr>
			           		           
					</c:forEach>
		           
		        </table>
        
        </c:if>  
        
        <c:if test="${empty awaitedData}">
                                            
                                            <div class="notice" style="margin-left: 245px;margin: 72px auto 0;width: auto;">
											      
											      <b>NOTICE :</b> Nothing is there in the Awaited Access from Individual !
											      
											</div>
										
        
        </c:if>
</body>
</html>