<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />

<style>

#circular3dG{
    height: 40px;
    margin-bottom: 5px;
    margin-top: -22px;
    position: relative;
    width: 40px;
}

#logoDiv
{
position:relative;
margin-right: 86px;
width:75px;
height:75px;
}

.circular3dG{
position:absolute;
background-color:#00b6f5; 
/* background-color:#777; */
width:11px;
height:11px;
-moz-border-radius:12px;
-moz-animation-name:bounce_circular3dG;
-moz-animation-duration:1.04s;
-moz-animation-iteration-count:infinite;
-moz-animation-direction:linear;
-webkit-border-radius:12px;
-webkit-animation-name:bounce_circular3dG;
-webkit-animation-duration:1.04s;
-webkit-animation-iteration-count:infinite;
-webkit-animation-direction:linear;
-ms-border-radius:12px;
-ms-animation-name:bounce_circular3dG;
-ms-animation-duration:1.04s;
-ms-animation-iteration-count:infinite;
-ms-animation-direction:linear;
-o-border-radius:12px;
-o-animation-name:bounce_circular3dG;
-o-animation-duration:1.04s;
-o-animation-iteration-count:infinite;
-o-animation-direction:linear;
border-radius:12px;
animation-name:bounce_circular3dG;
animation-duration:1.04s;
animation-iteration-count:infinite;
animation-direction:linear;
}

#circular3d_1G{
left:16px;
top:3px;
-moz-animation-delay:0.39s;
-webkit-animation-delay:0.39s;
-ms-animation-delay:0.39s;
-o-animation-delay:0.39s;
animation-delay:0.39s;
}

#circular3d_2G{
left:24px;
top:9px;
-moz-animation-delay:0.52s;
-webkit-animation-delay:0.52s;
-ms-animation-delay:0.52s;
-o-animation-delay:0.52s;
animation-delay:0.52s;
}

#circular3d_3G{
left:29px;
top:18px;
-moz-animation-delay:0.65s;
-webkit-animation-delay:0.65s;
-ms-animation-delay:0.65s;
-o-animation-delay:0.65s;
animation-delay:0.65s;
}

#circular3d_4G{
left:28px;
top:27px;
-moz-animation-delay:0.78s;
-webkit-animation-delay:0.78s;
-ms-animation-delay:0.78s;
-o-animation-delay:0.78s;
animation-delay:0.78s;
}

#circular3d_5G{
left:17px;
top:29px;
-moz-animation-delay:0.9099999999999999s;
-webkit-animation-delay:0.9099999999999999s;
-ms-animation-delay:0.9099999999999999s;
-o-animation-delay:0.9099999999999999s;
animation-delay:0.9099999999999999s;
}

#circular3d_6G{
left:3px;
top:19px;
-moz-animation-delay:1.04s;
-webkit-animation-delay:1.04s;
-ms-animation-delay:1.04s;
-o-animation-delay:1.04s;
animation-delay:1.04s;
}

#circular3d_7G{
left:0px;
top:6px;
-moz-animation-delay:1.1700000000000002s;
-webkit-animation-delay:1.1700000000000002s;
-ms-animation-delay:1.1700000000000002s;
-o-animation-delay:1.1700000000000002s;
animation-delay:1.1700000000000002s;
}

#circular3d_8G{
left:7px;
top:0px;
-moz-animation-delay:1.3s;
-webkit-animation-delay:1.3s;
-ms-animation-delay:1.3s;
-o-animation-delay:1.3s;
animation-delay:1.3s;
}

@-moz-keyframes bounce_circular3dG{
0%{
-moz-transform:scale(1)}

100%{
-moz-transform:scale(.3)}

}

@-webkit-keyframes bounce_circular3dG{
0%{
-webkit-transform:scale(1)}

100%{
-webkit-transform:scale(.3)}

}

@-ms-keyframes bounce_circular3dG{
0%{
-ms-transform:scale(1)}

100%{
-ms-transform:scale(.3)}

}

@-o-keyframes bounce_circular3dG{
0%{
-o-transform:scale(1)}

100%{
-o-transform:scale(.3)}

}

@keyframes bounce_circular3dG{
0%{
transform:scale(1)}

100%{
transform:scale(.3)}

} 
</style>

</head>
<body>

	<div align="center">
	
	<div id="logoDiv"><span class="logo"></span></div>
	
		<div id="circular3dG">
		
		    
		
			<div id="circular3d_1G" class="circular3dG"></div>
			<div id="circular3d_2G" class="circular3dG"></div>
			<div id="circular3d_3G" class="circular3dG"></div>
			<div id="circular3d_4G" class="circular3dG"></div>
			<div id="circular3d_5G" class="circular3dG"></div>
			<div id="circular3d_6G" class="circular3dG"></div>
			<div id="circular3d_7G" class="circular3dG"></div>
			<div id="circular3d_8G" class="circular3dG"></div>
			
		</div>
		Give Us Few Seconds.....
	</div>

</body>
</html>