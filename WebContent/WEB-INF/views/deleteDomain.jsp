<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>create Domain</title>

<script type="text/javascript">

function levelChange(id)
{
	var domainType = document.getElementById(id).value;	

	if(domainType!="Select Domain Level")
		{
				
		if(domainType=="domainName")
			        
			       {	
			$("#waitingdivId").show();
			
						$.ajax({
								
						        url: "getDomainValue.html",
						        type: 'POST',
						        data: "domainType="+domainType,       
						        success: function (response) { 
						      
						        	$("#waitingdivId").hide();
						        	
						        	var values = "";
						        	if(response!="No Data")		        	
						        	   {    
						        		   var childArray = response.split(",");
						        		   values += "<b>Exist Domains:</b><select id=\"domainValue\" onchange=\"displayRootDomainValue(this.id)\">";
						        		   values += "<option value=\"Select Domain\">Select Domain</option>";	
						        		   for(var i=0;i<childArray.length;i++)
						        			   {
						        		           values += "<option value=\""+childArray[i]+"\">"+childArray[i]+"</option>";		        		         
						        			   }
						        		   values += "</select>";
						        		}
						        	else
						        		{
						        		   values += "No data is there in this level !";
						        		}
						        	$("#noticeDivId").hide(); $("#successDivId").hide();
						        	$("#addChildDiv").html(values); 						        							        	
						        },	                      
						     });
												
			       }
			  
		  else
			  {
			  $("#waitingdivId").show();
					  $.ajax({
							
					        url: "getSubDomainValue.html",
					        type: 'POST',
					        data: "domainType="+domainType,       
					        success: function (response) { 
					      
					        	$("#waitingdivId").hide();
					        	
					        	var values = "";
					        	if(response!="No Data")		        	
					        	   {    					        		
					        		   var childArray = response.split(",");
					        		   values += "<b>Exist Domains:</b><select id=\"domainValue\" onchange=\"displaySubRootDomainValue(this.id)\">";
					        		   values += "<option value=\"Select Domain\">Select Domain</option>";	
					        		   for(var i=0;i<childArray.length;i++)
					        			   {
					        			       var optionValue = childArray[i].split(":");
					        		           values += "<option value=\""+optionValue[0]+"\">"+optionValue[1]+"</option>";		        		         
					        			   }
					        		   values += "</select>";
					        	   }
					        	else
					        		{
					        		   values += "No data is there in this level !";
					        		}
					        	$("#noticeDivId").hide(); $("#successDivId").hide();
					        	$("#addChildDiv").html(values); 	
					        },	                      
					     });
			  }
		}	
}

function deleteChildValue()
{	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var flag='true';
						var numOfElements = document.domainForm.elements.length - 1;		
								
						if(numOfElements==2)
							{			
							    var domainLavel = document.domainForm.elements[0].value;
								var domainType = document.domainForm.elements[1].value;
											
								if(domainType=="Select Domain")
									{
									   flag='false'; 
									   alert("Select a exist domain !");
									}
								
								if(flag=='true')
								{	
									$("#waitingdivId").show();
									
									$.ajax({			
								        url: "deleteDomainHierarchy.html",
								        type: 'POST',
								        data: "domainLavel="+domainLavel+"&domainType="+domainType,       
								        success: function (response) { 	        	
								            
								        	$("#waitingdivId").hide();
								        	
								        	if(response=="success")
								        		{	
								        		   $("#noticeDivId").hide();
								        		   $("#successDivId").show();
								        		   $("#successDivId").html(" Successfully deleted from domain heirarchy !");
								        		}
								        	else
								        		{
								        		   var childData = response.split(",");
								        		   var values  = "";
								        		   values += "<h1>Before delete parent,delete child !</h1><br><br>";
								        		   values += "<ul>";
								        		   values += "<b>Childs related to choosen Parent:</b><br>";
								        		   for(var i=0,j=1;i<childData.length;i++,j++)
								        			   {
								        			     values += "<li>";
								        			     values += j+"-"+childData[i];
								        			     values += "</li>";
								        			   }
								        		   values += "</ul>";
								        		   $("#successDivId").hide();
								        		   $("#noticeDivId").show();
								        		   $("#noticeDivId").html(values);
								        		}
								            			         
								        },	                      
								     });					   	   
								}				
							}
						else
							{
							      alert("Select the Level where you delete !");
							}
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
}

function displayRootDomainValue(id)
{
  var domainValue = document.getElementById(id).value; 
  if(domainValue!="Select Domain")
   {
	  $("#viewSubDomain1Id").html("");
	  $("#viewSubDomain2Id").html("");
	  $("#viewSubDomain3Id").html("");
	  $("#viewSubDomain4Id").html("");
	  $("#viewSubDomain5Id").html("");
	  $("#viewSubDomain6Id").html("");
	  $("#viewSubDomain7Id").html("");
	  $("#viewSubDomain8Id").html("");
	  $("#viewSubDomain9Id").html("");
	  $("#viewSubDomain10Id").html("");
	  $("#viewDomainNameId").html(document.domainForm.elements[1].value);
   }
  
  if(domainValue=="Select Domain")
  {
	  $("#viewDomainNameId").html("");
  }
  
}

function displaySubRootDomainValue(id)
{
	var domainLavel = document.domainForm.elements[0].value;
	var domainType = document.domainForm.elements[1].value;
	
	if(domainType!="Select Domain")
		{
			$.ajax({			
		        url: "displayDomainHierarchy.html",
		        type: 'POST',
		        data: "domainLavel="+domainLavel+"&domainType="+domainType,       
		        success: function (response) { 	        	
		            
		        	  $("#viewDomainNameId").html("");
	        		  $("#viewSubDomain1Id").html("");
	        		  $("#viewSubDomain2Id").html("");
	        		  $("#viewSubDomain3Id").html("");
	        		  $("#viewSubDomain4Id").html("");
	        		  $("#viewSubDomain5Id").html("");
	        		  $("#viewSubDomain6Id").html("");
	        		  $("#viewSubDomain7Id").html("");
	        		  $("#viewSubDomain8Id").html("");
	        		  $("#viewSubDomain9Id").html("");
	        		  $("#viewSubDomain10Id").html("");
	        		  
		        	var diffLevelData = response.split(",");		        	
		        	$("#viewDomainNameId").html(diffLevelData[0]);		        	
		        	for(var i=1;i<diffLevelData.length;i++)
		        		{
		        		  		        		  
		        		   var levelDiv = "viewSubDomain"+i+"Id";
		        		   var treeLook = "";
		        		   for(var j=0;j<i;j++)
		        			   {
		        			     treeLook += "<br><br>";
		        			   }
		        		   $("#"+levelDiv).html(treeLook+diffLevelData[i]);
		        		}		        			        	
		        },	                      
		     });				
		}
}

</script>

</head>



<body>

<div style="width:100%;">


<%String domainList = (String)request.getAttribute("domainName");  //Got the domain list
		   
if(domainList.equals("No Data")) {       // check the domain list containor nothing         
	
	%> 
		    		    
<div id="initialDomain">
<ul>
<li>
<div id="levelNoticeId" class="notice" style="margin-top:10px;margin-left:10px;width: 288px;">Domain has not created yet !</div>
</li>
</ul> 
</div> 

<%}else { %>         
<form name="domainForm" >
 
<div style="text-align:left;width:100%;">
<ul>

<li>						               
				<b>Select Level:</b>
				                  <select id="domainLevelId" onChange="levelChange(this.id)">            
					              <option value="Select Domain Level">Select Domain</option> 
					              <option value="domainName">Main Domain</option>
					              <option value="subDomainlevel1">Sub Domain Level 1</option>
					              <option value="subDomainlevel2">Sub Domain Level 2</option>
					              <option value="subDomainlevel3">Sub Domain Level 3</option>
					              <option value="subDomainlevel4">Sub Domain Level 4</option>
					              <option value="subDomainlevel5">Sub Domain Level 5</option>
					              <option value="subDomainlevel6">Sub Domain Level 6</option>
					              <option value="subDomainlevel7">Sub Domain Level 7</option>
					              <option value="subDomainlevel8">Sub Domain Level 8</option>
					              <option value="subDomainlevel9">Sub Domain Level 9</option>
					              <option value="subDomainlevel10">Sub Domain Level 10</option>
					              </select>
					              	
</li>
				              
<li>				
				<div id="addChildDiv" style="margin-top:11px;margin-left:0px;"> </div>
			
</li>
   
<li>				
				<div style="margin-top:10px;margin-left:10px;">				
				        <input type="button" value="Delete" onClick="deleteChildValue()"/>
				</div>  				                
</li>

<li>

          <div id="resultDivId" style="float:right;height:162px;margin-right:104px;margin-top:-79px;width:243px;">
                 
                 <div id="successDivId" class="success" style="float:right;height:14px;margin-right:-20px;margin-top:21px;width:243px;display:none">
          
                 </div>
                 
                 <div id="noticeDivId" class="notice" style="float:right;height:158px;margin-right:-40px;margin-top:-22px;width:271px;display:none">
          
                 </div>
          </div>  

</li>

</ul>					              
</div>		
</form>			              

                <%}%>
      
</div>

<div style="background-color: #00b6f5;height:1px;margin: 0 auto;width:100%;margin-top:96px;margin-left:10px;margin-right:10px;"></div>

<div id="displayChildValueDiv" style="height:348px;overflow-x:inline;width:100%;margin-top:27px;">
<table class='CSSTableGenerator'>

<tr>

<th>Domain Name</th>
<th>Level-1</th>
<th>Level-2</th>
<th>Level-3</th>
<th>Level-4</th>
<th>Level-5</th>
<th>Level-6</th>
<th>Level-7</th>
<th>Level-8</th>
<th>Level-9</th>
<th>Level-10</th>

</tr>


<tr>

<td><div id="viewDomainNameId"></div></td>
<td><div id="viewSubDomain1Id"></div></td>
<td><div id="viewSubDomain2Id"></div></td>
<td><div id="viewSubDomain3Id"></div></td>
<td><div id="viewSubDomain4Id"></div></td>
<td><div id="viewSubDomain5Id"></div></td>
<td><div id="viewSubDomain6Id"></div></td>
<td><div id="viewSubDomain7Id"></div></td>
<td><div id="viewSubDomain8Id"></div></td>
<td><div id="viewSubDomain9Id"></div></td>
<td><div id="viewSubDomain10Id"></div></td>

</tr>

</table>  
</div>

</body>
</html>