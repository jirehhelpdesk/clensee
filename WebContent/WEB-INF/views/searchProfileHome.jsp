
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="${pageContext.session.maxInactiveInterval};url=outduesession.html">

<title>${regname.firstname}'s Profile</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 


<script src="resources/jscripts/jquery.com.jquery-1.10.2.js"></script>
<script src="resources/jscripts/jquery.com.jqueryUi.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}


#aboutUsfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#aboutUslight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    border: 2px solid #00b5f6;
    display: none;
    height: auto;
    left: 42%;
    margin-left: -378px;
    margin-top: -220px;   
    position: fixed;
    top: 50%;
    width: 1000px;
    z-index: 1002;

}
</style>



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}



#sessionfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#sessionlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 131px;
    left: 50%;
    margin-left: -211px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 404px;
    z-index: 1002;
}



</style>

<script>

function aboutUsfadeout()
{
	document.getElementById('aboutUslight').style.display='none';
	document.getElementById('aboutUsfade').style.display='none';
}

function aboutUsSeeMore()
{
	var aboutUs = document.getElementById("myArea1").innerHTML;
	$("#aboutUsContent").html(aboutUs);
	document.getElementById('aboutUslight').style.display='block';	
	document.getElementById('aboutUsfade').style.display='block';
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>


<script>

$(document).ready(function(){

if(!Modernizr.input.placeholder){

	$('[placeholder]').focus(function() {
	  var input = $(this);
	  if (input.val() == input.attr('placeholder')) {
		input.val('');
		input.removeClass('placeholder');
	  }
	}).blur(function() {
	  var input = $(this);
	  if (input.val() == '' || input.val() == input.attr('placeholder')) {
		input.addClass('placeholder');
		input.val(input.attr('placeholder'));
	  }
	}).blur();
	$('[placeholder]').parents('form').submit(function() {
	  $(this).find('[placeholder]').each(function() {
		var input = $(this);
		if (input.val() == input.attr('placeholder')) {
		  input.val('');
		}
	  })
	});

}

});
</script>


<script type="text/javascript">	
function genderValue()
{
	var male = document.getElementById("sex_m");
	var female = document.getElementById("sex_f");
	
	if(male.checked)
	{
	   document.getElementById("genderId").value="MALE";	
	}
	if(female.checked)
	{
	   document.getElementById("genderId").value="FEMALE";	
	}
}
	

</script>

<script>

function showHideDiv1(id){
	

	myTextBox1 = document.getElementById("b6");
	value=myTextBox1.value;
	
	  var obj = document.getElementById(id);
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	    } else if(obj.style.display=="block"){
	      obj.style.display='none';
	    }
	}


</script>

<script>
 $(document).ready(function () {
        $("#dialog").dialog({ autoOpen: false });
 
          
                return false;
           
    });
</script>

<script type="text/javascript">

function viewPro1(){
		
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");		 
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();	
			$('.topprofilestyle').hide();
	    }
	 
}


function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

</script>





<script>
function switchToEdit(which)
{
	
	if ( which.value == "Edit" )
	{  which.value = "Save";
	
	var divvalue=document.getElementById("editsumdiv").innerHTML;

	
	}
	
	else
		if ( which.value == "Save" )
        {which.value = "Edit";
        document.getElementById("landpagedescription").value=document.getElementById("editsumdiv").innerHTML ;
        $('#flash').delay(500).fadeIn('normal', function() {
  	      $(this).delay(2500).fadeOut();
  	   });
        
        
        document.myform.submit();
        }
	
	var editter = document.getElementById('editsumdiv');
	editsumdiv.contentEditable="true"; 
	
	
	  
	var editname = document.getElementById('editname');
	editname.value="save";
	
}
function divvalue()
{
	
	document.getElementById("landpagedescription").value=document.getElementById("editsumdiv").innerHTML ;
	alert(""+document.getElementById("landpagedescription").value);
}

/*home page*/

function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/

</script>
         
 <script>
function openbrowse()
 
 { 
	 
	 var obj = document.getElementById('openbrowse');
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	      document.getElementById("removeaddphoto").style.display="block";
	     
	    }	  
 }
</script>
 
<script src="resources/texteditor/nicEdit.js" type="text/javascript"></script>

<script>
var area1, area2;

function toggleArea1() {
	
    var buttonName = document.getElementById("htmlbutton").innerHTML;  
    if(buttonName=="Edit")
    	{
    	$("#htmlbutton").empty();
    	$("#htmlbutton").html("Save");
    	}
    if(buttonName=="Save")
    	{
    	$("#htmlbutton").empty();
    	$("#htmlbutton").html("Edit");
    	}
	
	if(!area1) {
		area1 = new nicEditor({fullPanel : true}).panelInstance('myArea1',{hasPanel : true});
		
		
	} else {
		area1.removeInstance('myArea1');
		area1 = null;
		
        var divvalue=document.getElementById("myArea1").innerHTML;
		
    $.ajax({
	    	
	    	type: "post",	   
	    	url: "saveAboutUs.html",	             
	    	data: "aboutUs="+divvalue,	 	    		       
	        success: function (response) {
	        	 
	        	$("#aboutus").fadeIn(500); 
	            $("#aboutus").fadeOut(5000);
	            
	            var data = response.split("/");
	            
	        	$("#myArea1").html(data[0].substring(0,data[0].length-1));
	        	
	        },
	        error : function() {
				alert('Error while fetching response');
			}	        
	     });
		
	}
}

function getAboutUs()
{	
	var  ind_id = document.getElementById("Ind_id").value;
		
	 $.ajax({
	    	
	    	type: "post",	   
	    	url: "getOtherAboutUsInfo.html",	             
	    	data: "ind_kyc="+ind_id,	 	    		       
	        success: function (response) {
	        	 
	        	var data = response.split("KYCSEPERATORKYC");
	            
		        $("#myArea1").html(data[0].substring(0,data[0].length-1));	
		        
		        var element = document.querySelector('#myArea1');
		        
		        if( element.offsetHeight < element.scrollHeight) {
		        	  
		        	     $("#seemore").show();
		        	}	        	
	        },
	       	        
	     });
	 
	}
	
	
	
	
	
	
/* Main Header Actions  */

function gotoProfile()
{
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoMyDocument()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoSharingManager()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoStore()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoAlerts()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
}

function gotoPlans()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
}

function gotoSetings()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}

function hideAddvertise()
{
    $("#ProfileHeadingDiv").hide();	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}
</script>

<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
</head>

<body onload="getAboutUs()" onclick="hidesearchDiv()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };

    String regVisibility = (String)request.getAttribute("regVisibility");
    String basicVisibility = (String)request.getAttribute("basicVisibility");
    String familyVisibility = (String)request.getAttribute("familyVisibility");
    String socialVisibility = (String)request.getAttribute("socialVisibility");
    
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("sarchedIndId")%>"/>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
		
		  <a href="profiletohome.html"><div class="logo"></div></a>

			<div class="tfclear"></div>
			<div class=" mainmenu">
				
				<ul class="main_menu" >
					
					<li></li>
					<li id="myMenus0"><a class="myprofilefocus"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocuments"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
					
				</ul>

			</div>

		</div>
		
 <!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em>${indName.firstname}</em></a>
                 <div style="clear:both"></div>
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			      			
					<div class="leftside">
					
					<div id="textBox">
					<c:if test="${!empty profilePicInfo.profile_picture_name}">
	 
						 <%
	 						 	String filename1 = (String) session.getAttribute("profilepic");
	 						 	int searchedIndId = (Integer)request.getAttribute("searched_Indid");
						        String user0 = Integer.toString(searchedIndId);
	 					 %>   
	 					  <c:set var="profilePicture"  value="${profilePicInfo.profile_picture_name}"/>
	 					  <%String profilePicture = (String) pageContext.getAttribute("profilePicture");%>
									
										
						 <div class="user_photoupload view view-first" id="user_photoupload">
					    
					     <span> 
					     <%-- <img src="${pageContext.request.contextPath}<%="/previewprofileOther.html?fileName="+profilePicture+"&docCategory=Profile_Photo&year="+session.getAttribute("created_year")+"&indId="+user0+""%>" /></img> --%>
					          <img src="${pageContext.request.contextPath}<%="/previewThumbprofileOther.html?fileName="+profilePicture+"&docCategory=Profile_Photo&year="+session.getAttribute("created_year")+"&indId="+user0+""%>" /></img>
					     </span>						   
						     
						 </div>
						   
						 <div class="user_photo">
						 <p><strong><c:out value="${regname.firstname}"></c:out></strong> <br>
						 <span>
						 <c:if test="${!empty basicdetails.present_address}">
						 <c:set var="presentplace"  value="${basicdetails.present_address}"/>
									<%
										String myVariable = (String) pageContext
														.getAttribute("presentplace");
												String[] ary = myVariable.split("##");
												String str = ary[4];
												str = str.substring(0, str.length());
									%>
						 <%=str%>
						 </c:if>
						 </span>			 
						 </p>
						 </div>
				</c:if>	
									                
                <div class="user_display">
                <div id="closepic" style="display: block;">
                
                
                <c:if test="${empty profilePicInfo.profile_picture_name}">
                                
                <div class="user_photoupload view view-first" id="user_photoupload">
	 
				<span id="userphoto"><img src="resources/kyc_images/Koala.jpg" width="196" height="222" /></span>
					 
			   </div> 
						   
			 <div class="user_photo">
					<p><strong><c:out value="${regHisname.first_name}"></c:out></strong> <br>
					<span>
					<c:if test="${!empty basicdetails.present_address}">
					<c:set var="presentplace"  value="${basicdetails.present_address}"/>
									<%
										String str = "";
												String myVariable = (String) pageContext
														.getAttribute("presentplace");
												String[] ary = myVariable.split("##");
												str = ary[4];
												str = str.substring(0, str.length());
									%>
						<%=str%>
					</c:if>
					</span>
					</p>
			  </div>
                              	
		   </c:if>		          	 					
	      </div>
	      </div>		
	      
	         		     	         		      	         		                  								 				 																			
							<div class="myaccounts">
							<h2>
							<span>
							<img src="resources/kyc_images/contact_info.png" width="25" height="26" />
							Contact Info</span>
							</h2>							
							<div class="account_titel"><img src="resources/images/info_mainid_icons.png" /><span>E-mail address</span></div>
							<ul>
							<li>
							<table>
							<tbody>
							<tr>
							<td>							
							<span id="emailid">
							<%if(regVisibility.charAt(0)=='1') {%>
							<c:out value="${regHisname.email_id}"></c:out>
							<%}else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</span>							
							</td>
							</tr>
							</tbody>
							</table>
							</li>
							</ul>
							<div class="mobileaccount_titel"><img src="resources/images/info_mobile_icons.png" /><span>Mobile Number</span></div>
							<ul>
							<li>
							<table>
							<tbody>
							<tr>
							<td><span id="mobileno">
							<%if(regVisibility.charAt(1)=='1') {%>
							<c:out value="${regHisname.mobile_no}"></c:out>
							<%}else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</span></td>
							</tr>
							</tbody>
							</table>
							</li>
							</ul>
							
							<div class="present_addressaccount_titel"><img src="resources/images/info_presentaddress_icons.png" /><span>Present Address</span></div>
							<ul>
							<li>
							<div id="presentaddressget" >
								<span id="PresentAddID" style="color:#2d2d2d;">
								<c:if test="${!empty basicdetails.present_address}">
								<%-- <c:out  value="${basicdetails.present_address}" ></c:out> --%>
								<c:set var="present_address"  value="${basicdetails.present_address}" ></c:set>
								
								<%
																	String present_address = (String) pageContext
																				.getAttribute("present_address");
																		String pre_addressAry[] = present_address.split("##");
																%>
								<%if(basicVisibility.charAt(8)=='1') {%>
								
								<table class="textwidth">
								<tr><td><%=pre_addressAry[0]%>,<%=pre_addressAry[3]%>,<%=pre_addressAry[2]%>,
								         <%=pre_addressAry[1]%>,<%=pre_addressAry[1]%>,<%=pre_addressAry[7]%>,
								         <%=pre_addressAry[4]%>,<%=pre_addressAry[5]%>,<%=pre_addressAry[6]%>,
								         <%=pre_addressAry[8]%></td></tr>
								</table>
											
							
								<%}else{ %>
								
								      Restricted as per the visibility !
								      
								<%} %>
								
								</c:if>
								
								<c:if test="${empty basicdetails.present_address}">																
										Not Filled Yet !
								</c:if>
								
								</span>														
							</div>
							</li>
							</ul>
							</div>
							
							<c:if test="${!empty basicdetails.languages}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/languages_known_icon.png" width="25" height="26" />Languages Known</span>
							</h2>
							<ul>
							<li>
								<div id="languageDiv">
								           
								           <%if(basicVisibility.charAt(6)=='0') {%>								                 
								                 <c:out value="Language is Restricted"></c:out>								           
								           <%}else{ %> 
								                 <c:out value="${basicdetails.languages}"></c:out>
								           <%}%>
								           
								</div>
							</li>
							</ul>
							</div>
							</c:if>
							
							<c:if test="${empty basicdetails.languages}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/languages_known_icon.png" width="25" height="26" />Languages Known</span>
							</h2>
							<ul>
							<li>
							<div id="languageDiv">Not Filled Yet !</div>
							</li>
							</ul>
							</div>
							</c:if>
							
							
							<c:if test="${empty socialaccvalue}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/social_account_icon.png" width="25" height="26" />Social Account </span>
							</h2>
							<ul>
							<li>
							<img src="resources/kyc_images/face_book_icon.png" width="17" height="17" /><div id="websitelinkname">Not Available</div>
							</li>
							<li>
							<img src="resources/kyc_images/twitter_icon.png" width="17" height="17" /><div id="twitterlinkname">Not Available</div>
							</li>
							<li>
							<img src="resources/kyc_images/linkedin_icon.png" width="17" height="17" /><div id="linkedlnlinkname">Not Available</div>
							</li>							
							</ul>
							</div>
							</c:if>	
							
							<c:if test="${!empty socialaccvalue}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/social_account_icon.png" width="25" height="26" />Social Account </span>
							</h2>
							<ul>
							<li>
							<img src="resources/kyc_images/face_book_icon.png" width="17" height="17" /><div id="websitelinkname">
							<%if(socialVisibility.charAt(0)=='1') {%>
							
									<c:if test="${!empty socialaccvalue.socialwebsitelink}">
												<a href="${socialaccvalue.socialwebsitelink}" target="_blank">
													<c:out value="facebook connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.socialwebsitelink}">									
										<c:out value="facebook not connected"></c:out>						
									</c:if>	
									
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							<li>
							<img src="resources/kyc_images/twitter_icon.png" width="17" height="17" /><div id="twitterlinkname">
							<%if(socialVisibility.charAt(1)=='1') {%>
									
									<c:if test="${!empty socialaccvalue.sociallinkedlnlink}">
												<a href="${socialaccvalue.sociallinkedlnlink}" target="_blank">
													<c:out value="twitter connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.sociallinkedlnlink}">									
										<c:out value="twitter not connected"></c:out>						
									</c:if>	
								
								
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							<li>
							<img src="resources/kyc_images/linkedin_icon.png" width="17" height="17" /><div id="linkedlnlinkname">
							<%if(socialVisibility.charAt(2)=='1') {%>
							
							        <c:if test="${!empty socialaccvalue.socialtwitterlink}">
												<a href="${socialaccvalue.socialtwitterlink}" target="_blank">
													<c:out value="linkedin connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.socialtwitterlink}">									
										<c:out value="linkedin not connected"></c:out>						
									</c:if>	
																		
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							
							</ul>
							</div>
							</c:if>												
												                
					</div>
					
					 </div>
					
					<div class="rightside">
					
					<span class="topprofilestyle" style="display: none;"></span>
					
					<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							<div id="viewmore" title="View More">
							
							</div>
							
							<div id="dialog"  title="View More" style="width:400px;height:800px;">
							
							</div>
								
						   </div>
					</div>	
					 
					 
					  
					  <!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

									

					<div id="ProfileHeadingDiv" style="display: block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>



					
							        		
					   <!-- Advertisment Div for Entire Page  -->


	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
							        <b></b>
								    <span>Hey i am <c:out value="${regname.firstname}"></c:out></span>
								   <c:set var="member_date"  value="${regname.cr_date}"/>
		                              <%
		                              	Date member_date = (Date) pageContext.getAttribute("member_date");
		                              	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		                              	String reportDate = df.format(member_date);
		                              	String dateAry1[] = reportDate.split(" ");
		                              	String dateAry2[] = dateAry1[0].split("-");
		                              	int month = Integer.parseInt(dateAry2[1]);
		                              	String year = dateAry2[0];
		                              %>							
								    <p>Member since <%=months[month - 1] + " " + year%></p>
							</h2>
						 </div>
					
<div class="module-body">
	
	<div id="viewUsers1" class="aboutus" style=" height: auto !important;margin-bottom: 15px;">
																		
				<div id="aboutus" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:#05b7f5;color:#fff;display:none;font-size:16px;">Saved Successfully!</div>
											
						<h1 class="rightsideheader">About Me
						<c:if test="${!empty Summarydetails.cr_date}"> 
						 <p>Last updated on <c:set var="Aboutdate" value="${Summarydetails.cr_date}" />
						 		<%
						 			    Date about_date = (Date) pageContext.getAttribute("Aboutdate");
						 				String reportDate1 = df.format(about_date);
						 				String dateAry3[] = reportDate1.split(" ");
						 				String dateAry4[] = dateAry3[0].split("-");
						 				int abt_month = Integer.parseInt(dateAry4[1]);
						 				String abt_year = dateAry4[0];
						 		%>				 
						 <%=months[abt_month - 1] + " " + abt_year%></p> 
						 </c:if>
						</h1>
																		
						
						<div id="texteditediv" style="clear: both;">
						
							<div id="myArea1" style=" height: 200px;">   </div> 
							
							<div class="seemorediv" id="seemore" style="display:none;float:left" ><a href="#" onclick="aboutUsSeeMore()">See More..</a></div>
			  
				              <div id="textareaId" style="display:none;">
				              
					              
	                              
                             </div> 
                            
							<div id="flash" style="display: inline;width: 817px;">   </div>
							
						</div>
						
					
					<div style="clear: both;">
						
						<% String planName = (String)session.getAttribute("planName");%>
						
										 
						<h1 class="rightsideheader">Current Company Details	
							
							<c:if test="${!empty empdate}">
								<p>Last updated on <c:set var="empDate" value="${empdate}"></c:set>
								
								<%
								    String emp_date = (String) pageContext.getAttribute("empDate");
									String empDate = emp_date;
									String empdateAry1[] = empDate.split("%");
									String empdateAry2[] = empdateAry1[0].split("-");
									int emp_month = Integer.parseInt(empdateAry2[1]);
									String emp_year = empdateAry2[0];
														%>	
								<%=months[emp_month - 1] + " " + emp_year%>
								</p>
						  </c:if>
						
						</h1>
											
						<div class="companydetails">
						
						<c:if test="${!empty employeedetail}">
						
						<%
						String org_Name = "";
			 			String jobDesigNation= "";
			 			String job_profile = "";
			 			String jobFrom = "";
			 			String jobTo = ""; %>
			 			
			 			
						<% if(!planName.equals("Basic")) { %>
						
								<c:forEach items="${employeedetail}" var="det">
									
											<c:set var="employee"  value="${det.docs_data}"/>
								 <%
								 	String myVariable = (String) pageContext
								 					.getAttribute("employee");

								 			String[] ary1 = myVariable.split("&&");

								 			 org_Name = ary1[0];
								 			 jobDesigNation= ary1[1];
								 			 job_profile = ary1[2];
								 			 jobFrom = ary1[3];
								 			 jobTo = ary1[4];
								 %>		
								 
								</c:forEach>	
									
									
									<div style="width: 100%;">
												<ul>
													<li>
														<h1 class="currentcompanydetails">
															<span></span> <b><%=org_Name%></b>  <p style="float:right;"><%=jobFrom%> - <%=jobTo%></p>  
														</h1>
														<div>
														<table class="profilescroll" cellspacing="0" align="left" style="display: inline; background: #eee; margin-top:0px;border-radius: 0 0 5px 5px;margin-top:-9px; padding: 10px; width:808px !important;-webkit-logical-width:788px !important;">
														<tr style="height: 18px;">
																<th style="width: 150px; ">Job Designation</th>
																<td style="width: 10px;text-align: left;">:</td>
																<td><%=jobDesigNation%></td>
															</tr>
															
															<tr style="height: 18px;">
																<th style="width: 150px; text-align: left;vertical-align: top;">Job Summary</th>
																<td style="vertical-align: top;width: 10px;text-align: left;">:</td>
																<td style="width: 808px; text-align: left;vertical-align: top;text-align: justify;"><%=job_profile%></td>
															</tr>
															
														
														</table>
														</div>
													</li>
												</ul>
											</div>
							
							<%}else{ %>
							        <h1>To view the Details upgrade your plan !</h1>
							<%} %>
						</c:if>
						
						
						<c:if test="${empty employeedetail}">
						<span>Employment Information not Filled Yet !</span>
						</c:if>
						</div>
						
						</div> 
						
						
						
						
						
						
						<div style="clear: both;">
						 
						<h1 class="rightsideheader">Educational Details	
							
							<c:if test="${!empty postOn}">					
								<p>Last updated on <c:set var="Edudate" value="${postOn}" />
								<%
									String edu_date = (String) pageContext.getAttribute("Edudate");
										String docDate = edu_date;
										String docdateAry1[] = docDate.split(" ");
										String docdateAry2[] = docdateAry1[0].split("-");
										int doc_month = Integer.parseInt(docdateAry2[1]);
										String doc_year = docdateAry2[0];
								%>	
								<%=months[doc_month - 1] + " " + doc_year%></p> 
								
								
						    </c:if>			
						</h1>
						
						<div class="companydetails">
						<c:if test="${!empty documentdetail}">
						  
						  <c:set var="comp" value="${accordNames}"/>
						         <c:set var="documentdetail" value="${documentdetail}"/>
						
						<%
							String documentdetaildata = (String) pageContext.getAttribute("documentdetail");
  
								if (!documentdetaildata.equals("null/")) {

									String data[] = documentdetaildata.split("/");
									
							if(!planName.equals("Basic")) {
									 																					
									for (int i = 0; i < data.length; i++) {

										/* if (data[i].contains(",")) { */
											
											String datasplit[] = data[i].split("-");
											String heading =  datasplit[0];
											String academicData[] =  datasplit[1].split(",");
												%>
						            	 
						            	 <div style="width:100%;">
						            	 <ul>
						            	 <li>
						            	 <h1>
						            	 <span></span>
						            	 <b><%=heading%></b></h1>
						            	 <div>
						            	<table class="profilescroll" cellspacing="0" align="left" style=" margin-top: -9px; display: inline;background: #eee;border-radius: 0 0 5px 5px; padding: 10px; width: 400px !important;-webkit-logical-width: 380px !important;">
						            	
						            	
						            	 <%for(int d=0;d<academicData.length;d++) { 
						            	     String contentData[] = academicData[d].split(":");
						            	     %>
								            	   <tr style="height: 18px;">
								            	   <th style="width:150px;text-align:left;"><%=contentData[0]%></th><td>:</td><td><%=contentData[1]%></td>
								            	   </tr>
								           <%} %>
								            	 
						            	 </table>
						            	 </div>
						            	 </li>
						            	 </ul>
						            	 </div>
						        	 <%
						        	 	/* } */
						        	 
									}  // End of For loop
						        
										} else {%>
						            	 
				            	      	<h1>To view the details upgrade your Plan !</h1>
				            	       
				            	   <%} 
				            	   
								}else {
						        	 %>
							            <span>Academic Details Not Filled Yet !</span>
																																		            
							         <%}%> 
								
								
																
						</c:if>
						</div>
						<c:if test="${empty documentdetail}">
						<span>Academic Details Not Filled Yet !</span>
						</c:if>
						</div> 
						
					<!-- Required Division for viewUser1 not go beyond  -->
					
					<div style="clear: both;margin-top:0px;">
						
						</div>
					
					<!-- End of Required Division for viewUser1 not go beyond  -->	
						
			</div>
		</div>	
	</div>	<!-- End of -->	

	<div id="center-body-div" style="display:block   margin: 0 auto; width: 100%;">
<div  id="panel-1" >

 <h3 align="left">Registration Details</h3> 
         
<div  style="display:block" id="indexinputbutton">
              
        <c:if test="${!empty regHisname}">  
                                
       <form id="editregistration"  name="regform" >
       <div class="displayregidetails">
										
											<ul>
												<li class="form-row" style=" float: left;height: auto;width: 100%;">
																																	
												<input class="firstname" title="First Name" style="width: 102px;float: left;margin-right: 6px;" type="text"
														value="${regHisname.first_name}" name="firstname"
														id="reg1" disabled="true"
														
														onchange="myFunctionreg(this.id)" placeholder="First Name" onkeyup="validateRegDetailsEach()"/>
														
														
															<input class="middlename" title="Middle Name" style="width: 113px;float: left;margin-right: 6px;" type="text"
														value="${regHisname.middle_name}" name="middlename" placeholder="Middle Name"
														id="reg0" disabled="true"
														
														onchange="myFunctionreg(this.id)"  onkeyup="validateRegDetailsEach()"/>
													<input class="lasstname" title="Last Name" style="width: 113px;float: left;margin-right: 6px;" type="text"
														value="${regHisname.last_name}" name="lastname" id="reg3" placeholder="Lastname"
														disabled="true" 
														onchange="myFunctionreg(this.id)"  onkeyup="validateRegDetailsEach()"/>
											
												
												</li>
												<li class="form-row" >
												<%if(regVisibility.charAt(0)=='1') {%>
												<input class="mailidicons" title="Email Id" style="clear: both;
                                                  float: left;
                                                  margin-top: 5px;
                                                  position: relative;
                                                  text-align: left;
                                                  width: 375px;" type="text"
												value="${regHisname.email_id}" name="emailid" id="reg4" placeholder="Email-id" disabled="true" />
														
											    <%}else { %>
											    <input class="mailidicons" title="Email Id" style="clear: both;
                                                  float: left;
                                                  margin-top: 5px;
                                                  position: relative;
                                                  text-align: left;
                                                  width: 375px;" type="text"
												value="Email Id Restricted" name="emailid" id="reg4" placeholder="Email-id" disabled="true" />
													
											    <%} %>
												</li>

												<li class="form-row">
												
												<%if(regVisibility.charAt(1)=='1') {%>	
													<input class="mobileicons" title="Mobile No" style=" clear: both; 
														    float: left;
														    margin-top: 5px;
														    position: relative;
														    text-align: left;
														    width: 375px;" type="text" type="text"
														value="${regHisname.mobile_no}" name="mobileno" id="reg5" placeholder="Mobile No"
														disabled="true"	/>
												<%} else { %>
												<input class="mobileicons" title="Mobile No" style=" clear: both;
														    float: left;
														    margin-top: 5px;
														    position: relative;
														    text-align: left;
														    width: 375px;" type="text" type="text"
														value="Mobile.no Restricted" name="mobileno" id="reg5" placeholder="Mobile No"
														disabled="true"	/>
												<%} %>
												
												</li>
												
												<c:set var="indgender"  value="${regHisname.gender}"/>
												 <%
										            String indgender = (String)pageContext.getAttribute("indgender");										          
										         %>
         
          										<%if(indgender.equals("MALE")) {%>
          										
			          										<li class="form-row" >
																												
																<input class="gendericons" title="Gender" disabled="true" style=" clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;" type="text" type="text"
																	value="${regHisname.gender}" id="genderId" name="gender"  placeholder="Gender"/>												
															</li>
															
														   <li class="form-row" style=" clear: both;">
																										
															<div class="register-switch">
														      <input type="radio" name="sex" value="MALE" id="sex_m" class="register-switch-input" disabled="true" onclick="genderValue()">
														      <label for="sex_m" class="register-switch-malelabel">&nbsp;</label>														      
														    </div>
														   
														  </li>	
          										
          										
          										<%}else {%>
          										
			          										<li class="form-row" >
																												
																<input class="gendericons" title="Gender" disabled="true" style=" clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;" type="text" type="text"
																	value="${regHisname.gender}" id="genderId" name="gender"  placeholder="Gender"/>												
															</li>
															
														    <li class="form-row" style=" clear: both;">
																										
																<div class="register-switch">													      
															      <input type="radio" name="sex" value="FEMALE" id="sex_f" class="register-switch-input" disabled="true" onclick="genderValue()">
															      <label for="sex_f" class="register-switch-femalelabel">&nbsp;</label>														      
															    </div>
														   
														    </li>	
          										
          										<%} %>
													
  										
 </ul>	
									
 </div>
	</form> 
        </c:if> 
     
 </div>
  
  <c:if test="${empty basicdetails}">           
   
  <h3 align="left">Basic Details</h3>
      
   <div id="indexinputbutton">
      
   <form name="savebasic" id="savebasic"  enctype="multipart/form-data">
        
   		<div id ="familydetailalign">
		 <ul>
		 <li>
		
		 <input class="mailidicons"   id="street" style="width:160px;float: left;margin-right: 6px;"  type="text" value="Not Available" name="Emailalternative" title="Alternative Email Id" disabled="true" />		 		
		 <input class="dateofbirthicons"  style="width:160px;float: left;margin-right: 6px;" value="Not Available" title="Date Of Birth"  type="text"  name="DOB"  id="b8" disabled="true" />		
		
		 </li>
		  
		  <li>
		  
		  <input  class="telofficeicons"  placeholder="Tel-Office" style="width:160px;float: left;margin-right: 6px;" value="Not Available" name="Teloff" id="b4" title="Office Telephone Number" disabled="true" />		  		  
		  <input class="telresidentialicons"  value="Not Available" placeholder="Tel-Residence" style="width:160px;float: left;margin-right: 6px;" value="Not Available" title="Residence Telephone Number" name="Telres" id="b5"  disabled="true" />
		 
		  </li>
		  
		   <li>
		  
		   <input  class="nationalityicons"  placeholder="Nationality" style="width:160px;float: left;margin-right: 6px;" value="Not Available" name="Nationality" title="Nationality" id="b3" disabled="true" />		   		   
		   <input  class="matrialstatusicons"  placeholder="Marital Status" style="width:160px;float: left;margin-right: 6px;" value="Not Available" name="Matrialstatus" title="Marital Status" id="b2" disabled="true" />
		   
		   </li>
		   
		    <li>
		    	    
		    <input class="languagesicons"  placeholder="Language Known" style="width:160px;float: left;margin-right: 6px;" value="Not Available" name="Languages" title="Language known" id="b6" disabled="true" />		    		    		   
		    <input class="hobbiesicons" placeholder="Hobbies" style="width:160px;float: left;margin-right: 6px;" value="Not Available" title="Hobbies" name="Hobbies" id="b7" disabled="true" />
		    
		    </li>
		    
		     <li></li>
		 </ul>
		 </div>
					
			<div id="permanentaddressdetails">
			<h1>
			<img src="resources/kyc_images/permanentaddress_iconsone.png" />
			<span>Permanent Address</span>
			</h1>
			<ul>
			<li>
			
			<input id="perdoor"  disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available"  placeholder="Door" title="Door Number" type="text"/>
			
			<input id="permandal"  disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Street" title="Street Name" type="text" />
									
			<input id="perzone"  disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Area" title="Area Name" type="text" />
			
			</li>
			
			<li>
			
			<input id="perlandmark"  disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Landmark" title="Near By Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
			
			<input id="perpincode"  disabled="true" name="pincode" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Pincode" title="Pincode" type="text" maxlength="6"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="percity"  disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
						
			</li>
			
			<li>
			
			<input id="perdistrict" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Not Avilable" placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perstate" disabled="true" name="state" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			
			<input id="percountry" disabled="true" name="country" style="width:120px;float: left;margin-right: 6px;" value="Not Available" placeholder="Country" title="Country Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			</li>
					
			</ul>	
			 						
			</div>	
						
  </form>
 </div>
 
 </c:if>
  
  
  <c:if test="${!empty basicdetails}">           
   
  <h3 align="left">Basic Details</h3>
      
   <div id="indexinputbutton">
      
   <form name="savebasic" id="savebasic"  enctype="multipart/form-data">
        
   		<div id ="familydetailalign">
		 <ul>
		 <li>
		 <%if(basicVisibility.charAt(0)=='1') {%>
		 <input class="mailidicons" title="Alternativ Email Id"  id="street" style="width:160px;float: left;margin-right: 6px;"  type="text" value="${basicdetails.emailalternative}" name="Emailalternative" disabled="true" />
		 <%}if(basicVisibility.charAt(0)=='0') {%>
		 <input class="mailidicons" title="Alternativ Email Id"  id="street" style="width:160px;float: left;margin-right: 6px;"  type="text" value="Alternativ emailId Restricted" name="Emailalternative" disabled="true" />
		 <%} %>
		 
		 <%if(basicVisibility.charAt(1)=='1') {%>
		   <input class="dateofbirthicons" title="Date of Birth" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.DOB}"  type="text"  name="DOB"  id="b8" disabled="true" />
		 <%} if(basicVisibility.charAt(1)=='0') { %>
		  <input class="dateofbirthicons" title="Date of Birth" style="width:160px;float: left;margin-right: 6px;" value="DOB is Retricted"  type="text"  name="DOB"  id="b8" disabled="true" />
		 <%} %>
		 </li>
		  <li>
		  <%if(basicVisibility.charAt(2)=='1') {%>
		  <input  class="telofficeicons" title="Office Telephone Number" placeholder="Tel-Office" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.teloff}" name="Teloff" id="b4" disabled="true" />
		  <%}if(basicVisibility.charAt(2)=='0') { %>
		  <input  class="telofficeicons" title="Office Telephone Number" placeholder="Tel-Office" style="width:160px;float: left;margin-right: 6px;" value="Office No Restricted" name="Teloff" id="b4" disabled="true" />
		  <%} %>
		  <%if(basicVisibility.charAt(3)=='1') {%>
		  <input class="telresidentialicons" title="Residence Telephone Number" placeholder="Tel-Residence" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.telres}" name="Telres" id="b5"  disabled="true" />
		  <%} if(basicVisibility.charAt(3)=='0') {%>
		  <input class="telresidentialicons" title="Residence Telephone Number" placeholder="Tel-Residence" style="width:160px;float: left;margin-right: 6px;" value="Residence No Restricted" name="Telres" id="b5"  disabled="true" />
		  <%} %>
		  </li>
		   <li>
		   <%if(basicVisibility.charAt(4)=='1') {%>
		   <input  class="nationalityicons" title="Nationality" placeholder="Nationality" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.nationality}" name="Nationality" id="b3" disabled="true" />
		   <%}if(basicVisibility.charAt(4)=='0') { %>
		   <input  class="nationalityicons" title="Nationality" placeholder="Nationality" style="width:160px;float: left;margin-right: 6px;" value="Nationality Restricted" name="Nationality" id="b3" disabled="true" />
		   <%} %>
		   <%if(basicVisibility.charAt(5)=='1') {%>
		   <input  class="matrialstatusicons" title="Marital Status" placeholder="Marital Status" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.matrialstatus}" name="Matrialstatus" id="b2" disabled="true" />
		   <%}if(basicVisibility.charAt(5)=='0') { %>
		   <input  class="matrialstatusicons" title="Marital Status" placeholder="Marital Status" style="width:160px;float: left;margin-right: 6px;" value="Marital Status Restricted" name="Matrialstatus" id="b2" disabled="true" />
		   <%} %>
		   </li>
		    <li>
		    <%if(basicVisibility.charAt(6)=='1') {%>
		    <input class="languagesicons" title="Language Known" placeholder="Language Known" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.languages}" name="Languages" id="b6" disabled="true" />
		    <%}if(basicVisibility.charAt(6)=='0') {%>
		   <input class="languagesicons" title="Language Known" placeholder="Language Known" style="width:160px;float: left;margin-right: 6px;" value="Language is Restricted" name="Languages" id="b6" disabled="true" />
		    <%} %>
		    
		    <%if(basicVisibility.charAt(7)=='1') {%>
		    <input class="hobbiesicons" title="Hobbies" placeholder="Hobbies" style="width:160px;float: left;margin-right: 6px;" value="${basicdetails.hobbies}" name="Hobbies" id="b7" disabled="true" />
		    <%} if(basicVisibility.charAt(7)=='0') { %> 
		    <input class="hobbiesicons" title="Hobbies" placeholder="Hobbies" style="width:160px;float: left;margin-right: 6px;" value="Hobbies Restricted" name="Hobbies" id="b7" disabled="true" />
		    <%} %>
		    </li>
		     <li></li>
		 </ul>
		 </div>
			
			<%if(basicVisibility.charAt(9)=='1') {%>
			
			<div id="permanentaddressdetails">
			<h1>
			<img src="resources/kyc_images/permanentaddress_iconsone.png" />
			<span>Permanent Address</span>
			</h1>
			<ul>
			<li>
			
			<input id="perdoor" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[0];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Door" title="Door Number" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="permandal" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
           String[] ary1=myVariable1.split("##");
           String str = ary1[3];
           
         %>
        value="<%=str%>" </c:if> placeholder="Street" title="Street Name" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perzone" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
           String[] ary1=myVariable1.split("##");
           String str = ary1[2];
           
         %>
        value="<%=str%>" </c:if> placeholder="Area" title="Area Name" type="text"   onchange='ReviewDefectsper(length);' />
			</li>
			
			<li>
			
			<input id="perlandmark" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}" />
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[1];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Landmark" title="Near By Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
			
			<input id="perpincode" disabled="true" name="pincode" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[7];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Pincode" title="Pincode" type="text" maxlength="6"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="percity" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[4];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
						
			</li>
			
			<li>
			
			<input id="perdistrict" disabled="true" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[5];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perstate" disabled="true" name="state" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           System.out.println("myVariable1"+myVariable1);
           String[] ary1=myVariable1.split("##");
           String str = ary1[6];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			
			<input id="percountry" disabled="true" name="country" style="width:120px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
          String[] ary1=myVariable1.split("##");
          String str = ary1[8];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Country" title="Country Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			</li>
					
			</ul>	
			 						
			</div>	
			
			<%} else{%>
			
			<div id="permanentaddressdetails">
			<h1>
			<img src="resources/kyc_images/permanentaddress_iconsone.png" />
			<span>Permanent Address</span>
			</h1>
			<ul>
			<li>
			
			<input id="perdoor" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Door" title="Door Number" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="permandal" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility"placeholder="Street" title="Street Name" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perzone" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Area" title="Area Name" type="text"   onchange='ReviewDefectsper(length);' />
			</li>
			
			<li>
			
			<input id="perlandmark" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Landmark" title="Near By Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
			
			<input id="perpincode" disabled="true" name="pincode" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Pincode" title="Pincode" type="text" maxlength="6"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="percity" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
						
			</li>
			
			<li>
			
			<input id="perdistrict" disabled="true" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
									
			<input id="perstate" disabled="true" name="state" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
						
			<input id="percountry" disabled="true" name="country" style="width:120px;float: left;margin-right: 6px;" value="Restricted as per Visibility" placeholder="Country" title="Country Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			</li>
					
			</ul>	
			 						
			</div>	
			<%} %>      
  </form>
 </div>
 
 </c:if>
 
  <c:if test="${!empty familyvalue}">  
  <h3 align="left">Family Details</h3> 
  <div id="indexinputbutton">
  
  <div id ="familydetailalign">
  
  <% if(!planName.equals("Basic")) { %>

		 <ul>
		
				<li class="form-row">
				     
				    <%if(familyVisibility.charAt(0)=='1') {%>
				    <input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name" title="Father Name"  value="${familyvalue.fathername}" name="Fathername" id="fname" disabled="true" />
				    <%}else{ %>
				    <input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name" title="Father name"  value="Restricted as per Visibility" name="Fathername" id="fname" disabled="true" />
				    <%} %>
				
				<%if(familyVisibility.charAt(1)=='1') {%>
			       <input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" type="text" title="Mother Name"  value="${familyvalue.mothername}" name="Mothername" id="mname" disabled="true" />
				   <%}else{ %>
				   <input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" type="text" title="Mother Name"  value="Restricted as per Visibility" name="Mothername" id="mname" disabled="true" />
				   <%} %>
				   
				</li>
			
				<li class="form-row">
				    
				    <%if(familyVisibility.charAt(2)=='1') {%>
					<input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name"type="text" title="Sibling Name"   name="Brothername" value="${familyvalue.brothername}" id="bname" disabled="true" />
					<%}else{ %>
					<input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name"type="text"  title="Sibling name"  name="Brothername" value="Restricted as per Visibility" id="bname" disabled="true" />
					<%} %>
					
					<%if(familyVisibility.charAt(3)=='1') {%>
					<input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name"  type="text" title="Spouse Name" value="${familyvalue.spousename}" name="Spousename" id="sname" disabled="true" />
					<%}else { %>
					<input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name"  type="text" title="Spouse Name" value="Restricted as per Visibility" name="Spousename" id="sname" disabled="true" />
					<%} %>
										    
				</li>
				
				    
			    <li class="form-row">
			        
			        <%if(familyVisibility.charAt(4)=='1') {%>
			    	<input class="horoscopeicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Horoscopee"  type="text" title="Horoscope" name="HoroscopeInformation" value="${familyvalue.horoscopeInformation}" id="hinfo" disabled="true" />
			    	<%}else {%>
			    	<input class="horoscopeicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Horoscopee"  type="text" title="Horoscope"  name="HoroscopeInformation" value="Restricted as per Visibility" id="hinfo" disabled="true" />
			    	<%} %>
			    	
			    	<%if(familyVisibility.charAt(5)=='1') {%>
			    	<input class="foodprefericons"  style="width:160px;float: left;margin-right: 6px;" placeholder="Food Preferred"  type="text" title="Food Preffered" name="Foodpreferred" value="${familyvalue.foodpreferred}" id="foodpreferred" disabled="true" />	 
			        <%} else {%>
			        <input class="foodprefericons"  style="width:160px;float: left;margin-right: 6px;" placeholder="Food Preferred"  type="text" title="Food Preffered"  name="Foodpreferred" value="Restricted as per Visibility" id="foodpreferred" disabled="true" />
			        <%} %>
			    </li>
		 		 			          
	    </ul>
	
	   <%}else { %>
	
	
	      <ul>
		
				<li class="form-row">
					<input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name"  title="Father Name" value="Upgrade Plan to view" name="Fathername" id="fname" disabled="true" />
					<input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" title="Mother Name" type="text" value="Upgrade Plan to view" name="Mothername" id="mname" disabled="true" />				   	
				</li>
							   
				<li class="form-row">
					<input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name" title="Sibling Name" type="text"  name="Brothername" value="Upgrade Plan to view" id="bname" disabled="true" />
					<input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name" title="Spouse Name" type="text" value="Upgrade Plan to view" name="Spousename" id="sname" disabled="true" />
						    
				</li>
				
			    <li class="form-row">
			    	<input class="horoscopeicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Horoscope" title="Horoscope" type="text"  name="HoroscopeInformation" value="Upgrade Plan to view" id="hinfo" disabled="true" />
			    	<input class="foodprefericons"  style="width:160px;float: left;margin-right: 6px;" placeholder="Food Preferred" title="Food Preferred" type="text"  name="Foodpreferred" value="Upgrade Plan to view" id="foodpreferred" disabled="true" />	 
			    </li>
		 		 			          
	     </ul>
	
	  <%} %>
	  
	</div> 
      
     </div>
	</c:if>
  
   <c:if test="${empty familyvalue}">  
  <h3 align="left">Family Details</h3> 
  <div id="indexinputbutton">
												
	<div id ="familydetailalign">
		 	
	      <ul>
		
				<li class="form-row">
					<input class="fathernameicons" style="width:160px;float: left;margin-right: 6px;" type="text"  placeholder="Father Name" title="Father Name" value="Not Available" name="Fathername" id="fname" disabled="true" />
					<input class="mothernameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Mother Name" title="Mother Name" type="text" value="Not Available" name="Mothername" id="mname" disabled="true" />
				   	
				</li>
							    
				<li class="form-row">
					<input class="siblingnameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Sibling Name" title="Sibling Name" type="text"  name="Brothername" value="Not Available" id="bname" disabled="true" />
					<input class="spousenameicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Spouse Name" title="Spouse Name" type="text" value="Not Available" name="Spousename" id="sname" disabled="true" />
						    
				</li>
				    			   
			    <li class="form-row">
			    	<input class="horoscopeicons" style="width:160px;float: left;margin-right: 6px;"  placeholder="Horoscope" title="Horoscope" type="text"  name="HoroscopeInformation" value="Not Available" id="hinfo" disabled="true" />
			    	<input class="foodprefericons"  style="width:160px;float: left;margin-right: 6px;" placeholder="Food Preferred" title="Food Preferred" type="text"  name="Foodpreferred" value="Not Available" id="foodpreferred" disabled="true" />	 
			    </li>
		 		 			          
	     </ul>

	</div> 
      
     </div>
	</c:if>
	
	<c:if test="${empty socialaccvalue}">
	<h3 align="left">Social Account Details</h3>
		  		  
		  <div id="indexinputbutton">
		  
		  <div id="SocialSuccess" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:#05b7f5;color:#fff;display:none;font-size:16px;">Social Account Saved Successfully!</div>
		  
		  
		 <form  method="post" id="editsocialdetail">
		 
		<div id="socialbuttonsicon">
		
				<ul>
				
						<li id="social-facebook ">
						<input disabled="true" class="facebookicons" style="width:375px;"  type="text" value="Not Available" placeholder="Give your Facebook Url Link"  title="Facebook Url Link" name="Socialwebsitelink"  id="websitelink" />
						</li>
						
						<li id="social-facebook ">
						<input disabled="true" class="linkedinicons" style="width:375px;"  type="text" value="Not Available" placeholder="Give your Linkedin Url Link" title="Linkedin Url Link" name="Socialtwitterlink"  id="twitterlink" />
						</li>
						
						<li id="social-facebook ">
						<input disabled="true" class="twittericons" style="width:375px;"  type="text" value="Not Available" placeholder="Give your twitter Url Link" title="twitter Url Link" name="Sociallinkedlnlink"  id="linkedlnlink" />
						</li>
									
				</ul>
				
		</div> 
		
		
		</form>
		  </div>
	</c:if>
	
	
	
	
	
	
	
        </div> 
	   </div>
	    
        			 <!--end of right div  -->
	</div>

</div>
					
		<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>

		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
								
								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give Us Few Seconds.....
						       </div>
									
									
                      </div>
                      
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->
	
	
	
   <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:300px;margin-left:-99px;						
						color:#05b7f5;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		        <h3>Your session has expired,Please click on the button to relogin !</h3>      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->


 <!-- About Us Div -->
		
		<div id="aboutUslight" class="aboutUsContent">
		<div class="colsebutton" onclick="aboutUsfadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		<div class="popheaderstyle"><h1>About Me</h1></div>
				
			   <div id="aboutUsContent"></div> 	   
			   
		</div>
		
		<div id="aboutUsfade" onclick="aboutUsfadeout()"></div> 	
		
	<!-- EOF About Us Div -->	


	
	
</body>
</html>
     	