<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>
	<tiles:insertAttribute name="title" ignore="true"></tiles:insertAttribute>
</title>
</head>
<body>
<table border="1" cellpadding="1" cellspacing="0" align="center"  background="resources/kyc_images/user_profile_bg.jpg">
    <tr>
        <td height="60" colspan="3" >
        	<tiles:insertAttribute name="header"></tiles:insertAttribute>
        </td>
    </tr>
    <tr>        
         <td height="450" width="100"><tiles:insertAttribute name="menu"></tiles:insertAttribute>
         </td>
        
        <td  width="250"><tiles:insertAttribute name="body"></tiles:insertAttribute>
        <td height="450" width="250"><tiles:insertAttribute name="rightSignInUp"></tiles:insertAttribute> </td>
       
             
    </tr>
    <tr>
        <td height="60"  colspan="3">
        	<tiles:insertAttribute name="footer"></tiles:insertAttribute>
        </td>
    </tr>
</table>
</body>
</html>