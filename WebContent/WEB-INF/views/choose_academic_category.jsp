<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
<script type="text/javascript" src="resources/js/checkbox_jquery.js"></script>

<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>
<script src="resources/kyc_scripts/jquery-ui-10.js"></script>



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>

<script type="text/javascript">
$(document).ready(function(){
	$(".CheckBoxClass").change(function(){
		if($(this).is(":checked")){
			$(this).next("label").addClass("LabelSelected");
		}else{
			$(this).next("label").removeClass("LabelSelected");
		}
	});
	$(".RadioClass").change(function(){
		if($(this).is(":checked")){
			$(".RadioSelected:not(:checked)").removeClass("RadioSelected");
			$(this).next("label").addClass("RadioSelected");
		}
	});	
});
</script>


<script> 
$(document).ready(function(){
  $("#flip").click(function(){
    $("#panel").slideToggle("slow");
  });
});

var checkboxes = $("input[type='checkbox']"),
submitButt = $("input[type='submit']");

checkboxes.click(function() {
submitButt.attr("disabled", !checkboxes.is(":checked"));
});
</script>
 
<style type="text/css"> 
#panel,#flip
{
padding:5px;
text-align:left;
background-color:white;
border:solid 1px #c3c3c3;
width:328px;
}
#panel
{
padding:20px;
display:none;
}
</style>


<script type="text/javascript">
function chooseAcademic() {
	
	var click = [];
	$(".CheckBoxClass:checked").each(function() {
        click.push($(this).val());
    });
	 
	if(click.length==0)
		{
		   alert("Select at least one Document");
		}
	else
		{
	 $.ajax({
			type : "post",				
			url : "add/choosen/academic/category.html",				
			data :"click="+click,	  
			success : function(response) {
								
				 $("#viewUsers1")
					.html(response);
								
			},
			error : function() {
				alert('Error while fetching response');
			}
		});		 		 		 		
	$("#titleBar").html("<h2><span>DOCUMENTS</span></h2>");
}
}
</script>


</head>
<body>

<br><br><br><br><br><br>

<% List<String> ls =(List)request.getAttribute("comp"); 

String oldComp = (String)request.getAttribute("oldSelect");
%>

<div align="center">
<div id="flip" >Choose Type of Academic Document</div>
<div id="panel" >

<!-- <form name="qualifSelector" action="choose_category.html" method="POST" enctype="multipart/form-data"> -->

<% for(int j=0;j<ls.size();j++)
{  	      	
      boolean status = true;
      
      if(oldComp!=null)
      { 
    	  String oldarr[] = oldComp.split(",");
      
      for(int i=0;i<oldarr.length;i++)
      { 
    	  if(ls.get(j).equals(oldarr[i]))
    	  {     		 
    		  status = false;%>    	      		
    	<%  } 
      }   
      
      if(status){ 
%>     
     <input  type="checkbox" name="click" class="CheckBoxClass" value="<%=ls.get(j)%>" id="<%=ls.get(j)%>"/>
     <label id="Label1" for="<%=ls.get(j)%>" class="CheckBoxLabelClass"><%=ls.get(j)%></label>  <br><br>
          
      <%} } else{ %>
      
     <input  type="checkbox" name="click" class="CheckBoxClass" value="<%=ls.get(j)%>" id="<%=ls.get(j)%>"/>
     <label id="Label1" for="<%=ls.get(j)%>" class="CheckBoxLabelClass"><%=ls.get(j)%></label>
     <br><br>
<%}}
%>
<div id="proceedtbutton">
<input type="submit"  width="20" name="Proceed" value="Proceed" onClick="chooseAcademic()"/><br>
</div>

</div>
</div> 
</body>
</html>

