<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<%@ page language="java" import="java.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Admin My Individual</title>

<link href="admin_resources/kyc_css/kyc_admin_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="admin_resources/kyc_css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">

<link rel="stylesheet" href="admin_resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script type="text/javascript"src="admin_resources/kyc_scripts/demo.js"></script>

<link rel="stylesheet" href="admin_resources/css/logout.css">	
	
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />
	
<style>

#sessionfade{

    background-color: #000;
    height: 100%;
    left: 0;
    opacity: 0.7;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1001;
}

#sessionlight{

    background: #fff none repeat scroll 0 0;
    border-radius: 24px;
    height: auto;
    left: 43%;
    margin-left: -211px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: auto;
    z-index: 1002;
    right: 400px !important;

}

.adminleftMenuActive
{
	color:#00b6f5 ! important;
}
</style>	


<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}
</style>
	
	
	
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
$(document).ready(function () {
    $("#menu_wrapper").on("click", "ul.main_menu>li", function (event) {
        debugger;
        event.preventDefault();
        $("#menu_wrapper .active").removeClass("active");
        $(this).find('a:first').addClass("active");
    });
});
});//]]>  



function redirectToLogin()
{
	window.location = "logout_admin.html";	
}


</script>

<script type="text/javascript">
	function viewUsers() {
		
		$("#waitingdivId").show();
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						
						   $.ajax({
								type : "post",
								url : "sendAjax.html",
								cache : false,
								success : function(response) {
									
									$("#waitingdivId").hide();

									var usersTokens = response.split(",");
									var index = 0;
									var usersTableData = "<table style=\"width:840px\"><tr><th>S.NO</th><th>KYC-ID</th><th>FIRST-NAME</th><th>LAST-NAME</th><th>EMAIL-ID</th><th>MOBILE-NUMBER</th></tr>";
									for ( var i = 0; i < usersTokens.length; i = i + 5) {
										
										index += 1;
										usersTableData += "<tr><td>" + (index)
												+ "</td><td>" + usersTokens[0 + i]
												+ "</td><td>" + usersTokens[1 + i]
												+ "</td><td>" + usersTokens[2 + i]
												+ "</td><td>" + usersTokens[3 + i]
												+ "</td><td>" + usersTokens[4 + i]
												+ "</td></tr>";
									}
									usersTableData += "</table>";
									$('#viewUsers').html(usersTableData);
									$("#titleBar").html("<h2><span>View Users</span></h2>");
								},
								error : function() {
									
									$("#waitingdivId").hide();

									alert('Error while request..');
									
								}
							});
					   
					}
				else
					{
					     $("#waitingdivId").hide();

					     $("#sessionlight").show();
					     $("#sessionfade").show();
					}
			}
		});
	
	}
	
	function viewind(UrlLink)
	{
		$("#waitingdivId").show();

		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
					        url: "viewindividual.html",
					        type: 'POST',	       
					        success: function (data) {    
					        	
					        	$("#waitingdivId").hide();

					            $("#viewUsers")
								.html(data);
								$("#titleBar").html("<h2><span>VIEW INDIVIDUAL</span></h2>");
					        },
					        
					    });
					}
				else
					{
					hide
					   $("#sessionlight").show();
				           $("#sessionfade").show();
					}
			}
		});
						 
	}
	
	function organiseind(UrlLink)
	{
		$("#waitingdivId").show();

		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "adminleftMenuActive";
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
					        url: "organiseindividual.html",
					        type: 'POST',	       
					        success: function (data) {    
					        	
					        	$("#waitingdivId").hide();

					            $("#viewUsers")
								.html(data);
								$("#titleBar").html("<h2><span>ORGANISE INDIVIDUAL</span></h2>");
					        },
					        
					    });
					}
				else
					{
					   $("#waitingdivId").hide();

					   $("#sessionlight").show();
				           $("#sessionfade").show();
					}
			}
		});		 
	}
		

	function viewindividualsection()
	{ 
		$("#waitingdivId").show();

		var id=$("#inputString").val();
		var selectedvalue = $("#selectedidindividual").val();
		
		if(id.length>=3)
		{
						
		 $.ajax({  
		     type : "Post",   
		     url : "viewindividualsection.html", 	   	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	      	     	     
		     success : function(response) 
		     {   
		    	 $("#waitingdivId").hide();

		    	 var res=response.length;	    	
		    	 var res1 = response.substring(1, res-1);	    	    	 
		    	 var viewprofile = res1.split(", ");
		    	 var length=viewprofile.length;
		    	
		      if(length>1)
		        { 
					var values = "<table style=\"width:90%;\" align=\"center\" border=\"0\"><tr><th bgcolor=\"#999999\">kycid</th><th bgcolor=\"#999999\">Name</th><th bgcolor=\"#999999\">Emailid</th><th bgcolor=\"#999999\">Mobile number</th><th bgcolor=\"#999999\">Plan Available</th></tr>";
					for ( var i = 0; i < viewprofile .length; i = i + 10) {
					
						if(viewprofile[6 + i]!="No Data")
						{
							values +="<tr>" + "" 				
							+ "<td> "+ viewprofile[0 + i]+"</td><td>" + viewprofile[1 + i]+ "" + viewprofile[2 + i]
									+ "</td><td>" + viewprofile[3 + i]								
									+ "</td><td>" + viewprofile[4 + i]
									+ "</td><td>" + viewprofile[5 + i]								
									+ "</td></tr>";
						}
						else if(viewprofile[6 + i]=="No Data")
						{							
							values +="<tr>" + "" 				
							+ "<td> "+ viewprofile[0 + i]+"</td><td>" + viewprofile[1 + i]+ "" + viewprofile[2 + i]
									+ "</td><td>" + viewprofile[3 + i]									 
									+ "</td><td>" + viewprofile[4 + i]
									+ "</td><td>" + viewprofile[5 + i]									 
									+ "</td></tr>";														
						}
									
					}   
					values += "</table>";
					$('#viewIndDetailsDiv').html(values);					
		     }
		 		 
				 if(length==1)
				 {
					var values = "No data found";
					$("#waitingdivId").hide();
		
					$('#viewIndDetailsDiv').html(values);
				 }
																							
		     },  
		     
		    });  
		}
		if(id.length==0)
		{	
			$("#waitingdivId").hide();
			var values = "No Data Found !";		
			$('#viewIndDetailsDiv').html(values);
		}
		if(id.length<3)
		{	
			$("#waitingdivId").hide();
			var values = "No Data Found !";		
			$('#viewIndDetailsDiv').html(values);
		}
	}
	
	
	function CheckSessionOfAdmin(hitTheUrl)
	{
		$("#waitingdivId").show();

		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				$("#waitingdivId").hide();

				if(response=="Exist")
					{
						window.location = hitTheUrl;
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
					
	}

	function gotoMainPage()
	{
		window.location = "authenticateAdminLogin.html"
	}

</script>

<!-- Scripts for admin Pattern -->

</head>
<body>

	<div class="container">

		<!--header block should go here-->

		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner" style="width:1120px !important">
			
			<div class="logo" onclick="gotoMainPage()">MAIN PAGE</div>
			
			<div class="tfclear"></div>
			<div  class=" mainmenu" >
			<div id="menu_wrapper">
				
				<ul class="main_menu" >
					
					<li></li>
										
					<li id="myMenus1">				
					<a class="myprofile" style="color:#03b2ee" href="#" onclick="CheckSessionOfAdmin('viewindividual.html')"><span></span> My Individual
					</a>
					</li>
					
					<li id="myMenus2">				
					<a class="mydocuments" href="#" onclick="CheckSessionOfAdmin('createUtiliser.html')">	<span></span>  My Utilizer
					</a>
					</li>
					
					<li id="myMenus5"><a class="mystore" href="#" onclick="CheckSessionOfAdmin('createDomain.html')">
							Domain Hierarchy
					</a></li>
					
					<li id="myMenus6" style="width:140px;"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createKycHierarchy.html')">
							Document Hierarchy
					</a>
					</li>
					
					<li id="myMenus3"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createAdminPattern.html')"> 
							My Patterns
					</a></li>
					
					<li id="myMenus41"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createfeature.html')">  My Features
					</a></li>
					
					<li id="myMenus42"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createplan.html')">  My Plans
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('adminNotification.html')">  NOTIFICATION
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('admin_settings.html')">  SETTINGS
					</a></li> 
										
				</ul>
				
             </div>
                                   
			</div>

          
		</div>
   
	</header>


				 <div style="text-align: right; float: right; margin-top: -50px; width: 4%; margin-left: 10px;"> 
	               <a href="logout_admin.html" ><h4 style="color:#05b7f5;">Logout</h4></a>
	             </div>

		<div class="main" ><!-- background-color: #585759; -->
			<div class="main_center">
				<div class="leftside">

					<!--left menus should go here-->
					<div class="user_display">


					 <form>
							<div id="textBox">
							
							        <div id="button">
							        
							            <ul>
							             	
							                <li id="Hi-B-1"><a href="#" id="1stId" class="adminleftMenuActive" class="active_button" onClick="CheckSessionOfAdmin('viewindividual.html')"><span>View Individual</span></a></li>
											<li id="Hi-B-1"><a href="#" id="2stId" onClick="organiseind('organiseindividual.html')"><span>Organize Individual</span></a></li>
										 	
									    </ul>
									    
									</div>
								
							</div>
						</form>

					</div>




				</div>


				<div class="rightside" >

					<!--center body should go here-->
					<div id="wishAdmin">
						<h1>Welcome To KYC Admin</h1>
					</div>
					
					<div class="border_line"></div>
					<div class=" username_bar">
						 <div id="titleBar">
							<h2>
								<span></span>
								View Individual  
							</h2>
						</div>
					</div>
				
					<div class="module-body" >
						<div id="viewUsers" style="height: auto !important;margin-bottom: 15px"> 
    
						 
						 
						 <!-- AdminBody startts Here -->
						 
						 
											<div id="viewindividualpage">
														
														
														<div id="searchicon">
														 
															<div id="profilesearch">                       
																<input type="text" id="inputString" class="profilesearchtextinput2" name="q" size="20" maxlength="80" onkeyup="viewindividualsection()"/>
																<input type='image' alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="viewindividualsection()"/>
														    </div>
														
														</div>
														
														<div class="searchiconselect">
															  		
																	<select id="selectedidindividual">
																			<option value="name">Name</option>
																			<option value="plan">Plan</option>
																			<option value="kycid">Kycid</option>
																	</select>
															  		
														</div>
														
														
											</div>
											
														
														
											<div id="viewIndDetailsDiv" class="datagrid" align="center" style="display: inline ! important;">
										
										
								            </div>
						 
						 
						 
						 <!-- Admin Body Ends Here -->
						 
						</div>

					</div>
					

				</div>
				
			</div>
		</div>
</div>
		
		<!--footer block should go here-->
		
		   <%
			  ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			  String version=resource.getString("kycversion");
			  String versionDate=resource.getString("kyclatestDate");
		    %>
		
		
          <footer class="footer">
		   
		  
				<div class="footer_copyrights">
                <div class="copyrights">    
					 <ul >
                     		
							<li class="footer_space01"><p>Design by:<span> <a href="http://jirehsol.com/">Jireh </a></span></p>
                            <p> � All rights Reserved CLENSEE 2014-15</p>
                             <span>
									<a href="#">Version:<%=version%></a>
									</span>
                            </li>
							<li class="footer_space02"> <p><a href="#">About Us</a> |<a href="#"> Terms and Conditions</a> |<a href="#"> Privacy Policy</a> | <a href="#">Contact Us</a></p></li>
							<li class="footer_space03"> 
                            
                            <div class="four columns">
                                <ul class="social_media">
                                    <li>
                                     <a class="fb" href="https://www.facebook.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="google_plus" href="https://plus.google.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="http://www.linkedin.com/company/1872121?trk=tyah" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li>
                                   
                                    
                                </ul>
                              </div>
                            
                            </li>
						   
						</ul>     
					</div>
                
				</div>
		   
			<div class="footer_line">
			</div>

		 </footer>



<!-- Waiting Div -->
<div id="waitingdivId" >
									    
		    <div id="waitingdivContentId">
				    
				     <div class="kycloader">
		
		             </div>
		             <h4>Please wait For the Portal response...   </h4>
		              				    
		    </div>
	     
</div>
	
<!-- EOF Waiting Div -->




<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#05b7f5;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		        <h3>Your session has expired,Please click on the button to relogin !</h3>      
		         
		         <input type="button" value="Login" onclick="redirectToLogin()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	
	
	<!------container End-------->
</body>
</html>
