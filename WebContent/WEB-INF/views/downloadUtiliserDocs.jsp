<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Download</title>
<%@page import="java.io.File" %><%   
%><%@page import="java.io.*" %><%
%><%@page import="java.util.*" %><% 
%><%@page language="java" trimDirectiveWhitespaces="true"%><%
%><%@page import="java.awt.Image.*" %><%
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>
 
</head>
<body>

<%
//Required file Config for entire Controller 

	ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	String kycMainDirectory = fileResource.getString("fileDirectory");		
	File filePath = new File(kycMainDirectory);

	String year  = (String)request.getAttribute("year");
	String ind_id= (String)request.getAttribute("ind_id");
	String dir = (String)request.getAttribute("dir");
	String docname = (String)request.getAttribute("docname");
	
	
	//End of Required file Config for entire Controller 
	    
		
		if(dir.equals("KYC_DOCUMENT"))
		{		 	 
			    response.setContentType("text/html");  
				PrintWriter out1 = response.getWriter();  
				String filename = docname;   
				String filepath = filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"KYC_DOCUMENT"+"/";   
				response.setContentType("APPLICATION/OCTET-STREAM");   
				response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
				  
				FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
				            
				int i;   
				while ((i=fileInputStream.read()) != -1) {  
				    out1.write(i);   
				}   
				fileInputStream.close();   
				out1.close(); 
			 	 
		}
		    
		if(dir.equals("ACADEMIC_DOC"))
		{		
		        response.setContentType("text/html");  
				PrintWriter out1 = response.getWriter();  
				String filename = docname;   
				String filepath = filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"ACADEMIC_DOC"+"/";   
				response.setContentType("APPLICATION/OCTET-STREAM");   
				response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
				  
				FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
				            
				int i;   
				while ((i=fileInputStream.read()) != -1) {  
				    out1.write(i);   
				}   
				fileInputStream.close();   
				out1.close(); 
					
		}

		if(dir.equals("Financial_DOC"))
		{			
			response.setContentType("text/html");  
			PrintWriter out1 = response.getWriter();  
			String filename = docname;   
			String filepath = filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"Financial_DOC"+"/";   
			response.setContentType("APPLICATION/OCTET-STREAM");   
			response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
			  
			FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
			            
			int i;   
			while ((i=fileInputStream.read()) != -1) {  
			    out1.write(i);   
			}   
			fileInputStream.close();   
			out1.close(); 	
		}

		if(dir.equals("Employement_DOC"))
		{
			
			response.setContentType("text/html");  
			PrintWriter out1 = response.getWriter();  
			String filename = docname;   
			String filepath = filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"Employement_DOC"+"/";   
			response.setContentType("APPLICATION/OCTET-STREAM");   
			response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
			  
			FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
			            
			int i;   
			while ((i=fileInputStream.read()) != -1) {  
			    out1.write(i);   
			}   
			fileInputStream.close();   
			out1.close();
				
		}
		
	%>


</body>
</html>