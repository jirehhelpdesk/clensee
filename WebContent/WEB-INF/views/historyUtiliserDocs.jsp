<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>History</title>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<script type="text/javascript">

window.document.onkeydown = function (e)
{
    if (!e)
    {
        e = event;
    }
    if (e.keyCode == 27)
    {
        lightbox_close();
    }
}

function lightbox_open(){
    window.scrollTo(0,0);
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block'; 
}



</script>	

<style type="text/css">

#fade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#light{
    background: none repeat scroll 0 0 #fff;
    border: 6px solid #ccc;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -321px;
    margin-top: -332px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 409px;
    z-index: 1002;
}


</style>


<script>

function viewDocUti1(docname,kycid,docid)
{
	
	$('#displaySharedDocDiv').empty();
	$('#light').show();
	$('#fade').show();
	
	  $.ajax({
	    	
	    	type: "post",	   
	    	url: "viewDocforUti.html",	    	
	    	data: "docname="+docname+ "&kycid="+kycid,	 	    		       
	        success: function (response) {
	        	
	        	$('#displaySharedDocDiv').html(response);
	        	
	        },
	                
	    });
}


function getHistoryDate(docid)
{
	$.ajax({
    	
    	type: "post",	   
    	url: "getDocDetailsHistoryRecord.html",	    	
    	data: "idIndex="+docid,	 	    		       
        success: function (response) {
        	
        	$("#HitorydateId").html(" as on "+response);
        	
        },
	 });
}
	
function viewDocUti(docname,kycid,docid,docType)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';	       
    
    
	if(docType=="ACADEMIC_DOC")
		{
			 $.ajax({
			    	
			    	type: "post",	   
			    	url: "academicHistorydetailsFromUtilizer.html",	    	
			    	data: "id="+docname+ "&idIndex="+docid,	 	    		       
			        success: function (response) {
			        	
			        	document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';	       
					    
			        	$('#viewUsers1').html(response);
			        	
			        },
			                
			    });
		}
	else if(docType=="Employment_DOC")
		{
			$.ajax({
		    	
		    	type: "post",	   
		    	url: "employeeHistorydetailsFromUtilizer.html",	    	
		    	data: "id="+docname+ "&idIndex="+docid,	 	    		       
		        success: function (response) {
		        	
		        	document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';	       
				    
				   
		        	$('#viewUsers1').html(response);
		        	
		        },
		                
		    });
		}
	else
		{
			$.ajax({
		    	
		    	type: "post",	   
		    	url: "financialHistorydetailsFromUtilizer.html",	    	
		    	data: "id="+docname+ "&idIndex="+docid,	 	    		       
		        success: function (response) {
		        	
		        	document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';	       
				    
		        	$('#viewUsers1').html(response);
		        	
		        },
		                
		    });
		}
	}
	
function downloadDocUti(docname,kycid)
{
	
	$.ajax({
    	
    	type: "post",	   
    	url: "downloadDocforUti.html",	    	
    	data: "docname="+docname+ "&kycid="+kycid,	 	    		       
        success: function (response) {
        	
        	$('#viewUsers').html(response);			
        },
        error : function() {
			alert('Error while fetching response');
		}	        
    });
	
	}
	
	function backToMain(url)
	{		
		window.location = url;
	}

</script>



</head>
<body>
<div id="kycdocumentpage" class="kycdocumentpage" style="margin-left: 0px ! important;">
<%
String kycid = (String)request.getAttribute("kycid");
String date = (String)request.getAttribute("date");
%>
<center><!-- <button onClick="viewDetailsDoc('+kycid+','+date+')">Back to Documents</button> --></center>

		<c:if test="${!empty IndHistorydetails}">
		   
		        <%String documentName = "";%>
					
					<c:forEach items="${IndHistorydetails}" var="det">	
					
								<c:set var="docname" value="${det.doc_name}" />
								
								<%String checkDocumentName = (String)pageContext.getAttribute("docname") + "&"; %>
								
								<%if(checkDocumentName.contains(",")) {%>
								
								      <%String eachdocName = (String)pageContext.getAttribute("docname"); %>
								      <%String eachDocNameArray[] = eachdocName.split(",");%>
								      <%for(int d=0;d<eachDocNameArray.length;d++){ %>
								          
								             <%if(!eachDocNameArray[d].equals("No Document")){%>
								                    
								                     <%documentName += eachDocNameArray[d] + "&";%>	
								                    
								              <%} %>
								      <%} %>
								      
								<%}else{ %>
								
								      <%documentName += checkDocumentName + "&";%>	
								 
								<%} %>
								
					</c:forEach>
					
												
				   <div class="showcodepage" >
				        
				        <ul style="display:none;">
				            <li>
				            <table>
				            <tr>
				            <td>
					            <img src="resources/kyc_images/kyczipicon.jpg" height="50px" width="50px" />
					            </td>
					            
					            <td>
						        
						        <form id="downloadAllFormId" action="downlaodalldocumentsinzip.html" method="post">
						        
							            <input type="hidden" value="indHistoryDocument" name="codeName" />
							            <input type="hidden" name="docnames" value="<%=documentName%>" />
							            <input type="hidden" name="kycId" value="<%=kycid%>" /> 
							            <input type="submit" value="Click here to download all history Document in zip." />
							            
						        </form>	
						        
						        </td>
						        </tr>
						        </table>
					        </li>
					    </ul>
					    
					    <input class="backbuttonindex_histy" type="button" value="Back" onclick="backToMain('<%=(String)session.getAttribute("historyBackUrl")%>')"/>
		
						
					</div>
			
		</c:if>

<c:if test="${!empty IndHistorydetails}">
		
	<table class='CSSTableGenerator' align="left" border="1" cellspacing="1" align="center">
		
		<tr>
		    <th height="10">Serial No</th>			
			<th height="10">Documents Name</th>
			<th height="10">Created Date</th>		
			<th height="10">Document Description</th>
			<th height="10">View Document</th>			
		</tr>
        <%int i=0; %>
		<c:forEach items="${IndHistorydetails}" var="det">					 
			 <tr>
			    <td height="10" width="120"><c:out value="<%=++i%>"/></td>	
			    
			   <c:set var="docNames" value="${det.doc_name}" />
														 		
		 		<%
		 		String documentsNames = (String) pageContext.getAttribute("docNames");	
		 		String documentNameArray[] = documentsNames.split(",");
		 		String modifyDocName = "";
		 		for(int d=0;d<documentNameArray.length;d++)
		 		{
		 			modifyDocName += documentNameArray[d].split("_")[0] + ",";
		 		}
		 		modifyDocName = modifyDocName.substring(0, modifyDocName.length()-1);
		 		%>	
			    
			    <td width="230"><c:out value="<%=modifyDocName%>"/></td>			
				<td width="230"><c:out value="${det.cr_date}"/></td>
				<td width="270"><c:out value="${det.doc_des}"/></td>	
				<td width="270"><a class="utiviewdocument" href="#" onclick="viewDocUti('${det.doc_name}','<%=kycid%>','${det.doc_id}','${det.doc_type}'),getHistoryDate('${det.doc_id}')">View</a></td>											
			 
			 
			 </tr>
			 			
		</c:forEach>	
			
	</table>	
	
</c:if>

<c:if test="${empty IndHistorydetails}">
<center><h1>For this Document No history is there!</h1></center>
</c:if>



<div id="light">
<div class="colsebutton" onclick="lightbox_close()"><img height="22" width="22" src="resources/images/close_button.png"></div>
		   <div id="displaySharedDocDiv"> 
		   
		           
		   </div>
		   
	</div>

	<div id="fade" onClick="lightbox_close();"></div> 
	
	</div>
</body>
</html>