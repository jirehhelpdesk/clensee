
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="resources/error_resources/css/kyceditor.css">
<title>KYC Page Not Found</title>

<script>

function backToHome()
{
	window.location = "getintotheHomePage.html";
}
</script>


</head>
<body class="errorpage bodybg">

  <div class="errorpagediv" >
   
		<div class="error-left">
		<h2>404</h2>
		</div>
		<div class="line"></div>
			<div class="error-right">
			<p>Page Not Found!</p>
			<div class="homebutton">
			<a onclick="backToHome()"> BACK TO HOME PAGE</a>
			</div>
		</div>
    
  </div>

</body>
</html>