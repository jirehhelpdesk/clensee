
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>View Profile</title>

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">

<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>
<script src="resources/kyc_scripts/jquery-ui-10.js"></script>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>


 <div id="center-body-div">
		
	<div  id="panel-1">
	
 <h3 align="left"><b>Basic Details</b>
 </h3> 
         
 <div>
        
         <form>         
          
           <c:if test="${!empty viewBasicProfile}">
              <c:forEach items="${viewBasicProfile}" var="det">	
           
         <table align="center" border="0" class="tabledesign" style="height:250px">
		   
		   <tr>
		   <th>KYC_ID</th><td><input type="text" value="${det.kyc_uti_id}"/></td>
		   </tr>
		   <tr>
		   <th>First Name</th><td><input type="text" value="${det.uti_first_name}"/></td>
		   </tr>
		   <tr>
		   <th>Middle Name</th><td><input type="text" value="${det.uti_middle_name}"/></td>
		   </tr>
		   <tr>
		   <th>Last Name</th><td><input type="text" value="${det.uti_last_name}"/></td>
		   </tr>
		   <tr>
		   <th>Email Id</th><td><input type="text" value="${det.uti_email_id}"/></td>
		   </tr>
		
	     </table>
	     </c:forEach>		
	     </c:if>  
	     
	     
	      <c:if test="${empty viewBasicProfile}">
              
         <table align="center" border="0" class="tabledesign" style="height:250px">
		   
		   <tr>
		   <th>KYC_ID</th><td><input type="text" value=""/></td>
		   </tr>
		   <tr>
		   <th>First Name</th><td><input type="text" value=""/></td>
		   </tr>
		   <tr>
		   <th>Middle Name</th><td><input type="text" value=""/></td>
		   </tr>
		   <tr>
		   <th>Last Name</th><td><input type="text" value=""/></td>
		   </tr>
		   <tr>
		   <th>Email Id</th><td><input type="text" value=""/></td>
		   </tr>
		
	     </table>
	     
	     </c:if>  
	     
         </form> 
         
 </div>
        
        
        
        
  <h3 align="left"><b>Professional Details</b></h3>
       <div >
 
       
       <form>   
       
           <c:if test="${!empty viewBasicProfile}">
           <c:forEach items="${viewBasicProfile}" var="det">	  
	       <table align="left" border="0" class="tabledesign" style="height:50px">		   
		   <tr>
		   <td style="width:50px"><input type="hidden" value="${det.kyc_uti_id}"/></td>		   
		   </tr>
	       </table>
	       </c:forEach>
	       </c:if>
             
           <c:if test="${!empty viewProfessional}">
              <c:forEach items="${viewProfessional}" var="det">	  
              
          <table align="left" border="0" class="tabledesign" style="height:450px">		   		   		  
		   <tr>
		   <th>About us</th><td><textarea style="width: 300px; height:50px">${det.about_utiliser}</textarea></td><!-- style="width: 300px; height:50px" -->
		   </tr>
		   <tr>
		   <th>Company Details</th><td><textarea style="width: 300px; height:50px">${det.about_company}</textarea></td>
		   </tr>
		   <tr>
		   <th>Present Address</th><td><textarea style="width: 300px; height:50px">${det.present_address}</textarea></td>
		   </tr>
		   <tr>
		   <th>Permanent Address</th><td><textarea style="width: 300px; height:50px">${det.permanent_address}</textarea></td>
		   </tr>	
		   	
		   	
	       </table>  
	      
	     </c:forEach>
	     </c:if>	
	     	       	       	       	       	       
	       <c:if test="${empty viewProfessional}">               
              
           <table align="left" border="0" class="tabledesign" style="height:450px">
           		 		   		   		  
		   <tr>
		   <th>About us</th><td><textarea style="width: 300px; height:50px"></textarea></td>
		   </tr>
		   <tr>
		   <th>Company Details</th><td><textarea style="width: 300px; height:50px"></textarea></td>
		   </tr>
		   <tr>
		   <th>Present Address</th><td><textarea style="width: 300px; height:50px"></textarea></td>
		   </tr>
		   <tr>
		   <th>Permanent Address</th><td><textarea style="width: 300px; height:50px"></textarea></td>
		   </tr>	
		   	
	       </table>  
	      	     
	       </c:if>	
	            
           </form> 
                   
           </div>
 
 
 
 </div></div> 
</body>
</html> 