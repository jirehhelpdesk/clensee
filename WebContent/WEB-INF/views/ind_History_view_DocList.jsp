<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ind Doc List</title>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*;"%>

<link rel="stylesheet" href="resources/table/table.css"></link>

<!-- /* Pop up Script and css*/ -->

<script type="text/javascript">

window.document.onkeydown = function (e)
{
    if (!e)
    {
        e = event;
    }
    if (e.keyCode == 27)
    {
        lightbox_close();
    }
}

function lightbox_open(){
    window.scrollTo(0,0);
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block'; 
}

function lightbox_close(){
	
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';
    
}

</script>	

<style type="text/css">

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}
</style>

<!-- /* End of Pop up Script and css*/ -->
<script type="text/javascript">

function viewImage(name,kycid) {
	

	 $.ajax({
				type : "post",
				url : "checksessionforIndividual.html",
				success : function(response) {
					
					 document.getElementById('waitlight').style.display='block';
				      document.getElementById('waitfade').style.display='block';
				       
				       
					if(response=="Exist")
						{
							 $.ajax({  
							     type : "Post",   
							     url : "viewDocuments.html", 
							     data : "docName="+name+ "&kycid="+kycid,	     	     	     
							     success : function(response) 
							     {  
							    	 
							    	 document.getElementById('waitlight').style.display='none';
								     document.getElementById('waitfade').style.display='none';
								       
						 	  	     
							    		$('#kycDocPopUpDisplay').empty();
							    		$('#kycDocPopUplight').show();
							    		$('#kycDocPopUpfade').show();
							    		
							    		$('#kycDocPopUpDisplay').html(response);   	 																				
							     },  
							      
							    }); 
						}
					else
					{
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
					       
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
	
}


function fadeoutPopUp()
{
    document.getElementById('kycDocPopUplight').style.display='none';
    document.getElementById('kycDocPopUpfade').style.display='none';
}

</script>

</head>
<body>

<div id="applyCodeid1" >
<div>
		<table class="CSSTableGenerator" align="center" style="width:98% !important;">
			<tr>
				<th height="10" >Serial No</th>
				<th height="10">Document Name</th>
				<th height="10">View Document</th>				
			</tr>
			<%
				int j = 0;
				String docs = (String) request.getAttribute("documents");
				String documents[] = docs.split("&");
				String kycid = (String) request.getAttribute("foldrid");
				kycid = kycid.replaceAll(" ","");
				
				String own_kyc_id = (String)request.getAttribute("Ind_id");
				
				for (int i = 0; i < documents.length; i++) {
					%>
			 <tr>
			 
				<td height="10" width="70"><c:out value="<%=++j%>" /></td>
				<td width="190"><%=documents[i].split("_")[0]%></td>
				<td  align="center" width="190"><button id="viewDocument" onclick="viewImage('<%=documents[i]%>','<%=own_kyc_id%>');">View </button></td>				
			</tr>
			<%
				}
			 %>
		</table>
		</div>
	</div>
	
	
      <div id="kycDocPopUplight">
    
		
		 <div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		         
		       	
			<div id="kycDocPopUpDisplay" style=" border: 2px solid #03b2ee;
			    border-radius: 5px;
			    color: #555;
			    margin: 0 auto;
			    padding: 0;
			    text-align: left;
			    width: auto;"> 
					              
					       
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
	

</body>
</html>