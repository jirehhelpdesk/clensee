<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<link href="resources/css/kyc_admin_style.css" rel="stylesheet" type="text/css" />

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%>
<%@page isELIgnored="false"%> 


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Ind Notification</title>


<script type="text/javascript">

function viewindividualsection(){ 
	
	$("#waitingdivId").show();
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var id=$("#inputString").val();
						var selectedvalue = $("#selectedidindividual").val();
						
						if(id.length>0)
						{
						 $.ajax({  
						     type : "Post",   
						     url : "viewindividualsection.html", 	   	  
						     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	      	     	     
						     success : function(response) 
						     {   
						    	 $("#waitingdivId").hide();
						        	
						    	 var res=response.length;	    	
						    	 var res1 = response.substring(1, res-1);	    	    	 
						    	 var viewprofile = res1.split(", ");
						    	 var length=viewprofile.length;
						    	
						    	 if(length>1)
						        { 
									var values = "<table align=\"center\" border=\"0\"><tr><th bgcolor=\"#999999\">Profile Pic</th><th bgcolor=\"#999999\">kycid</th><th bgcolor=\"#999999\">Name</th><th bgcolor=\"#999999\">Emailid</th><th bgcolor=\"#999999\">Mobile number</th><th bgcolor=\"#999999\">Plan Available</th><th bgcolor=\"#999999\">Send Notification</th></tr>";
									for ( var i = 0; i < viewprofile .length; i = i + 10) {
									
										if(viewprofile[6 + i]!="No Data")
										{
										values +="<tr><td>" + "<img src=\"${pageContext.request.contextPath}<%="/KYC_DOC_REPOSITORY"+"/"%>"+viewprofile[9 + i]+"/"+viewprofile[8 + i]+"/Profile_Photo/"+viewprofile[6 + i]+"\"   width=\"40\" height=\"45\" /></img>" 				
										+ "</td><td> "+ viewprofile[0 + i]+"</td><td>" + viewprofile[1 + i]+ "" + viewprofile[2 + i]
												+ "</td><td>" + viewprofile[3 + i]
												
												+ "</td><td>" + viewprofile[4 + i]
												+ "</td><td>" + viewprofile[5 + i]
												+ "</td><td>" + "<input type=\"button\" value=\"Notify\" id=\""+viewprofile[8 + i]+","+ viewprofile[1 + i]+" "+viewprofile[2 + i]+"\" onclick=\"specificIndNotify(this.id)\" />"
												+ "</td></tr>";
										}
										else if(viewprofile[6 + i]=="No Data")
										{
											
											values +="<tr><td>" + "<img src=\"resources/images/kyc_inner_cons_name.png\" width=\"40\" height=\"45\" /></img>" 				
											+ "</td><td> "+ viewprofile[0 + i]+"</td><td>" + viewprofile[1 + i]+ "" + viewprofile[2 + i]
													+ "</td><td>" + viewprofile[3 + i]
													
													+ "</td><td>" + viewprofile[4 + i]
													+ "</td><td>" + viewprofile[5 + i]
													+ "</td><td>" + "<input type=\"button\" value=\"Notify\" id=\""+viewprofile[8 + i]+","+ viewprofile[1 + i]+" "+viewprofile[2 + i]+"\" onclick=\"specificIndNotify(this.id)\" />"
													+ "</td></tr>";
																	
										}
													
									}   
									values += "</table>";
									$('#applyCodeid').html(values);
									
						     }
						 
						 
						 if(length==1)
						 {
							var values = "No data found";
							$('#displayvalues').html(values);
						 }
																									
						     },  
						     
						    });  
						}
						if(id.length==0)
						{	
							$("#waitingdivId").hide();
				        	
							var values = " ";
							$('#displayvalues').html(values);
						}
				}
			else
				{
				   $("#waitingdivId").hide();
	        	
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	});
	
	
	
}


function searchIndForNotify()
{
	$("#notifydivId").hide();				
	$("#notifyspecificIndId").show();
}

function specificIndNotify(idValue)
{
	var notifyDetails = idValue.split(",");
	document.getElementById("indToValue").value = notifyDetails[0];	
	document.getElementById("indToNameValue").value = notifyDetails[1];	
	
	$("#notifydivId").show();
}

function allIndNotify()
{
   $("#notifyspecificIndId").hide(); 
   
   document.getElementById("indToValue").value = "All Individual";
   document.getElementById("indToNameValue").value = "All Individual";
   
   $("#notifydivId").show();
   
}

function notifyToIndividual()
{
	$("#waitingdivId").show();
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
					var formData = new FormData($("#notifyForm")[0]);
					
				    $.ajax({
				        url: "notifyToIndividual.html",
				        type: 'POST',
				        data: formData,
				        //async: false,
				        success: function (data) {          
				            
				        	$("#waitingdivId").hide();
				        	
				        	if(data=="success")
				        		{
				        			alert("Admin Notification has sent successfully.");
				        		}
				        	else
				        		{
				        			alert("Admin Notification has failed,try again !");
				        		}
				        	
				        	
				        },
				        cache: false,
				        contentType: false,
				        processData: false
				    });	
				}
			else
				{
				   $("#waitingdivId").hide();
	        	
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	});
	
}


</script>




</head>



<body>
      <div id="notificationpage">
           <input type="button" value="Notify Specific Individual" onclick="searchIndForNotify()"/>
           <input type="button" value="Notify All Individual" onclick="allIndNotify()"/>
      </div>

	  <div id="notifyspecificIndId" >
			<table>
				<tr>
					
					<td>
						<div id="searchicon">
							<div id="profilesearch">
								<input type="text" id="inputString" class="profilesearchtextinput2" name="q" size="20" maxlength="80" value="" /> 
								
								<input type='image' alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="viewindividualsection()" />
							</div>
						</div>
					</td>
					
					<td><select id="selectedidindividual">
							<option value="name">Name</option>
							<option value="plan">Plan</option>
							<option value="kycid">Kycid</option>
					</select></td>
	
				</tr>
			</table>
			
			
			<div id="applyCodeid" class="datagrid" style="margin-left:-45px;px;margin-top:16px;margin-right:40px;">
		
            </div>
	 </div>
    
     <div id="notifydivId" style="display:none;margin-left:74px;margin-top:32px;width:725px;background-color: #fff;border: 2px solid #00b6f5;border-radius: 14px;">
	       
	       <form name="notifyForm" id="notifyForm" >
	       
			      <ul style="text-align:left;width:665px;">
			      
					       <li>
					              <input type="hidden"  value="Individual"   name="notification_type" />
					              <input type="hidden" id="indToValue" value="" name="notification_to" readonly/>
					           To:<input type="text" id="indToNameValue" value=""  readonly/>
					       </li>
					       
					       <li>    
					           Notification Subject:
					           <input type="text" name="notification_subject" maxlength="90" style="width:652px;height:20px;"/>
					       </li>
					       
					       <li>    			           
					           Notification Message:
					           <textarea name="notification_message" style="width:652px;height:60px;">
					           
					           </textarea>
			               </li>	
			               
			               <li>    			           
					            <input type="button" value="Notify" style="align:right;" onclick="notifyToIndividual()"/>
			               </li>               
	              </ul> 
	            
	       </form>
	       	        
	 </div>

</body>
</html>