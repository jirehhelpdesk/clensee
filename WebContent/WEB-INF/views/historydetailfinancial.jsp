<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript">

function editFinancial()
{	
	var flag = 'true';
	
	var filedetails = document.getElementById("financialfileId").value;
	
	if(filedetails=="")
		{
		    flag='false';
		    alert("Select a file !");
		}
	
	if(filedetails!="")
	{
		
		    var ext=filedetails.substring(filedetails.lastIndexOf('.')+1);
		    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf");						    
		    var condition = "NotGranted";
		   
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}
			if(condition=="NotGranted")
				{
				   flag = 'false';
				   alert("pdf/office/image files are only allowed!");
				}
			
	}
		
	 var fileDetails1 = document.getElementById("financialfileId");
	 var fileSize = fileDetails1.files[0];
	 var fileSizeinBytes = fileSize.size;
	 var sizeinKB = +fileSizeinBytes / 1024;
	 var sizeinMB = +sizeinKB / 1024;
	 
	 if(sizeinMB>2)
		 {
		 	flag = 'false';
		    alert("File size should not more then 2 MB !");
		 }
	 
	if(flag=='true')
		{
		 		 			
			 $.ajax({
					type : "post",
					url : "checksessionforIndividual.html",
					success : function(response) {
						
						if(response=="Exist")
							{
							 		
										$.ajax
									    ({
											type : "post",				
											url : "validateFileStorage.html",	
											data: "givenfileSize="+sizeinKB,
											success : function(response) {
												
												if(response=="withinStorage")
													{
													
													 document.getElementById('waitlight').style.display='block';
											    	 document.getElementById('waitfade').style.display='block';
											    	
			
													   var formData = new FormData($("#FinancialEditForm")[0]);
												    	
														formData.append("fromYear",document.getElementById("fyid").value);
														formData.append("toYear",document.getElementById("tyid").value);
															
															$.ajax({
																type : "POST",
																url : "financialDocument.html",
																data : formData,
																//async: false,
																success : function(response) {
													               
																	document.getElementById('waitlight').style.display='none';
															    	document.getElementById('waitfade').style.display='none';
															    	
																	alert("Financial Document added Successfully .");
																	window.location = "search_financialDoc.html";
													           
																},
																cache: false,
														        contentType: false,
														        processData: false,
																error : function() {
																	alert('Error while fetching response');
																}
															});
															$("#titleBar").html("<h2><span>FINANCIAL DOCUMENT</span></h2>");
													}
												else
													{
													      alert("Failed due to storage limitation,to Upload more document migrate your Plan!");
													}
											},
											
										});	
									 	
										
							}
						else
							{
							   $("#sessionlight").show();
						       $("#sessionfade").show();
							}
					}
				}); 
					
		}	
}

function cancelForm()
{
	$("#viewDiv").show();
	$("#formDiv").hide();
	$("#viewFinDoc").show();
	$("#viewBrowseDoc").hide();
	$("#editId").show();
	$("#fileChangeId").hide();
}

function showBrowse(id)
{
	if(id=="upload")
		{
			$("#viewFinDoc").hide();
			$("#viewBrowseDoc").show();
		}
	else
		{
			$("#viewFinDoc").show();
			$("#viewBrowseDoc").hide();
		}
	
}

</script>


</head>
<body >
         <% String docsData = (String)request.getAttribute("docsData");
         
            int indId = (Integer)request.getAttribute("indId");
            
           String finHierarchy =  (String)request.getAttribute("finHierarchy");
           
           String heirarchyArray[] = finHierarchy.split(",");
           
           String fromYear = docsData.split("&&")[0].split("-")[0];
           String toYear = docsData.split("&&")[0].split("-")[1];
         %>
         
          <div style="height: 35px;margin-bottom: 6px; margin-right: 29px; margin-top: 10px;">
             <input type="button" onclick="myDocuments2('search_financialDoc.html')" value="Back" class="backbuttonindex_histy">
          </div>
         
         <div class="FinancialEditForm">
          
                
          
          <form name="FinancialEditForm" id="FinancialEditForm"  enctype="multipart/form-data">                     
                              
       
        <div id="viewDiv" class="viewdiv">
        
             <table>
                 <tr>
                 <td style="width:auto;"><b>Financial Year</b></td>
		         <td style="width:10px;">:</td>
		         <td style="width:auto;"> <%=fromYear%>-<%=toYear%></td>		
		         <input type="hidden" id="fyid" name="fromYear" value="<%=fromYear%>"/>
                 <input type="hidden" id="tyid" name="toYear" value="<%=toYear%>" />             		            		           
		         </tr>
		      
		         <tr>
		         <td style="width:auto;"><b>Financial Document Type</b></td>
		          <td style="width:10px;">:</td>
		          <td style="width:auto;"><%=docsData.split("&&")[1]%>
		          </td><input type="hidden" id="finDocType" name="docType" value="<%=docsData.split("&&")[1]%>"/>
		          </tr> 
		     
		          <tr >
		          <td style="width:auto;">
		          <b>Document Mode</b></td> 
		          <td style="width:10px;">:</td>
		          <td style="width:auto;">
		          <%=docsData.split("&&")[2]%>
		          </td>
		          <input type="hidden" id="finDocMode" name="docMode" value="<%=docsData.split("&&")[2]%>"/>
		          </tr> 		
           		  <tr >
           		  <td style="width:auto;"><b>Added on:</b> </td>
	                      <td style="width:10px;">:</td>
	                      <td style="width:auto;">
	                      <%=docsData.split("&&")[3]%></td>
	              </tr>			             		  
              </table>
              </div>
             
               
             
			      
		       <div id="viewBrowseDoc" class="viewBrowseDoc" style=" display:none;
    float: right;
    margin-right: 10px;
    margin-top: 10px;
    width: 254px;"> 	
    		          
    		          <ul class="panviewinner_acad boxclassbg"><li >	
    		          		     <b>Financial Document</b>          
		                <input type="file" name="upl" id="financialfileId"/>
		              
		                <a  class="cancelbule_button_fin" href="#" id="cancel" onclick="showBrowse(this.id)"></a>
		                <a  class="saveinnerblue_button" href="#"  onclick="editFinancial()"></a>
		                <div id="fileDiv" class="error" style="display:none;margin-left: 283px;margin-top: -22px;"></div>
		               
		               </li></ul> 			       
		       </div>  	
			       		       
		
			   
					<div id="viewFinDoc" class="viewFinDoc">
		                  		
		                  		<ul><li class="panviewinner_acad">
		                  		
		                  			 <b>Financial Document</b>
				                     
				                     <%if(!docsData.split("&&")[4].contains("jpeg") && !docsData.split("&&")[4].contains("jpg") && !docsData.split("&&")[4].contains("gif") && !docsData.split("&&")[4].contains("png")) { %>
																																											             
					                            <%if(docsData.split("&&")[4].contains("pdf")) {%>
																																												                        
						                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
						                            		
						                        <%}else if(docsData.split("&&")[4].contains("doc")) {%>
						                        
						                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
						                        
						                        <%}else if(docsData.split("&&")[4].contains("docx")) {%>
						                        
						                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
						                        
						                        <%}else if(docsData.split("&&")[4].contains("xls")) {%>
						                        
						                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
						                         
						                        <%}else { %>
					                        
					                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
					                        
					                           <%} %>																																								    		
					                          <br>
					                           <a class="mydocumentdownloadpan" href="downloadfinancial.html?fileName=<%=docsData.split("&&")[4]%>"> </a>
					                           <a class="editacod_button"  id="upload" onclick="showBrowse(this.id)"> </a> 
					                           
					                <%}else{ %>
				           				                     
						                     <%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+docsData.split("&&")[4]+"&docCategory=Financial_DOC"%>"   width="200" height="150" /></img> --%>
						                     <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+docsData.split("&&")[4]+"&docCategory=Financial_DOC"%>"   width="200" height="150" /></img>    			                
							                 <br>					          
							                 <a class="mydocumentviewpan" onclick="viewDocumentInPopUp('<%=docsData.split("&&")[4]%>')"> </a>
							                 <a class="mydocumentdownloadpan" href="downloadfinancial.html?fileName=<%=docsData.split("&&")[4]%>"> </a> 
							                 <a  class="editacod_button" id="upload" onclick="showBrowse(this.id)"> </a>  
					                 
					                 <%}%>	    
					                 
					                 </li></ul>	     
					 </div>
			   
			  </form>   
			     </div> 
			    
			 
			                
      
                       
</body>
</html>