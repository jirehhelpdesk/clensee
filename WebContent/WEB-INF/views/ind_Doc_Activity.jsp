<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Document Activity</title>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>


</head>
<body>
<c:if test="${!empty indDOC_Activity}">
		
	<table class='CSSTableGenerator' align="left" border="1" cellspacing="1" align="center">
		<tr>
		    <th height="10">SERIAL NO</th>			
			<th height="10">Ind-KYCID</th>
			<th height="10">TYPE OF DOC</th>
			<th height="10">DATE</th>
			<th height="10">DOC NAME</th>
		</tr>
        <%int i=0; %>
		<c:forEach items="${indDOC_Activity}" var="det">					 
			 <tr>
			    <td height="10" width="120"><c:out value="<%=++i%>"/></td>				
				<td width="230"><c:out value="${det.kycid}"/></td>
				<td width="270"><c:out value="${det.typeofdoc}"/></td>	
				<td width="270"><c:out value="${det.date}"/></td>	
				<td width="270"><c:out value="${det.docname}"/></td>			
				<td align="center" width="210"><a href="#" ><button id="viewhistory">View</button></a></td>
			 </tr>			
		</c:forEach>		
	</table>	
</c:if>
</body>
</html>