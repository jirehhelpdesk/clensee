<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>  
<head>  

<link rel="stylesheet" href="jQueryDialog/jquery-ui.css">
<script src="jQueryDialog/jquery-1.10.2.js"></script>
<script src="jQueryDialog/jquery-ui.js"></script>
<link rel="stylesheet" href="resources/kyc_css/dialog.css"></link>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">

  <script>
   $(function() {
		$("#viewProfile").dialog({
			autoOpen : false,
			
		});
	}); 
   	
</script>	

<!-- /* Pop up Script and css*/ -->

<script type="text/javascript">

window.document.onkeydown = function (e)
{
    if (!e)
    {
        e = event;
    }
    if (e.keyCode == 27)
    {
        lightbox_close();
    }
}

function lightbox_open(){
    window.scrollTo(0,0);
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block'; 
}

function lightbox_close(){
	
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';
    $("#successcode").hide();
    
}

</script>	

<style type="text/css">



</style>

<!-- /* End of Pop up Script and css*/ -->
	
<script type="text/javascript">

function showData(value){ 
      	
	 $.ajax({  
	     type : "Post",   
	     url : "search1_profile.html",   
	     data : "doctype=" +value+ "&searchby=" +document.getElementById('searchType').value ,
	     success : function(response)
	     {    
	    	 var obj = response;		
			 $('#mydiv').empty();
			 $('#searchDetails').empty();
			 var arr = obj.split(",");
			 for(var i=0;i<arr.length;i++)
				 {				 
			          $('#mydiv').append("<h>"+arr[i]+"</h1><br>"); 				         
				 }							 
	          },  
	     error : function(e) {  
	      alert('Error: ' + e);
	     }  
	    });  
}
</script>
  
<script type="text/javascript">

function viewPro(id){ 
	
	 $.ajax({  
	     type : "Post",   
	     url : "viewProfileInGC.html", 
	     data : "kycid="+id,	     	     	     
	     success : function(output) 
	     {   	    	
	    	 alert("Hello "+output);
	    	 var data = output.split(",");
	    	 $('#viewProfile').empty();
	    	 for(var i=0;i<data.length;i++)
	    		 {    		    
	    		    $('#viewProfile').append(data[i]+"<br><br>");
	    		 }	    	 	    	 	    	 
	     },  
	     error : function(e) {  
	      alert('Error: ' + e);
	     }  
	    });  
}

function shareDoc(urlLink)
{
	
	$.ajax
    ({
		type : "post",				
		url : urlLink,						
		success : function(response) {
									
			 $("#viewProfile")
				.html(response);
							
		},
		error : function() {
			alert('Error while fetching response');
		}
	});		 		 		 		
         
	}

function gen_rec_details() {
	
		var id1 = $("#searchType").val();
		var id2 = $("#name").val();	
	
	if(id2.length<=3)
		{
		   $('#searchDetails').hide();
		}		
	else
		{   	  
		
		      $.ajax
			          ({
						type : "post",				
						url : "generateRecipient.html",			
						data :"searchby="+id1+"&name="+id2 ,	  
						success : function(response) {
							
								$('#searchDetails').empty();
								$('#searchDetails').show();
								$('#searchDetails').html(response); 	
						
							},																	
						error : function() {
							
							   $('#searchDetails').empty();
							   $('#searchDetails').show();
							   $('#searchDetails').html("<h>As per Search criteria No Data Found !</h>"); 
						}
					});		
				 
				   $("#titleBar").html("<h2><span>Individual Search Result</span></h2>");
   }
}  


function forKycUser()
{
	$("#nonKycUserDiv").hide();
	$("#searchDetails").show();
	document.getElementById("generatecodesearch_icon").style.display = "block";	
}

function fornonKycUser()
{
	$("#generatecodesearch_icon").hide();
	$("#searchDetails").hide();
		   
	document.getElementById("kycnameId").value = "";
	document.getElementById("kycuserId").value = "";
	document.getElementById("kycPhnoId").value = "";
	document.getElementById("CommentshareId").value = "";
	
	$("#nonKycName").hide();
	
   	$("#nonKycUserDiv").show();
   	$("#titleBar").html("<h2><span>Share Generated Code</span></h2>");
}

function shareNonKycOnKeyUp()
{  
	var name = /^[a-zA-Z ]+$/;	
	var num = /^[0-9]+$/;	
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
		
	var SharedName = document.getElementById("kycnameId").value;
	var SharedEmailId = document.getElementById("kycuserId").value;
	var SharedPhno = document.getElementById("kycPhnoId").value;
		  	
	if(SharedName=="")
		{
		   $("#nonKycName").hide();
		}
	
	if(SharedName!="")
	{
		if(!SharedName.match(name))
		{
			$("#nonKycName").show();
			$("#nonKycName").html("Enter Only Alphabet for Name !");
			return false;
		}
		else
		{
			$("#nonKycName").hide();	
		}
	}
	
	if(SharedEmailId=="")
	{
	   $("#nonKycName").hide();
	}
	
	if(SharedEmailId!="")
	{
		$("#nonKycName").hide();												
	}
					
	if(SharedPhno!="")
	{
		if(!SharedPhno.match(num))
		{
			$("#nonKycName").show();
			$("#nonKycName").html("Enter Valid Phone no. !");
			 return false;
		}
		else
		{
			$("#nonKycName").hide();	
		}
	}
	
	if(SharedPhno=="")
	{
		$("#nonKycName").hide();	
	}
}

function shareNonKyc()
{	
	var flag='true';  
	var name = /^[a-zA-Z ]+$/;	
	var num = /^[0-9]{10}$/;	
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
		
	var SharedName = document.getElementById("kycnameId").value;
	var SharedEmailId = document.getElementById("kycuserId").value;
	var SharedPhno = document.getElementById("kycPhnoId").value;
		  	
	if(SharedName=="")
		{
		   flag='false';
		   $("#nonKycName").show();
		   $("#nonKycName").html("Enter your Sender Name!");
		   return false;
		}
	if(SharedName!="")
	{
		if(!SharedName.match(name))
		{
			flag='false';
			$("#nonKycName").show();
			$("#nonKycName").html("Enter Only Alphabet for Name !");
			return false;
		}
		else
		{
			$("#nonKycName").hide();	
		}
	}
	
	if(SharedEmailId=="")
	{
	   flag='false';
	   $("#nonKycName").show();
	   $("#nonKycName").html("Enter your Sender EmailId!");
	   return false;
	}
	
	if(SharedEmailId!="")
	{
		if(!SharedEmailId.match(emailId))
		{
			flag='false';
			$("#nonKycName").show();
			$("#nonKycName").html("Enter a Valid EmailId !");	
			 return false;
		}
		else
		{
			$("#nonKycName").hide();												
		}
	}
					
	if(SharedPhno!="")
	{
		if(!SharedPhno.match(num))
		{
			flag='false';
			$("#nonKycName").show();
			$("#nonKycName").html("Enter a Valid Phone no. !");
			 return false;
		}
		else
		{
			$("#nonKycName").hide();	
		}
	}
	
	if(flag=='true')
	{
		    document.getElementById('waitlight').style.display='block';
	        document.getElementById('waitfade').style.display='block';
	        $("#waitdisplaySharedDocDiv").html("<img src=\"resources/ajax_loaderimage/registerloading.gif\" style=\"margin-left:121px;height:50px;width:50px;\"></img><h4>Processing please wait for portal response...... </h4>");
	        
	        
		$.ajax
	    ({
			type : "post",				
			url : "validate/nonkyc/user/emailId.html",	
			data : "SharedEmailId="+SharedEmailId,
			success : function(response) { 
				
					if(response=="Exist")
						{
						
						document.getElementById('waitlight').style.display='none';
				        document.getElementById('waitfade').style.display='none';
				        
							$("#nonKycName").show();
							$("#nonKycName").html("Given EmailId is already Existed User !");	
							 return false;
						}
					
					else
						
						{					
							var fd = $('#nonKycUserFormId').serialize();						 
							$.ajax
						    ({
								type : "post",				
								url : "sendNonKycUser.html",	
								data : fd,
								success : function(response) { 
									
									if(response=="success")
										{	
										
											document.getElementById('waitlight').style.display='none';
								       		document.getElementById('waitfade').style.display='none';
								        								        
										    alert("Your Code has Sucessfully Sent .");										    										    
										    fornonKycUser();
									    }
									else
										{
											document.getElementById('waitlight').style.display='none';
									        document.getElementById('waitfade').style.display='none';
									        
										    alert("Server Failed please Try Again!");
										}
								},
								error : function() 
								{
									alert('Server Problem ,Try Again Later!');
								}
							});		 	
						}
			 },				
		});	
										
	}
	
}

function closePopUp()
{	
	$("#successabc").hide();
}

function div_show(kycid)
{ 
	
	$.ajax
    ({
		type : "post",				
		url : "sendingPattern.html",	
		data: "kycid="+kycid,
		success : function(response) {
			
			 window.scrollTo(0,0);
			 
			 document.getElementById('light').style.display='block';
			 document.getElementById('fade').style.display='block'; 
			    
			var data = response.split("$");
			
			document.getElementById("sshareId").value="";
			
			$("#kycUserNameDiv").html("TO:"+data[3]);
			$("#SuccessUserNameDiv").html(data[3]);
			$("#kycUserIdDiv").html(data[1]);
			
			document.getElementById("senderId").value=data[0];
			document.getElementById("reciverId").value=data[1];
					  			  			 
		},
		error : function() {
			alert('Error while fetching response');
		}
	});	
		
}
	
	
function sendTo(urlLink)
{	
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    $("#waitdisplaySharedDocDiv").html("<img src=\"resources/ajax_loaderimage/registerloading.gif\" style=\"margin-left:121px;height:50px;width:50px;\"></img><h4>Processing please wait for portal response...... </h4>");
    
    $("#light").hide();
    
	$.ajax
    ({
		type : "post",				
		url : "validategenerateCode.html",	
		success : function(response) { 
			
			if(response=="withinLimit")
				{				 
					$.ajax
				    ({
						type : "post",				
						url : urlLink,	
						data : fd,
						success : function(response) { 
							
							if(response=="success")
								{	
								
								document.getElementById('waitlight').style.display='none';
							    document.getElementById('waitfade').style.display='none';
							    								   
								    $("#successcode").show();
							    }
							else
								{
								    alert("Server Failed please Try Again!");
								}
						},						
					});	
				}
			else
				{
				    alert("Generating code has beyound limit as per plan,to generate more code migrate your Plan !");
				}
		},
    })
	
	var fd = $('#sendForm').serialize();
	
	
	

}	
</script>  
  
<link rel="stylesheet"  href="resources/popUp/css/elements.css" />
<script src="resources/popUp/js/my_js.js"></script>	
  

</head>  
<body>  
 
		 <div id="chooseButtonDiv">
		 
		 <div id="revoke_tbutton">
		 <input type="button" value="Share Code to KYC User" onclick="forKycUser()"/>  <input type="button" value="Share Code to Non-KYC User" onclick="fornonKycUser()"/> </div>
		 
		 </div>
		 
   
			<div id="generatecodesearch_icon" style="display:none;">
						
			<div id="generatecodesearch">
			<input  type="text" class="generatecodesearch" name="name" id="name" onkeyup="gen_rec_details()">
			<input type="image" onClick="gen_rec_details()" src="resources/images/home_search_img.png">
			</div>
			
			<select name="searchby" id="searchType">
			<!-- <option value="KYC_ID">KYC_ID</option> -->
			<option value="Email_id">Email_id</option>
			<option value="Name">Name</option>
			</select>
			
			</div>


				     <div id="searchDetails"> </div>


			<div id="viewProfile"></div>

			<input type="hidden" name="pattern2" value="<%=request.getAttribute("pattern")%>"/>
			
			<%session.setAttribute("codepattern", (String)request.getAttribute("pattern"));%>
						
			
			
			
			<!-- Division for Non Kyc User -->
			
			
			
			<div id="nonKycUserDiv" style="display:none;">		
				
			<div class="nonkycuser">
			<h1 align="left">Send this code to a Non-KYC User:</h1>  <br>
			  <form id="nonKycUserFormId" name="nonKycUserForm" action="sendNonKycUser.html" method="post">
			  
			  <ul>
			       
			  		<li style="float:left;">
			  		     Name<span>*</span>:<input type="text" name="nonKycName" id="kycnameId" style="width:150px;" onkeyup="shareNonKycOnKeyUp()"/><div id="nonKycName" class="error" style="display:none;margin-left:280px;margin-top: -77px"></div>&nbsp; &nbsp;Email Id<span>*</span> :<input type="text" name="nonKycUser" id="kycuserId" style="width:150px;" onkeyup="shareNonKycOnKeyUp()"/> Mobile Nos<span></span>:<input type="text" name="nonKycPhno" id="kycPhnoId" style="width:150px;" onkeyup="shareNonKycOnKeyUp()"/>		  		
			  		     <input type="hidden" name="shareSubject" id="shareId" value="Sharing KYC Documents !" />			  		     
			  		</li>
			  			  		   		  		
			  		<li>
			  		     <textarea name="shareComment" id="CommentshareId" placeholder="Sharing Comment :" style="">
			  		     
			  		     </textarea>
			  		     
			  		     <%String code = (String)request.getAttribute("pattern");
                          String codeary[] = code.split(","); %>                                    
			  		     
			  		     <li>
			  		     <div id="sharecodeId" >
				  		    
				  		     <ul style="text-align:left;">
				  		     
					  		     <%for(int i=0;i<codeary.length;i++){%>
					  		     
					  		         <li><%=codeary[i].split("_")[0]%></li>
					  		         
					  		     <%}%>
					  		     
				  		     </ul>
				  		     
			  		     </div>	
			  		     </li>	
			  		     
			  		</li>
			  		
			  		
			  		
			  </ul>
			  <div class="sharingbuttonclass">
			  		
			  		     <input type="hidden" value="<%=request.getAttribute("pattern")%>" name="sahredCode"/>
 			  		     <input type="button" value="Share"  style="margin-left:-104px;margin-top:10px;" onclick="shareNonKyc()" />
 			  		     
			  		</div>
			  </form>
			  
			</div>			
			</div>
            <!-- End of Division for Non Kyc User -->
            
                    
<div id="light">
<div class="colsebutton" onclick="lightbox_close()"><img height="22" width="22" src="resources/images/close_button.png"></div>
<form id="sendForm" name="patternvalue" action="codeDetails.html" method="post">
							
							<div>
							<h1>Sending Code:</h1>
							<ul>	
							
							<li>			
							<div id="kycUserPicDiv">
							
							<img src="resources/image"  style="width:60px;height:60px;"></img>
							
							</div>	
							
							<div id="kycUserNameDiv"></div>									
							
							<input readonly type="hidden" name="sender" value="" id="senderId"/>
							<input readonly type="hidden" name="reciver" 	value="" id="reciverId"/>
							
 							</li>
							
							<li >					
							     <textarea name="des" id="sshareId" placeholder="Sharing Comments" ></textarea>
							</li>
							
							<li>						
							<%
							String code1 = (String)request.getAttribute("pattern");
                            String codeary1[] = code.split(","); %>                                           
			  		       
			  		        <div id="sharecodeId-ext" >
					  		        <ul style="text-align:left;">
					  		        
					  		        <%for(int i=0;i<codeary1.length;i++){%>
					  		       		 <li><%=codeary1[i].split("_")[0]%></li>
					  		        <%}%>
					  		        
					  		        </ul>
					  		        
			  		        </div>			      
			  		     
			  		        <input type="hidden" value="<%=request.getAttribute("pattern")%>"  name="codePattern"/>
							</li>	
										
							<li id="sharecodeIdbutton">
							    <input type="button" value="Send" onclick="sendTo('kyc/user/codeDetails.html')" />
							    <input type="button" value="Cancel" onClick="lightbox_close();" />							
							</li>
							
							</ul>		
							</div>	
							
						</form>


</div>

<div id="fade" onClick="lightbox_close();"  ></div> 	

               
                  <div id="successcode" style="display:none;"> 
												      
						      <h1>Your Code has Sent Successfully to</h1> <div id="SuccessUserNameDiv"></div><br>
						      <input type="button" value="OK" onclick="lightbox_close()" />
						     						
				  </div>  


	  
</body>  
</html>  