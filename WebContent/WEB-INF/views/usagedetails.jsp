
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<head>

<link rel="stylesheet" href="resources/css/PlanDesignResources/css/planmaster.css" media="screen">

</head>
<body>

<% double size = (Double)request.getAttribute("totalSizeinMB");
   float fileStorage = (float) Math.round(size * 100) / 100;
   
   String plan = (String)request.getAttribute("plan");
%>
<div id="visibility">
 <h3>Your Current Plan is <small><%=(String)request.getAttribute("currentPlanName")%></small></h3>
      </div> 
   <div id="plancontainer">
		 	
       <div id="plantablecontainer">
               
			<table class="planpricingtablecontent">
					  
					 <tr class="plantablecell">
					 
					    <th class="tableHeading02">Features</th>
					    <th class="tableHeading03">Current Usage</th>					 														
					 </tr>	
					
					 <%if(!plan.equals("Basic")) {%>
					  <tr>								 					  	
						<td style='width: 200px'>Profile Viewed</td>
						<td style='width: 200px'><%=(Integer)request.getAttribute("noOfFullProfileAsOfNow")%> / <%=(Integer)request.getAttribute("noOfFullProfileAsPerPlan")%></td>																										
					  </tr>
					 <%}%>	
								
					 <tr class="panaltrow">								 					  	
						<td style='width: 200px'>Individual Sharing</td>
						<td style='width: 200px'><%=(Integer)request.getAttribute("noOfGeneratedCode")%> / <%=(Integer)request.getAttribute("noOfGeneratedCodeAsperPlan")%></td>																									
					 </tr>
					 
					 <tr>								 					  	
						<td style='width: 200px'>Documents Received</td>
						<td style='width: 200px'><%=(Integer)request.getAttribute("noOfdApplyCode")%> / <%=(Integer)request.getAttribute("noOfdApplyCodeAsperPlan")%></td>																				
					 </tr>
					  
					  <tr class="panaltrow">								 					  	
						<td style='width: 200px'>Utilizer Sharing / Removing</td>
						<td style='width: 200px'><%=(Integer)request.getAttribute("noOfAllowAccess")%> / <%=(Integer)request.getAttribute("noOfAllowAccessAsperPlan")%></td>																										
					  </tr> 	
					  
					 <%--  <tr>								 					  	
						<td style='width: 200px'>Revoked Access</td>
						<td style='width: 200px'><%=(Integer)request.getAttribute("noOfRevokeAccess")%> / <%=(Integer)request.getAttribute("noOfRevokeAccessAsperPlan")%></td>																										
					  </tr> --%>
					 
					  <tr >								 					  	
						<td style='width: 200px'>Total File Storage</td>
						<td style='width: 200px'><%=fileStorage%> in MB / <%=request.getAttribute("totalSizeinMBAsperPlan")%> in MB																										
					  </tr> 
					  
					  <tr class="panaltrow">								 					  	
						<td style='width: 200px'>SMS </td>
						<td style='width: 200px'><%=(Integer)request.getAttribute("noOfSMSAsOfNow")%> / <%=(Integer)request.getAttribute("noOfSMSAsPerPlan")%></td>																										
					  </tr>
					  
					  			
			</table>
			<div class="planmigrate">
            <p>Please go to Plans &#x2192; Migrate Plan to know more about the features.</p>
         </div>	
         </div>
      
      </div>  
          

</body>
</html>