<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>

<script>



</script>



</head>
<body>
<div id="kycdocumentpage" class="kycdocumentpage" style="margin-left: 0px ! important;">
						    	
									   <% String empDetails = (String)request.getAttribute("employeeDetails"); 
									    
									     String data[] =  empDetails.split("@@@");
									     
									     String empdata[] = data[0].split("&&");
									     
									     String empdocname[] = data[1].split(",");
									   
									     String extraDocumentCheck = (String)session.getAttribute("extraDocumentCheck");
									     
									     String extraAddedDocs = (String)session.getAttribute("extraAddedDocsession");
									   %>
									   
									   <input type="hidden" value="<%=empdata[0]%>" id="backId"/>
									   
									      <div align="center" class="employeedetails" >
									               <div align="right" style="height:52px;"><input class="backbuttonindex_histy" type="button" value="Back" onclick="HistoryDocUti('<%=(String)session.getAttribute("urlValueToGoHistory")%>')" /></div>
									      
									      <div id="viewempdeatls">
									          <table >
									
									            <tr>
									            <td style="width:132px;">
									                    <b>Organization Name </b></td>
									                    <td>:</td>
									                    <td>
									                    <c:out value="<%=empdata[0]%>"></c:out></td>
									            </tr>
									            
									            <tr>
									              <td style="width:105px;">
									            <b>Job Designation </b></td>
									              <td>
									            :</td>
									              <td>
									              <c:out value="<%=empdata[1]%>"></c:out></td>
									            </tr>
									
									            <tr class="profiletextscroll" >
									              <td>
									            <b>Job Profile </b></td>
									              <td>:</td>
									                <td>
									              <c:out value="<%=empdata[2]%>"></c:out></td>
									            </tr>
									
									            <tr>
									            <td>
									            <b>Job Duration </b></td>
									            <td>:</td>
									            <td>
									             <b>form</b><c:out value="<%=empdata[3]%>"></c:out> <b>to</b> <c:out value="<%=empdata[4]%>"></c:out>
									             </td>
									            </tr>
									         
									         </table>
									        </div> 
									          
									       <div id="ImageSlider" class="kycemployeedetailimg" > 
									       <div class="employeedocumentdetails">
									       <span></span>
									          <h1>Employment Documents </h1> 
									          </div>
									          <div class="employmentdocument">
									          <div  style="float:left; position: relative;">
									                     
														 <div id="ShowOfferDiv" style="display:block;">
														 <ul>
														
														<%if(extraDocumentCheck.equals("No")) {%>	
														
																		 <li class="kyc_empviewinner">
																		 <b>Offer Letter</b>
																		 
																		 <%if(!empdocname[0].equals("No Document")){ %>
																		 
																		 		<%if(!empdocname[0].contains("jpeg") && !empdocname[0].contains("jpg") && !empdocname[0].contains("png") && !empdocname[0].contains("gif")) {%>
																		 		
																			 		  <%if(empdocname[0].contains("pdf")) {%>
																																																							                        
															                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
															                            		
															                        <%}else if(empdocname[0].contains("doc")) {%>
															                        
															                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
															                        
															                        <%}else if(empdocname[0].contains("docx")) {%>
															                        
															                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
															                        
															                        <%}else if(empdocname[0].contains("xls")) {%>
															                        
															                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
															                         
															                        <%}else { %>
														                        
														                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
														                        
														                           <%} %>		
																				 
																				 <br>							 	         
																		         <a class="mydocumentdownloadpan"  href="downloadDocfromUtilizer.html?fileName=<%=empdocname[0]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		         
																		 		
																		 		<%} else {%>
																		 		
																		 		<img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[0]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																				 
																				 <br>
																				 <a class="mydocumentviewpan"  onclick="viewKycDocumnt('<%=empdocname[0]%>')"></a>		         
																		         <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[0]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		         
																		 		<%} %>
																		 		
																		
																		 <%} else{%>
																		 
																		 <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
																		 
																		 
																		 <%}%>		
																			
																		 </li>
																		 
														 	<%}else{ %>
														 	
														 	
														 	        <%if(extraAddedDocs.contains(empdocname[0])) {%>
																							                                              
																		
																		 <li class="kyc_empviewinner">
																		 <b>Offer Letter</b>
																		 
																		 <%if(!empdocname[0].equals("No Document")){ %>
																		 
																		 		<%if(!empdocname[0].contains("jpeg") && !empdocname[0].contains("jpg") && !empdocname[0].contains("png") && !empdocname[0].contains("gif")) {%>
																		 		
																			 		  <%if(empdocname[0].contains("pdf")) {%>
																																																							                        
															                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
															                            		
															                        <%}else if(empdocname[0].contains("doc")) {%>
															                        
															                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
															                        
															                        <%}else if(empdocname[0].contains("docx")) {%>
															                        
															                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
															                        
															                        <%}else if(empdocname[0].contains("xls")) {%>
															                        
															                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
															                         
															                        <%}else { %>
														                        
														                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
														                        
														                           <%} %>		
																				 
																				 <br>							 	         
																		         <a class="mydocumentdownloadpan"  href="downloadDocfromUtilizer.html?fileName=<%=empdocname[0]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		         
																		 		
																		 		<%} else {%>
																		 		
																		 		<img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[0]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																				 
																				 <br>
																				 <a class="mydocumentviewpan"  onclick="viewKycDocumnt('<%=empdocname[0]%>')"></a>		         
																		         <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[0]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		         
																		 		<%} %>
																		 		
																		
																		 <%} else{%>
																		 
																		 <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
																		 
																		 
																		 <%} %>		
																			
																		 </li>
																							                                              
																	 <%} %>
														 	        
														 	
														 	<%} %>
														 	
														 	
														 		
														 </ul>
														 </div>
											 </div>
											 
											 <div  style="float:left; position: relative;">
											 
														<div id="ShowHikeDiv" style="display:block;"> 
														<ul>
														            
														            <%if(extraDocumentCheck.equals("No")) {%>	

                                                                               <li class="kyc_empviewinner">
																				<b>Hike Letter</b>
																				
																				<%if(!empdocname[1].equals("No Document")){ %>
																				
																				     <%if(!empdocname[1].contains("jpeg") && !empdocname[1].contains("jpg") && !empdocname[1].contains("png") && !empdocname[1].contains("gif")) {%>
																				 		
																				 		   <%if(empdocname[1].contains("pdf")) {%>
																																																									                        
																	                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																	                            		
																	                        <%}else if(empdocname[1].contains("doc")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(empdocname[1].contains("docx")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(empdocname[1].contains("xls")) {%>
																	                        
																	                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																	                         
																	                        <%}else { %>
																                        
																                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																                        
																                           <%} %>		
																						 
																						 <br>							 	         
																				         <a class="mydocumentdownloadpan"  href="downloadDocfromUtilizer.html?fileName=<%=empdocname[1]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																				         					 		
																				 		<%} else {%>
																				 		
																				         <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[1]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																				         
																				        <br>
																				        
																				         <a class="mydocumentviewpan"  onclick="viewKycDocumnt('<%=empdocname[1]%>')"></a>
																		         
																		                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[1]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>	
																		                  
																		                  <%} %>
																		                  
																		         <%} else {%>
																		         
																		                 <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
																		                 
																		         <%} %>
																		           
																				</li>		

																	<%}else{ %>
																																 	
																		
																		     <%if(extraAddedDocs.contains(empdocname[1])) {%>
																				
																				<li class="kyc_empviewinner">
																				<b>Hike Letter</b>
																				
																				<%if(!empdocname[1].equals("No Document")){ %>
																				
																				     <%if(!empdocname[1].contains("jpeg") && !empdocname[1].contains("jpg") && !empdocname[1].contains("png") && !empdocname[1].contains("gif")) {%>
																				 		
																				 		   <%if(empdocname[1].contains("pdf")) {%>
																																																									                        
																	                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																	                            		
																	                        <%}else if(empdocname[1].contains("doc")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(empdocname[1].contains("docx")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(empdocname[1].contains("xls")) {%>
																	                        
																	                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																	                         
																	                        <%}else { %>
																                        
																                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																                        
																                           <%} %>		
																						 
																						 <br>							 	         
																				         <a class="mydocumentdownloadpan"  href="downloadDocfromUtilizer.html?fileName=<%=empdocname[1]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																				         					 		
																				 		<%} else {%>
																				 		
																				         <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[1]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																				         
																				        <br>
																				        
																				         <a class="mydocumentviewpan"  onclick="viewKycDocumnt('<%=empdocname[1]%>')"></a>
																		         
																		                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[1]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>	
																		                  
																		                  <%} %>
																		                  
																		         <%} else {%>
																		         
																		                 <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
																		                 
																		         <%} %>
																		           
																				</li>		
																																			
																		      <%}%>
																		
																	<%}%>

																				
														</ul>
														</div>
									        </div>
											 
											 <div  style="float:left; position: relative;">
											  
														<div id="ShowExpDiv" >
														<ul>
														
														
														<%if(extraDocumentCheck.equals("No")) {%>	


																		<li class="kyc_empviewinner">
																		<b>Experience Letter </b>
																		     <%if(!empdocname[2].equals("No Document")){ %>
																		     
																		     		<%if(!empdocname[2].contains("jpeg") && !empdocname[2].contains("jpg") && !empdocname[2].contains("png") && !empdocname[2].contains("gif")) {%>
																		 		
																				 		   <%if(empdocname[2].contains("pdf")) {%>
																																																									                        
																	                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																	                            		
																	                        <%}else if(empdocname[2].contains("doc")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(empdocname[2].contains("docx")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(empdocname[2].contains("xls")) {%>
																	                        
																	                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																	                         
																	                        <%}else { %>
																                        
																                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																                        
																                           <%} %>		
																				 
																				 		 <br>							 	         
																		                 <a class="mydocumentdownloadpan"  href="downloadDocfromUtilizer.html?fileName=<%=empdocname[2]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		         					 		
																		 		  <%} else {%>
																		     		
																		                <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[2]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																		         
																		                <br><a class="mydocumentviewpan"  onclick="viewKycDocumnt('<%=empdocname[2]%>')"></a>
																                         <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[2]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																                     
																                     <%} %>
																                       
																              <%} else{%>
																              
																                      <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
																                      
																               <%} %> 
																		</li>
														
															<%}else{ %>
																													 	
															
															     <%if(extraAddedDocs.contains(empdocname[2])) {%>
																			
																			<li class="kyc_empviewinner">
																				<b>Experience Letter </b>
																				     <%if(!empdocname[2].equals("No Document")){ %>
																				     
																				     		<%if(!empdocname[2].contains("jpeg") && !empdocname[2].contains("jpg") && !empdocname[2].contains("png") && !empdocname[2].contains("gif")) {%>
																				 		
																						 		   <%if(empdocname[2].contains("pdf")) {%>
																																																											                        
																			                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																			                            		
																			                        <%}else if(empdocname[2].contains("doc")) {%>
																			                        
																			                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																			                        
																			                        <%}else if(empdocname[2].contains("docx")) {%>
																			                        
																			                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																			                        
																			                        <%}else if(empdocname[2].contains("xls")) {%>
																			                        
																			                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																			                         
																			                        <%}else { %>
																		                        
																		                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																		                        
																		                           <%} %>		
																						 
																						 		 <br>							 	         
																				                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[2]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																				         					 		
																				 		  <%} else {%>
																				     		
																				                <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[2]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																				         
																				                <br><a class="mydocumentviewpan"  onclick="viewKycDocumnt('<%=empdocname[2]%>')"></a>
																		                         <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[2]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		                     
																		                     <%} %>
																		                       
																		              <%} else{%>
																		              
																		                      <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
																		                      
																		               <%} %> 
																				</li>
																											
															      <%}%>
															
														<%}%>
																		
																		
																			
														</ul>
														</div>		
											</div>
											 
											<div  style="float:left; position: relative;">		 
											
											   <div id="ShowRelDiv" >
												
												 <ul> 
														
														<%if(extraDocumentCheck.equals("No")) {%>	

																	<li class="kyc_empviewinner">
																	 <b>Reliving Letter</b>
																	 <%if(!empdocname[3].equals("No Document")){ %>
																	 
																	          <%if(!empdocname[3].contains("jpeg") && !empdocname[3].contains("jpg") && !empdocname[3].contains("png") && !empdocname[3].contains("gif")) {%>
																	 		
																	 		 		  <%if(empdocname[3].contains("pdf")) {%>
																																																								                        
																                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																                            		
																                        <%}else if(empdocname[3].contains("doc")) {%>
																                        
																                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																                        
																                        <%}else if(empdocname[3].contains("docx")) {%>
																                        
																                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																                        
																                        <%}else if(empdocname[3].contains("xls")) {%>
																                        
																                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																                         
																                        <%}else { %>
															                        
															                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
															                        
															                           <%} %>		
																			 		 <br>							 	         
																	                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[3]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																	         					 		
																	 		  <%} else {%>
																	 		  
																	 		       <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[3]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																	          
																		           <br><a class="mydocumentviewpan"  onclick="viewKycDocumnt('<%=empdocname[3]%>')"></a>
																                       <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[3]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																                     
																	 		  <%} %>
																	 		  
																	            
															          <%} else{%>
															          
															                   <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
															                   
															           <%} %> 
															           
																	</li>	
																
														<%}else{ %>
																												 	
														
														     <%if(extraAddedDocs.contains(empdocname[3])) {%>
																		
																		
																		<li class="kyc_empviewinner">
																			 <b>Reliving Letter</b>
																			 <%if(!empdocname[3].equals("No Document")){ %>
																			 
																			          <%if(!empdocname[3].contains("jpeg") && !empdocname[3].contains("jpg") && !empdocname[3].contains("png") && !empdocname[3].contains("gif")) {%>
																			 		
																			 		 		  <%if(empdocname[3].contains("pdf")) {%>
																																																										                        
																		                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																		                            		
																		                        <%}else if(empdocname[3].contains("doc")) {%>
																		                        
																		                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																		                        
																		                        <%}else if(empdocname[3].contains("docx")) {%>
																		                        
																		                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																		                        
																		                        <%}else if(empdocname[3].contains("xls")) {%>
																		                        
																		                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																		                         
																		                        <%}else { %>
																	                        
																	                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																	                        
																	                           <%} %>		
																					 		 <br>							 	         
																			                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[3]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																			         					 		
																			 		  <%} else {%>
																			 		  
																			 		       <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[3]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																			          
																				           <br><a class="mydocumentviewpan"  onclick="viewKycDocumnt('<%=empdocname[3]%>')"></a>
																		                       <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[3]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		                     
																			 		  <%} %>
																			 		  
																			            
																	          <%} else{%>
																	          
																	                   <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
																	                   
																	           <%} %> 
																	           
																			</li>	
																													
														      <%}%>
														
														<%}%>
																		
																		
															
																
																	
														</ul>
														</div>
											</div>
											
											<div  style="float:left; position: relative;">							
														<div id="ShowFormDiv">
														<ul>
														
														
														<%if(extraDocumentCheck.equals("No")) {%>	

																	<li class="kyc_empviewinner">
														 <b>Form 16</b>
														 <%if(!empdocname[4].equals("No Document")){ %>
														 
														           <%if(!empdocname[4].contains("jpeg") && !empdocname[4].contains("jpg") && !empdocname[4].contains("png") && !empdocname[4].contains("gif")) {%>
														 		
														 		 		 <%if(empdocname[4].contains("pdf")) {%>
																																																					                        
													                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
													                            		
													                        <%}else if(empdocname[4].contains("doc")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(empdocname[4].contains("docx")) {%>
													                        
													                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
													                        
													                        <%}else if(empdocname[4].contains("xls")) {%>
													                        
													                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
													                         
													                        <%}else { %>
												                        
												                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
												                        
												                           <%} %>		
																 		 <br>							 	         
														                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[4]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
														         					 		
														 		  <%} else {%>
														 		  
														 		        <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[4]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
														           
															            <br><a  class="mydocumentviewpan" onclick="viewKycDocumnt('<%=empdocname[4]%>')"></a>
													                        <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[4]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
													                      					 		  
														 		  <%} %>
														            
												            <%} else{%>
												            
												                      <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
												                      
												             <%} %>
												             
														 </li>				
																	
																	
														<%}else{ %>
																												 	
														
														     <%if(extraAddedDocs.contains(empdocname[4])) {%>
																			
																	<li class="kyc_empviewinner">
																		 <b>Form 16</b>
																		 <%if(!empdocname[4].equals("No Document")){ %>
																		 
																		           <%if(!empdocname[4].contains("jpeg") && !empdocname[4].contains("jpg") && !empdocname[4].contains("png") && !empdocname[4].contains("gif")) {%>
																		 		
																		 		 		 <%if(empdocname[4].contains("pdf")) {%>
																																																									                        
																	                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																	                            		
																	                        <%}else if(empdocname[4].contains("doc")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(empdocname[4].contains("docx")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(empdocname[4].contains("xls")) {%>
																	                        
																	                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																	                         
																	                        <%}else { %>
																                        
																                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																                        
																                           <%} %>		
																				 		 <br>							 	         
																		                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[4]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		         					 		
																		 		  <%} else {%>
																		 		  
																		 		        <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[4]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																		           
																			            <br><a  class="mydocumentviewpan" onclick="viewKycDocumnt('<%=empdocname[4]%>')"></a>
																	                        <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[4]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																	                      					 		  
																		 		  <%} %>
																		            
																            <%} else{%>
																            
																                      <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
																                      
																             <%} %>
																             
																		 </li>																
														      <%}%>
														
														<%}%>
														
																		
														 	
														 </ul>
														 </div>
														 
											</div>
											
										
										 </div>
										 
											<div id="empsalaryslip">
											 <div class="employeedocumentdetails">
									       <span></span>
											  <h1>SalarySlips :</h1> 
											  </div>
											  <div class="employmentdocument"  id="kycemployeedetailimgbg">
									     <div id="PayslipSlider" class="epmloyeedocimgdet"  > 
									       	
									         <%if(empdocname.length>5) { 
												  
									        	 
									        	 
									        	 String monthNameArray[] = {"January", "February", "March", "April", "May",
 	    				                				"June", "July", "August", "September", "October",
 	    				                				"November", "December"};
					        	 
					        	 String SalarySlipNames = "";    
					        	 String monthyearName = "";    
				
					        	 for(int p=5;p<empdocname.length;p++)
								 {      
					        		 SalarySlipNames += empdocname[p] + ",";
								 } 
					        	 
					        	 if(SalarySlipNames.length()>0)
					        	 {
					        		 SalarySlipNames = SalarySlipNames.substring(0, SalarySlipNames.length()-1);
					        		 
					        		 String slipNamesArray[] = SalarySlipNames.split(",");
					        	     for(int sn=0;sn<slipNamesArray.length;sn++)
					        	     {
					        	    	 int i = slipNamesArray[sn].split("_")[0].lastIndexOf(" ");
					        	    	 String tempValue = slipNamesArray[sn].split("_")[0].substring(i+1);
					        	    	 
					        	    	 monthyearName += tempValue + ",";
					        	     }
					        	 }
					        	 
					        	 if(monthyearName.length()>0)
					        	 {
					        		 monthyearName = monthyearName.substring(0, monthyearName.length()-1);
					        	 }
					        	 
					        	 
					        	 //  Sorted logics
					        	 
					        	 String sortedMonthYearName = "";
					        	 
					        	 if(SalarySlipNames.length()>0)
						    	     {
						    	    	
						    	    	 String monthYearArray[] = monthyearName.split(","); 
						    	    	 
						    	    	 for(int mn=0;mn<monthYearArray.length;mn++)    // outer monthYear array
						    	    	 {
						    	    		 String monthValue = monthYearArray[mn].split("-")[0];
						    	    		 int outerindex = 0;
			    	    					 int innerindex = 0;
						    	    				 for(int imn=0;imn<monthYearArray.length;imn++)    // inner monthYear array
									    	    	 {
						    	    					 String imonthValue = monthYearArray[imn].split("-")[0];
							    	    				 for(int imName=0;imName<monthNameArray.length;imName++)
									    	    		 {
							    	    					 if(monthValue.equals(monthNameArray[imName]))
							    	    					 {
							    	    						 outerindex = imName;
							    	    					 }
							    	    					 if(imonthValue.equals(monthNameArray[imName]))
							    	    					 {
							    	    						 innerindex = imName;
							    	    					 }
									    	    		 }
							    	    				 if(outerindex>innerindex)
							    	    				 {
							    	    					 String temp = monthYearArray[mn];
							    	    					 monthYearArray[mn] = monthYearArray[imn];
								    	    				 monthYearArray[imn] = temp;
							    	    				 }
									    	    	 }
						    	    																														    	    		 
						    	    	 }
						    	    	 
						    	    	 
						    	    	 for(int k=0;k<monthYearArray.length;k++)
						    	    	 {																															    	    		 
						    	    		 int yearValue = Integer.parseInt(monthYearArray[k].split("-")[1]);
						    	    		 for(int l=0;l<monthYearArray.length;l++)
						    	    		 {
						    	    			 int tempyearValue = Integer.parseInt(monthYearArray[l].split("-")[1]);
						    	    			 if(yearValue>tempyearValue)
						    	    			 {
						    	    				 String temp = monthYearArray[k];
						    	    				 monthYearArray[k] = monthYearArray[l];
						    	    				 monthYearArray[l] = temp;
						    	    			 }
						    	    		 }																																    	    		 
						    	    	 }
						    	    	
						    	    	 for(int my=0;my<monthYearArray.length;my++)
						    	    	 {
						    	    		 sortedMonthYearName += monthYearArray[my] + ",";																														    	    		 
						    	    	 }
						    	    	 
						    	    	 if(sortedMonthYearName.length()>0)
						    	    	 {
						    	    		 sortedMonthYearName = sortedMonthYearName.substring(0, sortedMonthYearName.length()-1);
						    	    	 }
						    	    	 
						    	     }
						     
					        	    System.out.println("month-year Sorted Names="+sortedMonthYearName);
					        	 
					        		 String monthName[] = sortedMonthYearName.split(","); 
							    	 String salSlipArray1[] = SalarySlipNames.split(","); 
								     String sortedSlipName = "";
								     for(int sort=0;sort<monthName.length;sort++)
					    	    	 {
								    	 for(int sorti=0;sorti<salSlipArray1.length;sorti++)
						    	    	 {
								    		 if(salSlipArray1[sorti].contains(monthName[sort]))
								    		 {
								    			 sortedSlipName += salSlipArray1[sorti] + ",";	
								    		 }																										    	    		 
						    	    	 }																												    	    		 
					    	    	 }
								     
								     if(sortedSlipName.length()>0)
								     {
								    	 sortedSlipName =sortedSlipName.substring(0, sortedSlipName.length()-1);
								     }
								     
								     String salSlipArray[] = sortedSlipName.split(","); 
								     
					        	 
					        	 // End of sorted logics
					        	 
					        	 
					        	 System.out.println("Salary slips Sorted Names="+sortedSlipName);
									        	 
									        	 
									        	 
									        	 
									        	 
									        	 
									        	 
													  for(int p=0;p<salSlipArray.length;p++)
											   			{
												      %>      
											
											     
									         <div   style="float: left; position: relative; margin-bottom: 20px;">
												      <ul>
												          
												          
												          
												          <%if(extraDocumentCheck.equals("No")) {%>	

																	
																	<li class="panviewinner_acad">
																	      <%if(!salSlipArray[p].contains("jpeg") && !salSlipArray[p].contains("jpg") && !salSlipArray[p].contains("png") && !salSlipArray[p].contains("gif")) {%>
																		 		
																		 		 		  <%if(salSlipArray[p].contains("pdf")) {%>
																																																									                        
																	                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																	                            		
																	                        <%}else if(salSlipArray[p].contains("doc")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(salSlipArray[p].contains("docx")) {%>
																	                        
																	                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																	                        
																	                        <%}else if(salSlipArray[p].contains("xls")) {%>
																	                        
																	                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																	                         
																	                        <%}else { %>
																                        
																                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																                        
																                           <%} %>		
																				 		 <br>							 	         
																		                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=salSlipArray[p]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																		         					 		
																		 		  <%} else {%>
																		 		  
																		 		  
																					      <%=salSlipArray[p].split("_")[0]%><br>
																					      <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+salSlipArray[p]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																					      
																					      <br><a  onclick="viewKycDocumnt('<%=salSlipArray[p]%>')"><button class="mydocumentviewpan"></button></a>
																					      <a href="downloadDocfromUtilizer.html?fileName=<%=salSlipArray[p]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"><button class="mydocumentdownloadpan"></button></a>
																					       		
																	       		  <%} %>
																	       				      
																	      </li>
													      
														<%}else{ %>
																												 	
														
														     <%if(extraAddedDocs.contains(salSlipArray[p])) {%>
																		
																		
																		   <li class="panviewinner_acad">
																			      <%if(!empdocname[p].contains("jpeg") && !empdocname[p].contains("jpg") && !empdocname[p].contains("png") && !empdocname[p].contains("gif")) {%>
																				 		
																				 		 		  <%if(empdocname[p].contains("pdf")) {%>
																																																											                        
																			                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																			                            		
																			                        <%}else if(empdocname[p].contains("doc")) {%>
																			                        
																			                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																			                        
																			                        <%}else if(empdocname[p].contains("docx")) {%>
																			                        
																			                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																			                        
																			                        <%}else if(empdocname[p].contains("xls")) {%>
																			                        
																			                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																			                         
																			                        <%}else { %>
																		                        
																		                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																		                        
																		                           <%} %>		
																						 		 <br>							 	         
																				                 <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=empdocname[p]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																				         					 		
																				 		  <%} else {%>
																				 		  
																				 		  
																							      <%=empdocname[p].split("_")[0]%><br>
																							      <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+empdocname[p]+"&docCategory=Employment_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="120" height="80" /></img>
																							      
																							      <br><a  onclick="viewKycDocumnt('<%=empdocname[p]%>')"><button class="mydocumentviewpan"></button></a>
																							      <a href="downloadDocfromUtilizer.html?fileName=<%=empdocname[p]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"><button class="mydocumentdownloadpan"></button></a>
																							       		
																			       		  <%} %>
																			       				      
																			      </li>
																			      													
														      <%}%>
														
														<%}%>
													      
												      </ul>
												       			
											 </div>
											 
											   <%}
												  
												  } else {%>
												  			  
												     <h1>Salary slips are not added in this record !</h1>
												      			       					  		  			  
												  <%} %>
											 
										  </div>
									        </div>
									        </div>
									   
									     
									     </div>  
									     </div>         
</body>
</html>