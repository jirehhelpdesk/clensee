<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>

</head>
<body>

 <c:if test="${!empty organiseIndividualfeature}">
 
  <table class="CSSTableGenerator"  style="width: 92% ! important;" cellspacing="1">
			   
			   <tr>
			    <th style='width:auto'  bgcolor="#999">Sl.No</th>				
				<th style='width:auto'  bgcolor="#999">Name</th>
				<th style='width:auto'  bgcolor="#999">Value</th>			
				<th style='width:auto'  bgcolor="#999">Price</th>							
			    <th style='width:auto'  bgcolor="#999">Edit</th>
			    <th style='width:auto'  bgcolor="#999">Delete</th>		
			  </tr>	
			  
	 <%int i=0;%>
			
			<c:forEach items="${organiseIndividualfeature}" var="det">	
			  			  
			   <tr>
				    <td><c:out value="<%=++i%>"/></td>					   			
					<td><input  class="viewhistory" type="text"  name="feature_name" id="feature_name${det.feature_id}" value="${det.feature_name}" disabled="true"/></td>
					<td><input  class="viewhistory" type="text"  name="feature_value" id="feature_value${det.feature_id}" value="${det.feature_value}" disabled="true"/></td>			
					<td><input  class="viewhistory" type="text"  name="feature_price" id="feature_price${det.feature_id}" value="${det.feature_price}" disabled="true" /></td>							
				    <td><input class="viewhistory" type="button" id="${det.feature_id}" onclick="validatereg(this,this.id)" name="editorsave" value="Edit"></td>
				    <td><input class="viewhistory" type="button" id="${det.feature_id}" onclick="deleteorganisefeature(this.id)" name="delete" value="Delete"></td>  	
			   </tr>	
		       
			</c:forEach>

  </table>
</c:if>

<c:if test="${!empty organiseUtilizerfeature}">

	 <table class="CSSTableGenerator" align="left" cellspacing="1">
				   
				   <tr>
				    <th style='width:auto'  bgcolor="#999">Sl.No</th>				
					<th style='width:auto'  bgcolor="#999">Name</th>
					<th style='width:auto'  bgcolor="#999">Value</th>			
					<th style='width:auto'  bgcolor="#999">Price</th>							
				    <th style='width:auto'  bgcolor="#999">Edit</th>
				    <th style='width:auto'  bgcolor="#999">Delete</th>		
				  </tr>	
				  
		<%int j=0;%>
				
				<c:forEach items="${organiseUtilizerfeature}" var="det">	
				  			  
				   <tr>
					    <td><c:out value="<%=++j%>"/></td>					    		
						<td><input  class="viewhistory" type="text"  name="feature_name" id="feature_name${det.feature_id}" value="${det.feature_name}" disabled="true"/></td>
						<td><input  class="viewhistory" type="text"  name="feature_value" id="feature_value${det.feature_id}" value="${det.feature_value}" disabled="true"/></td>			
						<td><input  class="viewhistory" type="text"  name="feature_price" id="feature_price${det.feature_id}" value="${det.feature_price}" disabled="true" /></td>							
					    <td><input class="viewhistory" type="button" id="${det.feature_id}" onclick="validatereg(this,this.id)" name="editorsave" value="Edit"></td>
					    <td><input class="viewhistory" type="button" id="${det.feature_id}" onclick="deleteorganisefeature(this.id)" name="delete" value="Delete"></td>  	
				   </tr>	
			       
				</c:forEach>
	
	   </table>
   
</c:if>
  
  <c:if test="${!empty organiseCorporatefeature}"> 
   <table class="CSSTableGenerator" align="left" cellspacing="1">
			   
			   <tr>
			    <th style='width:auto'  bgcolor="#999">Sl.No</th>			    
				<th style='width:auto'  bgcolor="#999">Name</th>
				<th style='width:auto'  bgcolor="#999">Value</th>			
				<th style='width:auto'  bgcolor="#999">Price</th>							
			    <th style='width:auto'  bgcolor="#999">Edit</th>
			    <th style='width:auto'  bgcolor="#999">Delete</th>		
			  </tr>	
			  
	<%int k=0;%>
			
			<c:forEach items="${organiseCorporatefeature}" var="det">	
			  			  
			   <tr>
				    <td><c:out value="<%=++k%>"/></td>					   			
					<td><input  class="viewhistory" type="text"  name="feature_name" id="feature_name${det.feature_id}" value="${det.feature_name}" disabled="true"/></td>
					<td><input  class="viewhistory" type="text"  name="feature_value" id="feature_value${det.feature_id}" value="${det.feature_value}" disabled="true"/></td>			
					<td><input  class="viewhistory" type="text"  name="feature_price" id="feature_price${det.feature_id}" value="${det.feature_price}" disabled="true" /></td>							
				    <td><input class="viewhistory" type="button" id="${det.feature_id}" onclick="validatereg(this,this.id)" name="editorsave" value="Edit"></td>
				    <td><input class="viewhistory" type="button" id="${det.feature_id}" onclick="deleteorganisefeature(this.id)" name="delete" value="Delete"></td>  	
			   </tr>	
		       
			</c:forEach>

   </table>
</c:if>
</body>
</html>