<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<title>Insert title here</title>

<link rel="stylesheet" href="resources/css/PlanDesignResources/css/planmaster.css" media="screen">

</head>
<body>
<%String planname=(String)request.getAttribute("planname");
String plan=planname.substring(1, planname.length()-1);

String startDate = (String)request.getAttribute("startDate");
String endDate = (String)request.getAttribute("endDate");
%>
<div id="planmain">

<%if(plan.equals("Basic")) {%>
<h1><b>Your current plan is :</b> <%= plan %> Valid for lifetime.</h1>	
<%}else  {%>
<h1><b>Your current plan is :</b> <%= plan %> Valid from <%=startDate%> to <%=endDate%> . </h1>	
<%} %>

	<div id="plancontainer">
  <div id="plantablecontainer" class="">
   
			<table class="planpricingtablecontent">
					  
					 <tr class="plantablecell">
					 
					    <th class="tableHeading02">Feature Name</th>
					    <th class="tableHeading03">Feature Data</th>				
						<!-- <th class="tableHeading04">Feature Price</th> -->
						
					 </tr>	
					 <%int i=1; %>
					<c:forEach items="${viewplan}" var="det">   
					
					  <%if(i%2==0){ %>
					    <tr class="panaltrow">			
					   <%}else{ %>
					    <tr>
					   <%} %>  		
						<td style='width: 200px'><c:out value="${det.feature_name}"/></td>
						<td style='width: 200px'><c:out value="${det.feature_value}"/></td>			
						<%-- <td style='width: 200px'><c:out value="${det.feature_price}"/></td> --%>								
					   </tr>
					   	
					</c:forEach> 
					  
			</table>
         
         </div>
      
      </div>  
      </div>
            
</body>
</html>