<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Individual</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link href="resources/css/kycstyle.css" rel="stylesheet" type="text/css" />
<link href="resources/css/blue.css" rel="stylesheet" type="text/css" />


<script src="resources/onlineresources/code.jquery.com.jquery-1.9.1.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.10.2.jquery-ui.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 

<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>

<script src="resources/kyctabs/jquery.easytabs.min.js"></script>

<link href="resources/kyctabs/kyc_tabpsstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/UtiCss/doctable.css"/>
<link rel="stylesheet" href="resources/css/style_common.css" />
<link rel="stylesheet" href="resources/css/style_common.css" />
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css"/>

<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	$("#waitingdivId").show();
	$("#waitingdivContentId").show();
	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 $("#waitingdivId").hide();
	    	 $("#waitingdivContentId").hide();
	    	 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	$("#waitingdivId").show();
	$("#waitingdivContentId").show();
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 $("#waitingdivId").hide();
	    	 $("#waitingdivContentId").hide();
	    	 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	$("#waitingdivId").show();
	$("#waitingdivContentId").show();
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 $("#waitingdivId").hide();
	    	 $("#waitingdivContentId").hide();
	    	 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>

<!-- /* Pop up Script and css*/ -->

<script type="text/javascript">

window.document.onkeydown = function (e)
{
    if (!e)
    {
        e = event;
    }
    if (e.keyCode == 27)
    {
    	successlightbox_close('success');
    }
}


function successlightbox_close(action){
	
	if(action=='warning')
		{
			document.getElementById('successlight').style.display='none';
		    document.getElementById('successfade').style.display='none';		  
		}
	else
		{
			document.getElementById('successlight').style.display='none';
		    document.getElementById('successfade').style.display='none';
		    x=document.getElementById("saveButtonId")
		    x.disabled = !x.disabled;
		    window.location = "individual.html";
		}       
}

</script>	

<style type="text/css">





</style>


<!-- /* End of Pop up Script and css*/ -->



<script type="text/javascript">


function searchhomepage(){
	
	var id1=$("#searchvaluetext").val();
	
	if(id1.length>=3)
	{
		$(".topdivespan").show();		
		$('#searchhomepage').show();	
		$("#waitingdivId").show();
		$("#waitingdivContentId").show();
		 
		$.ajax({ 
			
		     type : "Post",   
		     url : "search_Outerprofile.html", 	  
		     data : "kycid="+id1,	     	     	     
		     success : function(response) 
		     {   					
					$("#waitingdivId").hide();		
					$("#waitingdivContentId").hide();
					 
					$('#searchhomepage').html(response);						 	    	
		     },  
		    
		    }); 		
	}	
	else if(id1.length<3)
	{	  
		  $(".topdivespan").hide();	
		  $('#searchhomepage').html("");
		  $('#searchhomepage').hide();		  
		  $("#waitingdivId").hide();	
		  $("#waitingdivContentId").hide();
	}
	else 		
	{	
		$(".topdivespan").hide();	
		$('#searchhomepage').html("");
		$('#searchhomepage').hide();		    		
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
	}	 
}

function validateMobileNoOnkeyUp()
{
	var phno =  /^[0-9\b]+$/;	
	var mobileno = document.getElementById("phno").value;
    
	var noLength = mobileno.length;
	if(noLength==0)
	{
		$("#phnoDiv").hide();		
	}		
	if(mobileno != "")
	{
		if(!mobileno.match(phno))  
        	{ 
				$("#phnoDiv").show();
				$("#phnoDiv").html("Please enter a valid mobile number.");
				return false;
        	}
		else
			{
			  $("#phnoDiv").hide();	
			}
	}
	
}

function validateMobileNo()
{
	var phno = /^[0-9]{10}$/;	
	var mobileno = document.getElementById("phno").value;
    
	var noLength = mobileno.length;
	
	if(noLength==0)
	{
		$("#phnoDiv").hide();		
	}
	if(noLength>10 || noLength<10)
	{
		$("#phnoDiv").show();
		$("#phnoDiv").html("Please enter a valid mobile number.");
		return 'false';
	}
	if(noLength==10)
	{
		if(mobileno != "")
		{
			if(!mobileno.match(phno))  
        		{ 
					$("#phnoDiv").show();
					$("#phnoDiv").html("Please enter a valid mobile number.");
					return 'false';
        		}
			else
				{
				 $.ajax({
			         url: "validatemobileno.html",
			         type: 'post',
			         data: "mobileno="+mobileno,        
			         success: function (data) {         	
			         	if(data=='Yes')
			         		{	        		
			         		    $("#phnoDiv").show();
			         		    $("#phnoDiv").html("Mobile number already exists.");		
			 	    	        return 'false';
			         		}
			         	else
			         		{
				         		$("#phnoDiv").hide();
							    return 'true';
			         		}        	        	
			           },       
			      }); 
				    
				}
		 }
	}
	
}


function validateKycidOnKeyUp()
{
	var poi= /^[a-zA-Z0-9]{1,30}$/;
    var kycid = document.getElementById("kid").value;
    var Id = document.getElementById("kid").value;
    if(Id=="")
	{
    	$("#kidDiv").hide();
	}
    if(Id!="")
	{
		if(!Id.match(poi))  
        {
			$("#kidDiv").show();
	        $("#kidDiv").html("Please enter valid document id.");
        }
		else
		{
			$("#kidDiv").hide();
		}
	}
} 


function validateKycid()
{	
	var poi= /^[a-zA-Z0-9]{1,30}$/;
    var kycid = document.getElementById("kid").value;
    var Id = document.getElementById("kid").value;
    
    if(Id!="")
	{
		if(!Id.match(poi))  
        {  	 
			$("#kidDiv").show();
	        $("#kidDiv").html("Please enter valid document id");
	        return 'false';
        } 
		else
		{			    
			     $.ajax({
			         url: "validateKYCid.html",
			         type: 'POST',
			         data: "kycId="+kycid,        
			         success: function (data) {         	
			         	if(data=='Yes')
			         		{	        		
			 	    			$("#kidDiv").show();
			 	    	        $("#kidDiv").html("Document id already exists.");		
			 	    	        document.getElementById('waitlight').style.display='none';
			 	  		        document.getElementById('waitfade').style.display='none';
			 	    	        return 'false';
			         		}
			         	else
			         		{
			        				$("#kidDiv").hide();
			        			    return 'true';
			         		}        	        	
			           },       
			      });
		 }
	}
          
}

function validateEmailId()
{	
	var regexEmailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	var emailId = document.getElementById("email").value;
    
	if(emailId!="")
		{
		      if(!emailId.match(regexEmailId))
		    	  {
		    	    $("#emailDiv").show();
	    	        $("#emailDiv").html("Please enter a valid email id.");	
	    	        return 'false';
		    	  }
		      else
		    	  {
			    	  $.ajax({
			    	       url: "validateEmailId.html",
			    	       type: 'POST',
			    	       data: "emailId="+emailId,        
			    	       success: function (data) { 
			    	       	if(data=='Yes')
			    	       		{	        		
			    		    			$("#emailDiv").show();
			    		    	        $("#emailDiv").html("Email id already exists.");	
			    		    	        document.getElementById('waitlight').style.display='none';
			    		    		    document.getElementById('waitfade').style.display='none';
			    		    	        return 'false';
			    	       		}
			    	       	else
			    	       		{
			    	       				$("#emailDiv").hide();
			    	       				return 'true';
			    	       		}           	
			    	       },       
			    	    });
		    	  }
		}
	if(emailId=="")
		{
		   $("#emailDiv").hide();
		}
    

}


function validateCaptcheCode()
{
	var captcheCode = document.getElementById("indcaptchCode").value;
		
	if(captcheCode!="")
	{
		 $.ajax({					
		        url: "validateCaptchaCode.html",
		        type: 'post',
		        data: "answer="+captcheCode,	        
		        success: function (data) { 
		        			        	
		        	if(data=="Incorrect")
		        		{	
			        		
		     		        document.getElementById('waitlight').style.display='none';
		     			    document.getElementById('waitfade').style.display='none';
		     			    
		     			    $("#captchadiv").show();
	        		        $("#captchadiv").html("Entered captcha code is incorrect.");	
	        		        
		     		        document.getElementById("indcaptchCode").value = "";
		     		        reloadCaptcha();
		     		        
		     		        return false;
		        		}
		        	else
		        		{
		        		    $("#regErrorId").hide();
		        		    return true;
		        		}
	           },	        	        	        
	      });
		  
	}
	if(captcheCode=="")
	{		 
		    $("#captchadiv").show();
	        $("#captchadiv").html("Please enter the captcha code.");
		 	return false;
	}
	
}
	
	
function validateOnEach()
{
	var name = /^[a-zA-Z\s]*$/;	
	//var name = /^[a-zA-Z]*$/;			
	var fname = document.getElementById("fname").value;
	var mname = document.getElementById("mname").value;
	var lname = document.getElementById("lname").value;
	var pw = document.getElementById("pw").value;
	var repw =document.getElementById("repw").value;
	var kycid = document.getElementById("kid").value;
	
	var captcheCode = document.getElementById("indcaptchCode").value;
						
	if(lname!="")
	{
		if(!lname.match(name))  
        {  	
			$("#lnameDiv").show();
	        $("#lnameDiv").html("Only Alphabets are allowed");		        
        } 
		else
		{
			$("#lnameDiv").hide();
		}
	}
	
	if(lname=="")
	{
		$("#lnameDiv").hide();
	}
	
	if(mname!="")
	{		
		if(!mname.match(name))  
        {  						
			$("#mnameDiv").show();
	        $("#mnameDiv").html("Only Alphabets are allowed");		        
        } 
		else
		{
			$("#mnameDiv").hide();
		}
	}
	
	if(mname=="")
	{
		$("#mnameDiv").hide();
	}
	
	if(fname!="")
	{
		if(!fname.match(name))  
        {  	
			$("#fnameDiv").show();
	        $("#fnameDiv").html("Only Alphabets are allowed");		        
        } 
		else
			{
			  $("#fnameDiv").hide();
			}
	}
	if(fname=="")
	{
		$("#fnameDiv").hide();
	}

	if(kycid!="")
	{				
		$("#kidDiv").hide();		
	}
	
	if(pw!="")
	{	
		$("#pwDiv").hide();	  
	}
	
	if(repw!="")
	{
		$("#repwDiv").hide();
	}
	
	/* if(pw!="" & repw!="")
	{
	    if(pw!=repw)
	    {	  
	       $("#repw").show();	
	       $("#repw").html("Password and confirm password doesn't match .");			       
	    }
	} */
	
	if(captcheCode!="")
	{
		 $("#captchadiv").hide();
	}
}


function checkboxStatus()
{
	var checkId = document.getElementById("checkId");	
	if(checkId.checked==true)
	{
	   $("#regErrorId").hide();	   
	   
	   document.getElementById('light').style.display='block';
	   document.getElementById('fade').style.display='block'
	}	
}

function showtermsAndCondForSignUp()
{
	$("#regErrorId").hide();	   	
	document.getElementById('light').style.display='block';
	document.getElementById('fade').style.display='block'
}


function showAcceptence(value)
{	
	if(value=="Accept")
		{
		   document.getElementById("checkId").checked = true;
		}
	else
		{
		   document.getElementById("checkId").checked = false;
		}

}

function validateGender()
{
	var gen = document.getElementById("gen").value;
	
	if(gen=="Select your Gender")
	{
			flag='false';	
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
        	$("#genderDiv").show();
        	$("#genderDiv").html("Select your Geneder !");
			return false;
	}
	else
	{		
		$("#genderDiv").hide();
	}  
}

function validateKYCID()
{
    var typeofKycId = document.getElementById("typeOfKYCId").value;
	
	if(typeofKycId!="Select your KYC Document")
	{
		$("#typeofKycDiv").hide();
		document.getElementById("kid").placeholder = typeofKycId+" ID *";
	}
	if(typeofKycId=="Select your KYC Document")
	{	
		document.getElementById('waitlight').style.display='none';
	    document.getElementById('waitfade').style.display='none';
	    		
		document.getElementById("kid").placeholder = "Document Id *";
		$("#typeofKycDiv").show();
		$("#typeofKycDiv").html("Please select document type.");
		
		return false;
	}
}

function saveSignUp()
{
	$("#waitingdivId").show();
	$("#waitingdivContentId").show();
	
	
	var flag='true';
	
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	//var name = /^[a-zA-Z]+$/;
	var name = /^[a-zA-Z\s]*$/;	
	var phno = /^[0-9]{10}$/;	
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
	var fname = document.getElementById("fname").value;
	var mname = document.getElementById("mname").value;
	var lname = document.getElementById("lname").value;
	var mobileno = document.getElementById("phno").value;
	var Id = document.getElementById("kid").value;
	var email = document.getElementById("email").value;
	var pw = document.getElementById("pw").value;
	var repw =document.getElementById("repw").value;
	var gen = document.getElementById("gen").value;
	
	
	if(fname=="")
	{		
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		
		flag='false';
		$("#fnameDiv").show();
	    $("#fnameDiv").html("Please enter first name.");	
	    return false;       
	}
	
	if(fname!="")
	{
		if(!fname.match(name))  
        {  	
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			flag='false';
			$("#fnameDiv").show();
	        $("#fnameDiv").html("Only Alphabets are allowed");		  
	        return false;       
        } 		
	}
	
		
	if(mname!="")
	{		
		if(!mname.match(name))  
        {  		
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			flag='false';
			$("#mnameDiv").show();
	        $("#mnameDiv").html("Only Alphabets are allowed");	
	        return false; 
        } 
	}
	
	
	if(lname=="")
	{		
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		
		flag='false';
		$("#lnameDiv").show();
	    $("#lnameDiv").html("Please enter last name.");	
	    return false;       
	}
	
	if(lname!="")
	{
		if(!lname.match(name))  
        {  	
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			flag='false';
			$("#lnameDiv").show();
	        $("#lnameDiv").html("Only Alphabets are allowed");	
	        return false;       
        } 
	}
	
	if(gen!="")
	{
		if(gen=="Select your Gender")
		{
			flag='false';			
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			
        	$("#genderDiv").show();
        	$("#genderDiv").html("Please select gender.");
			return false;
		}
	}
	
	var typeofKycId = document.getElementById("typeOfKYCId").value;
	
	if(typeofKycId!="Select your KYC Document")
	{
		$("#typeofKycDiv").hide();
	}
	
	if(typeofKycId=="Select your KYC Document")
	{
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		
		$("#typeofKycDiv").show();
		$("#typeofKycDiv").html("Please select document type.");
	    flag = 'false';
		return false;
	}
	
	
	if(Id=="")
	{
		flag='false';
		
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();		
        
		$("#kidDiv").show();
        $("#kidDiv").html("Please enter selected document id.");	
        return false;
	}
	
	
	if(Id!="")
	{		
		if(!Id.match(alfanumeric))  
        {  						
			$("#kidDiv").show();
	        $("#kidDiv").html("Please enter valid document id ");		        
        } 
		else
		{
			$("#kidDiv").hide();
		}
	}
	
	if(mobileno=="")
	{
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		$("#phnoDiv").show();
		$("#phnoDiv").html("Please enter mobile number.");
		return false;
	}
	
	if(mobileno!="")
	{		
		if(!mobileno.match(phno))  
        {  		
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			$("#phnoDiv").show();
			$("#phnoDiv").html("Please enter a valid mobile number.");	
			return false;
        } 
		else
		{
			$("#phnoDiv").hide();
		}
	}
	
	if(email=="")
	{
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		$("#emailDiv").show();
	    $("#emailDiv").html("Please enter email id");	
	    return false;
	}
	
	if(email!="")
	{		
		if(!email.match(emailId))  
        {  						
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			$("#emailDiv").show();
		    $("#emailDiv").html("Please enter a valid email id.");	
		    return false;      
        } 
		else
		{
			$("#emailDiv").hide();
		}
	}
	       
	
	if(pw=="")
	{
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		$("#pwDiv").show();
	    $("#pwDiv").html("Please enter password");	
	    return false;
	}
	
	if(repw=="")
	{
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		$("#repwDiv").show();
	    $("#repwDiv").html("Please re-enter password to confirm.");	
	    return false;
	}
    
	if(pw!="" & repw!="")
	{	
		if(pw.length<6)
		{			
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			flag='false';
			
			
			$("#regErrorId").show();
		    $("#regErrorId").html("Password must contain,<br>"+
		    		   "1. atleast one lowercase letter [a-z]<br>"+
		    		   "2. atleast one uppercase letter [A-Z]<br>"+
		    		   "3. atleast one number [0-9]<br>"+
		    		   "4. minimum of 6 character length");
		    
			return false;
		}
		
		if(!pw.match(".*[a-z].*"))
		{			
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			       
		   flag='false';
		   
		   $("#regErrorId").show();
		   $("#regErrorId").html("Password must contain, <br>"+
	    		   "1. atleast one lowercase letter [a-z]<br>"+
	    		   "2. atleast one uppercase letter [A-Z]<br>"+
	    		   "3. atleast one number [0-9]<br>"+
	    		   "4. minimum of 6 character length");
	       document.getElementById("pw").value = "";
	   	   document.getElementById("repw").value = "";
	       return false;
		}
		
		if(!pw.match(".*[A-Z].*"))
		{			
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
	        
		   flag='false';
		   $("#regErrorId").show();
		   $("#regErrorId").html("Password must contain, <br>"+
	    		   "1. atleast one lowercase letter [a-z]<br>"+
	    		   "2. atleast one uppercase letter [A-Z]<br>"+
	    		   "3. atleast one number [0-9]<br>"+
	    		   "4. minimum of 6 character length");
		   
	       document.getElementById("pw").value = "";
	   	   document.getElementById("repw").value = "";
	   	   return false;
		}
		
		if(!pw.match(".*\\d.*"))
		{			
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
	           
		    flag='false';
		    $("#regErrorId").show();
		    $("#regErrorId").html("Password must contain, <br>"+
		    		   "1. atleast one lowercase letter [a-z]<br>"+
		    		   "2. atleast one uppercase letter [A-Z]<br>"+
		    		   "3. atleast one number [0-9]<br>"+
		    		   "4. minimum of 6 character length");
		   
	        document.getElementById("pw").value = "";
	   	    document.getElementById("repw").value = "";
	  	 	return false;
		}
		
	    if(pw!=repw)
	    {	    	
	       $("#waitingdivId").hide();
		   $("#waitingdivContentId").hide();			
	           
	       flag='false';
	       $("#repwDiv").show();
	       $("#repwDiv").html("Password and confirm password doesn't match .");
	       /* document.getElementById("pw").value = "";
	   	   document.getElementById("repw").value = ""; */
	   	   return false;
	    }
	    
	}
		
	if(lname!="")
	{
		if(!lname.match(name))  
        {  	
			flag='false';			
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			       
			$("#lnameDiv").show();
	        $("#lnameDiv").html("Only Alphabets are allowed");
	        return false;
        } 
		else
		{
			$("#lnameDiv").hide();
		}
	}
	
	if(mname!="")
	{
		if(!mname.match(name))  
        {  				
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();			
			
			flag='false';
			$("#mnameDiv").show();
	        $("#mnameDiv").html("Only Alphabets are allowed");
	        return false;
        } 
		else
		{
			$("#mnameDiv").hide();
		}
	}
	
	
	if(fname!="")
	{
		if(!fname.match(name))  
        {  	
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
						
			flag='false';
			$("#fnameDiv").show();
	        $("#fnameDiv").html("Only Alphabets are allowed");	
	        return false;
        } 
		else
		{
			$("#fnameDiv").hide();
		}
	}
	
	var typeofKycId = document.getElementById("typeOfKYCId").value;
				
    var captcheCode = document.getElementById("indcaptchCode").value;
 	
 	if(captcheCode=="")
 	{		
 		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		   	
 		    flag = 'false';
 		    $("#captchadiv").show();
	        $("#captchadiv").html("Please enter the captcha code.");	
 		 	return false;
 	}
 	
	var checkId = document.getElementById("checkId");
	if(checkId.checked!=true)
		{		
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
		        
		   flag = 'false';
		   $("#regErrorId").show();
		   $("#regErrorId").html("Please accept Terms & Conditions.");
		   return false;
		}
	
	if(checkId.checked==true)
	{
	   $("#regErrorId").hide();	   
	}
	
		
	if(flag=='true')
	{	
		$("#waitingdivId").show();
		$("#waitingdivContentId").show();
				
		var formData = new FormData($(signUpForm)[0]);
					
		$.ajax({
			
	        url: "saveIndSignUp.html",
	        type: 'POST',
	        data: formData,	        	        
	        success: function (data) { 
	        	
	        	if(data=='success')
	        		{
	        		    x=document.getElementById("saveButtonId")
	        		    x.disabled = !x.disabled;
	        		    
	        		    $("#waitingdivId").hide();
	        			$("#waitingdivContentId").hide();
	        			
	        	        
		        	  	document.getElementById('successlight').style.display='block';
			            document.getElementById('successfade').style.display='block';
			            
			            document.getElementById("fname").value = "";
			        	document.getElementById("mname").value = "";
			        	document.getElementById("lname").value = "";
			        	document.getElementById("phno").value = "";
			        	document.getElementById("kid").value = "";
			        	document.getElementById("email").value = "";
			        	document.getElementById("pw").value = "";
			        	document.getElementById("repw").value = "";
			        	document.getElementById("gen").value = "";
			        	
			        	
			            $("#successdisplaySharedDocDiv").html("You have successfully registered. Your activation link has been sent to your mail id. "+"<input type=\"button\" class=\"button_style\" style=\"margin-top: 9px;\" value=\"OK\" onclick=\"successlightbox_close('success')\" />");	
			            
	        		}
	        	else	        		
	        		{
	        		    $("#waitingdivId").hide();
	        		    $("#waitingdivContentId").hide();
	        		    
	        		    if(data.indexOf("Document")>=0)
	        		    {
	        		    	$("#kidDiv").show();
	        		        $("#kidDiv").html(data);	
	        		    }
	        		    else if(data.indexOf("Mobile")>=0)
	        		    {
	        		    	$("#phnoDiv").show();
	        				$("#phnoDiv").html(data);
	        		    }
	        		    else if(data.indexOf("Email")>=0)
	        		    {
	        		    	$("#emailDiv").show();
	        			    $("#emailDiv").html(data);
	        		    }
	        		    else if(data.indexOf("captcha")>=0)
	        		    {
	        		    	$("#captchadiv").show();
	        		        $("#captchadiv").html(data);	
	        		    }
	        		    else
	        		    {
	        		        alert("Certain problem occured please try again later !");	
	        		    }
	        		    
	        		   
	     	            reloadCaptcha();
	     	            document.getElementById("indcaptchCode").value = "";
	     	            
	        		   
	        		}
	        },	        	        
	        cache: false,
	        contentType: false,
	        processData: false
	    });

	}
}

		
function openLogin(id)
{
	$('#searchhomepage').html("");
	$('#searchhomepage').hide();	
	document.getElementById("searchedIndividualId").value = id;	
    $('#tab-container').show();	
}


function validateForgetEmailId()
{
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var indemailId = document.getElementById("forgetPasswordId").value;	
	
	if(indemailId!="")
		{
			if(!indemailId.match(emailId))  
		    {  			
				$("#regErrorId").show();
		        $("#regErrorId").html("Please enter a valid email id");	
		        return false;
		    } 
			else
			{
				$("#regErrorId").hide();
				return true;
			}
		}
	else
		{
			$("#regErrorId").hide();
		}
	
}

function forgetPassword()
{	
	$("#waitingdivId").show();
	$("#waitingdivContentId").show();
	 
   var emailIdRegex = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
   var emailId = document.getElementById("forgetPasswordId").value;	
   
   if(emailId!="")
	  {	   
		  if(!emailId.match(emailIdRegex))  
		    {  	
			    $("#waitingdivId").hide();
			    $("#waitingdivContentId").hide();
			    
				$("#regErrorId").show();
		        $("#regErrorId").html("Please enter a valid email id.");	
		        return false;
		    } 
		 else
			{
						$.ajax({
						       url: "forgetpwValidateEmailId.html",
						       type: 'post',
						       data: "emailId="+emailId,        
						       success: function (data) { 
						       	if(data=='Yes')
						       		{	  
						       		       						       		       		       				       		
								       		$.ajax({
												
										        url: "formatPassword.html",
										        type: 'post',
										        data: "emailsignup="+emailId,		              
										        success: function (data) { 		        
										        if(data=="success")
										        {
										        	$("#waitingdivId").hide();
										        	$("#waitingdivContentId").hide();
										        	 
										        	document.getElementById('successlight').style.display='block';
										            document.getElementById('successfade').style.display='block';
										            $("#successdisplaySharedDocDiv").html("Password reset link has been sent to your registered email id." +"<input type=\"button\" value=\"OK\" style=\"margin-top: 9px;\" class=\"button_style\" onclick=\"successlightbox_close('success')\" />");			        			    		
										        }
										        else
										        {
										        	$("#waitingdivId").hide();
										        	$("#waitingdivContentId").hide();
										        	 
										        	document.getElementById('successlight').style.display='block';
										            document.getElementById('successfade').style.display='block';
										            $("#successdisplaySharedDocDiv").html("Your Request has failed due to certain reason,Please try again later !" +"<input type=\"button\" value=\"OK\" style=\"margin-top: 9px;\" class=\"button_style\" onclick=\"successlightbox_close('warning')\" />");		        	 		
										        }		        			        	
										    },	        		        
										       
										    });
						       		}
						       	else
						       		{
						       		    $("#waitingdivId").hide();
						       		    $("#waitingdivContentId").hide();
						       		 
						       			$("#regErrorId").show();
							        	$("#regErrorId").html(data);								            
						       		}           	
						       },       
						    });
			  }		 			     
		  }
	  else
		  {
		        $("#waitingdivId").hide();
		        $("#waitingdivContentId").hide();
		       
		        $("#regErrorId").show();
 		        $("#regErrorId").html("Please enter your registered email id.");		        		       
		  }
}

</script>


  <script type="text/javascript">
     $(document).ready( function() {
      $('#tab-container').easytabs();
    }); 
  </script>


<script>

function hideAllErrorDiv(tabName)
{		
	$("#loginerrordiv").hide();
	$("#regErrorId").hide();
	$("#loginsuccessdiv").hide();
	
	if(tabName=='signIn')
		{
		   $("#tabs1-js").show();
		   $("#tabs1-html").hide();
		   $("#tabs1-css").hide();
		   
		   document.getElementById("userId").value = "";
      	   document.getElementById("userPw").value = "";
   
      	  
      	    document.getElementById("fname").value = "";
			document.getElementById("mname").value = "";
			document.getElementById("lname").value = "";
			document.getElementById("phno").value = "";
			document.getElementById("kid").value = "";
			document.getElementById("email").value = "";
			document.getElementById("pw").value = "";
			document.getElementById("repw").value = "";
			document.getElementById("gen").value = "";
			document.getElementById("indcaptchCode").value = "";
			
			document.getElementById("forgetPasswordId").value = "";
		}
	else if(tabName=='signUp')
		{
		   $("#tabs1-js").hide();
		   $("#tabs1-html").show();
		   $("#tabs1-css").hide();
		   
		   document.getElementById("userId").value = "";
      	   document.getElementById("userPw").value = "";
   
      	   document.getElementById("forgetPasswordId").value = "";
		}
	else 
		{
		   $("#tabs1-js").hide();
		   $("#tabs1-html").hide();
		   $("#tabs1-css").show();
		   
		   document.getElementById("userId").value = "";
      	   document.getElementById("userPw").value = "";
   
      	  
      	    document.getElementById("fname").value = "";
			document.getElementById("mname").value = "";
			document.getElementById("lname").value = "";
			document.getElementById("phno").value = "";
			document.getElementById("kid").value = "";
			document.getElementById("email").value = "";
			document.getElementById("pw").value = "";
			document.getElementById("repw").value = "";
			document.getElementById("gen").value = "";
			document.getElementById("indcaptchCode").value = "";
		   		   
		}
	
}

function reloadCaptcha(){
    var d = new Date();
    $("#captcha").attr("src", "/simpleCaptcha.jpg?"+d.getTime());
}


function gotoHomePage()
{
	window.location = "clenseeHome.html";
}

function hideSearchBar()
{
	$(".topdivespan").hide();		
	$('#searchhomepage').hide();		
}

</script>

</head>

<body>
  
<%
  String kycComp = (String)request.getAttribute("kycdocumentList");
  String kycCompArray[] = kycComp.split(","); 
%>


<div id="centerbody">
 <div class="container">
		<div class="top_line"></div>
		
		<header class="header">
			
			<div class="header_inner">
				<div class="logo" onclick="gotoHomePage()">
				</div>
			  
			  <div class="search-bar">
				 <div id="tfheader">
					
						 <input type="text" id="searchvaluetext" maxlength="50" class="tftextinput2 button-submit" name="q" size="21" maxlength="120" title="Enter atleast 3 character for efficent search !" autofocus="autofocus" placeholder="Search Individual" onkeyup="searchhomepage()"/>
                         <input type='image' class="search_button-submit" alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="searchhomepage()"/>
						 						
						 <div class="tfclear"></div>
				 </div>     
			 </div>
                <div class="tfclear">
                  </div>
					 <div class=" mainmenu">
								          
							 <ul class="main_menu">
                             
								<li><a class="individualactive button-submit" href="individual.html"></a></li>
								<li style="border-right:1px solid #333;width:1px; height: 57px;   background-color: #777;"></li>
								<!-- <li><a class="corporate" href="clenseeHome.html"></a></li> 
								<li style="border-right:1px solid #333;width:1px; height: 57px;   background-color: #777;"></li>-->
								<li><a class="utiliser button-submit" href="utiliser.html"></a></li>
							   
							 </ul> 
							   
					 </div>       				
				 </div>
			</header>
			
			
			
			<div style="width:100%;height:100%;" class="banner_center_individual" onclick="hideSearchBar()">
									
            <div class="main" onclick="hideSearchBar()">
                         
                        
                         <div id="searchhomepage"  style="display: none;width:75% !important">
                        
                        
                         
                         </div>
                         
                                    
			<div class="leftside"> 
			
					    	
			<img src="resources/images/frontpage-kyc.png"></img>
			
			</div>
				
				 <div class="rightsideindex">
						<%String status = (String)request.getAttribute("status");
			if(status!=null){
			%>
			<div id="loginsuccessdiv"  class="successmage"><%=request.getAttribute("status")%></div>
			<%} %>
			
			
			<div id="regErrorId" style="display:none;
						color:red;
						border-radius:10px;
						font-family:Tahoma,Geneva,Arial,sans-serif;font-size:13px;
						padding:6px; 
					    margin-top:5px;  
					    width:415px;text-align: left;"></div>	
					    																		
		     <div id="tab-container" class='tab-container'>
		     
				 <ul class='etabs'>
				   <li class='tab'><a href="#tabs1-js"  class="button-submit"  onclick="hideAllErrorDiv('signIn')">Sign In</a></li>
				   <li class='tab'><a href="#tabs1-html" class="button-submit"  onclick="hideAllErrorDiv('signUp')">Sign Up</a></li>				   
				   <li class='tab'><a href="#tabs1-css" class="button-submit"  onclick="hideAllErrorDiv('resetPassword')">Reset Password</a></li>
				 </ul>
				<div class='panel-container'>
				
				<div id="tabs1-html" style="display:none;" onclick="hideSearchBar()">
				  
				
				<form id="signUpForm" name="signUpForm" >
				
				<table>
				<tbody>
				
				<tr>
				   <td>
				        <p style="color:red;">All fields are mandatory except middle name. </p>
				   </td>
				</tr>
				
				<tr>
				<td>
				<p class="uname" for="usernamesignup">
				<input class="field" id="fname" type="text"  autofocus="autofocus" maxlength="30" placeholder="First Name *" name="firstname" onkeyup="validateOnEach()"/><div id="fnameDiv" class="error"style="display:none;margin-left:10px;margin-top:36px;"></div>
				</p>
				</td>
				<td>
				<p class="uname" for="usernamesignup">
				<input class="field"  id="mname" type="text"  placeholder="Middle Name" maxlength="30" name="middlename" onkeyup="validateOnEach()"/><div id="mnameDiv" class="error" style="display:none;margin-left:10px;margin-top:36px;"></div>
				</p>
				</td>
				</tr>
				<tr>
				<td>
				<p class="uname" for="usernamesignup">
				 <input class="field" id="lname" type="text"  placeholder="Last Name *" maxlength="30" name="lastname" onkeyup="validateOnEach()"/><div id="lnameDiv" class="error" style="display:none;margin-left:10px;margin-top:36px;"></div>
				</p>
				</td>
				
				<td>
				<!-- <p class="signin button"> -->
				 <div class="styled-select">
				<select  required="required" id="gen" name="gender" onchange="validateGender()">
				<option value="Select your Gender">Select gender*</option>				
				<option value="MALE">Male</option>
				<option value="FEMALE">Female</option>
				</select>
				</div>
					<!-- <span class="dropdownarrow">Select your Document</span>		 -->		
				<div id="genderDiv" class="error" style="display:none;margin-left:10px;margin-top:49px;"></div>	
				<!-- </p>	 -->			
				</td>
				</tr>
				
				<tr>				
				<td>
				<!-- <p class="uname" for="usernamesignup"> -->
				
			<div class="styled-select">
				<select  required="required" id="typeOfKYCId" placeholder="Select your Document" name="typeofId" onchange="validateKYCID()">
				<option value="Select your KYC Document">Select document type*</option>
				<%for(int i=0;i<kycCompArray.length;i++){ %>
				      
				      <option value="<%=kycCompArray[i]%>"><%=kycCompArray[i].toLowerCase()%></option>
				      
				<%} %>
					
				</select>
				</div>
				<div id="typeofKycDiv" style="text-transform: none ! important;" class="error" style="display:none;margin-left:10px;margin-top:38px;"></div>						
				<!-- </p> -->
				</td>
				
				<td>
				
				<p class="uname" for="usernamesignup">
				<input class="field" style="text-transform: none ! important;" id="kid" type="text" placeholder="Document Id *" maxlength="30" name="kycid" onkeyup="validateKycidOnKeyUp()" onchange="validateKycid()"/><div id="kidDiv" class="error" style="display:none;margin-left:10px;margin-top:36px;"></div>
				</p>
				</td>
				
				</tr>
				
				<tr>
				<td>
				<p class="uname" for="usernamesignup">   
				<input class="field" id="phno" type="text" placeholder="Mobile Number *"  name="mobileno" maxlength="10" onkeyup="validateMobileNoOnkeyUp()" onchange="validateMobileNo()" /><div id="phnoDiv" class="error" style="display:none;margin-left:10px;margin-top:36px;"></div>
				</p>
				
				</td>
				
				<td>
				<p class="uname" for="usernamesignup">
				<input class="field" id="email" type="text" style="text-transform: none ! important;" placeholder="Email Id *"  name="emailid" maxlength="50" onchange="validateEmailId(this.id)"/><div id="emailDiv" class="error" style="display:none;margin-left:10px;margin-top:36px;"></div>
				</p>
				</td>
				</tr>
				<tr>
				<td>
				<p class="uname" for="usernamesignup">
				<input class="field" id="pw" type="password" style="text-transform: none ! important;" placeholder="Password *"  name="password" maxlength="15" onkeyup="validateOnEach()"/><div id="pwDiv" class="error" style="display:none;margin-top:35px;margin-left:10px;"></div>
				</p>
				</td>
				<td>
				<p class="uname" for="usernamesignup">
				<input class="field" id="repw" type="password" style="text-transform: none ! important;" placeholder="Confirm password *" name="re-password" maxlength="15" onkeyup="validateOnEach()"/><div id="repwDiv" class="error" style="display:none;margin-top:35px;margin-left:10px;"></div>
				</p>
				</td>
				</tr>				
				
				<tr>	
							   
				    <td>				       
				    	<img id="captcha" class="captcheClass" style="margin-top:8px;width:265px;margin-left:-4px;height:32px;" src="<c:url value="simpleCaptcha.jpg"/>"  /> <img src="resources/kyc_images/refreshCaptche.gif" style="cursor: pointer;" onclick="reloadCaptcha()" alt="reload" width="20" height="20"/>  	
				    </td>
				   
				    <td>
				    		<input class="field" placeholder="Enter the captcha code *" id="indcaptchCode" maxlength="6" style="text-transform: none ! important;" type="text" name="answer" onchange="validateCaptcheCode()" onkeyup="validateOnEach()"/>
				    		<div id="captchadiv" class="error" style="display:none;margin-left:3px;"></div>
				    </td>
				    								       				    				
				</tr>
				
					<tr>
					    <td>
					    		
					    </td>
					</tr>
					
				<tr>
									
					<td>
					
						    <p class="uname" for="usernamesignup" style="width: 102% ! important;">
						    
							   <input type="checkbox" id="checkId" onclick="checkboxStatus()"/>
						
							   <span style="color:#03b2ee;"><a href="#" onclick="showtermsAndCondForSignUp()">I agree with Clensee terms & conditions,privacy policy.</a></span>
			               
						    </p> 
					
					</td>								
					
					<td>
								
					<input type="button" class="signin button-submit" id="saveButtonId"   value="Sign Up" onclick="saveSignUp()" /><!-- onclick="saveSignUp()" -->
					<input class="signin button-submit" type="reset" value="Reset"/>
					</td>
					
				</tr>
				
				</tbody>
				</table>
				</form>
				  
				</div>
				
				<div id="tabs1-js" style="display:block;">
				
				 <%String authenticateStatus = (String)request.getAttribute("errormessage");
			if(authenticateStatus!=null){%>		
			<div  id="loginerrordiv"  class="errormage"><%=authenticateStatus%></div>
			<%} %>
			
				<form method="post" action="/KYC/authenticateLogin.html" autocomplete="off">
				
				<p>
				<input class="field" id="userId" type="text" autofocus="autofocus" maxlength="50" autocomplete="off" placeholder="Registered Email Id / KYC Id" required name="kycid"/>
				</p>
				<p>
				<input class="field" id="userPw" type="password" placeholder="Password" maxlength="15" autocomplete="off" required name="password"/>
				</p>
				<p>
				<input type="hidden" id="searchedIndividualId" name="searchedIndividualName"/>						 
				</p>
				<p class="keeplogin">
				<!-- <input id="loginkeeping" type="checkbox" value="loginkeeping" name="loginkeeping">
				Keep me logged in -->
				<input type="submit" class="button-submit" value="Sign In">
				</p>  
				
				<p style="margin-top:18px;text-align: left;color:#7c7c7c">
				   By Logging In, you agree to our <a href="clenseeTermConditions.html" target="_blank">Terms&Conditions</a> and that you understood our <a href="clenseePrivatePolicy.html" target="_blank">Privacy policy</a>
				</p>
				</form>
				
				</div>
				
				
				<div id="tabs1-css" style="display:none;">
				  				
					<form name="forgetPasswordForm" id="forgetPasswordForm" >
					
						<p>
						<input class="field button-submit" type="email" id="forgetPasswordId" maxlength="50" placeholder="Your Email Id" required name="emailsignup" onchange="validateForgetEmailId()" />
						</p>
					
						<p class="signin button">
						<input type="button" class="button-submit" value="Reset" onclick="forgetPassword()"/>
						</p>
					
					    <p style="margin-top: 60px;text-align: left;color:#7c7c7c">
					        We will send a link on your registered email to reset your password.
					    </p>
					    
					</form>
				
				</div>
				
				
				
			</div>

		</div>
	
					
	      </div>   
		</div>				
		</div>	
	    
	    
	    <!-- Terms & Conditions Message ! -->
	    
	     <div style="display:none;" id="fade" class="black_overlay"></div>
	    
	     <div style="display: none;" id="light" class="white_content">
					
					 <div class="termsStyle"><h1>Clensee Terms & Conditions,Privacy Policy</h1></div>
						<div class="termsscroll">
					    
					    
					    <h3><u>Terms & Conditions</u></h3>
					    
					      By accessing this website, you agree to be bound by these website Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trade mark laws.
					    
						
						  <br></br><b style="color:#00b6f5;">Access and Interference or Wrongful Conduct</b><br></br>

                          User shall not commit or permit wrongful or damaging acts which justify civil action including, but not limited to, posting of defamatory, scandalous, or private information about a person without their consent or intentionally inflicting emotional distress. Violations or attempts to violate CLENSEE systems or to interrupt CLENSEE services are strictly prohibited, and may result in criminal and civil liability.
						
						  <br></br><b style="color:#00b6f5;">Passwords</b><br></br>

						  We are not at all responsible for Misuse of Password and safeguarding the password that you use to access the Services and for any activities or actions under your password. We encourage you to use "strong" passwords (passwords that use a combination of upper and lower case letters, numbers and symbols) with your account. CLENSEE cannot and will not be liable for any loss or damage arising from your failure to comply with the above.
						
						 <br></br><b style="color:#00b6f5;"> Your Rights</b><br></br>

						  You retain your rights to any Content/Documents that you share, post, and store on or through the Services. By submitting, posting or sharing Content/Documents on or through the Services, you grant us across India, non-exclusive, royalty-free license (with the right to sublicense) to use, copy, reproduce, process, adapt, modify, publish, transmit, display and distribute such Content in any and all media or distribution methods (now known or later developed).
						
						  <br></br><b style="color:#00b6f5;">Copyright Policy</b><br></br>

						  We want to ensure that the content that we host can be re-used by other users without fear of liability and that it is not infringing the proprietary rights of others. In fairness to our users, as well as to other creators and copyright holders, our policy is to respond to notices of alleged infringement that comply with the formalities of the Digital Millennium Copyright Act (DMCA). Pursuant to the DMCA, we will terminate, in appropriate circumstances, users and account holders of our system and network who are repeat infringers.
						
						  
						  <br></br><b style="color:#00b6f5;">Geographical Limit</b><br></br>

						  Till date our website is limited to INDIA only.

						  <br></br><b style="color:#00b6f5;">Wavier</b><br></br>

					      The failure of CLENSEE to enforce any right or provision of these Terms will not be deemed a waiver of such right or provision. In the event that any provision of these Terms is held to be invalid or unenforceable, then that provision will be limited or eliminated to the minimum extent necessary, and the remaining provisions of these Terms will remain in full force and effect.

						  <br></br><b style="color:#00b6f5;">Limitation of liability</b><br></br>

                          We will not be liable to you or to any other party for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data, or other intangible losses, regardless of whether we were advised of the possibility of such damage.
					
						<br></br>
						
						 <p style="border-bottom: 1px solid #ccc !important;width:100%;"></p>
										
						<h3><u>Privacy Policy</u></h3>
						   
						   
						   <b style="color:#00b6f5;">We Collect</b><br></br>

						   We collect the personal information that you provided upon creation of your own account on our website, which includes your profile, key documents etc. You can present all your profile and share documents to our registered Utilizers and other individuals which you intend to share. Some basic profile information is visible to all the users (Both Individual and Utilizers), however, your profile visibility can be controlled by visibility settings.

                           If you are not at all intended to provide any information or share any documents, you are not obligated to do so.

                           <br></br><b style="color:#00b6f5;">Modification of Information</b><br></br>

							Your Information can be modified by only you alone, however latest data and previous data stored in our cloud is visible to users. If you do not wish to share the previous data, please approach us on support@clensee.com for any corrections, if you did not share your profile or Docs to Utilizers previously.
							
							Our terms clearly specify that if you share your data to the Utilizers even if you want your previous data not to present for Utilizers. Correction is only possible by acceptance of all Utilizers that you have given sharing.

						    <br></br><b style="color:#00b6f5;">Cookies</b><br></br>

							There is a technology called "cookies" which can be used to provide you with tailored information from a Web site. A cookie is an element of data that a Web site can send to your browser, which may then store it on your system. Some CLENSEE pages use cookies so that we can better serve you when you visit and then return to our site. We use cookies to deliver information specific to your interests and to save your password so you don't have to re-enter it each time you visit certain areas of our Web site. Your use of this site constitutes your acceptance.
							
							If you do not want to accept cookies from our website, you may refuse a cookie, or delete the cookies file from your computer at any time using available methods. Most browsers have a setting to turn off the automatic acceptance of cookies. You should be aware few portions of our website may not function properly if you do not accept cookies.

 
							<br></br><b style="color:#00b6f5;">Age Limit</b><br></br>

							Our Clensee product services are applicable to all persons whose age limit is above 18 years.

							<br></br><b style="color:#00b6f5;">Security</b><br></br>

							We have implemented technology and current internet security features and strict policy guidelines to safeguard the privacy of your personally identifiable information from unauthorized access and improper use or disclosure.

							<br></br><b style="color:#00b6f5;">Law and Jurisdiction</b><br></br>

							We shall be governed by and construed in accordance with the laws of India, without regard to principles of conflicts of law, within the jurisdiction of the Courts of Hyderabad.

							<br></br><b style="color:#00b6f5;">Change to Policy</b><br></br>

                            We will keep you updated on changes in our policies, if any.

					
						
		
					
	</div>
		 <div>
		 					  <input class="white_contentaccept" type="button" value="I ACCEPT" onclick="showAcceptence('Accept'), document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" />
		 			  <input class="white_contentdontaccept" type="button" value="I DONT ACCEPT" onclick="showAcceptence('dont Accept'), document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" />
	
		 </div>
		 </div>
		
		
		
		 
	     	<%
	     	
			   ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			   String version=resource.getString("kycversion");
			   String versionDate=resource.getString("kyclatestDate");
			   
			%>	
			
			
          <footer class="footer">
		   
		  
				
			<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p style="margin-top:25px;">Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p style="margin-top:35px;">
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactusbeforeLogin.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 905px !important; margin-top: -1px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		   
			<div class="footer_line">
			</div>

		 </footer>
           
	</div>	
</div>		
<!------center End-------->
		
 
<!------container End-------->

   <div id="successlight" style="height:auto ! important;">
   
   
			<!-- <div class="colsebutton" onclick="successlightbox_close()"><img height="22" width="22" class="successmassage_climge"  src="resources/images/close_button.png" /></div>
		    --> 
		    <div id="successdisplaySharedDocDiv"  class="successmassage" style="background: none repeat scroll 10px 50% yellowgreen;"> 
		  
		    </div>		
		     
		   
		       
	</div>
	
	<div id="successfade"></div> 	

<!-- Waiting Div -->
	
	
	 <div id="waitingdivContentId" style="display:none;">
				    
				     <div class="kycloader1">
		
		                   
		                   <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						   </div>
										                   
		                   
		             </div>
		             
		              				    
		    </div>
		    
			<div id="waitingdivId" >
											    
				   
			     
		    </div>
		    
							        
    <div id="signupwaitingId" >
	    
		    <div id="signupwaitingContentId">
				    
				     <div class="kycloader">
		
		             </div>
		             <h4>Your registration is in progress .Please wait ... </h4>
		              				    
		    </div>
	     
     </div>
       
           
    <!-- EOF Waiting Div -->
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight">
			
			<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpage_links"  src="resources/images/close_button.png" /></div>
		    
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
<!-- EOF kycDiv Div -->	

</body>
</html>
 