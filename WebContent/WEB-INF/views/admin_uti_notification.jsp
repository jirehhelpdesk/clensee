<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Uti Notification</title>

<link href="resources/css/kyc_admin_style.css" rel="stylesheet"type="text/css" />
<link rel="stylesheet" href="resources/css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">

<script>
	
function searchbyEvent()
{
	$("#waitingdivId").show();
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var type=document.getElementById("selectedidindividual");
						var searchvalue = document.getElementById("uti_search");
						var index=0;
						var type1 = type.value;
						var searchvalue1 = searchvalue.value;
							
						 $.ajax
					     ({
							type : "post",				
							url : "Profile_searchUtiliser.html",			
							data :"searchdata="+searchvalue1+"&type="+type1,	  
							success : function(response) {
								
								$("#waitingdivId").hide();
								
								var usersTokens = response.split(",");
								
								 var usersTableData = "<table border=\"0\" style=\"margin-left:0px\"><tr><th>KYC-ID</th><th>FIRST-NAME</th><th>LAST-NAME</th><th>EMAIL-ID</th><th>PLAN</th><th>DOMAIN</th><th>CONTACT-NUMBER</th><th>Send Notification</th></tr>";
									for ( var i = 0; i < usersTokens.length; i = i + 8) {
												
										var id = usersTokens[7 + i];
										id = id.replace(' ', '');
										
										index += 1;
										usersTableData += "<tr>"
												+ "<td>" + usersTokens[0 + i]
												+ "</td><td>" + usersTokens[1 + i]
												+ "</td><td>" + usersTokens[2 + i]
												+ "</td><td>" + usersTokens[3 + i]
												+ "</td><td>" + usersTokens[4 + i]
												+ "</td><td>" + usersTokens[5 + i]
												+ "</td><td>" + usersTokens[6 + i]
												+ "</td><td>" + "<input type=\"button\" value=\"Notify\" id=\""+id+","+ usersTokens[1 + i]+" "+usersTokens[2 + i]+"\" onclick=\"utiNotify(this.id)\" />"
												+ "</td></tr>";
									}
									
									usersTableData += "</table>";
									
									$("#applyCodeid").html(usersTableData);
															
							},
							
						});
				}
			else
				{				
				   $("#waitingdivId").hide();
				
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	});
	
	
	
	}
	
function specificUtiNotify()
{   
	$("#notifydivId").hide();
    $("#notifyspecificUtiId").show();    
}

function allUtiNotify()
{	
    $("#notifyspecificUtiId").hide();  
    
	document.getElementById("utiToValue").value = "All Utilizer";
	document.getElementById("utiToNameValue").value = "All Utilizer";
	
	$("#notifydivId").show();
}

function utiNotify(idValue)
{
   var notifyDetails = idValue.split(","); 
   
   document.getElementById("utiToValue").value = notifyDetails[0];
   document.getElementById("utiToNameValue").value = notifyDetails[1];
   
   $("#notifydivId").show();   
}

function notifyToUtilizer()
{
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				    $("#waitingdivId").show();
				
						var formData = new FormData($("#notifyForm")[0]);
						
					    $.ajax({
					        url: "notifyToUtilizer.html",
					        type: 'POST',
					        data: formData,
					        //async: false,
					        success: function (data) {          
					            
					        	$("#waitingdivId").hide();
					        	
					        	if(data=="success")
					        		{
					        			alert("Admin Notification has sent successfully.");
					        		}
					        	else
					        		{
					        			alert("Admin Notification has failed,try again !");
					        		}				        	
					        },
					        cache: false,
					        contentType: false,
					        processData: false
					    });	
				}
			else
				{
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	});
	
}

</script>

</head>
<body>
<br>

        <div id="notificationpage">
           <input type="button" value="Notify Specific Utilizer" onclick="specificUtiNotify()"/>
           <input type="button" value="Notify All Utilizer" onclick="allUtiNotify()"/>
      </div>

      <div id="notifyspecificUtiId" style="display: block;float: left;margin-left: 214px;margin-top: 32px;">

			<table>
				<tr>
					<td>
						<div id="profilesearch">			
							<div id="profilesearch">
								<input type="text" name="uti_search" id="uti_search" class="profilesearchtextinput2" name="q" size="20" maxlength="80" value="" />
								<input type='image' alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="searchbyEvent()" />
							</div>
						</div>
					</td>
					<td>
					    <select id="selectedidindividual" name="utiCategory">
							<option value="Name">Name</option>
							<option value="KYCID">KYCID</option>
							<option value="Plan">Plan</option>
							<option value="Domain">Domain</option>
					    </select></td>
				</tr>
			</table>


			<div id="applyCodeid" class="datagrid" style="margin-left:46px;px;margin-top:16px;margin-right:10px;">
		
		
		    </div>
		    
    
    </div>
    
    
    <div id="notifydivId" style="display:none;margin-left:74px;margin-top:32px;width:725px;background-color: #fff;border: 2px solid #00b6f5;border-radius: 14px;">
	       
	       <form name="notifyForm" id="notifyForm" >
	       
			      <ul style="text-align:left;width:665px;">
			      
					       <li>
					              <input type="hidden"  value="Utilizer"   name="notification_type" />
					              <input type="hidden" id="utiToValue" value="" name="notification_to"/>
					           To:<input type="text" id="utiToNameValue" value=""  readonly/>
					       </li>
					       
					       <li>    
					           Notification Subject:
					           <input type="text" name="notification_subject"  maxlength="90" style="width:652px;height:20px;"/>
					       </li>
					       
					       <li>    			           
					           Notification Message:
					           <textarea name="notification_message" style="width:652px;height:60px;">
					           
					           </textarea>
			               </li>	
			               
			               <li>    			           
					            <input type="button" value="Notify" style="align:right;" onclick="notifyToUtilizer()"/>
			               </li>    
			                          
	              </ul> 
	            
	       </form>
	       	        
	 </div>  
	 
	 
</body>
</html>