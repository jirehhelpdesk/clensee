<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>${regname.firstname}'s Profile</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />
<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>
<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/onlineresources/code.jquery.com.jquery-1.10.2.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<link rel="stylesheet" href="resources/documentResources/kyc_document_css.css" />


<style>

#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}


</style>



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 33%;
    width: 800px;
    z-index: 1002;
}

</style>



<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}



</script>

<script>

function showHideDiv1(id){
	

	myTextBox1 = document.getElementById("b6");
	value=myTextBox1.value;
	
	  var obj = document.getElementById(id);
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	    } else if(obj.style.display=="block"){
	      obj.style.display='none';
	    }
	}


</script>

<script>
 $(document).ready(function () {
        $("#dialog").dialog({ autoOpen: false });
 
          
                return false;
           
    });
</script>

<script type="text/javascript">

function viewPro1(){
		
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
        
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearchprofilefromUtilizer.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
}


function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function aboutUsfadeout()
{
	document.getElementById('aboutUslight').style.display='none';
	document.getElementById('aboutUsfade').style.display='none';
}


</script>





<script>

function divvalue()
{
	
	document.getElementById("landpagedescription").value=document.getElementById("editsumdiv").innerHTML ;
	alert(""+document.getElementById("landpagedescription").value);
}

/*home page*/

function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/

</script>
         
 <script>
function openbrowse()
 
 { 
	 
	 var obj = document.getElementById('openbrowse');
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	      document.getElementById("removeaddphoto").style.display="block";
	     
	    }	  
 }
</script>
 
<script src="resources/texteditor/nicEdit.js" type="text/javascript"></script>

<script>

/* Main Header Actions  */


function redirectToLoginIndividual()
{
	window.location = "utisignOut.html";	
}

function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}


</script>

<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}


</style>

<script>

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewKycDocumnt(fileName)
{	
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
		 
	    var individualId = document.getElementById("indId").value;
	    
		$.ajax({
			type : "post",				
			url : "viewDocFromUtilizer.html",
			data: "fileName="+fileName+"&individualId="+individualId,
			success : function(response) {
			
				document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
	
				document.getElementById('kycDocPopUplight').style.display='block';
		        document.getElementById('kycDocPopUpfade').style.display='block';
		        $("#kycDocPopUpDisplay").html(response);
		        			
			},		
		});		 		 		 					
}


function kycHistory(compType)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';	       
    
    
    
	 $.ajax({
			type : "post",				
			url : "kychistoryfromUti.html",				
			data :"fileName="+compType,	  
			success : function(response) {
			
				document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';	       
			    
			    document.getElementById('kycHistoryLight').style.display='block';
				document.getElementById('kycHistoryFade').style.display='block';
				$("#kycHistoryDisplay").html(response);
			    
							
			},
		
		});		 		
}

function fadeOutHistoryDiv()
{
	document.getElementById('kycHistoryLight').style.display='none';
	document.getElementById('kycHistoryFade').style.display='none';
}


function showProfileAgain()
{	
	window.location = document.getElementById("redirectUrl").value;	
}

function showKycDocuments()
{
	var individualId = document.getElementById("indId").value;
	//window.location = "viewKycDocumentsFrmUti.html";	
	window.location = "redirectviewKycDocumentsFrmUti.html?accgiver="+individualId;	
}

function showAcademicDocuments()
{
	var individualId = document.getElementById("indId").value;
	//window.location = "viewAcademicDocumentsFrmUti.html";	
	window.location = "redirectviewAcademicDocumentsFrmUti.html?accgiver="+individualId;	
}

function showEmpDocuments()
{
	var individualId = document.getElementById("indId").value;
	//window.location = "viewEmpDocumentsFrmUti.html";	
	window.location = "redirectviewEmpDocumentsFrmUti.html?accgiver="+individualId;	
}

function showFinancialDocuments()
{
	var individualId = document.getElementById("indId").value;
	//window.location = "viewfindocumentsfrmUti.html";	
	window.location = "redirectviewfindocumentsfrmUti.html?accgiver="+individualId;	
}


function profilePicHistory()
{
	var individualId = document.getElementById("indId").value;
	 $.ajax({
			type : "post",
			url : "getIndProfilePicturesHistory.html",	
			data: "indvidualId="+individualId,
			success : function(response) {		
						
			    $("#content").html(response);
			
			},
			
		});
}

function activeSidebutton()
{
	document.getElementById("empActive").style.background="-moz-linear-gradient(center top , #f4f4f4, #e9e9e9) repeat scroll 0 0 rgba(0, 0, 0, 0)";
	document.getElementById("empActive").style.color="#05b7f5";
	document.getElementById("empActive").style.border="1px solid #ccc";
}

function HistoryDocUti(docname,kycid)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';	       
    
    var individualId = document.getElementById("indId").value;
    
	$.ajax({
    	
    	type: "post",	   
    	url: "historyDocforUti.html",	    	
    	data: "docname="+docname+"&extp="+individualId,	 	    		       
        success: function (response) {       	
        	
        	document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';	       
		    
		    $("#HitorydateId").html("");
        	$('#viewUsers1').html(response);			
        },
                
    });
	
	}
	

	
</script>

<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
</head>

<body onload="hideAddvertise(),activeSidebutton()" onclick="hidesearchDiv()">

<%System.out.println("It Hits;");%>
               
               
<%

	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };

    String regVisibility = (String)request.getAttribute("regVisibility");
    String basicVisibility = (String)request.getAttribute("basicVisibility");
    String familyVisibility = (String)request.getAttribute("familyVisibility");
    String socialVisibility = (String)request.getAttribute("socialVisibility");
    
    
    String profileAccess = (String)request.getAttribute("profileAccessDetails");
    String otherAccess = (String)request.getAttribute("otherAccess");
   
%>


<input type="hidden" id="Ind_id" value="<%=session.getAttribute("tempIdasdgthfmUti")%>"/>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		
		<%
		   String noOfAwaitedDocuments = (String)request.getAttribute("noOfAwaitedDocuments");
		   String noOfRecivedDocuments = (String)request.getAttribute("noOfRecivedDocuments");
		%>
		
		
		<div class="header_inner">
		
		  <a href="utimyProfile.html"><div class="logo"></div></a>

			<div class="tfclear"></div>
			<div class="mainmenu" style=" margin-right: 28px;">
				
				<ul class="uti_main_menu">
					
					<li id="myMenus1"><a class="myprofilefocus" href="utimyProfile.html"> <br>
							<br> My Profile
					</a></li>
					
					<li id="myMenus2" style="width:140px">
					     
					        <a class="mydocuments" href="reciveddocuments.html" > <br>
							<br> Received Documents
					        </a>
					        
					        <%if(!noOfAwaitedDocuments.equals("0")){ %>
												  
							     <div class="noti_bubble_right"><%=noOfAwaitedDocuments%></div>
							     
							<%} %>
							
					 </li>
					 
					<li id="myMenus2" style="width:140px">
					</a></li>
					
					<li id="myMenus4"><a class="myplans" href="utilizerPlan.html"> <br> <br>
							My Plans					
					</a></li>
					
					<li id="myMenus6"><a  class="alearticons" href="utilizeralerts.html"><br>
							<br> Alerts
					</a>
					</li>	
					
					<li id="myMenus5"><a class="mysettings" href="utilizerSettings.html"  > <br> <br>
							My Settings					
					</a>
					</li>
					
				</ul>
				

			</div>

		</div>
		
 <!-- Login Starts Here -->
 
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=(String)request.getAttribute("utiFirstName")%></em></a>
                 <div style="clear:both"></div>
           <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            
            <span class="topprofilestyle" style="display: none;"></span>		
						<div id="searchicon" style="display:none;">
																
							 <div id="searchhomepage" style="display:block;">
							
							 </div>
													
				        </div>
				        
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="utisignOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
		</header>
		
	<div class="main">
				
				        
			<div class="main_center">
			      			
					<div class="leftside">
					
					<div id="textBox">
					<c:if test="${!empty profilePicInfo.profile_picture_name}">
	 
						 <%
	 						 	String filename1 = (String) session.getAttribute("profilepic");
	 						 	int searchedIndId = (Integer)request.getAttribute("searched_Indid");
						        String user0 = Integer.toString(searchedIndId);
	 					 %>   
	 					  <c:set var="profilePicture"  value="${profilePicInfo.profile_picture_name}"/>
	 					  <%String profilePicture = (String) pageContext.getAttribute("profilePicture");%>
									
										
						 <div class="user_photoupload view view-first" id="user_photoupload">
					    
					     <span> 
					     <img src="${pageContext.request.contextPath}<%="/previewprofileOther.html?fileName="+profilePicture+"&docCategory=Profile_Photo&year="+session.getAttribute("created_year")+"&indId="+user0+""%>" /></img>
					     </span>						   
						     
						    <div id="profileHover" class="mask">                       
	                               	                                	                                
	                               <form name="editbasicdetail" id="editbasicdetail"  method="POST" enctype="multipart/form-data">
	                                                                        
		                               <p style="float:left"><!-- --></p>
		                               <div id="profilePhotoDiv" style="float: left;margin-left: -5px;margin-top: -69px;width: 100px;display:none;"><a  class="info" style="height: 16px;margin-left: 0;margin-right: -7px;margin-top: 11px;width: 72px;" onclick="uploadbasicdetail1()">Save</a></div>		                               
		                               <p style="float:left;margin-top:5px;margin-left:8px;"><a href="#" class="info" onclick="openOffersDialog();profilePicHistory();">History</a>
		                               </p>
		                               
	                              </form>    
	                                                        														
						     </div>
						     
						 </div>
						   
						 <div class="user_photo">
						 <p><strong><c:out value="${regname.firstname}"></c:out></strong> <br>
						 <span>
						 <c:if test="${!empty basicdetails.present_address}">
						 <c:set var="presentplace"  value="${basicdetails.present_address}"/>
									<%
										String myVariable = (String) pageContext
														.getAttribute("presentplace");
												String[] ary = myVariable.split("##");
												String str = ary[4];
												str = str.substring(0, str.length());
									%>
						 <%=str%>
						 </c:if>
						 </span>			 
						 </p>
						 </div>
				</c:if>	
					
					<div id="overlay" ></div>
					
					<div id="boxpopup" class="box">
						
						<a onclick="closeOffersDialog('boxpopup');" class="boxclose"></a>
						 <div class="popheaderstyle"><h1 align="left">Previous Profile Pictures</h1></div>
						<div id="content">
						    
						</div>
					</div>
						                
                <div class="user_display">
                <div id="closepic" style="display: block;">
                
                
                <c:if test="${empty profilePicInfo.profile_picture_name}">
                                
                <div class="user_photoupload view view-first" id="user_photoupload">
	 
				<span id="userphoto"><img src="resources/kyc_images/Koala.jpg" width="196" height="222" /></span>
					 
			   </div> 
						   
			 <div class="user_photo">
					<p><strong><c:out value="${regHisname.first_name}"></c:out></strong> <br>
					<span>
					<c:if test="${!empty basicdetails.present_address}">
						
						<c:set var="presentplace"  value="${basicdetails.present_address}"/>
										<%
											String str = "";
													String myVariable = (String) pageContext
															.getAttribute("presentplace");
													String[] ary = myVariable.split("##");
													str = ary[4];
													str = str.substring(0, str.length());
										%>
							<%=str%>
							
					</c:if>
					</span>
					</p>
			  </div>
                              	
		   </c:if>		          	 					
	      </div>
	      </div>		
	      
	         		     	         		      	         		                  								 				 																			
							<div class="myaccounts">
							<h2>
							<span>
							<img src="resources/kyc_images/contact_info.png" width="25" height="26" />
							Contact Info</span>
							</h2>							
							<div class="account_titel"><img src="resources/images/info_mainid_icons.png" /><span>E-mail address</span></div>
							<ul>
							<li>
							<table>
							<tbody>
							<tr>
							<td>							
							<span id="emailid">
							<%if(regVisibility.charAt(0)=='1') {%>
							
							<c:out value="${regHisname.email_id}"></c:out>
							
							<%}else { %>
							
									<%if(profileAccess.contains("REGISTRATION-Email Id")) {%>
									
									    <c:out value="${regHisname.email_id}"></c:out>
									
									<%}else{ %>
									
									    <c:out value="Restricted"></c:out>
									
									<%} %>
							<%} %>
							</span>							
							</td>
							</tr>
							</tbody>
							</table>
							</li>
							</ul>
							<div class="mobileaccount_titel"><img src="resources/images/info_mobile_icons.png" /><span>Mobile Number</span></div>
							<ul>
							<li>
							<table>
							<tbody>
							<tr>
							<td><span id="mobileno">
							<%if(regVisibility.charAt(1)=='1') {%>
							<c:out value="${regHisname.mobile_no}"></c:out>
							<%}else { %>
							
									<%if(profileAccess.contains("REGISTRATION-Mobile No")) {%>
									
									    <c:out value="${regHisname.mobile_no}"></c:out>
									
									<%}else{ %>
									
									    <c:out value="Restricted"></c:out>
									
									<%} %>

							<%} %>
							
							</span></td>
							</tr>
							</tbody>
							</table>
							</li>
							</ul>
							
							<div class="present_addressaccount_titel"><img src="resources/images/info_presentaddress_icons.png" /><span>Present Address</span></div>
							<ul>
							<li>
							<div id="presentaddressget" >
								<span id="PresentAddID" style="color:#2d2d2d;">
								<c:if test="${!empty basicdetails.present_address}">
								
								<c:set var="present_address"  value="${basicdetails.present_address}" ></c:set>
								
								<%
																	String present_address = (String) pageContext
																				.getAttribute("present_address");
																		String pre_addressAry[] = present_address.split("##");
																%>
								<%if(basicVisibility.charAt(8)=='1') {%>
											
								           <table class="textwidth">
											<tr><td><%=pre_addressAry[0]%>,<%=pre_addressAry[3]%>,<%=pre_addressAry[2]%>,
											         <%=pre_addressAry[1]%>,<%=pre_addressAry[1]%>,<%=pre_addressAry[7]%>,
											         <%=pre_addressAry[4]%>,<%=pre_addressAry[5]%>,<%=pre_addressAry[6]%>,
											         <%=pre_addressAry[8]%></td></tr>
											</table>
								
								<%}else{ %>
								
								     <%if(profileAccess.contains("BASIC-Present Address")) {%>
									
									    	<table class="textwidth">
											<tr><td><%=pre_addressAry[0]%>,<%=pre_addressAry[3]%>,<%=pre_addressAry[2]%>,
											         <%=pre_addressAry[1]%>,<%=pre_addressAry[1]%>,<%=pre_addressAry[7]%>,
											         <%=pre_addressAry[4]%>,<%=pre_addressAry[5]%>,<%=pre_addressAry[6]%>,
											         <%=pre_addressAry[8]%></td></tr>
											</table>
									
									<%}else{ %>
									
									      Restricted as per the visibility !
									
									<%} %>
							      
								<%} %>
								
								</c:if>
								
								<c:if test="${empty basicdetails.present_address}">																
										Not Filled Yet !
								</c:if>
								
								</span>														
							</div>
							</li>
							</ul>
							</div>
							
							<c:if test="${!empty basicdetails.languages}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/languages_known_icon.png" width="25" height="26" />Languages Known</span>
							</h2>
							<ul>
							<li>
							<div id="languageDiv">
								           
								           <%if(basicVisibility.charAt(6)=='1') {%>			
								                 <c:out value="${basicdetails.languages}"></c:out>					                 								                 					           
								           <%}else{ %> 
								                 <c:out value="Language is Restricted"></c:out>			
								           <%}%>
								           
								</div>	
							</li>
							</ul>
							</div>
							</c:if>
							
							<c:if test="${empty basicdetails.languages}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/languages_known_icon.png" width="25" height="26" />Languages Known</span>
							</h2>
							<ul>
							<li>
							<div id="languageDiv">Not Filled Yet !</div>
							</li>
							</ul>
							</div>
							</c:if>
							
							
							<c:if test="${empty socialaccvalue}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/social_account_icon.png" width="25" height="26" />Social Account </span>
							</h2>
							<ul>
							<li>
							<img src="resources/kyc_images/face_book_icon.png" width="17" height="17" /><div id="websitelinkname">Not Available</div>
							</li>
							<li>
							<img src="resources/kyc_images/twitter_icon.png" width="17" height="17" /><div id="twitterlinkname">Not Available</div>
							</li>
							<li>
							<img src="resources/kyc_images/linkedin_icon.png" width="17" height="17" /><div id="linkedlnlinkname">Not Available</div>
							</li>							
							</ul>
							</div>
							</c:if>	
							
							<c:if test="${!empty socialaccvalue}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/social_account_icon.png" width="25" height="26" />Social Account </span>
							</h2>
							<ul>
							<li>
							<img src="resources/kyc_images/face_book_icon.png" width="17" height="17" /><div id="websitelinkname">
							<%if(socialVisibility.charAt(0)=='1') {%>
							
									<c:if test="${!empty socialaccvalue.socialwebsitelink}">
												<a href="${socialaccvalue.socialwebsitelink}" target="_blank">
													<c:out value="facebook connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.socialwebsitelink}">									
										<c:out value="facebook not connected"></c:out>						
									</c:if>	
									
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							<li>
							<img src="resources/kyc_images/twitter_icon.png" width="17" height="17" /><div id="twitterlinkname">
							<%if(socialVisibility.charAt(1)=='1') {%>
									
									<c:if test="${!empty socialaccvalue.sociallinkedlnlink}">
												<a href="${socialaccvalue.sociallinkedlnlink}" target="_blank">
													<c:out value="twitter connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.sociallinkedlnlink}">									
										<c:out value="twitter not connected"></c:out>						
									</c:if>	
								
								
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							<li>
							<img src="resources/kyc_images/linkedin_icon.png" width="17" height="17" /><div id="linkedlnlinkname">
							<%if(socialVisibility.charAt(2)=='1') {%>
							
							        <c:if test="${!empty socialaccvalue.socialtwitterlink}">
												<a href="${socialaccvalue.socialtwitterlink}" target="_blank">
													<c:out value="linkedin connected"></c:out>
									 			</a>
									</c:if>	
									
									<c:if test="${empty socialaccvalue.socialtwitterlink}">									
										<c:out value="linkedin not connected"></c:out>						
									</c:if>	
																		
							<%} else { %>
							<c:out value="Restricted"></c:out>
							<%} %>
							</div>
							</li>
							
							</ul>
							</div>
							</c:if>												
												                
					</div>
					
					 </div>
					
					<div class="rightside">
					
					 <input type="hidden" id="redirectUrl" value="<%=(String)session.getAttribute("reloadUrl")%>" />
	
	                  <input type="hidden" id="indId" value="<%=session.getAttribute("tempIdasdgthfmUti")%>" />
	                 
					
					<!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>
                                  <div id="ProfileHeadingDiv" class="utiProfileHeadingDiv" style="display:block;">
									
										<marquee behavior="scroll" scrollamount="2" direction="left">
											   <h1 class="rightsideheader">
											       
											        <%=utiAdvertise%>
											        
											   </h1>
									     </marquee>
							        </div>	
							       
							        		
					   <!-- Advertisment Div for Entire Page  -->
					   
					   
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
							       <b></b>
								   <span><c:out value="${regname.firstname}'s Employment Documents"></c:out> <div style="display: initial;" id="HitorydateId"></div></span>
								   <c:set var="member_date"  value="${regname.cr_date}"/>
		                              <%
		                              	Date member_date = (Date) pageContext.getAttribute("member_date");
		                              	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		                              	String reportDate = df.format(member_date);
		                              	String dateAry1[] = reportDate.split(" ");
		                              	String dateAry2[] = dateAry1[0].split("-");
		                              	int month = Integer.parseInt(dateAry2[1]);
		                              	String year = dateAry2[0];
		                              %>							
								    <p>Member since <%=months[month - 1] + " " + year%></p>
							</h2>
						 </div>
						 
					
<div class="module-body">
	
	<!-- side tab button div -->
					
							<div class="sidetabdiv">
							<ul>
							
							    <li class="tab-defaultbtn">
								     <input class="tab-defaultinput" id="ProfileActive" type="button" value="Profile" onclick="showProfileAgain()"/>							
								</li>
								
							<%if(otherAccess.contains("KYC:")){ %>
							
								<li class="tab-defaultbtn">
								<input class="tab-defaultinput" id="kycActive" type="button" value="KYC" onclick="showKycDocuments()"/>							
								</li>
								
						    <%} %>
						    
						    
						    <%if(otherAccess.contains("ACADEMIC:")){ %>
						    
								<li class="tab-defaultbtn">
								<input class="tab-defaultinput" id="academicActive" type="button" value="Academic" onclick="showAcademicDocuments()">							
								</li>
								
							<%} %>
							
							<%if(otherAccess.contains("EMPLOYEMENT:")){ %>
							
								<li class="tab-defaultbtn">
								<input class="tab-defaultinputActive" id="empActive" type="button" value="Employment" onclick="showEmpDocuments()">							
								</li>
								
							<%}else if(otherAccess.contains("Employment:")) {%>
							
								<li class="tab-defaultbtn">
								<input class="tab-defaultinputActive" id="empActive" type="button" value="Employment" onclick="showEmpDocuments()">							
								</li>
								
							<%} %>
							
							<%if(otherAccess.contains("FINANCIAL:")){ %>
							
								<li class="tab-defaultbtn">
								<input class="tab-defaultinput" id="financialActive" type="button" value="Financial" onclick="showFinancialDocuments()">							
								</li>
								
							<%}else if(otherAccess.contains("Financial:")) {%>	
													
								<li class="tab-defaultbtn">
									<input class="tab-defaultinput" id="financialActive" type="button" value="Financial" onclick="showFinancialDocuments()">							
								</li>
								
							<%} %>
							
							</ul>
							</div>		
							
							
	<!-- side tab button div END-->			
	       <div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
	
	
						<!-- Start of Required Division for viewUser1 not go beyond  -->	
					    
					  
					    <div id="center-body-div">
	  						
								<div class="centerbodydiv">
										  
										  
									<%String zipStatus = (String)session.getAttribute("allDocumentNames");  %>
										 
										<%if(zipStatus.contains("_")){ %> 
										  	  										  
												  <div class="showcodepage">
												        <ul>
												            <li>
												                <table>
												                <tr>
												                <td>
													            <img src="resources/kyc_images/kyczipicon.jpg" height="50px" width="50px" /></td>
													            
													            
														        <td>
														        <form  id="downloadAllFormId" action="downlaodalldocumentsinzipForUtilizer.html" method="post">
														    
														     
															        <input type="hidden" value="<%=(String)session.getAttribute("allDocumentNames")%>" name="exttemp" />
															        
															        <input type="hidden" value="<%=session.getAttribute("tempIdasdgthfmUti")%>" name="extp" /> 
															        
														            <input type="hidden" value="EmploymentDocuments" name="codeName" />
														      
														           
														            <input type="submit" value="Click here to download latest Employment documents in zip." /></form>
														            </td>
														            </tr>
														            </table>
														        
														        
													        </li>
													    </ul>
													</div>
													
										  <%}else{ %>
										  
										         <div class="makeinHide">
										         
													  <div class="showcodepage">
													        <ul>
													            <li>
													                <table>
														                <tr>
															                <td>
																            <img src="resources/kyc_images/kyczipicon.jpg" height="50px" width="50px" /></td>
																            													            
																	            <td>
																	        
																	               <input type="submit" style="cursor: not-allowed;" value="Click here to download all latest Academic documents in zip." />
																	          
																	            </td>
																	            
																          </tr>
															       </table>
															        
															        
														        </li>
														    </ul>
														</div>
										  
										         </div>
										  <%} %>
										  
										   
										  		  	    
										  			
										  			   
										  			<% 
										  			
										  			   String documentNamesWithDetails = (String)request.getAttribute("employeeData");								                       
								                       String empDetailsArray[] = documentNamesWithDetails.split("ENDSEP");
								
													   String pannelStatus = (String)request.getAttribute("getEmployeeStatus");
													   String pannelStatusArray[] = pannelStatus.split("/");
													   
													   String extraAddedDocuments = (String)request.getAttribute("extraAddedDocs");
													   String extraAddedDocumentsArray[] = extraAddedDocuments.split(",");
													
													   String extraDocumentCheck = (String)session.getAttribute("extraDocumentCheck");
								                     %>   
										  			   
										  			 <!-- Repete the block -->
										  		
										  		
										  <%if(!pannelStatus.equals("Not There")){%>
										  
										      <div  id="panel-1">
										  
										  			<%for(int i=0;i<pannelStatusArray.length;i++){ %>   
										  			
										  			
										  			   <h3 align="left" onclick="visibleAccordian('Accordian')"><b><%=pannelStatusArray[i]%></b></h3>  
									        
											  			   <div height="50px" id="Accordian" style="display:none;">
										            
										                          <div id="kycdocumentpage" style="margin-left: 0px ! important;" class="kycdocumentpage">  	<!-- start of kycdocumentpage -->														
													                        
													                         <%	
													                             String eachComponent = "";
															  			         for(int a=0;a<empDetailsArray.length;a++)
															  			         {
															  			        	
															  			        	 if(!empDetailsArray[a].equals("No Details")) 
															  			        	  {	 		
															  			        		
																	  			        	 if(empDetailsArray[a].startsWith(pannelStatusArray[i]))
																	  			        	 {
																	  			        		 eachComponent = empDetailsArray[a];														  			        		
																	  			        	
																		  			             String detailWithName[] = eachComponent.split("EMPSEP");															  			          
																		  			             String docs_dataArray[] = detailWithName[0].split("&&");
																		  			     
															  			          %>
													                        
													                       
													                           <div style="margin-top:15px;margin-left:0px ! important;">
																           	      <button class="backbuttonindex_histy" id="viewDocument" onclick="HistoryDocUti('<%=pannelStatusArray[i]%>')">History</button>   
																           	    </div>	 
																		                
																		                  <div class="kycdocumentdetails-ext">      <!-- Division for Left side Hierarchy other component with data is in left side -->
																		                         
																		                         <table>
																			                       
																				                         <tr style="margin-top:15px;">
																				                              <td style="width:105px;"><b>Job Designation</b></td>
																				                              <td style="width:10px;">:</td>
																				                              <td style="width:auto;"><%=docs_dataArray[1]%></td>			                              
																				                         </tr>
																				                         
																				                         <tr style="margin-top:15px;">
																				                              <td style="width:auto;"><b>Job Summary</b></td>
																				                              <td style="width:10px;">:</td>
																				                              <td style="width:auto;"><%=docs_dataArray[2]%></td>			                              
																				                         </tr>
																				                         
																				                         <tr style="margin-top:15px;">																				                         
																				                              <td style="width:auto;"><b>Duration</b></td>
																				                              <td style="width:10px;">:</td>
																				                              <td style="width:auto;"><%=docs_dataArray[3]%> &nbsp; to &nbsp; <%=docs_dataArray[4]%></td>			                              																				                        
																				                         </tr>
																			                     
																			                     </table>
																			                       					                       					                          
																			              </div>      <!-- End of Division for Left side Hierarchy other component with data is in left side -->
																			
																			
																		                     <div class="academicdocumenttitle">
																		                     
																								<h1>
																									<span></span>
																								 <%=pannelStatusArray[i]%> documents </h1>
																								</div>    	
																			    
																						  <div class="academicdetailsimg">     <!-- Division for right side Hierarchy file component with data  -->
																							             
																					              <ul>
																					               
																					               <%if(!detailWithName[1].equals("No Doc")) {
																					               
																							            	    String docNameArray[] = detailWithName[1].split(",");							            	    
																							            	    for(int dn=0;dn<docNameArray.length;dn++)
																							            	    {
																							                    %>
																							                    
																									                  <%if(!docNameArray[dn].contains("PaySlip")) {%>
																									                    	
																									                    		
																									                    		<%if(extraDocumentCheck.equals("No")) {%>	
																									                    				
																									                    				 <li class="panviewinner_acd">
																									                    				 																                  
																													                    <%if(docNameArray[dn].contains("pdf")) {%>
																													                    
																													                           <b><%=docNameArray[dn].split("_")[0]%></b> 
																													                           
																													                  		   <img src="resources/images/PNA_PDF.png"   width="150" height="100" /></img>
																																												              
																									                                           <a href="downloadDocfromUtilizer.html?fileName=<%=docNameArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
																															                    
																									                                    <%} else {%>
																									                                         	
																									                                         	<%if(docNameArray[dn].equals("No Document")) {%>
																									                                         	
																												                                         	<%if(dn==0){ %>
																																	                                         	
																												                                         	     <b>Offer Letter</b> 
																												                                         	
																												                                         	<%}else if(dn==1){ %>
																												                                         	
																												                                         		<b>Hike Letter</b> 
																												                                         	
																												                                         	<%}else if(dn==2){ %>
																												                                         	
																												                                         		<b>Experience Letter</b> 
																												                                         	
																												                                         	<%}else if(dn==3){ %>
																												                                         	
																												                                         		<b>Reliving Letter</b> 
																												                                         	
																												                                         	<%} else{%>
																												                                         	
																												                                         	    <b>Form-16</b> 
																												                                         	   
																												                                         	<%} %>
																																	                                         	 
																												                                         	
																																						    <img src="resources/images/noDocumentFound.png"   width="150" height="100" /></img>
																																			                
																												                                  <%}else{%>     
																												                                  
																												                                           <b><%=docNameArray[dn].split("_")[0]%></b> 
																												                                         	
																																						    <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+docNameArray[dn]+"&docCategory=Employment_DOC&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="150" height="100" /></img>
																																			                                                         
																											                                                <a onclick="viewKycDocumnt('<%=docNameArray[dn]%>')"><button class="mydocumentviewpan"></button></a>
																												                                            <a  href="downloadDocfromUtilizer.html?fileName=<%=docNameArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
																												                                  
																												                                  <%} %>     
																									                                    <%}%> 
																									                                    
																									                                    </li>  
																					                                             
																					                                             <%}else{ %>
																					                                              
																							                                                    <%if(extraAddedDocuments.contains(docNameArray[dn])) {%>
																							                                              
																							                                              
																							                                                           <li class="panviewinner_acd">
																							                                               
																									                                                   <%if(docNameArray[dn].contains("pdf")) {%>
																																	                    
																																	                           <b><%=docNameArray[dn].split("_")[0]%></b> 
																																	                           
																																	                  		   <img src="resources/images/PNA_PDF.png"   width="150" height="100" /></img>
																																																              
																													                                           <a href="downloadDocfromUtilizer.html?fileName=<%=docNameArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
																																			                    
																													                                    <%} else {%>
																													                                         	
																													                                         	      <%if(docNameArray[dn].equals("No Document")) {%>
																									                                         	
																																	                                         	<%if(dn==0){ %>
																																	                                         	
																																	                                         	     <b>Offer Letter</b> 
																																	                                         	
																																	                                         	<%}else if(dn==1){ %>
																																	                                         	
																																	                                         		<b>Hike Letter</b> 
																																	                                         	
																																	                                         	<%}else if(dn==2){ %>
																																	                                         	
																																	                                         		<b>Experience Letter</b> 
																																	                                         	
																																	                                         	<%}else if(dn==3){ %>
																																	                                         	
																																	                                         		<b>Reliving Letter</b> 
																																	                                         	
																																	                                         	<%} else{%>
																																	                                         	
																																	                                         	    <b>Form-16</b> 
																																	                                         	   
																																	                                         	<%} %>
																																	                                         	
																																	                                         	
																																											    <img src="resources/images/noDocumentFound.png"   width="150" height="100" /></img>
																																								                
																																	                                  <%}else{%>     
																																	                                  
																																	                                           <b><%=docNameArray[dn].split("_")[0]%></b> 
																																	                                         	
																																											    <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+docNameArray[dn]+"&docCategory=Employment_DOC&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="150" height="100" /></img>
																																								                                                         
																																                                                <a onclick="viewKycDocumnt('<%=docNameArray[dn]%>')"><button class="mydocumentviewpan"></button></a>
																																	                                            <a  href="downloadDocfromUtilizer.html?fileName=<%=docNameArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
																																	                                  
																																	                                  <%} %>     
																													                                    <%}%>   
																											                                           </li>
																											                                    <%}%>
																					                                             <%} %>                            
																									                      
																									                      <%} %>
																									                   
																									              <%} %>  
																									              
																							                    
																						                <%} else {%> 
																						                
																						                          <h1> Employment Documents are not uploaded yet !</h1>
																						                <%} %>    
																					              </ul>
																							   
																							</div>                               <!-- Division for right side Hierarchy file component with data -->
																				           	   
																				           	   
																				           	   
																				           	   <div class="academicdocumenttitle">
																		                     
																								<h1>
																									<span></span>
																								 <%=pannelStatusArray[i]%> PaySlips </h1>
																								</div>   
																				           	   
																				           	    <div class="academicdetailsimg">     <!-- Division for right side Hierarchy file component with data  -->
																							             
																					              <ul>
																					               
																					               <%if(!detailWithName[1].equals("No Doc")) {
																					               
																							            	    String docNameArray[] = detailWithName[1].split(",");	
																							            	    
																							            	    String monthNameArray[] = {"January", "February", "March", "April", "May",
															    	    				                				"June", "July", "August", "September", "October",
															    	    				                				"November", "December"};
																				        	 
																									        	String SalarySlipNames = "";    
																									        	String monthyearName = "";    
																									        	
																							            	    for(int dn=0;dn<docNameArray.length;dn++)
																							            	    {
																							            	    	if(docNameArray[dn].contains("PaySlip")) {
																							            	    		
																							            	    		SalarySlipNames += docNameArray[dn] + ",";
																							            	    		
																							            	    	}
																							            	    }
																							            	    
																							            	   
																							            	    
																							            	    if(SalarySlipNames.length()>0)
																									        	 {
																									        		 SalarySlipNames = SalarySlipNames.substring(0, SalarySlipNames.length()-1);
																									        		 
																									        		 String slipNamesArray[] = SalarySlipNames.split(",");
																									        	     for(int sn=0;sn<slipNamesArray.length;sn++)
																									        	     {
																									        	    	 int j = slipNamesArray[sn].split("_")[0].lastIndexOf(" ");
																									        	    	 String tempValue = slipNamesArray[sn].split("_")[0].substring(j+1);
																									        	    	 
																									        	    	 monthyearName += tempValue + ",";
																									        	     }
																									        	 }
																									        	 
																									        	 if(monthyearName.length()>0)
																									        	 {
																									        		 monthyearName = monthyearName.substring(0, monthyearName.length()-1);
																									        	 }
																									        	 
																									        	 
																									        	 //  Sorted logics
																									        	 
																									        	 String sortedMonthYearName = "";
																									        	 
																									        	 if(SalarySlipNames.length()>0)
																										    	     {
																										    	    	
																										    	    	 String monthYearArray[] = monthyearName.split(","); 
																										    	    	 
																										    	    	 for(int mn=0;mn<monthYearArray.length;mn++)    // outer monthYear array
																										    	    	 {
																										    	    		 String monthValue = monthYearArray[mn].split("-")[0];
																										    	    		 int outerindex = 0;
																							    	    					 int innerindex = 0;
																										    	    				 for(int imn=0;imn<monthYearArray.length;imn++)    // inner monthYear array
																													    	    	 {
																										    	    					 String imonthValue = monthYearArray[imn].split("-")[0];
																											    	    				 for(int imName=0;imName<monthNameArray.length;imName++)
																													    	    		 {
																											    	    					 if(monthValue.equals(monthNameArray[imName]))
																											    	    					 {
																											    	    						 outerindex = imName;
																											    	    					 }
																											    	    					 if(imonthValue.equals(monthNameArray[imName]))
																											    	    					 {
																											    	    						 innerindex = imName;
																											    	    					 }
																													    	    		 }
																											    	    				 if(outerindex>innerindex)
																											    	    				 {
																											    	    					 String temp = monthYearArray[mn];
																											    	    					 monthYearArray[mn] = monthYearArray[imn];
																												    	    				 monthYearArray[imn] = temp;
																											    	    				 }
																													    	    	 }
																										    	    																														    	    		 
																										    	    	 }
																										    	    	 
																										    	    	 
																										    	    	 for(int k=0;k<monthYearArray.length;k++)
																										    	    	 {																															    	    		 
																										    	    		 int yearValue = Integer.parseInt(monthYearArray[k].split("-")[1]);
																										    	    		 for(int l=0;l<monthYearArray.length;l++)
																										    	    		 {
																										    	    			 int tempyearValue = Integer.parseInt(monthYearArray[l].split("-")[1]);
																										    	    			 if(yearValue>tempyearValue)
																										    	    			 {
																										    	    				 String temp = monthYearArray[k];
																										    	    				 monthYearArray[k] = monthYearArray[l];
																										    	    				 monthYearArray[l] = temp;
																										    	    			 }
																										    	    		 }																																    	    		 
																										    	    	 }
																										    	    	
																										    	    	 for(int my=0;my<monthYearArray.length;my++)
																										    	    	 {
																										    	    		 sortedMonthYearName += monthYearArray[my] + ",";																														    	    		 
																										    	    	 }
																										    	    	 
																										    	    	 if(sortedMonthYearName.length()>0)
																										    	    	 {
																										    	    		 sortedMonthYearName = sortedMonthYearName.substring(0, sortedMonthYearName.length()-1);
																										    	    	 }
																										    	    	 
																										    	     }
																										     
																									        	    
																									        		 String monthName[] = sortedMonthYearName.split(","); 
																											    	 String salSlipArray1[] = SalarySlipNames.split(","); 
																												     String sortedSlipName = "";
																												     for(int sort=0;sort<monthName.length;sort++)
																									    	    	 {
																												    	 for(int sorti=0;sorti<salSlipArray1.length;sorti++)
																										    	    	 {
																												    		 if(salSlipArray1[sorti].contains(monthName[sort]))
																												    		 {
																												    			 sortedSlipName += salSlipArray1[sorti] + ",";	
																												    		 }																										    	    		 
																										    	    	 }																												    	    		 
																									    	    	 }
																												     
																												     if(sortedSlipName.length()>0)
																												     {
																												    	 sortedSlipName =sortedSlipName.substring(0, sortedSlipName.length()-1);
																												     }
																												     
																												     String salSlipArray[] = sortedSlipName.split(","); 
																												     
																												     
																									        	 
																									        	 // End of sorted logics
																									        	 
																									        	 
																							            	    
																							            	    for(int dn=0;dn<salSlipArray.length;dn++)
																							            	    {
																							                    %>
																							                    
																									                  
																									               <%if(salSlipArray[dn].contains("PaySlip")) {%>     	
																									                    		
																									                    		<%if(extraDocumentCheck.equals("No")) {%>	
																									                    				
																									                    				 <li class="panviewinner_acd">
																									                    				 																                  
																													                    <%if(salSlipArray[dn].contains("pdf")) {%>
																													                    
																													                           <b><%=salSlipArray[dn].split("_")[0]%></b> 
																													                           
																													                  		   <img src="resources/images/PNA_PDF.png"   width="150" height="100" /></img>
																																												              
																									                                           <a href="downloadDocfromUtilizer.html?fileName=<%=salSlipArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
																															                    
																									                                    <%} else {%>
																									                                         	
																									                                         	
																									                                         	<b><%=salSlipArray[dn].split("_")[0]%></b> 
																									                                         	
																																			    <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+salSlipArray[dn]+"&docCategory=Employment_DOC&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="150" height="100" /></img>
																																                                                         
																								                                                <a onclick="viewKycDocumnt('<%=salSlipArray[dn]%>')"><button class="mydocumentviewpan"></button></a>
																									                                            <a  href="downloadDocfromUtilizer.html?fileName=<%=salSlipArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
																									                                            
																									                                    <%}%> 
																									                                    
																									                                    </li>  
																					                                             
																					                                             <%}else{ %>
																					                                              
																							                                                    <%if(extraAddedDocuments.contains(salSlipArray[dn])) {%>
																							                                              
																							                                              
																							                                                           <li class="panviewinner_acd">
																							                                               
																									                                                   <%if(salSlipArray[dn].contains("pdf")) {%>
																																	                    
																																	                           <b><%=salSlipArray[dn].split("_")[0]%></b> 
																																	                           
																																	                  		   <img src="resources/images/PNA_PDF.png"   width="150" height="100" /></img>
																																																              
																													                                           <a href="downloadDocfromUtilizer.html?fileName=<%=salSlipArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
																																			                    
																													                                    <%} else {%>
																													                                         	
																													                                         	
																													                                         	<b><%=salSlipArray[dn].split("_")[0]%></b> 
																													                                         	
																																							    <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+salSlipArray[dn]+"&docCategory=Employment_DOC&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="150" height="100" /></img>
																																				                                                         
																												                                                <a onclick="viewKycDocumnt('<%=salSlipArray[dn]%>')"><button class="mydocumentviewpan"></button></a>
																													                                            <a  href="downloadDocfromUtilizer.html?fileName=<%=salSlipArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
																													                                            
																													                                    <%}%>   
																											                                           </li>
																											                                    <%}%>
																					                                             <%} %>                            
																									                             
																									                             
																									                             <%} %>
																									              <%} %>  
																									              
																							                    
																						                <%} else {%> 
																						                
																						                          <h1> Salary slips are not uploaded yet !</h1>
																						                <%} %>    
																					              </ul>
																							   
																							</div> 
																							
																				                 <%} %>
																				            <%} %>
																				            
																				       <%} %>    		
																				         
																				         
																				         
																				              <%if(!documentNamesWithDetails.contains(pannelStatusArray[i])){ %>
															  			                   
																	  			                     <div class="alert-box notice" style="width:250px;">
																										 
																											Information are not updated yet.
																													 
																									 </div> 
																							 
															  			                      <%} %>
															  			                      
															  			                        	    				           	    						           	           	      				                          
																	           </div>      <!-- End of kycdocumentpage -->		
																	                	
											  			         
											  			    </div>     
											  			    
											  			<%} %>      
											  			
											  	
										  		</div><!-- End of panel-1 -->
										
									  	<%}else { %> 
									  	
									  	      <div style="margin-left: 276px;margin-top: 50px;"> <h1>As of now ${regname.firstname} didn't added any employment details.</h1> </div>
									  	
									  	<%} %>  
											  	 		
										  </div> 
										     
					     </div>
					        
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
	
						
			</div>
		</div>	
	</div>	<!-- End of -->	


        			 <!--end of right div  -->
	</div>

</div>
					
		<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="utiContactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>



<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">

 								<div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
									
                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->
	
	
	
  <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	         
  	<!-- kycDoc Pop up Div -->
	
	<div id="kycDocPopUplight">
		
		 <div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		         
		       	
			<div id="kycDocPopUpDisplay" style=" border: 2px solid #03b2ee;
			    border-radius: 5px;
			    color: #555;
			    margin: 0 auto;
			    padding:0;
			    text-align: left;
			    width: auto;"> 
					              
					       
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF kycDoc Pop up Div -->	                           
                             
                             
                             
                            
                             
<!-- History POPUP -->
	
	<div id="kycHistoryLight" class="kycHistoryLightcss" style="display:none;">
			
			<div class="colsebutton" onclick="fadeOutHistoryDiv()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		 
		    <div id="kycHistoryDisplay" class="kycHistoryDisplaycss"> 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="kycHistoryFade" class="kycHistoryFadecss" onclick="fadeOutHistoryDiv()"></div> 	
	
<!-- EOF  History POPUP -->
	
	
	
	
	
	
	
</body>
</html>
     	