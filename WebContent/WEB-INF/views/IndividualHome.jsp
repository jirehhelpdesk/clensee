 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>	
<title>My Profile</title>

<%
   //Required file Config for entire Controller 

	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 String kycMainDirectory = fileResource.getString("fileDirectory");		
	 File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller 	
%>


    <!-- <link rel="stylesheet" type="text/css" href="resources/datePicker/employeeDatePicker/dhtmlxcalendar.css"/>

	<script src="resources/datePicker/employeeDatePicker/dhtmlxcalendar.js"></script>
	
	<style>
		#calendar,
		#calendar2,
		#calendar3 {
			border: 1px solid #909090;
			font-family: Tahoma;
			font-size: 12px;
		}
	</style>
	<script>
		var myCalendar;
		function doOnLoad() {
			myCalendar = new dhtmlXCalendarObject(["calendar","calendar2"]);
		}
	</script> -->
	
	

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/jscripts/jquery.com.jquery-1.10.2.js"></script>
<script src="resources/jscripts/jquery.com.jqueryUi.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />



<link rel="stylesheet" type="text/css" href="resources/datePicker/datePicker/jquery.datetimepicker.css"/>


<style>


#waitfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 5px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

</style>


<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
    
}
#aboutUsfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#aboutUslight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    border:2px solid #00b5f6;
    display: none;
    height: auto;
    left: 42%;
    margin-left: -378px;
    margin-top: -220px;   
    position: fixed;
    top: 50%;
    width: 1000px;
    z-index: 1002;

}
#profilePhotoPopfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#profilePhotoPoplight{
    border: 2px solid #05b7f5;
    /* background: none repeat scroll 0 0 #fff; */
    background: none repeat scroll 0 0 #333;
    border-radius: 10px;
    display: none;
    height: 550px;
    left: 40%;
    margin-left: -132px;
    margin-top: -220px;
    position: fixed;
    top: 40%;
    width: 550px;
    z-index: 1002;

}

</style>

<script>

function profilePhotoPopfadeout()
{
	document.getElementById('profilePhotoPoplight').style.display='none';
	document.getElementById('profilePhotoPopfade').style.display='none';
}

function aboutUsfadeout()
{
	document.getElementById('aboutUslight').style.display='none';
	document.getElementById('aboutUsfade').style.display='none';
}

function aboutUsSeeMore()
{
	var aboutUs = document.getElementById("myArea1").innerHTML;
	$("#aboutUsContent").html(aboutUs);
	document.getElementById('aboutUslight').style.display='block';	
	document.getElementById('aboutUsfade').style.display='block';
}

function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function fadeoutReport()
{
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
}

function fadeoutReportPopup(action)
{
	if(action=='success')
		{
			document.getElementById('sociallight').style.display='none';
			document.getElementById('socialfade').style.display='none';
			$("#socialdisplaySharedDocDiv").html("");
			window.location = "profiletohome.html";
		}
	else
		{
			document.getElementById('sociallight').style.display='none';
			document.getElementById('socialfade').style.display='none';
			$("#socialdisplaySharedDocDiv").html("");
		}
	
}

function getkycAboutUs()
{
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	   
	    });
	
}

function getTermsConditions()
{
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';

	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
					$.ajax({  
					     type : "Post",   
					     url : "getKYCTermConditions.html", 	     
					     success : function(response) 
					     {  
					    	 document.getElementById('waitlight').style.display='none';
					         document.getElementById('waitfade').style.display='none';

					    	 document.getElementById('kycdivlight').style.display='block';
					    	 document.getElementById('kycdivfade').style.display='block';
					    	 $("#kycdivdisplaySharedDocDiv").html(response);
					    	    	    														
					     },  
					    
					    });
				}
			else
				{
				    document.getElementById('waitlight').style.display='none';
			        document.getElementById('waitfade').style.display='none';

				       $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
}

function getPolicy()
{
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
				   
						$.ajax({
							type : "post",
							url : "checksessionforIndividual.html",
							cache : false,
							success : function(response) {
								
								if(response=="Exist")
									{
									    document.getElementById('waitlight').style.display='block';
								        document.getElementById('waitfade').style.display='block';

										$.ajax({  
										     type : "Post",   
										     url : "getKYCPrivatePolicy.html", 	     
										     success : function(response) 
										     {  
										    	 document.getElementById('waitlight').style.display='none';
											     document.getElementById('waitfade').style.display='none';

										    	 
										    	 document.getElementById('kycdivlight').style.display='block';
										    	 document.getElementById('kycdivfade').style.display='block';
										    	 $("#kycdivdisplaySharedDocDiv").html(response);
										    	    	    														
										     },  
										    
										    });
									}
								else
									{
									   $("#sessionlight").show();
								           $("#sessionfade").show();
									}
							}
						});
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
	
	
}

</script>



<script>

function showSocialHint()
{
	$("#sociallight").show();
}

function hideSocialHint()
{
	$("#sociallight").hide();
}

</script>





<script>

$(document).ready(function(){

if(!Modernizr.input.placeholder){

	$('[placeholder]').focus(function() {
	  var input = $(this);
	  if (input.val() == input.attr('placeholder')) {
		input.val('');
		input.removeClass('placeholder');
	  }
	}).blur(function() {
	  var input = $(this);
	  if (input.val() == '' || input.val() == input.attr('placeholder')) {
		input.addClass('placeholder');
		input.val(input.attr('placeholder'));
	  }
	}).blur();
	
	$('[placeholder]').parents('form').submit(function() {
	  $(this).find('[placeholder]').each(function() {
		var input = $(this);
		if (input.val() == input.attr('placeholder')) {
		  input.val('');
		}
	  })
	});
}

});

</script>



<script type="text/javascript">

function validateRegDetailsEach()
{
	var name = /^[a-zA-Z ]+$/;	
	var num = /^[0-9]{10}$/;	
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	var first = document.getElementById("reg1").value;

	var middle = document.getElementById("reg0").value;
	
	var last = document.getElementById("reg3").value;
	
	var email = document.getElementById("reg4").value;

	var phno = document.getElementById("reg5").value;

	if(first=="")
	{ 
		$("#reg1Div").hide();
		document.getElementById("reg1").style = "width: 102px;float: left;margin-right: 6px;";
	}
	if(first!="")
		{
		  if(!first.match(name))
			  {			    
			     $("#reg1Div").show();  
			     $("#reg1Div").html("Only Alphabets are allowed for First Name!");
			     document.getElementById("reg1").style ="width: 102px;float: left;margin-right: 6px;border:1px solid red";
			     return false;
			  }
		  else
			  {
			     $("#reg1Div").hide();		
			     document.getElementById("reg1").style ="width: 102px;float: left;margin-right: 6px;";
			  }
		}
	
	if(middle=="")
		{
		$("#reg0Div").hide();
		document.getElementById("reg0").style = "width: 113px;float: left;margin-right: 6px;";
		}
	if(middle!="")
	{
	  if(!middle.match(name))
		  {	
		    $("#reg0Div").show();
		    $("#reg0Div").html("Only Alphabets are allowed for middle name!");
		    document.getElementById("reg0").style = "width: 113px;float: left;margin-right: 6px;border:1px solid red";
		    return false;
		  }
	  else
		  {
		    $("#reg0Div").hide();
		    document.getElementById("reg0").style = "width: 113px;float: left;margin-right: 6px;";
		  }
	}
	
	if(last=="")
		{
		$("#reg3Div").hide();
		document.getElementById("reg3").style = "width: 113px;float: left;margin-right: 6px;";
		
		}
	
	if(last!="")
	{
	  if(!last.match(name))
		  {	
		    $("#reg3Div").show();
		    $("#reg3Div").html("Only Alphabets are allowed for Last Name");
		    document.getElementById("reg3").style = "width: 113px;float: left;margin-right: 6px;border:1px solid red";
		    return false;
		  }
	  else
		  {
		    $("#reg3Div").hide();
		    document.getElementById("reg3").style = "width: 113px;float: left;margin-right: 6px;";
		  }
	}
	if(email!="")
	{
	  if(!email.match(emailId))
		  {	
		    $("#reg4Div").show();
		    $("#reg4Div").html("Enter a valid Email Id");
		    document.getElementById("reg4").style = "border:1px solid red;clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
		    return false;
		  }
	  else
		  {
		    $("#reg4Div").hide();
		    document.getElementById("reg4").style = "clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
		  }
	}
	if(email=="")
		{
		$("#reg4Div").hide();
		document.getElementById("reg4").style = "clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
		}
	
	if(phno!="")
	{
	  if(!phno.match(num))
		  {	
		    $("#reg5Div").show();
		    $("#reg5Div").html("Enter a valid Phone Number");
		    document.getElementById("reg5").style = "border:1px solid red;clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
		    return false;
		  }
	  else
		  {
		    $("#reg5Div").hide();
		    document.getElementById("reg5").style = "clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
		  }
	}
	
	if(phno=="")
		{
		 $("#reg5Div").hide();
		 document.getElementById("reg5").style = "clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
		}
}
	
function genderValue()
{
	
	var male = document.getElementById("sex_m");
	var female = document.getElementById("sex_f");
	
	if(male.checked)
	{
	   document.getElementById("editgenderId").value="MALE";	
	}
	if(female.checked)
	{
	   document.getElementById("editgenderId").value="FEMALE";	
	}
}
	
	
function saveRegDetails()
{			
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
						var value=document.getElementById("editreg").value;
						
						if(value=="Edit")
						{
						
							document.getElementById("editreg").value="Save";
									
							var gender = document.getElementById("genderId").value;		
							
							if(gender=="MALE")
								{
									$("#genderValueli").hide();
									$("#malelibutton").hide();
								}
							if(gender=="FEMALE")
								{
									$("#genderValueli").hide();
									$("#femalelibutton").hide();
								}
							
							$("#hiddenmalelibutton").hide();
							$("#hiddenfemalelibutton").hide();
							
						   $("#genderli").show();
						   $("#genderlibutton").show();
							
							x=document.getElementById("reg1")
						    x.disabled = !x.disabled;
							document.getElementById("reg1").style.backgroundColor = "#fff";
							
						    x=document.getElementById("reg0")
						    x.disabled = !x.disabled;
						    document.getElementById("reg0").style.backgroundColor = "#fff";
						    
						    x=document.getElementById("reg3")
						    x.disabled = !x.disabled;
						    document.getElementById("reg3").style.backgroundColor = "#fff";
						    
						    x=document.getElementById("reg4")
						    x.disabled = !x.disabled;
						    document.getElementById("reg4").style.backgroundColor = "#fff";
						    
						    x=document.getElementById("reg5")
						    x.disabled = !x.disabled;
						    document.getElementById("reg5").style.backgroundColor = "#fff";
						    
						    x=document.getElementById("sex_m")
						    x.disabled = !x.disabled;
							x=document.getElementById("sex_f")
						    x.disabled = !x.disabled;
						    
						}
						
						if(value=="Save")
						 {													
							
							var flag='true';  
							var name = /^[a-zA-Z ]+$/;	
							//var name = /^[a-zA-Z]+$/;	
							var num = /^[0-9]{10}$/;	
							var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
							
							var first = document.getElementById("reg1").value;
		
							var middle = document.getElementById("reg0").value;
							
							var last = document.getElementById("reg3").value;
							
							var email = document.getElementById("reg4").value;
		
							var phno = document.getElementById("reg5").value;
							
							
							
							
							if(email!="")
							{
							  if(!email.match(emailId))
								  {	
								    flag='false';
								    $("#reg4Div").show();
								    $("#reg4Div").html("Please Enter a valid Email Id !");
								    document.getElementById("reg4").style = "border:1px solid red;clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
								    return false;
								  }
							  else
								  {
								    $("#reg4Div").hide();
								  }
							}
							
							if(first!="")
								{
								  if(!first.match(name))
									  {		
									     flag='false';
									     $("#reg1Div").show();  
									     $("#reg1Div").html("Only Alphabets are allowed for First Name!");
									     document.getElementById("reg1").style = "width:102px;float: left;margin-right: 6px;border:1px solid red";
									     return false;
									  }
								  else
									  {
									     $("#reg1Div").hide();			     
									  }
								}
							
							if(middle!="")
							{
							  if(!middle.match(name))
								  {	
								    flag='false';
								    $("#reg0Div").show();
								    $("#reg0Div").html("Only Alphabets are allowed for middle name!");
								    document.getElementById("reg0").style = "width:113px;float: left;margin-right: 6px;border:1px solid red";
								    return false;
								  }
							  else
								  {
								    $("#reg0Div").hide();
								  }
							}
							
							
							if(last!="")
							{
							  if(!last.match(name))
								  {	
								    flag='false';
								    $("#reg3Div").show();
								    $("#reg3Div").html("Only Alphabets are allowed for Last Name");
								    document.getElementById("reg3").style = "width:113px;float: left;margin-right: 6px;border:1px solid red";
								    return false;
								  }
							  else
								  {
								    $("#reg3Div").hide();
								  }
							}
							
							
							if(phno!="")
							{
							  if(!phno.match(num))
								  {	
								  	flag='false';
								    $("#reg5Div").show();
								    $("#reg5Div").html("Please Enter a valid Mobile number !");
								    document.getElementById("reg5").style = "border:1px solid red;clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
								    return false;
								  }
							  else
								  {
								    $("#reg5Div").hide();
								  }
							}
							
							
							if(first=="" || last=="" || email=="" || phno=="")
							{
							      alert("Please fill in all the details !")
							      flag='false';
							      return false;
							}
							
						if(flag=='true')
							{
							   
							$.ajax({  
							     type : "Post",   
							     url : "getvalidateRegistrationEmailId.html", 
							     data: "emailId="+email,
							     success : function(response) 
							     {  
							    	 if(response!="Exist")
							    		 {
							    		 
										    		 $.ajax({  
											    	     type : "Post",   
											    	     url : "getvalidateRegistrationMobileNo.html", 	
											    	     data : "mobileNo="+phno,
											    	     success : function(response) 
											    	     {  
											    	    		
											    	    	 if(response!="Exist")
												    		 {
											    	    		 $.ajax({  
												    			     type : "Post",   
												    			     url : "editregdetail.html", 
												    	      	     data :$('#editregistration').serialize()+"&gender="+document.getElementById("editgenderId").value,    	     	     
												    			     success : function(response) 
												    			     {  
												    			    	 
												    			    	 $("#registration").fadeIn(500); 
												    			         $("#registration").fadeOut(5000);
												    			         
												    			    	 $('#mobileno').html($("#reg5").val());
												    	     	    	 $('#emailid').html($("#reg4").val());
												    	     	    	  
												    	     	    	 $("#nameStrngId").html(first);       // Change Below the name mentioned in Profile Photo
												    	     	    	 
												    	     	    	 $("#loginNameId").html(first);          // Change the Logout Button Name 
												    	     	    	 
												    	     	    	document.getElementById("editreg").value="Edit";
												    	     	    	 
												    	     	    	
												    	     	    	x=document.getElementById("reg1")
												    	     		    x.disabled = !x.disabled;
												    	     	    	document.getElementById("reg1").style.backgroundColor = "#efefef";
												    	     	    	
												    	     		    x=document.getElementById("reg0")
												    	     		    x.disabled = !x.disabled;
												    	     		   document.getElementById("reg0").style.backgroundColor = "#efefef";
												    	     		   
												    	     		    x=document.getElementById("reg3")
												    	     		    x.disabled = !x.disabled;
												    	     		   document.getElementById("reg3").style.backgroundColor = "#efefef";
												    	     		   
												    	     		    x=document.getElementById("reg4")
												    	     		    x.disabled = !x.disabled;
												    	     		   document.getElementById("reg4").style.backgroundColor = "#efefef";
												    	     		   
												    	     		    x=document.getElementById("reg5")
												    	     		    x.disabled = !x.disabled;
												    	     		    document.getElementById("reg5").style.backgroundColor = "#efefef";
												    	     		    
												    	     		    x=document.getElementById("sex_m")
												    	    		    x.disabled = !x.disabled;							    	     		    
												    	     		    x=document.getElementById("sex_f")
												    	   		        x.disabled = !x.disabled;
												    	     		        		    
												    	     		   								    	     		    
												    	     		   var editedgender = document.getElementById("editgenderId").value;		
												    	     			
												    	     			if(editedgender=="MALE")
												    	     				{
												    	     				    document.getElementById("genderId").value ="MALE";
												    	     					$("#genderValueli").show();
												    	     					$("#hiddenmalelibutton").show();
												    	     				}
												    	     			if(editedgender=="FEMALE")
												    	     				{
												    	     				    document.getElementById("genderId").value ="FEMALE";
												    	     					$("#genderValueli").show();
												    	     					$("#hiddenfemalelibutton").show();
												    	     				}
												    	     		     	 	
												    	     		   $("#genderli").hide();
												    	     		   $("#genderlibutton").hide();
												    	     																	
												    			     },  
												    			        
												    			    });   // End of save reg details
												    	    	 
												    		 }
											    	    	 else
											    	    		 {
											    	    		    $("#reg5Div").show();
											    				    $("#reg5Div").html("Given Mobile no is already exist with some other Individual !");
											    				    document.getElementById("reg5").style = "border:1px solid red;clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
											    				    return false;
											    	    		 }
											    	    	 
											    	     },  
										    	        		    	   
										    	    });	// end of mobileno validate
							    		 }
							    	 else
							    		 {
										    $("#reg4Div").show();
										    $("#reg4Div").html("Given Email Id is already exist with some other Individual !");
										    document.getElementById("reg4").style = "border:1px solid red;clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 375px;";
										    return false;
							    		 }
							    	 
							      },  
		
							    });   // end of emailId validate
																					  
							} // End of Flag true condition
						}
				}
			else
				{
				      $("#sessionlight").show();
			          $("#sessionfade").show();
				}
		}
	});

}

</script>

<script type="text/javascript">
function validateFamilyEach()
{
	
	var name = /^[a-zA-Z ]+$/;
	var poi= /^[a-zA-Z0-9]*$/;
	var regexHoro  =  /^[a-zA-Z0-9,]*$/; 
	//var alfaspe = /^[ A-Za-z_@.,/#&]+$/;
	
	var fname = document.getElementById("fname").value;
	var fpoi = document.getElementById("poi").value;
	var mname = document.getElementById("mname").value;
	var mpoi = document.getElementById("mpoi").value;
	var bname = document.getElementById("bname").value;
	var bpoi = document.getElementById("bpoi").value;
	
	var sname= document.getElementById("sname").value;
	var spoi = document.getElementById("spoi").value;
	var horo = document.getElementById("hinfo").value;
	var food = document.getElementById("foodpreferred").value;
	
	if(fname!="")
	{
		if(!fname.match(name))  
        {  	
			$("#fnameDiv").show();
	        $("#fnameDiv").html("Enter alphabet for Father Name!");
	        document.getElementById("fname").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
		{
			$("#fnameDiv").hide();
			document.getElementById("fname").style = "width:160px;float: left;margin-right: 6px;";		
		}
	}
	
	if(fname=="")
	{
		$("#fnameDiv").hide();
		document.getElementById("fname").style = "width:160px;float: left;margin-right: 6px;";	
	}
		
	if(fpoi!="")
	{
		if(!fpoi.match(poi))  
        {  	
			$("#poiDiv").show();
	        $("#poiDiv").html("Enter alphanumeric for Father POI!");
	        document.getElementById("poi").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
		{
			$("#poiDiv").hide();
			document.getElementById("poi").style="width:160px;float: left;margin-right: 6px;";
		}
	}
	
	if(fpoi=="")
	{
		$("#poiDiv").hide();
		document.getElementById("poi").style="width:160px;float: left;margin-right: 6px;";
	}
	
	if(mname!="")
	{
		if(!mname.match(name))  
        {  	
			$("#mnameDiv").show();
	        $("#mnameDiv").html("Enter alphabet for Mother Name");
	        document.getElementById("mname").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
			{
			$("#mnameDiv").hide();
			document.getElementById("mname").style = "width:160px;float: left;margin-right: 6px;";
			}
	}
	
	if(mname=="")
	{
		$("#mnameDiv").hide();
		document.getElementById("mname").style = "width:160px;float: left;margin-right: 6px;";		
	}
	
	
	if(mpoi!="")
	{
		if(!mpoi.match(poi))  
        {  	
			$("#mpoiDiv").show();
	        $("#mpoiDiv").html("Enter alphanumeric for Mother POI");
	        document.getElementById("mpoi").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
		{
			$("#mpoiDiv").hide();
			document.getElementById("mpoi").style="width:160px;float: left;margin-right: 6px;";
		}
	}
	
	if(mpoi=="")
	{
		$("#mpoiDiv").hide();
		document.getElementById("mpoi").style="width:160px;float: left;margin-right: 6px;";		
	}
	
	
	if(bname!="")
	{
		if(!bname.match(name))  
        {  	
			$("#bnameDiv").show();
	        $("#bnameDiv").html("Enter alphanumeric for Sibling Name");
	        document.getElementById("bname").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
		{
			$("#bnameDiv").hide();
			document.getElementById("bname").style="width:160px;float: left;margin-right: 6px;";
		}
	}
	
	if(bname=="")
	{
		$("#bnameDiv").hide();
		document.getElementById("bname").style="width:160px;float: left;margin-right: 6px;";
	}
	
	if(bpoi!="")
	{
		if(!bpoi.match(poi))  
        {  	
			$("#mpoiDiv").show();
	        $("#mpoiDiv").html("Enter alphanumeric for Sibling POI");
	        document.getElementById("bpoi").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
		{
			$("#mpoiDiv").hide();
			document.getElementById("bpoi").style = "width:160px;float: left;margin-right: 6px;";
		}
	}
	if(bpoi=="")
	{
		$("#bpoiDiv").hide();
		document.getElementById("bpoi").style = "width:160px;float: left;margin-right: 6px;";
	}

	if(sname!="")
	{
		if(!sname.match(name))  
        {  	
			$("#snameDiv").show();
	        $("#snameDiv").html("Enter alphabet for Spouse Name");
	        document.getElementById("sname").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
		{
			$("#snameDiv").hide();
			document.getElementById("sname").style = "width:160px;float: left;margin-right: 6px;";
		}
	}
	
	if(sname=="")
	{
		$("#snameDiv").hide();
		document.getElementById("sname").style = "width:160px;float: left;margin-right: 6px;";
	}
	
	
	if(spoi!="")
	{
		if(!spoi.match(poi))  
        {  	
			$("#spoiDiv").show();
	        $("#spoiDiv").html("Enter alphanumeric for Spouse POI");	
	        document.getElementById("spoi").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
		{
			$("#spoiDiv").hide();
			document.getElementById("spoi").style = "width:160px;float: left;margin-right: 6px;";
		}
	}
	
	if(spoi=="")
	{
		$("#spoiDiv").hide();
		document.getElementById("spoi").style = "width:160px;float: left;margin-right: 6px;";
	}
	
	if(horo!="")
	{
		if(!horo.match(regexHoro))  
        {  	
			$("#hinfoDiv").show();
	        $("#hinfoDiv").html("Enter alphabet for Horoscope!");		
	        document.getElementById("hinfo").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
		{
			$("#hinfoDiv").hide();
			document.getElementById("hinfo").style = "width:160px;float: left;margin-right: 6px;";
		}
	}
	
	if(horo == "")
	{
		$("#hinfoDiv").hide();
		document.getElementById("hinfo").style = "width:160px;float: left;margin-right: 6px;";
	}
	
	/* if(food!="")
	{
		if(!food.match(alfaspe))  
        {  
			$("#foodpreferredDiv").show();
	        $("#foodpreferredDiv").html("Enter alfabet with ',' seperator");		 
	        document.getElementById("foodpreferred").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
        } 
		else
			{
			$("#foodpreferredDiv").hide();
			document.getElementById("foodpreferred").style = "width:160px;float: left;margin-right: 6px;";
			}
	} */
	
	if(food=="")
	{
		$("#foodpreferredDiv").hide();
		document.getElementById("foodpreferred").style = "width:160px;float: left;margin-right: 6px;";
	}
	
	}


</script>


<script type="text/javascript">
function validateSocialEach1()
{
	//var url = /(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    //var url = "";
    var face = document.getElementById("websitelink").value;
    var twitter = document.getElementById("twitterlink").value;
    var link = document.getElementById("linkedlnlink").value;
        
    if(face.indexOf(",")>-1)
	{
	     $("#websitelinkDiv").show();
	     $("#websitelinkDiv").html("A url shouldn't contain ',' in the link !");
	     return false;
	}
    if(face.indexOf(",")<-1)
	{
	     $("#websitelinkDiv").hide();	    
	     
	}

    
	if(twitter.indexOf(",")>-1)
	{
	     $("#websitelinkDiv").show();
	     $("#websitelinkDiv").html("A url shouldn't contain ',' in the link !");
	     return false;
	}
	if(twitter.indexOf(",")<-1)
	{
	     $("#websitelinkDiv").hide();
	}
	
	
	if(link.indexOf(",")>-1)
	{
	     $("#websitelinkDiv").show();
	     $("#websitelinkDiv").html("A url shouldn't contain ',' in the link !");
	     return false;
	}
	if(link.indexOf(",")<-1)
	{
	     $("#websitelinkDiv").hide();
	}

}

function saveSocialDetail()
{
	var value=document.getElementById("editsocial").value;
	if(value=="Edit")
		{
			document.getElementById("editsocial").value="Save";
			
			x=document.getElementById("websitelink")
		    x.disabled = !x.disabled;
			document.getElementById("websitelink").style.backgroundColor = "#fff";
			
		    x=document.getElementById("twitterlink")
		    x.disabled = !x.disabled;
		    document.getElementById("twitterlink").style.backgroundColor = "#fff";
		    
		    x=document.getElementById("linkedlnlink")
		    x.disabled = !x.disabled;
		    document.getElementById("linkedlnlink").style.backgroundColor = "#fff";
		}
	
	if(value=="Save")
	{
		var flag = 'true';
		
	    //var url = /(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	    
	    var face = document.getElementById("websitelink").value;
	    var twitter = document.getElementById("twitterlink").value;
	    var link = document.getElementById("linkedlnlink").value;
	    
	    if(face=="" & twitter=="" & link=="")
    	{
    	   flag='false';
    	   alert("Please enter some details in the corresponding fields !");
    	   return false;
    	}
	   
    	
	    if(flag=='true')
	    	{
	    		 
	    	 $.ajax({  
			     type : "Post",   
			     url : "editsocialdetail.html", 
			     data :$('#editsocialdetail').serialize(),	     	     	     
			     success : function(response) 
			     {  
			    	 
			    	 $("#SocialSuccess").fadeIn(500); 
			    	 $("#SocialSuccess").fadeOut(5000);
			    	 			    	
			    	 document.getElementById("editsocial").value="Edit";
			    	 
			    	 if($("#websitelink").val()!="")
			    		 {
			    		     $('#websitelinkname').html("facebook connected");
			    		 }
			    	 if($("#websitelink").val()=="")
			    		 {
			    		     $('#websitelinkname').html("facebook not connected");
			    		 }
			    	 
			    	 if($("#linkedlnlink").val()!="")
			    		 {
			    		     $('#twitterlinkname').html("twitter connected");
			    		 }
			    	 if($("#linkedlnlink").val()=="")
			    		 {
			    		     $('#twitterlinkname').html("twitter not connected");
			    		 }
	      	    	 if($("#twitterlink").val()!="")
	      	    		 {
	      	    		     $('#linkedlnlinkname').html("linkedin connected"); 
	      	    		 }
	      	    	  if($("#twitterlink").val()=="")
	      	    		  {
	      	    		      $('#linkedlnlinkname').html("linkedin not connected"); 
	      	    		  }
	      	    				    		      	    		      	    	
			 		x=document.getElementById("websitelink")
			 	    x.disabled = !x.disabled;
			 		document.getElementById("websitelink").style.backgroundColor = "#efefef";
			 		
			 	    x=document.getElementById("twitterlink")
			 	    x.disabled = !x.disabled;
			 	   document.getElementById("twitterlink").style.backgroundColor = "#efefef";
			 	   
			 	    x=document.getElementById("linkedlnlink")
			 	    x.disabled = !x.disabled;
			 	   document.getElementById("linkedlnlink").style.backgroundColor = "#efefef";
			 	   
			 	   document.getElementById("socialHistorybuttonId").style = "width:100px;display:block;";
			 	   
			     },  
			     
			     error : function(e) 
				    {  
				       alert('Error: ' + e);
				    }			     
			    });  
	    	}	    
	  }
}

</script>


<script>
$(function() {
$("#b8").datepicker();
});
</script>


<script>

function ReviewDefects1(length)
{
	document.getElementById('savepresentaddress').value = document.getElementById('door').value+"##"+

	document.getElementById('landmark').value+"##"+document.getElementById('zone').value+"##"+
    
	document.getElementById('mandal').value+"##"+document.getElementById('city').value+"##"+
    
	document.getElementById('district').value+"##"+document.getElementById('state').value+"##"+
    
    document.getElementById('pincode').value+"##"+
    
    document.getElementById('country').value;
	
	//alert("document.getElementById('savepresentaddress').value"+document.getElementById('presentaddress').value);
		
	}


function ReviewDefectsper(length)
{
	document.getElementById('permanentaddress').value = document.getElementById('perdoor').value + "##" +

	document.getElementById('perlandmark').value+"##"+document.getElementById('perzone').value+"##"+
    
	document.getElementById('permandal').value+"##"+document.getElementById('percity').value+"##"+
    
	document.getElementById('perdistrict').value+"##"+document.getElementById('perstate').value+"##"+
    
    document.getElementById('perpincode').value+"##"+
    
    document.getElementById('percountry').value;
	
	
	
	}




	
function showHideDiv1(id){
	

	myTextBox1 = document.getElementById("b6");
	value=myTextBox1.value;
	
	  var obj = document.getElementById(id);
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	    } else if(obj.style.display=="block"){
	      obj.style.display='none';
	    }
	}


function validateBasicEach()
{
	
	var pinnum = /^[0-9]{6}$/; 
	var alfabet  = /^[a-zA-Z]+$/;
	var num = /^[0-9]+$/;
	var alfaspe = /^[ A-Za-z_@.,/#&]+$/;
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;	
	//var alphanumeric = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i;	
	
	var alphanumeric = /^[a-zA-Z0-9]*$/;
	
	var alphanumStreet = /^[a-zA-Z0-9]+$/;
	
	var pincode =  document.getElementById("pincode").value;	
	var perpincode =  document.getElementById("perpincode").value;
	var mari_status = document.getElementById("b2").value;
	var country = document.getElementById("country").value;		
	var percountry = document.getElementById("percountry").value;
	var nation = document.getElementById("b3").value;
	var state = document.getElementById("state").value;
	var perstate = document.getElementById("perstate").value;
	var teloff = document.getElementById("b4").value;
	var district = document.getElementById("district").value;
	var perdistrict = document.getElementById("perdistrict").value;
	var telers = document.getElementById("b5").value;
	var mandal = document.getElementById("mandal").value;
	var permandal = document.getElementById("permandal").value;
	var lang = document.getElementById("b6").value;
	var city = document.getElementById("city").value;
	var percity = document.getElementById("percity").value;
	var email = document.getElementById("street").value;
	var door = document.getElementById("door").value;
	var perdoor = document.getElementById("perdoor").value;
	
	
	if(percity!="")
	{
		if(!percity.match(alfaspe))  
        {  	
			$("#percityDiv").show();
	        $("#percityDiv").html("Enter alphabet for City Name!");	
	        document.getElementById("percity").style = "width:120px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
			{
		    $("#percityDiv").hide();
		    document.getElementById("percity").style = "width:120px;float: left;margin-right: 6px;";		    
			}
	}
	if(percity=="")
		{
		$("#percityDiv").hide();
	    document.getElementById("percity").style = "width:120px;float: left;margin-right: 6px;";
		}
	
	if(city!="")
	{
		if(!city.match(alfaspe))  
        {  
			$("#cityDiv").show();
	        $("#cityDiv").html("Enter alphabet for City Name");	
	        document.getElementById("city").style = "width:120px;float: left;margin-right: 6px;border:1px solid red;";
	        return false;
        } 
		else
			{
			$("#cityDiv").hide();
			document.getElementById("city").style = "width:120px;float: left;margin-right: 6px;";
			}
	}
		
	if(city=="")
		{
		$("#cityDiv").hide();
		document.getElementById("city").style = "width:120px;float: left;margin-right: 6px;";
		}
	
	
	if(email!="")
	{	
		
		if(!email.match(emailId))  
            {  
			   $("#streetDiv").show();
		       $("#streetDiv").html("Enter a proper EmailId");
		       document.getElementById("street").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
		       return false;
            }  
		else
			{
			   $("#streetDiv").hide();
			   document.getElementById("street").style = "width:160px;float: left;margin-right: 6px;";
			}
	}
	
	if(email=="")
		{
     	$("#streetDiv").hide();
		   document.getElementById("street").style = "width:160px;float: left;margin-right: 6px;";
		}
	
	if(lang!="")
	{
		if(!lang.match(alfaspe))  
            {  
			 $("#b6Div").show();
		        $("#b6Div").html("Type a proper value");	
		        document.getElementById("b6").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            }  
		else
			{
		    $("#b6Div").hide();
		    document.getElementById("b6").style="width:160px;float: left;margin-right: 6px;";
			}
	} 
	if(lang=="")
		{
		$("#b6Div").hide();
	    document.getElementById("b6").style="width:160px;float: left;margin-right: 6px;";
		}

	
	if(permandal=="")
	{
		$("#permandalDiv").hide();
        document.getElementById("permandal").style="width:120px;float: left;margin-right: 6px;";
	}

	
	if(mandal=="")
		{
		    $("#mandalDiv").hide();
	        document.getElementById("mandal").style="width:120px;float: left;margin-right: 6px;";
		}
	
	
	if(pincode!="")
		{
		if(!pincode.match(num))  
            { 	
			$("#pincodeDiv").show();
		    $("#pincodeDiv").html("Enter 6 digit numeric Pincode");		
		    document.getElementById("pincode").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
		    return false;
            }   
		else
			{
			$("#pincodeDiv").hide();
			document.getElementById("pincode").style="width:120px;float: left;margin-right: 6px;";
			}
		}
	if(pincode=="")
		{
		$("#pincodeDiv").hide();
		document.getElementById("pincode").style="width:120px;float: left;margin-right: 6px;";
		}
	
	if(perpincode!="")
	{
		if(!perpincode.match(num))  
            { 
			    $("#perpincodeDiv").show();
		        $("#perpincodeDiv").html("Enter 6 digit numeric Pincode");		   
		        document.getElementById("perpincode").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            }
		else
			{
		    $("#perpincodeDiv").hide();
		    document.getElementById("perpincode").style="width:120px;float: left;margin-right: 6px;";
			}
	}
	if(perpincode=="")
		{
		$("#perpincodeDiv").hide();
	    document.getElementById("perpincode").style="width:120px;float: left;margin-right: 6px;";
		}
	
	if(mari_status!="")
	{
		if(!mari_status.match(alfaspe))  
            { 
			$("#b2Div").show();
		       $("#b2Div").html("Enter alphabet for Marital Status");		
		       document.getElementById("b2").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
		       return false;
            }   
		else
			{
			   $("#b2Div").hide();
			   document.getElementById("b2").style="width:160px;float: left;margin-right: 6px;";
			}
	}
	if(mari_status=="")
		{
		   $("#b2Div").hide();
		   document.getElementById("b2").style="width:160px;float: left;margin-right: 6px;";
		}
	if(country!="")
	{
		if(!country.match(alfaspe))  
            { 		
			    $("#countryDiv").show();
		        $("#countryDiv").html("Enter alphabet for Country Name");	
		        document.getElementById("country").style="width:120px;float: left;margin-right: 6px;border:1px solid red;"; 
		        return false;
             }  
		else
			{
			$("#countryDiv").hide();
			document.getElementById("country").style="width:120px;float: left;margin-right: 6px;";
			}
	}
	if(country=="")
		{
		$("#countryDiv").hide();
		document.getElementById("country").style="width:120px;float: left;margin-right: 6px;";
		}
	
	if(percountry!="")
	{
		if(!percountry.match(alfaspe))  
            { 
			    $("#percountryDiv").show();
		        $("#percountryDiv").html("Enter alphabet for Country Name");	
		        document.getElementById("percountry").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            }
		else
			{
		    $("#percountryDiv").hide();
		    document.getElementById("percountry").style="width:120px;float: left;margin-right: 6px;"; 
			}
	}
	
	if(percountry=="")
	{
		$("#percountryDiv").hide();
	    document.getElementById("percountry").style="width:120px;float: left;margin-right: 6px;"; 
	}
	
	if(nation!="")
	{
		if(!nation.match(alfabet))  
            { 
				$("#b3Div").show();
		        $("#b3Div").html("Enter alphabet for Nationality");		
		        document.getElementById("b3").style="width:160px;float: left;margin-right: 6px;border:1px solid red;"; 
		        return false;
            }
		else
			{
		    $("#b3Div").hide();
		    document.getElementById("b3").style="width:160px;float: left;margin-right: 6px;"; 
			}
	}
	if(nation=="")
		{
		$("#b3Div").hide();
	    document.getElementById("b3").style="width:160px;float: left;margin-right: 6px;"; 
		}
	
	if(state!="")
	{
		if(!state.match(alfaspe))  
            { 
				$("#stateDiv").show();
		        $("#stateDiv").html("Enter alphabet for State Name");	
		        document.getElementById("state").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            }
		else
			{
		    $("#stateDiv").hide();
		    document.getElementById("state").style="width:120px;float: left;margin-right: 6px;";
			}
	}
	if(state=="")
		{
		$("#stateDiv").hide();
		document.getElementById("state").style="width:120px;float: left;margin-right: 6px;";
		}
	
	if(perstate!="")
	{
		if(!perstate.match(alfaspe))  
            { 
				$("#perstateDiv").show();
		        $("#perstateDiv").html("Enter alphabet for for State Name");		   
		        document.getElementById("perstate").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            }
		else
			{
		        $("#perstateDiv").hide();
		        document.getElementById("perstate").style="width:120px;float: left;margin-right: 6px;";
			}
	}
	if(perstate=="")
		{
		    $("#perstateDiv").hide();
	        document.getElementById("perstate").style="width:120px;float: left;margin-right: 6px;";
		}
	if(teloff!="")
	{
		if(!teloff.match(num))  
            { 
				$("#b4Div").show();
		        $("#b4Div").html("Enter Numeric Value for Telephone no.");		
		        document.getElementById("b4").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            } 
		else
			{
		    $("#b4Div").hide();
		    document.getElementById("b4").style="width:160px;float: left;margin-right: 6px;";
			}
	}
	if(teloff=="")
		{
		$("#b4Div").hide();
	    document.getElementById("b4").style="width:160px;float: left;margin-right: 6px;";
		}
	
	if(district!="")
	{
		if(!district.match(alfaspe))  
            { 		
				$("#districtDiv").show();
		        $("#districtDiv").html("Enter alphabet for District Name");		
		        document.getElementById("district").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            } 
		else
			{
			 $("#districtDiv").hide();
			 document.getElementById("district").style="width:120px;float: left;margin-right: 6px;";
			}
	}
	if(district=="")
		{
		$("#districtDiv").hide();
		 document.getElementById("district").style="width:120px;float: left;margin-right: 6px;";
		}
	
	if(perdistrict!="")
	{
		if(!perdistrict.match(alfaspe))  
            { 
			    $("#perdistrictDiv").show();
		        $("#perdistrictDiv").html("Enter alphabet for District Name");
		        document.getElementById("perdistrict").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            } 
		else
			{
		    $("#perdistrictDiv").hide();
		    document.getElementById("perdistrict").style="width:120px;float: left;margin-right: 6px;";
			}
	}
	if(perdistrict=="")
		{
		$("#perdistrictDiv").hide();
	    document.getElementById("perdistrict").style="width:120px;float: left;margin-right: 6px;";
		}
	if(telers!="")
	{
		if(!telers.match(num))  
            { 
				$("#b5Div").show();
		        $("#b5Div").html("Enter numeric for Telephone no.");
		        document.getElementById("b5").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
            } 
		else
			{
		    $("#b5Div").hide();
		    document.getElementById("b5").style="width:160px;float: left;margin-right: 6px;";
			}
	}
	if(telers=="")
		{
		$("#b5Div").hide();
	    document.getElementById("b5").style="width:160px;float: left;margin-right: 6px;";
		}
	}




function  validatesavebasic(which)

{
	
	if ( which.value == "Edit" )
        
	{
		which.value = "Save";
	
	x=document.getElementById("checkStatus")
	x.disabled = !x.disabled;
	
    x=document.getElementById("b2")
    x.disabled = !x.disabled; 
   
    x=document.getElementById("b4")
    x.disabled = !x.disabled;
    
    x=document.getElementById("b5")
    x.disabled = !x.disabled;
    
    x=document.getElementById("b6")
    x.disabled = !x.disabled;
    
    x=document.getElementById("b7")
    x.disabled = !x.disabled;
    
    x=document.getElementById("calendar")
    x.disabled = !x.disabled;
    
    x=document.getElementById("street")
    x.disabled = !x.disabled;
         
    x=document.getElementById("pincode")
    x.disabled = !x.disabled;
   
    x=document.getElementById("state")
    x.disabled = !x.disabled;
    
    x=document.getElementById("mandal")
    x.disabled = !x.disabled;
    
   /*  x=document.getElementById("country")
    x.disabled = !x.disabled; */
    x=document.getElementById("district")
    x.disabled = !x.disabled;
    
    x=document.getElementById("city")
    x.disabled = !x.disabled;
    
    x=document.getElementById("zone")
    x.disabled = !x.disabled;
   
    x=document.getElementById("door")
    x.disabled = !x.disabled;
    
    x=document.getElementById("landmark")
    x.disabled = !x.disabled;
    
    x=document.getElementById("perpincode")
    x.disabled = !x.disabled;
    
    x=document.getElementById("perstate")
    x.disabled = !x.disabled;
    
    x=document.getElementById("permandal")
    x.disabled = !x.disabled;
    
    x=document.getElementById("percountry")
    x.disabled = !x.disabled; 
    
    x=document.getElementById("perdistrict")
    x.disabled = !x.disabled;
    
    x=document.getElementById("percity")
    x.disabled = !x.disabled;
    
    x=document.getElementById("perzone")
    x.disabled = !x.disabled;
    
    x=document.getElementById("perdoor")
    x.disabled = !x.disabled;
    
    x=document.getElementById("perlandmark")
    x.disabled = !x.disabled;
   
	}	
	else
		if ( which.value == "Save" )
        {       	      	        	
			var flag='true';
			  			
			var pinnum = /^[0-9]{6}$/; 
			var alfabet  = /^[a-zA-Z ]+$/;
			var num = /^[0-9]+$/;
			var alfaspe = /^[ A-Za-z_@.,/#&]+$/;
			var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
			
			var alphanumeric = /((^[0-9]+[a-z]+)|(^[a-z]+[0-9]+))+[0-9a-z]+$/i;
				
			var pincode =  document.getElementById("pincode").value;	
			var perpincode =  document.getElementById("perpincode").value;
			var mari_status = document.getElementById("b2").value;
			var country = document.getElementById("country").value;		
			var percountry = document.getElementById("percountry").value;
			var nation = document.getElementById("b3").value;
			var state = document.getElementById("state").value;
			var perstate = document.getElementById("perstate").value;
			var teloff = document.getElementById("b4").value;
			var district = document.getElementById("district").value;
			var perdistrict = document.getElementById("perdistrict").value;
			var telers = document.getElementById("b5").value;
			var mandal = document.getElementById("mandal").value;
			var permandal = document.getElementById("permandal").value;
			var lang = document.getElementById("b6").value;
			var city = document.getElementById("city").value;
			var percity = document.getElementById("percity").value;
			var email = document.getElementById("street").value;
			var door = document.getElementById("door").value;
			var perdoor = document.getElementById("perdoor").value;
					
			if(percity!="")
			{
				if(!percity.match(alfaspe))  
		        {  	
					flag = 'false';
					$("#percityDiv").show();
			        $("#percityDiv").html("Enter alphabet for City Name!");	
			        document.getElementById("percity").style = "width:120px;float: left;margin-right: 6px;border:1px solid red;";
			        return false;
		        } 
				else
					{
				    $("#percityDiv").hide();
				    document.getElementById("percity").style = "width:120px;float: left;margin-right: 6px;";		    
					}
			}
			
			
			if(city!="")
			{
				if(!city.match(alfaspe))  
		        {  
					flag = 'false';
					$("#cityDiv").show();
			        $("#cityDiv").html("Enter alphabet for City Name");	
			        document.getElementById("city").style = "width:120px;float: left;margin-right: 6px;border:1px solid red;";
			        return false;
		        } 
				else
					{
					$("#cityDiv").hide();
					document.getElementById("city").style = "width:120px;float: left;margin-right: 6px;";
					}
			}
				
			
			
			if(email!="")
			{	
				
				if(!email.match(emailId))  
		            {  
					flag = 'false';
					   $("#streetDiv").show();
				       $("#streetDiv").html("Enter a proper EmailId");
				       document.getElementById("street").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
				       return false;
		            }  
				else
					{
					   $("#streetDiv").hide();
					   document.getElementById("street").style = "width:160px;float: left;margin-right: 6px;";
					}
			}
			
			
			if(lang!="")
			{
				if(!lang.match(alfaspe))  
		            {  
					    flag = 'false';
					    $("#b6Div").show();
				        $("#b6Div").html("Type a proper value");	
				        document.getElementById("b6").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
				        return false;
		            }  
				else
					{
				    $("#b6Div").hide();
				    document.getElementById("b6").style="width:160px;float: left;margin-right: 6px;";
					}
			} 
								
			if(pincode!="")
				{
				if(!pincode.match(pinnum))  
		            { 	
					flag = 'false';
					$("#pincodeDiv").show();
				    $("#pincodeDiv").html("Enter 6 digit numeric Pincode");		
				    document.getElementById("pincode").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
				    return false;
		            }   
				else
					{
					$("#pincodeDiv").hide();
					document.getElementById("pincode").style="width:120px;float: left;margin-right: 6px;";
					}
				}
			
			
			if(perpincode!="")
			{
				if(!perpincode.match(pinnum))  
		            { 
					   flag = 'false';
					    $("#perpincodeDiv").show();
				        $("#perpincodeDiv").html("Enter 6 digit numeric Pincode");		   
				        document.getElementById("perpincode").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
				        return false;
		            }
				else
					{
				    $("#perpincodeDiv").hide();
				    document.getElementById("perpincode").style="width:120px;float: left;margin-right: 6px;";
					}
			}
			
			
			if(mari_status!="")
			{
				if(!mari_status.match(alfaspe))  
		            { 
						flag = 'false';
						$("#b2Div").show();
				       $("#b2Div").html("Enter alphabet for Marital Status");		
				       document.getElementById("b2").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
				       return false;
		            }   
				else
					{
					   $("#b2Div").hide();
					   document.getElementById("b2").style="width:160px;float: left;margin-right: 6px;";
					}
			}
			
			if(country!="")
			{
				if(!country.match(alfaspe))  
		            { 		
						flag = 'false';
					    $("#countryDiv").show();
				        $("#countryDiv").html("Enter alphabet for Country Name");	
				        document.getElementById("country").style="width:120px;float: left;margin-right: 6px;border:1px solid red;"; 
				        return false;
		            }  
				else
					{
					$("#countryDiv").hide();
					document.getElementById("country").style="width:120px;float: left;margin-right: 6px;";
					}
			}
			
			
			if(percountry!="")
			{
				if(!percountry.match(alfaspe))  
		            { 
						flag = 'false';
						$("#percountryDiv").show();
				        $("#percountryDiv").html("Enter alphabet for Country Name");	
				        document.getElementById("percountry").style="width:120px;float: left;margin-right: 6px;border:1px solid red;"; 
				        return false;
		            }
				else
					{
				    $("#percountryDiv").hide();
				    document.getElementById("percountry").style="width:120px;float: left;margin-right: 6px;"; 
					}
			}
			
			
			
			if(nation!="")
			{
				if(!nation.match(alfabet))  
		            { 
						flag = 'false';
						$("#b3Div").show();
				        $("#b3Div").html("Enter alphabet for Nationality");		
				        document.getElementById("b3").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";   
				        return false;
		            }
				else
					{
				    	$("#b3Div").hide();
				    	document.getElementById("b3").style="width:160px;float: left;margin-right: 6px;"; 
					}
			}
			
			
			if(state!="")
			{
				if(!state.match(alfaspe))  
		            { 
						flag = 'false';
						$("#stateDiv").show();
				        $("#stateDiv").html("Enter alphabet for State Name");	
				        document.getElementById("state").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
		           
		            }
				else
					{
				    $("#stateDiv").hide();
				    document.getElementById("state").style="width:120px;float: left;margin-right: 6px;";
					}
			}
			
			
			if(perstate!="")
			{
				if(!perstate.match(alfaspe))  
		            { 
						flag = 'false';
						$("#perstateDiv").show();
				        $("#perstateDiv").html("Enter alphabet for for State Name");		   
				        document.getElementById("perstate").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
				        return false; 
		            }
				else
					{
				        $("#perstateDiv").hide();
				        document.getElementById("perstate").style="width:120px;float: left;margin-right: 6px;";
					}
			}
			
			if(teloff!="")
			{
				if(!teloff.match(num))  
		            { 
						flag = 'false';
						$("#b4Div").show();
				        $("#b4Div").html("Enter Numeric Value for Telephone no.");		
				        document.getElementById("b4").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
				        return false; 
		            } 
				else
					{
				    	$("#b4Div").hide();
				    	document.getElementById("b4").style="width:160px;float: left;margin-right: 6px;";
					}
			}
			
			
			if(district!="")
			{
				if(!district.match(alfaspe))  
		            { 		
						flag = 'false';
						$("#districtDiv").show();
				        $("#districtDiv").html("Enter alphabet for District Name");		
				        document.getElementById("district").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
				        return false;
		            } 
				else
					{
					 $("#districtDiv").hide();
					 document.getElementById("district").style="width:120px;float: left;margin-right: 6px;";
					}
			}
			
			
			if(perdistrict!="")
			{
				if(!perdistrict.match(alfaspe))  
		            { 
						flag = 'false';
					    $("#perdistrictDiv").show();
				        $("#perdistrictDiv").html("Enter alphabet for District Name");
				        document.getElementById("perdistrict").style="width:120px;float: left;margin-right: 6px;border:1px solid red;";
				        return false;
		            } 
				else
					{
				    $("#perdistrictDiv").hide();
				    document.getElementById("perdistrict").style="width:120px;float: left;margin-right: 6px;";
					}
			}
			
			if(telers!="")
			{
				if(!telers.match(num))  
		            { 
						flag = 'false';
						$("#b5Div").show();
				        $("#b5Div").html("Enter numeric for Telephone no.");
				        document.getElementById("b5").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
				        return false;
		            } 
				else
					{
				    $("#b5Div").hide();
				    document.getElementById("b5").style="width:160px;float: left;margin-right: 6px;";
					}
			}
			
			
			if(email=="" & lang=="" & permandal=="" & mandal=="" & pincode=="" & perpincode=="" & mari_status==""  & state=="" & perstate=="" & teloff=="" & district=="" & perdistrict=="" & telers=="")
			{
				flag='false';
				alert("Please enter some details in the corresponding fields !");
				return false;
			}
			
			
			if(flag=='true'){
	      	       	     
	            var formData = new FormData($(savebasic)[0]);
	           	            	            
	            $.ajax({  
	   		     type : "Post",   
	   		     url : "getvalidateBasicEmailId.html", 
	   		     data: "emailId="+email,
	   		     success : function(response) 
	   		     {  
	   		    	 if(response!="Exist")
	   		    		 {
	   					    		 $.ajax({  
	   						    	     type : "Post",   
	   						    	     url : "getvalidateBasicMobileNo.html", 	
	   						    	     data : "mobileNo="+teloff,
	   						    	     success : function(response) 
	   						    	     {  
	   						    	    	 
			   						    	    	 if(response!="Exist")
			   							    		 {
						   						    	    	
			   						    	    		      
						   						    	    		$.ajax({  
						   		   						    	     type : "Post",   
						   		   						    	     url : "getvalidateBasicMobileNo.html", 	
						   		   						    	     data : "mobileNo="+telers,
						   		   						    	     success : function(response) 
						   		   						    	     {  
						   		   						    	    	 
						   				   						    	    	 if(response!="Exist")
						   				   							    		 {
						   				   						    	    		 
											   				   						    	    	 $.ajax({
																   							                url: "editbasicdetail.html",
																   							                type: 'POST',
																   							                data: formData,
																   							                async: false,
																   							                success: function (data) {          
																   							                   	     
																   							                	$("#BasicSuccess").fadeIn(500); 
																   							                	 $("#BasicSuccess").fadeOut(5000);
																   							                	
																   							            var res1 = data;
																   							        	var viewprofile = res1.split(",");
																   							        	        
																   							        	var add = $(savepresentaddress).val();
																   							        	
																   							        	if (/\S/.test(add)) {    // checks that it contains any character or not.
																   							        		
																   							        		var addressAry = add.split("##");
																   								        	
																   								        	var addressTable = "<table class=\"textwidth\">";
																   											for ( var i = 0; i < addressAry.length; i = i + 9) {
																   																									   																											   												
																												addressTable += "<tr><td>"+addressAry[0 + i]+","+addressAry[3 + i]+","+
																												                           addressAry[2 + i]+","+addressAry[1 + i]+","+
																												                           addressAry[4 + i]+","+addressAry[5 + i]+","+
																												                           addressAry[6 + i]+","+addressAry[7 + i]+","+
																												                           addressAry[2 + i]+"</td></tr>";
																												                           
																												
																   												/* addressTable += "<tr><th>Door No:</th><td style=\"margin-top:2px;\">" + addressAry[0 + i]
																   														+ "</td></tr><tr><th>Street:</th><td style=\"margin-top:2px;\">" + addressAry[3 + i]																					
																   														+ "</td></tr><tr><th>Area :</th><td style=\"margin-top:2px;\">" + addressAry[2 + i]
																   														+ "</td></tr><tr><th>LandMark :</th><td style=\"margin-top:2px;\">" + addressAry[1 + i]
																   														+ "</td></tr><tr><th>City :</th><td style=\"margin-top:2px;\">" + addressAry[4 + i]	
																   														+ "</td></tr><tr><th>District :</th><td style=\"margin-top:2px;\">" + addressAry[5 + i]	
																   														+ "</td></tr><tr><th>State :</th><td style=\"margin-top:2px;\">" + addressAry[6 + i]	
																   														+ "</td></tr><tr><th>Pincode :</th><td style=\"margin-top:2px;\">" + addressAry[7 + i]	
																   														+ "</td></tr><tr><th>Country :</th><td style=\"margin-top:2px;\">" + addressAry[8 + i]	
																   														
																   														+ "</td></tr>"; */
																   											}
																   											
																   											addressTable += "</table>";
																   											
																   								        	$("#PresentAddID").html(addressTable);
																   							        	}
																   							        	
																   							        	
																   							        	
																   							        	$("#languageDiv").html($(b6).val());
																   							        	
																   							        		        		        	
																   							        	x=document.getElementById("checkStatus")
																   							        	x.disabled = !x.disabled;
																   							        	
																   							            x=document.getElementById("b2")
																   							            x.disabled = !x.disabled;  
																   							        	
																   							            x=document.getElementById("b4")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("b5")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("b6")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("b7")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("calendar")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("street")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("pincode")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("state")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("mandal")
																   							            x.disabled = !x.disabled;
																   							           
																   							            /* x=document.getElementById("country")
																   							            x.disabled = !x.disabled; */
																   							            x=document.getElementById("district")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("city")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("zone")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("door")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("landmark")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("perpincode")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("perstate")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("permandal")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("percountry")
																   							            x.disabled = !x.disabled; 
																   							            
																   							            x=document.getElementById("perdistrict")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("percity")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("perzone")
																   							            x.disabled = !x.disabled;
																   							           
																   							            x=document.getElementById("perdoor")
																   							            x.disabled = !x.disabled;
																   							            
																   							            x=document.getElementById("perlandmark")
																   							            x.disabled = !x.disabled;
																   							                   
																   							                    which.value = "Edit";
																   							                    
																   							           document.getElementById("basichistorybuttonId").style="width:100px;display:block";
																   							                    
																   							                },
																   							                cache: false,
																   							                contentType: false,
																   							                processData: false
																   							            });	 
						   		   							    	    	 
						   				   							    		 }
						   				   						    	    	 else
					   				   						    	    		 {
						   				   						    	    			$("#b5Div").show();
						   				   							        			$("#b5Div").html("Given Residence no is already exist with some other Individual !");
						   				   							        			document.getElementById("b5").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
						   				   							        			return false;
					   				   						    	    		 }
						   				   						    	    	 
						   		   						    	     },  
						   		   					    	        		    	   
						   		   					    	    });	// end of residence no validate
			   							    		 }
			   						    	    	 else
		   						    	    		 {
			   						    	    		$("#b4Div").show();
			   									        $("#b4Div").html("Given Office no is already exist with some other Individual !");		
			   									        document.getElementById("b4").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
			   									        return false;
		   						    	    		 }
			   						    	    	 
	   						    	     },  
	   					    	        		    	   
	   					    	    });	// end of office No validate
	   		    		 }
	   		    	 else
	   		    		 {
		   		    		   $("#streetDiv").show();
						       $("#streetDiv").html("Given Email Id is already exist with some other Individual !");
						       document.getElementById("street").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
						       return false;
	   		    		 }
	   		    	 
	   		      },  

	   		    });   // end of Alternative emailId validate
	            
	            
	            
	            
	          

	            return false;
	        
			}
        }	   
}


</script>


<script>
function myFunctionreg(id)
{
		   
	    if(id=="reg1")
		{
		document.getElementById("profileupdatereg1").innerHTML += 'Firstname changed';
		}
	    if(id=="reg2")
	    {
	    document.getElementById("profileupdatereg1").innerHTML += 'Middlename changed';
		}
   	    if(id=="reg3")
	    {
	    document.getElementById("profileupdatereg1").innerHTML += 'Lastname changed';
		}
	    if(id=="reg4")
	    {
	    document.getElementById("profileupdatereg1").innerHTML += 'Emailid changed';
		}
	    if(id=="reg5")
	    {
	    document.getElementById("profileupdatereg1").innerHTML += 'Mobile No changed';
		}
   document.getElementById("profileupdatereg").value=document.getElementById("profileupdatereg1").innerHTML;
}


function myFunctionsoc(id)
{
	
	
	    if(id=="sociallink")
		{
		document.getElementById("profileupdatesoc1").innerHTML += 'Sociallink changed';
		}
	    if(id=="socialname")
	    {
	    document.getElementById("profileupdatesoc1").innerHTML += 'Socialname changed';
		}
   	   
   document.getElementById("profileupdatesoc").value=document.getElementById("profileupdatesoc1").innerHTML;
}


function myFunctionsum(id)
{
		
	    if(id=="sum1")
		{
		document.getElementById("profileupdatesum1").innerHTML += 'Academicdetail changed';
		}
	    if(id=="sum2")
	    {
	    document.getElementById("profileupdatesum1").innerHTML += 'Brief Individual changed';
		}
	    if(id=="sum3")
	    {
	    document.getElementById("profileupdatesum1").innerHTML += 'Summary emp changed';
		}
	    if(id=="sum4")
	    {
	    document.getElementById("profileupdatesum1").innerHTML += 'Business detail changed';
		}
   	   
   document.getElementById("profileupdatesum").value=document.getElementById("profileupdatesum1").innerHTML;
}

function myFunctionfamily(id)
{		
	    if(id=="fname")
		{
		document.getElementById("profileupdatefam1").innerHTML += 'Fathername changed';
		}
	   
	    if(id=="mname")
	    {
	    document.getElementById("profileupdatefam1").innerHTML += 'Mothername changed';
		}
   	    
	    if(id=="poi")
	    {
	    document.getElementById("profileupdatefam1").innerHTML += 'FatherPOI changed';
		}
	   
   	    if(id=="mpoi")
	    {
	    document.getElementById("profileupdatefam1").innerHTML += 'MotherPOI changed';
		}
	    
	    if(id=="spoi")
	    {
	    document.getElementById("profileupdatefam1").innerHTML += 'Spouse POI changed';
		}
	    
	    if(id=="sname")
	    {
	    document.getElementById("profileupdatefam1").innerHTML += 'Spouse Name changed';
		}
	    
	    if(id=="bname")
	    {
	    document.getElementById("profileupdatefam1").innerHTML += 'Sibling Name changed';
		}
	    
	    if(id=="bPOI")
	    {
	    document.getElementById("profileupdatefam1").innerHTML += 'Sibling POI changed';
		}
   	
	  document.getElementById("profileupdatefam").value=document.getElementById("profileupdatefam1").innerHTML;
	  
}


function myFunctionbasic(id)
{  
	    if(id=="gender")
		{
		document.getElementById("profileupdatebasic1").innerHTML += 'Gender changed';
		}
	    if(id=="b2")
	    {
	    document.getElementById("profileupdatebasic1").innerHTML += 'Matrimony status changed';
		}
   	    if(id=="b3")
	    {
	    document.getElementById("profileupdatebasic1").innerHTML += 'Nationality changed';
		}
	    if(id=="b4")
	    {
	    document.getElementById("profileupdatebasic1").innerHTML += 'Telloffice data changed';
		}
	    if(id=="b5")
	    {
	    document.getElementById("profileupdatebasic1").innerHTML += 'Tellres data changed';
		}
	    
	    if(id=="b6")
	    {
	    document.getElementById("profileupdatebasic1").innerHTML += 'Languages changed';
		}
	    
	    if(id=="b7")
	    {
	    document.getElementById("profileupdatebasic1").innerHTML += 'Hobbies changed';
		}
	  if(id=="inputDate")
	    {
	    document.getElementById("profileupdatebasic1").innerHTML += 'DOB changed';
		}
   document.getElementById("profileupdatebasic").value=document.getElementById("profileupdatebasic1").innerHTML;
}


function myFunctionmat(id)
{	
	    if(id=="mat1")
		{
		document.getElementById("profileupdatemat1").innerHTML += 'Spousename changed';
		}
	    if(id=="mat2")
	    {
	    document.getElementById("profileupdatemat1").innerHTML += 'SpousePOI changed';
		}
   	    if(id=="mat3")
	    {
	    document.getElementById("profileupdatemat1").innerHTML += 'Gender changed';
		}
	    if(id=="mat4")
	    {
	    document.getElementById("profileupdatemat1").innerHTML += 'horoscope_info changed';
		}
	    if(id=="mat5")
	    {
	    document.getElementById("profileupdatemat1").innerHTML += 'food preferred changed';
		}
	    document.getElementById("profileupdatemat").value=document.getElementById("profileupdatemat1").innerHTML;
}

</script>





<script>
 $(document).ready(function () {
        $("#dialog").dialog({ autoOpen: false });
 
          
                return false;
           
    });
</script>

<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
   
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
		$('.topprofilestyle').show();
		
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 						
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {		    		 
			    		 var values = "No data found";			    		
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('.topprofilestyle').hide();
		    $('#searchicon').hide();
		    $('#searchhomepage').html("");
		    $('#searchhomepage').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
					 	
	    }
}


function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}

function individualsearchhomepage(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}


</script>





<script>
function switchToEdit(which)
{
	
	if ( which.value == "Edit" )
	{  which.value = "Save";
	
	var divvalue=document.getElementById("editsumdiv").innerHTML;

	
	}
	
	else
		if ( which.value == "Save" )
        {which.value = "Edit";
        document.getElementById("landpagedescription").value=document.getElementById("editsumdiv").innerHTML ;
        $('#flash').delay(500).fadeIn('normal', function() {
  	      $(this).delay(2500).fadeOut();
  	   });
        
        
        document.myform.submit();
        }
	
	var editter = document.getElementById('editsumdiv');
	editsumdiv.contentEditable="true"; 
	
	
	  
	var editname = document.getElementById('editname');
	editname.value="save";
	
}
function divvalue()
{
	
	document.getElementById("landpagedescription").value=document.getElementById("editsumdiv").innerHTML ;
	alert(""+document.getElementById("landpagedescription").value);
}


/*home page*/

function familyHistory()
{
	 $.ajax({
			type : "post",
			url :"familyHistory.html",
			cache : false,
			success : function(response) {
				
			 if(response.length>2)
			    {		 
				 
				 //var constructHistory = response.substring(1,response.length-1);
					 
		    	 var familyHistory = response.split("KYCSEP");
		    	 
					var index = 0;
					var familyHistoryTable = "<table class='CSSTableGenerator' style=\"width:700px\"><tr><th>S.NO</th><th>Father Name</th><th>Father POI</th><th>Mother Name</th><th>Mother POI</th><th>Sibling Name</th><th>Sibling POI</th><th>Spouse Name</th><th>Spouse POI</th><th>Horoscope</th><th>Food Preferable</th></tr>";
					for ( var i = 0; i < familyHistory.length; i = i + 10) {
						index += 1;
						familyHistoryTable += "<tr><td>" + (index)
								+ "</td><td>" + familyHistory[0 + i]
								+ "</td><td>" + familyHistory[1 + i]
								+ "</td><td>" + familyHistory[2 + i]
								+ "</td><td>" + familyHistory[3 + i]
								+ "</td><td>" + familyHistory[4 + i]	
								+ "</td><td>" + familyHistory[5 + i]	
								+ "</td><td>" + familyHistory[6 + i]	
								+ "</td><td>" + familyHistory[7 + i]	
								+ "</td><td>" + familyHistory[8 + i]	
								+ "</td><td>" + familyHistory[9 + i]										
								+ "</td></tr>";
					}
					familyHistoryTable += "</table>";
					$('#editfamilydetail').hide();
					$('#showfamilydetail').show();
					$('#showfamilydetail').html(familyHistoryTable+"<br><input type=\"button\" class=\"backbuttonindex_histy\" value=\"Back\" onClick=\"backtofamily()\" />");
			    }
			 else
				{
				    $('#editfamilydetail').hide();
					$('#showfamilydetail').show();
					$('#showfamilydetail').html("<h1>Till now you don't have history for Family Details!</h1><br><input type=\"button\" value=\"Back\" onClick=\"backtofamily()\" />");
				}
										
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	}
function backtofamily()
{
	$('#editfamilydetail').show();
	$('#showfamilydetail').hide();
	}
	



function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/

/*profile page*/

	function editProfile(handlerToHit) {
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					// $("#viewUsers1")
					//	.html(response);
					},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		
		$("#titleBar").html("<h2><span>VIEW PROFILE</span></h2>");
		
	}
	
	function editProfile1(handlerToHit) {
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
						alert(response);				
				//	 $("#viewUsers1")
					//	.html(response);
			
			
					},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		
		$("#titleBar").html("<h2><span>EDIT PROFILE</span></h2>");
		
	}
	
	function editProfile2(handlerToHit) {
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
					},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		
		$("#titleBar").html("<h2><span>MY NOTIFICATIONS</span></h2>");
		
	}
	
	function editProfile3(handlerToHit) {
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
					},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		
		$("#titleBar").html("<h2><span>MY SETTINGS</span></h2>");
		
	}
/*end profile*/








/* All Scripts for MyDocument pannel  */
	
	
	
	function Profile(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 $("#center-body-div").hide();			 		 		
		$("#titleBar").html("<h2><span>MY VISITORS</span></h2>");
	}
	
	function Profile1(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 $("#center-body-div").hide();	 	
		$("#titleBar").html("<h2><span>MY NOTIFICATION</span></h2>");
	}
	
	
	function Profile3(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
										
				},
				
			});
		 $("#center-body-div").hide(); 		 		 		
		$("#titleBar").html("<h2><span>MY HISTORY</span></h2>");
	}
	

	function reSelect() {
				
	 $.ajax
	      ({
			type : "post",			
			url : "reselect.html",			
			success : function(response) {
														
				$("#viewUsers1")
					.html(response);
												
			},
			error : function() {
				alert('Error while fetching response');
			}
		});	
	 $("#titleBar").html("<h2><span>DOCUMENTS</span></h2>");

	}  
	
		function cancelFormDetails(val1)
		{
			if (document.getElementById("hiddenAccform"+val1+"").style.display =="block") {
				document.getElementById("hiddenAccform"+val1+"").style.display ="none";
				
				document.getElementById("invisible"+val1+"").style.display ="block";								
				}
				else {
				document.getElementById("hiddenAccform"+val1+"").style.display ="block";
				
				}
				if (document.getElementById("showAccform"+val1+"").style.display =="block") {
				document.getElementById("showAccform"+val1+"").style.display ="none";
				
				}
				else {
				document.getElementById("showAccform"+val1+"").style.display ="block";
				
				}
		}
		
		/* <!-- change the state noneditable to editable for document--> */
		
		function changeState(docHierId)
		{		   
		   var docHir = document.getElementById(docHierId).value;		
			
			var docHirarr = docHir.split(",");
							
			for(var j=0;j<docHirarr.length;j++)
			{			
			   var docHir1 = docHirarr[j];
			   var docHir2 = docHir1.split(":");			   	   			   
		       document.getElementById(docHir2[0]).removeAttribute("readonly");	
		       document.getElementById(docHir2[0]).removeAttribute("disabled");
			}
		}
		
		
		/* <!-- End of code for noneditable to editabel for document-->  */
		
		function chooseAcaIds() {
			
			
			var click = [];
			$(".checkbox:checked").each(function() {
		        click.push($(this).val());
		    });
			 
			if(click.length==0)
				{
				   alert("Select atleast one Document");
				}
			else
				{
			 $.ajax({
					type : "post",				
					url : "add/choosen/academic/category.html",				
					data :"click="+click,	  
					success : function(response) {
										
						 $("#viewUsers1")
							.html(response);
										
					},
					error : function() {
						alert('Error while fetching response');
					}
				});		 		 		 		
			$("#titleBar").html("<h2><span>DOCUMENTS</span></h2>");
		}
	}
				
	/* End of MyDocument Scripts  */
	
	/*Start script for Plans*/
	
	 function Myplansview(handlerToHit) {
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
								 
				 $("#viewUsers1")
					.html(response);
				},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		
	$("#titleBar").html("<h2><span>MY PLAN</span></h2>");
	
	$("#searchicon").hide();
}
	
	 function Myplansmigrate(handlerToHit) {
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										 
					 $("#viewUsers1")
						.html(response);
					},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		
		$("#titleBar").html("<h2><span>MIGRATE PLAN</span></h2>");
		
		 $("#searchicon").hide();
	}
	 
	 function Myplansenquire(handlerToHit) {
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										 
					 $("#viewUsers1")
						.html(response);
					},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		
		$("#titleBar").html("<h2><span>ENQUIRE PLAN</span></h2>");
		
		 $("#searchicon").hide();
	}
    /*End Script for Plans */
    
        
	/* All scripts for Access Manager */
	
	function generateCode(handlerToHit) {
		 		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				
			});
		 		 		 		
		$("#titleBar").html("<h2><span>GENERATE CODE</span></h2>");
	}
	function allowAccess(handlerToHit) {
 		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>ALLOW ACCESS</span></h2>");
	}
	function revokeAccess(handlerToHit) {
 		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>REVOKE CODE</span></h2>");
	}
	function accessHistory(handlerToHit) {
 		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>SHARING HISTORY</span></h2>");
	}
	
	function applyCode(handlerToHit) {
 		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				
			});
		 		 		 		
		$("#titleBar").html("<h2><span>APPLY CODE</span></h2>");
	}
		
	
	 /* End of AccessManger scripts */

</script>
      
        <style>
            .notClicked {color: black}
            .Clicked {color: red}
        </style>
        
             
<script>
    

function uploadbasicdetail1() {
	 		
        var flag = 'true';        	
		var profielFile = document.getElementById("file_browse").value;		 
		var ext=profielFile.substring(profielFile.lastIndexOf('.')+1);
		var extension = new Array("jpg","jpeg","png");						    
		var condition = "NotGranted";
		
		 document.getElementById('waitlight').style.display='block';
	     document.getElementById('waitfade').style.display='block';
	   
	     
		   
		for(var m=0;m<extension.length;m++)
		    {
				if(ext==extension[m])
				    {				    	    
				       condition="Granted";				    	    
				    }				    
			}
		
		if(condition=="NotGranted")
			{
				 flag ='false';
				 document.getElementById('waitlight').style.display='none';
			     document.getElementById('waitfade').style.display='none';
				 alert("only image files are allowed!");
			}	 		 
		
		var fileDetails = document.getElementById("file_browse");
		var fileSize = fileDetails.files[0];
		var fileSizeinBytes = fileSize.size;
		var sizeinKB = +fileSizeinBytes / 1024;
		var sizeinMB = +sizeinKB / 1024;
				
		if(sizeinMB>2)
			{
			    flag ='false';
			    document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';
			    alert("Image files should not more then 2 MB !");
			}
		
		
		if(flag=='true')
			{
			   	

			   var oMyForm = new FormData($("#editbasicdetail")[0]);						   
			   //oMyForm.append("Passportsize", file_browse.files[0]);
			 
			 
			   $.ajax({
			        url: "saveProfilepicture.html",	
			        type: 'POST',
			        data: oMyForm,			        
			        success: function (response) {          
			           
			        	 document.getElementById('sociallight').style.display='block';
	    			     document.getElementById('socialfade').style.display='block';
	    			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
	    				 $("#socialdisplaySharedDocDiv").html("Profile picture has beed added successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReportPopup('success')\"  /> </div>");  
       		   	    							    	 
			        	
			        	document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
			        					      
			        	
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });
			}
		 		     
}
  
  
  

  
</script>  

<script>
   
 function lasttry() {
	 
	var oMyForm = new FormData();				
	oMyForm.append("Gender",$(b1).val());
			
	    $.ajax({
	    	
	    	type: "post",	   
	    	url: "savebasicdetail.html",	             
	    	data: oMyForm,	 	    	
	        processData: false,
	        contentType: false,
	        success: function (response) {
	        	
	        	alert(response);
	        	
	        },
	                
	    });
     
}
   
   </script>  


 <script>
function openbrowse()
 
 { 
	 
	 var obj = document.getElementById('openbrowse');
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	      document.getElementById("removeaddphoto").style.display="block";
	     
	    }	  
 }

 
 function basicHistory()
 {
	   
	   $.ajax({	    	
	    	type: "post",	   
	    	url: "basicDetailHistory.html",	             
	        success: function (response) {
	        	
	            
	        	var basicHistory = response.split("$$$");
				var index = 0;
				var basicHistoryTable = "<table class='CSSTableGenerator' style=\"width:800px\"><tr><th>S.NO</th><th>Natonality</th><th>DOB</th><th>Email_id</th><th>Hobbies</th><th>Present Address</th><th >Permanent Address</th><th>Language</th><th>Marital Status</th><th>Date of Change</th></tr>";
				for ( var i = 0; i < basicHistory.length; i = i + 9) {
					index += 1;
					basicHistoryTable += "<tr><td>" + (index)
							+ "</td><td>" + basicHistory[0 + i]
							+ "</td><td>" + basicHistory[1 + i]
							+ "</td><td>" + basicHistory[2 + i]
							+ "</td><td>" + basicHistory[3 + i]
							+ "</td><td>" + basicHistory[4 + i]
							+ "</td><td>" + basicHistory[5 + i]
							+ "</td><td>" + basicHistory[6 + i]
							+ "</td><td>" + basicHistory[7 + i]
							+ "</td><td>" + basicHistory[8 + i]							
							+ "</td></tr>";
				}
				basicHistoryTable += "</table>";
				$('#savebasic').hide();
				$('#showBasicHistory').show();
				$('#showBasicHistory').html(basicHistoryTable+"<br><input type=\"button\" class=\"backbuttonindex_histy\" value=\"Back\" onClick=\"backtoBasic()\" />");
				
	        },
	        error : function() {
				alert('Failed due to server problem please try again later !');
			}	        
	    });
 }
 
 function backtoBasic()
 {
	 $('#savebasic').show();
	 $('#showBasicHistory').hide();
 }
 

 </script>
 
<!-- <script src="resources/texteditor/nicEdit.js" type="text/javascript"></script> -->

<script type="text/javascript" src="resources/jscripts/tiny_mce/tiny_mce.js"></script>

<!-- Script for Text Area -->

<script>
tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",
		// Replace values for the template plugin
		template_replace_values : 
		{
		username : "Some User",
		staffid : "991234"
		}
	});
	
</script>

<!-- End of Script for Text Area -->

<script>
var area1, area2;

function toggleArea1() {
	
    var buttonName = document.getElementById("htmlbutton").innerHTML; 
    
    var existDivValue = document.getElementById("myArea1").innerHTML;
    
    if(buttonName=="Edit")
    	{
	    	$("#htmlbutton").empty();
	    	$("#htmlbutton").html("Save");
	    	
	    	$("#myArea1").hide();
	    	$("#textareaId").show();
	    	
	    	tinyMCE.get("indaboutUs").setContent(existDivValue);	    	
	    	$("#seemore").hide();
    	}    
    if(buttonName=="Save")
    	{
    		document.getElementById('waitlight').style.display='block';
	   		document.getElementById('waitfade').style.display='block';
	   		
	    	$("#htmlbutton").empty();
	    	$("#htmlbutton").html("Edit");
	    	
	        var divvalue = tinymce.get("indaboutUs").getContent();
	        	        
	        var formData = new FormData($("#textareaform")[0]);
	         
	        formData.append("textAreaContent",divvalue);
	        
            $.ajax({
                url: "saveAboutUs.html",
                type: 'POST',
                data: formData,
                async: false,
                success: function(response) {          
                   	
                	document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';
				    
                	$("#aboutus").fadeIn(500); 
    	            $("#aboutus").fadeOut(5000);
    	            
    	            var data = response.split("KYCSEPERATORKYC");
    	            
    	            $("#myArea1").show();
    	            	    	           
    	        	$("#myArea1").html(data[0].substring(0,data[0].length-1));
    	        	
    	        	$("#textareaId").hide();
    	        	
    	        	var element = document.querySelector('#myArea1');
    		           	        	
    		        if( element.offsetHeight < element.scrollHeight) {
    		        	  
    		        	     $("#seemore").show();
    		        	}
    		        else
    		        	{
    		        	    $("#seemore").hide();
    		        	}
    	        	
                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
            
    	}
}

function getAboutUs()
{			
	 $.ajax({
	    	
	    	type: "post",	   
	    	url: "getAboutUsInfo.html",	              	    		       
	        success: function (response) {
	        	 
	        	var data = response.split("KYCSEPERATORKYC");
		            
		        $("#myArea1").html(data[0].substring(0,data[0].length-1));	
		        
		        var element = document.querySelector('#myArea1');
		        
		        if( element.offsetHeight < element.scrollHeight) {
		        	  
		        	     $("#seemore").show();
		        	}
		        	
	        },	        	        
	     }); 
	 
}


function passwordsetting()
{
	
	 $.ajax({
			type : "post",
			url : "setting.html",
			cache : false,
			success : function(response) {		
		
				$("#viewUsers1").html(response);
				
			},
			
		});
	 		 		 		
	$("#titleBar").html("<h2><span>PASSWORD SETTING</span></h2>");
	
}


function visibilitySetting()
{
	
	 $.ajax({
			type : "post",
			url : "visibility/setting.html",
			cache : false,
			success : function(response) {		
					
				$("#viewUsers1").html(response);
												 
			},			
		});
	 		 		 		
	$("#titleBar").html("<h2><span>VISIBILITY SETTING</span></h2>");
	
}


function myUsageDetails()
{	
	 $.ajax({
			type : "post",
			url : "usage/details.html",
			cache : false,
			success : function(response) {		
				
				$("#viewUsers1").html(response);								
			},			
		});	 		 		 		
	$("#titleBar").html("<h2><span>USAGE DETAILS</span></h2>");
	
}



function showSave()
{
	var obj1 = document.getElementById("saveimg_button_wrapper"); 
	var obj2 = document.getElementById("file_browse_wrapper");
	var obj3 = document.getElementById("removeaddphoto");
	var obj4 = document.getElementById("removeaddphotoDiv");
	var file = document.getElementById("file_browse").value;
	
    if (file!="")
    {
      obj4.style.display="none";	 	
      obj3.style.display="none";	
      obj2.style.display="none";	
      obj1.style.display='block';            
    }
    
}
	
</script>

<script src="resources/js/kyc_side_login.js"></script>

<script type="text/javascript">


function addProfilePhoto()
{
	var profilePhoto = document.getElementById("file_browse").value;
	
	var flag = 'true';        	
	var profielFile = document.getElementById("file_browse").value;		 
	var ext=profielFile.substring(profielFile.lastIndexOf('.')+1);
	var extension = new Array("jpg","jpeg","png");						    
	var condition = "NotGranted";
	
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
       
	for(var m=0;m<extension.length;m++)
	    {
			if(ext==extension[m])
			    {				    	    
			       condition="Granted";				    	    
			    }				    
		}
	
	if(condition=="NotGranted")
		{
			 flag ='false';
			 
			 document.getElementById('waitlight').style.display='none';
		     document.getElementById('waitfade').style.display='none';
		     
		     
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ffecec";
			 $("#socialdisplaySharedDocDiv").html("Only image files are allowed ! <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReportPopup('warning')\"  /> </div>");  
	   	    		
		    
			 return false;
		}	 		 
			
	var fileDetails = document.getElementById("file_browse");
	var fileSize = fileDetails.files[0];
	var fileSizeinBytes = fileSize.size;
	var sizeinKB = +fileSizeinBytes / 1024;
	var sizeinMB = +sizeinKB / 1024;
			
	if(sizeinMB>2)
		{
		    flag ='false';
		    document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
		    
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ffecec";
			 $("#socialdisplaySharedDocDiv").html("Image files should not more then 2 MB ! <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReportPopup('warning')\"  /> </div>");  
	   	    
		     return false;
		}
	
	if(flag=='true')
		{
			if(profilePhoto!='')
				{		
				    document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';
				   $("#profilePhotoDiv").show();		  
				}
		}
}

	
	
function CheckFormatofProfilePhoto()
{
	var profilePhoto = document.getElementById("file_browse").value;
	
	var flag = 'true';        	
	var profielFile = document.getElementById("file_browse").value;		 
	var ext=profielFile.substring(profielFile.lastIndexOf('.')+1);
	var extension = new Array("jpg","jpeg","png");						    
	var condition = "NotGranted";
	 
	for(var m=0;m<extension.length;m++)
	    {
			if(ext==extension[m])
			    {				    	    
			       condition="Granted";				    	    
			    }				    
		}
	
	if(condition=="NotGranted")
		{
			 flag ='false';		
			 
			 document.getElementById("selectFile").style.color ="blue";			 
			 $("#selectFile").html("Your selected file is "+profilePhoto); 
			 
			 document.getElementById("selectFileInfo").style.color ="red";			 
			 $("#selectFileInfo").html("Only image files are allowed !"); 
			 
			 return false;
		}	 		 
			
	var fileDetails = document.getElementById("file_browse");
	var fileSize = fileDetails.files[0];
	var fileSizeinBytes = fileSize.size;
	var sizeinKB = +fileSizeinBytes / 1024;
	var sizeinMB = +sizeinKB / 1024;
			
	if(sizeinMB>2)
		{
		     flag ='false';
		    
		     document.getElementById("selectFile").style.color ="blue";			 
			 $("#selectFile").html("Your selected file is "+profilePhoto); 
			 
			 document.getElementById("selectFileInfo").style.color ="red";			 
			 $("#selectFileInfo").html("Image files should not more then 2 MB !"); 
				   	    
		     return false;
		}
	
	if(flag=='true')
		{		    
		     document.getElementById("selectFile").style.color ="green";			 
			 $("#selectFile").html("Your selected file is "+profilePhoto); 
			 
			 document.getElementById("selectFileInfo").style.color ="green";			 
			 $("#selectFileInfo").html("Now you can save your file."); 
			 
		}
}


function uploadProfilePicture() {
	 		
        var flag = 'true';        	
		var profielFile = document.getElementById("file_browse").value;		 
		var ext=profielFile.substring(profielFile.lastIndexOf('.')+1);
		var extension = new Array("jpg","jpeg","png");						    
		var condition = "NotGranted";
		   		   
		for(var m=0;m<extension.length;m++)
		    {
				if(ext==extension[m])
				    {				    	    
				       condition="Granted";				    	    
				    }				    
			}
		
		if(profielFile=="")
			{
				 flag ='false';				
				 document.getElementById("selectFile").style.color ="blue";			 
				 $("#selectFile").html("You didn't select any file."); 
				 
				 document.getElementById("selectFileInfo").style.color ="red";			 
				 $("#selectFileInfo").html("Please select a file before save."); 
				 return false;
			}
		
		if(condition=="NotGranted")
			{
				 flag ='false';				
				 document.getElementById("selectFile").style.color ="blue";			 
				 $("#selectFile").html("Your selected file is "+profielFile); 
				 
				 document.getElementById("selectFileInfo").style.color ="red";			 
				 $("#selectFileInfo").html("Only image files are allowed !"); 
				 return false;
			}	 		 
		
		var fileDetails = document.getElementById("file_browse");
		var fileSize = fileDetails.files[0];
		var fileSizeinBytes = fileSize.size;
		var sizeinKB = +fileSizeinBytes / 1024;
		var sizeinMB = +sizeinKB / 1024;
				
		if(sizeinMB>2)
			{
			    flag ='false';
			    document.getElementById("selectFile").style.color ="blue";			 
				$("#selectFile").html("Your selected file is "+profielFile); 
				 
				document.getElementById("selectFileInfo").style.color ="red";			 
				$("#selectFileInfo").html("Image files should not more then 2 MB !"); 		
				return false;
			}
		
		
		if(flag=='true')
			{
			   	
			   var oMyForm = new FormData($("#uploadProfileImage")[0]);						   
			   
			   document.getElementById("profilePhotoChangelight").style.display = "none";
			   document.getElementById("profilePhotoChangefade").style.display = "none";
			   
			   document.getElementById('waitlight').style.display='block';
			   document.getElementById('waitfade').style.display='block';
			    
			    
			   $.ajax({
			        url: "saveProfilepicture.html",	
			        type: 'POST',
			        data: oMyForm,			        
			        success: function (response) {          
			           
			        	document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
					    
			        	 document.getElementById('sociallight').style.display='block';
	    			     document.getElementById('socialfade').style.display='block';
	    			     
	    			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
	    				 $("#socialdisplaySharedDocDiv").html("Profile picture has beed added successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReportPopup('success')\"  /> </div>");  
       		   	    							    	 
			        	
			        	
			        					      
			        	
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });
			}
		 		     
}

function profilePicHistory()
{
	 $.ajax({
			type : "post",
			url : "profilePhoto.html",			
			success : function(response) {		
						
			    $("#content").html(response);
			
			},
			
		});
}


function ChangeProfilePic(picName)
{	
	$.ajax({
		type : "post",
		url : "updateProfilePhoto.html",
		data : "pictureName="+picName,
		success : function(response) {		
			 			
			 document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
			 $("#socialdisplaySharedDocDiv").html("Profile picture Changed Successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReportPopup('success')\"  /> </div>");  
	   	    			
		},		
	});  	
}


function searchedProfileDetails()
{	
    var beforeSearchedIndId = document.getElementById("beforeSearchedIndId").value;
    if(beforeSearchedIndId!='null')
    	{    	  
    	   window.location = "viewSearchedProfile.html?asdgth="+beforeSearchedIndId+"";
    	   document.getElementById("beforeSearchedIndId").value = "";
    	}    		
}


function viewHomeProfile(url) 
{
	window.location = url;	 
}	

function backtoReg()
{
	$('#editregistration').show();
	$('#showRegDetails').hide();
}

function reghistory() {
	 	
	 $.ajax({
			type : "post",
			url :"reghistory.html",
			cache : false,
			success : function(response) {
				
				if(response.length>2)
				{					
				   var constructHistory = response.substring(1,response.length-1);
				
		    	   var indHistory = constructHistory.split(",");
		    	 
					var index = 0;
					var indHistoryTable = "<table class='CSSTableGenerator' style=\"width:700px\"><tr><th>S.NO</th><th>First Name</th><th>Middle Name</th><th>Last Name</th><th>Gender</th><th>Email_id</th><th>Ph.No</th><th>Date of Change</th></tr>";
					for ( var i = 0; i < indHistory.length; i = i + 7) {
						index += 1;
						indHistoryTable += "<tr><td>" + (index)
								+ "</td><td>" + indHistory[0 + i]
								+ "</td><td>" + indHistory[1 + i]
								+ "</td><td>" + indHistory[2 + i]
								+ "</td><td>" + indHistory[3 + i]
								+ "</td><td>" + indHistory[4 + i]
								+ "</td><td>" + indHistory[5 + i]
								+ "</td><td>" + indHistory[6 + i]
								+ "</td></tr>";
					}
					indHistoryTable += "</table>";
					$('#editregistration').hide();
					$('#showRegDetails').show();
					$('#showRegDetails').html(indHistoryTable+"<br><input type=\"button\"  class=\"backbuttonindex_histy\" value=\"Back\" onClick=\"backtoReg()\" />");
															
			
			}
				else
		          {
	        	    $('#editregistration').hide();
					$('#showRegDetails').show();
					$('#showRegDetails').html("<h1>Till now you don't have history for registration!</h1><input type=\"button\" value=\"Back\" onClick=\"backtoReg()\" />");
		          }
			},
			
		});
	 		 		 		
	} 


function socialHistory()
{	
	 $.ajax({
			type : "post",
			url :"socialHistory.html",
			cache : false,
			success : function(response) {
												
				if(response.length>2)
				{
									 				
		    	 var socialHistory = response.split("URLDIV");
				 var index = 0;
				 					 
				 var socialHistoryTable = "<table class='CSSTableGenerator' style=\"width:700px\"><tr><th>S.NO</th><th>Facebook</th><th>Linkedin</th><th>Twitter</th><th>Date of Change</th></tr>";
					for ( var i = 0; i < socialHistory.length; i = i + 4) {
						
						
						index += 1;
						socialHistoryTable += "<tr><td>" + (index)
								+ "</td><td>" + socialHistory[0 + i]																					
								+ "</td><td>" + socialHistory[2 + i]
								+ "</td><td>" + socialHistory[1 + i]
								+ "</td><td>" + socialHistory[3 + i]
								+ "</td></tr>";
					}
					socialHistoryTable += "</table>";
					$('#editsocialdetail').hide();
					$('#showSocialHistory').show();
					$('#showSocialHistory').html(socialHistoryTable+"<br><input type=\"button\" class=\"backbuttonindex_histy\" value=\"Back\" onClick=\"back_social()\" />"); 															
				}
				else
					{
					$('#editsocialdetail').hide();
					$('#showSocialHistory').show();
					$('#showSocialHistory').html("<h1>Till now you don't have history for registration!</h1><br><input type=\"button\" value=\"Back\" onClick=\"back_social()\" />"); 																			
					}
				},
			error : function() {
				alert('Error while fetching response');
			}
		});
}


function back_social()
{	
	$('#showSocialHistory').hide();
	$("#editsocialdetail").show();
}



</script>



<script type="text/javascript">
function copyPresent()
{
	var checkedStatus = document.getElementById("checkStatus");
	var door = document.getElementById("door").value;
	var zone = document.getElementById("zone").value;
	var landmark = document.getElementById("landmark").value;
	var pincode =  document.getElementById("pincode").value;	
	var country = document.getElementById("country").value;		
	var state = document.getElementById("state").value;
	var district = document.getElementById("district").value;
	var mandal = document.getElementById("mandal").value;
	var city = document.getElementById("city").value;
	
		
	if(checkedStatus.checked==true)
		{
		
		document.getElementById("perdoor").value = door;
		document.getElementById("perzone").value = zone;
		document.getElementById("perlandmark").value = landmark;
		document.getElementById("perpincode").value = pincode;
		document.getElementById("percountry").value = country;
		document.getElementById("perstate").value = state;
		document.getElementById("perdistrict").value = district;
		document.getElementById("permandal").value = mandal;
		document.getElementById("percity").value = city;
		
		document.getElementById("permanentaddress").value = door + "##" + landmark + "##" + zone + "##" + mandal + "##" + city + "##" + district + "##" + state + "##" + pincode + "##" + country;
		
		}
		
    if(checkedStatus.checked==false)
    	{
    	
    	document.getElementById("perdoor").value = "";
		document.getElementById("perzone").value = "";
		document.getElementById("perlandmark").value = "";
		document.getElementById("perpincode").value = "";
		document.getElementById("percountry").value = "";
		document.getElementById("perstate").value = "";
		document.getElementById("perdistrict").value = "";
		document.getElementById("permandal").value = "";
		document.getElementById("percity").value = "";
		
		document.getElementById("permanentaddress").value = "";
		
    	}
		
}

function saveFamilyDetail()
{
	var value=document.getElementById("editfamily").value;	
	
	if(value=="Edit")
		{
		
		document.getElementById("editfamily").value="Save";
		
		    x=document.getElementById("fname")
	 		x.disabled = !x.disabled;
		    document.getElementById("fname").style.backgroundColor = "#fff";
		    
	 		x=document.getElementById("poi")
	 		x.disabled = !x.disabled;
	 		document.getElementById("poi").style.backgroundColor = "#fff";
	 		
	 		x=document.getElementById("mname")
	 		x.disabled = !x.disabled;
	 		document.getElementById("mname").style.backgroundColor = "#fff";
	 		
	 		x=document.getElementById("mpoi")
	 		x.disabled = !x.disabled;
	 		document.getElementById("mpoi").style.backgroundColor = "#fff";
	 		
	 		x=document.getElementById("bname")
	 		x.disabled = !x.disabled;
	 		document.getElementById("bname").style.backgroundColor = "#fff";
	 		
	 		x=document.getElementById("bpoi")
	 		x.disabled = !x.disabled;
	 		document.getElementById("bpoi").style.backgroundColor = "#fff";
	 		
	 		x=document.getElementById("sname")
	 		x.disabled = !x.disabled;
	 		document.getElementById("sname").style.backgroundColor = "#fff";
	 		
	 		x=document.getElementById("spoi")
	 		x.disabled = !x.disabled;
	 		document.getElementById("spoi").style.backgroundColor = "#fff";
	 		
	 		x=document.getElementById("hinfo")
	 		x.disabled = !x.disabled;
	 		document.getElementById("hinfo").style.backgroundColor = "#fff";
	 		
	 		x=document.getElementById("foodpreferred")
	 		x.disabled = !x.disabled;
	 		document.getElementById("foodpreferred").style.backgroundColor = "#fff";
		}
	
	if(value=="Save")
	{
		var flag = 'true';
		var name = /^[a-zA-Z ]+$/;
		var poi= /^[a-zA-Z0-9]*$/;
		var regexHoro  =  /^[a-zA-Z0-9,]*$/; 
		
		var fname = document.getElementById("fname").value;
		var fpoi = document.getElementById("poi").value;
		var mname = document.getElementById("mname").value;
		var mpoi = document.getElementById("mpoi").value;
		var bname = document.getElementById("bname").value;
		var bpoi = document.getElementById("bpoi").value;
		
		var sname= document.getElementById("sname").value;
		var spoi = document.getElementById("spoi").value;
		var horo = document.getElementById("hinfo").value;
		var food = document.getElementById("foodpreferred").value;
		
		if(fname=="" & fpoi=="" & mname=="" & mpoi=="" & bname=="" & bpoi=="" & sname=="" & spoi=="" & horo=="" & food=="")
		{
			flag='false';
			alert("Please enter some details in the corresponding fields !");
		}
				
		if(fname!="")
		{
			if(!fname.match(name))  
	        {  	
				flag = 'false';
				$("#fnameDiv").show();
		        $("#fnameDiv").html("Enter alphabet for Father Name!");
		        document.getElementById("fname").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
			{
				$("#fnameDiv").hide();
				document.getElementById("fname").style = "width:160px;float: left;margin-right: 6px;";
			}
		}
		
		if(fpoi!="")
		{
			if(!fpoi.match(poi))  
	        {  				 	
				flag = 'false';
				$("#poiDiv").show();
		        $("#poiDiv").html("Enter alphanumeric for Father POI!");
		        document.getElementById("poi").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
			{
				$("#poiDiv").hide();
				document.getElementById("poi").style="width:160px;float: left;margin-right: 6px;";
			}
		}
		
		if(mname!="")
		{
			if(!mname.match(name))  
	        {  	
				flag = 'false';
				$("#mnameDiv").show();
		        $("#mnameDiv").html("Enter alphabet for Mother Name");
		        document.getElementById("mname").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
			{
				$("#mnameDiv").hide();
				document.getElementById("mname").style = "width:160px;float: left;margin-right: 6px;";
			}
		}
				
		if(mpoi!="")
		{
			if(!mpoi.match(poi))  
	        {  	
				flag = 'false';
				$("#mpoiDiv").show();
		        $("#mpoiDiv").html("Enter alphanumeric for Mothere POI");
		        document.getElementById("mpoi").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
			{
				$("#mpoiDiv").hide();
				document.getElementById("mpoi").style="width:160px;float:left;margin-right: 6px;";
			}
		}
		
		if(bname!="")
		{
			if(!bname.match(name))  
	        {  	
				flag = 'false';
				$("#bnameDiv").show();
		        $("#bnameDiv").html("Enter alphanumeric for Sibling Name");
		        document.getElementById("bname").style="width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
			{
				$("#bnameDiv").hide();
				document.getElementById("bname").style="width:160px;float: left;margin-right: 6px;";
			}
		}
		
		
		if(bpoi!="")
		{
			if(!bpoi.match(poi))  
	        {  	
				flag = 'false';
				$("#bpoiDiv").show();
		        $("#bpoiDiv").html("Enter alphanumeric for Sibling POI");
		        document.getElementById("bpoi").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
			{
				$("#bpoiDiv").hide();
				document.getElementById("bpoi").style = "width:160px;float: left;margin-right: 6px;";
			}
		}
		
					
		if(sname!="")
		{
			if(!sname.match(name))  
	        {  	
				flag = 'false';
				$("#snameDiv").show();
		        $("#snameDiv").html("Enter alphabet for Spouse Name");
		        document.getElementById("sname").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
			{
				$("#snameDiv").hide();
				document.getElementById("sname").style = "width:160px;float: left;margin-right: 6px;";
			}
		}
		
		
		if(spoi!="")
		{
			if(!spoi.match(poi))  
	        {  	
				flag = 'false';
				$("#spoiDiv").show();
		        $("#spoiDiv").html("Enter alfanumeric for Spouse POI");	
		        document.getElementById("spoi").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
				{
				$("#spoiDiv").hide();
				document.getElementById("spoi").style = "width:160px;float: left;margin-right: 6px;";
				}
		}
		
		if(horo!="")
		{
			if(!horo.match(regexHoro))  
	        {  	
				flag = 'false';
				$("#hinfoDiv").show();
		        $("#hinfoDiv").html("Enter alphabet for Horoscope!");		
		        document.getElementById("hinfo").style = "width:160px;float: left;margin-right: 6px;border:1px solid red;";
		        return false;
	        } 
			else
				{				$("#hinfoDiv").hide();
				document.getElementById("hinfo").style = "width:160px;float: left;margin-right: 6px;";
				}
		}
		
		if(flag=='true')
		{						
			$.ajax({  
				
							     type : "post",   
							     url : "editfamilydetail.html", 
							     data :$('#editfamilydetail').serialize(),	     	     	     
							     success : function(response) 
							     {  							    	
									    	 $("#familySuccess").fadeIn(500); 
									    	 $("#familySuccess").fadeOut(5000);
									    	 
									    	 document.getElementById("editfamily").value="Edit";
									    	
									    	
									    	x=document.getElementById("fname")
									 		x.disabled = !x.disabled;
									    	document.getElementById("fname").style.backgroundColor = "#efefef";
									    	
									 		x=document.getElementById("poi")
									 		x.disabled = !x.disabled;
									 		document.getElementById("poi").style.backgroundColor = "#efefef";
									 		
									 		x=document.getElementById("mname")
									 		x.disabled = !x.disabled;
									 		document.getElementById("mname").style.backgroundColor = "#efefef";
									 		
									 		x=document.getElementById("mpoi")
									 		x.disabled = !x.disabled;
									 		document.getElementById("mpoi").style.backgroundColor = "#efefef";
									 		
									 		x=document.getElementById("bname")
									 		x.disabled = !x.disabled;
									 		document.getElementById("bname").style.backgroundColor = "#efefef";
									 		
									 		x=document.getElementById("bpoi")
									 		x.disabled = !x.disabled;
									 		document.getElementById("bpoi").style.backgroundColor = "#efefef";
									 		
									 		x=document.getElementById("sname")
									 		x.disabled = !x.disabled;
									 		document.getElementById("sname").style.backgroundColor = "#efefef";
									 		
									 		x=document.getElementById("spoi")
									 		x.disabled = !x.disabled;
									 		document.getElementById("spoi").style.backgroundColor = "#efefef";
									 		
									 		x=document.getElementById("hinfo")
									 		x.disabled = !x.disabled;
									 		document.getElementById("hinfo").style.backgroundColor = "#efefef";
									 		
									 		x=document.getElementById("foodpreferred")
									 		x.disabled = !x.disabled;
									 		document.getElementById("foodpreferred").style.backgroundColor = "#efefef";
												
									 		document.getElementById("familyHistorybuttonId").style = "width:100px;display:block;";
									    	 
							     },  
			     });  		    						
		
		}  /* End of If */
		
	}
	
	 
}

</script>


<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
 
 
 <script>
 
 function changeVisibilitySetting()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					   window.location = "indVisibilitySetting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 	 
 }
 
 /* Main Header Actions  */
 
 function gotoProfile()
 {
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoMyDocument()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }
 
 function gotoSharingManager()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }
 
 function gotoStore()
 {	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					   window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoAlerts()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
 }
 
 function gotoPlans()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
 }
 
 function gotoSetings()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }

 
 function closeRestofAll(profilePart)
 {
	 $("#showRegDetails").hide();
     $("#editregistration").show();
     
     $("#showBasicHistory").hide();
     $("#savebasic").show();
     
     $("#showfamilydetail").hide();
     $("#editfamilydetail").show();
     
     $("#showSocialHistory").hide();
     $("#editsocialdetail").show();		 
 }

 function viwProfilePhoto(photoName)
 {
	 
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						 document.getElementById('profilePhotoPoplight').style.display='block';
					     document.getElementById('profilePhotoPopfade').style.display='block';
	
						 $.ajax({
								type : "post",
								url : "previewProfilePhoto.html",
								data : "photoName="+photoName,
								success : function(response) {
									
									 document.getElementById('waitlight').style.display='none';
							         document.getElementById('waitfade').style.display='none';
	
							    	 document.getElementById('profilePhotoPoplight').style.display='block';
							    	 document.getElementById('profilePhotoPopfade').style.display='block';
							    	
							    	 $("#profilePhotoPopContent").html(response);
									
								}
							}); 
					}
				else
				{
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 
	 
	
 }
 
 
 function hideAddvertise()
 {
     $("#ProfileHeadingDiv").hide();	 
 }
 
 function signOutviaFun(url)
 {
	 window.location=url;
 }
 
 
 function hidesearchDiv()
 {
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
 }
 
 function showsearchDiv()
 {
	 $('#searchicon').show();
	 $('#closediv').show();	 
 }
 
function hideAddvertise()
 {
	//$('#ProfileHeadingDiv').fadeOut(90000);
 }


function profilePhotoChange()
{
	document.getElementById("profilePhotoChangelight").style.display = "block";
	document.getElementById("profilePhotoChangefade").style.display = "block";
}

function profilePhotoChangefadeout()
{
	document.getElementById("selectFile").style.color ="#00b6f5";			 
	$("#selectFile").html(""); 	 
	$("#selectFileInfo").html(""); 	
	document.getElementById("file_browse").value = "";
	
	document.getElementById("profilePhotoChangelight").style.display = "none";
	document.getElementById("profilePhotoChangefade").style.display = "none";
}

 </script>
 
 
</head>

<body onload="getAboutUs(),searchedProfileDetails(),doOnLoad(),hideAddvertise()" onclick="hidesearchDiv()">

<%
  response.setHeader("Cache-Control","no-cache");
  response.setHeader("Cache-Control","no-store");
  response.setHeader("Pragma","no-cache");
  response.setDateHeader ("Expires", 0);

  if((String)session.getAttribute("Ind_id")==null)
  {
	  response.sendRedirect("/individual.html");
  }
      
  %> 
  
  
<%
	String months[] = {"January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December"};
%>


<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>



	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofilefocus"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocuments"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
		
		<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton">
                <span></span>
                <em id="loginNameId" >${regHisname.first_name}</em></a>
                 <div style="clear:both"></div>
            
            
            
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
         
            <span class="topprofilestyle" style="display: none;"></span>
            
						
						<div id="searchicon" style="display:none;">
																	
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
							
								
						</div>
            
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="#" onclick="signOutviaFun('signOut.html')"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
 
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
            
        
			
					<div id="leftsideBasicInfoDiv" class="leftside">
					
					<div id="textBox">
					<c:if test="${!empty profilePicInfo.profile_picture_name}">
	 
						      <%
	 						 		String filename1 = (String) session.getAttribute("profilepic");
	 						 		String user0 = (String) session.getAttribute("Ind_id");
	 						 %>   
						 <div class="user_photoupload view view-first" id="user_photoupload">
						 
					     <c:set var="profilephotoName"  value="${profilePicInfo.profile_picture_name}"/>
					     <% String filename = (String) pageContext.getAttribute("profilephotoName");%>
					     
					          <span><%--  <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+filename+"&docCategory=Profile_Photo"%>" /></img> --%>
					          
					                  <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+filename+"&docCategory=Profile_Photo"%>" /></img>
					          </span>	
						     
						     
						      
						     <div id="profileHover" class="mask">                       
	                               	                                	                                
	                               <form name="editbasicdetail" id="editbasicdetail"  method="POST" enctype="multipart/form-data">
	                                    
	                                   <a href="#"  class="zoomview" onclick="viwProfilePhoto('<%=filename%>')"></a> 
	                                    
		                               <p style="float:left"><a href="#"  class="change" onclick="profilePhotoChange()">Change<!-- <input type="file" name="Passportsize" id="file_browse" onchange="addProfilePhoto()"> --></a></p>
		                               <div id="profilePhotoDiv" style="float: left;margin-left: -5px;margin-top: -69px;width: 100px;display:none;"><a  class="info" style="height: 16px;margin-left: 0;margin-right: -7px;margin-top: 11px;width: 72px;" onclick="uploadbasicdetail1()">Save</a></div>		                               
		                               <p style="float:right;margin-top:-68px; width: 60px;"><a href="#" class="info" onclick="openOffersDialog();profilePicHistory();">History</a>
		                               </p>
		                               
	                              </form>    
	                                                        														
							 </div>
							 
							 
							            
		
							 
						 </div>
						   
						   
		
						 <div class="user_photo">
						 <p><strong id="nameStrngId"><c:out value="${regHisname.first_name}"></c:out></strong> <br>
						 <span>
						 <c:if test="${!empty basicdetails.present_address}">
						 <c:set var="presentplace"  value="${basicdetails.present_address}"/>
									<%
										String myVariable = (String) pageContext
														.getAttribute("presentplace");
												String[] address = myVariable.split("##");																								
									%>
						  <%=address[5]%>
						 </c:if>
						 </span>			 
						 </p>
						 </div>
				
				
				
				
				</c:if>	
				
					<div id="overlay" ></div>
					
					<div id="boxpopup" class="box">
						
						<a onclick="closeOffersDialog('boxpopup');" class="boxclose"></a>
						 <div class="popheaderstyle"><h1 align="left">Previous Profile Pictures</h1></div>
						<div id="content">
						    
						</div>
					</div>
					
                
                <div class="user_display">
                <div id="closepic" style="display: block;">
                
                
                <c:if test="${empty profilePicInfo.profile_picture_name}">
                
                
                <div class="user_photoupload view view-first" id="user_photoupload">
	 
				<span id="userphoto"><img src="resources/kyc_images/Koala.jpg" width="196" height="222" /></span>
				<div id="removeaddphotoDiv" style="display: block;"><a href="#" onclick="profilePhotoChange()"><div id="removeaddphoto" style="display: block;">Add Photo</div></a></div>
					 
					  <form name="editbasicdetail" id="editbasicdetail"  method="POST" enctype="multipart/form-data"> 
					  
	                            <div id="closepic" style="display: block;">          
	     
						                <div id="openbrowse" style="display: block;">
					  
					                        <!-- <div id="file_browse_wrapper" style="display: block;">				
					                       
					                             <input  type="button" id="file_browse" name="Passportsize" onchange="showSave()"/>
					                    
					                        </div> -->
					                 
						                    <div id="saveimg_button_wrapper" style="display: none;">
												
						                         <input type="button" id="saveimg_button" onclick="uploadbasicdetail1();" value="Save">
						                
						                    </div>	
						                    
					                   </div>
					                			                                 
			                    </div>
			                    
		              </form>
		              
			   </div> 
						   
			 <div class="user_photo">
					<p><strong id="nameStrngId"><c:out value="${regHisname.first_name}"></c:out></strong> <br>
					<span>
					<c:if test="${!empty basicdetails.present_address}">
					<c:set var="presentplace"  value="${basicdetails.present_address}"/>
									<%
										String str = "";
												String myVariable = (String)pageContext
														.getAttribute("presentplace");
												String[] ary = myVariable.split("##");
												
									%>
						 <%=ary[5]%>
					</c:if>
					</span>
					</p>
			 </div>
                              	
		   </c:if>		          	 					
	      </div>
	      </div>		
	      
	         		     	         		      	         		                  								 				 																			
							<div class="myaccounts">
							<h2>
							<span>
							<img src="resources/kyc_images/contact_info.png" width="25" height="26" />
							Contact Info</span>
							</h2>
							
							<div class="account_titel"><img src="resources/images/info_mainid_icons.png" /><span>E-mail address</span></div>
							<ul>
							<li>
							
							<table>
							<tbody>
							<tr>
							<td><span id="emailid"><c:out value="${regHisname.email_id}"></c:out></span></td>
							</tr>
							</tbody>
							</table>
							       
							</li>
							</ul>
							<div class="mobileaccount_titel"><img src="resources/images/info_mobile_icons.png" /><span>Mobile Number</span></div>
							<ul>
							<li>
							<table>
							<tbody>
							<tr>
							<td><span id="mobileno"><c:out value="${regHisname.mobile_no}"></c:out></span></td>
							</tr>
							</tbody>
							</table>
							</li>
							</ul>
							
							<div class="present_addressaccount_titel"><img src="resources/images/info_presentaddress_icons.png" /><span>Present Address</span></div>
							<ul>
							<li>
							<div id="presentaddressget" >
								<span id="PresentAddID" style="color:#2d2d2d;">
								<c:if test="${!empty basicdetails.present_address}">
								
								<%-- <c:out  value="${basicdetails.present_address}" ></c:out> --%>
								
								<c:set var="present_address"  value="${basicdetails.present_address}" ></c:set>
								
								<%
																	String present_address = (String) pageContext
																				.getAttribute("present_address");
																		String pre_addressAry[] = present_address.split("##");
																		
																		String presentAddress = present_address.replaceAll("##", ",");
																%>
																
									<table class="textwidth">
											
											<tr><td>
											
													<%=pre_addressAry[0]%>,<%=pre_addressAry[3]%>, 
													<%=pre_addressAry[2]%>,<%=pre_addressAry[1]%>,
													<%=pre_addressAry[1]%>,<%=pre_addressAry[4]%>,
													<%=pre_addressAry[5]%>,<%=pre_addressAry[6]%>,
													<%=pre_addressAry[7]%>,
													<%=pre_addressAry[8]%>
													
											</td></tr>
											
									</table>
								
								</c:if>
								</span>														
							</div>
							</li>
							</ul>
							</div>
							
							<c:if test="${!empty basicdetails.languages}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/languages_known_icon.png" width="25" height="26" />Languages Known</span>
							</h2>
							<ul>
							<li>
							<div id="languageDiv"><c:out value="${basicdetails.languages}"></c:out></div>
							</li>
							</ul>
							</div>
							</c:if>
							
							<c:if test="${empty basicdetails.languages}">
								<div class="myaccounts">
								<h2>
								<span><img src="resources/kyc_images/languages_known_icon.png" width="25" height="26" />Languages Known</span>
								</h2>
								<ul>
								<li>
								<div id="languageDiv"></div>
								</li>
								</ul>
								</div>
							</c:if>
														
							<c:if test="${!empty socialaccvalue}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/social_account_icon.png" width="25" height="26" />Social Account </span>
							</h2>
							<ul>
							<li>
							<img src="resources/kyc_images/face_book_icon.png" width="17" height="17" />							
								
								<div id="websitelinkname">
								<c:if test="${!empty socialaccvalue.socialwebsitelink}">
									<a href="${socialaccvalue.socialwebsitelink}" target="_blank">
									facebook connected
									 </a>
								</c:if>	
								<c:if test="${empty socialaccvalue.socialwebsitelink}">									
									facebook not connected									
								</c:if>																						    							     
							    </div>
							    
							</li>
							<li>
							<img src="resources/kyc_images/twitter_icon.png" width="17" height="17" />
							     
							     <div id="twitterlinkname">
								     <c:if test="${!empty socialaccvalue.sociallinkedlnlink}">
								      <a href="${socialaccvalue.sociallinkedlnlink}" target="_blank">
								      <c:out value="twitter connected"></c:out>
								      </a>
								     </c:if>
								     <c:if test="${empty socialaccvalue.sociallinkedlnlink}">								      
								      <c:out value=" twitter not connected"></c:out>								      
								     </c:if>
							      </div>
							      
							</li>
							<li>
							<img src="resources/kyc_images/linkedin_icon.png" width="17" height="17" />
							
							    <div id="linkedlnlinkname">
							    	<c:if test="${!empty socialaccvalue.socialtwitterlink}">
							    	  <a href="${socialaccvalue.socialtwitterlink}" target="_blank">
							           <c:out value="linkedin connected"></c:out>
							          </a>
							    	</c:if>
							       <c:if test="${empty socialaccvalue.socialtwitterlink}">							    	  
							           <c:out value="linkedin not connected"></c:out>							          
							    	</c:if>
							     </div>
							     
							</li>
							
							</ul>
							</div>
							</c:if>												
							
			
			                <c:if test="${empty socialaccvalue}">
							<div class="myaccounts">
							<h2>
							<span><img src="resources/kyc_images/social_account_icon.png" width="25" height="26" />Social Account </span>
							</h2>
							<ul>
							<li>
							<img src="resources/kyc_images/face_book_icon.png" width="17" height="17" /><div id="websitelinkname">not connected !</div>
							</li>
							<li>
							<img src="resources/kyc_images/twitter_icon.png" width="17" height="17" /><div id="twitterlinkname">not connected !</div>
							</li>
							<li>
							<img src="resources/kyc_images/linkedin_icon.png" width="17" height="17" /><div id="linkedlnlinkname">not connected !</div>
							</li>
							
							</ul>
							</div>
							</c:if>	
							
			
					
					</div>
					
					 </div>
					
					<div class="rightside">
										
						
						<!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

									<div id="ProfileHeadingDiv" style="display:block;">
									
								<marquee behavior="scroll" scrollamount="2" direction="left">
									   <h1 class="rightsideheader">
									       
									        <%=indAdvertise%>
									        <!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
									   </h1>
									   </marquee>
							        </div>	
							        		
					   <!-- Advertisment Div for Entire Page  -->
					   
					   
	          <div id="suggestions1" style="display: block;  margin:0px auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
						
							<h2>
							 <b></b>
								<span>Hey I am <c:out value="${regname.firstname}"></c:out></span>
								   <c:set var="member_date"  value="${regname.cr_date}"/>
		                              <%
		                              	Date member_date = (Date) pageContext.getAttribute("member_date");
		                              	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		                              	String reportDate = df.format(member_date);
		                              	String dateAry1[] = reportDate.split(" ");
		                              	String dateAry2[] = dateAry1[0].split("-");
		                              	int month = Integer.parseInt(dateAry2[1]);
		                              	String year = dateAry2[0];
		                              %>							
								    <p>Member since <%=months[month - 1] + " " + year%></p>
							</h2>
						 </div>
					
<div class="module-body">
	
	<div id="viewUsers1" class="aboutus" style=" height: auto !important;margin-bottom: 15px;">
							
					
						<!--  About Us Design   -->
						
						
						<div id="aboutus" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:yellowgreen;color:#fff;display:none;font-size:16px;">Saved Successfully!</div>
											
						<h1 class="rightsideheader">About Me
						<c:if test="${!empty Summarydetails.cr_date}"> 
						
						 <p>Last updated on <c:set var="Aboutdate" value="${Summarydetails.cr_date}" />
						 		<%
						 			Date about_date = (Date) pageContext.getAttribute("Aboutdate");
						 				String reportDate1 = df.format(about_date);
						 				String dateAry3[] = reportDate1.split(" ");
						 				String dateAry4[] = dateAry3[0].split("-");
						 				int abt_month = Integer.parseInt(dateAry4[1]);
						 				String abt_year = dateAry4[0];
						 		%>				 
						   
						        <%=months[abt_month - 1] + " " + abt_year%></p> 
						 
						 </c:if>
						
						<button onClick="toggleArea1();"><div id="htmlbutton" class="editbutton">Edit</div></button>
						</h1>
						
						<div id="texteditediv" style="clear: both;">
						
							<div id="myArea1" style=" height: 200px;">   </div> 
							
							<div class="seemorediv" id="seemore" style="display:none;"><a href="#" onclick="aboutUsSeeMore()">See More..</a></div>
			  
				              <div id="textareaId" style="display:none;">
				              
					              <form id="textareaform" name="textareaform" enctype="multipart/form-data">
						                
						                <textarea id="indaboutUs" name="aboutusTextArea" style="width:100px;" maxlength="2500">
					
					                         			                         
			                            </textarea>
			                            
	                              </form>  
	                              
                             </div> 
                            
							<div id="flash" style="display: inline;width: 817px;">   </div>
							
						</div>
						
						
						<!--  End of About Us Design   -->
						
						
						
					<div style="clear: both;">
						 
						<h1 class="rightsideheader">Current Company Details	
						<c:if test="${!empty empdate}">
						<p>Last updated on <c:set var="empDate" value="${empdate}"></c:set>
						
						<%
						    String emp_date = (String) pageContext.getAttribute("empDate");
							String empDate = emp_date;
							String empdateAry1[] = empDate.split("%");
							String empdateAry2[] = empdateAry1[0].split("-");
							int emp_month = Integer.parseInt(empdateAry2[1]);
							String emp_year = empdateAry2[0];
												%>	
						<%=months[emp_month - 1] + " " + emp_year%>
						</p>
						</c:if>
						<a href="employeeDetails.html"  id="Hi-B-4"><button ><div id="htmlbutton" class="editbutton">Edit</div></button></a>
						</h1>
											
						<div class="companydetails">
						
						<%String org_Name = ""; 
						String jobDesigNation = "";
						String job_profile = "";
						String jobFrom = "";
						String jobTo = "";
						%>
						<c:if test="${!empty employeedetail}">
									
									<c:forEach items="${employeedetail}" var="det">
										
												<c:set var="employee"  value="${det.docs_data}"/>
									 <%
									 	String myVariable = (String) pageContext
									 					.getAttribute("employee");
	
									 			String[] ary1 = myVariable.split("&&");
	
									 			org_Name = ary1[0];
									 			jobDesigNation= ary1[1];
									 			job_profile = ary1[2];
									 			jobFrom = ary1[3];
									 			jobTo = ary1[4];
									 %>										
										
									</c:forEach>


											<div style="width: 100%;">
												<ul>
													<li>
														<h1 class="currentcompanydetails">
															<span></span> <b><%=org_Name%></b>  <p style="float:right;"><%=jobFrom%> - <%=jobTo%></p>  
														</h1>
														<div >
														<table class="profilescroll" align="left"
															style="display: inline; background: #eee; margin-top:0px;border-radius: 0 0 5px 5px;margin-top:-9px; padding: 10px; width:808px  !important;-webkit-logical-width:788px !important;"
															cellspacing="0">

																<tbody>
															<tr style="height: 18px;">
																<th style="width: 150px;text-align: left; ">Job Designation</th>
																<td style="width: 10px;text-align: left;">:</td>
																<td><%=jobDesigNation%></td>
															</tr>
															
															<tr style="height: 18px;">
																<th style="width: 150px; text-align: left;vertical-align: top;">Job Summary</th>
																<td style="vertical-align: top;width: 10px;text-align: left;">:</td>
																<td style="width: 808px; text-align: left;vertical-align: top;text-align: justify;"><%=job_profile%></td>
															</tr>
															</tbody>
														
														</table>
														</div>
													</li>
												</ul>
											</div>







										</c:if>
						
						
						<c:if test="${empty employeedetail}">
						<span>Click on Edit to upload the company details.</span>
						</c:if>
						</div>
						</div> 
						
						<div style="clear: both;">
						 
						<h1 class="rightsideheader">Educational Details	
						<c:if test="${!empty postOn}">					
						<p>Last updated on <c:set var="Edudate" value="${postOn}" />
						<%
							String edu_date = (String) pageContext.getAttribute("Edudate");
								String docDate = edu_date;
								String docdateAry1[] = docDate.split(" ");
								String docdateAry2[] = docdateAry1[0].split("-");
								int doc_month = Integer.parseInt(docdateAry2[1]);
								String doc_year = docdateAry2[0];
						%>	
						<%=months[doc_month - 1] + " " + doc_year%></p> 
						
						
						</c:if>
						 <a href="getacademicdetails.html"  id="Hi-B-2"><button ><div id="htmlbutton" class="editbutton">Edit</div></button></a>
						</h1>
						
						<div class="companydetails">
						<c:if test="${!empty documentdetail}">
						  
						  <c:set var="comp" value="${accordNames}"/>
						         <c:set var="documentdetail" value="${documentdetail}"/>
						
						<%
							String documentdetaildata = (String) pageContext.getAttribute("documentdetail");
  
						       
								if (!documentdetaildata.equals("null/")) {									
								       
									String data[] = documentdetaildata.split("/");
																		
									for (int i = 0; i < data.length; i++) {
										
											if(!data[i].equals("null"))
											{
												
														    String datasplit[] = data[i].split("-");
															String heading =  datasplit[0];
															String academicData[] =  datasplit[1].split(",");
										    %>
										
											            	 <div style="width:100%;">
											            	 
											            	 <ul>
											            	 <li>
											            	 <h1>
											            	 <span></span>
											            	 <b><%=heading%></b></h1>
											            	 <div >
											            	 <table class="profilescroll" align="left" style="   margin-top: -9px; display: inline;background: #eee;border-radius: 0 0 5px 5px;
																									  padding: 10px;
																									    width: 400px !important;-webkit-logical-width: 380px !important;" cellspacing="0" >
											            	<tbody>
											            	
											            	 <%for(int d=0;d<academicData.length;d++) {
											            		 
													            	     String contentData[] = academicData[d].split(":");
													            	     %>
															            	   <tr style="height: 18px;">
															            	   <th style="width:150px;text-align:left;"><%=contentData[0]%></th><td>:</td><td><%=contentData[1]%></td>
															            	   </tr>
													           <%} %>
													            	 </tbody>
											            	 </table>
											            	 </div>
											            	 </li>
											            	 </ul>
											            	 </div>
											            	 
										
										<%}%>
											
						        	      <%}
						        	 		} else {
						        	 %>
						            <span>Click on Edit to upload the academic details.</span>
																																	            
						             <%
																																	            						             	}																																	            						             %>
						</c:if>
						</div>
						<c:if test="${empty documentdetail}">
						<span>Click on Edit to upload the academic details.</span>
						</c:if>
						</div> 
						
					<!-- Required Division for viewUser1 not go beyond  -->
					
					<div style="clear: both;margin-top:0px;">
						
						</div>
					
					<!-- End of Required Division for viewUser1 not go beyond  -->	
						
			</div>
		</div>	
	</div>	<!-- End of -->	

	<div id="center-body-div" style="display:block   margin: 0 auto; width: 100%;">
<div  id="panel-1" >

 <h3 align="left" onclick="closeRestofAll('Registration')">
 
 
 Registration Details</h3> 
 
       
<div  style="display:block" id="indexinputbutton">
       
       <div id="registration" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:yellowgreen;color:#fff;display:none;font-size:16px;">Registration details saved successfully.</div>
       
        <c:if test="${!empty regHisname}">  
                        
        <div id="reg1Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
        <div id="reg0Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
        <div id="reg3Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
        <div id="reg4Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
        <div id="reg5Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
        
        
        <form id="showRegDetails"></form>
                      
            <form id="editregistration"  name="regform" >
            
             <a id="editregistrationhelp" href="registration_help.html" target="_blank" title="Help For Registration Details" ><p class="askhelp"></p></a> 
             
				       <div class="displayregidetails">
												
															<ul>
																<li class="form-row" style=" float: left;height: auto;width: 100%;">
																																					
																<input class="firstname" style="width: 102px;float: left;margin-left: 0px;" type="text"
																		value="${regHisname.first_name}" title="First Name" name="firstname"
																		id="reg1" disabled="true"
																		
																		onchange="myFunctionreg(this.id)" maxlength="30"  placeholder="First Name" onkeyup="validateRegDetailsEach()"/>
																		
																		
																<input class="middlename"style="width: 113px;float: left;margin-left: 6px;" type="text"
																		value="${regHisname.middle_name}" name="middlename" placeholder="Middle Name" title="Middle Name"
																		id="reg0" disabled="true"
																		
																		onchange="myFunctionreg(this.id)" maxlength="30"  onkeyup="validateRegDetailsEach()"/>
															    <input class="lasstname"style="width: 114px;float: left;margin-left:6px;" type="text"
																		value="${regHisname.last_name}" name="lastname" id="reg3" placeholder="Lastname" title="Last Name"
																		disabled="true" 
																		onchange="myFunctionreg(this.id)" maxlength="30"  onkeyup="validateRegDetailsEach()"/>
															
																
																</li>
																<li class="form-row" >
															
																<input class="mailidicons" style="clear: both;float: left;position: relative;text-align: left;width: 375px;" type="text"
																		value="${regHisname.email_id}" name="emailid" id="reg4" maxlength="50"  placeholder="Email-id" title="Your Email Id"
																		disabled="true" 
																		onchange="validateRegDetailsEach(),myFunctionreg(this.id)" />
															
																</li>
				
																<li class="form-row">
															
																	
																	<input class="mobileicons"style="clear: both;float: left;position: relative;text-align: left;width: 375px;" type="text" type="text"
																		value="${regHisname.mobile_no}" name="mobileno" id="reg5" placeholder="Mobile No" title="Your Mobile Number"
																		disabled="true" 
																		onchange="myFunctionreg(this.id)" maxlength="10" onkeyup="validateRegDetailsEach()"/>
																
																</li>
																
																<c:set var="genderValue" value="${regHisname.gender}"/>
										
										                        <%String genderValue = (String)pageContext.getAttribute("genderValue");%>
											                     
											                     <li class="form-row" id="genderValueli">
																														         											
																			   <input class="gendericons" disabled="true" style="clear: both;float: left;position: relative;text-align: left;width: 375px;" type="text" type="text"
																				             value="${regHisname.gender}" id="genderId" name="gender"  placeholder="Gender" title="Gender" onchange="myFunctionreg(this.id)"  />
																				 																										
																 </li>
																		  
											                     <%if(genderValue.equals("MALE")) {%>
											                     
													                     								                      
													                      <li class="form-row" id="malelibutton" style="clear: both;">
																														
																					<div class="register-switch">
																				     
																				      <input type="radio" name="sex" value="MALE"  class="register-switch-input" disabled="true" >
																				      <label for="sex_m" class="register-switch-malelabel">&nbsp;</label>
																				      
																				    </div>
																		   
																			</li>
																	
											                     <%}else { %>							                     
														                      										                    
														                      <li class="form-row" id="femalelibutton" style="clear:both;">
																															
																						<div class="register-switch">
																					     
																					      <input type="radio" name="sex" value="FEMALE"  class="register-switch-input" disabled="true" >
																					      <label for="sex_f" class="register-switch-femalelabel">&nbsp;</label>
																					      
																					    </div>
																			   
																			 </li>
											                     
											                     <%} %>
											                     
											                                <li class="form-row" id="hiddenmalelibutton" style="clear: both;display:none;">
																														
																					<div class="register-switch">
																				     
																				      <input type="radio" name="sex" value="MALE"  class="register-switch-input" disabled="true" >
																				      <label for="sex_m" class="register-switch-malelabel">&nbsp;</label>
																				      
																				    </div>
																		   
																			</li>
																			
														                     
														                      <li class="form-row" id="hiddenfemalelibutton" style="clear:both;display:none;">
																															
																						<div class="register-switch">
																					     
																					      <input type="radio" name="sex" value="FEMALE"  class="register-switch-input" disabled="true" >
																					      <label for="sex_f" class="register-switch-femalelabel">&nbsp;</label>
																					      
																					    </div>
																			   
																			 </li>
																	
																	
																	<li class="form-row" id="genderli" style="display:none;">
																													         											
																		   <input class="gendericons" disabled="true" style="clear: both;float: left;position: relative;text-align: left;width: 375px;" type="text" type="text"
																			             value="${regHisname.gender}" id="editgenderId" name="gender"  placeholder="Gender" title="Gender" onchange="myFunctionreg(this.id)"  />
																			 																										
																	</li>
																
																    <li class="form-row" id="genderlibutton" style="clear: both;display:none;">
																												
																			<div class="register-switch">
																		     
																		      <input type="radio" name="sex" value="MALE" id="sex_m" class="register-switch-input" disabled="false" onclick="genderValue()">
																		      <label for="sex_m" class="register-switch-malelabel">&nbsp;</label>
																		      <input type="radio" name="sex" value="FEMALE" id="sex_f" class="register-switch-input" disabled="false" onclick="genderValue()">
																		      <label for="sex_f" class="register-switch-femalelabel">&nbsp;</label>
																		      
																		    </div>
																   
																	</li>
																
																
																<li id="indexinputbutton"class="form-row" style="clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 445px;" >
																<input type="button" value="Edit" id="editreg" class="editebutton"  onClick="saveRegDetails()"/> 
																	
																<input type="button" value="History" style="width:100px" onclick="reghistory()" class="historybutton" />
																
																
																
																</li>
																
															</ul>										
				        </div>
				</form> 
        </c:if> 
  
  						<div class="notemodel">
  						    <b>Note:</b><br>
  						    Please click <a href="#" onclick="changeVisibilitySetting()">here</a> to change your profile view settings. 
  						     
  						</div>
     
 </div>
        
  <h3 align="left" onclick="closeRestofAll('Basic')">
 
  Basic Details</h3>
      
       <div id="indexinputbutton">
        <a id="editregistrationhelp" href="basic_help.html" target="_blank" title="Help For Basic Details" ><p class="askhelp"> </p></a>
       <div id="BasicSuccess" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:yellowgreen;color:#fff;display:none;font-size:16px;">Basic details saved successfully.</div>
       
       <form id="showBasicHistory" ></form>
       
       <div id="streetDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="b4Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="b5Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="b3Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="b2Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="b6Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="b7Div" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       
       <div id="zoneDiv" class="error" style="display:none;margin-left: 240px;margin-top: 10px;"></div>
       <div id="mandalDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="districtDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="cityDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="countryDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="stateDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="pincodeDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>              
       <div id="districtDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       
       <div id="percityDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="perstateDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="perdistrictDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="perzoneDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="permandalDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="percountryDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="perpincodeDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       <div id="perstateDiv" class="error" style="display:none;margin-left: 280px;margin-top: 10px;"></div>
       
       
       
         <form name="savebasic" id="savebasic"  enctype="multipart/form-data">
   
        <c:if test="${empty basicdetails}">  
   
  
   		<div id ="familydetailalign">
   		
   		
		 <ul>
		 <li>
		
		 <input class="mailidicons" id="street" maxlength="50" style="width:160px;float: left;margin-right: 6px;"  type="text" placeholder="Alternative Email" title="Enter your Alternative Email Id" value="" name="Emailalternative" onchange="validateBasicEach(),myFunctionbasic(this.id)" />
		 
		 <input type="text" readonly class="dateofbirthicons some_class" style="width:160px;float: left;margin-right: 6px;" placeholder="DOB" value="" name="DOB" title="Yout Date Of Birth" id="calendar" onchange="myFunctionbasic(this.id)"/>
		 
		 <!-- <input class="dateofbirthicons" readonly style="width:160px;float: left;margin-right: 6px;" placeholder="DOB"  type="text"  name="DOB"  title="Yout Date Of Birth" id="calendar"  onchange="myFunctionbasic(this.id)"/>
		  -->
		 </li>
		  <li>
		  <input class="telofficeicons" maxlength="15"  style="width:160px;float: left;margin-right: 6px;" placeholder="Tel-Office" title="Office Telephone number" type="text" name="Teloff" id="b4" onchange="myFunctionbasic(this.id)" onkeyup='validateBasicEach()'/>
		  <input class="telresidentialicons" maxlength="15"  style="width:160px;float: left;margin-right: 6px;" placeholder="Tel-Residential"   title="Residential Telephone Number" type="text"  name="Telres" id="b5" onchange="myFunctionbasic(this.id)" onkeyup='validateBasicEach()'/>
		  </li>
		   <li>
		   <input class="nationalityicons"  style="width:160px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Nationality" title="Nationality"  type="text"  name="Nationality" id="b3" value="Indian" disabled="true" onchange="myFunctionbasic(this.id)" onkeyup='validateBasicEach()'/>
		   <input  class="matrialstatusicons"  style="width:160px;float: left;margin-right: 6px;" maxlength="20"  placeholder="Marital Status" title="Marital Status" type="text"  name="Matrialstatus" id="b2" onchange="myFunctionbasic(this.id)" onkeyup='validateBasicEach()'/>
		   </li>
	    <li>
	    <input class="languagesicons" maxlength="50"  style="width:160px;float: left;margin-right: 6px;" placeholder="Languages Known" title="Languages you knows"  type="text"  name="Languages" id="b6" onchange="myFunctionbasic(this.id)" onkeyup='validateBasicEach()'/>
	    <input class="hobbiesicons" maxlength="50"  style="width:160px;float: left;margin-right: 6px;" placeholder="Hobbies" title="Your Hobbies" type="text"  name="Hobbies" id="b7" onchange="myFunctionbasic(this.id)" />
	    </li>
		     <li></li>
		 </ul>
		 </div>
		 
		 
			<div id="presentaddressdetails">
			<h1>
			<img src="resources/kyc_images/presentaddress_iconsone.png" />
			<span>Present Address &nbsp;&nbsp;</span>
			</h1>
			
			<ul>
			<li>
				<input id="door" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="Door" title="Door Number"  type="text"    onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
				<input id="mandal" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="Street" title="Street Name or Number" type="text"    onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>				
				<input id="zone" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="Area" title="Area where you stay " type="text"   onchange='ReviewDefects1(length);' />
			</li>
			
			<li>
				<input id="landmark" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="landmark" title="Land Mark" type="text"   onchange='ReviewDefects1(length);' />
				<input id="pincode" value=""  maxlength="6" name="pincode" style="width:130px;float: left;margin-right: 6px;" placeholder="Pincode" title="Pincode" type="text"   onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
				<input id="city" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="City" title="City Name" type="text"   onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>				
			</li>
			
			<li>
				<input id="district" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="District" title="District Name" type="text" onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
				<input id="state" value="" maxlength="30"  name="state" style="width:130px;float: left;margin-right: 6px;" placeholder="State" title="State Name" type="text"  onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/> 				
				<input id="country" value="India" maxlength="30"  disabled="true" name="country" style="width:130px;float: left;margin-right: 6px;" placeholder="Country" title="Country Name" type="text"   onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
			</li>
		
			</ul>
			
			
			</div>
			
			<span class="spanright"><input type="checkbox" style="width:9px;" id="checkStatus" onclick="copyPresent()"/>Same as Present Address.</span>
			
			<div id="permanentaddressdetails">
									
			<h1>
			<img src="resources/kyc_images/permanentaddress_iconsone.png" />
			<span>Permanent Address</span> 
			</h1>
			
			<ul>
			<li>
				<input id="perdoor" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="Door" title="Door Number" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
				<input id="permandal" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="Street" title="Street Name or Number" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>				
				<input id="perzone" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="Area" title="Area where you stay " type="text"   onchange='ReviewDefectsper(length);' />
			</li>
			
			<li>
				<input id="perlandmark" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="landmark" title="Near Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
				<input id="perpincode" value="" maxlength="6" name="pincode" style="width:130px;float: left;margin-right: 6px;" placeholder="Pincode" title="Pincode" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
				<input id="percity" value="" maxlength="30"  style="width:130px;float: left;margin-right: 6px;" placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>				
			</li>
			
			<li>
				<input id="perdistrict" value="" maxlength="30" style="width:130px;float: left;margin-right: 6px;" placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
				<input id="perstate" value="" maxlength="30" name="state" style="width:130px;float: left;margin-right: 6px;" placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/> 				
				<input id="percountry"  maxlength="30"  name="country" style="width:130px;float: left;margin-right: 6px;" placeholder="Country" title="Country Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			</li>
			
			<li style=" clear: both;float: left; margin-top: 5px;position: relative;text-align: left;width: 440px;">
				
				<input style="width:130px;"   type="hidden"  id="savepresentaddress" name="Present_address" />
				<input style="width:130px;"   type="hidden"  id="permanentaddress" name="Permanent_address" />
				<input type="button" class="editebutton" id="basicbuttonId" value="Save" onclick="validatesavebasic(this)" />
			
				<input style="width:100px;display:none;" id="basichistorybuttonId" type="button" value="History" class="historybutton" onclick="basicHistory()" />
			
			</li>
			</ul>
			
			
			</div>
	  
       </c:if>
   
   
    
   <c:if test="${!empty basicdetails}">  
   
       
   		<div id ="familydetailalign">
   		
		 <ul>
		 <li>
			 <input class="mailidicons" maxlength="50" placeholder="Alternative Email" title="Alternative Email Id" id="street" style="width:156px;float: left;margin-right: 6px;"  type="text" value="${basicdetails.emailalternative}" name="Emailalternative" onchange="validateBasicEach(),myFunctionbasic(this.id)" disabled="true" />
		 	 <input type="text" readonly class="dateofbirthicons some_class" style="width:156px;float: left;margin-right: 6px;" placeholder="DOB" value="${basicdetails.DOB}" name="DOB" title="Yout Date Of Birth" id="calendar" onchange="myFunctionbasic(this.id)" disabled="true" />
		 
		 	 <%-- <input class="dateofbirthicons" readonly placeholder="Date of Birth" title="Date of Birth" style="width:156px;float: left;margin-right: 6px;" value="${basicdetails.DOB}"  type="text"  name="DOB"  id="calendar" onchange="myFunctionbasic(this.id)" disabled="true" /> --%>
		 </li>
		 
		  <li>
		  <input  class="telofficeicons" maxlength="30"  placeholder="Tel-Office" title="Office Telephone Number" style="width:156px;float: left;margin-right: 6px;" value="${basicdetails.teloff}" name="Teloff" id="b4" onchange="myFunctionbasic(this.id)" disabled="true" onkeyup='validateBasicEach()'/>
		  <input class="telresidentialicons" maxlength="30"  placeholder="Tel-Residence" title="Residence Telephone Number" style="width:156px;float: left;margin-right: 6px;" value="${basicdetails.telres}" name="Telres" id="b5" onchange="myFunctionbasic(this.id)" disabled="true" onkeyup='validateBasicEach()'/>
		  </li>
		   <li>
		   <input  class="nationalityicons" maxlength="30"  placeholder="Nationality" title="Nationality" style="width:156px;float: left;margin-right: 6px;" value="${basicdetails.nationality}" name="Nationality" id="b3" value="Indian" disabled="true" onchange="myFunctionbasic(this.id)" disabled="true" onkeyup='validateBasicEach()'/>
		   <input  class="matrialstatusicons" maxlength="30"  placeholder="Marital Status" title="Marital Status" style="width:156px;float: left;margin-right: 6px;" value="${basicdetails.matrialstatus}" name="Matrialstatus" id="b2" onchange="myFunctionbasic(this.id)"  disabled="true" onkeyup='validateBasicEach()'/>
		   </li>
		    <li>
		    <input class="languagesicons" maxlength="50"  placeholder="Languages Known" title="Languages you knows" style="width:156px;float: left;margin-right: 6px;" value="${basicdetails.languages}" name="Languages" id="b6" onchange="myFunctionbasic(this.id)" disabled="true" onkeyup='validateBasicEach()'/>
		    <input class="hobbiesicons" maxlength="50"  placeholder="Hobbies" title="Your Hobbies" style="width:156px;float: left;margin-right: 6px;" value="${basicdetails.hobbies}" name="Hobbies" id="b7" onchange="myFunctionbasic(this.id)" disabled="true" onkeyup='validateBasicEach()'/>
		    </li>
		     <li></li>
		 </ul>
		 </div>
			
			<div id="presentaddressdetails">
			<h1>
			<img src="resources/kyc_images/presentaddress_iconsone.png" />
			 <span>Present Address &nbsp;&nbsp;</span>
			</h1>
			<ul>
			
			<li>
			
			<input id="door" disabled="true" style="width:125px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Door" title="Door Number" <c:if test="${!empty basicdetails.present_address}">
        	<c:set var="divide"  value="${basicdetails.present_address}"/>
		
			 <%
	            String myVariable = (String)pageContext.getAttribute("divide");
	            
	            String[] ary=myVariable.split("##");
	            String str = ary[0];       
	         %>
         
         	value="<%=str%>" </c:if> type="text"    onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
			
			<input id="mandal" disabled="true" maxlength="30"  style="width:125px;float: left;margin-right: 6px;" placeholder="Street" title="Street Name or Number" <c:if test="${!empty basicdetails.present_address}">
        <c:set var="divide"  value="${basicdetails.present_address}"/>
		 <%
         String myVariable = (String)pageContext.getAttribute("divide");
           System.out.println("myVariable"+myVariable);
           String[] ary=myVariable.split("##");
           String str = ary[3];
           
         %>
        value="<%=str%>"</c:if> type="text"    onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
										
			<input id="zone" disabled="true" maxlength="30"  style="width:125px;float: left;margin-right: 6px;" placeholder="Area" title="Area Name" <c:if test="${!empty basicdetails.present_address}">
		        <c:set var="divide"  value="${basicdetails.present_address}"/>
				 <%
		         String myVariable = (String)pageContext.getAttribute("divide");
		          
		           String[] ary=myVariable.split("##");
		           String str = ary[2];
		           
		         %>
		         value="<%=str%>" </c:if> type="text"   onchange='ReviewDefects1(length);' />
								
			</li>
			
			<li>
			
			
			<input id="landmark" disabled="true" maxlength="30"  style="width:125px;float: left;margin-right: 6px;" placeholder="LandMark" title="Near Land Mark" <c:if test="${!empty basicdetails.present_address}">
        	<c:set var="divide"  value="${basicdetails.present_address}"/>
				 <%
		         String myVariable = (String)pageContext.getAttribute("divide");
		           
		           String[] ary=myVariable.split("##");
		           String str = ary[1];
		           
		         %>
		        value="<%=str%>" </c:if> type="text"   onchange='ReviewDefects1(length);' />
			
			
			<input id="pincode" disabled="true" name="pincode" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.present_address}">
        <c:set var="divide"  value="${basicdetails.present_address}"/>
		 <%
         String myVariable = (String)pageContext.getAttribute("divide");
           
           String[] ary=myVariable.split("##");
           String str = ary[7];
           //str = str.substring(0,str.length()-1);
           
         %>
        value="<%=str%>" </c:if> placeholder="Pincode" title="Pincode" type="text"  maxlength="6" onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
			
				
			<input id="city" disabled="true" maxlength="30"  style="width:125px;float: left;margin-right: 6px;" placeholder="City" title="City Name" <c:if test="${!empty basicdetails.present_address}">
        <c:set var="divide"  value="${basicdetails.present_address}"/>
		 <%
         String myVariable = (String)pageContext.getAttribute("divide");
           System.out.println("myVariable"+myVariable);
           String[] ary=myVariable.split("##");
           String str = ary[4];
           
         %>
        value="<%=str%>" </c:if> type="text"   onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
						
			</li>
			
			<li>
			
			<input id="district" disabled="true" maxlength="30"  style="width:125px;float: left;margin-right: 6px;" placeholder="District" title="District Name" <c:if test="${!empty basicdetails.present_address}">
        <c:set var="divide"  value="${basicdetails.present_address}"/>
		 <%
         String myVariable = (String)pageContext.getAttribute("divide");         
           String[] ary=myVariable.split("##");
           String str = ary[5];
           
         %>
        value="<%=str%>" </c:if> type="text" onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
			
			<input id="state" disabled="true" maxlength="30"  name="state" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.present_address}">
        <c:set var="divide"  value="${basicdetails.present_address}"/>
		 <%
         String myVariable = (String)pageContext.getAttribute("divide");
           System.out.println("myVariable"+myVariable);
           String[] ary=myVariable.split("##");
           String str = ary[6];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="State" title="State Name"  type="text"  onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/> 
			
			
			<input id="country" disabled="true" maxlength="30" name="country" style="width:125px;float: left;margin-right: 6px;"        
              value="India" placeholder="Country" title="Country Name" type="text"   onchange='ReviewDefects1(length);' onkeyup='validateBasicEach()'/>
			
			</li>
		
			</ul>
						
			</div>
						
			<span class="spanright"><input type="checkbox" style="width:9px;" id="checkStatus" disabled="true" onclick="copyPresent()"/>Same as Present Address.</span>
			
			<div id="permanentaddressdetails">
			<h1>
			<img src="resources/kyc_images/permanentaddress_iconsone.png" />
			<span>Permanent Address</span>
			</h1>
			<ul>
			<li>
			
			<input id="perdoor" disabled="true" maxlength="30" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[0];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Door" title="Door Number" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="permandal" disabled="true" maxlength="30" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
           String[] ary1=myVariable1.split("##");
           String str = ary1[3];
           
         %>
        value="<%=str%>" </c:if> placeholder="Street" title="Street Name" type="text"    onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perzone" disabled="true" maxlength="30" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
           String[] ary1=myVariable1.split("##");
           String str = ary1[2];
           
         %>
        value="<%=str%>" </c:if> placeholder="Area" title="Area Name" type="text"   onchange='ReviewDefectsper(length);' />
			</li>
			
			<li>
			
			<input id="perlandmark" disabled="true" maxlength="30" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}" />
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[1];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Landmark" title="Near By Land Mark" type="text"   onchange='ReviewDefectsper(length);' />
			
			<input id="perpincode" disabled="true" name="pincode" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[7];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Pincode" title="Pincode" type="text" maxlength="6"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			<input id="percity" disabled="true" maxlength="30" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[4];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="City" title="City Name" type="text"   onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
						
			</li>
			
			<li>
			
			<input id="perdistrict" disabled="true" maxlength="30" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           
           String[] ary1=myVariable1.split("##");
           String str = ary1[5];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="District" title="District Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
						
			<input id="perstate" disabled="true" maxlength="30" name="state" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
           System.out.println("myVariable1"+myVariable1);
           String[] ary1=myVariable1.split("##");
           String str = ary1[6];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="State" title="State Name" type="text"  onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			
			
			<input id="percountry" disabled="true" maxlength="30" name="country" style="width:125px;float: left;margin-right: 6px;" <c:if test="${!empty basicdetails.permanent_address}">
        <c:set var="divide1"  value="${basicdetails.permanent_address}"/>
		 <%
         String myVariable1 = (String)pageContext.getAttribute("divide1");
          
          String[] ary1=myVariable1.split("##");
          String str = ary1[8];
           //str = str.substring(0,str.length()-1);
         %>
        value="<%=str%>" </c:if> placeholder="Country" title="Country Name" type="text" onchange='ReviewDefectsper(length);' onkeyup='validateBasicEach()'/>
			</li>
		
			
			<li style="clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 87%;" >			
			<input style="width:120px;"   type="hidden"   class="editebutton" id="savepresentaddress" name="Present_address" />
			<input style="width:120px;"   type="hidden"  class="historybutton" id="permanentaddress" name="Permanent_address" />
						
			<input type="button"  class="editebutton" id="basicbuttonId" value="Edit" onclick="return validatesavebasic(this)"/>
			<input style="width:100px" type="button" value="History" class="historybutton" onclick="basicHistory()" />
			</li>
			</ul>
						
			</div>
	  
       </c:if>
       
       
		
  </form>
  
        <div class="notemodel">
		    <b>Note:</b><br>
		    Please click <a href="#" onclick="changeVisibilitySetting()">here</a> to change your profile view settings. 
		    
		</div>
	    
 </div>
 
 
 
 
  
    <h3 align="left" onclick="closeRestofAll('Family')">
    
    Family Details</h3> 
  <div id="indexinputbutton">
   <a id="editregistrationhelp" href="family_help.html" target="_blank" title="Help For Family Details" ><p class="askhelp"> </p></a>
  <div id="familySuccess" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:yellowgreen;color:#fff;display:none;font-size:16px;">Family details saved successfully.</div>
  
  <form id="showfamilydetail"   ></form>
  
  <div id="fnameDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="poiDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="mnameDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="mpoiDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="bnameDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="bPOIDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="mstatusDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="sgenderDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="snameDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="spoiDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="hinfoDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="foodpreferredDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  
  
  
 <form id="editfamilydetail" name="familyform">
   
  <c:if test="${!empty familyvalue}">
 
  
	<div id ="basicsdetailalign">
	
	
		 <ul>
		<li class="form-row">
		<input class="fathernameicons" style="width:156px;float: left;margin-right: 6px;" type="text" maxlength="30"  placeholder="Father Name" title="Father Name" value="${familyvalue.fathername}" name="Fathername" id="fname" disabled="true" onchange="myFunctionfamily(this.id)"  disabled="true" onkeyup="validateFamilyEach()"/>
		<input class="fathernameicons" style="width:156px;float: left;margin-right: 6px;" type="text" maxlength="20"  placeholder="Father POI" title="Father's Proof of Identity" value="${familyvalue.fatherPOI}" name="FatherPOI" id="poi" disabled="true" onchange="myFunctionfamily(this.id)"  disabled="true" onkeyup="validateFamilyEach()"/>
		</li>
		
	    <li class="form-row">
	       <input class="mothernameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Mother Name" title="Mother Name" type="text" value="${familyvalue.mothername}" name="Mothername" id="mname" disabled="true" onchange="myFunctionfamily(this.id)" disabled="true" onkeyup="validateFamilyEach()"/>
		   <input class="mothernameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="20"  placeholder="Mother POI" title="Mother's Proof of Identity" type="text" value="${familyvalue.motherPOI}" name="MotherPOI" id="mpoi" disabled="true" onchange="myFunctionfamily(this.id)" disabled="true" onkeyup="validateFamilyEach()"/>
	
		</li>
		   
		<li class="form-row">
		<input class="siblingnameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Sibling Name" title="Sibling Name" type="text"  name="Brothername" value="${familyvalue.brothername}" id="bname" disabled="true" onchange="myFunctionfamily(this.id)" disabled="true" onkeyup="validateFamilyEach()"/>
		<input class="siblingnameicons"style="width:156px;float: left;margin-right: 6px;" maxlength="20"  placeholder="Sibling POI" title="Sibling's Proof of Identity" type="text"  name="BrotherPOI" value="${familyvalue.brotherPOI}" id="bpoi" disabled="true" onchange="myFunctionfamily(this.id)" disabled="true" onkeyup="validateFamilyEach()"/>	    
		</li>
	
	    <li class="form-row">
		<input class="spousenameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Spouse Name" title="Spouse Name"  type="text" value="${familyvalue.spousename}" name="Spousename" id="sname" disabled="true" onchange="myFunctionfamily(this.id)" disabled="true" onkeyup="validateFamilyEach()"/>
		<input class="spousenameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="20"  placeholder="Spouse POI" title="Spouse's Proof of Identity" type="text" value="${familyvalue.spousePOI}" name="SpousePOI" id="spoi" disabled="true" onchange="myFunctionfamily(this.id)" disabled="true" onkeyup="validateFamilyEach()"/>
	   
		</li>
		    
		          <li class="form-row">
	   <input class="horoscopeicons" style="width:156px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Horoscopee"  title="Your Horoscope" type="text"  name="HoroscopeInformation" value="${familyvalue.horoscopeInformation}" id="hinfo" disabled="true" onchange="myFunctionfamily(this.id)" disabled="true" onkeyup="validateFamilyEach()"/>
	    <input class="foodprefericons"  style="width:156px;float: left;margin-right: 6px;" maxlength="50"  placeholder="Food Preferred" title="Food you like very much" type="text"  name="Foodpreferred" value="${familyvalue.foodpreferred}" id="foodpreferred" disabled="true" onchange="myFunctionfamily(this.id)" disabled="true" onkeyup="validateFamilyEach()"/>
	 
	   </li>
		 
		 	

		          <li class="form-row" style="clear: both;float: left;margin-top: 5px;position: relative;text-align: left;width: 100%;">
	   <div  style="display:none;visibility:hidden;" id="profileupdatefam1"></div>
	   <input type="hidden" id="profileupdatefam" name="profileupdatereg" /> 
	    
	   <input type="button" id="editfamily"  value="Edit" class="editebutton" onClick="saveFamilyDetail()"/> 
	   <input type="button" style="width:100px" id="familyHistorybuttonId" value="History" class="historybutton" onClick="familyHistory()"/>
	                       
	</li>
	</ul>
	</div> 
    </c:if>
  
  
   <c:if test="${empty familyvalue}">
   
   
   
  						<div id ="basicsdetailalign">
		                    
		                    
		<ul>
				   <li class="form-row">	
		   		<input class="fathernameicons" style="width:156px;float: left;margin-right: 6px;" type="text"  title="Father Name" maxlength="30"  placeholder="Father Name" name="Fathername" id="fname" onkeyup="validateFamilyEach()"/>
		        <input class="fathernameicons" style="width:156px;float: left;margin-right: 6px;" type="text" maxlength="20"  placeholder="Father POI" title="Father's Proof of Identity" name="FatherPOI" id="poi" onkeyup="validateFamilyEach()"/>
		 
		         </li>
		         
		          <li class="form-row">
		          <input class="mothernameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Mother Name" title="Mother Name" type="text" name="Mothername" id="mname" onkeyup="validateFamilyEach()"/>
		          <input class="mothernameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="20"  placeholder="Mother POI" title="Mother's Proof of Identity" type="text" name="MotherPOI"  id="mpoi" onkeyup="validateFamilyEach()"/>
		
		        </li>
		      
		      
		      <li class="form-row">
		      <input class="siblingnameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Sibling Name" title="Sibling Name" type="text" name="Brothername" id="bname" onkeyup="validateFamilyEach()"/>
		      <input class="siblingnameicons"style="width:156px;float: left;margin-right: 6px;"  maxlength="20"  placeholder="Sibling POI" title="Sibling's Proof of Identity" type="text" name="BrotherPOI"  id="bpoi" onkeyup="validateFamilyEach()" />
	          
              </li>
		        	     		       
		          <li class="form-row">
		 <input class="spousenameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Spouse Name" title="Spouse Name" type="text" name="Spousename"  id="sname" onkeyup="validateFamilyEach()"/>
		 <input class="spousenameicons" style="width:156px;float: left;margin-right: 6px;" maxlength="20"  placeholder="Spouse POI" title="Spouse's Proof of Identity" type="text" name="SpousePOI" id="spoi" onkeyup="validateFamilyEach()"/>
	    </li>
		    
		          <li class="form-row">
	    <input class="horoscopeicons" style="width:156px;float: left;margin-right: 6px;" maxlength="30"  placeholder="Horoscope" title="Your Horoscope" type="text" name="HoroscopeInformation" id="hinfo" onkeyup="validateFamilyEach()"/>
	    <input class="foodprefericons"  style="width:156px;float: left;margin-right: 6px;" maxlength="50"  placeholder="Food Preferred" title="Food you like very much" type="text" name="Foodpreferred"  id="foodpreferred" onkeyup="validateFamilyEach()"/>
	 
	   </li>
		  
		<li class="form-row" style="clear: both; float: left; margin-top: 5px; position: relative; text-align: left; width: 100%;">
	  				  			
	  			<input type="button" id="editfamily" class="editebutton" value="Save" onClick="saveFamilyDetail()"/>
	  			<input type="button" id="familyHistorybuttonId" style="width:100px;display:none;" value="History" class="historybutton" onclick="familyHistory()"/> 		
		
		</li>
			
		</ul>
		</div>
		</c:if>
		</form>
		
		
		<div class="notemodel">
  				<b>Note:</b><br>
  			    Please click <a href="#" onclick="changeVisibilitySetting()">here</a> to change your profile view settings.  <br>
  			    POI - Proof Of Identity (like Id of pancard,driving licence,voter id etc.)
  		 </div>  
		 
  </div>
	
  
  <h3 align="left" onclick="closeRestofAll('Social')">
  
 
  Social Account Details</h3>
   
  <div id="indexinputbutton">  
 <div id="SocialSuccess" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:yellowgreen;color:#fff;display:none;font-size:16px;">Social account details saved successfully.</div>
  <a id="editregistrationhelp" href="social_help.html" target="_blank" ><p class="askhelp" tittle="Help For Basic Details"> </p></a>
  <form  method="post" id="showSocialHistory"></form>
  
  <div id="websitelinkDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="twitterlinkDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  <div id="linkedlnlinkDiv" class="error" style="display:none;margin-left: 280px;margin-top: -10px;"></div>
  
  
 <form  method="post" id="editsocialdetail">
 
 <c:if test="${empty socialaccvalue}">
 
<div id="socialbuttonsicon">

	<ul>
		
				<li id="social-facebook ">
				<input  class="facebookicons" style="width:375px;" maxlength="230"  type="text" value="" placeholder="Give your Facebook Url Link"  title="Give your Facebook Url Link" name="Socialwebsitelink"  id="websitelink" />
				</li>
				
				<li id="social-facebook ">
				<input class="linkedinicons" style="width:375px;" maxlength="230"  type="text" value="" placeholder="Give your Linkedin Url Link" title="Give your Linkedin Url Link" name="Socialtwitterlink"  id="twitterlink" />
				</li>
				
				<li id="social-facebook ">
				<input class="twittericons" style="width:375px;margin-left:0px;" maxlength="230"  type="text" value="" placeholder="Give your twitter Url Link" title="Give your twitter Url Link" name="Sociallinkedlnlink"  id="linkedlnlink" />
				</li>
			
				<li class="form-row" style="clear: both; float: left; margin-top: 5px; position: relative; text-align: left; width: 78%;">
				<input  type="button" class="editebutton" name="submit" id="editsocial" value="Save" onclick="saveSocialDetail()"/>
				<input  type="button"  style="width:100px;display:none;" id="socialHistorybuttonId" value="History" onclick="socialHistory()" class="historybutton" />
				</li>
				
		</ul>
		
		<!-- <ul style="margin-top:75px;margin-left:20px;">
		
		    <li>
		        <a href="javascript:void(0)" onclick="showSocialHint()">How to get your social account profile url !</a>
		        
		    </li>
		    
		</ul> -->
</div> 
</c:if>
 <c:if test="${!empty socialaccvalue}">
<div id="flashsocial"></div>

<div id="socialbuttonsicon">
		
	
				   <ul>
				   <li id="social-facebook ">
			
				<input class="facebookicons" style="width:375px;" maxlength="230"  placeholder="Give your Facebook Url Link"  title="Give your Facebook Url Link" name="Socialwebsitelink"  type="text" value="${socialaccvalue.socialwebsitelink}" name="Socialwebsitelink"  id="websitelink" disabled="true"/>
				</li>
				
				 <li  id="social-facebook ">
				 <input class="linkedinicons" style="width:375px;" maxlength="230"  placeholder="Give your LinkedIn Url Link"  title="Give your LinkedIn Url Link" name="Socialtwitterlink" type="text" value="${socialaccvalue.socialtwitterlink}" name="Socialtwitterlink"  id="twitterlink" disabled="true" />
				 </li>
				
				<li  id="social-facebook ">				 
				<input class="twittericons" style="width:375px;margin-left:0px;" maxlength="230"  placeholder="Give your twitter Url Link"  title="Give your twitter Url Link" name="Sociallinkedlnlink"  type="text" value="${socialaccvalue.sociallinkedlnlink}" name="Sociallinkedlnlink"  id="linkedlnlink" disabled="true" />
				</li>
		
				<li class="form-row" style="clear: both; float: left; margin-top: 5px; position: relative; text-align: left; width: 78%;">
				  <input  type="button" name="submit" id="editsocial" value="Edit"  class="editebutton" onclick="saveSocialDetail()"/>
				  <input  type="button"  style="width:100px" value="History" id="socialHistorybuttonId" onclick="socialHistory()" class="historybutton" />
				</li>
			</ul>
			</div> 
			
</c:if>
</form>
 
		<div class="notemodel">
		
		    <b>Note:</b><br>
		    Please click <a href="#" onclick="changeVisibilitySetting()">here</a> to change your profile view settings. 
		  
		</div>
 



  </div>
     
        </div> 
	   </div>
  	    
        	</div> <!--end of right div  -->
	</div>

</div>
					
		
					
																			
		<%
		
		ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
		String version=resource.getString("kycversion");
		String versionDate=resource.getString("kyclatestDate");
		
		%>			
	
					
					
									
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
		              
		              			 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									

                      </div>
                  
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->
	
<!-- Waiting Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF Waiting Div -->

   <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
		
		
    <!-- About Us Div -->
		
		<div id="aboutUslight" class="aboutUsContent">
		<div class="colsebutton" onclick="aboutUsfadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		 <div class="popheaderstyle"><h1>About Me</h1></div>
				
			   <div id="aboutUsContent"></div> 	   
			   
		</div>
		
		<div id="aboutUsfade" onclick="aboutUsfadeout()"></div> 	
		
	<!-- EOF About Us Div -->	


    <!-- Profile Photo Pop Div -->
		
		<div id="profilePhotoPoplight">
		
		<div class="colsebutton" onclick="profilePhotoPopfadeout()"><img height="22" width="22" class="frofilepic_closebtn"  src="resources/images/close_button.png" /></div>
	       
	       <div id="profilePhotoPopContent" style=" display: block;
    height: 536px;
    margin: 0;
    width: auto;"></div> 	   
			   
		</div>
		
		<div id="profilePhotoPopfade" onclick="profilePhotoPopfadeout()"></div> 	
		
	<!-- EOF Profile Photo Pop Div -->	

                                       <div id="profilePhotoChangelight">
		
		                                <div class="colsebutton" onclick="profilePhotoChangefadeout()"><img height="22" width="22" class="frofilepic_closebtn"  src="resources/images/close_button.png" /></div>
	       
	                                    <div id="profilePhotoPopContent" style=" display: block;"></div> 	   
			                               
			                               <div class="reciveddocument" style="text-align:left;">Change Profile Picture</div>
			                               
			                                <form name="uploadProfileImage" id="uploadProfileImage"  method="POST" enctype="multipart/form-data">
	                                    			
	                                    			<ul>
	                                    			    
	                                    			    <li style="margin-right:45px;">
	                                    			    		<p id="selectFile" style="width:100%;overflow:hidden;color:#00b6f5;"><!-- Please browse a file for profile picture. --></p>
	                                    			    </li>
	                                    			    <li style="margin-right:45px;">
	                                    			    		<p id="selectFileInfo" style="width:100%;overflow:hidden;"></p>
	                                    			    </li>
	                                    			    <li style="float:left; margin-top: -50px; margin-left: 90px;">
	                                    			    		<p style="float:left"><a href="#"  class="change" >Browse<input type="file" name="Passportsize" id="file_browse" onchange="CheckFormatofProfilePhoto()"></a></p>
	                                    			    </li>
	                                    			    
	                                    			    <li style="float:left;">
	                                    			            <div id="profilePhotoDiv" style="float: left; width: 100px; margin-top:9px; margin-left: 112px;"><a  class="change" style="height: 16px;margin-left: 0;margin-right: -7px;margin-top: 11px;width: 72px;cursor: pointer;" onclick="uploadProfilePicture()">Save</a></div>
	                                    			    </li>
	                                    			    
	                                    			</ul>	                                   		                                    					                              				                               	                               				                               
					                               
				                             </form> 
				                             
				                             
				                             <div style="float:left;margin-top:50px;margin-bottom: 15px;margin-left: 52px;margin-top: 31px;">
				                                <b style="color:red;float:left;" >Important !!</b><br>
				                                 Please do not upload the profile picture that is not you.Upload good looking picture.</div>
		                                </div>
		
		                                <div id="profilePhotoChangefade" onclick="profilePhotoChangefadeout()"></div> 
		
		
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->


<script src="resources/datePicker/datePicker/jquery.datetimepicker.js"></script>
<script>

$('.some_class').datetimepicker();


$('#datetimepicker_dark').datetimepicker({theme:'dark'})

</script>


</body>
</html>
     	