<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<script type="text/javascript">
	
function empViewDetails(data)
{
	var info = data.split("?");
	var url = info[0];
	var docid = info[1];
	
	$("#waitingdivId").show();
	
	$.ajax
    ({
		type : "post",				
		url : url,		
		data : "docId="+docid,
		success : function(response) {
			
			$("#waitingdivId").hide();
			
			 $("#viewUsers1")
				.html(response);
							
		},
		error : function() {
			
			$("#waitingdivId").hide();
			
			alert('Your request has failed due some reasom please try again later !');
		}
	});	
	
	$("#titleBar").html("<h2><span>Employment Details</span></h2>");
}

function backtoMain(url)
{
   window.location = url;	
}
</script>


</head>

<body>
<div id="History" style="display: inline-table; overflow: visible; margin-top: 25px;">	
<c:if test="${!empty Historydetails}">

<input type="button" value="Back to Employment Document" style="margin-left:569;margin-top:-15px;" onclick="backtoMain('employeeDetails.html')"/>
	
	<table class='CSSTableGenerator' align="left" width="500px" border="1" cellspacing="1" align="center">
		<tr>
		    <th height="10">Serial No</th>
			<th height="10">Doc Data's Description</th>	
			<th height="10">DOC CR_Date</th>
			<th height="10">DocNames</th>			
			<th height="10">Details</th>
		</tr>
        <%int i=0; %>
		<c:forEach items="${Historydetails}" var="det">					 
			 <tr>
			    <td height="10" width="50"><c:out value="<%=++i%>"/></td>
				<td width="230" align="left"><c:out value="*${det.doc_des}"/></td>
				<td width="190"><c:out value="${det.cr_date}"/></td>
				<td width="190"><c:set var="docName" value="${det.doc_name}"/>
				
				<% String docName = (String)pageContext.getAttribute("docName");
				   String nameAry[] = docName.split(",");
				   int k = 0;
				   for(int j=0;j<nameAry.length;j++)
				   {
					   if(!nameAry[j].equals("No Document"))
					   {%>
						   <%=++k%>.<%=nameAry[j].split("_")[0]%><br>
					   <% }
				   }
				%>
				</td>				
				<td align="center" width="190"><a href="#" onClick="empViewDetails('emp_viewmore.html?${det.doc_id}')"><button id="viewhistory">View</button></a></td>
			 </tr>			
		</c:forEach>		
	</table>	
</c:if>

<c:if test="${empty Historydetails}">
<br><br><br><br><br>
<center>
<h>You don't have any Employment Details ,To view or Share try to fill the employement Details </h>
</center>

</c:if>
</div>
</body>
</html>