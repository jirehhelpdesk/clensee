<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<html>
<head>
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/kyc_admin_style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script>

function viewFeatureAsperCategory(featureName)
{
   var featureCategory = document.getElementById("featureCategoryId").value;
  
  if(featureCategory=="Select Category")
  {
	  $("#orgindividualFeatureId").hide();
      $("#orgutilizerFeatureId").hide();
      $("#orgcorporateFeatureId").hide();  
  }
  else if(featureCategory=="Individual" && featureName!="Select feature")
  {
        $("#orgindividualFeatureId").show();
        $("#orgutilizerFeatureId").hide();
        $("#orgcorporateFeatureId").hide();
        
        $.ajax({  
	  	     type : "Post",   
	  	     url : "organisefeaturefromAdmin.html", 
	  	     data :"featureCategory="+featureCategory+"&featureName="+featureName,	     	     	     
	  	     success : function(response) 
	  	     {  
	  	    	
	  	    	$("#orgindividualFeatureId").html(response);
	  	     },    	   
 	    });  
  }
  else if(featureCategory=="Utilizer" && featureName!="Select feature")
  {
	  $("#orgindividualFeatureId").hide();
      $("#orgutilizerFeatureId").show();
      $("#orgcorporateFeatureId").hide();
      
      $.ajax({  
	  	     type : "Post",   
	  	     url : "organisefeaturefromAdmin.html", 
	  	     data :"featureCategory="+featureCategory+"&featureName="+featureName,     	     	     
	  	     success : function(response) 
	  	     {  
	  	    	 $("#orgutilizerFeatureId").html(response);
	  	     },    	   
	    });  
      
  }
  else if(featureCategory=="Corporate" && featureName!="Select feature")
  {
	  $("#orgindividualFeatureId").hide();
      $("#orgutilizerFeatureId").hide();
      $("#orgcorporateFeatureId").show();
      
      $.ajax({  
	  	     type : "Post",   
	  	     url : "organisefeaturefromAdmin.html", 
	  	     data : "featureCategory="+featureCategory+"&featureName="+featureName,     	     	     
	  	     success : function(response) 
	  	     {  
	  	    	$("#orgcorporateFeatureId").html(response);
	  	     },    	   
	    });  
  }
  else
  {
	  $("#orgindividualFeatureId").hide();
      $("#orgutilizerFeatureId").hide();
      $("#orgcorporateFeatureId").hide();  	  
  }
  
  
}

function deleteorganisefeature(which)
{		
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var feature_category=document.getElementById("featureCategoryId").value;
						
						 $.ajax({  
						  	     type : "Post",   
						  	     url : "deleteFeature.html", 
						  	     data :"feature_id="+which,	     	     	     
						  	     success : function(response) 
						  	     {  
						  	    	alert(response);
						  	    	if(response=="Feature deleted successfully !")
						  	    		{
								  	     $.ajax({
								  					type : "post",
								  					url : 'organisefeature.html',
								  					cache : false,
								  					success : function(response) {
								  											
								  						 $("#viewUsers").html(response);
								  						 viewFeatureAsperCategory(feature_category);
								  						
								  						},				
								  				});
								  			 		 		
								  			    $("#titleBar").html("<h2><span>ORGANIZE FEATURE</span></h2>");
						  				
						  	    		}
						  	     },    	   
					  	    });  
				}
			else
				{
				     $("#sessionlight").show();
			         $("#sessionfade").show();
				}
		}
	});
	
	
	
	 
}


function validatereg(which,id) {

	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
							if ( which.value == "Edit" )
							{
						     	  which.value = "Save";
							}							
						else
					        {	   
								   which.value = "Edit";
								   
							       var feature_name=document.getElementById("feature_name"+id).value;
							       var feature_value=document.getElementById("feature_value"+id).value;
							       var feature_price=document.getElementById("feature_price"+id).value;
							       var feature_id=id;
							       var feature_category=document.getElementById("featureCategoryId").value;
							        		      
							       $.ajax({  
								      	     type : "Post",   
								      	     url : "editfeaturedetail.html", 
								      	     data :"feature_name="+feature_name+"&feature_value="+feature_value+"&feature_price="+feature_price+"&feature_id="+feature_id+"&feature_category="+feature_category,	     	     	     
								      	     success : function(response) 
								      	     {  
								      	         alert(response);															
								      	     },  		      	     
							      	    });                
					        }
						
						x=document.getElementById("feature_name"+id)
					    x.disabled = !x.disabled;
						x=document.getElementById("feature_value"+id)
					    x.disabled = !x.disabled;
						x=document.getElementById("feature_price"+id)
					    x.disabled = !x.disabled;
						x=document.getElementById("feature_category"+id)
					    x.disabled = !x.disabled;
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
   
}

</script>

</head>
<body>
<div id="featuredetail">


<form id="editfeaturedetail" name="featureform"  method="post" >

<div >
<b>Feature Category:</b><select id="featureCategoryId">
             <option value="Select Category">Select Category</option>
             <option value="Individual">Individual</option>
             <option value="Utilizer">Utilizer</option>
             <option value="Corporate">Corporate</option>
     </select>
</div>

<div >
<b>Feature Name:</b>
   
   
   <select  name="feature_name" id="featNameId" onchange="viewFeatureAsperCategory(this.value)">
		   <option value="Select feature">Select feature</option>
           <option value="Profile Viewing">Profile Viewing</option>
           <option value="Individual Sharing">Individual Sharing</option>
           <option value="Documents Received">Documents Received</option>
           <option value="Receiving Mails">Receiving Mails</option>
           <option value="Utilizer Sharing / Removing">Utilizer Sharing / Removing</option>
           <option value="SMS">SMS</option>
           <option value="Files Storage">Files Storage</option>
           <option value="Doc  Accessory of Employees and customers">Doc  Accessory of Employee's and customers</option>		                                        
           <option value="Advanced Filters">Advanced Filters </option>
    </select>
    
    
</div>


	
			
				<div id="orgindividualFeatureId" style="display:none;">
				
				  
				</div>
		
				<div id="orgutilizerFeatureId" style="display:none;">
				
				 
				</div>
		
				<div id="orgcorporateFeatureId" style="display:none;">
				 
				  
				</div>
		
	
	</form>
			           
</div>
</body>
</html>