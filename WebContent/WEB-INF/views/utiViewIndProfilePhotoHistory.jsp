<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Individual Profile Photo History</title>
</head>
<body>

<%String photoHistory = (String)request.getAttribute("profilePhotoHistory");

String  dataValue[] = photoHistory.split("@@@");

String picValue = dataValue[1];


  %>

<%if(!picValue.equals("NoData")){ %>
   
   <%
       picValue = picValue.substring(0,picValue.length());
                
      String picData[] = picValue.split(",");	
   %>
   <div class="profilePhotoHistory" >

       <ul>
           <%for(int i=0;i<picData.length;i++) {
                     
        	   String picDetails[] = picData[i].split("&&");
           %>
           
             <li>
             
             		<a  id="<%=picDetails[0]%>" >
             		
             		      <img src="${pageContext.request.contextPath}<%="/previewThumbprofileOther.html?fileName="+picDetails[0]+"&docCategory=Profile_Photo&year="+request.getAttribute("regYear")+"&indId="+dataValue[0]+""%>"   width="100" height="100" /></img>
             		
             		</a>
             		
             		<br>
             		<b>Updated On:<%=picDetails[1].split(" ")[0]%></b>
             		
             
             </li> 
           <%} %>
       </ul>
     
   </div>
 
 <%}else{ %>  
   <div class="profilePhotoHistory" >
         <ul>
			   <li>Till now Nothing has set for Profile Photo !</li>				       
		 </ul>
   </div>
<%} %>

</body>
</html>