
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<html>
<head>
<title></title>

<script>

function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}	

</script>

</head>
<body>
	<%String viewerDetails = (String)request.getAttribute("visiterProfileDetails");
	  String viewerDetailsArray[] = viewerDetails.split(",");
	 
	 %>
	  <div id='visitorContent'>	
	     <%if(!viewerDetails.equals("No Data")) {%>	  	
	      
		<ul >
		
		
		   <%for(int i=0;i<viewerDetailsArray.length;i++){ %> 
			 <li>
			
					         <%String viewerIndId = viewerDetailsArray[i].split(":")[0];
					           String viewerYear = viewerDetailsArray[i].split(":")[1];
					           String viewerPhoto = viewerDetailsArray[i].split(":")[2];
					           String viewerName = viewerDetailsArray[i].split(":")[3];
					           String viewedDate = viewerDetailsArray[i].split(":")[4];
					         %>
					         <div class="visitnamedeatils-photo">
					            <%if(!viewerPhoto.equals("no image"))
							   {%>		
							   
							       <img src="${pageContext.request.contextPath}<%="/previewprofileOther.html?fileName="+viewerPhoto+"&docCategory=Profile_Photo&year="+viewerYear+"&indId="+viewerIndId+""%>" width="80" height="60" /></img>						    							     
							        
							   <%}
							   else
							   {%>
								    <img src="resources/kyc_images/Koala.jpg"  width="80" height="60" ></img>
								    
							    <%}%>
					         </div>
					         <div class="visitnamedeatils">
					             <p> <b>Name :</b> <%=viewerName%>	</p>				         
					         
					              <p><b>Viewed Date :</b> <%=viewedDate%></p>					         
					         </div>	
					         <div class="visitnamedeatils-button">
					              <input value="View Profile" type="button"  onclick="viewOtherProfile('validateProfileViewing.html?asdgth=<%=viewerIndId%>')"/>					         
					         </div>		
					        		         
				 </li>
		    <%} %>
		   
		</ul>
		<%} else {%>
		
		                <div class="notice" style="margin-left: 245px;margin: 73px auto 38px;width: 250px; display: inline;">
						
						   No one has viewed your profile yet !
						   
						</div>
		<%} %>
	 </div>

</body>
</html>
   