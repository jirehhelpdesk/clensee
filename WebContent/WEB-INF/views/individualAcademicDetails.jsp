 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Documents</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/onlineresources/code.jquery.com.jquery-1.10.2.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css"/>

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<style>

#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
    
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

</style>



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}




</style>

<script>
   
function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		window.location = "getacademicdetails.html";
		}
}


function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>


<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
      left: 50%;
    top:0%;
    left:0;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}


 #kycDocPopUplight{
 border: 2px solid #03b2ee;
    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
    left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;  
    z-index: 1002;
    
}

</style>
 
<script>

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewDocumentInPopUp(fileName)
{
	 $.ajax({
				type : "post",
				url : "checksessionforIndividual.html",
				success : function(response) {
					
					if(response=="Exist")
						{
								document.getElementById('waitlight').style.display='block';
								document.getElementById('waitfade').style.display='block';
								 
								$.ajax({
									type : "get",				
									url : "viewAcademic.html",
									data: "fileName="+fileName,
									success : function(response) {
									
										document.getElementById('waitlight').style.display='none';
										document.getElementById('waitfade').style.display='none';
										 
										document.getElementById('kycDocPopUplight').style.display='block';
								        document.getElementById('kycDocPopUpfade').style.display='block';
								        $("#kycDocPopUpDisplay").html(response);
								        			
									},		
								});		 
						}
					else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 		 		
	
}

</script> 
 



<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
		
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('.topprofilestyle').hide();
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();		 	
	    }
}


function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}

</script>





<script>

/* All Scripts for MyDocument pannel  */
		
	function myDocuments(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
										
				},
				
			});
		 		 		 		
		$("#titleBar").html("<h2><span>ACADEMIC DOCUMENTS</span></h2>");
	}
	
	function myDocuments1(handlerToHit) {
		 
		var v= '1stUser';
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				data : "status=" +v,
				success : function(response) {
					$("#center-body-div").hide();					
					$("#searchicon").hide();          					
					 $("#viewUsers1")
						.html(response);
					 					   
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>KYC DOCUMENTS</span></h2>");
	}
	
	function myDocuments2(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 window.location = "search_financialDoc.html";
										
				},
				
			});
		 		 		 		
		
	}
	
	function myDocuments3(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>EMPLOYMENT DOCUMENTS</span></h2>");
	}
	
	function myDocuments4(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>EMPLOYEMENT DETAILS</span></h2>");
	}
	
	function reSelect() {
				
	 $.ajax
	      ({
			type : "post",			
			url : "reselect.html",			
			success : function(response) {
														
				$("#viewUsers1")
					.html(response);
												
			},
			error : function() {
				alert('Error while fetching response');
			}
		});	
	 $("#titleBar").html("<h2><span>DOCUMENTS</span></h2>");

	}  
	
	
	function Academictoggle1(val1) {
		
		if (document.getElementById("pkg1"+val1+"").style.display =="block") {
		document.getElementById("pkg1"+val1+"").style.display ="none";
		}
		else {
		document.getElementById("pkg1"+val1+"").style.display ="block";
		}
		if (document.getElementById("table_pkg1"+val1+"").style.display =="block") {
		document.getElementById("table_pkg1"+val1+"").style.display ="none";
		}
		else {
		document.getElementById("table_pkg1"+val1+"").style.display ="block";
		}

		}

		function Academictoggle2(val2) {
			
			if (document.getElementById("pkg2"+val2+"").style.display =="block") {
			document.getElementById("pkg2"+val2+"").style.display ="none";
			}
			else {
			document.getElementById("pkg2"+val2+"").style.display ="block";
			}
			if (document.getElementById("table_pkg2"+val2+"").style.display =="block") {
			document.getElementById("table_pkg2"+val2+"").style.display ="none";
			}
			else {
			document.getElementById("table_pkg2"+val2+"").style.display ="block";
			}

			}
         
		function academicFormDetails(val1)
		{
			if (document.getElementById("hiddenAccform"+val1+"").style.display =="block") {
				document.getElementById("hiddenAccform"+val1+"").style.display ="none";
				
				document.getElementById("invisible"+val1+"").style.display ="none";	
				
				}
				else {
				document.getElementById("hiddenAccform"+val1+"").style.display ="block";
				
				}
				if (document.getElementById("showAccform"+val1+"").style.display =="block") {
				document.getElementById("showAccform"+val1+"").style.display ="none";
				
				}
				else {
				document.getElementById("showAccform"+val1+"").style.display ="block";				
				}
		}
	
		function cancelFormDetails(val1)
		{
			if (document.getElementById("hiddenAccform"+val1+"").style.display =="block") {
				document.getElementById("hiddenAccform"+val1+"").style.display ="none";
				
				document.getElementById("invisible"+val1+"").style.display ="block";								
				}
				else {
				document.getElementById("hiddenAccform"+val1+"").style.display ="block";
				
				}
				if (document.getElementById("showAccform"+val1+"").style.display =="block") {
				document.getElementById("showAccform"+val1+"").style.display ="none";
				
				}
				else {
				document.getElementById("showAccform"+val1+"").style.display ="block";
				
				}
		}
		
		/* <!-- change the state noneditable to editable for document--> */
		
		function changeState(docHierId)
		{		   
		   var docHir = document.getElementById(docHierId).value;		
			
			var docHirarr = docHir.split(",");
							
			for(var j=0;j<docHirarr.length;j++)
			{			
			   var docHir1 = docHirarr[j];
			   var docHir2 = docHir1.split(":");			   	   			   
		       document.getElementById(docHir2[0]).removeAttribute("readonly");	
		       document.getElementById(docHir2[0]).removeAttribute("disabled");
			}
		}
		
		
		/* <!-- End of code for noneditable to editabel for document-->  */
		
function chooseAcaIds() {
		
	x=document.getElementById("proceedButtonId")
    x.disabled = !x.disabled;
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	var click = [];
	$(".checkbox:checked").each(function() {
        click.push($(this).val());
    });
	 	
	if(click.length==0)
		{ 		  
		   document.getElementById('waitlight').style.display='none';
	       document.getElementById('waitfade').style.display='none';	       
	    
			x=document.getElementById("proceedButtonId")
	    	x.disabled = !x.disabled;
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		     
			 $("#socialdisplaySharedDocDiv").html(" Select at least one document type. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");             
		}
	else
		{

		
		 $.ajax({
				type : "post",				
				url : "add/choosen/academic/category.html",				
				data :"click="+click,	  
				success : function(response) {
					 
					 x=document.getElementById("proceedButtonId")
		 		     x.disabled = !x.disabled;
					 
					 document.getElementById('waitlight').style.display='none';
				     document.getElementById('waitfade').style.display='none';
					 					     
				     document.getElementById('sociallight').style.display='block';
				     document.getElementById('socialfade').style.display='block';
				     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
				     $("#socialdisplaySharedDocDiv").html("Academic type has been added successfully. <div style=\"width:97%;margin 0 auto;\">  <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\" /> </div>");
				     						 
				},
			});		 	
	
		}//End Of else
    }
				
	/* End of MyDocument Scripts  */
	
</script>


       
 <style>
            .notClicked {color: black}
            .Clicked {color: red}
        </style>
        


<script src="resources/js/kyc_side_login.js"></script>

<script>
function viewHomeProfile(url) 
{
	window.location = url;	 
}	
</script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
 
 <!-- Script and CSS for Academic Documents -->
 
<!-- Script for validation with submit -->

<script type="text/javascript">
function hidebButtons(supId)
{
	
	if (document.getElementById("EditId"+supId).style.display =="block")
	{
		document.getElementById("EditId"+supId).style.display ="none";
		document.getElementById("submitId"+supId).style.display ="block";
	    document.getElementById("resetId"+supId).style.display ="block";		
	}
}

function updateAcademicDocument(index,formTag)
{
	
	var viewDiv = "view"+index+formTag;
	var editDiv = "edit"+formTag;
	
	document.getElementById(viewDiv).style.display = "none";
	document.getElementById(editDiv).style.display = "block";
	
}

function cancelUploadAcademicDoc(index,formTag)
{
	var viewDiv = "view"+index+formTag;
	var editDiv = "edit"+formTag;
	
	document.getElementById(viewDiv).style.display = "block";
	document.getElementById(editDiv).style.display = "none";	
}


function validateOnEachComp(name,document_Hir)
 {	 	
	 
		var docHir = document.getElementById(document_Hir).value;		
		
		var docHirarr = docHir.split(",");
		
		var alfanumeric = /^[a-zA-Z0-9_ ]+$/;
		var alfabet = /^[a-zA-Z]+$/;
		var numeric = /^[0-9]+$/;  
		
		for(var j=0;j<docHirarr.length-3;j++)
		{	
		  
		   var docHir1 = docHirarr[j];
		   var docHir2 = docHir1.split(":");
		   	   
		   for(var r=0;r<docHir2.length;r++)
			   {		   		    	    
			         if(docHir2[r]=="N" && name==docHir2[r-1])
			    	   {		    	   
			    	  	   var id = document.getElementById(docHir2[r-r]).value;		    	 			    	 	
			    	 	   
			    	 	   if(id=="")
			    	 		 {
			    	 		   $("#"+docHir2[r-1]).hide();			    	 		   	    	 		  
			    	 		 }
			    	 	   else if(id.match(numeric))  
			    	         {  
			    	 		   $("#"+docHir2[r-1]).hide();		    	           
			    	         } 
			    	       else  
			    	         { 
			    	    	   $("#"+docHir2[r-1]).show();
			    	 		   $("#"+docHir2[r-1]).html("Enter numeric value for this field.");		    	           
			    	         }  			    	
			    	   }
			         
			         if(docHir2[r]=="A" && name==docHir2[r-1])
			    	   {		    	   
			    	  	   var id = document.getElementById(docHir2[r-r]).value;		    	 			    	 			    	 	   
			    	 	   if(id=="")
			    	 		 {
			    	 		    $("#"+docHir2[r-1]).hide();
			    	 		 }
			    	 	   else if(id.match(alfanumeric))
			    	 		 {
			    	 		    $("#"+docHir2[r-1]).hide();
			    	 		 }
			    	 	   else
			    	 		   {
			    	 		       $("#"+docHir2[r-1]).show();
			    	 		       $("#"+docHir2[r-1]).html("Enter alphanumeric value for this field.");
			    	 		   }
			    	 	    			    	
			    	   }
			        
			         if(docHir2[r]=="AN" && name==docHir2[r-1])
			    	   {		    	   
			    	  	   var id = document.getElementById(docHir2[r-r]).value;		    	 			    	 	
			    	 	 
			    	 	   if(id=="")
			    	 		 {
			    	 		     $("#"+docHir2[r-1]).hide();
			    	 		 }
			    	 	  else if(id.match(alfanumeric))
			    	 		 {
			    	 		     $("#"+docHir2[r-1]).hide();
			    	 		 }
			    	 	  else
			    	 		  {
			    	 		      $("#"+docHir2[r-1]).show();
			    	 		      $("#"+docHir2[r-1]).html("Enter alphanumeric value for this field.");
			    	 		  }
			    	   }		           
			      }
		  }

   }


function uploadAcademicDoc(formName,standardName,labelName,count)
{	
	var flag = 'true';
	
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 	
	var description = document.getElementById("desc"+count+formName).value;
	var fileDetails = document.getElementById("file"+count+formName).value;
	
	
	if(description.length<5)
		{	
		    flag = 'false';
		    document.getElementById('waitlight').style.display='none';
			document.getElementById('waitfade').style.display='none';
			
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		     $("#socialdisplaySharedDocDiv").html("Give some description to upload the file. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  
	         
			 return false;		     
		}
	
	if(description.length>250)
	{	
	    flag = 'false';
	    document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
		
		
	     document.getElementById('sociallight').style.display='block';
	     document.getElementById('socialfade').style.display='block';
	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
	     $("#socialdisplaySharedDocDiv").html("Give description with in 250 characters. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  
         
		 return false;		     
	}
	
	if(fileDetails=="")
	{  
	    flag = 'false';
	    document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
				
	    document.getElementById('sociallight').style.display='block';
	    document.getElementById('socialfade').style.display='block';
	    document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		$("#socialdisplaySharedDocDiv").html("Select a file to upload. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  
         
		return false;
	    
	}
	
	if(fileDetails!="")
		{
		    var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
		    var extension = new Array("jpg","jpeg","gif","png","pdf");		
		    
		    var condition = "NotGranted";
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}		    
			if(condition=="NotGranted")
				{
				     flag = 'false';
				     document.getElementById('waitlight').style.display='none';
				     document.getElementById('waitfade').style.display='none';
					 					
				     document.getElementById('sociallight').style.display='block';
				     document.getElementById('socialfade').style.display='block';
				     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
					 $("#socialdisplaySharedDocDiv").html(" pdf/image files are only allowed. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
				     
				     return false;
				}
		}
	
	var fileDetails1 = document.getElementById("file"+count+formName);  
	var fileSize = fileDetails1.files[0];
	var fileSizeinBytes = fileSize.size;
	var sizeinKB = +fileSizeinBytes / 1024;
	var sizeinMB = +sizeinKB / 1024;
	
	if(sizeinMB>2)
		{
			flag = 'false';
			document.getElementById('waitlight').style.display='none';
			document.getElementById('waitfade').style.display='none';
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html(" File size should not more then 2 MB. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\"  class=\"button_style\"value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  
		    
			 return false;
		}
	
	if(flag=='true')
		{
		
		
		 $.ajax({
				type : "post",
				url : "checksessionforIndividual.html",
				success : function(response) {
					
					if(response=="Exist")
						{
						      						
								$.ajax
							    ({
									type : "post",				
									url : "validateFileStorage.html",
									data: "givenfileSize="+sizeinKB,
									success : function(response) {
										
										if(response=="withinStorage")
										{
										    var formData = new FormData($("#"+formName)[0]);
										    formData.append("labelName",labelName);
										    formData.append("standardname",standardName);
										   	
										    
										    $.ajax({
										        url: "uploadAcademicDocuments.html",
										        type: 'POST',
										        data: formData,
										        //async: false,
										        success: function (data) {          
										           		  
										        		document.getElementById('waitlight').style.display='none';
														document.getElementById('waitfade').style.display='none';
													
														
													     document.getElementById('sociallight').style.display='block';
													     document.getElementById('socialfade').style.display='block';
													     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
													     $("#socialdisplaySharedDocDiv").html("Your document has been uploaded successfully.  <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");   
							        	
											        	$("#"+formName).hide();
											        	$("#buttonDiv"+formName).html("<h1>Successfully uploaded .</h1>");
										        	
										        },
										        cache: false,
										        contentType: false,
										        processData: false
										    });
										}
										else
											{
													document.getElementById('waitlight').style.display='none';
													document.getElementById('waitfade').style.display='none';
													
											        alert("Failed due to storage limitation,to upload more documents migrate your plan!");
											}
									},
							    })
														
						}
					else
						{
						   $("#sessionlight").show();
					       $("#sessionfade").show();
						}
				}
			}); 
	
	   }
 }


function ValidateCompOnEach(formName,standard)
{	
	
	
	var buttonValue = document.getElementById("buttonId"+formName).value;
		
	var docHir = document.getElementById("acaHirarchy"+formName).value;		
	var standardName = document.getElementById("standard"+standard).value;		
			
	var alfanumeric = /^[a-zA-Z0-9]+$/;
	var alfabet = /^[a-zA-Z]+$/;
	var numeric = /^[0-9]+$/;  
	var alfanumericSpecialCharacter = "/^[ A-Za-z0-9_@./#+-]*$/";
	
	var eachDocHir = docHir.split(",");
	var eachDocCompHir = "";
			
	for(var achier=0;achier<eachDocHir.length;achier++)
		{
			if(eachDocHir[achier].indexOf("tb") > -1 || eachDocHir[achier].indexOf("dd") > -1)
				{
				   eachDocCompHir +=  eachDocHir[achier] + ",";
				}
		}
	
	eachDocCompHir = eachDocCompHir.substring(0,eachDocCompHir.length-1);
	var eachDocCompHirArry = eachDocCompHir.split(",");

			for(var j=0;j<eachDocHir.length;j++)
			{		   		
				var eachcomponent = eachDocHir[j];
			    var componentArray = eachcomponent.split(":");
			  
			    for(var k=0;k<componentArray.length;k++)
				  {		    
			    	   if(componentArray[k]=="N")
				    	   {			        	  
					    	  	   var id = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	  	 		    	 			    	 	
					    	 	   			    	  	
					    	 	   if(id=="")
					    	 		 { 			    	 									    	 		    
					    	 		    $("#div"+formName).hide();					    	 		   
					    	 		 } 
					    	 	   else if(id.match(numeric))  
					    	         {  			    	 		    
					    	 		    $("#div"+formName).hide();		    	           
					    	         } 
					    	       else  
					    	         { 					    	    	   
					    	    	   $("#div"+formName).show();
					    	 		   $("#div"+formName).html("Enter numeric value for "+componentArray[k-k]+" !");	
					    	 		   return false;
					    	         } 
			    	         }
				    	
				    	if(componentArray[k]=="A")
				    	   {			        	 
				    	  	   var id1 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
				    	 	   		    	  	  
				    	  	    if(id1=="")
				    	 		 {				    	  	    	
				    	 		    $("#div"+formName).hide();				    	 		    
				    	 		 }
				    	 	    else if(id1.match(alfabet))
				    	 		 {
				    	 	    	$("#div"+formName).hide();
				    	 		 }
				    	 	    else
				    	 		   {			    	 		    				    	 		  
				    	 		      $("#div"+formName).show();
				    	 		      $("#div"+formName).html("Enter alphabets value for "+componentArray[k-k]+" !");		
				    	 		      return false;
				    	 		   }
				    	    }
				        
				         if(componentArray[k]=="AN")
				    	   {			        	 
				        	 var id2 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
				    	 	  		        	
				    	  	 if(id2=="")
				    	 		 {		    	 		    					    	  		
				    	 		    $("#div"+formName).hide();				    	 		   
				    	 		 }
				    	 	  else if(id2.match(alfanumeric))
				    	 		 {		    	 		    
				    	 		    $("#div"+formName).hide();
				    	 		 }
				    	 	  else
				    	 		  {				    	 		   				    	 		      
				    	 		      $("#div"+formName).show();
				    	 		      $("#div"+formName).html("Enter alphanumeric value for "+componentArray[k-k]+" !");	
				    	 		     return false;
				    	 		  }
				    	   }	
				         
				         
				         if(componentArray[k]=="ASC")
				    	   {					        	 				        	 			        	 
				        	 var id2 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
				        	 
				        	 
				        	 
				    	  	 if(id2=="")
				    	 		 {				    	  					    	 		    					    	  		
				    	 		    $("#div"+formName).hide();				    	 		  				    	 		   
				    	 		 }
				    	 	  else if(id2.indexOf(",")>0)
				    	 		 {		    	 		    
					    	 		 $("#div"+formName).show();
				    	 		     $("#div"+formName).html("Comma are not allowed for "+componentArray[k-k]+" !");	
				    	 		     return false;
				    	 		 }
				    	 	 else if(id2.indexOf("&")>0)
				    	 		 {		    	 		    
					    	 		 $("#div"+formName).show();
				    	 		     $("#div"+formName).html(" '&' are not allowed for "+componentArray[k-k]+" !");	
				    	 		     return false;
				    	 		 }
				    	 	  else
				    	 		  {				    	 		     
				    	 		      $("#div"+formName).hide();				    	 		     
				    	 		  }
				    	   }				         
				    }			    	      	     
			  }			
}





function ValidateComp(formName,standard)
{
	var flag= 'true';
	
	var buttonValue = document.getElementById("buttonId"+formName).value;
		
	var docHir = document.getElementById("acaHirarchy"+formName).value;		
	var standardName = document.getElementById("standard"+standard).value;		
			
	var alfanumeric = /^[a-zA-Z0-9]+$/;
	var alfabet = /^[a-zA-Z]+$/;
	var numeric = /^[0-9]+$/;  
	var alfanumericSpecialCharacter = "/^[ A-Za-z0-9_@./#+-]*$/";
	
	
	var eachDocHir = docHir.split(",");
	var eachDocCompHir = "";
			
	for(var achier=0;achier<eachDocHir.length;achier++)
	{
		if(eachDocHir[achier].indexOf("tb") > -1 || eachDocHir[achier].indexOf("dd") > -1)
			{
				eachDocCompHir +=  eachDocHir[achier] + ",";
			}
	}
	
	eachDocCompHir = eachDocCompHir.substring(0,eachDocCompHir.length-1);
	var eachDocCompHirArry = eachDocCompHir.split(",");
	
	if(buttonValue=="Edit")
	{
		document.getElementById("buttonId"+formName).value = "Save";
		document.getElementById("buttonId"+formName).className = "saveinner_button";
		document.getElementById("buttonIdUpper"+formName).value = "Save";
		document.getElementById("buttonIdUpper"+formName).className = "saveinner_button";
		
		
		for(var j=0;j<eachDocCompHirArry.length;j++)
		{		   		
			var eachcomponent = eachDocCompHirArry[j];
		    var componentArray = eachcomponent.split(":");		
		    
		    
		    if(document.getElementById("comp"+standard+componentArray[0]).value!="")
		    	{
		         	x= document.getElementById("comp"+standard+componentArray[0]);	
			        x.disabled = !x.disabled;	
			        
			        if(document.getElementById("comp"+standard+componentArray[0]).value=="Not filled")
			        {
			        	 document.getElementById("comp"+standard+componentArray[0]).value = "";
					     document.getElementById("comp"+standard+componentArray[0]).placeholder = "Not filled";	
			        }			       
		    	}
		   
		}
		
		var editHiddenIds = document.getElementById("editHidden"+formName).value;		
		var editHiddenIdsArray = editHiddenIds.split("##");
		
		for(var d=0;d<editHiddenIdsArray.length;d++)
		{
			$("#"+editHiddenIdsArray[d]).show();
		}
				
		var v = cleanGarbageRecords();
			
	}
	
	if(buttonValue=="Save")
	{		
		document.getElementById('waitlight').style.display='block';
		document.getElementById('waitfade').style.display='block';
		
			for(var j=0;j<eachDocHir.length;j++)
			{		   		
				var eachcomponent = eachDocHir[j];
				
			    var componentArray = eachcomponent.split(":");
			  
			         if(j<6){
			        	 	       
			        	        // Validation compulsory upto six components
			        	        
							    for(var k=0;k<componentArray.length;k++)
								  {	    			    	
							    	   if(componentArray[k]=="N")
								    	   {			        	  
									    	  	   var id = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	  	 		    	 			    	 	
									    	 	   			    	  	
									    	 	   if(id=="")
									    	 		 { 
									    	 		    document.getElementById('waitlight').style.display='none';
								    	 				document.getElementById('waitfade').style.display='none';
								    	 				
									    	 		    flag = 'false';
									    	 		    $("#div"+formName).show();
									    	 		    $("#div"+formName).html("Fill the field of "+componentArray[k-k]+" .");	
									    	 		    return false; 
									    	 		 } 
									    	 	   else if(id.match(numeric))  
									    	         {  			    	 		    
									    	 		    $("#div"+formName).hide();		    	           
									    	         } 
									    	       else  
									    	         { 
									    	    	    document.getElementById('waitlight').style.display='none';
								    	 				document.getElementById('waitfade').style.display='none';
								    	 				
									    	    	   	flag = 'false';
									    	    	   	$("#div"+formName).show();
									    	 		   	$("#div"+formName).html("Enter numeric value for "+componentArray[k-k]+" .");
									    	 		   	return false;
									    	         } 
							    	         }
								    	
								    	if(componentArray[k]=="A")
								    	   {			        	 
								    	  	   var id1 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
								    	 	   		    	  	  
								    	  	    if(id1=="")
								    	 		 {
								    	  	    	document.getElementById('waitlight').style.display='none';
							    	 				document.getElementById('waitfade').style.display='none';
							    	 				
								    	  	    	flag = 'false';
								    	 		    $("#div"+formName).show();
								    	 		    $("#div"+formName).html("Fill the field of "+componentArray[k-k]+" .");	
								    	 		    return false; 
								    	 		 }
								    	 	    else if(id1.match(alfabet))
								    	 		 {
								    	 	    	$("#div"+formName).hide();
								    	 		 }
								    	 	    else
								    	 		   {
								    	 	    		document.getElementById('waitlight').style.display='none';
							    	 					document.getElementById('waitfade').style.display='none';
							    	 				
								    	 		    
								    	 		      flag = 'false';
								    	 		      $("#div"+formName).show();
								    	 		      $("#div"+formName).html("Enter alphabets value for "+componentArray[k-k]+" .");	
								    	 		      return false;
								    	 		   }
								    	    }
								        
								         if(componentArray[k]=="AN")
								    	   {			        	 
								        	 var id2 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
								    	 	  		        	
								    	  	 if(id2=="")
								    	 		 {
								    	  			document.getElementById('waitlight').style.display='none';
						    	 					document.getElementById('waitfade').style.display='none';
						    	 							    	 		    
									    	  		flag = 'false';
								    	 		    $("#div"+formName).show();
								    	 		    $("#div"+formName).html("Fill the field of "+componentArray[k-k]+" .");	
								    	 		    return false; 
								    	 		 }
								    	 	  else if(id2.match(alfanumeric))
								    	 		 {		    	 		    
								    	 		    $("#div"+formName).hide();
								    	 		 }
								    	 	  else
								    	 		  {
								    	 		      document.getElementById('waitlight').style.display='none';
							    	 				  document.getElementById('waitfade').style.display='none';
							    	 				
								    	 		      flag = 'false';
								    	 		      $("#div"+formName).show();
								    	 		      $("#div"+formName).html("Enter alphanumeric value for "+componentArray[k-k]+" .");	
								    	 		      return false;
								    	 		  }
								    	   }
								         
								         
								         if(componentArray[k]=="ASC")
								    	   {	
								        	 				        	 			        	 
								        	 var id2 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
								    	 	  		        	
								    	  	 if(id2=="")
								    	 		 {
								    	  			document.getElementById('waitlight').style.display='none';
						    	 					document.getElementById('waitfade').style.display='none';
						    	 							    	 		    
									    	  		flag = 'false';
								    	 		    $("#div"+formName).show();
								    	 		    $("#div"+formName).html("Fill the field of "+componentArray[k-k]+" .");	
								    	 		    return false; 
								    	 		 }
								    	  	  else if(id2.indexOf(",")>0)			    	 		 
								    	 		 {		  
								    	  			 document.getElementById('waitlight').style.display='none';
						    	 				 	 document.getElementById('waitfade').style.display='none';
						    	 				
									    	  		  flag = 'false';
								    	 		      $("#div"+formName).show();
								    	 		      $("#div"+formName).html("Comma are not allowed for "+componentArray[k-k]+" .");	
								    	 		      return false;
								    	 		 }
								    	  	 else if(id2.indexOf("&")>0)			    	 		 
								    	 		 {		   
								    	  		      document.getElementById('waitlight').style.display='none';
						    	 				 	  document.getElementById('waitfade').style.display='none';
						    	 				
									    	  		  flag = 'false';
								    	 		      $("#div"+formName).show();
								    	 		      $("#div"+formName).html(" '&' are not allowed for "+componentArray[k-k]+" .");	
								    	 		      return false;
								    	 		 }
								    	 	  else
								    	 		  {
								    	 		     
								    	 		  }
								    	   }	
								    }
							    
							    
			                   }else{
			                	   
			                	        
			                	        // After six components filled values are optional 
			                	        
					                	 for(var k=0;k<componentArray.length;k++)
					     				  {	    			    	
					     			    	   if(componentArray[k]=="N")
					     				    	   {			        	  
					     					    	  	   var id = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	  	 		    	 			    	 	
					     					    	 	   			    	  	
					     					    	 	   if(id=="")
					     					    	 		 { 
					     					    	 		    /* document.getElementById('waitlight').style.display='none';
					     				    	 				document.getElementById('waitfade').style.display='none';
					     				    	 				
					     					    	 		    flag = 'false';
					     					    	 		    $("#div"+formName).show();
					     					    	 		    $("#div"+formName).html("Fill the field of "+componentArray[k-k]+" .");	
					     					    	 		    return false;  */
					     					    	 		 } 
					     					    	 	   else if(id.match(numeric))  
					     					    	         {  			    	 		    
					     					    	 		    $("#div"+formName).hide();		    	           
					     					    	         } 
					     					    	       else  
					     					    	         { 
					     					    	    	    document.getElementById('waitlight').style.display='none';
					     				    	 				document.getElementById('waitfade').style.display='none';
					     				    	 				
					     					    	    	   	flag = 'false';
					     					    	    	   	$("#div"+formName).show();
					     					    	 		   	$("#div"+formName).html("Enter numeric value for "+componentArray[k-k]+" .");
					     					    	 		   	return false;
					     					    	         } 
					     			    	         }
					     				    	
					     				    	if(componentArray[k]=="A")
					     				    	   {			        	 
					     				    	  	   var id1 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
					     				    	 	   		    	  	  
					     				    	  	    if(id1=="")
					     				    	 		 {
					     				    	  	    	/* document.getElementById('waitlight').style.display='none';
					     			    	 				document.getElementById('waitfade').style.display='none';
					     			    	 				
					     				    	  	    	flag = 'false';
					     				    	 		    $("#div"+formName).show();
					     				    	 		    $("#div"+formName).html("Fill the field of "+componentArray[k-k]+" .");	
					     				    	 		    return false;  */
					     				    	 		 }
					     				    	 	    else if(id1.match(alfabet))
					     				    	 		 {
					     				    	 	    	$("#div"+formName).hide();
					     				    	 		 }
					     				    	 	    else
					     				    	 		   {
					     				    	 	    		document.getElementById('waitlight').style.display='none';
					     			    	 					document.getElementById('waitfade').style.display='none';
					     			    	 				
					     				    	 		    
					     				    	 		      flag = 'false';
					     				    	 		      $("#div"+formName).show();
					     				    	 		      $("#div"+formName).html("Enter alphabets value for "+componentArray[k-k]+" .");	
					     				    	 		      return false;
					     				    	 		   }
					     				    	    }
					     				        
					     				         if(componentArray[k]=="AN")
					     				    	   {			        	 
					     				        	 var id2 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
					     				    	 	  		        	
					     				    	  	 if(id2=="")
					     				    	 		 {
					     				    	  			/* document.getElementById('waitlight').style.display='none';
					     		    	 					document.getElementById('waitfade').style.display='none';
					     		    	 							    	 		    
					     					    	  		flag = 'false';
					     				    	 		    $("#div"+formName).show();
					     				    	 		    $("#div"+formName).html("Fill the field of "+componentArray[k-k]+" .");	
					     				    	 		    return false;  */
					     				    	 		 }
					     				    	 	  else if(id2.match(alfanumeric))
					     				    	 		 {		    	 		    
					     				    	 		    $("#div"+formName).hide();
					     				    	 		 }
					     				    	 	  else
					     				    	 		  {
					     				    	 		      document.getElementById('waitlight').style.display='none';
					     			    	 				  document.getElementById('waitfade').style.display='none';
					     			    	 				
					     				    	 		      flag = 'false';
					     				    	 		      $("#div"+formName).show();
					     				    	 		      $("#div"+formName).html("Enter alphanumeric value for "+componentArray[k-k]+" .");	
					     				    	 		      return false;
					     				    	 		  }
					     				    	   }
					     				         
					     				         
					     				         if(componentArray[k]=="ASC")
					     				    	   {	
					     				        	 				        	 			        	 
					     				        	 var id2 = document.getElementById("comp"+standard+componentArray[k-k]).value;		    	 			    	 			    	 	   		    	  	     	 			    	 			    	 	   
					     				    	 	  		        	
					     				    	  	 if(id2=="")
					     				    	 		 {
					     				    	  			/* document.getElementById('waitlight').style.display='none';
					     		    	 					document.getElementById('waitfade').style.display='none';
					     		    	 							    	 		    
					     					    	  		flag = 'false';
					     				    	 		    $("#div"+formName).show();
					     				    	 		    $("#div"+formName).html("Fill the field of "+componentArray[k-k]+" .");	
					     				    	 		    return false;  */
					     				    	 		 }
					     				    	  	  else if(id2.indexOf(",")>0)			    	 		 
					     				    	 		 {		  
					     				    	  			 document.getElementById('waitlight').style.display='none';
					     		    	 				 	 document.getElementById('waitfade').style.display='none';
					     		    	 				
					     					    	  		  flag = 'false';
					     				    	 		      $("#div"+formName).show();
					     				    	 		      $("#div"+formName).html("Comma are not allowed for "+componentArray[k-k]+" .");	
					     				    	 		      return false;
					     				    	 		 }
					     				    	  	 else if(id2.indexOf("&")>0)			    	 		 
					     				    	 		 {		   
					     				    	  		      document.getElementById('waitlight').style.display='none';
					     		    	 				 	  document.getElementById('waitfade').style.display='none';
					     		    	 				
					     					    	  		  flag = 'false';
					     				    	 		      $("#div"+formName).show();
					     				    	 		      $("#div"+formName).html(" '&' are not allowed for "+componentArray[k-k]+" .");	
					     				    	 		      return false;
					     				    	 		 }
					     				    	 	  else
					     				    	 		  {
					     				    	 		     
					     				    	 		  }
					     				    	   }	
					     				    }
			                   }
			    	      	     
			  }
				
			
			for(var j=0;j<eachDocHir.length;j++)
			{		   		
				var eachcomponent = eachDocHir[j];
			    var componentArray = eachcomponent.split(":");		
			    
			   if(componentArray[1]=="dd")	
				   {
				      if(document.getElementById("comp"+standard+componentArray[0]).value=="Select Category")
				    	  {				    	     
	    	 		    	 document.getElementById('waitlight').style.display='none';
	    	 				 document.getElementById('waitfade').style.display='none';
	    	 				
					    	  flag = 'false';
		    	 		      $("#div"+formName).show();
		    	 		      $("#div"+formName).html("Select a category for  "+componentArray[0]+" .");	
		    	 		      return false;
				    	  }
				      else
				    	  {
				    	  	  $("#div"+formName).hide();
				    	  }
				   }
			}
				 
			if(flag=='true')
				{
						
				var formData = new FormData($("#"+formName)[0]);
						
			    $.ajax({
			        url: "Academic.html?standardname="+standardName+"",
			        type: 'POST',
			        data: formData,
			        //async: false,
			        success: function (data) {          
			            
			        	if(data=='success')
			        		{
				        		document.getElementById('waitlight').style.display='none';
					    		document.getElementById('waitfade').style.display='none';
		  		        	    
							    document.getElementById('sociallight').style.display='block';
							    document.getElementById('socialfade').style.display='block';
							    document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
							    $("#socialdisplaySharedDocDiv").html("Your Documents has been saved successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\" /> </div>");					        	     						        	
			        		}
			        	else
			        		{
				        		document.getElementById('waitlight').style.display='none';
					    		document.getElementById('waitfade').style.display='none';
		  		        	    
					    		alert("There mighty be some problem occured so please try again later !");			        	
					        	window.location = "getacademicdetails.html";
			        		}
			        			        	
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });
		      }
	
     }   /* End of If(Save) */
}



</script>


     <script type="text/javascript">
            
				function submitForHistory(msg) 
				{					
					window.location = "academicHistorydetail.html?id="+msg;					
				}
				
	</script>


<script>
$(document).ready(function(){
    $('.MyAction').live("click",function () {
    	
        var id = $(this).attr('id');
        
    });
});

</script>


<!-- Code for reselect Documents -->

<script> 


var checkboxes = $("input[type='checkbox']"),
submitButt = $("input[type='submit']");

checkboxes.click(function() {
submitButt.attr("disabled", !checkboxes.is(":checked"));
});

</script>
 
<style type="text/css"> 
#panel,#flip
{
padding:5px;
text-align:left;
background-color:white;
border:solid 1px #c3c3c3;
width:328px;
text-align: center;
}
#panel
{
  display: none;
    padding-bottom:15px;
    padding-top: 0;
    text-align: left !important;
    width: 80%;
}
.adddocpanel{
 display: inline-table !important;
    height: auto;
    padding-bottom: 15px;
    padding-top: 0;
    text-align: left !important;
    width: 100%;
    }
#panel li{
    cursor: pointer;
    float: left;
    padding:5px;
      width: 30%;
}
.adddocpanel li span{
 padding-left: 3px;
    padding-top: 2px;
}
.checkbox{
    vertical-align: sub;
}
</style>




<!-- End of code for reselect Documents -->
 
 <!-- End of Script and CSS for Academic Documents -->
 <script>
 
 function cleanGarbageRecords()
 {
	
	 $.ajax({
	        url: "deleteAcademicfileRecord.html",
	        type: 'POST',		        		        
	        success: function (data) {          
	            
	        	   
	         },		        
	    });	
 }
 
 
 
 /* Main Header Actions  */
 
 function gotoProfile()
 {
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoMyDocument()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }
 
 function gotoSharingManager()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }
 
 function gotoStore()
 {	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoAlerts()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
 }
 
 function gotoPlans()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
 }
 
 function gotoSetings()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}

function ChangeFlipStatus()
{
	var dropId = document.getElementById("flipedId").value;
	if(dropId=="0")
		{
		   document.getElementById("flip").style.color = "#05b7f5";
		   $("#flip").html("Select Academics");
		   document.getElementById("flipedId").value = "1";
		}
	else
		{
		   document.getElementById("flip").style.color = "";
		   $("#flip").html("Select Academics");
		   document.getElementById("flipedId").value = "0";
		}
}

function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide(); 
	 document.getElementById("inputString").value = "";	 
}	

function visibleAccordian(accordianId)
{
   $("."+accordianId).slideToggle("slow");
}


 </script>
 
  
<style type="text/css">
 
.message 
{
    /* background-color: #ccc; */
    border-radius: 12px;
    left: 8%;
    margin: 1px;
    padding: 22px;
    position: fixed;
    top: 73%;
    width: 200px;
    color:#00b6f5;    
}

</style>
</head>

<body onload="cleanGarbageRecords(),hideAddvertise()" onclick="hidesearchDiv()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocumentsfocus"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 
 
 		<!-- Login Starts Here -->
 		  
 		  
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
            
            
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"> </span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
             
        
					
		
					<div class="leftside">
					
							<div id="textBox">
									<div id="button">
									  <ul>
									     <li id="Hi-B-1"><a href="myDocumentsMain.html"  id="Hi-B-1" ><span>KYC Documents</span></a></li>
										 <li id="academic_document"><a href="getacademicdetails.html"  id="academic_documentactive" ><span>Academic Documents</span></a></li>															
										 <li id="employee_document"><a href="employeeDetails.html"  id="Hi-B-4"><span>Employment Documents</span></a></li>
										 <li id="financial_document"><a href="search_financialDoc.html"  id="Hi-B-3" ><span>Financial Documents</span></a></li>
									 </ul>
								   </div>    		
							</div>
					
					
					       <div class="message">
							<h4><i>Contact us if document type not found in the above list.</i></h4>
							</div>
					
					
					 </div>
					
					<div class="rightside">
					
					  
					  <!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

									

					<div id="ProfileHeadingDiv" style="display: block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>

					
							        		
					   <!-- Advertisment Div for Entire Page  -->
					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
							    <span><%=name%>'s Academic Documents</span>								   
							</h2>
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
					        
					        
		<br>
	    <div id="center-body-div">
					   
          
              <span class="reselect button">
	         	          
	          <% List<String> ls =(List)request.getAttribute("comp");    /* List of acdemic components givn by admin */

                 String oldComp = (String)request.getAttribute("oldSelect"); /* If a user not added acdimic details it will return null   */
               
               %>
<%

boolean match = true;

if(!oldComp.equals("")) 											 /* If is null then no need to check for furthr process. */
{
	if(oldComp.split(",").length==ls.size())
	{
		match = false;
	} 
}


/* If match = false means no.of component given by admin and no.of component added by user is same then drop down will not appear. */

/* If match = true means no.of component given by admin and no.of component added by user is less so drop down will appear to add the component which are not been added. */


if(match==true)
{ 
%>

<div align="center">

<input type="hidden" value="0"  id="flipedId"/>

<div id="flip" onclick="ChangeFlipStatus()">Select Academics</div>

<div id="panel">
<div class="adddocpanel">
<ul>
<%
for(int j=0;j<ls.size();j++)
{  	      	
      boolean status = true;
           
      if(oldComp!=null)
      {
    	  String oldarr[] = oldComp.split(",");
      
      for(int i=0;i<oldarr.length;i++)
      {
    	  if(ls.get(j).equals(oldarr[i]))
    	  {     		 
    		  status = false;    	      		
    	  }
      }     
      if(status){
%>     
    
       <li> <input  type="checkbox" name="click" class="checkbox" value="<%=ls.get(j)%>" /><%=ls.get(j)%> </li>
      
      <%} } else{ %>
      
        <li> <input  type="checkbox" name="click" class="checkbox" value="<%=ls.get(j)%>" /><%=ls.get(j)%> </li>
<%}}
%>
</ul>


</div>

<div id="proceedtbutton">
<input type="submit"  width="20" name="Proceed" id="proceedButtonId" value="Add" onclick="chooseAcaIds()"/><br>
</div>

</div>
</div> 
<%}%>	 

         
	         <!-- Code for reselect Category -->
	         </span>
	
	
	
<%if(oldComp.length()!=0) {%>	       
	        
	        
	     <c:set var="comp" value="${accordNames}"/>
         <c:set var="NeworView" value="${accDataAndHierarchy}"/>
         
         <% 
            
            String pannel = (String)pageContext.getAttribute("comp");
            String hierarchyWithData =(String)pageContext.getAttribute("NeworView");          
            
            String panelArray[] = pannel.split(",");
            String hierarchyWithDataArray[] = hierarchyWithData.split("#");             
            
         %>      
             
             	          
			<div id="panel-1" style="width:100%;">
			   			  			   
			    <% for(int h=0;h<hierarchyWithDataArray.length;h++) { %>    <!-- This is the 1st for loop for docHierarchy Data with components -->
			    			
			    			
			    				    			    								         				         				            
				            <h3 align="left" onclick="visibleAccordian('<%=panelArray[h].replaceAll(" ","")%>accordianClass')"><%=panelArray[h]%></h3>
				            
										                    
			                    <div id="kycdocumentpage" class="<%=panelArray[h].replaceAll(" ","")%>accordianClass" style="display:none;margin-left: 0px ! important;">  	<!-- start of kycdocumentpage -->														
			                       
			                        <a id="editregistrationhelp" href="accademic_details_help.html" target="_blank"><p class="askhelp" tittle="Help For Basic Details"> </p></a>
				                         
				                         <div class="hscregister">      <!-- Division for Left side Hierarchy other component with data is in left side -->
				                         
					                      <% String formName = panelArray[h].replaceAll(" ",""); %>
					                      <%  String academicCompData[] = hierarchyWithDataArray[h].split("/");
					                          String  acaHierarchy[] = academicCompData[0].split(",");
					                                       
					                          String docName = academicCompData[2];
					                      %>
					                                      	
					                         	
	<!-- Button division for academic details -->
					                         	
					                         	
							                       <div class="hscbuttondiv">
								                        
								                        <ul>
									                        <li>
									                           	
									                           	<%if(!academicCompData[1].equals("No Data")) { %>
								                                
									                             	   <input class="historyinner_button" type="button" value="History" name="History" onclick="submitForHistory('<%=panelArray[h]%>')"/>
									                             	   
									                             <%} %>
									                             	                           		                            
								                                <%if(!academicCompData[1].equals("No Data")) { %>
								                                
								                                       <input class="editinner_button" type="button" id="buttonIdUpper<%=formName%>" value="Edit" onclick="ValidateComp('<%=formName%>','<%=panelArray[h]%>')" />	
								                                <%} %>
								                                
								                                 <%if(academicCompData[1].equals("No Data")) { %>
								                                
								                                       <input class="saveinner_button" type="button" id="buttonIdUpper<%=formName%>" value="Save" onclick="ValidateComp('<%=formName%>','<%=panelArray[h]%>')" />	
								                                <%} %>
								                                			
								                                
									                         </li>
								                        </ul>  
								                            
							                     </div>  
					                         	
					                         	<!-- End of Button div for academic details -->
					                         	
					                         	
					                         	
					                         
					                         <div id="div<%=formName%>" class="error" style="display:none;margin-left:282px;margin-top:-13px;"></div> 
					                         	    			                       
					                          <form id="<%=formName%>" name="<%=formName%>" class="hscregister-ext"enctype="multipart/form-data"> 
					                           
					                               <p style="overflow: auto;">   
					                                     
					                                    
					                                        					                                      					                                      
					                                      <input type="hidden" id="acaHirarchy<%=formName%>" value="<%=academicCompData[0]%>" /> 
					                                      <input type="hidden" id="standard<%=panelArray[h]%>" value="<%=panelArray[h]%>" /> 
					                                      
					                                     <ul>
					                                     <%for(int c=0;c<acaHierarchy.length;c++) {           //  <!-- Hierarchy for loop to generate components -->
					                                     
					                                             boolean dataCondition = true;
					                                     %>         
					                                     
						                                       <li>
						                                           <% String eachComponent[] = acaHierarchy[c].split(":"); 
						                                          						                                           
						                                            if(eachComponent[1].equals("tb")){
						                                            	
						                                            %>
							                                                <%if(!academicCompData[1].equals("No Data")) {
							                                                	
									                                                    String diffData[] = academicCompData[1].split("-");
									                                                    String accData[] = diffData[1].split(",");
									                                                    
									                                                     for(int ad=0;ad<accData.length;ad++) 
									                                                         {									                                                    	 
									                                                    	         String labelWithData[] = accData[ad].split(":"); 
											                                                    	 if(labelWithData[0].equals(eachComponent[0]))  
											                                                    	 {
											                                                    		      dataCondition = false;
											                                                        	%>
											                                                           	
											                                                           	<%if(labelWithData[1].equals("Not filled")) {%>
											                                                           	
											                                                 			    <%=eachComponent[0]%>:<input type="text" id="comp<%=panelArray[h]%><%=eachComponent[0]%>" disabled="true" value="<%=labelWithData[1]%>"  name="<%=eachComponent[0]%>" maxlength="<%=eachComponent[3]%>" onkeyup="ValidateCompOnEach('<%=formName%>','<%=panelArray[h]%>')"/>						                                              
											                                                            
											                                                            <%}else{%>
											                                                              
											                                                                <%=eachComponent[0]%>:<input type="text" id="comp<%=panelArray[h]%><%=eachComponent[0]%>" disabled="true"  value="<%=labelWithData[1]%>" name="<%=eachComponent[0]%>" maxlength="<%=eachComponent[3]%>" onkeyup="ValidateCompOnEach('<%=formName%>','<%=panelArray[h]%>')"/>						                                              
											                                                            
											                                                            <%}%>
											                                                               
											                                                       <%} %>
									                                                       <%} %>									                                                       									                                                     
							                                                     
							                                                     <%}%>
							                                                     
						                                                     <% if(dataCondition) {%>
								                                                       
								                                                   <%=eachComponent[0]%>:<input type="text" id="comp<%=panelArray[h]%><%=eachComponent[0]%>" name="<%=eachComponent[0]%>" maxlength="<%=eachComponent[3]%>" onkeyup="ValidateCompOnEach('<%=formName%>','<%=panelArray[h]%>')"/>						                                              
								                                              
								                                             <%}%>
						                                               							                                               
						                                           <%} %>
						                                           
						                                           
						                                           
						                                          <% if(eachComponent[1].equals("dd")){
						                                            %>
						                                                <%if(academicCompData[1].equals("No Data")) {%>
						                                                
									                                             <%=eachComponent[0]%>:
									                                             <div class="styled-select">
									                                             <select id="comp<%=panelArray[h]%><%=eachComponent[0]%>" name="<%=eachComponent[0]%>" >
									                                                                   <option value="Select Category">Select Category</option>
									                                                                   <%for(int option=2;option<eachComponent.length;option++){ %>
									                                                                   		
									                                                                  
									                                                                   		<option value="<%=eachComponent[option]%>"><%=eachComponent[option]%></option>  
									                                                                   		    
									                                                                   <%} %>
									                                                                   </select>
									                                                                   </div>
						                                                     
						                                                  <%}else { %>
						                                                  
								                                                  <%=eachComponent[0]%>:
								                                                  <div class="styled-select">
								                                                  <select id="comp<%=panelArray[h]%><%=eachComponent[0]%>" disabled="disabled" name="<%=eachComponent[0]%>" >
											                                                                  
											                                                                    <%String optionValue= ""; %>
											                                                                     
											                                                                      <%	
											                                                                            String diffData[] = academicCompData[1].split("-");
									                                                                        			String accData[] = diffData[1].split(",");
									                                                                        			for(int ad=0;ad<accData.length;ad++) 
																                                                        {									                                                    	 
															                                                    	         String labelWithData[] = accData[ad].split(":"); 
																	                                                    	 if(labelWithData[0].equals(eachComponent[0]))  
																	                                                    	 {
																	                                                    		 optionValue = labelWithData[1];
																	                                                    	 }
																                                                        }
									                                                                        			 
										                                                                   	       %>
												                                                                  										                                                                  
												                                                               
												                                                                   <option value="Select Category">Select Category</option>
												                                                                   <%for(int option=2;option<eachComponent.length;option++){%>
												                                                                   
												                                                                    	  <%if(optionValue.equals(eachComponent[option])){%>
												                                                                               <option value="<%=eachComponent[option]%>" selected="selected"><%=eachComponent[option]%></option> 
												                                                                           <%}else   {%> 
												                                                                               <option value="<%=eachComponent[option]%>"><%=eachComponent[option]%></option> 
												                                                                           <%} %>    
												                                                                   <%}%>
												                                                                   
											                                                                   </select>
								                                                  </div>
						                                                   <%} %>                 
						                                           <%} %>
						                                         
						                                       </li>   
					                                      <%} %>   <!-- End of Hierarchy for loop to generate components -->
					                                      </ul>
					                               </p>
					                  
					                        
					                          </form>  
					                        					                       					                          
					                     </div>      <!-- End of Division for Left side Hierarchy other component with data is in left side -->
					
				                         	<div class="documenttitle">
				                         <span></span>
					       						<h1 >
					       							
				                         	Documents
				                         	</h1>
				                         	</div>

											<%
											int noOfFileBrowse = 0;   
											for(int c=0,j=0;c<acaHierarchy.length;c++) {
												
												String eachComponent[] = acaHierarchy[c].split(":");

															if (eachComponent[1].equals("fb")) {
																
																noOfFileBrowse = j++;
															}
														}
											%>

                                            <%if(noOfFileBrowse>3){ %>
                                            
                                                  <div class="hscregisterdocument" id="documentHover">     <!-- Division for right side Hierarchy file component with data  -->
									  
                                            <%}else{ %>
                                            
                                                  <div class="hscregisterdocument">     <!-- Division for right side Hierarchy file component with data  -->
									  
                                            <%} %>
											
									              <ul>
									               <%
									               String editDivNam = "";
									               
									               if(docName.equals("No Data")) {
									            	   
									                  for(int c=0;c<acaHierarchy.length;c++) {%>          <!-- Hierarchy for loop to generate file components -->
					                                     
						                                       
						                                       <% String eachComponent[] = acaHierarchy[c].split(":"); 
						                                             
						                                            
						                                             
						                                            if(eachComponent[1].equals("fb")){
						                                            	
						                                            	 String removeSpace = eachComponent[0];
						                                            	 
						                                            %>
						                                             <% String fileUploadFormName = panelArray[h].replaceAll(" ", "") + removeSpace.replaceAll("\\s+","");	%>			                                           
						                                              
						                                              <%editDivNam += "Editbutton"+fileUploadFormName + "##"; %>
						                                              
						                                              <li class="panviewinner_acad">
						                                              
							                                                     <div id="view<%=c%><%=fileUploadFormName%>">
									                                                 
									                                                 <b><u><%=eachComponent[0]%></u></b>       
									                                                 <img src="resources/images/noDocumentFound.png"   width="150" height="100" /></img>
									                                      
										                                                  <div id="Editbutton<%=fileUploadFormName%>" style="display:none;float:right;">
																	                                         	
																	                          <button class="editacod_button" onclick="updateAcademicDocument('<%=c%>','<%=fileUploadFormName%>')"></button>
																	                   
																	                      </div>
									                                                
									                                              </div> 
								                                             
								                                             
								                                              
								                                               <div id="edit<%=fileUploadFormName%>" style="height:auto;width:240;margin-top: -11px;display:none;">	
										                                         
											                                         <form id="<%=fileUploadFormName%>" name="<%=fileUploadFormName%>" style="height:166px;width:261px;margin-top:10px;">
										                                              
											                                              <b><u><%=eachComponent[0]%></u></b> Description : 
											                                              <br>
											                                              <textarea id="desc<%=c%><%=fileUploadFormName%>" maxlength="80" name="description" placeholder="Description in 80 Character"></textarea>
											                                              Document:<input type="file" id="file<%=c%><%=fileUploadFormName%>" name="uploadcert" />
											                                             				                                              
										                                              </form> 
									                                              
										                                              <div id="buttonDiv<%=fileUploadFormName%>" style="height:20px;width:261px;">
												                                      			
												                                      		  <button class="cancelbule_button " onclick="cancelUploadAcademicDoc('<%=c%>','<%=fileUploadFormName%>')"></button>
													                                          							                                             
											                                             	  <button class="uploadimgbutton" onclick="uploadAcademicDoc('<%=fileUploadFormName%>','<%=panelArray[h]%>','<%=eachComponent[0]%>','<%=c%>')"  ></button>
													                                     	    								                                              
												                                      </div>
											                                      
										                                        </div>
              
            															  </li> 
						                                       <%} %>
						                                           						                                        
						                                         
					                                      <%}           /*  <!-- End of Hierarchy for loop to generate components --> */
									                  
									                  }
									               else if(docName.equals("No Document"))
									               {
									            	   for(int c=0;c<acaHierarchy.length;c++) {%>          <!-- Hierarchy for loop to generate file components -->
					                                     
				                                       
				                                       <% String eachComponent[] = acaHierarchy[c].split(":"); 
				                                             
				                                            			                                         
				                                            if(eachComponent[1].equals("fb")){
				                                            	
				                                            	 String removeSpace = eachComponent[0];
				                                            	 
				                                            %>
				                                             <% String fileUploadFormName = panelArray[h].replaceAll(" ", "") + removeSpace.replaceAll("\\s+","");	%>			                                           
				                                              
				                                              <%editDivNam += "Editbutton"+fileUploadFormName + "##"; %>
				                                              
				                                              <li class="panviewinner_acad">
				                                              
					                                                     <div id="view<%=c%><%=fileUploadFormName%>">
							                                                 
							                                                 <b><u><%=eachComponent[0]%></u></b>      
							                                                  
							                                                 <img src="resources/images/noDocumentFound.png"   width="150" height="100" /></img>
							                                      
								                                                  <div id="Editbutton<%=fileUploadFormName%>" style="display:block;float:right;">
															                                         	
															                          <button  class="editacod_button" onclick="updateAcademicDocument('<%=c%>','<%=fileUploadFormName%>')"></button>
															                           
															                      </div>
							                                                
							                                              </div> 
						                                             
						                                             
						                                              
						                                               <div id="edit<%=fileUploadFormName%>" style="height:auto;margin-top:10px;display:none;width: 240px;">	
								                                         
									                                         <form id="<%=fileUploadFormName%>" name="<%=fileUploadFormName%>" style="height:166px;width:242px;margin-top:10px;">
								                                              
									                                              <b><u><%=eachComponent[0]%></u></b> Description : 
									                                              <br>
									                                              <textarea id="desc<%=c%><%=fileUploadFormName%>" maxlength="80" name="description" placeholder="Description in 80 Character"></textarea>
											                                              Document:<input type="file" id="file<%=c%><%=fileUploadFormName%>" name="uploadcert" />
											                                     
									                                              <%-- <input type="button" value="Upload"  onclick="uploadAcademicDoc('<%=fileUploadFormName%>','<%=panelArray[h]%>','<%=eachComponent[0]%>','<%=c%>')" />
									                                               --%>					                                              
								                                              </form> 
							                                              
								                                              <div id="buttonDiv<%=fileUploadFormName%>" style="height:20px;width:242px;">
										                                      			
										                                      		  <button class="cancelbule_button" onclick="cancelUploadAcademicDoc('<%=c%>','<%=fileUploadFormName%>')"></button>
											                                          							                                             
									                                             	  <button class="uploadimgbutton" onclick="uploadAcademicDoc('<%=fileUploadFormName%>','<%=panelArray[h]%>','<%=eachComponent[0]%>','<%=c%>')" ></button>
											                                     	    								                                              
										                                      </div>
									                                      
								                                        </div>
      
    															  </li>  
				                                       <%} %>
				                                           						                                        
				                                        
			                                      <%}           /*  <!-- End of Hierarchy for loop to generate components --> */
									               }
									                  else {    //<!-- End of If condition to  -->
					                                      
							                                      for(int c=0;c<acaHierarchy.length;c++) {%>          <!-- Hierarchy for loop to generate file components -->
							                                     
								                                       
								                                       <% String eachComponent[] = acaHierarchy[c].split(":"); 
								                                             
								                                            if(eachComponent[1].equals("fb")){
								                                            	
								                                            	String removeSpace = eachComponent[0];
								                                            	
								                                            %>
								                                             <% String fileUploadFormName = panelArray[h].replaceAll(" ", "") + removeSpace.replaceAll(" ", "");	%>			                                           
								                                             
								                                             <%editDivNam += "Editbutton"+fileUploadFormName + "##"; %>
								                                             
								                                             <li class="panviewinner_acad">
								                                             
								                                             <div id="edit<%=fileUploadFormName%>" style="height:auto;width:242px;margin-top:10px;display:none;">	
								                                             								                                              
									                                             <form id="<%=fileUploadFormName%>" name="<%=fileUploadFormName%>" >
									                                              
									                                              <p><u><%=eachComponent[0]%></u></p> Description : 
									                                              <br>
									                                             
									                                              <textarea id="desc<%=c%><%=fileUploadFormName%>" maxlength="80" name="description" placeholder="Description in 80 Character"></textarea>
											                                              Document:<input type="file" id="file<%=c%><%=fileUploadFormName%>" name="uploadcert" />
									                                              <br>
									                                              								                                              								                                              					                                              
									                                              </form> 
									                                              
									                                              <div id="buttonDiv<%=fileUploadFormName%>" style="height:20px;width:242px;">
									                                             	 
									                                             	 <button class="cancelbule_button" onclick="cancelUploadAcademicDoc('<%=c%>','<%=fileUploadFormName%>')"></button>
										                                              
										                                              <button class="uploadimgbutton" onclick="uploadAcademicDoc('<%=fileUploadFormName%>','<%=panelArray[h]%>','<%=eachComponent[0]%>','<%=c%>')" ></button>
										                                              
									                                              </div>
								                                              
								                                              </div>
								                                              
								                                              <%String docNameArray[] = docName.split(",");
								                                               
								                                                boolean fileCondition = true;
								                                                for(int dName=0;dName<docNameArray.length;dName++) {
								                                                 
								                                            	  if(docNameArray[dName].contains(eachComponent[0])) {
								                                            		  
								                                            		  fileCondition = false;
								                                                %>
								                                                        
								                                                        <div id="view<%=c%><%=fileUploadFormName%>" >
								                                                        <b><u><%=eachComponent[0]%></u></b>
								                                                        
								                                                        
										                                                 <%if(docNameArray[dName].contains("pdf")) {%>   
										                                                    
										                                                         <img src="resources/images/PNA_PDF.png" width="150" height="100" /></img>
										                                                        
														                                         <br>
										                                                         										                                                         
														                                         <a class="mydocumentdownloadpan" href="downloadAcademic.html?fileName=<%=docNameArray[dName]%>" ></a>
														                                         
														                                         <div id="Editbutton<%=fileUploadFormName%>" style="display:none;float:right;">
														                                         	<button class="editacod_button" onclick="updateAcademicDocument('<%=c%>','<%=fileUploadFormName%>')"></button>
														                                         </div>
										                                                         
														                                   <%}else { %>
														                                        
														                                        <%--  <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+docNameArray[dName]+"&docCategory=ACADEMIC_DOC"%>" width="150" height="100" /></img> --%>
										                                                         <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+docNameArray[dName]+"&docCategory=ACADEMIC_DOC"%>" width="150" height="100" /></img>
														                                         <br>
										                                                         
										                                                         <a onclick="viewDocumentInPopUp('<%=docNameArray[dName]%>')"><button class="mydocumentviewpan"></button> </a>
														                                         <a href="downloadAcademic.html?fileName=<%=docNameArray[dName]%>" ><button class="mydocumentdownloadpan"></button></a>
														                                        
														                                         <div  id="Editbutton<%=fileUploadFormName%>" style="display:none;float:right;">
														                                         	<button class="editacod_button" onclick="updateAcademicDocument('<%=c%>','<%=fileUploadFormName%>')"></button>
														                                         </div>
														                                        
														                                   <%}%>
														                                          
												                                         </div>
								                                                    <%} %>
								                                              <%} %>
								                                              
								                                             <%if(fileCondition){%>
								                                              
								                                               <div id="view<%=c%><%=fileUploadFormName%>" >
								                                                 
								                                                 <b><u><%=eachComponent[0]%></u></b>       
								                                                 <img src="resources/images/noDocumentFound.png"   width="150" height="100" /></img>
								                                      
									                                                 <div id="Editbutton<%=fileUploadFormName%>" style="display:none;float:right;">
															                                   <button class="editacod_button" onclick="updateAcademicDocument('<%=c%>','<%=fileUploadFormName%>')"></button>
															                         </div>
								                                                
								                                               </div> 
								                                               
								                                              <%}%>
								                                              
								                                               </li>   
								                                       <%} %>
								                                           						                                        
								                                      
							                                      <%}     
					                                      
					                                      } %>
									              </ul>
									   
									     </div>                               <!-- Division for right side Hierarchy file component with data -->
						           	    
						           	       <div class="hscbuttondiv">
						                        
						                        <ul>
							                        <li>
							                            <%if(!editDivNam.equals("")){%>
							                            
							                        	<%editDivNam = editDivNam.substring(0,editDivNam.length()-2);%>		
							                        	
							                        	<%} %>
							                        		   <input type="hidden"  id="editHidden<%=formName%>"  value="<%=editDivNam%>" />
							                        				                           		                            
						                                <%if(!academicCompData[1].equals("No Data")) { %>
						                                
						                                       <input class="editinner_button" type="button" id="buttonId<%=formName%>" value="Edit" onclick="ValidateComp('<%=formName%>','<%=panelArray[h]%>')" />	
						                                <%} %>
						                                
						                                 <%if(academicCompData[1].equals("No Data")) { %>
						                                
						                                       <input class="saveinner_button" type="button" id="buttonId<%=formName%>" value="Save" onclick="ValidateComp('<%=formName%>','<%=panelArray[h]%>')" />	
						                                <%} %>
						                                			
						                                <%if(!academicCompData[1].equals("No Data")) { %>
						                                
							                             	   <input class="historyinner_button" type="button" value="History" name="History" onclick="submitForHistory('<%=panelArray[h]%>')"/>
							                             	   
							                             <%} %>
							                             
							                         </li>
						                        </ul>  
						                            
					                        </div>  
						           	    						           	    						           	           	      				                          
			                    </div>      <!-- End of kycdocumentpage -->		
			                				 				   					  
	        
	                        <%} %>  <!-- End of 1st for loop -->            
	                                                     
	           
			          </div>   <!-- End of panel-1 division  -->
			
		<%} %>	
		
	              </div>  <!-- End of Center Body Div -->
					        
					        
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

	
	
	
	
	    
</div> <!--end of right div  -->

	</div>

</div>


					
			
					
					
					
					
		<%
		ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
		String version=resource.getString("kycversion");
		String versionDate=resource.getString("kyclatestDate");
		%>			
					
					
					
			
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
					  		    
		      <div class="kycloader1">
						
						 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
									
              </div>
              
                
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->
	
	
	
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	
	<!-- Document Pop up Div -->
	
	<div id="kycDocPopUplight">
			<div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:0; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF Document Pop up Div -->	

	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->
	
	<!-- report Div -->
	
	<div id="sociallight" style="display:none;">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->
	
	
</body>
</html>
     	