<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Utilizer</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 
<%@ page import="java.text.SimpleDateFormat"%>  

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">

<link rel="stylesheet" href="resources/kyc_css/tableAdmin.css" type="text/css" />

  <link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/style_common.css">
<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />


<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
 
<script src="resources/texteditor/nicEdit.js" type="text/javascript"></script>


<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

 
<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />
 
 
<style>


#waitfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);   
}

#waitlight
{
    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002; 
}

#sessionfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#sessionlight
{
    background: none repeat scroll 0 0 #fff;
    border: 2px solid #00b6f5;
    display: none;
    height: 46px;
    left: 50%;
     border-radius: 5px;
    margin-left: -272px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
      position: fixed;
    top: 50%; 
    width: 564px;
    z-index: 1002;
     right: -10px;  
}

#aboutUsfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#aboutUslight
{
    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    border:2px solid #00b5f6;
    display: none;
    height: auto;
    left: 42%;
    margin-left: -378px;
    margin-top: -220px;   
    position: fixed;
    top: 50%;
    width: 1000px;
    z-index: 1002;
}


#profilePhotoPopfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#profilePhotoPoplight{
    border: 2px solid #05b7f5;
    /* background: none repeat scroll 0 0 #fff; */
    background: none repeat scroll 0 0 #333;
    border-radius: 10px;
    display: none;
    height: 550px;
    left: 40%;
    margin-left: -132px;
    margin-top: -220px;
    position: fixed;
    top: 40%;
    width: 550px;
    z-index: 1002;

}

</style>

<style>

#kycdivfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);   
}

#kycdivlight
{
    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 33%;
    width: 800px;
    z-index: 1002;
}

</style>
<script>


function profilePhotoPopfadeout()
{
	document.getElementById('profilePhotoPoplight').style.display='none';
	document.getElementById('profilePhotoPopfade').style.display='none';
}


function aboutUsfadeout()
{
	document.getElementById('aboutUslight').style.display='none';
	document.getElementById('aboutUsfade').style.display='none';
}


function fadeoutReport(action)
{	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		    window.location = "utimyProfile.html";
		}
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		       
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	   
	    });
	
}


function redirectToLoginIndividual()
{
	window.location = "utisignOut.html";	
}


 function openbrowse()
 { 	 
	 var obj = document.getElementById('openbrowse');
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	
	    }	  
	   	  
 }
 </script>

<script type="text/javascript">
			function resolveSrcMouseover(e) {
				alert("Hello mouse over");
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				if (node.nodeName != "UL") {
					node.style.fontWeight= "bold";
					showRollover(e, node.innerHTML);
				}
			}
			function resolveSrcMouseout(e) {
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				node.style.fontWeight = "normal";
				clearRollover(e);
			}
			function takeAction(e) {
				var node = e.srcElement == undefined ? e.target : e.srcElement;
									
				document.getElementById("DisplayInfo").innerHTML = "Clicked " + node.innerHTML;
				
				var id = node.getAttribute("id"); 
				if (id != null && id.indexOf("F") > -1) {
					if (node.innerHTML == "-") {
						node.innerHTML = "+";
						document.getElementById("EC" + id).style.display = "none";
					} else if (node.innerHTML == "+") {
						node.innerHTML = "-";
						document.getElementById("EC" + id).style.display = "block";
					}
				}
			}
</script>



<script type="text/javascript" src="resources/jscripts/tiny_mce/tiny_mce.js"></script>

<!-- Script for Text Area -->

<script>
tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",
		// Replace values for the template plugin
		template_replace_values : 
		{
		username : "Some User",
		staffid : "991234"
		}
	});
	
</script>

<!-- End of Script for Text Area -->


<script type="text/javascript">
	

function acceptDoc(id,date) {
		
	$.ajax({
		type : "Post",
		url : "validate/doc/viewing/via/allow/access/per/month.html",		
		success : function(response) {
				
			if(response=="withinLimit")
				
				{				
					$.ajax({
						type : "Post",
						url : "acceptDocs.html",
						data: "id=" +id+ "&date=" +date,
						success : function(response) {
				                 	
							alert(response);
							awaitedDocument();
							deleteUsers();
							
							},		
						});		
				}
			else
				{
					alert("As per your plan you can't accept/moved these Document,To Accept Migrate your Plan !");			
				}
				
		},
	});		
	
    }
	
	/* Script for view the documents */
	
	function viewDetailsDoc(kycid,time,index)
	{		
		
		$.ajax({
			type : "Post",
			url : "viewDetailsDocs.html",
			data: "kycid="+kycid+"&date="+time,
			success : function(response) {
                	
				$('#viewUsers').html(response);
				
				},		
			});	
		
	}
	
	
	//Script for Profile
	
	function viewUsers()
	{
		$.ajax({
			type : "Post",
			url : "viewUtiProfile.html",
			success : function(response) {
                	
					$('#viewUsers').html(response);
					$("#titleBar").html("<h2><span>View Profile</span></h2>");
				},
			error : function(e) 
			   {				
				  alert('Error: ' + e);				
			   }				
		  });	
	}
			
	function editUtiUsers()
	{
		$.ajax({
			type : "Post",
			url : "editUtiProfile.html",
			success : function(response) {
                	
				$('#viewUsers').html(response);
				$("#titleBar").html("<h2><span>Edit Profile</span></h2>");
				},
			error : function(e) {
				
				alert('Error: ' + e);
				
			}
			});	
	}
	
	
	function userHome()
	{
		window.open("UtiHome.html",true);			     			
	}
	
	
	//End of Script for Profile
	
</script>

<script>
var area1, area2;

function toggleArea1() {
	
    var buttonName = document.getElementById("htmlbutton").innerHTML; 
    
    var existDivValue = document.getElementById("myArea1").innerHTML;
    
    if(buttonName=="Edit")
    	{
	    	$("#htmlbutton").empty();
	    	$("#htmlbutton").html("Save");
	    	
	    	$("#myArea1").hide();
	    	$("#textareaId").show();
	    	
	    	tinyMCE.get("indaboutUs").setContent(existDivValue);	    	
	    	$("#seemore").hide();
    		    	
    	}
    
    if(buttonName=="Save")
    	{
	    	$("#htmlbutton").empty();
	    	$("#htmlbutton").html("Edit");
	    	
	        var divvalue = tinymce.get("indaboutUs").getContent();
	        	        
	        var formData = new FormData($("#textareaform")[0]);
	         
	        formData.append("textAreaContent",divvalue);
	        
	        $.ajax({
		    	
		    	type: "post",	   
		    	url: "saveutiAboutUs.html",	             
		    	data: "aboutUs="+divvalue,	 	    		       
		        success: function (response) {
		        	
		        
		        	$("#aboutus").fadeIn(500); 
		            $("#aboutus").fadeOut(5000);
		           
		            $("#textareaId").hide();	
		            $("#myArea1").show();
		            $("#myArea1").html(response);
		            
                    var data = response;
    	            
    	            $("#myArea1").show();
    	            	    	           
    	        	$("#myArea1").html(data);
    	        	
    	        	$("#textareaId").hide();
    	        	
    	        	var element = document.querySelector('#myArea1');
    		        
    	        	
    		        if( element.offsetHeight < element.scrollHeight) {
    		        	  
    		        	     $("#seemore").show();
    		        	}
    		        else
    		        	{
    		        	    $("#seemore").hide();
    		        	}
    		        
		            
		        	
		        },	        	        
		     });
	        
    	}
}


function getUtiAboutUs()
{			
	 $.ajax({
	    	
	    	type: "post",	   
	    	url: "getutiAboutUsInfo.html",	             	 	    		       
	        success: function (response) {
	        	 	     
	        	if(response!="No Data")
	        		{
		        		var data = response;
			            
				        $("#myArea1").html(data);	
				        
				        var element = document.querySelector('#myArea1');
				        
				        if( element.offsetHeight < element.scrollHeight) {
				        	  
				        	     $("#seemore").show();
				        	}
	        		}
		       	        		        		        	
	        },	        	        
	     }); 
}

function searchIndividual(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{					
	    $('#searchhomepage').show();
		
	    $('#searchicon').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		 $.ajax({  
		     type : "Post",   
		     url : "indsearchprofilefromUtilizer.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 												
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		     $('#searchicon').show();
			    		 var values = "No data found";			    					    		
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
			    		 $('.topprofilestyle').show();
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");		 
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			 $('#searchicon').hide();
			 $('.topprofilestyle').hide();
	    }
}


function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}

function individualsearchhomepage(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}

function gotoHome()
{
    window.location = "utimyProfile.html";	
}

function  opensignOut()
{
	
	if(document.getElementById("flagbutton").value=='true')
		{
		   $("#loginBox").show();	
		   document.getElementById("flagbutton").value='false';
		}
	if(document.getElementById("flagbutton").value=='false')
		{
		   $("#loginBox").hide();	
		   document.getElementById("flagbutton").value='true';
		}
   
}

function addProfilePhoto()
{
	var profilePhoto = document.getElementById("file_browse").value;
	var flag = 'true';        	
	var profielFile = document.getElementById("file_browse").value;		 
	var ext=profielFile.substring(profielFile.lastIndexOf('.')+1);
	var extension = new Array("jpg","jpeg","png");						    
	var condition = "NotGranted";
	
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
       
	for(var m=0;m<extension.length;m++)
	    {
			if(ext==extension[m])
			    {				    	    
			       condition="Granted";				    	    
			    }				    
		}
	
	if(condition=="NotGranted")
		{
			 flag ='false';
			 document.getElementById('waitlight').style.display='none';
		     document.getElementById('waitfade').style.display='none';
			 alert("only image files are allowed!");
		}	 		 
			
	var fileDetails = document.getElementById("file_browse");
	var fileSize = fileDetails.files[0];
	var fileSizeinBytes = fileSize.size;
	var sizeinKB = +fileSizeinBytes / 1024;
	var sizeinMB = +sizeinKB / 1024;
			
	if(sizeinMB>2)
		{
		    flag ='false';
		    document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
		    alert("Image files should not more then 2 MB !");
		}
	
	if(flag=='true')
		{
			if(profilePhoto!='')
				{		  
				    document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';
				   $("#profilePhotoDiv").show();		  
				}
		}
}


function uploadbasicdetail1() {
	 	
	  document.getElementById('waitlight').style.display='block';
      document.getElementById('waitfade').style.display='block';
     
        var flag = 'true';        	
		var profielFile = document.getElementById("file_browse").value;		 
		var ext=profielFile.substring(profielFile.lastIndexOf('.')+1);
		var extension = new Array("jpg","jpeg","png");						    
		var condition = "NotGranted";
		
		for(var m=0;m<extension.length;m++)
		    {
				if(ext==extension[m])
				    {				    	    
				       condition="Granted";				    	    
				    }				    
			}
		
		if(condition=="NotGranted")
			{
			    document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		        
				 flag ='false';
				 alert("only image files are allowed!");
			}	 		 
				
		var fileDetails = document.getElementById("file_browse");
		var fileSize = fileDetails.files[0];
		var fileSizeinBytes = fileSize.size;
		var sizeinKB = +fileSizeinBytes / 1024;
		var sizeinMB = +sizeinKB / 1024;
				
		if(sizeinMB>2)
			{
				 document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		        
			    flag ='false';
			    alert("Image files should not more then 2 MB !");
			}
		
		if(flag=='true')
			{
			   var oMyForm = new FormData();	
			   oMyForm.append("Passportsize", file_browse.files[0]);
			 
			   $.ajax({
			    	
			    	type: "post",	   
			    	url: "saveProfilepictureofUtilizer.html",	             
			    	data: oMyForm,	 	    	
			        processData: false,
			        contentType: false,
			        success: function (response) {
			        
			        	document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
					    
					     document.getElementById('sociallight').style.background = "none repeat scroll 0 0 #e9ffd9";
					     document.getElementById('sociallight').style.border= "1px solid #a6ca8a";
					     document.getElementById('sociallight').style.display='block';
					     document.getElementById('socialfade').style.display='block';
					     $("#socialdisplaySharedDocDiv").html("<h3>  Profile Photo Successfully Uploaded ! <input type=\"button\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </h3>");   
						
			        	
			        				        					      
			        },			       
			        error : function() 
			        {
			        	document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						alert('Problem occured due to server slow please try again later !');
					}	        
			    });
			}
		 		     
}
  
 function showSignOut()
 {
	 
	var signOutFlag = document.getElementById("signOutFlag").value;
	
	if(signOutFlag=='0')
		{
		   $("#loginBox").show();
		   document.getElementById("signOutFlag").value = "1";
		}
	if(signOutFlag=='1')
		{
		   $("#loginBox").hide();
		   document.getElementById("signOutFlag").value = "0";
		}
		 
 }
 
	
 function profilePicHistory()
 {
 	 $.ajax({
 			type : "post",
 			url : "utilizerProfilePhoto.html",			
 			success : function(response) {		
 						
 			    $("#content").html(response);
 			
 			},
 			
 		});
 }


 function ChangeProfilePic(picName)
 {	
 	$.ajax({
 		type : "post",
 		url : "updateProfilePhotoofUtilizer.html",
 		data : "pictureName="+picName,
 		success : function(response) {		
 			 
 			 document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
			 $("#socialdisplaySharedDocDiv").html("Profile picture Changed Successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReportPopup('success')\"  /> </div>");  
	   	    
 			
 		},		
 	});  	
 }
 
 function checkSession(hittheUrl)
 {
	 $.ajax({
			type : "post",
			url : "checksessionforUtilizer.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = hittheUrl;
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }
 
 
 function viwProfilePhoto(photoName)
 {
	 
	 $.ajax({
			type : "post",
			url : "checksessionforUtilizer.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						 document.getElementById('profilePhotoPoplight').style.display='block';
					     document.getElementById('profilePhotoPopfade').style.display='block';
	
						 $.ajax({
							    type : "post",
								url : "viewUtilizrProfilePhoto.html",
								data : "fileName="+photoName,
								success : function(response) {
									
									 document.getElementById('waitlight').style.display='none';
							         document.getElementById('waitfade').style.display='none';
	
							    	 document.getElementById('profilePhotoPoplight').style.display='block';
							    	 document.getElementById('profilePhotoPopfade').style.display='block';
							    	
							    	 $("#profilePhotoPopContent").html(response);
									
								}
							}); 
					}
				else
				{
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 
	 
	
 }
 
 
 function hideAddvertise()
 {
     $("#ProfileHeadingDiv").fadeOut(90000);	 
 }

 function hidesearchDiv()
 {
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
 }	
 
 

 function profilePhotoChange()
 {
 	document.getElementById("profilePhotoChangelight").style.display = "block";
 	document.getElementById("profilePhotoChangefade").style.display = "block";
 }

 function profilePhotoChangefadeout()
 {
 	document.getElementById("selectFile").style.color ="#00b6f5";			 
 	$("#selectFile").html("Please browse a file for profile picture."); 	 
 	$("#selectFileInfo").html(""); 	
 	document.getElementById("file_browse").value = "";
 	
 	document.getElementById("profilePhotoChangelight").style.display = "none";
 	document.getElementById("profilePhotoChangefade").style.display = "none";
 }
 
 function CheckFormatofProfilePhoto()
 {
 	var profilePhoto = document.getElementById("file_browse").value;
 	
 	var flag = 'true';        	
 	var profielFile = document.getElementById("file_browse").value;		 
 	var ext=profielFile.substring(profielFile.lastIndexOf('.')+1);
 	var extension = new Array("jpg","jpeg","png");						    
 	var condition = "NotGranted";
 	 
 	for(var m=0;m<extension.length;m++)
 	    {
 			if(ext==extension[m])
 			    {				    	    
 			       condition="Granted";				    	    
 			    }				    
 		}
 	
 	if(condition=="NotGranted")
 		{
 			 flag ='false';		
 			 
 			 document.getElementById("selectFile").style.color ="blue";			 
 			 $("#selectFile").html("Your selected file is "+profilePhoto); 
 			 
 			 document.getElementById("selectFileInfo").style.color ="red";			 
 			 $("#selectFileInfo").html("Only image files are allowed !"); 
 			 
 			 return false;
 		}	 		 
 			
 	var fileDetails = document.getElementById("file_browse");
 	var fileSize = fileDetails.files[0];
 	var fileSizeinBytes = fileSize.size;
 	var sizeinKB = +fileSizeinBytes / 1024;
 	var sizeinMB = +sizeinKB / 1024;
 			
 	if(sizeinMB>2)
 		{
 		     flag ='false';
 		    
 		     document.getElementById("selectFile").style.color ="blue";			 
 			 $("#selectFile").html("Your selected file is "+profilePhoto); 
 			 
 			 document.getElementById("selectFileInfo").style.color ="red";			 
 			 $("#selectFileInfo").html("Image files should not more then 2 MB !"); 
 				   	    
 		     return false;
 		}
 	
 	if(flag=='true')
 		{		    
 		     document.getElementById("selectFile").style.color ="green";			 
 			 $("#selectFile").html("Your selected file is "+profilePhoto); 
 			 
 			 document.getElementById("selectFileInfo").style.color ="green";			 
 			 $("#selectFileInfo").html("Now you can save your file."); 
 			 
 		}
 }


 function uploadProfilePicture() {
 	 		
         var flag = 'true';        	
 		var profielFile = document.getElementById("file_browse").value;		 
 		var ext=profielFile.substring(profielFile.lastIndexOf('.')+1);
 		var extension = new Array("jpg","jpeg","png");						    
 		var condition = "NotGranted";
 		   		   
 		for(var m=0;m<extension.length;m++)
 		    {
 				if(ext==extension[m])
 				    {				    	    
 				       condition="Granted";				    	    
 				    }				    
 			}
 		
 		if(profielFile=="")
 			{
 				 flag ='false';				
 				 document.getElementById("selectFile").style.color ="blue";			 
 				 $("#selectFile").html("You didn't select any file."); 
 				 
 				 document.getElementById("selectFileInfo").style.color ="red";			 
 				 $("#selectFileInfo").html("Please select a file before save."); 
 				 return false;
 			}
 		
 		if(condition=="NotGranted")
 			{
 				 flag ='false';				
 				 document.getElementById("selectFile").style.color ="blue";			 
 				 $("#selectFile").html("Your selected file is "+profielFile); 
 				 
 				 document.getElementById("selectFileInfo").style.color ="red";			 
 				 $("#selectFileInfo").html("Only image files are allowed !"); 
 				 return false;
 			}	 		 
 		
 		var fileDetails = document.getElementById("file_browse");
 		var fileSize = fileDetails.files[0];
 		var fileSizeinBytes = fileSize.size;
 		var sizeinKB = +fileSizeinBytes / 1024;
 		var sizeinMB = +sizeinKB / 1024;
 				
 		if(sizeinMB>2)
 			{
 			    flag ='false';
 			    document.getElementById("selectFile").style.color ="blue";			 
 				$("#selectFile").html("Your selected file is "+profielFile); 
 				 
 				document.getElementById("selectFileInfo").style.color ="red";			 
 				$("#selectFileInfo").html("Image files should not more then 2 MB !"); 		
 				return false;
 			}
 		
 		
 		if(flag=='true')
 			{
 			   	
 			   var oMyForm = new FormData($("#uploadProfileImage")[0]);						   
 			   
 			   document.getElementById("profilePhotoChangelight").style.display = "none";
 			   document.getElementById("profilePhotoChangefade").style.display = "none";
 			   
 			   document.getElementById('waitlight').style.display='block';
 			   document.getElementById('waitfade').style.display='block';
 			    
 			    
 			   $.ajax({
 			        url: "saveProfilepictureofUtilizer.html",	
 			        type: 'POST',
 			        data: oMyForm,			        
 			        success: function (response) {          
 			           
 			        	document.getElementById('waitlight').style.display='none';
 					    document.getElementById('waitfade').style.display='none';
 					    
 			        	 document.getElementById('sociallight').style.display='block';
 	    			     document.getElementById('socialfade').style.display='block';
 	    			     
 	    			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
 	    				 $("#socialdisplaySharedDocDiv").html("Profile picture has beed added successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReportPopup('success')\"  /> </div>");  
        		   	    							    	 
 			        	
 			        	
 			        					      
 			        	
 			        },
 			        cache: false,
 			        contentType: false,
 			        processData: false
 			    });
 			}
 		 		     
 }

 function fadeoutReportPopup(action)
 {
 	if(action=='success')
 		{
 			document.getElementById('sociallight').style.display='none';
 			document.getElementById('socialfade').style.display='none';
 			$("#socialdisplaySharedDocDiv").html("");
 			window.location = "utimyProfile.html";
 		}
 	else
 		{
 			document.getElementById('sociallight').style.display='none';
 			document.getElementById('socialfade').style.display='none';
 			$("#socialdisplaySharedDocDiv").html("");
 		}
 	
 }
 
 function aboutUsSeeMore()
 {
 	var aboutUs = document.getElementById("myArea1").innerHTML;
 	$("#aboutUsContent").html(aboutUs);
 	document.getElementById('aboutUslight').style.display='block';	
 	document.getElementById('aboutUsfade').style.display='block';
 }

 
</script>

</head>
<body onload="getUtiAboutUs()" onclick="hidesearchDiv()"> 

<%
	String months[] = { "January", "February", "March", "April", "May","June", "July", "August", "September", "October","November", "December" };
%>

	<div class="container">
		
		<div class="top_line"></div>
		
		
		<header class="header">
		
		<%
		   String noOfAwaitedDocuments = (String)request.getAttribute("noOfAwaitedDocuments");
		   String noOfRecivedDocuments = (String)request.getAttribute("noOfRecivedDocuments");
		%>
		
		
		
		<div class="header_inner">
			<div class="logo" onclick="gotoHome()"></div>

			<div class="tfclear"></div>
			<div class="utimainmenu">
			<div id="utimenu_wrapper">
				
				<ul class="uti_main_menu">
					
					<li></li>
					<li id="myMenus1"><a class="myprofilefocus" href="#" onclick="checkSession('utimyProfile.html')"> <br>
							<br> My Profile
					</a></li>
					
					<li id="myMenus2" style="width:140px">
					     
					       
					        <a class="mydocuments" href="#" onclick="checkSession('reciveddocuments.html')"> <br>
							<br> Received Documents
					        </a>
					     
					        <%if(!noOfAwaitedDocuments.equals("0")){ %>
												  
							     <div class="noti_bubble_right"><%=noOfAwaitedDocuments%></div>
							     
							 <%} %>
					     
					 </li>
					 
					
					
					<li id="myMenus4"><a class="myplans" href="#" onclick="checkSession('utilizerPlan.html')"><br><br>
							My Plans					
					</a></li>
					
					<li id="myMenus6"><a  class="alearticons" href="#" onclick="checkSession('utilizeralerts.html')"><br>
							<br> Alerts
					</a>
					</li>	
					
					<li id="myMenus5"><a class="mysettings" href="#"  onclick="checkSession('utilizerSettings.html')"> <br> <br>
							My Settings					
					</a></li>
					
				</ul>
				
			</div>
			</div>
		</div>
			
				<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
             
              <input type="hidden" id="signOutFlag" value="0" />
                                
             <a href="#" id="loginButton" onclick="showSignOut()">
                <span></span>
                <em id="loginNameId">${utilizerProfilePic.uti_first_name}</em></a>
                 <div style="clear:both"></div>
            
            
           <div style="clear:both"></div>
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="searchIndividual()" />
             </span>
             
            </div>
            
            
            <span class="topprofilestyle" style="display: none;"></span>		
						<div id="searchicon" style="display:none;">
																
							 <div id="searchhomepage" style="display:block;">
							
							 </div>
													
				        </div>
            
            
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            
            </div>
            
               <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="utisignOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
 
 
          
       
		</header>


		<div class="utimain"  id="mainDivID">
			
			<div class="main_center">
											
            	
                													
				<div id="utileftsideId" class="utileftside">      <!--left menus should go here-->
						
						
				  
									
						<div class="utiuser_display">  
																				
								<div id="textBox">
									
									
								<!-- Profile picture History -->	
									
									<div id="overlay" ></div>
					
									<div id="boxpopup" class="box">
										
										<a onclick="closeOffersDialog('boxpopup');" class="boxclose"></a>
										 <div class="popheaderstyle"><h1 align="left">Previous Profile Pictures</h1></div>
										<div id="content">
										    
										</div>
									</div>
					
									
									
								<!-- End of Profile picture History -->	
									
																														
										<div class="user_display">
										        
										       <c:if test="${empty utilizerProfilePic.uti_profile_pic}">
										        
												        <div id="closepic" style="display: block;">          
												          <span> <img src="resources/kyc_images/Profile_pic.png"  style=" width:200px;
													               height:234px; margin-top: -12px;" /></img> </span> 
													         <!--  <a onclick="openbrowse();">Change</a> -->
													    </div>
													 
													    <div id="openbrowse" style="display: none;">
														     <form  id="editbasicdetail"  method="POST" enctype="multipart/form-data"> 					
															 <input  type="file"  name="Passportsize" id="p1" /><input type="button" id="edit" onclick="uploadbasicdetail1();" value="save"> 				
														     </form>	
														</div>	
														
											    </c:if>	
											                
											                 <%
										                         String filename1 = (String)session.getAttribute("profilepic");     
										                         String user0 =(String)session.getAttribute("Kyc_id");	
										                      %>    						
												 					
												   <c:if test="${!empty utilizerProfilePic.uti_profile_pic}">
	 											 
	 											       
	 											  			<c:set var="profilePhoto" value="${utilizerProfilePic.uti_profile_pic}" />
														 		
													 		<%
													 		String PhotoName = (String) pageContext.getAttribute("profilePhoto");												 		      
													 		%>				 
														 					 
															 <div class="utiuser_photoupload view view-first" id="utiuser_photoupload">
														     <span> <img src="${pageContext.request.contextPath}<%="/utilizerProfilePhoto.html?fileName="+PhotoName+"&indId="+session.getAttribute("uti_Ind_id")%>" style=" width:200px;height:237px; margin-top: -12px;" ></img></span>						   
															     <div id="profileHover" class="mask utimask">                       
										                               <form>
										                                   <a class="utizoomview" href="#"  onclick="viwProfilePhoto('<%=PhotoName%>')"></a> 
	                               
											                               <p style="float:left"><a href="#"  class="utichange" onclick="profilePhotoChange()">Change<!-- <input type="file" name="Passportsize" id="file_browse" onchange="addProfilePhoto()"> --></a></p>
											                               <!-- <div id="profilePhotoDiv" style="float: left;margin-left: -5px;margin-top: -69px;width: 100px;display:none;"><a href="#" class="info" style="height: 16px;margin-left: 0;margin-right: -7px;margin-top: 11px;width: 72px;" onclick="uploadbasicdetail1()">Save</a></div> -->
											                               <p style="float:right;margin-top:-68px; width: 60px;"><a href="#" class="info" onclick="openOffersDialog();profilePicHistory();">History</a>
		                              									   </p>
										                              </form>                              														
																 </div>
															 </div>
															   
															 <div class="utiuser_photo">
															 <p><strong><c:out value="${utilizerProfilePic.uti_first_name}"></c:out></strong> <br>
															 <span>
															 
															 </span>			 
															 </p>
															 </div>
															 
													</c:if>		
															
				 							 			
										</div>	     <!-- End of Usr Display -->
							
							
														
									     <c:if test="${!empty viewBasicProfile}">	
									
													<c:forEach items="${viewBasicProfile}" var="det">
																											
															<div class="utimyaccounts">
															
																		<h2>
																		<span>
																		<img src="resources/kyc_images/contact_info.png" width="25" height="26" />
																		Contact Info</span>
																		</h2> 
																		
																		<div class="utiaccount_titel">First Name</div> 
																		
																		<ul> 
																		<li>
																		<table>
																		<tbody>
																		<tr>
																		<td>
																		<td>
																		<c:out value="${det.uti_first_name}"></c:out>
																		</td>
																		</td>
																		</tr>
																		</tbody>
																		</table>
																		</li>
																		</ul> 
																		
																		<div class="utiaccount_titel">Last Name</div> 
																		
																		<ul> 
																		<li>
																		<table>
																		<tbody>
																		<tr>
																		<td>
																		<td>
																		<c:out value="${det.uti_last_name}"></c:out>
																		</td>
																		</td>
																		</tr>
																		</tbody>
																		</table>
																		</li>
																		</ul> 
																																																						
																		<div class="utiaccount_titel">E-mail address</div> 
																		
																		<ul> 
																		<li>
																		<table>
																		<tbody>
																		<tr>
																		<td>
																		<td>
																		<c:out value="${det.uti_email_id}"></c:out>
																		</td>
																		</td>
																		</tr>
																		</tbody>
																		</table>
																		</li>
																		</ul> 
																		
																		<div class="utiaccount_titel">Office E-mail address</div> 
																		
																		<ul> 
																		<li>
																		<table>
																		<tbody>
																		<tr>
																		<td>
																		<td>
																		<c:out value="${det.uti_office_emailId}"></c:out>
																		</td>
																		</td>
																		</tr>
																		</tbody>
																		</table>
																		</li>
																		</ul> 
																		
																		<div class="utiaccount_titel">Office Number 1st No.</div> 
																		
																		<ul>
																		<li>
																		<table>
																		<tbody>
																		<tr>
																		<td>
																		<td>
																		<c:out value="${det.uti_office_first_no}"></c:out>
																		</td>
																		</td>
																		</tr>
																		</tbody>
																		</table>
																		</li>
																		
																		</ul>
																		
																		<div class="utiaccount_titel">Office Number 2nd No.</div> 
																		
																		<ul>
																		<li>
																		<table>
																		<tbody>
																		<tr>
																		<td>
																		<td>
																		<c:out value="${det.uti_office_second_no}"></c:out>
																		</td>
																		</td>
																		</tr>
																		</tbody>
																		</table>
																		</li>
																		
																		</ul>
																
															</div>				<!-- End of myaccounts Div -->
															
													</c:forEach>
																										
									      </c:if>							
														
								</div> 			<!-- End of textBox Div  -->
													
						</div>           <!-- End of user_display -->
					
				</div>    <!-- End of left side Div -->
				
						
											
				<div id="utiRightSideDivId" class="utirightside">
								
						
								       
								<!--center body should go here-->
						
						
						
						<!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>
                                  <div id="ProfileHeadingDiv" class="utiProfileHeadingDiv" style="display:block;">
									
										<marquee behavior="scroll" scrollamount="2" direction="left">
											   <h1 class="rightsideheader">
											       
											        <%=utiAdvertise%>
											        
											   </h1>
									     </marquee>
							        </div>	
							       
							        		
					   <!-- Advertisment Div for Entire Page  -->
						
						
						
						<div id="suggestions1">
																 																
								  <div class="border_line"></div>
								  
								  <div class=" username_bar">
										
										<div id="titleBar">
											
											<h2>
											
											    <c:if test="${!empty viewBasicProfile}">
												    
												    <c:forEach items="${viewBasicProfile}" var="det">	
												       
												          <b></b>
													      <span>Hey I am ${det.uti_first_name}</span>
													      <c:set var="member_date"  value="${det.uti_cr_date}"/>
							                             
							                              <%
							                              	Date member_date = (Date) pageContext.getAttribute("member_date");
							                              	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							                              	String reportDate = df.format(member_date);
							                              	String dateAry1[] = reportDate.split(" ");
							                              	String dateAry2[] = dateAry1[0].split("-");
							                              	int month = Integer.parseInt(dateAry2[1]);
							                              	String year = dateAry2[0];
							                              %>		
							                              					
													    <p>Member since <%=months[month - 1] + " " + year%></p>
													    
													</c:forEach>
													
													
												</c:if>	
																		
												<!--  Member since march 2014 -->
												
											</h2>
											
										</div>      <!-- End of tittleBar Div -->
										
								  </div>			<!-- End of username_bar Div -->
									
								
								
								<div class="module-body">
									
									<div id="viewUsers" style="height: auto; width: auto;  margin:10px auto;">
			                           
					                            
					                            <div id="aboutus" style="width:600px;border:1px solid #222;margin-left:180px;height:25px;background-color:#05b7f5;color:#fff;display:none;font-size:16px;">Saved Successfully!</div>
											         
													<h1 class="utirightsideheader">About Me
													
													<c:if test="${!empty utilizerAboutUsDate.cr_date}"> 
														 <p>Last updated on <c:set var="Aboutdate" value="${utilizerAboutUsDate.cr_date}" />
														 		<%
														 			    Date about_date = (Date) pageContext.getAttribute("Aboutdate");
														 		        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
														 		        String reportDate1 = df.format(about_date);
														 				String dateAry3[] = reportDate1.split(" ");
														 				String dateAry4[] = dateAry3[0].split("-");
														 				int abt_month = Integer.parseInt(dateAry4[1]);
														 				String abt_year = dateAry4[0];
														 		%>				 
														        <%=months[abt_month - 1] + " " + abt_year%></p> 
													</c:if>
													
													<button onClick="toggleArea1();"><div id="htmlbutton" class="editbutton">Edit</div></button>
						</h1>
						
						<div id="texteditediv" style="clear: both;">
						
							<div id="myArea1">   </div>
			                  
			                  <div class="seemorediv" id="seemore" style="display:none;"><a href="#" onclick="aboutUsSeeMore()">See More..</a></div>
			  
				              <div id="textareaId" style="display:none;">
				              
					              <form id="textareaform" name="textareaform" enctype="multipart/form-data">
						                
						                <textarea id="indaboutUs" name="aboutusTextArea" style="width:100px;" maxlength="2500">
					
					                         			                         
			                            </textarea>
			                            
	                              </form>  
	                              
                             </div> 
                            
							<div id="flash" style="display: inline;width: 817px;">   </div>
							
						</div>
					                            
					                           					                           					                              					                           
					                            <c:forEach items="${viewBasicProfile}" var="det">
					                            
							                            <h1 class="utirightsideheader">Branch Address</h1>
							                            								                           	
														<div>														
														     <h4>	
														     <span></span>													     
														     <c:out value="${det.uti_office_Address}"></c:out>
														     </h4>
														</div>
														<h1 class="utirightsideheader">Head Office Address</h1>
							                            								                           	
														<div>														
														     <h4>
														     <span></span>															     
														     <c:out value="${det.uti_head_office_Address}"></c:out>
														     </h4>
														</div>
																																																																																						
												</c:forEach>
												
												 <c:forEach items="${viewBasicProfile}" var="det">
					                            
							                            <h1 class="utirightsideheader">Domain Details</h1>
							                            								                           	
														<div>
														      <h4>
														      <span></span>	
														      <c:out value="${det.uti_dom_hierarchy}"></c:out></h4>
														      <c:set var="domainHierarchy" value="${det.uti_dom_hierarchy}" />
														 	  <%
														 		  String about_date = (String) pageContext.getAttribute("domainHierarchy");	
														 	       
														 	   %>			
														</div>
																																																																																						
												</c:forEach>
												
												
												<div id="ClsTreeList"></div>
			
									</div>			<!-- End of viewUsers Div -->
									
									              								
								
					            </div>      <!-- End of module-body Div -->
					            
							
                           </div>    <!-- Supports for when we search the individual -->


				  </div>				 <!-- End of rightside div -->
				
				
				<br />

			</div>            <!-- End of Main Center div -->
			
		</div>          <!-- End of Main div -->

		<!--footer block should go here-->
		
		<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>

		
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="utiContactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	
			<div class="footer_line"></div>

	</div>
	<!------container End-------->
	
	
 <!-- About Us Div -->
		
		<div id="aboutUslight" class="aboutUsContent">
		<div class="colsebutton" onclick="aboutUsfadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		 <div class="popheaderstyle"><h1>About Me</h1></div>
				
			   <div id="aboutUsContent"></div> 	   
			   
		</div>
		
		<div id="aboutUsfade" onclick="aboutUsfadeout()"></div> 	
		
	<!-- EOF About Us Div -->	


<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
							
							 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give Us Few Seconds.....
						       </div>
									
                      </div>
                       
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->

<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->
	
	
	
    <!-- Profile Photo Pop Div -->
		
		<div id="profilePhotoPoplight">
		
		<div class="colsebutton" onclick="profilePhotoPopfadeout()"><img height="22" width="22" class="frofilepic_closebtn"  src="resources/images/close_button.png" /></div>
	       
	       <div id="profilePhotoPopContent" style=" display: block;
    height: 536px;
    margin: 0;
    width: auto;"></div> 	   
			   
		</div>
		
		<div id="profilePhotoPopfade" onclick="profilePhotoPopfadeout()"></div> 	
		
	<!-- EOF Profile Photo Pop Div -->	
	
	
	
	<div id="profilePhotoChangelight">
		
		                                <div class="colsebutton" onclick="profilePhotoChangefadeout()"><img height="22" width="22" class="frofilepic_closebtn"  src="resources/images/close_button.png" /></div>
	       
	                                    <div id="profilePhotoPopContent" style=" display: block;"></div> 	   
			                               
			                               <div class="reciveddocument" style="text-align:left;">Change Profile Picture</div>
			                               
			                                <form name="uploadProfileImage" id="uploadProfileImage"  method="POST" enctype="multipart/form-data">
	                                    			
	                                    			<ul>
	                                    			    
	                                    			    <li style="margin-right:45px;">
	                                    			    		<p id="selectFile" style="width:100%;overflow:hidden;color:#00b6f5;">Please browse a file for profile picture.</p>
	                                    			    </li>
	                                    			    <li style="margin-right:45px;">
	                                    			    		<p id="selectFileInfo" style="width:100%;overflow:hidden;"></p>
	                                    			    </li>
	                                    			    <li style="float:left; margin-top: -50px; margin-left: 90px;">
	                                    			    		<p style="float:left"><a href="#"  class="change" >Browse<input type="file" name="Passportsize" id="file_browse" onchange="CheckFormatofProfilePhoto()"></a></p>
	                                    			    </li>
	                                    			    
	                                    			    <li style="float:left;">
	                                    			            <div id="profilePhotoDiv" style="float: left; width: 100px; margin-top:9px; margin-left: 112px;"><a  class="change" style="height: 16px;margin-left: 0;margin-right: -7px;margin-top: 11px;width: 72px;cursor: pointer;" onclick="uploadProfilePicture()">Save</a></div>
	                                    			    </li>
	                                    			    
	                                    			</ul>	                                   		                                    					                              				                               	                               				                               
					                               
				                             </form> 
				                             
		                                </div>
		
		                                <div id="profilePhotoChangefade" onclick="profilePhotoChangefadeout()"></div> 
		
		
	
	
	
			
	<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->

	
	
	
</body>
</html>
