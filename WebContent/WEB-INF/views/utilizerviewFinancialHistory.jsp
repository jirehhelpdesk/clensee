<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<div id="panel-2" style="width:100%;">

												 <% String docsData = (String)request.getAttribute("docsData");
														         
														           int indId = (Integer)request.getAttribute("indId");
														            
														           String finHierarchy =  (String)request.getAttribute("finHierarchy");
														           
														           String heirarchyArray[] = finHierarchy.split(",");
														           
														           String fromYear = docsData.split("&&")[0].split("-")[0];
														           String toYear = docsData.split("&&")[0].split("-")[1];
														       %>
														      
														            <div class="financialviewbutton" style="width: 96% ! important;">
														       
																            <input	class="backbuttonindex_histy"  type="button" value="Back"  onclick="HistoryDocUti('<%=(String)session.getAttribute("urlValueToGoHistory")%>')"/>
																  
																  
																   </div>
														         <div id="financialviewDiv">
														          
														        <div id="viewDiv" class="viewdiv">
														             <table>
														                 <tr >
														                 <td style="width:auto;">
																             <b>Financial Year </b> 
																                          		            		           
																         </td>
																         <td style="width:10px;">:</td>
																         <td><%=fromYear%>-<%=toYear%></td>
																         </tr>
																         <tr>
																         <td style="width:auto;">
																          <b>Financial Document Type </b></td>
																          
																          <td style="width:10px;">:</td>
																          <td style="width:auto;"><%=docsData.split("&&")[1]%><input type="hidden" id="finDocType" value="<%=docsData.split("&&")[1]%>"/>
																          </td>
																          </tr> 
																          
																          <tr>
																          <td style="width:auto;"><b>Document Mode</b></td>
																          <td style="width:10px;">:</td>   
																          <td style="width:auto;"><%=docsData.split("&&")[2]%><input type="hidden" id="finDocMode" value="<%=docsData.split("&&")[2]%>"/>
																           </td>
																          </tr> 		
														           		  <tr>
														           		  <td style="width:auto;">
															                      <b>Added on</b>
															                      </td>
															                      <td style="width:10px;">:</td>
															                      <td style="width:auto;">
															                      <%=docsData.split("&&")[3]%>
															                      </td>
															              </tr>			             		  
														              </table>
														        </div>   
														     
																			<div id="viewFinDoc" class="viewdivimg">
																             
																             <ul><li class="utilizerFinancial">
																             
																             <b>Financial Document</b>
																           
																		               <%if(!docsData.split("&&")[4].contains("jpeg") && !docsData.split("&&")[4].contains("jpg") && !docsData.split("&&")[4].contains("gif") && !docsData.split("&&")[4].contains("png")) { %>
																																																									             
																	                            <%if(docsData.split("&&")[4].contains("pdf")) {%>
																																																										                        
																		                            <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																		                            		
																		                        <%}else if(docsData.split("&&")[4].contains("doc")) {%>
																		                        
																		                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																		                        
																		                        <%}else if(docsData.split("&&")[4].contains("docx")) {%>
																		                        
																		                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																		                        
																		                        <%}else if(docsData.split("&&")[4].contains("xls")) {%>
																		                        
																		                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																		                         
																		                        <%}else { %>
																	                        
																	                                <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																	                        
																	                           <%} %>		
																	                           																																						    		
																	                          <br> 
																				               <br>
																	                           <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=docsData.split("&&")[4]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"></a>
																	             
																			                    <%}else{ %>
																		           
																				                    <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+docsData.split("&&")[4]+"&docCategory=Financial_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="200" height="150" 	 /></img> 
																				                    <br> 
																				                    <br>   			                		              
																					                             
																					                <a class="mydocumentviewpan" onclick="viewKycDocumnt('<%=docsData.split("&&")[4]%>')">  </a>
																					                <a class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=docsData.split("&&")[4]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>"> </a>             	         
																					              
																								<%} %>
																			
																			           </li></ul>
																			 </div>
																			 
																			
																   </div>
																  
		</div>									
</body>
</html>