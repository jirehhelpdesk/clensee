 <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Sharing Manager</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>
<%@ page import="java.util.Date"%>
<%@page import="java.text.DateFormat" %> 
<%@page import="java.text.SimpleDateFormat;" %>

<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<style>

#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
    
}
</style>

<script>

function showHideDiv1(id){
	
	myTextBox1 = document.getElementById("b6");
	value=myTextBox1.value;
	
	  var obj = document.getElementById(id);
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	    } else if(obj.style.display=="block"){
	      obj.style.display='none';
	    }
	}
</script>

<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}



</style>

<script>

function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		   window.location = "generateCode.html";
		}
	else
		{
		    
		}
}


function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
        
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>

<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	
	 if(id.length<3)
	    {
		    document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
		    
		    $('.topprofilestyle').hide();
		    $('#searchhomepage').hide();
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}
</script>





<script>

/*home page*/

function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/
       
	/* All scripts for Access Manager */
	
	function generateCode(handlerToHit) {
		
		document.getElementById("Hi-B-1").className = "generatcodeiconsactive";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "";
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Server Problem Try again Later !');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Generate Code</span></h2>");
	}
	
	function allowAccess(handlerToHit) {
 		
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "allowaccessiconactive";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "";
		
		 document.getElementById('waitlight').style.display='block';
	     document.getElementById('waitfade').style.display='block';
	       
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					
					 document.getElementById('waitlight').style.display='none';
				     document.getElementById('waitfade').style.display='none';
				       
				        
					$('#suggestions1').show();
					$("#ProfileHeadingDiv").show();
					$("#center-body-div").show();
					$("#leftsideBasicInfoDiv").show();					
					$('#searchhomepage').hide();
				 	
					
					 $("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Allow Access</span></h2>");
	}
	
	
	
    function revokeAccess(handlerToHit) {
 		
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "revokecodeiconactive";
		document.getElementById("Hi-B-5").className = "";
		
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	    
	    $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
								$.ajax({
									type : "post",
									url : handlerToHit,
									cache : false,
									success : function(response) {		
										 
										document.getElementById('waitlight').style.display='none';
									    document.getElementById('waitfade').style.display='none';
									    
										$('#suggestions1').show();
										$("#ProfileHeadingDiv").show();
										$("#center-body-div").show();
										$("#leftsideBasicInfoDiv").show();					
										$('#searchhomepage').hide();
										
										$("#viewUsers1").html(response);
										
										getIndAccDetails();
										
									},
									error : function() {
										alert('Error while fetching response');
									}
								});
							 		 		 		
							$("#titleBar").html("<h2><span>Revoke Code</span></h2>");
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
		 
	}
	
	function accessHistory(handlerToHit) {
 		
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	    
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "sharingdocumenthisticonactive";
						
		$.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
								$.ajax({
									type : "post",
									url : handlerToHit,
									cache : false,
									success : function(response) {		
										
										document.getElementById('waitlight').style.display='none';
									    document.getElementById('waitfade').style.display='none';
									    
										$('#suggestions1').show();
										$("#ProfileHeadingDiv").show();
										$("#center-body-div").show();
										$("#leftsideBasicInfoDiv").show();					
										$('#searchhomepage').hide();
										
										
										$("#viewUsers1").html(response);
										
										IndHistory();
										 
									},
									error : function() {
										alert('Error while fetching response');
									}
								});
							 		 		 		
							$("#titleBar").html("<h2><span>Sharing History</span></h2>");
					}
					else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
				}
			}); 		 
     	}
	
	function applyCode(handlerToHit) {
 		
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "applycodeiconactive";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "";
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Server Problem Try again Later !');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Apply Code</span></h2>");
	}
		
	
	 /* End of AccessManger scripts */

	
</script>


<script type="text/javascript">

function viewHomeProfile(url) 
{
	window.location = url;	 
}	

function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}	


</script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
 
 <!-- Script for sharing Manager -->
 
 <script type="text/javascript">

function applyNow(urlLink,codeId,serialId,senderName)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
    
	var j = serialId - 1;
	
	jQuery.support.cors = true;
	$.ajax
    ({
		type : "post",				
		url : "validateapplyCode.html",		
		contentType: "application/jsonp; charset=utf-8",
		success : function(response) {
		
			var i = response;			
			if(i>j)
				{
				
				jQuery.support.cors = true;
					$.ajax
				    ({
						type : "post",				
						url : urlLink,		
						contentType: "application/jsonp; charset=utf-8",
						success : function(response) {
							
							document.getElementById('waitlight').style.display='none';
		       	    		document.getElementById('waitfade').style.display='none';
		       	    
							$("#viewUsers1").html(response);	
							 
						},
						
					});		 		 		 		
				    $("#titleBar").html("<h2><span>"+senderName+" documents</span></h2>");				   
				}
           else
        	   {
        	  		document.getElementById('waitlight').style.display='none';
       	    		document.getElementById('waitfade').style.display='none';
       	    
       	    
        	   		document.getElementById('sociallight').style.display='block';
  			 		document.getElementById('socialfade').style.display='block';
  			 		document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
  			 		$("#socialdisplaySharedDocDiv").html("To view shared documents migrate your plan. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  
  		    
        	   }
		},	
     })
	
	}





/* Main Header Actions  */

function gotoProfile()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoMyDocument()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoSharingManager()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoStore()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoAlerts()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
}

function gotoPlans()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
}

function gotoSetings()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}


function signOutviaFun(url)
{
	 window.location=url;
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}	

function hideAddvertise()
{
	$('#ProfileHeadingDiv').fadeOut(90000);
}

</script>
 
 
 <!-- End of Script for Sharing Manager -->
 
</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocuments"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanagerfocus" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
            
            
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->			
						
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="#" onclick="signOutviaFun('signOut.html')"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
					
		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
								  <ul>
								     <li id="generatcodeicons"><a href="generateCode.html" id="Hi-B-1" ><span> Individual Sharing</span></a></li>
									 <li id="applycodeicon"><a href="applyCode.html" id="Hi-B-2" class="applycodeiconactive" ><span>Documents Received</span></a></li>
									 <li id="allowaccessicon"><a href="sharingtoUti.html" id="Hi-B-3"><span> Utilizer Sharing</span></a></li>
									 <li id="revokecodeicon"><a href="#" id="Hi-B-4" onclick="revokeAccess('revokeAccess.html')"><span>Revoke Sharing </span></a></li>
									 <li id="sharingdocumenthisticon"><a href="#" id="Hi-B-5" onclick="accessHistory('accessHistory.html')"><span>Sharing History </span></a></li>
								 </ul>
							</div>     		
					</div>
					
					 </div>
					
					<div class="rightside">
					
					   
						<!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

							    <div id="ProfileHeadingDiv" style="display:block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>	
							        		
					   <!-- Advertisment Div for Entire Page  -->
					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span>View Shared Documents</span>								   
							</h2>
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
					
					<div id="History"  class="revokeCodeidscroll" >
									    
									<div id="applyCodeid" class="applyCodeidscroll">
									
									<c:if test="${!empty applyCode}">
											
										<table class="CSSTableGenerator"  align="center" style="width:98% !important;">
											<tr>
											    <!-- <th height="10" align="center">Serial No</th>class="parentbgcolorone"	 -->
											    <!--  <th height="10" align="center" >Received Code</th>		 -->
												<th height="10" align="center">Sender Name</th>
												<th height="10" align="center">Received Date</th>
												<th height="10" align="center">Reason for Sharing</th>			
												<th height="10"></th>
											</tr>
									        <%int i=0; %>
											
											<c:forEach items="${applyCode}" var="det">					 
												 
												 <c:set var="activityDateStamp"  value="${det.cr_date}"/>
		
												 <%
												     Date activityTime = (Date)pageContext.getAttribute("activityDateStamp");
												 
													 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");									 							
													 String reportDate = df.format(activityTime);
													 
													 String input = reportDate;
									    			 DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
									    			 DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd KK:mm a"); 
									    			 										            
										         %>	 
										         
										          
												 <tr class="alt">
												    <%-- <td height="10" width="90" align="center"><c:out value="<%=++i%>"/></td>	 --%>
												    <%-- <td width="80"><c:out value="${det.code_generated}"/></td>	 --%>		
													<td width="150"><c:out value="${det.cr_by}"/></td>	
													<td width="150"><c:out value="<%=outputFormat.format(inputFormat.parse(input))%>"/></td>				
													<td width="200"><c:out value="${det.code_description}"/></td>											
													<td align="center" width="100"><a href="#" onclick="applyNow('showCode.html?id=${det.code_id}&kycid=${det.code_sender}&codename=${det.code_generated}','${det.code_generated}','<%=++i%>','${det.cr_by}')"><button class="mydocumentviewimg">View</button></a></td>						
												 </tr>	
												   		
											</c:forEach>	
												
										</table>	
										
									</c:if> 
									
									<c:if test="${empty applyCode}">
									
									    <div class="notice" style="margin-left: 245px;margin: 72px auto 0;width: 250px;">  <!-- display:inline; -->
										<b>NOTICE :</b>
										    No One Shared Documents yet !
										</div>
										
									</c:if> 
									</div>
						</div>
				
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

 	
	    
</div> <!--end of right div  -->

	</div>

</div>
					
					
					
					
					
		<%
			ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			String version=resource.getString("kycversion");
			String versionDate=resource.getString("kyclatestDate");
			
		%>			
					
					
					
					
			
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
                                       
                                        <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
											
										</div>
										Give us few seconds.....
						       </div>
									
                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->
	
	
	
	
   <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		         Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
	
<!-- EOF session Div -->


<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->


</body>
</html>
     	