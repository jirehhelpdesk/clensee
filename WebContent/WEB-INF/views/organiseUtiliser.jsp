<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/css/kyc_admin_style.css" rel="stylesheet"type="text/css" />
<link rel="stylesheet" href="resources/css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Organise Utiliser</title>
<script>

function searchbyEvent()
{
	
	var type=document.getElementById("selectedidindividual");
	var searchvalue = document.getElementById("uti_search");
	var index=0;
	var type1 = type.value;
	var searchvalue1 = searchvalue.value;
		
	 $.ajax
     ({
		type : "post",				
		url : "Profile_searchUtiliser.html",			
		data :"searchdata="+searchvalue1+"&type="+type1,	  
		success : function(response) {
			
			if(response.length>0)
				{
				var usersTokens = response.split(",");
				
				 var usersTableData = "<table border=\"0\" style=\"width:840px\"><tr><th>S.NO</th><th>KYC-ID</th><th>FIRST-NAME</th><th>LAST-NAME</th><th>EMAIL-ID</th><th>PLAN</th><th>CONTACT-NUMBER</th><th>UPDATE</th></tr>";
					for ( var i = 0; i < usersTokens.length; i = i + 8) {
																
						index += 1;
						usersTableData += "<tr><td>" + (index)
								+ "</td><td>" + usersTokens[0 + i]
								+ "</td><td>" + usersTokens[1 + i]
								+ "</td><td>" + usersTokens[2 + i]
								+ "</td><td>" + usersTokens[3 + i]
								+ "</td><td>" + usersTokens[4 + i]
								
								+ "</td><td>" + usersTokens[6 + i]
								+ "</td><td>" +"<button onClick=\"UpdateUtiProfileDetails('"+usersTokens[7 + i]+"');\">Update</button>"
								+ "</td></tr>";
					}
					usersTableData += "</table>";
					
					$("#applyCodeid").html(usersTableData);
				}
			else
				{
				    $("#applyCodeid").html("No data Found !");
				}
			
				
							
		},
		
	});
	
	}
	
	
	function UpdateUtiProfileDetails(uniquId)
	{	
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
					        url: "UpdateUtiliserDetails.html",
					        type: 'POST',
					        data: "uniquId="+uniquId,		        
					        success: function (data) {          
					            $("#viewUsers").html(data);
					        },
					        
					    });
					}
				else
					{
					   $("#sessionlight").show();
				           $("#sessionfade").show();
					}
			}
		});
		
		 
	}
</script>
</head>
<body>
<div id="viewindividualpage">
				<div id="profilesearch"> 

<div id="profilesearch">  
<input type="text" name="uti_search" id="uti_search" class="profilesearchtextinput2" name="q" size="20" maxlength="80" value="" />
<input type='image' alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onClick="searchbyEvent()"/>
</div>					
					    </div>
<div class="searchiconselect">						
						<select id="selectedidindividual" name="utiCategory">
						<option value="Name">Name</option>
						<option value="KYCID">KYCID</option>
						<option value="Plan">Plan</option>
						<option value="Domain">Domain</option>						                              
						</select>
</div>
</div>
<div id="viewindividualpage">
	
<div id="applyCodeid" class="datagrid" align="center">


</div>
</div>
</body>
</html>
