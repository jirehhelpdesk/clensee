<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
   
<html>
<head>

<%@page import="java.text.DateFormat" %> 
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date,java.util.*"%>

<title>jQuery pagination</title>

<script>

    $(document).ready(function () {//alert(10);
    $("#dialog").dialog({ autoOpen: false });
    	
    return false;
    });
</script>


<style>
#page_navigation a{
	padding:3px;
	border:1px solid gray;
	margin:2px;
	color:black;
	text-decoration:none
}
.active_page{
	background:darkblue;
	color:white !important;
}



.activity1 {

		background:url('resources/kyc_images/notice1.png')no-repeat scroll 10px 50% #e3f7fc;
		border: 1px solid #8ed9f6;
		border-radius: 10px;
		color: #555;
		font-family: Tahoma,Geneva,Arial,sans-serif;
		font-size: 12px;
		margin: 10px auto 0 0px;
		padding: 10px 12px;
		text-align:left;
}

.activity2 {

        margin-top:27px;
        background:#e9ffd9 url('resources/kyc_images/notice1.png') no-repeat 10px 50%;
		border:1px solid #a6ca8a;
		color:#555;
		border-radius:10px;	
		padding:12px; 
		font-size: 12px;
		margin: 10px auto 0 0px;
		text-align:left;
}

</style>


<script>
function viewPro(id,param){ 
	
	document.getElementById('dialog').innerHTML=param;
	$("#dialog").dialog('open');
    
   
  
}
</script>
</head>
<body>
	
	
	<input type='hidden' id='current_page' />
	<input type='hidden' id='show_per_page' />
	
	<div id='activityContent' style="">
	 <table style='width: 100%' border="0" >
			   
			
			 </table>
	
	<c:if test="${not empty profilenotification}">
	<%int i=1;%>
	  
    <div class="table">
	      
	       <ul>
		    
			    <c:forEach items="${profilenotification}" var="det">				   			   
				  		 		
				  		 		<c:set var="activityDateStamp"  value="${det.profileupdatetime}"/>
		
								 <%
								     Date activityTime = (Date)pageContext.getAttribute("activityDateStamp");
								 
									 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");									 							
									 String reportDate = df.format(activityTime);
									 
									 String input = reportDate;
					    			 DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					    			 DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd KK:mm a"); 
					    			 
						            
						         %>
					        	
						
						<%if(i%2!=0){ %>
						
							<div class="activity2" > <b> <c:out value="<%=outputFormat.format(inputFormat.parse(input))%>"/> </b> : <c:out value="${det.profileupdatedetail}"/>  </div>							
						
						<%}else{ %>
						
							<div class="activity1" > <b> <c:out value="<%=outputFormat.format(inputFormat.parse(input))%>"/> </b> : <c:out value="${det.profileupdatedetail}"/> </div>
						
						<%} %>  
							
							<%i++;%>																								
				</c:forEach>
								    				    
		   </ul>
		  		
   </div>
   </c:if>
   
   
   <c:if test="${empty profilenotification}">
	<div class="notice" style="margin-left: 245px;margin: 72px auto 0;width: 250px;">
						<b>NOTICE :</b>
						Nothing is updated in your Profile!
						</div>
	        
	</c:if>
	
	</div>
	
<div id="dialog" title="View More">

</div>	 
	
	<div id='page_navigation'></div>
</body>
</html>
   