<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Academic Hierarchy</title>

<script>
function newHierarchy()
{
	$("#newAcaHierId").show();
	$("#orgAcaHierId").hide();
}

function orgHierarchy()
{
	$("#orgAcaHierId").show();
	$("#newAcaHierId").hide();	
}

function asPerTypeDiv()
{
	
	var type = document.getElementById("typeId").value;
	if(type=="dd")
		{
		   $("#DropDownCriteria").show();
		   $("#TextBoxCriteria").hide();
		}
	if(type=="tb")
		{
		   $("#TextBoxCriteria").show();
		   $("#DropDownCriteria").hide();
		}
	if(type=="Select Type" || type=="fb")
		{
		   $("#TextBoxCriteria").hide();
	       $("#DropDownCriteria").hide();
		}
	
}

function createNewHierarchy()
{	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				    $("#waitingdivId").show();
				
					var hierarchyName = document.getElementById("newHierarchyId").value;
					if(hierarchyName!="")
						{
							$.ajax({
								
							    type : "post",				
								url : "academic/newHierachy.html",				
								data :"hierarchyName="+hierarchyName,	  
								success : function(response) {
									
									$("#waitingdivId").hide();
									
									alert("Hierarchy type has created successfully !");
													
									$("#viewUsers").html(response);	
									
								},
								
							 });
						}
					else
						{
						
						   $("#waitingdivId").hide();
						   
						   alert("Please Enter Hierarchy Name !");
						}
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
}

function displayComponents(id)
{
	
	var hierarchyType = document.getElementById(id).value;
	if(hierarchyType!="Select Document Type")
		{
		
		$("#waitingdivId").show();
		
			$.ajax({
				
			    type : "post",				
				url : "gethierarchy/components.html",				
				data :"hierarchyType="+hierarchyType,	  
				success : function(response) {
									
					if(response!="No Data")
						{
						
						$("#waitingdivId").hide();
						
						    var values= "";
						    values += "<ul>"; 
						    var componentArray = response.split(",");
						   
						    for(var i=0,n=1;i<componentArray.length;i++,n++)
						    	{						    	   
						    	   var curComponent = componentArray[i].split(":");
						    	   
						    	   if(curComponent[1]=="tb")
						    		   {
						    		     values += "<li>"+n+"."+curComponent[0]+"->Text Box"; 
						    		   }
						    	   if(curComponent[1]=="fb")
						    		   {
						    		     values += "<li>"+n+"."+curComponent[0]+"->File Browse"; 
						    		   }
						    	   if(curComponent[1]=="dd")
						    		   {
						    		     values += "<li>"+n+"."+curComponent[0]+"->Drop Down"; 
						    		   }
						    	   if(curComponent[1]=="ta")
					    		      {
					    		         values += "<li>"+n+"."+curComponent[0]+"->Text Area"; 
					    		      }
						    	   
						    	   values += "<ul>";
						    	   for(var j=2;j<curComponent.length;j++)
						    		   {
						    		      values += "<li>->"+curComponent[j]+"</li>";
						    		   }
						    	   values += "</ul>";
						    	   values += "</li>";
						    	}
						    
						    values +="</ul>"; 
						    $("#viewComponentId").html(values);
						}
					else
						{
						    $("#waitingdivId").hide();
						    
						    $("#viewComponentId").html("Nothing has added till now !");
						}
				},
				
			 });
		}
}

function addMoreOptionValue()
{
	var values = "";
	
	var labelname = document.getElementById("labelnameId").value;	
	var labeltype = document.getElementById("typeId").value;	
	
	var ddOptionValue = document.getElementById("ddOptionId").value;
	
	if(ddOptionValue!="")
		{
			document.getElementById("optionDropdownStringId").value = document.getElementById("optionDropdownStringId").value + ddOptionValue + ":";
			
			var optionValue = document.getElementById("optionDropdownStringId").value;
			var optionArray = optionValue.split(":");
			
			values += "<ul>"+labelname+"<li><ul>"+labeltype;
			for(var i=0;i<optionArray.length;i++)
				{
				    values += "<li>"+optionArray[i]+"</li>";
				}
			values += "</ul></li></ul>";
				
			$("#viewComponentId").html(values);
			
			document.getElementById("ddOptionId").value = "";
		}
	else
		{
		    alert("Please give a Drop down value !");
		}
		
}

function saveDocumentHierarchy()
{
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				
				$("#waitingdivId").show();
				
							var flag = 'true';
							
							var heirarchyType = document.getElementById("acaDocTypeId").value;
							
							var labelname = document.getElementById("labelnameId").value;	
							var labeltype = document.getElementById("typeId").value;	
							
							var finVal = "";
							      
							if(heirarchyType!="Select Document Type")		
							{	
								
									if(labelname=="")
									   {
										
										$("#waitingdivId").hide();
									      flag = 'false';
									      alert("Please Enter a Label Name !");
									   }
								
									if(labeltype=="Select Type")
									   {   
										$("#waitingdivId").hide();
									      flag = 'false';
									      alert("Please Select a Label Type !");
									   }
												
									if(labeltype=="fb")
									   {	
										
										  finVal += labelname+":"+labeltype;
									   }
									
									if(labeltype=="dd")
									   {			
									      finVal += labelname+ ":" + labeltype + ":" +document.getElementById("optionDropdownStringId").value;
									      finVal = finVal.substring(0,finVal.length-1);				      
									   }
									     
									if(labeltype=="tb")
									   {	      
									      var textContentSize = document.getElementById("labelsizeId").value;	     	    
									      var ddtype = document.getElementById("ddTextContentType").value;	   
									     
									      var num = /^[0-9]+$/;	
									      
									      if(textContentSize=="")
									    	  {
									    	  
									    	  $("#waitingdivId").hide();
										    	  flag = 'false';
											      alert("Please Enter Text content size !");	    	
									    	  }	      	     
									      else if(!textContentSize.match(num))
									    	  {
									    	  
									    	  $("#waitingdivId").hide();
										    	  flag = 'false';
											      alert("Please Enter Text content size in Numeric Value!");
									    	  }
									      else
									    	  {	  	    	     
									    	      finVal += labelname+":"+labeltype+":"+ddtype+":"+textContentSize;	   			    	     
									    	  }			     
									   } 
									
									if(labeltype=="ta")
									   {	      			          	    			      			          	     
									      finVal += labelname+":"+labeltype;	    	     			    		     
									   }  	
									
									if(flag=='true')
									   {				     
											$.ajax({
												
											    type : "post",				
												url : "saveAcademicHierarchy.html",				
												data :"heirarchyType="+heirarchyType+"&component="+finVal,	  
												success : function(response) {
													
													$("#waitingdivId").hide();
													
													 alert("Hierarchy Component has Saved Successfully !");
													
													 document.getElementById("labelnameId").value = "";
													 $("#TextBoxCriteria").hide();
												     $("#DropDownCriteria").hide();
												     					    						     
												     var values= "";
													    values += "<ul>"; 
													    var componentArray = response.split(",");
													   
													    for(var i=0,n=1;i<componentArray.length;i++,n++)
													    	{						    	   
													    	   var curComponent = componentArray[i].split(":");
													    	   
													    	   if(curComponent[1]=="tb")
													    		   {
													    		     values += "<li>"+n+"."+curComponent[0]+"->Text Box"; 
													    		   }
													    	   if(curComponent[1]=="fb")
													    		   {
													    		     values += "<li>"+n+"."+curComponent[0]+"->File Browse"; 
													    		   }
													    	   if(curComponent[1]=="dd")
													    		   {
													    		     values += "<li>"+n+"."+curComponent[0]+"->Drop Down"; 
													    		   }
													    	   if(curComponent[1]=="ta")
												    		   {
												    		        values += "<li>"+n+"."+curComponent[0]+"->Text Area"; 
												    		   }
													    	   values += "<ul>";
													    	   for(var j=2;j<curComponent.length;j++)
													    		   {
													    		      values += "<li>->"+curComponent[j]+"</li>";
													    		   }
													    	   values += "</ul>";
													    	   values += "</li>";
													    	}
													    
													    values +="</ul>"; 
												     						     
													 $("#viewComponentId").html(values);
													 
													 document.getElementById("optionDropdownStringId").value = "";        //What ever we added into the drop down list remove after save the component
													 
												},
											 });	
									   }
							}
							else
								{								
								    $("#waitingdivId").hide();
								    alert("Select a Hierarchy Type !");
								}
				}
			else
				{
				    $("#sessionlight").show();
			        $("#sessionfade").show();
				}
		}
	});
		
}

function showAcademicDocTypeHier(id)
{    
	var docType = document.getElementById(id).value;
		
	if(docType!="Select Document Type")
		{
		
		$("#waitingdivId").show();

		$.ajax({					
				    type : "post",				
					url : "grabAcademicHierByDocType.html",				
					data :"docType="+docType,	  
					success : function(response) {
						
						
						$("#waitingdivId").hide();
						
						var values = "";
						
						values += "<ul style=\"text-align:left;\">";
						var componentArray = response.split(",");
						
						
						if(response=="No Data")
						{
							 $("#showAcademicHierarchy").html("No Component's are there in this hierarchy !");
						}
						else if(response.length>3)
							{
									for(var i=0;i<componentArray.length;i++)
									{
									    var labelComp = componentArray[i].split(":");
									    values += "<li style=\"margin-top:10px;\"><input type=\"button\" id=\""+labelComp[0]+"\" value=\"Organize\" onclick=\"showLabelOperation(this.id)\"/>"+labelComp[0]+"</li>";
									}
								
								values += "</ul>";								
								$("#showAcademicHierarchy").html(values);								
								document.getElementById("hierarchyValue").value = response;
							}
						else
							{
							    $("#showAcademicHierarchy").html("No Component's are there in this hierarchy !");
							}
						
					},
					
				 });
			    
			    $("#hirarchyDeletebuttonId").show();
		}
	else
		{
		
		      $("#waitingdivId").hide();
		
		      $("#showAcademicHierarchy").html("Select a Document Type");
		      $("#hirarchyDeletebuttonId").hide();
		}
    
}

function showLabelOperation(id)
{
	
	var hierarchy = document.getElementById("hierarchyValue").value;
	var hierarchyArray = hierarchy.split(",");
	var values= "";
    values += "<ul>";  
	
    for(var i=0;i<hierarchyArray.length;i++)
		{		   		    
		    var componentArray = hierarchyArray[i].split(":");
		    if(id==componentArray[0])
		    	{ 		    	
				       if(componentArray[1]=="tb")
			    		   {
			    		     values += "<li>"+componentArray[0]+"->Text Box"; 
			    		   }
			    	   if(componentArray[1]=="fb")
			    		   {
			    		     values += "<li>"+componentArray[0]+"->File Browse"; 
			    		   }
			    	   if(componentArray[1]=="dd")
			    		   {
			    		     values += "<li>"+componentArray[0]+"->Drop Down"; 
			    		   }
			    	   if(componentArray[1]=="ta")
		    		   {
		    		     values += "<li>"+componentArray[0]+"->Text Area"; 
		    		   }
			    	   values += "<ul>";
			    	   			    	   
			    	   for(var j=2;j<componentArray.length;j++)
			    		   {
			    		      values += "<li>->"+componentArray[j]+"</li>";
			    		   }
			    	   
			    	   values += "</ul>";
			    	   values += "</li>";
		    	}
		}
	
	values +="</ul>"; 
	values +="<input type=\"button\" value=\"Remove\" id=\""+id+"\" onclick=\"removeComponent(this.id)\">"; 
	values +="<input type=\"button\" value=\"Modify\" id=\""+id+"\" onclick=\"updateComponent(this.id)\">"; 
	
    $("#showOrgHierarchy").html(values);
    
}

function removeComponent(id)
{
	var x=window.confirm("Are you sure you want to remove the Component from Hierarchy ?")
	if (x)
		{
			$.ajax({
				type : "post",
				url : "sessioncheck.html",
				cache : false,
				success : function(response) {
					
					if(response=="Exist")
						{
						
							$("#waitingdivId").show();
						
							var docType = document.getElementById("viewCompId").value;		
							var component = id;
								
								$.ajax({
										
									    type : "post",				
										url : "removeAcademicLabel.html",				
										data :"docType="+docType+"&component="+component,	  
										success : function(response) {
											
											$("#waitingdivId").hide();
											
											document.getElementById("hierarchyValue").value = response;
											var values = "";
											
											values += "<ul style=\"text-align:left;\">";
											var componentArray = response.split(",");
											for(var i=0;i<componentArray.length;i++)
												{
												    var labelComp = componentArray[i].split(":");
												    values += "<li style=\"margin-top:10px;\"><input type=\"button\" id=\""+labelComp[0]+"\" value=\"Organize\" onclick=\"showLabelOperation(this.id)\"/>"+labelComp[0]+"</li>";
												}
											values += "</ul>";
											
											$("#showAcademicHierarchy").html(values);
											
											$("#showOrgHierarchy").html("Selected component Removed from the Hierarchy successfully!");									
										},
										
									 });	
						}
					else
						{
						    $("#sessionlight").show();
					        $("#sessionfade").show();
						}
				}
			});
			
		}	
	else
		{
		    window.alert("It's ok mistake happens !");
		}
	

	
			 
}

function updateComponent(componentName)
{		
    var hierarchy = document.getElementById("hierarchyValue").value;	
	var docType = document.getElementById("viewCompId").value;		
	var hierarchyArray = hierarchy.split(",");
	var componentType = "";
	var existComponentstr = "";	
	
	for(var i=0;i<hierarchyArray.length;i++)
		{
			var eachComponentArray = hierarchyArray[i].split(":");
			if(eachComponentArray[0]==componentName)
				{
				    componentType = eachComponentArray[1];
				    existComponentstr = hierarchyArray[i];
				}
		}
	
	if(componentType!="dd")
		{
		    $("#kycdivdisplaySharedDocDiv").show();
		    $("#updateDropDownComponentId").hide();
			document.getElementById('kycdivlight').style.display='block';
			document.getElementById('kycdivfade').style.display='block';
			
			document.getElementById("hierarchyNameId").value = docType;
			document.getElementById("componntNameId").value = componentName;
			
			document.getElementById("modifycomponentNameId").value =  existComponentstr;
		}
	else
		{
			$("#kycdivdisplaySharedDocDiv").hide();
	   	    $("#updateDropDownComponentId").show();
	   	    document.getElementById('kycdivlight').style.display='block';
			document.getElementById('kycdivfade').style.display='block';
			
			document.getElementById("hierarchyNameIdfordd").value = docType;
			document.getElementById("componntNameIdfordd").value = componentName;	
			
			document.getElementById("modifycomponentNameforddId").value =  existComponentstr;
			
			var optionValueArray = existComponentstr.split(":");			
			var leftDivValues = "";
			leftDivValues += "<ul>";
			
			for(var j=2;j<optionValueArray.length;j++)
				{
				    leftDivValues += "<li style=\"margin-top:10px;\">"+optionValueArray[j]+" <a href=\"#\" onclick=\"removeOptionValue('"+optionValueArray[j]+"')\"><img src=\"admin_resources/resources/images/removeOption.png\" /></a></li>";
				}
			leftDivValues += "</ul>";
			
			$("#leftOptionDiv").html(leftDivValues);
			
		}
	
}

function removeOptionValue(optionValue)
{
   	var dropDownHier = document.getElementById("modifycomponentNameforddId").value;
   	var optionValueArray = dropDownHier.split(":");			
	
   	var changeddropDownHier = "";
   	changeddropDownHier = optionValueArray[0]+":"+optionValueArray[1];
   	
   	var leftDivValues = "";
	leftDivValues += "<ul>";
	
	for(var j=2;j<optionValueArray.length;j++)
		{
		   if(optionValueArray[j]!=optionValue)
			   {
			  		changeddropDownHier += ":"+optionValueArray[j];
			    	leftDivValues += "<li style=\"margin-top:10px;\">"+optionValueArray[j]+" <a href=\"#\" onclick=\"removeOptionValue('"+optionValueArray[j]+"')\"><img src=\"admin_resources/resources/images/removeOption.png\" /></a></li>";
			   }	     	
		}
	
	leftDivValues += "</ul>";
	
	document.getElementById("modifycomponentNameforddId").value = changeddropDownHier;
	
	$("#leftOptionDiv").html("");	
	$("#leftOptionDiv").html(leftDivValues);
}

function addOptionValue()
{
	var dropDownHier = document.getElementById("modifycomponentNameforddId").value;
   	var optionValueArray = dropDownHier.split(":");			
	var addedoptionValue = document.getElementById("addedoptionValueId").value;
   	
   	var changeddropDownHier = "";
   	changeddropDownHier = optionValueArray[0]+":"+optionValueArray[1];
   	
   	var leftDivValues = "";
	leftDivValues += "<ul>";
	
	for(var j=2;j<optionValueArray.length;j++)
		{	
		     changeddropDownHier += ":"+optionValueArray[j];
			 leftDivValues += "<li style=\"margin-top:10px;\">"+optionValueArray[j]+" <a href=\"#\" onclick=\"removeOptionValue('"+optionValueArray[j]+"')\"><img src=\"admin_resources/resources/images/removeOption.png\" /></a></li>";			       	
		}
	
	changeddropDownHier += ":"+addedoptionValue;
	
	leftDivValues += "<li style=\"margin-top:10px;\">"+addedoptionValue+" <a href=\"#\" onclick=\"removeOptionValue('"+addedoptionValue+"')\"><img src=\"admin_resources/resources/images/removeOption.png\" /></a></li>";			       	
	
	leftDivValues += "</ul>";
	
	document.getElementById("modifycomponentNameforddId").value = changeddropDownHier;
	
	document.getElementById("addedoptionValueId").value = "";
	
	$("#leftOptionDiv").html("");	
	$("#leftOptionDiv").html(leftDivValues);
	
}

function updateComponentFromHierarchyfordd()
{
	 
   var componentProperty = document.getElementById("modifycomponentNameforddId").value;
   var docType = document.getElementById("hierarchyNameIdfordd").value;
   
   var componentName = document.getElementById("componntNameIdfordd").value;
	
   $("#waitingdivId").show();
   
   $.ajax({
		
	    type : "post",				
		url : "updateAcademicComponent.html",				
		data :"docType="+docType+"&componentName="+componentName+"&componentProperty="+componentProperty,	  
		success : function(response) {
			
			$("#waitingdivId").hide();
			
			document.getElementById("hierarchyValue").value = response;
			
			document.getElementById('kycdivlight').style.display='none';
			document.getElementById('kycdivfade').style.display='none';
			
			$("#showOrgHierarchy").html(componentName+" has updated successfully !");
						
		},
		
	 });
   
}

function updateasPerTypeDiv()
{	
	var type = document.getElementById("updatetypeId").value;
	var componentName = document.getElementById("componntNameId").value;
	
	if(type=="tb")
		{
		   $("#updateTextBoxCriteria").show();
		   $("#updateDropDownCriteria").hide();
		   
		}
	if(type=="Select Type" || type=="fb")
		{
		   $("#updateTextBoxCriteria").hide();
	       $("#updateDropDownCriteria").hide();	       
		}
	
}

function updateComponentFromHierarchy()
{
	var changePattern = document.getElementById("changeComponentPatternId").value;
	
	var componentName = document.getElementById("componntNameId").value;
	var docType = document.getElementById("hierarchyNameId").value;
	
	var ddTextContentType = document.getElementById("updateddTextContentType").value;
	
	var type = document.getElementById("updatetypeId").value;
	
	if(type!="Select Type")
		{
		   changePattern = componentName+":"+type;
		}
	
	if(ddTextContentType!="")
		{
		   changePattern += ":"+ddTextContentType;
		   changePattern += ":"+document.getElementById("updatelabelsizeId").value;
		}
	
	$("#waitingdivId").show();
	
	$.ajax({
			
		    type : "post",				
			url : "updateAcademicComponent.html",				
			data :"docType="+docType+"&componentName="+componentName+"&componentProperty="+changePattern,	  
			success : function(response) {
				
				$("#waitingdivId").hide();
				
				document.getElementById("hierarchyValue").value = response;
				
				document.getElementById('kycdivlight').style.display='none';
				document.getElementById('kycdivfade').style.display='none';
				
				$("#showOrgHierarchy").html(componentName+" has updated successfully !");
				
				showAcademicDocTypeHier(id);
			},
			
		 });
	
}


function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	
}

function deleteAcademicHierarchy()
{
    var hierarchyName = document.getElementById("viewCompId").value;	
    
    var x=window.confirm("Are you sure you want to Delete the Academic Hierarchy ?")
	if (x)
		{
		
		$("#waitingdivId").show();
		
			$.ajax({
				
			    type : "post",				
				url : "deleteAcademicHierarchy.html",				
				data :"hierarchyName="+hierarchyName,	  
				success : function(response) {
					
					$("#waitingdivId").hide();
					
					if(response=="success")
						{
							alert("Hierarchy had successfully Removed !");
							createAcademicHierachy();
						}
					else
						{
							alert("There might be some Problem please try again Later !");
						}
					
				},
				
			 });
		}
	else
		{
		    alert("It's Ok Mistake Happens !");
		}
    
}

</script>


</head>
<body>   
     <div id="adimacademichierarchy">
     <div>
          <span><input type="button" value="Create Hierarchy" onclick="newHierarchy()"/></span><span><input type="button" value="Organize Hierarchy" onclick="orgHierarchy()"/></span>
     </div>
    
    
     <div id="newAcaHierId" style="display:block;margin-left: 30px;background-color: #fff;border-color: #999;float: left;width:800px;height: 578px;">
          
         <div id="leftHierDiv"  style="margin-top:10px;margin-left: 10px;border-color: #999;border-radius: 5px;border-style: solid;border-width: 1px;float: left;width: 450px;height:556px;">
         
         <span>
               Hierarchy Name :<input type="text" id="newHierarchyId" /><input type="button" value="Create" onclick="createNewHierarchy()"/>
         </span>
         
          <div style="background-color: #999;height: 1px;margin: 5px 0 0;width: 100%;"> </div>
          
         <form>
           
           <div>
           
           <ul>
               <li> 
               <%String hierarchyTypes = (String)request.getAttribute("docType"); 
                   if(!hierarchyTypes.equals("No Data")) 
                   {                	  
               %>              
               Hierarchy Type
               
                <select name="acaDocType" id="acaDocTypeId" onchange="displayComponents(this.id)">
                                      <option value="Select Document Type">Select Document Type</option>                                      
                                       <% for(int j=0;j<hierarchyTypes.split(",").length;j++)
					                       {
					                    %>					                   
					                  <option value="<%=hierarchyTypes.split(",")[j]%>"><%=hierarchyTypes.split(",")[j]%></option>                   
                                      <%} %>
                               </select>
                                                                      
                  <% }%>                  
                    
               </li>
             
               <li> 
               
                    Label Name
                    <input type="text" name="labelname" id="labelnameId"/>
                    
               </li>
                             
               <li>
               
                    Label Type 
                    
                    <select id="typeId" onchange="asPerTypeDiv()">
                                       <option value="Select Type">Select Type</option>
                                       <option value="dd">Drop Down</option>
                                       <option value="tb">Text Box</option> 
                                       <option value="fb">File Browse</option>   
                                       <!-- <option value="ta">Text Area</option>    -->                                   
                                </select>
                                
               </li>
                               
               <li> 
               
                     <div id="DropDownCriteria" style="display:none;">
                            <ul>
                                <li>
                                     <input type="text" name="dropdownoptionName" id="ddOptionId"/><input type="button" value="Add Option" onClick="addMoreOptionValue()"/>                                
                                </li>
                                <li>
                                     <div id="dropDownOptionId"></div>                                                                               
                                </li>
                            </ul>
                     </div>
                     
               </li>
               
               
               <li> 
               
                    <div id="TextBoxCriteria" style="display:none;">
                          <ul>
                               <li>
                                    Label Contains:<select id="ddTextContentType">                                                               
                                                                <option value="A">Alphabet</option>
                                                                <option value="N">Numeric</option>
                                                                <option value="AN">AlphaNumeric</option>
                                                                <option value="ASC">AlphaNumeric Special Character</option>
                                                   </select>
                               </li>
                               
                               <li >
                                     Label Size:<input type="text" name="labelsize" id="labelsizeId"/>
                               </li>
                               
                          </ul>
                    </div>
                    
                    
               </li>
                             
               <li>
                 
                    <input type="button" value="ADD" onclick="saveDocumentHierarchy()"/>
                    
                    <!-- <input type="button" value="SAVE" onclick="saveAcaHierarchy()///  addAcaHierarchy(),showHierarchy()"/> -->
              
               </li>
                           
         
		           <li>
		           
		            <input type="hidden" name="hierdropdownString" id="optionDropdownStringId" />
		                                                             
		            <input type="hidden" name="hierString" id="hierStringId" />
		            
		            </li>
            
             </ul>
               </div>
         </form> 
         
          </div>         
          
          <div id="rightHierDiv" Style="border-color: #999;margin-right:13px;margin-top:10px;border-radius: 5px;border-style: solid;border-width: 1px;float:right;width: 303px;height:556px;overflow-y: scroll;">
          
		          <div id="viewComponentId" style="text-align:left;">
		          	          
		          </div>
                                       
          </div> 
          
           
     </div>
    
              
           <!-- Organise Academic Heirarchy in right side Div -->
          
				     <div id="orgAcaHierId" style="display:none;margin-left: 30px;background-color: #fff;border-color: #999;border-radius: 5px;border-style: solid;border-width: 1px;float: left;width:800px;height:578px;">
				          
				         <span style="margin-top:10px;margin-left: 128px;">
				                
				                Hierarchy Type: <select name="acaDocType" id="viewCompId" onchange="showAcademicDocTypeHier(this.id)">
				                                      <option value="Select Document Type">Select Document Type</option>                                      
				                                       <% for(int j=0;j<hierarchyTypes.split(",").length;j++)
									                       {
									                    %>					                   
									                  <option value="<%=hierarchyTypes.split(",")[j]%>"><%=hierarchyTypes.split(",")[j]%></option>                   
				                                      <%} %>
				                               </select>
				                <input type="hidden" id="hierarchyValue" />          
				                   
				         </span>
				         
				         <div id="hirarchyDeletebuttonId" style="display:none;float: right;margin-right: 196px;margin-top: 4px;">
				               <input type="button" value="Delete" onclick="deleteAcademicHierarchy()"/>
				         </div>
				         
				         <div style="background-color: #999;height: 1px;margin: 30px 0 0;width: 100%;"> </div>
				         <div style="background-color: #999;height: 478px;margin: 24px 406px 0;width:1px;"> </div>
				
							<div id="showAcademicHierarchy" style="height: 498px; margin-left: 0; margin-top: -498px; overflow-y: scroll; width: 406px;">
				
				
							</div>
				
				
							<div id="showOrgHierarchy" style="float: right;overflow-y:scroll; height: 500px; margin-right: 1px; margin-top: -499px; width: 390px;">
				
				
							</div>
				
				
				             
				
				
						  <div Style="float:right;display:block;">
										            	            
					              <div id="divforLabel" style="display:none;"><input type="button" value="Remove Label" onclick="removeLabel()"/><input type="button" value="Add Option" onclick="showOption()"/></div>
					                
					              <div id="divforOption" style="display:none;"><input type="button" value="Remove Option"  onclick="removeOption()"/></div>
					               
					              <div id="divaddOption" style="display:none;"><input type="text" id="addOptionVal" /><input type="button" value="Add"  onclick="addOption()"/></div>
					               
				          </div>
				          
				     </div>
        
           <!-- End of Organise Academic Heirarchy in right side Div -->
        
       </div>  

<!-- kycDiv Div -->

	
	<div id="kycdivlight">
			
		    <div id="kycdivdisplaySharedDocDiv" style="display:none;text-align:left;margin-top:27px;width:800px;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		              
		     <ul>
		             <li style="margin-top:10px;">
		             
		                 Hierarchy Name <input type="text" readonly disabled="true" name="hierarchyName" id="hierarchyNameId"  />
		                 
		              </li>
		             
		              <li style="margin-top:10px;">  
		                 
		                 Component Name <input type="text" readonly disabled="true" name="componentName" id="componntNameId"  />
		             
		             </li>
		                  
		          <li style="margin-top:10px;">
               
                    Label Type 
                    
                    <select id="updatetypeId" onchange="updateasPerTypeDiv()">
                                       <option value="Select Type">Select Type</option>                                      
                                       <option value="tb">Text Box</option> 
                                       <option value="fb">File Browse</option>                                                                    
                                </select>
                                
               </li>
            
               <li> 
               
                    <div id="updateTextBoxCriteria" style="display:none;">
                          <ul>
                               <li>
                                    Label Contains:<select id="updateddTextContentType" >
                                                                <option value="">Select Validation</option>                                                               
                                                                <option value="A">Alphabet</option>
                                                                <option value="N">Numeric</option>
                                                                <option value="AN">AlphaNumeric</option>
                                                                <option value="ASC">AlphaNumeric Special Character</option>
                                                   </select>
                               </li>
                               
                               <li >
                                     Label Size:<input type="text" name="labelsize" id="updatelabelsizeId"/>
                               </li>
                               
                          </ul>
                    </div>
                    
                 
               </li>
		            
		            <li>
		                <input type="hidden" name="modifycomponentName" id="modifycomponentNameId" />
		                
		                <input type="hidden" name="changeComponentPattern" id="changeComponentPatternId" />
		                
		                <input type="button" value="update" onclick="updateComponentFromHierarchy()"/>
		            </li> 
		                  
		       </ul>
		              
		              
		    </div>	
		    
		    <div id="updateDropDownComponentId" style="display:none;margin-top:27px;width:800px;margin-left:-99px;color:#555;border-radius:24px;	padding:7px 12px;margin:0 auto;"> 	
						
				<ul>				
				      <li style="margin-top:10px;">		             
		                 Hierarchy Name <input type="text" readonly disabled="true" name="hierarchyNamefordd" id="hierarchyNameIdfordd"  />		                 
		              </li>
		             
		              <li style="margin-top:10px;">  
		                	Component Name <input type="text" readonly disabled="true" name="componentNamefordd" id="componntNameIdfordd"  />		             
		              </li>		             								     								
				</ul>		 
				
				<div id="leftOptionDiv" style="height: 300px;margin-left:-17px; overflow-y: scroll;text-align: left;width: 434px;">
				
				
				</div>
   
 					
				<div style="float:right;margin-right:10px;margin-top:-187px;">
				
				     <input type="text" name="optionValue" style="width:100px;" id="addedoptionValueId" />
				    
				     <input type="button" value="Add Option Value" onclick="addOptionValue()"/>
				    
				</div>							
				
				<div>
					 <input type="hidden" name="modifycomponentNamefordd" id="modifycomponentNameforddId" />
		              		                
		             <input type="button" value="update" onclick="updateComponentFromHierarchyfordd()"/>	
		        </div>	
		       											
			</div>	   
			
		    
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
<!-- EOF kycDiv Div -->	             
</body>
</html>