<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Setting</title>

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">

    

<script type="text/javascript">
function fncSubmit(){ 
	
	var flag = 'true';
	
	if(document.ChangePasswordForm.OldPassword.value == "")
	{
		flag = 'false';
		alert('Please input old password');
		document.ChangePasswordForm.OldPassword.focus();	
	} 

	if(document.ChangePasswordForm.newpassword.value == "")
	{
		flag = 'false';
		alert('Please input Password');
		document.ChangePasswordForm.newpassword.focus(); 
		
	} 

	if(document.ChangePasswordForm.conpassword.value == "")
	{
		flag = 'false';
		alert('Please input Confirm Password');
		document.ChangePasswordForm.conpassword.focus(); 
		
	} 

	if(document.ChangePasswordForm.newpassword.value != document.ChangePasswordForm.conpassword.value)
	{
		flag = 'false';
		alert('Confirm Password Not Match');
		document.ChangePasswordForm.conpassword.focus(); 
		
	} 

	 var username = $("#newpassword").val();
     var password=$("#conpassword").val();
     var password1=$("#OLDpwd").val();
	
     if(flag == 'true')
    	 {
				 $.ajax({  
				     type : "Post",   
				     url : "changeUtiPassword.html", 
				     data :"conpassword="+password+"&newpassword="+username+"&OldPassword="+password1,	     	     	     
				     success : function(response) 
					     {  
					    	 alert(response);				    	 
					    	 if(response=='Your Password Successfully Changed')
					    		 {
						    		 alert("Your session has expired due to changing password,so further process please relogin!");	    	
							         window.location = "utiliser.html";
					    		 }				    	 
					     },  
				       
				    });  
    	 }
}

</script>

</head>
<body>
<div class="utisettingsdiv">

		<form  name="ChangePasswordForm">
		
		<table  align="center">
		
		<tr>
		<th>Old Password</th><td>:</td>
		<td><input style="width:120px;" name="OldPassword" type="password" id="OLDpwd" size="20"></td>
		</tr>
		<tr>
		<th>New Password</th><td>:</td>
		<td><input style="width:120px;" name="newpassword" type="password" id="newpassword">
		</td>
		</tr>
		<tr>
		<th>Confirm Password</th><td>:</td>
		<td><input style="width:120px;" name="conpassword" type="password" id="conpassword">
		</td>
		</tr>
		<tr>
		<td> </td><td> </td>
		<td><input type="button" name="Submit" value="Save" onclick="fncSubmit()"></td>
		</tr>
		
		</table>
		</form>
</div>
</body>
</html>