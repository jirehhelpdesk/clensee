<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>kycindex</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link href="resources/css/kycstyle_profile.css" rel="stylesheet" type="text/css" />
</head>
<body>

    <div class="container">
		
				<!--header block should go here-->
				<tiles:insertAttribute name="header"></tiles:insertAttribute>

            <div class="main">
				<div class="main_center">
					<div class="leftside">
							
							<!--left menus should go here-->
							<tiles:insertAttribute name="menu"></tiles:insertAttribute>
							
					</div>


					 <div class="rightside">

							<!--center body should go here-->
							<tiles:insertAttribute name="body"></tiles:insertAttribute>
							
					 
					</div>
                    <br />

				</div>
                </div>
				
		<!--footer block should go here-->
		<tiles:insertAttribute name="footer"></tiles:insertAttribute>


    </div>
<!------container End-------->
</body>
</html>
