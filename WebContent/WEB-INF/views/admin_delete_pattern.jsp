<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete Pattern</title>

<script>
	function OpenDialog()
	{
	  $(function() {
			$("#show").dialog({
				autoOpen : false,
				
			});
		}); 
	}	
</script>
	
	
</head>


<script type="text/javascript">

function deletePattern(id)
{
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				
				var x=window.confirm("Are you sure you want to remove the Component from Hierarchy ?")
				if (x)
					{
						$.ajax
					     ({
							type : "post",				
							url : "deletePattern.html",			
							data :"p_id="+id,	  
							success : function(response) {
								
								alert(response);
								DeletePattern('deleteAdminPattern.html')
												
							},
							error : function() {
								alert('Error while fetching response');
							}
						});	
					}
				else
					{
					    window.alert("It's ok mistake happens !");
					}
					
				}
			else
				{
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	});
	
	 
	
	}

</script>


</head>
<body>
<c:if test="${!empty PatternDetails}">
		
	<table class='CSSTableGenerator' align="left"  cellspacing="1" align="center" style="width: 98% !important;">
		<tr>
		    <th height="10">Serial No</th>			
			<th height="10">Pattern Name</th>
			<th height="10">Cr_By</th>
			<th height="10">Date of Creation</th>
			<th height="10">Delete</th>
		</tr>
        <%int i=0; %>
		<c:forEach items="${PatternDetails}" var="det">					 
			 <tr>
			    <td height="10" width="120"><c:out value="<%=++i%>"/></td>				
				<td width="230"><c:out value="${det.pattern_name}"/></td>
				<td width="270"><c:out value="${det.cr_by}"/></td>			
				<td width="270"><c:out value="${det.cr_date}"/></td>	
				<td align="center" width="210"><a href="#" ><button id=viewhistory onClick="deletePattern(${det.pattern_id})">Delete</button></a></td>
			 </tr>			
		</c:forEach>		
	</table>	
</c:if>

</body>
</html>