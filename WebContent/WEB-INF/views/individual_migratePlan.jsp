 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Plans</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>
   <link href="resources/css/rupees/rupees.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/onlineresources/code.jquery.com.jquery-1.10.2.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<link rel="stylesheet" href="resources/planCss/planCss.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
    
}



</style>



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}
</style>



<script>
function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		   
		}
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        	     
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>

<script>
function migratePlan(planName)
{
	$.ajax({
        url: "changeplan.html",
        type: 'POST',	
        data: "planName="+planName,
        success: function (data) {   
	         
        	if(data=="success")
        		{      		
   		     		document.getElementById('sociallight').style.display='block';
   		     		document.getElementById('socialfade').style.display='block';
   		      		document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
   		     		$("#socialdisplaySharedDocDiv").html("Your plan Migrate to "+planName+" . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");   

        		}
        },
	});	
}

function migratePlan1(planName)
{
	window.location = "indRequestplanchange.html?planName="+plaName;
}
</script>



<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();	
			$('.topprofilestyle').hide();
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}

</script>

<script>


/*home page*/

function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/
		
	/* End of MyDocument Scripts  */
	
	/*Start script for Plans*/
	
	 function Myplansview(handlerToHit) {
		
		document.getElementById("Hi-B-1").className = "myplanextactive";
		document.getElementById("Hi-B-2").className = "";
		
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
								 
				 $("#viewUsers1")
					.html(response);
				},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		
	$("#titleBar").html("<h2><span>My Plan</span></h2>");
	
	$("#searchicon").hide();
}
	
	 function Myplansmigrate(handlerToHit) {
		 
		 document.getElementById("Hi-B-2").className = "migrateplanactive";
		 document.getElementById("Hi-B-1").className = "";
		 
			
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										 
					 $("#viewUsers1")
						.html(response);
					},
				
			});
		 		 		
		$("#titleBar").html("<h2><span>Migrate Plan</span></h2>");
		
		 $("#searchicon").hide();
	}
	 
	
    /*End Script for Plans */
    

    
    
 /* Main Header Actions  */
 
 function gotoProfile()
 {
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoMyDocument()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }
 
 function gotoSharingManager()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }
 
 function gotoStore()
 {	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoAlerts()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
 }
 
 function gotoPlans()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
 }
 
 function gotoSetings()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	
 }


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}
  

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}	



function hideAddvertise()
{
   $("#ProfileHeadingDiv").fadeOut(9000);	
}

</script>


<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
<link rel="stylesheet" href="resources/css/PlanDesignResources/css/planmaster.css" media="screen" />
            
<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>

<!-- Script and CSS for KYC Documents -->
 

 
 
</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocuments"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplansfocus" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>


 		<!-- Login Starts Here -->
        
            <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
            
           
           
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
			
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
         		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
							  <ul>
							    <li id="myplanext"><a href="#"  id="Hi-B-1"  onclick="gotoPlans()"><span>My Plan</span></a></li>
								<li id="migrateplan"><a href="individualgetmigrateplan.html"  id="Hi-B-2" class="migrateplanactive"><span>Migrate Plan</span></a></li>
							  </ul>
							</div>    		
					</div>
					
					 </div>
					
					<div class="rightside">
					
					
						
					    <!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>


					<div id="ProfileHeadingDiv" style="display: block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>

					
							        		
					   <!-- Advertisment Div for Entire Page  -->
				         
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
										
						 <div id="titleBar">
							
							<h2>
								   <span>Migrate Plan</span>								   
						    </h2>
						    
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
					
														      
									<%
									String currentPlan = (String)request.getAttribute("currentPlanName");
									%>
									
									
                                   <%String  featureName = (String)request.getAttribute("featurecategory");
								  String featureArray[] = featureName.split(",");
								  
								  String basicDetails = (String)request.getAttribute("basicDetails");
								  String basicDetailsArray[] = basicDetails.split(",");
								  
								  String silverDetails = (String)request.getAttribute("silverDetails");
								  String silverDetailsArray[] = silverDetails.split(",");
								  
								  String goldDetails = (String)request.getAttribute("goldDetails");
								  String goldDetailsArray[] = goldDetails.split(",");
								  
								  String platinumDetails = (String)request.getAttribute("platinumDetails");
								  String platinumDetailsArray[] = platinumDetails.split(",");
								  
								  %>	
											  
											  							
								
	<div id="kcyplanContainer">
		
	  <div id="tableContainer">
		
			<div class="tableCell highlight ">
				<div class="tableHeading">
					<h2>Features</h2>
					
				<div class="kycplanprice1 kycplanpriceContainer">
						<p><img src="resources/images/cloud_kyc.png" /><sup></sup></p>
						<span></span>
					</div> 	<!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>
		
		
			<div class="tableCell">
				<div class="tableHeading">
					<h2>Basic</h2>
					
					<div class="kycplanprice4 kycplanpriceContainer">
						<p>Free<sup></sup></p>
						<!-- <span>per month</span> -->
					</div> <!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>

			
			<div class="tableCell noBorder">
				<div class="tableHeading">
					<h2>Silver</h2>
					
					<div class="kycplanprice3 kycplanpriceContainer">
						<div class="WebRupee"> Rs.</div><%=request.getAttribute("silverprice")%>
						<span>per annum</span>
					</div> <!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>
            
		  
		  <div class="tableCell">
				<div class="tableHeading">
					<h2>Gold</h2>
					
					<div class="kycplanprice2 kycplanpriceContainer">
						<div class="WebRupee"> Rs.</div><%=request.getAttribute("goldprice")%>
						<span>per annum</span>
					</div> <!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>
			
			
			
			<div class="tableCell noBorder">
				<div class="tableHeading">
					<h2>Platinum</h2>
					
					<div class="kycplanprice5 kycplanpriceContainer">
						<div class="WebRupee"> Rs.</div><%=request.getAttribute("platinumprice")%>
						<span>per annum</span>
					</div> <!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>
			
			
		<div class="cl"><!-- --></div>
				
			
			
			<table width="100%" class="pricingTableContent">
				
				<tr class="LearnMoreRow">
					<td class="highlight" width="202"></td>
					<td width="202"></td>
                    <td width="202"></td>
					<td width="200" class="noBorder"></td>
					<td width="200 " class="noBorder"></td>
				</tr>
				
				
				<!-- Table Content -->
				
				
				
				                            <%for(int i=0,j=1;i<featureArray.length;i++,j++) {%>	<!-- Start of Feature Name for-loop -->
															
															<%if(j%2==0){%>
															<tr>
															<%}else{%>
															<tr class="altRow">						
															<%}%>	
																				    
																<td class="highlight"><strong><%=featureArray[i]%></strong> </td>
																
																
																
																
																<%boolean basiccondition = true;
																for(int k=0;k<basicDetailsArray.length;k++) {
																    String basicDataArray[] = basicDetailsArray[k].split("##");
																      %>
																       
																       <%if(k==1){ %>
																       
																               
																               <%if(featureArray[i].equals(basicDataArray[0])){ 
																		      
																			     basiccondition = false;
																		        %>
																		
																				<td><strong><%=basicDataArray[1]%></strong></td>
																				
																		       <% }%>	
																		
																       <%}else{ %>
																       
																	             <%if(featureArray[i].equals(basicDataArray[0])){ 
																			      
																				     basiccondition = false;
																			        %>
																			
																					<td><strong><%=basicDataArray[1]%></strong></td>
																					
																			     <% }%>	
																		
																       <%}%>
																       
																									     								      	
																  <%} %>
																  
																  <%if(basiccondition == true) {%>
																  
																                 <td><strong>NA</strong></td>
																              
																   <%} %>
																   
																   
																   
																
																<%boolean silvercondition = true;
																for(int k=0;k<silverDetailsArray.length;k++) {
																    String silverDataArray[] = silverDetailsArray[k].split("##");
																      %>
																    
																		<%if(k==1){ %>	
																				
																					<%if(featureArray[i].equals(silverDataArray[0])){ 
																		      
																			        silvercondition = false;
																		           %>
																		
																				      <td><strong><%=silverDataArray[1]%></strong></td>
																				
																		           <% }%>	
																		
																		<%}else{ %>
																					
																					<%if(featureArray[i].equals(silverDataArray[0])){ 
																		      
																			        silvercondition = false;
																		           %>
																		
																				    <td><strong><%=silverDataArray[1]%></strong></td>
																				
																		           <% }%>	
																		<%}%>						     								      	
																  <%} %>
																  
																  <%if(silvercondition == true) {%>
																  
																              <td><strong>NA</strong></td>
																              
																  <%} %>
																
																
																<%boolean goldcondition = true;
																for(int k=0;k<goldDetailsArray.length;k++) {
																    String goldDataArray[] = goldDetailsArray[k].split("##");
																      %>
																    
																      <%if(k==1){ %>
																      
																      				<%if(featureArray[i].equals(goldDataArray[0])){ 
																		      
																						goldcondition = false;
																					        %>
																					
																							<td><strong><%=goldDataArray[1]%></strong></td>
																							
																					 <% }%>
																		 
																      <%}else{ %>
																      				
																      				<%if(featureArray[i].equals(goldDataArray[0])){ 
																		      
																						goldcondition = false;
																					        %>
																					
																							<td><strong><%=goldDataArray[1]%></strong></td>
																							
																					 <% }%>
																      <%} %>
																		
																										     								      	
																  <%} %>
																  
																  <%if(goldcondition == true) {%>
																  
																              <td><strong>NA</strong></td>
																              
																  <%} %>
																
																
																<%boolean platinumcondition = true;
																for(int k=0;k<platinumDetailsArray.length;k++) {
																    String platinumDataArray[] = platinumDetailsArray[k].split("##");
																      %>
																    
																       <%if(k==1){ %>
																       
																       					<%if(featureArray[i].equals(platinumDataArray[0])){ 
																		      
																							platinumcondition = false;
																						        %>
																						
																								<td><strong><%=platinumDataArray[1]%></strong></td>
																								
																						<%}%>	
																		
																       <%}else{ %>
																       
																       					<%if(featureArray[i].equals(platinumDataArray[0])){ 
																		      
																							platinumcondition = false;
																						        %>
																						
																								<td><strong><%=platinumDataArray[1]%></strong></td>
																								
																						<%}%>	
																       <%} %>
																									     								      	
																  <%} %>
																  
																  <%if(platinumcondition == true) {%>
																  
																              <td><strong>NA</strong></td>
																              
																  <%} %>
															</tr>
															
									                <%} %>													<!-- End of Feature Name for-loop -->
									
				
		
						
				                               <tfoot>
														<tr>
															 <td class="highlight">
																  <h1 style="margin-left:37px;"> Apply Plan  </h1>
															</td>
									                                    
									                      
													 <td>
															
																
																<%if(currentPlan.equals("Basic")) {%>
																
																  <a class="LearnMoreButton" style="background-color: #00b6f5 ! important;">Active Plan</a>
																
																<%} %>													
															</td>
															
															<td>
																								
																<%if(currentPlan.equals("Platinum")) {%>
																	 <form action="indRequestplanchange.html" class="formDisable" method="post">
																       <input type="hidden" name="planName" value="Silver" />
																       <input type="submit" disabled="true" class="planbutton"  value="Down Grade" />
																     </form>
																<%}else if(currentPlan.equals("Gold")) {%>
																	 <form action="indRequestplanchange.html" class="formDisable" method="post">
																       <input type="hidden" name="planName" value="Silver" />
																       <input type="submit" disabled="true"  class="planbutton"  value="Down Grade" />
																     </form>
																<%}else if(currentPlan.equals("Silver")) {%>
																
																	 <a class="LearnMoreButton" style="background-color: #00b6f5 ! important;">Active Plan</a>
																	
																<%}else { %>
																
																     <form action="indRequestplanchange.html" method="post">
																       <input type="hidden" name="planName" value="Silver" />
																       <input type="submit" class="planbutton"  value="Upgrade" />
																     </form>
																  
																<%}%>
															</td>
									
															<td >
																
																<%if(currentPlan.equals("Platinum")) {%>
																     
																  <form action="indRequestplanchange.html" class="formDisable" method="post">
																       <input type="hidden" name="planName" value="Gold" />
																       <input type="submit" disabled="true" class="planbutton"  value="Down grade" />
																  </form>															
																
																<%}else if(currentPlan.equals("Gold")) {%>
																        
																        <a class="LearnMoreButton" style="background-color: #00b6f5 ! important;">Active Plan</a>
																        
																<%}else{ %>
																   
																  <form action="indRequestplanchange.html" method="post">
																       <input type="hidden" name="planName" value="Gold" />
																       <input type="submit"  class="planbutton"  value="Upgrade" />
																  </form>
																
																<%} %>
																
																
															</td>
									
															<td class="noBorder">
																
																<%if(currentPlan.equals("Platinum")) {%>
																
																     <a class="LearnMoreButton" style="background-color: #00b6f5 ! important;">Active Plan</a>
																
																<%}else{%>
																
																  <form action="indRequestplanchange.html" method="post">
																       <input type="hidden" name="planName" value="Platinum" />
																       <input type="submit" class="planbutton"  value="Upgrade" />
																  </form>	
																  
																<%}%> 
																
															</td> 
															
														</tr>
													</tfoot>
													
													
			</table>
			
		</div> 
		
		
		<div style="magin-left:0px;text-align:left;">
		    
		    <ul>  
		    
		          <li style="margin-top:0px;">
					<h1 style="font-size: 18px;"></h1>
					
					<span style="color: #7c7c7c;font-size: 12px;">
						<img src="resources/images/paymentLogo.jpg" width="800" height="80"></img>
					</span>
		        </li>
		        
			      <li style="margin-top:0px;">
				      
				      <h1 style="font-size: 18px;">Profile Viewing</h1>
				        
				        <span style="color: #7c7c7c;font-size: 12px;">
						This feature enables Individual to view other individuals profile.
						</span>
						
						<div>
						
						   <ul><li>
						   
							<h1 style="font-size: 12px !important; margin-left: 40px;">Basic Profile</h1>
							
							<p style="color: #7c7c7c;font-size: 12px;text-align:left ; margin-left: 40px;">
							Profile picture,About me, Registration details, Basic details, Family details and Social account
							details of other individuals can be seen
							</p>
							
							<h1 style="font-size: 12px !important; margin-left: 40px;">Full Profile</h1>
							
							<p style="color: #7c7c7c;font-size: 12px; margin-left: 40px;">
							Basic Profile, Current company and Education details of other individuals can be seen.
							</p>
							</li></ul>
					   </div>
					
				  </li>
			  	
			  	  <li style="margin-top:0px;">	
						<h1 style="font-size: 18px;">Individual Sharing</h1>
						
						        <span style="color: #7c7c7c;font-size: 12px;">
								This feature enables Individual to share his/her Kyc documents, Academic documents, Employment
								documents and Financial documents to other individuals and sharing will be enabled for 24 hours from
								the time of sharing. After 24 hours sharing will be disabled.
								</span>
					
					</li>
					
					<li style="margin-top:0px;">
					    <h1 style="font-size: 18px;">Documents Received</h1>
					    
					       <span style="color: #7c7c7c;font-size: 12px;">
							This feature enables to receive the documents from other individuals and received documents are
							available to view/download only for 24 hrs.
							</span>
				  </li>
				  
				  <li style="margin-top:0px;">
					<h1 style="font-size: 18px;">Receiving Mails</h1>
					
					   <span style="color: #7c7c7c;font-size: 12px;">
						This feature enables to receive mail alerts when you receive the documents from other individuals.
						</span>
				</li>
				
				<li style="margin-top:0px;">
					<h1 style="font-size: 18px;">Utilizer Sharing / Removing</h1>
					 
					   <span style="color: #7c7c7c;font-size: 12px;">
						This feature enables individual to give persistent access to the Utilizers (Employers, Banks etc.) and
						revoke access from Utilizer once the purpose is served.
						</span>
				</li>
				
				<li style="margin-top:0px;">
					<h1 style="font-size: 18px;">SMS</h1>
					
					   <span style="color: #7c7c7c;font-size: 12px;">
					    This feature enables to send sms alerts to individuals and utilizers,when you share documents with them.
						<!-- This feature enables to receive sms alerts when you receive the documents from other individuals. -->
						</span>
			    </li>
			    
			    <li style="margin-top:0px;">
					<h1 style="font-size: 18px;">Files Storage</h1>
					
					<span style="color: #7c7c7c;font-size: 12px;">
						This feature enables to save the documents to a certain limit.
						
					</span>
		        </li>
		        
		        
		        
			</ul>
		</div>
		
		
		<!-- end tableContainer -->

	</div> <!-- end pageContainer -->	
									
									
							
									
								
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	


</div> <!--end of right div  -->

	</div>

</div>
	
<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>		
			
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>
<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">

								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
								
                      </div>
                      
		   	   
	</div>
	
	<div id="waitfade" ></div> 	

	
<!-- EOF Waiting Div -->
	
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	<!-- kycDoc Pop up Div -->
	
	<div id="kycDocPopUplight">
			
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;display:none;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF kycDoc Pop up Div -->	

	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	
	<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->
	
	
	
</body>
</html>
     	