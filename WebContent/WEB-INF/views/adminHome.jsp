<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<%@ page language="java" import="java.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Admin</title>

<link href="admin_resources/kyc_css/kyc_admin_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="admin_resources/kyc_css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">

<link rel="stylesheet" href="admin_resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script type="text/javascript" src="admin_resources/kyc_scripts/demo.js"></script>

<link rel="stylesheet" href="admin_resources/css/logout.css">	
	
	<script type="text/javascript" src="resources/onlineresources/jquery-1.6.4.min.js"></script>
	
	<link rel="stylesheet" href="resources/onlineresources/watch.css">


	
<script type="text/javascript">

function redirectToLogin()
{
	window.location = "logout_admin.html";	
}


</script>

<script type="text/javascript">
	
	
function CheckSessionOfAdmin(hitTheUrl)
{
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
					window.location = hitTheUrl;
				}
			else
				{
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	});
				
}

function gotoMainPage()
{
	window.location = "authenticateAdminLogin.html"
}
</script>

<!-- End of Script for admin Pattern -->

</head>
<body>

           <%
			  ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			  String version=resource.getString("kycversion");
			  String versionDate=resource.getString("kyclatestDate");
		    %>
		    
		    
	<div class="container">

		<!--header block should go here-->

		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner" style="width:1120px !important">
			
			<div class="logo" onclick="gotoMainPage()">MAIN PAGE</div>

			<div class="tfclear"></div>
			<div  class=" mainmenu" >
			<div id="menu_wrapper">
				
				<ul class="main_menu" >
					
					<li></li>
										
					<li id="myMenus1">				
					<a class="myprofile" href="#" onclick="CheckSessionOfAdmin('viewindividual.html')"><span></span> My Individual
					</a>
					</li>
					
					<li id="myMenus2">				
					<a class="mydocuments" href="#" onclick="CheckSessionOfAdmin('createUtiliser.html')">	<span></span>  My Utilizer
					</a>
					</li>
					
					<li id="myMenus5"><a class="mystore" href="#" onclick="CheckSessionOfAdmin('createDomain.html')">
							Domain Hierarchy
					</a></li>
					
					<li id="myMenus6" style="width:140px;"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createKycHierarchy.html')">
							Document Hierarchy
					</a>
					</li>
					
					<li id="myMenus3"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createAdminPattern.html')"> 
							My Patterns
					</a></li>
					
					<li id="myMenus41"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createfeature.html')">  My Features
					</a></li>
					
					<li id="myMenus42"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createplan.html')">  My Plans
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('adminNotification.html')">  NOTIFICATION
					</a></li>
					
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('admin_settings.html')">  SETTINGS
					</a></li>
					
					 
										
				</ul>
				
             </div>
                                   
			</div>

          
		</div>
   
	</header>


				 <div style="margin-top:-50px;margin-left:10px;text-align:right;"> 
	               <a href="logout_admin.html" ><h4 style="color:#05b7f5;">Logout</h4></a>
	             </div>

		<div class="main" ><!-- background-color: #585759; -->
			<div class="main_center">
				<div class="leftside">

					<!--left menus should go here-->
					<div class="user_display">


					 <form>
							<div id="textBox">
								
							</div>
						</form>

					</div>




				</div>


				<div class="rightside" >

					<!--center body should go here-->
					<div id="wishAdmin">
						<h1>Welcome To Clensee Admin</h1>
					</div>
					
					<div class="border_line"></div>
					<div class=" username_bar">
						
						<div id="titleBar">
							<h2>
								<span></span>
								
							</h2>
						</div>
						
					</div>
				
					<div class="module-body" >
						
						<div id="viewUsers" style="height:auto !important;margin-bottom:15px">
   
									
			
										<h1>Maintain the application wisely .</h1>
										
									
										
									 <div class="timeContainer">
												
												<div class="clock">
													
													<div id="Date"></div>
													
														<ul>
																<li id="hours"> </li>
															    <li id="point">:</li>
															    <li id="min"> </li>
															    <li id="point">:</li>
															    <li id="sec"> </li>
														</ul>
												
												</div>
									  </div>
			
			
			                        <br>
			                        <div id="ClsTreeList">Lastest version of CLENSEE updated on <%=versionDate%></div>
			                        <br>
			                        <div>Lastest version of CLENSEE is <%=version%></div>
						</div>

					</div>
					

				</div>
				
			</div>
		</div>
</div>
		
		<!--footer block should go here-->
		
		  
		
		
          <footer class="footer">
		   
		  
				<div class="footer_copyrights">
                <div class="copyrights">    
					 <ul >
                     		
							<li class="footer_space01"><p>Design by:<span> <a href="http://jirehsol.com/">Jireh </a></span></p>
                            <p> � All rights Reserved CLENSEE 2014-15</p>
                             <span>
									<a href="#">Version:<%=version%></a>
									</span>
                            </li>
							<li class="footer_space02"> <p><a href="#">About Us</a> |<a href="#"> Terms and Conditions</a> |<a href="#"> Privacy Policy</a> | <a href="#">Contact Us</a></p></li>
							<li class="footer_space03"> 
                            
                           <div class="four columns">
                                <ul class="social_media">
                                    <li>
                                     <a class="fb" href="https://www.facebook.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="google_plus" href="https://plus.google.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="http://www.linkedin.com/company/1872121?trk=tyah" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li>
                                   
                                    
                                </ul>
                              </div>
                            
                            </li>
						   
						</ul>     
					</div>
                
				</div>
		   
			<div class="footer_line">
			</div>

		 </footer>



<!-- Waiting Div -->
	
	<div id="waitlight">
			
		    <div id="waitdisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:300px;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->




<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#05b7f5;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		        <h3>Your session has expired,Please click on the button to relogin !</h3>      
		         
		         <input type="button" value="Login" onclick="redirectToLogin()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	
	
	<!------container End-------->
</body>
</html>
