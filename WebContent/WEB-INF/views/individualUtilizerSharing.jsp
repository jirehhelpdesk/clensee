 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Sharing Manager</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/kycslide.js"></script>
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css"/>

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1009;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1011;
    
}

</style>

<script>

function showHideDiv1(id){
	

	myTextBox1 = document.getElementById("b6");
	value=myTextBox1.value;
	
	  var obj = document.getElementById(id);
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	    } else if(obj.style.display=="block"){
	      obj.style.display='none';
	    }
	}
</script>


<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}


</style>

<script>

function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		    window.location = "sharingtoUti.html";
		}
	else
		{
		    $("#successcode").hide();
		    $("#fade").hide();
		}
	
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
			 document.getElementById('waitfade').style.display='none';
			    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
			 document.getElementById('waitfade').style.display='none';
			    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         } 
	     
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';
			    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>

<script type="text/javascript">

function viewPro1()
{
		
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';      
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		{		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none")
					    {
					      obj.style.display='block';
					    } 
					    else if(obj.style.display=="block")
					    {
					      obj.style.display='none';
					    }
		    		}		    	 
		    	 else if(res==2)
		    		 {		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);	    		 
		    		 }		    	 
		         },  	    
		    }); 
		}
	
	 if(id.length==0)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
		 	
	    }
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}
</script>


<!--  Script's for utilizer Sharing   -->


<script src="resources/table/jquery-1.11.1.js"></script>

 <link rel="stylesheet"  href="resources/domainTree/css/elements.css" /> 

<script src="resources/domainTree/js/my_js.js"></script>	

<script src="resources/onlineresources/aspnetcdn.com.ajax.jquery-1.7.1.min.js" type="text/javascript"></script>

<script type="text/javascript">

window.document.onkeydown = function (e)
{
    if (!e)
    {
        e = event;
    }
    if (e.keyCode == 27)
    {
        lightbox_close();
    }
}

function lightbox_open()
{
    window.scrollTo(0,0);
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block'; 
}

function lightbox_close(){
	
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';
    
}
 


function showDocumetSelectDiv()
{
   var toggleValue = document.getElementById("addDocumentHiddenValue").value;	
   if(toggleValue=="0")
	   {
	        $("#showDocDiv").slideToggle("slow");
	        document.getElementById("addDocumentHiddenValue").value = "1";
	   }
   else
	   {
	       document.getElementById("addDocumentHiddenValue").value = "0";
	       $("#showDocDiv").slideToggle("slow");
	   }
}
</script>	
<!-- /* End of Pop up Script and css*/ -->

<!-- End of css and script for popup -->


<script type="text/javascript">
      
	function accessDetails(accData) {
				
		var str = document.getElementById("utiComp");
		var arrayData = new String(accData);
		var addOption = document.createElement("option");
		addOption.value = arrayData.valueOf();
		str.add(addOption, null);
		var pattern = "";

		for ( var i = 0; i < str.options.length; i++) {
			pattern = pattern + str.options[i].value + "/";
		}
		
		return pattern;

	}

	/* After geting the user selection pattern this function pass the pattern to utiliser table and find the appropriate uti_kyc_id */

	function findkycid() {
		
		
		var str = document.getElementById('addComp');
		var pattern = "";
		var dom_pat = "";
				
		for ( var i = 0; i < str.options.length; i++) {
			pattern = pattern + str.options[i].value + ">";
		}
				
        pattern = pattern.substring(0, pattern.length-1);
		
		//alert(pattern);
		
		$("#waitingdivId").show();
		
		$
				.ajax({
					type : "Post",
					url : "findkycid.html",
					data : "pattern=" + pattern,
					success : function(response) {
                        
						$("#waitingdivId").hide();
						
						var id = response;
						if (id != " "){
							
							$('#ShowDomain').append("<br>");
							$('#ShowDomain').append("<br><br><input name=\"uti_kycid\" type=\"checkbox\" value=\""+ id+ "\" onClick=\"accessDetails(this.value)\"/>"+ id + "");
							$('#ShowDomain').append("<br><br><h>Reason for Allowing Access</h>");
							$('#ShowDomain').append("<br><br><textarea name=\"des\" style=\"width: 300px; height:100px\" onChange=\"accessDetails(this.value)\"></textarea>");
						
						}
					},
					
				});
	}

	/* After geting the proper utiliser kyc_id  allow access the uti kyc_id */

	function sharetoutilizer() {
		
		    document.getElementById('waitlight').style.display='block';
	        document.getElementById('waitfade').style.display='block';
	        
	        document.getElementById('light').style.display='none';
			document.getElementById('fade').style.display='none';
			
		$.ajax({
			
			type : "Post",
			url : "validateAllowAccess.html",
            success : function(response) {

            	
				if(response=="withinLimit")
					{
					
						var utilisrId = document.getElementById("utiliserId").value;	
						var domainPatternDetails = document.getElementById("defaultPattern").value;	
						var allowComment = document.getElementById("allowComment").value;	
						var newPattern = document.getElementById("addedPattern").value;
						
						if(newPattern!="")
							{
							    newPattern = newPattern.substring(0,newPattern.length-1);
							}
						
						
							$.ajax({
								type : "Post",
								url : "shareToUtilizer.html",
								data : "utilisrId=" + utilisrId+"&pattern="+domainPatternDetails+"&allowComment="+allowComment+"&newPattern="+newPattern,
								success : function(response) {
					
									 document.getElementById('waitlight').style.display='none';
								     document.getElementById('waitfade').style.display='none';
								    
								     document.getElementById('sociallight').style.display='block';
								     document.getElementById('socialfade').style.display='block';
								     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
								     $("#socialdisplaySharedDocDiv").html(""+response+"  <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");  				
								     
									
									 
								},
								
							});
					}
				else
					{
						 document.getElementById('waitlight').style.display='none';
			        	 document.getElementById('waitfade').style.display='none';
			        	
			        	 document.getElementById('sociallight').style.display='block';
					     document.getElementById('socialfade').style.display='block';
					     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
					     $("#socialdisplaySharedDocDiv").html("Your limit is over for sharing documents to utilizer,Migrate your plan to share more. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  									     			        
					}
			},
		
		})
		
		
	}
</script>



<!-- From here all script for user choice to add more Documents in the default list -->

<script type="text/javascript">

</script>

<script type="text/javascript">
function madeAjaxCall(type) {  
	
	$.ajax({
			type : "Post",
			url : "latestDocumentType.html",
			data : "doctype=" + type,			
			success : function(response) {     
								
				var obj = response;
			
				var dat = obj.split(",");
				$('#viewUsers').empty();
				$('#viewUsers').append("<h1><span><font size=\"2\">" + type + "</font></span></h1><br>");			
				for ( var k = 0; k < dat.length; k++) {								
					$('#viewUsers').append("<input  name=\"txtYearValue" + k + "\" id=\"txtYearValue" + k + "\" type=\"checkbox\" value=\"" + dat[k] + "\"    onClick=\"addNewListItem(this," + k + ")\"  />" + dat[k] + "<br />");
				}
			},
			error : function(e) {
				alert('Error: ' + e);
			}			
		});	    
	}
</script>


<!-- Here is the end of user choice doc script -->

<script type="text/javascript">

/* Script for Domain creation  */

function getSubDomainValue(id)
{			
	$("#searchUtiliserDivId").hide();
	$("#searchUtiliserDivId").html("");
			
	var values = "";
	var parentValue = document.getElementById(id).value; 
	
	for(var d=1;d<=10;d++)
		{
		    $("#childRow"+d).hide();
		}
	
	if(parentValue!="Select Your Domain")
		{
				$.ajax
			    ({
					type :"post",				
					url : "subdomaincreateutiliser.html",			
					data :"domainValue="+parentValue,	  
					success : function(response) {
									
						$("#utilisersearchtableDivId").show();
									
						var result = response.split(",");
						values += "<option value=\"Select Sub-Domain\">Select Sub-Domain</option>";
						for(var i=0;i<result.length;i++)
							{
							    var optionarray = result[i];
							    var optionValue = optionarray.split(":");				    
							    values += "<option value=\""+result[i]+"\">"+optionValue[1]+"</option>";
							}
						
						$("#childRow1").show();
						$("#parentId1").html(parentValue);
						$("#cb1").html(values);	
						
						document.getElementById("selectedDomainPattern").value = parentValue + ">";
									
					},
					
				});		
		}
	else
		{
		    $("#utilisersearchtableDivId").hide();
		}	  	
 }


function getHeirarchyFromDomain(id)
{	
	var choiceValue = document.getElementById(id).value;	
	if(choiceValue!="Select Sub-Domain")
		{		
		        var chossenValue = choiceValue.split(":");		
		        
		        var index = id.charAt(id.length-1);
				var nextIndex = +index + 1;
				
				var rowId = "childRow"+nextIndex;
				var parentId = "parentId"+nextIndex;
				var optionId = "cb"+nextIndex;
			   			  
				var values = "";
				
				for(var nxrw=nextIndex;nxrw<=10;nxrw++)
					{
					     $("#"+"childRow"+nxrw).hide();
					}
				
				$("#searchUtiliserDivId").hide();
				
				document.getElementById("cbHidden"+index).value = chossenValue[1];
				
				for(var l = nextIndex;l<=10;l++)
				{
					document.getElementById("cbHidden"+l).value = "";
				}
				
				$.ajax
			    ({
					type :"post",				
					url : "heirarchysubdomain.html",			
					data :"domainValue="+chossenValue[0]+"&currentLevel="+id,	  
					success : function(response) {
						
						var result  = response.split("#");
						var parentValue = result[0];
						
						if(result[1]!="NoChild")
							{
								var optionResult = result[1].split(",");
								values += "<option value=\"Select Sub-Domain\">Select Sub-Domain</option>";
								
								for(var i=0;i<optionResult.length;i++)
									{
									    var optionarray = optionResult[i];
									    var optionValue = optionarray.split(":");
									    values += "<option value=\""+optionResult[i]+"\">"+optionValue[1]+"</option>";							    
									}
								
								$("#"+rowId).show();
								$("#"+parentId).html(parentValue);
								$("#"+optionId).html(values);																	
					        }
						
						var choosenValue = document.getElementById("selectedDomainPattern").value;						
						var choosenValueArray = choosenValue.split(">");						
						var newChoosenValue = "";						
						for(var cv=0;cv<choosenValueArray.length;cv++)
							{
							    if(index==cv)
							    	{
							    	    newChoosenValue  += parentValue + ">";
							    	}
							    else
							    	{
							    	    newChoosenValue += choosenValueArray[cv] + ">";
							    	}
							}						
						        document.getElementById("selectedDomainPattern").value  = newChoosenValue;		
						        
						        domainSlectionOpeation();				       						        
					},					
				});					
		}
	else
		{
		     alert("Choose Sub Domain !");
		}
}

function domainSlectionOpeation()
{	
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
    var finanldomainPattern = "";
	finanldomainPattern =  document.getElementById("cb").value;
	 
    for(var i=1;i<=10;i++)
    	{
    	   var hiddenValue = document.getElementById("cbHidden"+i).value;
    	   if(hiddenValue!="")
    		   {
    		       finanldomainPattern += ">"+hiddenValue;
    		   }
    	}
    
	$.ajax
    ({
		type :"post",				
		url : "searchUtilizerFromInd.html",			
		data :"domainPattern="+finanldomainPattern,	  
		success : function(response){
			
			
			
			if(response.indexOf("No Utilizer found as per your selection criteria !")>0)
				{
					document.getElementById('waitlight').style.display='none';
			     	document.getElementById('waitfade').style.display='none';
			     	
				     $("#searchUtiliserDivId").hide();
				     $("#searchUtiliserDivId").html("No Utilizers found yet, continue ...");
				}
			else
				{
					document.getElementById('waitlight').style.display='none';
			     	document.getElementById('waitfade').style.display='none';
			     	
					$("#searchUtiliserDivId").show();	
					$("#searchUtiliserDivId").html(response);
				}
			
		},		
	});		
}

function openAllowPopop(id)
{
	document.getElementById('waitlight').style.display='block';
 	document.getElementById('waitfade').style.display='block';
 	
 	
	var data = id.split("##");
	var name = data[0];
	var patternId = data[1];
	var domainPattern = data[2];
    var utiliserId = data[3];
	var utiAddress = data[4];
    
	$.ajax
    ({
		type :"post",				
		url : "getPatternfromId.html",			
		data :"patternId="+patternId+"&utiAddress="+utiAddress+"&name="+name+"&domainPattern="+domainPattern+"&utiliserId="+utiliserId,	  
		success : function(response){
		     
				document.getElementById('waitlight').style.display='none';
	     		document.getElementById('waitfade').style.display='none';
	     	
				$("#divContents").html(response);
			     
			    document.getElementById('light').style.display='block';
 			    document.getElementById('fade').style.display='block';	
 			    
	      },		
	});	
	
}


function closePopup()
{   	
	document.getElementById('light').style.display='none';
	document.getElementById('fade').style.display='none';
}

/* End of Script for Domain Selection  */


/* Start Script for choosing own documents  */


/* End of Script for choosing own documents  */
 


/* End of script for utilizer sharing */



/*home page*/

function myhome(handlerToHit) {

	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/
       
	/* All scripts for Access Manager */
	
	function generateCode(handlerToHit) {
		
		document.getElementById("Hi-B-1").className = "generatcodeiconsactive";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "";
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Server Problem Try again Later !');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Generate Code</span></h2>");
	}
	
	function allowAccess(handlerToHit) {
 		
		
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "allowaccessiconactive";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "";
		
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					 
					$('#suggestions1').show();
					$("#ProfileHeadingDiv").show();
					$("#c-body-div").show();

							
							
					$("#leftsideBasicInfoDiv").show();					
					$('#searchhomepage').hide();
				 	
					document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';
				    
					$("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Allow Access</span></h2>");
	}
	
	
function revokeAccess(handlerToHit) {
 		
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "revokecodeiconactive";
		document.getElementById("Hi-B-5").className = "";
		
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	    
	    $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
								$.ajax({
									type : "post",
									url : handlerToHit,
									cache : false,
									success : function(response) {		
										 
										document.getElementById('waitlight').style.display='none';
									    document.getElementById('waitfade').style.display='none';
									    
										$('#suggestions1').show();
										$("#ProfileHeadingDiv").show();
										$("#center-body-div").show();
										$("#leftsideBasicInfoDiv").show();					
										$('#searchhomepage').hide();
										
										$("#viewUsers1").html(response);
										
										getIndAccDetails();
										
									},
									error : function() {
										alert('Error while fetching response');
									}
								});
							 		 		 		
							$("#titleBar").html("<h2><span>Revoke Code</span></h2>");
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
		 
	}
	
	function accessHistory(handlerToHit) {
 		
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	    
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "sharingdocumenthisticonactive";
						
		$.ajax({
			
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
								$.ajax({
									
									type : "post",
									url : handlerToHit,
									cache : false,
									success : function(response) {		
																				
										document.getElementById('waitlight').style.display='none';
									    document.getElementById('waitfade').style.display='none';
									    
										$('#suggestions1').show();
										$("#ProfileHeadingDiv").show();
										$("#center-body-div").show();
										$("#leftsideBasicInfoDiv").show();					
										$('#searchhomepage').hide();
																				
										$("#viewUsers1").html(response);
										
										IndHistory();
										 
									},
									error : function() {
										alert('Error while fetching response');
									}
									
								});
							 		 		 		
							$("#titleBar").html("<h2><span>Sharing History</span></h2>");
					}
					else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
				}
			}); 		 
     	}
	
		
	 /* End of AccessManger scripts */

	
</script>


<script type="text/javascript">

function viewHomeProfile(url) 
{
	window.location = url;	 
}	

function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}	


</script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
 
 
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
 
 
 <!-- Start Script for Recipent Details -->
 
 <script type="text/javascript">
 
 
/* Main Header Actions  */
 
 function gotoProfile()
 {
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoMyDocument()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					   window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoSharingManager()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	
 }
 
 function gotoStore()
 {	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 
 function gotoAlerts()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
 }
 
 
 function gotoPlans()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
				
			}
		}); 		 
 }
 
 
 function gotoSetings()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}


function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}
 
 
function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}


function showSearchBar(parameter)
{
    $("#generatecodesearch").show();	
    document.getElementById("name").value = "";
    document.getElementById("name").placeholder = "Search Individual Via "+parameter;
    $("#searchDetails").hide(); 
}


 </script>

 
 <!-- End Of Script for Recipent Details -->
 
</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocuments"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanagerfocus" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- 
					
					<li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li>
					
					-->
					
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
           
           
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
           
            </div>
            
               
                <div style="clear:both"></div>
	                <div id="loginBox">  
	                          <span></span>   
	                    <form id="loginForm">
	                                          
	                        <fieldset id="body">
	                        
	                        <fieldset>                       
	                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
	                        </fieldset>                                                
	                        </fieldset>
	                       
	                    </form>
	                </div>
            </div>
            <!-- Login Ends Here -->
		</header>
		
	<div class="main">
				                      
			<div class="main_center">
						
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
								  <ul>
								      <li id="generatcodeicons"><a href="generateCode.html" id="Hi-B-1"  ><span> Individual Sharing</span></a></li>
									  <li id="applycodeicon"><a href="applyCode.html" id="Hi-B-2" ><span>Documents Received</span></a></li>
									  <li id="allowaccessicon"><a href="sharingtoUti.html" id="Hi-B-3"  class="allowaccessiconactive"><span> Utilizer Sharing</span></a></li>
									  <li id="revokecodeicon"><a href="#" id="Hi-B-4" onclick="revokeAccess('revokeAccess.html')"><span>Revoke Sharing </span></a></li>
									  <li id="sharingdocumenthisticon"><a href="#" id="Hi-B-5" onclick="accessHistory('accessHistory.html')"><span>Sharing History </span></a></li>
								 </ul>
							</div>     		
					</div>
					
					 </div>
					
					<div class="rightside">
					
					
					<!-- Advertisment Div for Entire Page  -->
																		
						  <%
							 
						        //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
						  							  	
						  %>
									
					<div id="ProfileHeadingDiv" style="display: block;">
						
							<marquee behavior="scroll" scrollamount="5" direction="left">
								<h1 class="rightsideheader">
		
									<%=indAdvertise%>
									
								</h1>
							</marquee>
						
					</div>

											        		
			<!-- Advertisment Div for Entire Page  -->
					
	      <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span>Allow Access</span>								   
							</h2>
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
										
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
									
					
											<%
											String domain = (String)request.getAttribute("domainType");
											String domainType[] = domain.split(",");
											%>
											       
											 <div id="popupContact"> 
											
												<!-- contact us form -->
													<form id="domainForm" name="domainSelectionForm" id="form" >
														
														<h3>Select domain to find Utilizer</h3>
														<br>
														
														<form id="form1">
											
											    <div>
											
											             <select id="cb" onchange="getSubDomainValue(this.id)" >
											         
											                             <option value="Select Your Domain">Select Your Domain</option>
																	<%for(int i=0;i<domainType.length;i++){ %>
																	     
																	     <option value="<%=domainType[i]%>"><%=domainType[i]%></option>
																	
																	<%}%>
											             </select>
											
											               <div id="utilisersearchtableDivId" style="display:none;">
											               
														        <table id="table1">
														
														            <tbody>
																	
														              
																	<tr id="childRow1" style="display:none;color:#000;"><td id="parentId1" class="parentbgcolorone" width="150p" ></td>			 
																	 <td>						    
																		<select id="cb1" onchange="getHeirarchyFromDomain(this.id)"></select>
																		<input type="hidden" id="cbHidden1" />
														             </td>
														             
														            </tr>
														           
														           <tr id="childRow2" style="display:none;color:#000;"><td id="parentId2"></td>			 <!--  class="parentbgcolortwo" -->
																	 <td>
																		<select id="cb2" onchange="getHeirarchyFromDomain(this.id)"></select>
																		<input type="hidden" id="cbHidden2" />
														             </td>
														            </tr>
														            
														            <tr id="childRow3" style="display:none;color:#000;"><td id="parentId3"></td>			 
																	 <td>
																		<select id="cb3" onchange="getHeirarchyFromDomain(this.id)"></select>
																		<input type="hidden" id="cbHidden3" />
														             </td>
														            </tr>
														            
														            <tr id="childRow4" style="display:none;color:#000;"><td id="parentId4"></td>		<!--  class="parentbgcolortwo"	  -->
																	 <td>
																		<select id="cb4" onchange="getHeirarchyFromDomain(this.id)"></select>
																		<input type="hidden" id="cbHidden4" />
														             </td>
														            </tr>
														            
														            <tr id="childRow5" style="display:none;color:#000;"><td id="parentId5"></td>			 
																	 <td>
																		<select id="cb5" onchange="getHeirarchyFromDomain(this.id)"></select>
																		<input type="hidden" id="cbHidden5" />
														             </td>
														            </tr>
														              
														            <tr id="childRow6" style="display:none;color:#000;"><td id="parentId6" ></td>		<!-- 	class="parentbgcolortwo"  -->
																	 <td>
																		<select id="cb6" onchange="getHeirarchyFromDomain(this.id)"></select>
																		<input type="hidden" id="cbHidden6" />
														             </td>
														            </tr>   
														             
														            <tr id="childRow7" style="display:none;color:#000;"><td id="parentId7"></td>			 
																	 <td>
																		<select id="cb7" onchange="getHeirarchyFromDomain(this.id)"></select>
																		<input type="hidden" id="cbHidden7" />
														             </td>
														            </tr>   
														             
														            <tr id="childRow8" style="display:none;color:#000;"><td id="parentId8" ></td>		<!-- 	 class="parentbgcolortwo" -->
																	 <td>
																		<select id="cb8" onchange="getHeirarchyFromDomain(this.id)"></select>
																		<input type="hidden" id="cbHidden8" />
														             </td>
														            </tr>  
														            
														            <tr id="childRow9" style="display:none;color:#000;"><td id="parentId9"></td>			 
																	 <td>
																		  <select id="cb9" onchange="getHeirarchyFromDomain(this.id)"></select>
																		  <input type="hidden" id="cbHidden9" />
														             </td>
														            </tr>  
														            
														            <tr id="childRow10" style="display:none;color:#000;"><td id="parentId10" ></td>			<!--  class="parentbgcolortwo" -->
																	 <td>
																		  <select id="cb10"></select>
																		  <input type="hidden" id="cbHidden10" />
														             </td>
														             
														            </tr>  
														                 		              
														           </tbody>
														
														        </table>
														           
														          <span >
														          
														          <input type="hidden" id="selectedDomainPattern" />
														                 <!-- <input type="button"  id="submit"  value="Search Utilizer"  onclick="domainSlectionOpeation()"/>	 -->          
														          </span>
														          
														    </div>      
											    </div>
											         
											    </form>										
													</form>
													
											 </div> 
											 
											 <!-- Code for searched utiliser details -->
											     
											     <div id="searchUtiliserDivId">
											     
											     
											     </div>  
											 
											 <!-- End of Code for searched utiliser details -->
											 
											 <!-- Popup div ends here -->
											
												<input type="hidden" id="patternDocsHiddenValueId" value="" />
																
												
												<div id="light" class="allowacce_light" style="width: 870px;">
												
												
												<div class="colsebutton" onclick="lightbox_close()"><img height="22" width="22" src="resources/images/close_button.png"></div>
													
												<div id="divContents"></div>
														  	
																	
												</div>
												
												<div id="fade"></div> 	<!-- onClick="lightbox_close();" -->
												  
											
												<!-- EOF code for  allow access show a pop up -->

						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

 	
	    
</div> <!--end of right div  -->

	</div>

</div>
					
					
					
					
					
		<%
			ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			String version=resource.getString("kycversion");
			String versionDate=resource.getString("kyclatestDate");
			%>			
					
					
					
					
			
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">

							 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									

                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->
	
	
	
	
	
	
   <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	<!-- session Div -->
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->

   
</body>
</html>
     	