<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Views Documents</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>



<script src="resources/kyc_scripts/jquery-1.11.1.js"></script>

  
</head>
<body>
<%
String year  = (String)request.getAttribute("year");
String ind_id= (String)request.getAttribute("ind_id");
String dir = (String)request.getAttribute("dir");
String docname = (String)request.getAttribute("docname");

String documentComponentValue = (String)request.getAttribute("documentComponentValue");

System.out.println("Document components are "+documentComponentValue);

//docname = docname.replaceAll(" " , "");
System.out.println(docname);
System.out.println(year+"  "+ind_id+" "+dir+" "+docname);
%>



<%if(dir.equals("KYC_DOCUMENT"))
{
	System.out.println("the docName is "+docname);
	
	String documentComponentValueArray[]  =  documentComponentValue.split(",");
%>

     <div class="reciveddocument"><%=docname.split("_")[0]%></div>

	<img src="${pageContext.request.contextPath}<%="/previewDocs_apply_code.html?fileName="+docname+"&docCategory=KYC_DOCUMENT"+"&year="+year+"&ind_id="+ind_id%>" width="670" height="421"></img>
	
	
	
	<div>
	 
	  <%-- <table style="text-align:left;">
			<tr>
			<td><b>KYC ID</b></td>
			<td>:</td>
			<td><%=documentComponentValueArray[1]%></td>
			
			
			</tr>
			
			<tr>
			<td><b>Document Description</b></td>
			<td>:</td>
			<td><%=documentComponentValueArray[2]%></td>
			
			</tr>
			
			<tr>
			<td><b>Document Uploaded Date</b></td>
			<td>:</td>
			<td><%=documentComponentValueArray[3]%></td>
		
			</tr>
	  </table> --%>
	  
	</div>
	
	
<%}%>

<%if(dir.equals("ACADEMIC_DOC"))
{
	String documentComponentValueArray[]  =  documentComponentValue.split(",");
%>
     <div class="reciveddocument"><%=docname.split("_")[0]%></div>

	<%if(!docname.contains("jpeg") && !docname.contains("jpg") && !docname.contains("gif") && !docname.contains("png")) { %>
																																									             
             <%if(docname.contains("pdf")) {%>
             
                 <img src="resources/images/PNA_PDF.png"  width="670" height="421" /></img>
                 		
             <%}else if(docname.contains("doc")) {%>
             
                 <img src="resources/images/PNA_DOC.png"  width="670" height="421" /></img>
             
             <%}else if(docname.contains("docx")) {%>
             
                 <img src="resources/images/PNA_DOC.png"  width="670" height="421" /></img>
             
             <%}else if(docname.contains("xls")) {%>
             
                 <img src="resources/images/PNA_XLS.png"  width="670" height="421" /></img>
              
             <%}else { %>
             
                 <img src="resources/images/PNA_XLS.png"  width="670" height="421" /></img>
             
             <%} %>
             
    <%}else { %>      
	
	<img src="${pageContext.request.contextPath}<%="/previewDocs_apply_code.html?fileName="+docname+"&docCategory=ACADEMIC_DOC"+"&year="+year+"&ind_id="+ind_id%>" width="670" height="421"></img>
	
	<%} %>
	
   <%--  <div class="reciveddocument">Document Name :<%=docname.split("_")[0]%></div>
    
    <div>
	 
	  <table style="text-align:left;">
			
		<%for(int i=0;i<documentComponentValueArray.length;i++){
			
			String dynanmicComponent[] = documentComponentValueArray[i].split(":");
			%>	
		
			<tr>
			<td><b><%=dynanmicComponent[0]%></b></td>
			<td>:</td>
			<td>
			<%=dynanmicComponent[1]%>
			</td>
			</tr>
				
		<%} %>						
	  </table>
	  
	</div> --%>
    
<%}%>

<%if(dir.equals("Financial_DOC"))
{
	String documentComponentValueArray[]  =  documentComponentValue.split("&&");
	
	%>
	<div class="reciveddocument"><%=docname.split("_")[0]%></div>
    
    
    
	<%if(!docname.contains("jpeg") && !docname.contains("jpg") && !docname.contains("gif") && !docname.contains("png")) { %>
																																									             
             <%if(docname.contains("pdf")) {%>
             
                 <img src="resources/images/PNA_PDF.png"   width="670" height="421" /></img>  <!-- width="670" height="421" -->
                 		
             <%}else if(docname.contains("doc")) {%>
             
                 <img src="resources/images/PNA_DOC.png"   width="670" height="421" /></img>
             
             <%}else if(docname.contains("docx")) {%>
             
                 <img src="resources/images/PNA_DOC.png"   width="670" height="421" /></img>
             
             <%}else if(docname.contains("xls")) {%>
             
                 <img src="resources/images/PNA_XLS.png"   width="670" height="421" /></img>
              
             <%}else { %>
             
                 <img src="resources/images/PNA_XLS.png"   width="670" height="421" /></img>
             
             <%} %>
             
    <%}else { %>      
	
	
	<img src="${pageContext.request.contextPath}<%="/previewDocs_apply_code.html?fileName="+docname+"&docCategory=Financial_DOC"+"&year="+year+"&ind_id="+ind_id%>"  width="670" height="421"></img>
	
	<%} %>
	
    <%-- <div class="reciveddocument">Document Name :<%=docname.split("_")[0]%></div>
    
    
    <div>
	 
	  <table style="text-align:left;">
			 <tr>
			 <td>
			<b>Document Financial Year</b>
			</td>
			<td>:</td>
			<td>
			<%=documentComponentValueArray[0]%>
			</td>
			</tr>
			
			<tr>
			<td>
			<b>Financial Document Type</b>
			</td>
			<td>:</td>
			<td>
			<%=documentComponentValueArray[1]%>
			</td>
			</tr>
			
			<tr>
			<td>
			
			<b>Financial Document Mode:</b>
			</td>
			<td>:</td>
			<td>
			<%=documentComponentValueArray[2]%>
			</td>
			</tr>
	  </table>
	  
	</div> --%>
	
<%}%>

<%if(dir.equals("Employement_DOC"))
{
	String documentComponentValueArray[]  =  documentComponentValue.split("&&");
%>
	<div class="reciveddocument"><%=docname.split("_")[0]%></div>
	
	<%if(!docname.contains("jpeg") && !docname.contains("jpg") && !docname.contains("gif") && !docname.contains("png")) { %>
																																									             
             <%if(docname.contains("pdf")) {%>
             
                 <img src="resources/images/PNA_PDF.png"   width="670" height="421" /></img>
                 		
             <%}else if(docname.contains("doc")) {%>
             
                 <img src="resources/images/PNA_DOC.png"   width="670" height="421" /></img>
             
             <%}else if(docname.contains("docx")) {%>
             
                 <img src="resources/images/PNA_DOC.png"   width="670" height="421" /></img>
             
             <%}else if(docname.contains("xls")) {%>
             
                 <img src="resources/images/PNA_XLS.png"   width="670" height="421" /></img>
              
             <%}else { %>
             
                 <img src="resources/images/PNA_XLS.png"   width="670" height="421" /></img>
             
             <%} %>
             
    <%}else { %> 
       
         
	<%System.out.println("It hits");%>
	
	<img src="${pageContext.request.contextPath}<%="/previewDocs_apply_code.html?fileName="+docname+"&docCategory=Employment_DOC"+"&year="+year+"&ind_id="+ind_id%>"  width="670" height="421"></img>
	
	<%} %>
    <%-- <div class="reciveddocument">Document Name :<%=docname.split("_")[0]%></div>
    
     <div>
	 
	  <table style="text-align:left;">
			 <tr>
			 <td>
			<b>Organization name</b>
			</td>
			<td>:</td>
			<td>
			<%=documentComponentValueArray[0]%>
			</td>
			</tr>
			
			<tr>
			<td>
			<b>Job Designation</b>
			</td>
			<td>:</td>
			<td>
			<%=documentComponentValueArray[1]%>
			</td>
			</tr>
			
			<tr>
			<td>
			<b>Job Duration</b>
			</td>
			<td>:</td>
			<td>
			<%=documentComponentValueArray[3]%>- to <%=documentComponentValueArray[4]%>
			</td>
			</tr>
	  </table>
	  
	</div> --%>
    
<%}%>

</body>
</html>