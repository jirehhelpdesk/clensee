<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Individual Search Result</title>

	
<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>


<script>

	function openSearchedProfileinNewTab(url)
	{		
	     var win = window.open(url, '_blank');
		 win.focus();		
	}

</script>

</head>

<body>
<% 
List<String> indResult = (List<String>)request.getAttribute("indSearchResult"); 
%>

<%if(indResult.size()>2){ %>

   <div>
   
       <ul>
                                
              <%for(int i=0;i<indResult.size();i++) {%>
              
               <li> 
                   <%String address = indResult.get(6 + i); %>
                    
                   <%if(!indResult.get(5 + i).equals("no image")) {%> 
                   
                     <%
                        String arr1 = indResult.get(4 + i);
						String arr2[] = arr1.split(" ");
						String date[] = arr2[0].split("-");
						String year = date[0];
                     %>
                     
	          		<div class="namedeatils-photo" onclick="openSearchedProfileinNewTab('validateprofileviewingForutilizer.html?asdgth=<%=indResult.get(0 + i)%>')">
	          		
	          				<img src="${pageContext.request.contextPath}<%="/previewprofileOther.html?fileName="+indResult.get(5 + i)+"&docCategory=Profile_Photo&year="+year+"&indId="+indResult.get(0 + i)+""%>" width="35px" height="35px" ></img>
	          		
	          		</div>
	          		
	          		<%}else { %>
	          		
	          		<div class="namedeatils-photo" onclick="openSearchedProfileinNewTab('validateprofileviewingForutilizer.html?asdgth=<%=indResult.get(0 + i)%>')">
	          		
	          				<img src="resources/kyc_images/Koala.jpg" width="35px" height="35px"  ></img>
	          		
	          		</div>
	          		
	          		<%} %>
	          		
	          		<div class="namedeatils" onclick="openSearchedProfileinNewTab('validateprofileviewingForutilizer.html?asdgth=<%=indResult.get(0 + i)%>')">
	          		
	          		         <p><b></b><strong></strong><%=indResult.get(1 + i)%> <%=indResult.get(2 + i)%> <%=indResult.get(3 + i)%></p>
	          		         <span><b></b><strong></strong><%=address%></span>
	          		         
	          		</div>
	          		
	          		<img src="resources/images/stock-photo-arrow-top-icon.png" border="0" title="View Profile" onclick="openSearchedProfileinNewTab('validateprofileviewingForutilizer.html?asdgth=<%=indResult.get(0 + i)%>')" style="margin-top:0px;width:20px;" >
	          		
	          		<%-- <div class="namedeatils-button"><input type="button" value="View Profile" onclick="openSearchedProfileinNewTab('validateprofileviewingForutilizer.html?asdgth=<%=indResult.get(0 + i)%>')" /></div>
	          	 --%>
	          	  </li>
	          	  
	          	<%i = i+6;%>
	          <%} %>	
        
          
      </ul>
      
  </div>

	<%}else {%>
		<div>
			      <h1>Sorry, no results found for this search !</h1>
			</div>
	<%} %>

</body>
</html>