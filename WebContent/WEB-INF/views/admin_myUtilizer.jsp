                                                            <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<%@ page language="java" import="java.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Admin My Utilizer</title>
<link href="admin_resources/kyc_css/kyc_admin_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="admin_resources/kyc_css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">

<link rel="stylesheet" href="admin_resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script type="text/javascript"src="admin_resources/kyc_scripts/demo.js"></script>
<link rel="stylesheet" href="admin_resources/css/logout.css">	


	
<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="admin_resources/kyc_css/utiliserCreateTable.css">
<link rel="stylesheet"  href="admin_resources/domainTree/css/elements.css" />
<script src="admin_resources/domainTree/js/my_js.js"></script>	
<script src="admin_resources/kyc_scripts/ajax.jquery-min-1.71.js" type="text/javascript"></script>
	
	
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />
	
	
<style>

#sessionfade{

    background-color: #000;
    height: 100%;
    left: 0;
    opacity: 0.7;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1001;
}

#sessionlight{

    background: #fff none repeat scroll 0 0;
    border-radius: 24px;
    height: auto;
    left: 43%;
    margin-left: -211px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: auto;
    z-index: 1002;
    right: 400px !important;

}

.adminleftMenuActive
{
	color:#00b6f5 ! important;
}


</style>	


<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}
</style>
	
	
	
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
$(document).ready(function () {
    $("#menu_wrapper").on("click", "ul.main_menu>li", function (event) {
        debugger;
        event.preventDefault();
        $("#menu_wrapper .active").removeClass("active");
        $(this).find('a:first').addClass("active");
    });
});
});//]]>  



function redirectToLogin()
{
	window.location = "logout_admin.html";	
}


</script>

<script type="text/javascript">
	function viewUsers() {
		
		$("#waitingdivId").show();
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						
						   $.ajax({
								type : "post",
								url : "sendAjax.html",
								cache : false,
								success : function(response) {
									
									$("#waitingdivId").hide();
									
									var usersTokens = response.split(",");
									var index = 0;
									var usersTableData = "<table style=\"width:840px\"><tr><th>S.NO</th><th>KYC-ID</th><th>FIRST-NAME</th><th>LAST-NAME</th><th>EMAIL-ID</th><th>MOBILE-NUMBER</th></tr>";
									for ( var i = 0; i < usersTokens.length; i = i + 5) {
										index += 1;
										usersTableData += "<tr><td>" + (index)
												+ "</td><td>" + usersTokens[0 + i]
												+ "</td><td>" + usersTokens[1 + i]
												+ "</td><td>" + usersTokens[2 + i]
												+ "</td><td>" + usersTokens[3 + i]
												+ "</td><td>" + usersTokens[4 + i]
												+ "</td></tr>";
									}
									usersTableData += "</table>";
									$('#viewUsers').html(usersTableData);
									$("#titleBar").html("<h2><span>View Users</span></h2>");
								},
								error : function() {
									
									$("#waitingdivId").hide();
									
									alert('Your request has failed due to some reason please try again later !');
								}
							});
					   
					}
				else
					{
					     $("#waitingdivId").hide();
					     
					     $("#sessionlight").show();
					     $("#sessionfade").show();
					}
			}
		});
	
	}
	
	
	
	function viewUti(UrlLink)
	{
		$("#waitingdivId").show();
		
		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "adminleftMenuActive";
		document.getElementById("3stId").className = "";
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						 $.ajax({
						        url: "viewUtiliser.html",
						        type: 'POST',	       
						        success: function (data) {   
						        	
						        	$("#waitingdivId").hide();
						        	
						            $("#viewUsers")
									.html(data);
									$("#titleBar").html("<h2><span>View Utilizer</span></h2>");
						        },
						        
						    });
					}
				else
					{
					   $("#waitingdivId").hide();
					   
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
		
		
	}
	
	
	function organiseUti(UrlLink)
	{
		$("#waitingdivId").show();
		
		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "";
		document.getElementById("3stId").className = "adminleftMenuActive";
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
					        url: "organiseUtiliser.html",
					        type: 'POST',	       
					        success: function (data) {  
					        	
					        	$("#waitingdivId").hide();
					        	
					            $("#viewUsers")
								.html(data);
								$("#titleBar").html("<h2><span>Organise Utilizer</span></h2>");
					        },
					        
					    });
					}
				else
					{
					    $("#waitingdivId").hide();
					
					    $("#sessionlight").show();
				        $("#sessionfade").show();
					}
			}
		});	 
	}
	
	
	function CheckSessionOfAdmin(hitTheUrl)
	{
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = hitTheUrl;
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
					
	}
	
	
	
	
	function save_Utiliser() {
		
		$("#waitingdivId").show();
		
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
					      
					      var flag = 'true';      
					
					      var regKycId = document.getElementById("regkycId").value;					     
					      var firstName = document.getElementById("firstNameId").value;
					      var middleName = document.getElementById("middleNameId").value;
					      var lastName = document.getElementById("lastNameId").value;
					      var gender = document.getElementById("genderId").value;
					      
					      var emailid = document.getElementById("emailidId").value;
					      var ofcemail = document.getElementById("ofcemailIdId").value;
					      var mobileNo = document.getElementById("ofcphno1Id").value;
					      var ofcphno = document.getElementById("ofcphno2Id").value;
					      var ofcaddress = document.getElementById("ofcaddressId").value;
					      var headofcaddress = document.getElementById("headofcaddressId").value;
					      
					      var userPhoto = document.getElementById("userPhotoId").value;
					      var pattern_name = document.getElementById("pattern_nameId").value;
					      var plan_domain = document.getElementById("plan_domainId").value;
					      var plan_name = document.getElementById("plan_nameId").value;
					      var domain = document.getElementById("domainPatternId").value;	
						 
					      if(regKycId=="")
					    	  {					    	     
					    	     flag = 'false'; 
					    	     $("#waitingdivId").hide();
					    	     $("#erroeMessageId").html("Please enter Your register KYCID !");  
					    	     return false;
					    	  }
					      
					      if(firstName=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please enter Your First Name !");  
				    	     return false;
				    	  }  
					      
					      if(lastName=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please enter Your Last Name !");  
				    	     return false;
				    	  }
					     
					      if(gender=="Select Gender")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please Select your gender !");  
				    	     return false;
				    	  }
					      
					      if(emailid=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please enter Your email Id !");  
				    	     return false;
				    	  }
					      
					      if(ofcemail=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please enter Your office email Id !");  
				    	     return false;
				    	  }
					      
					      if(mobileNo=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please enter Your Mobile No !");  
				    	     return false;
				    	  }
					       
					      if(ofcphno=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please enter Your office phone number !");  
				    	     return false;
				    	  }
					      
					      if(ofcaddress=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please enter Your office Address !");  
				    	     return false;
				    	  }
					      
					      if(headofcaddress=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please enter Your Head office Adress !");  
				    	     return false;
				    	  }
					      
					      if(userPhoto=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please brows a profile Photo !");  
				    	     return false;
				    	  }
					      
					      if(pattern_name=="Select Pattern Name")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please select a Pattern Name !");  
				    	     return false;
				    	  }
					      
					      if(plan_domain=="Select a Plan Domain")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please select a Plan Domain !");  
				    	     return false;
				    	  }
					     	
					      if(plan_name=="Select Plan")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please select a Plan Domain !");  
				    	     return false;
				    	  }
					      
					      if(domain=="")
				    	  {					    	     
				    	     flag = 'false'; 
				    	     $("#waitingdivId").hide();
				    	     $("#erroeMessageId").html("Please select a Domain for this utilizer !");  
				    	     return false;
				    	  }
					      
					      if(flag=='true')
					    	 {
						    	 var formData = new FormData($("#createUtiForm")[0]);
								 
									
								    formData.append("domain",domain);
								    $.ajax({
								        url: "saveUtiliser.html",
								        type: 'POST',
								        data: formData,
								        //async: false,
								        success: function (data) {          
								            
								        	$("#waitingdivId").hide();
								        	
								        	alert(data);
								        	CheckSessionOfAdmin('createUtiliser.html');
								        	
								        },
								        cache: false,
								        contentType: false,
								        processData: false
								    });
					    	 }
					      
					}
				else
					{
					   $("#waitingdivId").hide();
					
					   $("#sessionlight").show();
				           $("#sessionfade").show();
					}
			}
		});
		
		

		  
		
	}
		
		
	function getSubDomainValue(id)
	{
		var values = "";
		var parentValue = document.getElementById(id).value; 
		
		for(var d=1;d<=10;d++)
			{
			    $("#childRow"+d).hide();
			}
		$.ajax
	    ({
			type :"post",				
			url : "subdomaincreateutiliser.html",			
			data :"domainValue="+parentValue,	  
			success : function(response) {
				
				var result = response.split(",");
				values += "<option value=\"Select Sub-Domain\">Select Sub-Domain</option>";
				for(var i=0;i<result.length;i++)
					{
					    var optionarray = result[i];
					    var optionValue = optionarray.split(":");				    
					    values += "<option value=\""+optionValue[0]+"\">"+optionValue[1]+"</option>";
					}
				
				$("#childRow1").show();
				$("#parentId1").html(parentValue);
				$("#cb1").html(values);	
			},
			
		});		
			  	
	}

	function getHeirarchyFromDomain(id)
	{
		var choiceValue = document.getElementById(id).value;
		
		if(choiceValue!="Select Sub-Domain")
			{
					
			        var index = id.charAt(id.length-1);
					var nextIndex = +index + 1;
					
					var rowId = "childRow"+nextIndex;
					var parentId = "parentId"+nextIndex;
					var optionId = "cb"+nextIndex;
				   			  
					var values = "";
									
					$.ajax
				    ({
						type :"post",				
						url : "heirarchysubdomain.html",			
						data :"domainValue="+choiceValue+"&currentLevel="+id,	  
						success : function(response) {
							
							var result  = response.split("#");
							var parentValue = result[0];
							
							if(result[1]!="NoChild")
							{
								var optionResult = result[1].split(",");
								values += "<option value=\"Select Sub-Domain\">Select Sub-Domain</option>";
								
								for(var i=0;i<optionResult.length;i++)
									{
									    var optionarray = optionResult[i];
									    var optionValue = optionarray.split(":");
									    values += "<option value=\""+optionValue[0]+"\">"+optionValue[1]+"</option>";							    
									}
								
								$("#"+rowId).show();
								$("#"+parentId).html(parentValue);
								$("#"+optionId).html(values);	
							}
						},
						
					});	
					
			}
		else
			{
			        alert("Choose Sub Domain !");
			}
		
	}

	function domainSlectionOpeation(id)
	{
		if(id=="close")
			{
				var obj = document.getElementById('abc'); 	
				obj.style.display='none';
			}
		if(id=="submit")
			{		
			   var numOfElements = document.domainSelectionForm.elements.length - 1;		
			   var domainLevelValues = "";
			   for(var i=0;i<numOfElements;i++)
				   {
				       var elementValue = document.domainSelectionForm.elements[i].value;			       	      
				       if(elementValue!="")
				    	   {
				               domainLevelValues += elementValue + ",";
				    	   }			       
				   }
			   
			   domainLevelValues = domainLevelValues.substring(0, domainLevelValues.length-1);
			   
			   $.ajax
			    ({
					type :"post",				
					url : "getselectiondomainName.html",			
					data :"selectedList="+domainLevelValues,	  
					success : function(response) {
						
						var values = "";
						var heirarchyNames = response.split(",");
						var domainDirectory = "";
						
						for(var d=0;d<heirarchyNames.length;d++)
						{					  
							domainDirectory += heirarchyNames[d]+">";
						}
						domainDirectory = domainDirectory.substring(0,domainDirectory.length-1);					
						
						values += "<ul style=\"margin-left:-20px;margin-top: 10px;\"><li>";
						values += "--><b>"+heirarchyNames[0]+"       (Main Domain)</b>";
						
						for(var t=1;t<heirarchyNames.length;t++)
							{
							    values += "<ul style=\"margin-left:-20px;margin-top: 10px;\"><li>";
							    values += "-->"+heirarchyNames[t];						    
							}	
						
						for(var t=0;t<heirarchyNames.length;t++)
							{
							    values += "</li></ul>";					  	    
							}	
						
						$("#displayselectedDomainDiv").html(values);
						
						document.getElementById("domainPatternId").value = domainDirectory;
							
						var obj = document.getElementById('abc'); 	
						obj.style.display='none';
					},
					
				});	
			  
			}
		
	}

	function getPlanNamesAsPerDomain(planDomain)
	{
		 if(planDomain!="Select a Plan Domain")
			 {
			 $("#waitingdivId").show();
				 $.ajax
				    ({
						type :"post",				
						url : "get/planname/as/per/plandomain.html",			
						data :"plandomain="+planDomain,	  
						success : function(response) {
							
							if(response!="")
								{
								
								$("#waitingdivId").hide();
								
									var values = ""; 
									var planName = response.split(",");
									values += "<option value=\"Select Plan\">Select Plan</option>";
									for(var i=0;i<planName.length;i++)
										{
										    values += "<option value=\""+planName[i]+"\">"+planName[i]+"</option>";
										}
									
									$("#plan_nameId").html(values);
									$("#plan_nameId").show();
								}
							else
								{
								
								    $("#waitingdivId").hide();
								
									$("#plan_nameId").html("");
									$("#plan_nameId").hide();
								}
							
						},
				    });
			 }
		 else
			 {
				  $("#plan_name").hide();
			 }
	}


	function getKycUserDetails()
	{
		var kycId = document.getElementById("regkycId").value;
		$("#waitingdivId").show();
		
		$.ajax
	    ({
			type :"post",				
			url : "getdtailsviakycId.html",			
			data :"kycId="+kycId, 
			success : function(response) {
				
				
				if(response!="")
					{
					    $("#waitingdivId").hide();
					
						var regdata = response.split(",");
						
						document.getElementById("firstNameId").value = regdata[0];
						document.getElementById("middleNameId").value = regdata[1];
						document.getElementById("lastNameId").value = regdata[2];
						document.getElementById("emailidId").value = regdata[3];
						
						//gender
					}
				
				else
					{
					     $("#waitingdivId").hide();
					     document.getElementById("regkycId").value = "";
					     alert("Enter a valid Registered Kyc Id !");
					}
				
			},
	    });
		
	}
	
	function gotoMainPage()
	{
		window.location = "authenticateAdminLogin.html"
	}

</script>

<!-- Scripts for admin Pattern -->

</head>

<body>

	<div class="container">

		<!--header block should go here-->

		<div class="top_line"></div>
		
		<header class="header">
		
		<div class="header_inner" style="width:1120px !important">
			
			<div class="logo" onclick="gotoMainPage()">MAIN PAGE</div>


			<div class="tfclear"></div>
			<div  class=" mainmenu" >
			<div id="menu_wrapper">
				
				<ul class="main_menu" >
					
					<li></li>
										
					<li id="myMenus1">				
					<a class="myprofile" href="#" onclick="CheckSessionOfAdmin('viewindividual.html')"><span></span> My Individual
					</a>
					</li>
					
					<li id="myMenus2">				
					<a class="mydocuments" style="color:#03b2ee" href="#" onclick="CheckSessionOfAdmin('createUtiliser.html')">	<span></span>  My Utilizer
					</a>
					</li>
					
					<li id="myMenus5"><a class="mystore" href="#" onclick="CheckSessionOfAdmin('createDomain.html')">
							Domain Hierarchy
					</a></li>
					
					<li id="myMenus6" style="width:140px;"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createKycHierarchy.html')">
							Document Hierarchy
					</a>
					</li>
					
					<li id="myMenus3"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createAdminPattern.html')"> 
							My Patterns
					</a></li>
					
					<li id="myMenus41"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createfeature.html')">  My Features
					</a></li>
					
					<li id="myMenus42"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createplan.html')">  My Plans
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('adminNotification.html')">  NOTIFICATION
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('admin_settings.html')">  SETTINGS
					</a></li> 
										
				</ul>
				
             </div>
                                   
			</div>

          
		</div>
   
	</header>


				 <div style="text-align: right; float: right; margin-top: -50px; width: 4%; margin-left: 10px;"> 
	               <a href="logout_admin.html" ><h4 style="color:#05b7f5;">Logout</h4></a>
	             </div>

		<div class="main" ><!-- background-color: #585759; -->
			<div class="main_center">
				<div class="leftside">

					<!--left menus should go here-->
					<div class="user_display">


					 <form>
							<div id="textBox">
							
							        <div id="button">
							        
							            <ul>
							                <li id="Hi-B-1"><a href="#" id="1stId" class="adminleftMenuActive" onClick="CheckSessionOfAdmin('createUtiliser.html')"><span>CREATE UTILIZER</span></a></li>
							               	<li id="Hi-B-1"><a href="#" id="2stId" onClick="viewUti('viewUtiliser.html')"><span>VIEW UTILIZER</span></a></li>										   	
										   	<li id="Hi-B-1"><a href="#" id="3stId" onClick="organiseUti('organiseUtiliser.html')"><span>ORGANIZE UTILIZER</span></a></li>
									    </ul>
									    
									</div>
								
							</div>
						</form>

					</div>




				</div>


				<div class="rightside" >

					<!--center body should go here-->
					<div id="wishAdmin">
						<h1>Welcome To KYC Admin</h1>
					</div>
					
					<div class="border_line"></div>
					<div class=" username_bar">
						 <div id="titleBar">
							<h2>
								<span></span>
								 Create Utilizer
							</h2>
						</div>
					</div>
				
					<div class="module-body" >
						<div id="viewUsers" style="height: auto !important;margin-bottom: 15px"> 
    
						 
						 
						 <!-- AdminBody startts Here -->
						 
						 				
														<%String patt_name = (String)request.getAttribute("patt_name"); 
														  String Pattern_arr[] = patt_name.split(",");
														 %>
														
														<%String domain = (String)request.getAttribute("domainType").toString();
														String domainType[] = domain.split(",");
														%>
														
														<%String plan = (String)request.getAttribute("plan_name").toString();
														String planType[] = plan.split(",");
														%>
														<div>
														
														<span id="erroeMessageId" style="color:red;">All fields are Mandatory *</span>
														
															 <form name="createUtiForm" id="createUtiForm" enctype="multipart/form-data"> 
														           
																<table border="0" align="center">
																	<tr>
																		<td>
																			<div class="left" align="left">
																			
																			 <h1><span>Basic Information</span></h1><br><br>
														<table align="left" class="createutiform">
														
														<tr>
														<td>KYC Id<input type="text" id="regkycId" name="utiregkycId" onchange="getKycUserDetails()"/></td>
														</tr>
														<tr>
														<td>First Name<input type="text" id ="firstNameId" name="firstName"></input></td>
														</tr>
														<tr>
														<td>Middle Name<input type="text" id="middleNameId" name="middleName"/></td>
														</tr>
														<tr>
														<td>Last Name<input type="text" id="lastNameId" name="lastName"/></td>
														</tr>
														
														<tr>
														<td>Gender<select  id="genderId" name="gender" style="">
														          <option value="Select Gender">Select Gender</option>          
														          <option value="Female">Female</option>
														          <option value="Male">Male</option>
														          </select>
														          </td>
														</tr>
														<tr>
														<td>Personal Email ID<input type="text" id="emailidId" name="emailid"/></td>
														</tr>
														<tr>
														<td>Office Email ID<input type="text" id="ofcemailIdId" name="officeEmailId"/></td>
														</tr>
														<tr>
														<td>Office 1st Ph.No<input type="text" id="ofcphno1Id" name="ofcphno1"/></td>
														</tr>
														<tr>
														<td>Office 2nd Ph.No<input type="text" id="ofcphno2Id" name="ofcphno2"/></td>
														</tr>
														<tr>
														<td>Branch Office Address<textarea id="ofcaddressId" name="ofcaddress" style="width: ; height: 46px;"/></textarea>
														</tr>
														<tr>
														<td>Head Office Address<textarea id="headofcaddressId" name="headofcaddress" style="width: ; height: 46px;"/></textarea>
														</tr>
														<tr>
														<td>User Photo<input type="file" id="userPhotoId" name="userPhoto"/></td>
														</tr>
														
														</table>
																			</div>
																		</td>
																		
																		<td>
																			<div class="right" >
																			<h1><span></span></h1><br><br>
																				
																				<table align="right">
																				<div id="pattern_name_td">
																							
																					<select name="pattern_name" id="pattern_nameId">
																											<option value="Select Pattern Name">Select Pattern Name</option>
																											<%for(int i=0;i<Pattern_arr.length;i++)
																												{%>
																												<option value="<%=Pattern_arr[i]%>"><%=Pattern_arr[i]%></option>
																											<%}%>
																					</select>
																				</div>
																				
																				<div id="pattern_name_td">	
																					
																					<select name="plan_domain" id="plan_domainId" onchange="getPlanNamesAsPerDomain(this.value)">
																											<option value="Select a Plan Domain">Select a Plan Domain</option>
																									         <option value="Recruiter">Recruiter</option>
																									         <option value="Banks">Banks</option>
																									         <option value="Insurance">Insurance</option>
																									         <option value="Travel Agent">Travel Agent</option>
																									         <option value="Govt-Department">Govt-Department</option>
																									         <option value="Telecommunication">Telecommunication</option>
																									         <option value="Money Transfer Agents">Money Transfer Agents</option>
																									         <option value="Hotel & Lodging">Hotel & Lodging</option>
																									         <option value="Corporate">Corporate</option>
																									          <option value="Other">Other</option>
																					</select>
																																				
																				<select name="plan_name" id="plan_nameId" style="display:none;">
																										
																				</select>
																				</div>
														
																				<tr>
																				<td>
																				
																				<div id="ChooseShowDomain" align="left" style=" margin-right:-99px;margin-top:10px">						
																				
																				<a href="#" id="popup" onclick ="div_show()"><b>SELECT DOMAIN</b></a>
																				
																				</div>
																				
																				<div id="displayselectedDomainDiv" style=" margin-right: -99px;margin-top: 41px;width: 420px;">						
																				
																						
																				</div>
																				
																				</td>
																				</tr>
														
														
														</table>
																			</div>
																		</td>
																	</tr>
																</table>
														          
														          
														          <input type="hidden"  id="domainPatternId" name="domainPattern" value=""/>
														                
														</form> 
														</div>
														       <div id="Utiliser">
																<input align="right" type="button" name="Next" value="Create Utilizer" onClick="save_Utiliser()"/>
																</div>
														
														<div id="abc" style="display:none;">
														 
															 <!-- Popup div starts here -->
														 <div id="popupContact"> 
														
															<!-- contact us form -->
																<form id="domainForm" name="domainSelectionForm" id="form" >
																	<img src="admin_resources/domainTree/images/3.png" id="close" onclick="domainSlectionOpeation(this.id)"/>	
																	<h3>Select Domain</h3><hr/>
																	
																	<form id="form1"  >
														
														    <div style="border: thin solid #c8c8c8;width:500px;">
														
														        Select Domain <select id="cb" onchange="getSubDomainValue(this.id)">
														            <option value="Select Your Domain">Select Your Domain</option>
																				<%for(int i=0;i<domainType.length;i++){ %>
																				<option value="<%=domainType[i]%>"><%=domainType[i]%></option>
																				<%}%>
														             </select>
														
														        <table id="table1">
														
														            <tbody>
														
														                <tr><th>Sub Domain Level</th>
														                    <th>Parent Name</th>
														                    <th>Sub Domain Value</th>
														                </tr>
														              
																	<tr id="childRow1" style="display:none;"><td>1</td><td id="parentId1"></td>			 
																	 <td>
																		<select id="cb1" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>
														           
														           <tr id="childRow2" style="display:none;"><td>2</td><td id="parentId2"></td>			 
																	 <td>
																		<select id="cb2" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>
														            
														            <tr id="childRow3" style="display:none;"><td>3</td><td id="parentId3"></td>			 
																	 <td>
																		<select id="cb3" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>
														            
														            <tr id="childRow4" style="display:none;"><td>4</td><td id="parentId4"></td>			 
																	 <td>
																		<select id="cb4" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>
														            
														            <tr id="childRow5" style="display:none;"><td>5</td><td id="parentId5"></td>			 
																	 <td>
																		<select id="cb5" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>
														              
														            <tr id="childRow6" style="display:none;"><td>6</td><td id="parentId6"></td>			 
																	 <td>
																		<select id="cb6" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>   
														             
														            <tr id="childRow7" style="display:none;"><td>7</td><td id="parentId7"></td>			 
																	 <td>
																		<select id="cb7" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>   
														             
														            <tr id="childRow8" style="display:none;"><td>8</td><td id="parentId8"></td>			 
																	 <td>
																		<select id="cb8" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>  
														            
														            <tr id="childRow9" style="display:none;"><td>9</td><td id="parentId9"></td>			 
																	 <td>
																		  <select id="cb9" onchange="getHeirarchyFromDomain(this.id)"></select>
														             </td>
														            <tr>  
														            
														            <tr id="childRow10" style="display:none;"><td>10</td><td id="parentId10"></td>			 
																	 <td>
																		  <select id="cb10"></select>
														             </td>
														             
														            <tr>  
														                 		              
														           </tbody>
														
														        </table>
														           
														          <span >
														          <input type="button"  id="submit"  value="Done"  onclick="domainSlectionOpeation(this.id)"/>	          
														          </span>
														    </div>
														         
														    </form>										
																</form>
																
														 </div> 
														 <!-- Popup div ends here -->
														 </div>
														 <!-- display popup button -->
														
														
														
						 				
						 
						 <!-- Admin Body Ends Here -->
						 
						</div>

					</div>
					

				</div>
				
			</div>
		</div>
</div>
		
		<!--footer block should go here-->
		
		   <%
			  ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			  String version=resource.getString("kycversion");
			  String versionDate=resource.getString("kyclatestDate");
		    %>
		
		
          <footer class="footer">
		   
		  
				<div class="footer_copyrights">
                <div class="copyrights">    
					 <ul >
                     		
							<li class="footer_space01"><p>Design by:<span> <a href="http://jirehsol.com/">Jireh </a></span></p>
                            <p> � All rights Reserved CLENSEE 2014-15</p>
                             <span>
									<a href="#">Version:<%=version%></a>
									</span>
                            </li>
							<li class="footer_space02"> <p><a href="#">About Us</a> |<a href="#"> Terms and Conditions</a> |<a href="#"> Privacy Policy</a> | <a href="#">Contact Us</a></p></li>
							<li class="footer_space03"> 
                            
                            <div class="four columns">
                                <ul class="social_media">
                                    <li>
                                     <a class="fb" href="https://www.facebook.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="google_plus" href="https://plus.google.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="http://www.linkedin.com/company/1872121?trk=tyah" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li>
                                   
                                    
                                </ul>
                              </div>
                            
                            </li>
						   
						</ul>     
					</div>
                
				</div>
		   
			<div class="footer_line">
			</div>

		 </footer>



<!-- Waiting Div -->
	
	<div id="waitingdivId" >
									    
		    <div id="waitingdivContentId">
				    
				     <div class="kycloader">
		
		             </div>
		             <h4>Please wait For the Portal response...   </h4>
		              				    
		    </div>
	     
     </div>
	
<!-- EOF Waiting Div -->




<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#05b7f5;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		        <h3>Your session has expired,Please click on the button to relogin !</h3>      
		         
		         <input type="button" value="Login" onclick="redirectToLogin()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	
	
	<!------container End-------->
</body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              