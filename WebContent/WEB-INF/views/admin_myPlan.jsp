<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<%@ page language="java" import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Plan</title>

<link href="admin_resources/kyc_css/kyc_admin_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="admin_resources/kyc_css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">

<link rel="stylesheet" href="admin_resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script type="text/javascript"src="admin_resources/kyc_scripts/demo.js"></script>

<link rel="stylesheet" href="admin_resources/css/logout.css">	
	
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />
	
	
<style>

#sessionfade{

    background-color: #000;
    height: 100%;
    left: 0;
    opacity: 0.7;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1001;
}

#sessionlight{

    background: #fff none repeat scroll 0 0;
    border-radius: 24px;
    height: auto;
    left: 43%;
    margin-left: -211px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: auto;
    z-index: 1002;
    right: 400px !important;

}

.adminleftMenuActive
{
	color:#00b6f5 ! important;
}

</style>	


<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}
</style>
	
	
	
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
$(document).ready(function () {
    $("#menu_wrapper").on("click", "ul.main_menu>li", function (event) {
        debugger;
        event.preventDefault();
        $("#menu_wrapper .active").removeClass("active");
        $(this).find('a:first').addClass("active");
    });
});
});//]]>  



function redirectToLogin()
{
	window.location = "logout_admin.html";	
}


</script>

<script type="text/javascript">
	
	

function Selectedfeature()
{
	$("#waitingdivId").show();

	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var click = [];
						$("#selectedfeature:checked").each(function() {
					        click.push($(this).val());
					    });
						 	
						document.getElementById('feature_id').value=click;
						
						 $.ajax({
								type : "post",				
								url : "checkPlanExistence.html",				
								data :$('#createplanform').serialize(),	  
								success : function(response) {
									
									if(response!="Yes")
										{
												$.ajax({
													type : "post",				
													url : "savecreateplan.html",				
													data :$('#createplanform').serialize(),	  
													success : function(response) {
														
														$("#waitingdivId").hide();

														if(response!="Problem occured due to some reason,Try Again !!")
															{
															    alert(response);
															    CheckSessionOfAdmin('createplan.html')
															}
														else
															{
																alert("Problem occured due to some reason,Try Again !!");
															}												
													},
													
												});		
										}
									else
										{
										       $("#waitingdivId").hide();

										       alert("This Plan Name Already exist in the givn Plan Type,you may Go for Organize Plan !")
										}																
								},
								
							});	
				}
			else
				{
				     $("#waitingdivId").hide();

				     $("#sessionlight").show();
			         $("#sessionfade").show();
				}
		}
	});
	
	
	
	  	 	
}


function viewFeatureAsPerCategory(featureCategory)
{
	
  if(featureCategory=="Select Category")
	  {
	      $("#individualFeatureDivId").hide();
	      $("#utilizerFeatureDivId").hide();
	      $("#corporateFeatureDivId").hide();
	      $("#plan_domainId").hide();
	  }
  if(featureCategory=="Individual")
	  {
		  $("#individualFeatureDivId").show();
	      $("#utilizerFeatureDivId").hide();
	      $("#corporateFeatureDivId").hide();
	      $("#plan_domainId").hide();
	  }
  if(featureCategory=="Utilizer")
	  {
		  $("#individualFeatureDivId").hide();
	      $("#utilizerFeatureDivId").show();
	      $("#corporateFeatureDivId").hide();
	      $("#plan_domainId").show();
	  }
  if(featureCategory=="Corporate")
	  {
		  $("#individualFeatureDivId").hide();
	      $("#utilizerFeatureDivId").hide();
	      $("#corporateFeatureDivId").show();
	      $("#plan_domainId").hide();
	  }
  
}
	
	

function viewPlan(handlerToHit) {
	
	document.getElementById("1stId").className = "";
	document.getElementById("2stId").className = "adminleftMenuActive";
	document.getElementById("3stId").className = "";
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				    $("#waitingdivId").show();

					 $.ajax({
							type : "post",
							url : handlerToHit,
							cache : false,
							success : function(response) {
								
								 $("#waitingdivId").hide();

								 $("#viewUsers").html(response);
								
							},
							
						});
					 		 		
					$("#titleBar").html("<h2><span>VIEW PLAN</span></h2>");
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
	
}

function organizePlan(handlerToHit) {
	
	document.getElementById("1stId").className = "";
	document.getElementById("2stId").className = "";
	document.getElementById("3stId").className = "adminleftMenuActive";
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				    $("#waitingdivId").show();

						$.ajax({
							type : "post",
							url : handlerToHit,
							cache : false,
							success : function(response) {
								
								$("#waitingdivId").hide();

								 $("#viewUsers").html(response);
								},				
						});
					 		 		
					$("#titleBar").html("<h2><span>ORGANIZE PLAN</span></h2>");
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});

}



	function CheckSessionOfAdmin(hitTheUrl)
	{
		$("#waitingdivId").show();

		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				$("#waitingdivId").hide();

				if(response=="Exist")
					{
						window.location = hitTheUrl;
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
					
	}

	function gotoMainPage()
	{
		window.location = "authenticateAdminLogin.html"
	}

	
</script>

<!-- Scripts for admin Pattern -->

</head>
<body>

	<div class="container">

		<!--header block should go here-->

		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner" style="width:1120px !important">
			
			<div class="logo" onclick="gotoMainPage()">MAIN PAGE</div>


			<div class="tfclear"></div>
			<div  class=" mainmenu" >
			<div id="menu_wrapper">
				
				<ul class="main_menu" >
					
					<li></li>
										
					<li id="myMenus1">				
					<a class="myprofile" href="#" onclick="CheckSessionOfAdmin('viewindividual.html')"><span></span> My Individual
					</a>
					</li>
					
					<li id="myMenus2">				
					<a class="mydocuments" href="#" onclick="CheckSessionOfAdmin('createUtiliser.html')">	<span></span>  My Utilizer
					</a>
					</li>
					
					<li id="myMenus5"><a class="mystore" href="#" onclick="CheckSessionOfAdmin('createDomain.html')">
							Domain Hierarchy
					</a></li>
					
					<li id="myMenus6" style="width:140px;"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createKycHierarchy.html')">
							Document Hierarchy
					</a>
					</li>
					
					<li id="myMenus3"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createAdminPattern.html')"> 
							My Patterns
					</a></li>
					
					<li id="myMenus41"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createfeature.html')">  My Features
					</a></li>
					
					<li id="myMenus42"><a class="accessmanager" style="color:#03b2ee" href="#" onclick="CheckSessionOfAdmin('createplan.html')">  My Plans
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('adminNotification.html')">  NOTIFICATION
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('admin_settings.html')">  SETTINGS
					</a></li>					
				</ul>
				
             </div>
                                   
			</div>

          
		</div>
   
	</header>


				 <div style="text-align: right; float: right; margin-top: -50px; width: 4%; margin-left: 10px;"> 
	               <a href="logout_admin.html" ><h4 style="color:#05b7f5;">Logout</h4></a>
	             </div>

		<div class="main" ><!-- background-color: #585759; -->
			<div class="main_center">
				<div class="leftside">

					<!--left menus should go here-->
					<div class="user_display">


					 <form>
							<div id="textBox">
							
							        <div id="button">
							        
							            <ul>
							                <li id="Hi-B-1"><a href="#" id="1stId" class="adminleftMenuActive" onClick="CheckSessionOfAdmin('createplan.html');"><span>CREATE PLAN</span></a></li>
											<li id="Hi-B-1"><a href="#" id="2stId" onClick="viewPlan('viewplan.html');"><span>VIEW PLAN</span></a></li>
											<li id="Hi-B-1"><a href="#" id="3stId" onClick="organizePlan('organiseplan.html');"><span>ORGANIZE PLAN</span></a></li> 
									    </ul>
									    
									</div>
								
							</div>
						</form>

					</div>




				</div>


				<div class="rightside" >

					<!--center body should go here-->
					<div id="wishAdmin">
						<h1>Welcome To KYC Admin</h1>
					</div>
					
					<div class="border_line"></div>
					<div class=" username_bar">
						 <div id="titleBar">
							<h2>
								<span></span>
								 Create Plan
							</h2>
						</div>
					</div>
				
					<div class="module-body" >
						<div id="viewUsers" style="height: auto !important;margin-bottom: 15px"> 
    
						 
						 
						 <!-- AdminBody startts Here -->
						 
																 
										        <form id="createplanform"  name="createplanform"  method="post">         
										           
										        <table align="center" border="0" class="tabledesign">
												
												<tr>    <th>Name Of Plan </th><td>:</td><td>
												                                            <select class="plan_view_search" name="plan_name">
												                                            
												                                                  <option value="Select Your Plan Name">Select Your Plan Name</option>
												                                                  <option value="Basic">Basic</option>
												                                                  <option value="Silver">Silver</option>
												                                                  <option value="Gold">Gold</option>
												                                                  <option value="Platinum">Platinum</option>
												                                                  
												                                            </select>
												                                    
												                                    </td></tr>
												<tr>	<th>Plan Type</th><td>:</td><td>
												
												<select id="plan_view" name="plan_type" id="planame" onchange="viewFeatureAsPerCategory(this.value)">
																<option value="Select Category">Select Category</option>
																<option value="Individual">Individual</option>
																<option value="Utilizer">Utilizer</option>
																<option value="Corporate">Corporate</option>
												</select>
												
												</td>
												
												<tr id="plan_domainId" style="display:none;">	
												    <th>Plan Domain </th><td>:</td>				
													<td>		
														<select class="plan_view_search"  name="plan_domain" >
														         <option value="Select Domain">Select Domain</option>
														         <option value="Recruiter">Recruiter</option>
														         <option value="Banks">Banks</option>
														         <option value="Insurance">Insurance</option>
														         <option value="Travel Agent">Travel Agent</option>
														         <option value="Govt-Department">Govt-Department</option>
														         <option value="Telecommunication">Telecommunication</option>
														         <option value="Money Transfer Agents">Money Transfer Agents</option>
														         <option value="Hotel and Lodging">Hotel & Lodging</option>
														         <option value="Corporate">Corporate</option>
														         <option value="Other">Other</option>
														</select>		
													</td>		
												</tr>
												
												<tr><th>Price of Plan:</th><td>:</td><td><input class="plan_view_search" type="text" name="plan_price"/></td></tr>
												
												<tr>	<td><input type="hidden" value="${regname.lastname}" name="feature_ids" id="feature_id" /></td></tr>
													 	
										       	<tr><td><div style="display:none;visibility:hidden;" id="profileupdatereg1"></div><input type="hidden" id="profileupdatereg" name="profileupdatereg"/></td></tr>	 
										       		
										       
											    </table>   
											   
											   
											   <div id="individualFeatureDivId" class="datagrid" style="display:none;">   
											   
											    <table style='width: 800px' border="1" >
													   <tr>
													    <th style='width: 80px'  bgcolor="#999">Sl.No </th>				
														<th style='width: 200px'  bgcolor="#999">Name</th>
														<th style='width: 180px'  bgcolor="#999">Value</th>			
														<th style='width: 180px'  bgcolor="#999">Price</th>								 
													    <th style='width: 160px'  bgcolor="#999">Select</th>	
													  </tr>	
													<%int i=0;%>	
													<c:forEach items="${Individualfeature}" var="det">				
													   <tr>
													    <td style='width: 80px'><c:out value="<%=++i%>"/></td>				
														<td style='width: 200px'><c:out value="${det.feature_name}"/></td>
														<td style='width: 180px'><c:out value="${det.feature_value}"/></td>			
														<td style='width: 180px'><c:out value="${det.feature_price}"/></td>							
													    <td style='width: 180px'>			    
													    <input type="checkbox" id="selectedfeature"  name="selectedfeature" value="${det.feature_id}">    
													    </td>				 
													  </tr>							
										            </c:forEach> 
												</table>
													
													
											   </div>
												 
												 <div id="utilizerFeatureDivId" class="datagrid" style="display:none;">   
											   
											    <table style='width: 800px' border="1" >
													   <tr>
													    <th style='width: 80px'  bgcolor="#999">Sl.No </th>				
														<th style='width: 200px'  bgcolor="#999">Name</th>
														<th style='width: 180px'  bgcolor="#999">Value</th>			
														<th style='width: 180px'  bgcolor="#999">Price</th>								 
													    <th style='width: 160px'  bgcolor="#999">Select</th>	
													  </tr>	
													<%int j=0;%>	
													<c:forEach items="${Utilizerfeature}" var="det">				
													   <tr>
													    <td style='width: 80px'><c:out value="<%=++j%>"/></td>				
														<td style='width: 200px'><c:out value="${det.feature_name}"/></td>
														<td style='width: 180px'><c:out value="${det.feature_value}"/></td>			
														<td style='width: 180px'><c:out value="${det.feature_price}"/></td>							
													    <td style='width: 180px'>			    
													    <input type="checkbox" id="selectedfeature"  name="selectedfeature" value="${det.feature_id}">    
													    </td>				 
													  </tr>							
										            </c:forEach> 
												</table>
													
													
											   </div>
											   
											   
											    <div id="corporateFeatureDivId" class="datagrid" style="display:none;">   
											   
											    <table style='width: 800px' border="1" >
													   <tr>
													    <th style='width: 80px'  bgcolor="#999">Sl.No </th>				
														<th style='width: 200px'  bgcolor="#999">Name</th>
														<th style='width: 180px'  bgcolor="#999">Value</th>			
														<th style='width: 180px'  bgcolor="#999">Price</th>								 
													    <th style='width: 160px'  bgcolor="#999">Select</th>	
													  </tr>	
													<%int k=0;%>	
													<c:forEach items="${Corporatefeature}" var="det">				
													   <tr>
													    <td style='width: 80px'><c:out value="<%=++k%>"/></td>				
														<td style='width: 200px'><c:out value="${det.feature_name}"/></td>
														<td style='width: 180px'><c:out value="${det.feature_value}"/></td>			
														<td style='width: 180px'><c:out value="${det.feature_price}"/></td>							
													    <td style='width: 180px'>			    
													    <input type="checkbox" id="selectedfeature"  name="selectedfeature" value="${det.feature_id}">    
													    </td>				 
													  </tr>							
										            </c:forEach> 
												</table>
													
													
											   </div>    
											
											     <div id="Utiliser" align="center">
											     <input type="button" id="edit" onclick="Selectedfeature()" value="Create"> 
											     </div>
											  
											    	   
										        </form> 
																 
																 
						 
						 <!-- Admin Body Ends Here -->
						 
						</div>

					</div>
					

				</div>
				
			</div>
		</div>
</div>
		
		<!--footer block should go here-->
		
		   <%
			  ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			  String version=resource.getString("kycversion");
			  String versionDate=resource.getString("kyclatestDate");
		    %>
		
		
          <footer class="footer">
		   
		  
				<div class="footer_copyrights">
                <div class="copyrights">    
					 <ul >
                     		
							<li class="footer_space01"><p>Design by:<span> <a href="http://jirehsol.com/">Jireh </a></span></p>
                            <p> � All rights Reserved CLENSEE 2014-15</p>
                             <span>
									<a href="#">Version:<%=version%></a>
									</span>
                            </li>
							<li class="footer_space02"> <p><a href="#">About Us</a> |<a href="#"> Terms and Conditions</a> |<a href="#"> Privacy Policy</a> | <a href="#">Contact Us</a></p></li>
							<li class="footer_space03"> 
                            
                           <div class="four columns">
                                <ul class="social_media">
                                    <li>
                                     <a class="fb" href="https://www.facebook.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="google_plus" href="https://plus.google.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="http://www.linkedin.com/company/1872121?trk=tyah" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li>
                                   
                                    
                                </ul>
                              </div>
                            
                            </li>
						   
						</ul>     
					</div>
                
				</div>
		   
			<div class="footer_line">
			</div>

		 </footer>



<!-- Waiting Div -->
	
	<div id="waitingdivId" >
									    
		    <div id="waitingdivContentId">
				    
				     <div class="kycloader">
		
		             </div>
		             <h4>Please wait For the Portal response...   </h4>
		              				    
		    </div>
	     
    </div>
	
<!-- EOF Waiting Div -->




<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#05b7f5;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		        <h3>Your session has expired,Please click on the button to relogin !</h3>      
		         
		         <input type="button" value="Login" onclick="redirectToLogin()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	
	
	<!------container End-------->
</body>
</html>
