 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Documents</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/onlineresources/code.jquery.com.jquery-1.10.2.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page language="java" import="java.util.*" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
    
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
    
}
</style>




<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
    
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
    
}


</style>

<script>


function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		   window.location = "search_financialDoc.html";
		}
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>




<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{
  border: 2px solid #03b2ee;
    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}

</style>
 
<script>

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewDocumentInPopUp(fileName)
{

	 $.ajax({
				type : "post",
				url : "checksessionforIndividual.html",
				success : function(response) {
					
					if(response=="Exist")
						{
							document.getElementById('waitlight').style.display='block';
							document.getElementById('waitfade').style.display='block';
							
							$.ajax({
								type : "get",				
								url : "viewfinancial.html",
								data: "fileName="+fileName,
								success : function(response) {
								
									document.getElementById('waitlight').style.display='none';
									document.getElementById('waitfade').style.display='none';
									
									document.getElementById('kycDocPopUplight').style.display='block';
							        document.getElementById('kycDocPopUpfade').style.display='block';
							        $("#kycDocPopUpDisplay").html(response);
							        			
								},		
							});		
						}
					else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	 
	 		 		 		
	
}

</script> 
 



<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
		
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 
		    			$('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
		 	
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

</script>





<script>
/*home page*/

function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/

/*profile page*/


/*end profile*/



/* All Scripts for MyDocument pannel  */

	
	function myDocuments1(handlerToHit) {
		 
		var v= '1stUser';
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				data : "status=" +v,
				success : function(response) {
					$("#center-body-div").hide();					
					$("#searchicon").hide();          					
					 $("#viewUsers1")
						.html(response);
					 					   
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>KYC DOCUMENTS</span></h2>");
	}
	
	function myDocuments2(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 window.location = "search_financialDoc.html";
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		
	}
	
	function myDocuments3(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>EMPLOYMENT DOCUMENTS</span></h2>");
	}
	
	function myDocuments4(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Employment Details</span></h2>");
	}
	

	/* End of MyDocument Scripts  */
 
	 	 	 	 	 	 		 
</script>



<script src="resources/js/kyc_side_login.js"></script>



<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 

<!-- Script for Reselect Financial Document Type -->

 
<script>
$(function() {
  $( "#datepicker" ).datepicker();
});
</script>

<!-- Script for Validation -->
<script type="text/javascript">

function ValidateFinancial()
{	
	
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	
	var flag = 'true';
	
	var docType = document.getElementById("docTypeId").value;
	
	
	if(document.getElementById("fromYearId").value=="Select Year")
	{
		document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
		
	   flag = 'false';
	   $("#yearDiv").show();
	   $("#yearDiv").html("Select financial year .");
	   return false;
	}
	if(document.getElementById("fromYearId").value!="Select Year")
	{
		$("#yearDiv").hide();
	}
	 	
	if(docType=="Select Type")
	{
		document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
		flag = 'false';
		$("#fileTypeDiv").show();
		$("#fileTypeDiv").html("Select document type .");
		return false;
	}
	
    var docMode = document.getElementById("docModeId").value
	
	if(docMode=="Select Mode")
	{
		document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
		
		flag = 'false';
		$("#fileModeDiv").show();
		$("#fileModeDiv").html("Select document mode.");
		return false;
	}
	
	if(document.getElementById("financialfileId").value=="")
	  {
		document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
		
	     flag = 'false';
	     $("#fileDiv").show();
	     $("#fileDiv").html("Please a select a file to upload.");
	     return false;
	  }
	
	
	
	if(document.getElementById("financialfileId").value!="")
		{		
		   var fileDetails = document.getElementById("financialfileId").value;
		   $("#fileDiv").hide();
		    var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
		    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf","xlsx","xls");						    
		    var condition = "NotGranted";
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}
			if(condition=="NotGranted")
				{
				   document.getElementById('waitlight').style.display='none';
				   document.getElementById('waitfade').style.display='none';
				
				   flag = 'false';
				   
  			     document.getElementById('sociallight').style.display='block';
  			     document.getElementById('socialfade').style.display='block';
  			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
  				 $("#socialdisplaySharedDocDiv").html("pdf/office/image files are only allowed. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

  				 return false;
				  
				}
		}
			 var fileDetails1 = document.getElementById("financialfileId");
			 var fileSize = fileDetails1.files[0];
			 var fileSizeinBytes = fileSize.size;
			 var sizeinKB = +fileSizeinBytes / 1024;
	  		 var sizeinMB = +sizeinKB / 1024;
	  		 
	  		 if(sizeinMB>2)
	  			 {
	  				document.getElementById('waitlight').style.display='none';
	  				document.getElementById('waitfade').style.display='none';
	  			
	  				 flag = 'false';
	  				 
    			     document.getElementById('sociallight').style.display='block';
    			     document.getElementById('socialfade').style.display='block';
    			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
    				 $("#socialdisplaySharedDocDiv").html("File size should not more then 2 MB. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

    				 return false;	 		   		 
	  			 }
	
			if (flag == 'true') 
			{
											
				 $.ajax({
						type : "post",
						url : "checksessionforIndividual.html",
						success : function(response) {
							
							if(response=="Exist")
								{								       						 								
											$.ajax
										    ({
												type : "post",				
												url : "validateFileStorage.html",	
												data: "givenfileSize="+sizeinKB,
												success : function(response) {
													
													if(response=="withinStorage")
														{
															 var formData = new FormData($("#FinancialForm")[0]);
													            
															 formData.append("toYear",document.getElementById("toYearId").value);
															 formData.append("docMode",document.getElementById("docModeId").value);
															 
																$.ajax({
																	type : "POST",
																	url : "financialDocument.html",
																	data : formData,
																	//async: false,
																	success : function(response) {
													                    
																		document.getElementById('waitlight').style.display='none';
																		document.getElementById('waitfade').style.display='none';
																												
																	     document.getElementById('sociallight').style.display='block';
																	     document.getElementById('socialfade').style.display='block';
																	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
																	     $("#socialdisplaySharedDocDiv").html("Financial details has been added successfully . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");   
																		 																														
																	},
																	cache: false,
														 	        contentType: false,
														 	        processData: false,
																	error : function() {
																		alert('Error while fetching response');
																	}
																});
																$("#titleBar").html("<h2><span>Financial Documents</span></h2>");
														}
													else
														{
															document.getElementById('waitlight').style.display='none';
															document.getElementById('waitfade').style.display='none';
															
														      alert("Failed due to storage limitation,to upload more document migrate your Plan!");
														}
												},
												
											});		 		 		 													
								}
							else
								{
								   $("#sessionlight").show();
							       $("#sessionfade").show();
								}
						}
					}); 			
			}
	}
	

function getNextYear()
{
	$("#finalyearDiv").html("");
	
	if(document.getElementById("fromYearId").value!="Select Year")
		{
	       document.getElementById("toYearId").value = +document.getElementById("fromYearId").value + 1;
		}
	if(document.getElementById("fromYearId").value=="Select Year")
		{
		   document.getElementById("toYearId").value = "";
		}
	
	var fyear = document.getElementById("fromYearId").value;
	var tyear = document.getElementById("toYearId").value;
	
	var inrfyear1 = +fyear + 1;
	var inrfyear2 = +fyear + 2;
		
	if(document.getElementById("fromYearId").value!="Select Year")
		{
			$("#finalyearDiv").show();
			$("#finalyearDiv").append("Financial Year "+fyear+"-"+tyear);
			$("#finalyearDiv").append("<br>Assesment Year "+inrfyear1+"-"+inrfyear2);
			$("#yearDiv").hide();
		}
	else
		{
		    $("#finalyearDiv").hide();
			$("#yearDiv").show();
			$("#yearDiv").html("Please a select year.");
		}
}

function getFinancialDetails(picname)
{	
	$.ajax
    ({
		type : "post",				
		url : "financialDetails.html",	
		data : "docName="+picname, 
		success : function(response) {
			
			 $("#viewUsers1")
				.html(response);
							
		},
		
	});		 		 		 		
     $("#titleBar").html("<h2><span>Financial Details</span></h2>");
}


/* function getFinancialDetails(picname)
{	
	window.location = "viewfinancialdetails.html?docName="+picname;
} */



function getHistoryDetails(picName,yeardiff)
{	
	window.location = "financialhistorydetails.html?docName="+picName+"&yeardiff="+yeardiff;
}
 

function generateFinancialMode(id)
{
	var docType = document.getElementById(id).value;
	var mode = docType.split(":");
	
	if(mode[0]=="Select Type")
		{			
		    $("#fileTypeDiv").show();
		    $("#fileTypeDiv").html("Select Document Type !");
		}
	else
		{
				$("#fileTypeDiv").hide();
								
				var values = "";
				if(mode[1]=="Both")
					{
					    values += "Document Mode:<select name=\"docMode\" id=\"docModeId\" onchange=\"showModeAlert(this.value);\">";
					    values +=  "<option value=\"Select Mode\">Select Mode</option>";
					    values +=  "<option value=\"Original\">Original</option>";
					    values +=  "<option value=\"Revised\">Revised</option>";
					    values += "</select>";
					    $("#docmodecompId").html(values);
					}
				if(mode[1]=="Original")
				{
				    values += "Document Mode:<input type=\"text\" name=\"docMode\" id=\"docModeId\" value=\"Original\" disabled=\"true\" />";
				    $("#docmodecompId").html(values);	    
				}
				if(mode[1]=="Revised")
				{
					values += "Document Mode:<input type=\"text\" name=\"docMode\" id=\"docModeId\" value=\"Revised\" disabled=\"true\" />";
					$("#docmodecompId").html(values);
				}
	    
		}
	
}

function showModeAlert(docMode)
{
	if(docMode=="Select Mode")
		{
			$("#fileModeDiv").show();
			$("#fileModeDiv").html("Select document mode !");
			return false;
		}
	else
		{		
			$("#fileModeDiv").hide();		
		}
}
</script>
<!-- End of Script for Validation -->

<script> 
$(document).ready(function(){
  $("#financialAddDocumentFlip").click(function(){
    $("#financialAddDocumentPannel").slideToggle("slow");
  });
});










/* Main Header Actions  */

function gotoProfile()
{
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoMyDocument()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoSharingManager()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoStore()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoAlerts()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
}

function gotoPlans()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
}

function gotoSetings()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}


function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 
	 document.getElementById("inputString").value = "";	 
}


function changeDesignStatus()
{
    var flipValue = doacument.getElementById("flipValue").value;
    if(flipValue=="0")
    	{
    		document.getElementById("finButId").style.background = "#00b6f5";
    		document.getElementById("finButId").style.color = "#fff";
    		doacument.getElementById("flipValue").value = "1";
    	}
    else
    	{
    		document.getElementById("finButId").style.background = "#ebebeb";
			document.getElementById("finButId").style.color = "#4a484b";
			doacument.getElementById("flipValue").value = "0";
    	}
}


function hideAddvertise()
{
   $("#ProfileHeadingDiv").fadeOut(90000);	
}

function visibleAccordian(accordianId)
{
   $("#"+accordianId).slideToggle("slow");
}

</script>
 
 <!-- End of Script and CSS for Financial Documents -->
 
 
 
</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocumentsfocus"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 
 
 					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
           
           
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
             
        
					
		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
							  <ul>
							     <li id="Hi-B-1"><a href="myDocumentsMain.html"  id="Hi-B-1" ><span>KYC Documents</span></a></li>
								 <li id="academic_document"><a href="getacademicdetails.html"  id="Hi-B-2" ><span>Academic Documents</span></a></li>															
								 <li id="employee_document"><a href="employeeDetails.html"  id="Hi-B-4"><span>Employment Documents</span></a></li>
								 <li id="financial_document"><a href="#"  id="financial_documentactive" onclick="myDocuments2('search_financialDoc.html')"><span>Financial Documents</span></a></li>
							 </ul>
						   </div>    		
					</div>
					
					 </div>
					
					<div class="rightside">
					
						
					    <!-- Advertisment Div for Entire Page  -->
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

									

					<div id="ProfileHeadingDiv" style="display: block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>

					
							        		
					   <!-- Advertisment Div for Entire Page  -->
					   
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span><%=name%>'s Financial Documents</span>								   
							</h2>
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
															
									
													                 
										         <c:set var="financialHierarchy" value="${financial}"/>
										         
										         <c:set var="docName" value="${docName}"/>
										         
										         <% String fin_heirarchy =(String)pageContext.getAttribute("financialHierarchy");
										           
										               String financialData = "";
										          
											            String docName =(String)pageContext.getAttribute("docName");
											            											            
											            
											            financialData = (String)request.getAttribute("financialData");
											            
										          
										           int year = (Integer)request.getAttribute("year");
										           
										          %>         
												 
										    <input type="hidden" id="flipValue" value="0" />
											<div id="financialAddDocumentFlip" ><button id="finButId" onclick="changeDesignStatus()">Add Financial Document</button></div>
										    <div id="financialAddDocumentPannel">
										        
										        <div style="width: 600px;">
										          <form name="FinancialForm" id="FinancialForm" enctype="multipart/form-data">
										          
										             <ul>
										                 <li>
												             From Year:<select id="fromYearId" name="fromYear" onchange="getNextYear()">
												                                <option  value="Select Year">Select Year</option>
												                                <%for(int i=2009;i<year+1;i++){%>
												                                <option value="<%=i%>"><%=i%></option>
												               				    <%} %>
												               				    </select>
												             To Year:<input type="text"   name="toYear" id="toYearId"  value="" disabled="disabled"/>
												             <div id="yearDiv" class="error" style="display:none;margin-left: 224px; margin-top: -39px;"></div>
												             <div id="finalyearDiv" class="success" style="display:none;"></div>
												           </li>
												         
												         
													             <li >
													             
													                <% if(!fin_heirarchy.isEmpty())
															           {
															        	   String heirarchyArray[] = fin_heirarchy.split(",");
																             %>
													                   Financial Document Type:<select name="docType" id="docTypeId" onchange="generateFinancialMode(this.id)">
													                                                         <option  value="Select Type">Select Type</option>
													                                                          <% for(int j=0;j<heirarchyArray.length;j++){%>
													                                                          <option value="<%=heirarchyArray[j]%>"><%=heirarchyArray[j].split(":")[0]%></option>
																				               				  <%} %>
																				               		        </select>		
																				               	<div id="fileTypeDiv" class="error" style="display:none;margin-left:239px;margin-top:-37px;"></div>	        	                   
													            
													            		<%} %>
													             </li>      
												                   <li>
												                   
												                        <div id="docmodecompId"> 
												                        
												                        
												                        </div>
												                        
												                        <div id="fileModeDiv" class="error" style="display:none;margin-left:239px;margin-top:-37px;"></div>
													                  	                   
													             </li> 	
												                   	     	                   		                                      
												                 
												           		          
												           <li >
													                   Financial Document:<input type="file" name="upl" id="financialfileId"/>
													                   <div id="fileDiv" class="error" style="display:none;margin-left:239px;margin-top:-37px;"></div>
													       </li> 
													      
												           <li >
												           <input class="cancelinner_button" type="Reset" value="Reset" "/><input class="saveinner_button" type="button" value="Save" onclick="ValidateFinancial()"/> 
												           </li>             		  
										              </ul>
										        
										         </form>
										        </div>
										    
										    </div>
										    	
											 <div id="center-body-div">
																
											   <div  id="panel-1">
											
											     <%for(int i=year+1;i>=2010;i--){%>
												        
												        <% int from = i-1;
												           int to = i;
												          		           
												          String yearDiff = from+"-"+to; %>
												          
												         <h3 align="left" onclick="visibleAccordian('accordianClass<%=yearDiff%>')">Financial Year : <b><%=yearDiff%></b></h3> 
												         		
													          <div id="accordianClass<%=yearDiff%>" style="display:none;">			                			                  
													                
																<a id="editregistrationhelp" href="financial_details_help.html" target="_blank"><p class="askhelp" tittle="Help For Basic Details"> </p></a>            						           
																              
													                  <%if(financialData.contains("&&"))
													                  { 			                    
													                	  String finDataArray[] = financialData.split("/");
													                	  
													                	  for(int f=0;f<finDataArray.length;f++)
													                	  {
														                	  if(finDataArray[f].split("&&")[0].equals(yearDiff))
														                	   {						                 
														                		  %>	
														                		        		  					                  						                  
																                  <div class="financialdocumentview">
																                  
																                  <ul>
																                  <li style="width:290px;background-color:#eee;"><%=finDataArray[f].split("&&")[1]%></li>
																                  <li style="width:155px;background-color:#eee;"><%=finDataArray[f].split("&&")[2]%></li>
																                  <li style="width:155px;background-color:#eee;">Attached On-<%=finDataArray[f].split("&&")[3]%></li>					                  
																                  <li style="width:60px;background-color:#eee;"><a href="#" onclick="getFinancialDetails('<%=finDataArray[f].split("&&")[4]%>')">View</a></li>
																                  <li style="width:60px;background-color:#eee;"><a href="#" onclick="getHistoryDetails('<%=finDataArray[f].split("&&")[4].split("_")[0]%>','<%=yearDiff%>')">History</a></li>
																                  </ul>
																          	     </div>
																               							                  
																                  						                  				                   						                 			                        			                       			                	 
													                	 <% } else { %>
													                	 			                	       			                	 
													                	 <% } //End Of Else  
													                	 
													                        }  //End of For    
													                	  			                	  
													                	 } %>
													                	 
													              
													               
													          </div>
												  
										          <%} %>
										          
										       </div> 
										
											</div>
											
											  
			               		
			               		
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

  
	
	
	
	
	
	
	    
</div> <!--end of right div  -->

	</div>

</div>
					
					
					
					
					
					
					
					
		<%
		
			ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			String version=resource.getString("kycversion");
			String versionDate=resource.getString("kyclatestDate");
			
		%>			
					
			
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	 <div id="waitlight">
			
		 
		              <div class="kycloader1">
		              
		              		 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									

                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	

	
<!-- EOF Waiting Div -->
	

   <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
		<!-- Document Pop up Div -->
	
	<div id="kycDocPopUplight">
			
			<div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:0; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF Document Pop up Div -->	

	
	<!-- session Div -->
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 		
	
	
<!-- EOF session Div -->

<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->

</body>
</html>
     	