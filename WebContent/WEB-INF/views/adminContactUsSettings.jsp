<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="resources/table/table.css">
<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<title>Insert title here</title>


<style>
.liAlign
{
  float:left;
  width:100%;
}

.divAlign
{
  float:left;
  margin-top:12px;
  text-align: left;
}


.designHiddenDiv
{   
    height: auto;
    width: 97%;
    background-color: #eee;
    border: 1px solid #ccc;
    border-radius: 5px;
    display: table;
    padding: 5px;
    
}


#IndividualDetailsAsPerData
{
   height: auto;
   margin-top: 100px;
   width: auto;
}




.searchliAlign
{
  float:left;
  margin-left: 15px;
}

.textStyle
{
    background-color: #ccc;
    height: 27px;
    margin-top: 10px;
    width: 400px;
}

.buttonStart
{
    height: 33px;
    margin-top: 9px;
    width: 65px;
}


#IndividualDetailsAsPerData
{
   height: auto;
   margin-top: 100px;
   width: auto;
}

#kycdocumentpage
{
    
}

</style>


<script>

function viewDocUti(name,contactfor,emailid,mobileNo,date,conId)
{	
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
					$.ajax({
						type : "post",
						url : "getcontactMessageByConId.html",
						data: "contactId="+conId,
						success : function(response) {
							
							   $("#waitingdivId").hide();
				
								$("#ShowContactDetails").slideDown();
								
								$("#nameId").html(name);
								$("#contactForId").html(contactfor);
								$("#EmailId").html(emailid);
								$("#contactNoId").html(mobileNo);
								$("#contactDateId").html(date);
								$("#messageId").html(response);
							
							},							
					});
					
				}
			else
				{
				       $("#waitingdivId").hide();

				       $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
	
}

function hideDivision()
{
	$("#ShowContactDetails").slideUp();
}

function showSearchCategory(searchType)
{
	if(searchType=="Email Id")
		{
		        document.getElementById("individualId").placeholder = "Search via Email Id";
		}
	else if(searchType=="Name")
		{
				document.getElementById("individualId").placeholder = "Search via Name";
		}
	else if(searchType=="Mode of Contact")
		{
				document.getElementById("individualId").placeholder = "Search via Mode of contact like(Business,Enquiry,Feedback)";
		}
	else
		{
				document.getElementById("individualId").placeholder = "Search via date (Enter same format mentioned in table)";
		}
	
}

function searchContactDetails()
{
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				    var flag = 'true';
				    var searchType = document.getElementById("directoryValue").value;
				    var searchValue = document.getElementById("searchValueId").value;
				    
				    if(searchValue.length<3)
				    {
				    	flag = 'false';
				    	alert('Please enter some search values');
				    }
				    if(searchType=="Search Contacts Via")
				    {
				    	flag = 'false';
				    	alert('Please select under search contact by.');
				    }
				   
				    if(flag=='true')
				    	{
					    	$.ajax({
								type: "post",
								url : "searchContactDetails.html",
								data: "searchType="+searchType+"&searchValue="+searchValue,
								success : function(response) { 
									   
									   $("#waitingdivId").hide();						  			   
									   $("#viewUsers").html(response);
									   
								},							
							});
				    	}							
				}
			else
				{
				       $("#waitingdivId").hide();

				       $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
}

</script>

</head>
<body>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>


<div id="encryptOfIndDoc">
     
     <ul>
         
        <li class="searchliAlign">         	
         	   
         	    <select id="directoryValue" onchange="showSearchCategory(this.value)">
               			
               			<option value="Search Contacts Via">Search Contacts Via</option>
               			<option value="Email Id">By Email Id</option>
               			<option value="Name">By Name</option>
               			<option value="Mode of Contact">By Mode of Contact</option>              			
               			<option value="Date">By Date</option>
               			
               </select>   
                     
         </li>
         
         <li class="searchliAlign"> 
         
              <input type="text" class="textStyle" id="searchValueId" placeholder="Search Contacts"  onchange="getThisUserDetails()"/>        
       
         </li>
          
         <li class="searchliAlign">
              
              <input class="buttonStart" type="button" value="Search"  onclick="searchContactDetails()" />
         
         </li>
          
     </ul>
      
</div>


   
   <div   id="IndividualDetailsAsPerData">
   
   
   </div>
   
   

<div id="ShowContactDetails" class="designHiddenDiv" style="display:none;">
     
     <ul>
     
         <li class="liAlign"><h1>Name:</h1><div class="divAlign" id="nameId"></div></li>
     
     	 <li class="liAlign"><h1>Contact For:</h1><div class="divAlign" id="contactForId"></div></li>
     
         <li class="liAlign"><h1>Email Id:</h1><div class="divAlign" id="EmailId"></div></li>
     
         <li class="liAlign"><h1>Contact No:</h1><div class="divAlign" id="contactNoId"></div></li>
         
         <li class="liAlign"><h1>Contact Date:</h1><div class="divAlign" id="contactDateId"></div></li>
         
         <li class="liAlign"><h1>Message:</h1><div class="divAlign" id="messageId"></div></li>
         
         <li class="liAlign" style="margin-top:50px;">
         
            <input type="button" value="Close" onclick="hideDivision()"/>
         </li>
     </ul>
     
     
</div>
   
   
    <div id="kycdocumentpage" class="kycdocumentpage" style="margin-left: 0px ! important;">
     
			<c:if test="${!empty contactUsDtails}">
					
				<table class='CSSTableGenerator' id="tableDivId" align="left" border="1" cellspacing="1" align="center" style="width: 98% !important;">
					
					<tr>					    			
						<th height="10">Contact Person</th>
						<th height="10">Belongs To</th>
						<th height="10">Email Id</th>		
						<th height="10">Mobile No</th>
						<th height="10">Mode Of Contact</th>
						<th height="10">Contact Date</th>
						<th height="10" >Message</th>
						<th height="10" >Extract</th>			
					</tr>
			        <%int i=0; %>
					
					<c:forEach items="${contactUsDtails}" var="det">					 
						 <tr>					
						 	    					    
						    <td width="100"><c:out value="${det.contact_name}"/></td>
						    <td width="60"><c:out value="${det.contact_type}"/></td>				
							<td width="100"><c:out value="${det.contact_emailid}"/></td>
							<td width="80"><c:out value="${det.contact_mobile_no}"/></td>	
							<td width="60"><c:out value="${det.contact_for}"/></td>	
							<td width="80"><c:out value="${det.contact_date}"/></td>	
							
						    <c:set var="contactMessage" value="${det.contact_message}" />
							
							<%String contactMessage = (String)pageContext.getAttribute("contactMessage"); %>
							
							<%if(contactMessage.length()>40){%>
							
								<td width="500" style="width:200px ! important;overflow:hidden;"><c:out value="<%=contactMessage.substring(0,39)%>"/>...</td>	
							
							<%}else{ %>
							
								<td width="500" style="width:200px ! important;overflow:hidden;"><c:out value="<%=contactMessage%>"/></td>	
							
							<%} %>
							
							<td width="50"><input type="button" value="View" onclick="viewDocUti('${det.contact_name}','${det.contact_for}','${det.contact_emailid}','${det.contact_mobile_no}','${det.contact_date}','${det.contact_id}')"/></td>											
						 
						 
						 </tr>
						 			
					</c:forEach>	
						
				</table>	
				
			</c:if>

            
			
     </div>
     
     
     <div style="margin-left: 290px !important;">
            <c:if test="${empty contactUsDtails}">
			
					<h1>As of now contact details not exist.</h1>
					
			</c:if>
     
     </div>



</body>
</html>