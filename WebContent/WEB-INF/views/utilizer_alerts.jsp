<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Alerts</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 
<%@ page import="java.text.SimpleDateFormat"%>  

<%@page import="java.text.DateFormat" %> 
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date,java.util.*"%>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">

<link rel="stylesheet" href="resources/kyc_css/tableAdmin.css" type="text/css" />
<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />


<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<style>


#waitfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

#sessionfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#sessionlight
{
    background: none repeat scroll 0 0 #fff;
    border: 2px solid #00b6f5;
    display: none;
    height: 46px;
    left: 50%;
     border-radius: 5px;
    margin-left: -272px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
      position: fixed;
    top: 50%; 
    width: 564px;
    z-index: 1002;
     right: -10px;  
}


</style>

<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

     background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 33%;
    width: 800px;
    z-index: 1002;
}

#page_navigation a{
	padding:3px;
	border:1px solid gray;
	margin:2px;
	color:black;
	text-decoration:none
}
.active_page{
	background:darkblue;
	color:white !important;
}
.activity1 {
		background:url('')no-repeat scroll 10px 50% #e3f7fc;
		border: 1px solid #8ed9f6;
		border-radius: 10px;
		color: #555;
		font-family: Tahoma,Geneva,Arial,sans-serif;
		font-size: 11px;
		display: inline-table;
		margin: 10px auto 0 0px;
		padding: 10px 14px;
		text-align: left;
        width: 90%;
} 

.activity2 {
 background: url("") no-repeat scroll 10px 50% #e9ffd9;
    border: 1px solid #a6ca8a;
    border-radius: 10px;
    color: #555;
    display: inline-table;
    font-size: 11px;
    margin: 10px auto 0 0;
    padding: 12px;
    text-align: left;
    width:90%;
}
.activity3 {
 background: url("") no-repeat scroll 10px 50% #e9ffd9;
    border: 1px solid #a6ca8a;
    border-radius: 10px;
    color: #555;
    display: inline-table;
    font-size: 11px;
    margin: 10px auto 0 0;
    padding: 12px;
    text-align: left;
    width:90%;
}

#utiactivityContent {

    margin-top: 25px;
    position: relative;
    width: 800px;
}

.utiactivitytable {
    border: 5px solid #fff;
    border-radius: 0;
    border-spacing: 0;
    box-shadow: 0 4px 10px #888888;
    display: inline-block;
    font-family: "Trebuchet MS","Lucida Grande",Verdana,Arial,sans-serif;
    font-size: 12px;
    height: auto;
    margin: 10px auto 0;
    overflow: hidden;
    width: 80%;
}
</style>



<script type="text/javascript">
			function resolveSrcMouseover(e) {
				alert("Hello mouse over");
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				if (node.nodeName != "UL") {
					node.style.fontWeight= "bold";
					showRollover(e, node.innerHTML);
				}
			}
			function resolveSrcMouseout(e) {
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				node.style.fontWeight = "normal";
				clearRollover(e);
			}
			function takeAction(e) {
				var node = e.srcElement == undefined ? e.target : e.srcElement;
									
				document.getElementById("DisplayInfo").innerHTML = "Clicked " + node.innerHTML;
				
				var id = node.getAttribute("id"); 
				if (id != null && id.indexOf("F") > -1) {
					if (node.innerHTML == "-") {
						node.innerHTML = "+";
						document.getElementById("EC" + id).style.display = "none";
					} else if (node.innerHTML == "+") {
						node.innerHTML = "-";
						document.getElementById("EC" + id).style.display = "block";
					}
				}
			}
</script>


<script type="text/javascript">


function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		       
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	   
	    });
	
}


	function userHome()
	{
		window.open("UtiHome.html",true);			     			
	}
	


	function docActivity()
	{
		document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		 $.ajax({
				type : "post",
				url : "doc_activity.html",
				cache : false,
				success : function(response) {
						
					document.getElementById('waitlight').style.display='none';
			        document.getElementById('waitfade').style.display='none';
			       
			        
					 $("#viewUsers")
						.html(response);
					},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		
		$("#titleBar").html("<h2><span>DOCUMENT ACTIVITY</span></h2>");
	}
	
	//End of Script for Profile
	
	
	// Start of Script for Alert Functionality 
	
	function utilizerActivity()
	{
		 $.ajax({
				type : "post",
				url : "utilzer_activity.html",
				success : function(response) {
										
					 $("#viewUsers")
						.html(response);
					},				
			});
		 		 		
		 $("#titleBar").html("<h2><span>My Activities</span></h2>");			
		 $("#searchicon").hide();
	 		 				
	}
	
	function utilizerNotification()
	{
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	   
		 $.ajax({
				type : "post",
				url : "utilzer_notification.html",
				success : function(response) {
					
					document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';
				   
					document.getElementById("Hi-B-2").className = "mynotificationactive";
					document.getElementById("Hi-B-1").className = "";
					
					$('#suggestions1').show();			
					$("#utileftsideId").show();					
					$('#searchhomepage').hide();
					
					 $("#viewUsers")
						.html(response);
					},				
			});
		 		 		
		 $("#titleBar").html("<h2><span>My Notification</span></h2>");				
		 
	}
	
	
	function gotoHome()
	{
	    window.location = "utimyProfile.html";	
	}
	
	function searchIndividual(){
		
		
		var selectedvalue = $("#selectedid").val();
	    var id=$("#inputString").val();
		if(id.length>=3)
		{					
		    $('#searchhomepage').show();
			
		    $('#searchicon').show();
		    $('.topprofilestyle').show();
		    
		    document.getElementById('waitlight').style.display='block';
	        document.getElementById('waitfade').style.display='block';
	       
			 $.ajax({  
			     type : "Post",   
			     url : "indsearchprofilefromUtilizer.html", 	  
			     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
			     success : function(response) 
			     {   	    	
			    	 if(response.length>3)
			    		 {		    		 												
							document.getElementById('waitlight').style.display='none';
						    document.getElementById('waitfade').style.display='none';
							$('#searchhomepage').html(response);
							
							 var obj = document.getElementById(searchhomepage);
							 
						    if (obj.style.display=="none"){
						      obj.style.display='block';
						    } else if(obj.style.display=="block"){
						      obj.style.display='none';
						    }
			    		 }
			    	 
			    	 else if(res==2)
			    		 {
			    		     $('#searchicon').show();
				    		 var values = "No data found";			    					    		
				    		 document.getElementById('waitlight').style.display='none';
				    	     document.getElementById('waitfade').style.display='none';
				    		 $('#searchhomepage').html(response);
				    		 $('.topprofilestyle').show();
			    		 }
			         },  	    
			    }); 
			}
		
		 if(id.length<3)
		    {
			    $('#searchhomepage').html("");		 
				document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';
				$('#searchhomepage').hide();
				 $('#searchicon').hide();
				 $('.topprofilestyle').hide();
		    }
	}

	

	function showSignOut()
	{
		 
		var signOutFlag = document.getElementById("signOutFlag").value;
		
		if(signOutFlag=='0')
			{
			   $("#loginBox").show();
			   document.getElementById("signOutFlag").value = "1";
			}
		if(signOutFlag=='1')
			{
			   $("#loginBox").hide();
			   document.getElementById("signOutFlag").value = "0";
			}
			 
	}

	 function checkSession(hittheUrl)
	 {
		 $.ajax({
				type : "post",
				url : "checksessionforUtilizer.html",
				success : function(response) {
					
					if(response=="Exist")
						{
						 	window.location = hittheUrl;
						}
					else
						{
						   $("#sessionlight").show();
					       $("#sessionfade").show();
						}
				}
			}); 
		
	 }

	 function redirectToLoginIndividual()
	 {
	 	window.location = "utisignOut.html";	
	 }

	 function hideAddvertise()
	 {
	     $("#ProfileHeadingDiv").fadeOut(90000);	 
	 }
	 
	 
	 function hidesearchDiv()
	 {
		 $('#searchicon').hide();
		 $('#searchhomepage').hide();
		 //$('#closediv').hide();
		 $('.topprofilestyle').hide();
		 document.getElementById("inputString").value = "";	 
	 }

</script>


<link rel="stylesheet" href="resources/css/PlanDesignResources/css/planmaster.css" media="screen">


</head>
<body onclick="hidesearchDiv()"> 


	<div class="container">
		
		<div class="top_line"></div>
		
		
		<header class="header">
		
		<%
		   String noOfAwaitedDocuments = (String)request.getAttribute("noOfAwaitedDocuments");
		   String noOfRecivedDocuments = (String)request.getAttribute("noOfRecivedDocuments");
		%>
		
		
		
		<div class="header_inner">
			<div class="logo" onclick="gotoHome()"></div>

			<div class="tfclear"></div>
			<div class="utimainmenu">
			<div id="utimenu_wrapper">
				<ul class="uti_main_menu">
					
					<li></li>
					<li id="myMenus1"><a class="myprofile" href="#" onclick="checkSession('utimyProfile.html')"> <br>
							<br> My Profile
					</a></li>
					
					<li id="myMenus2" style="width:140px">
					     
					        
					        <a class="mydocuments" href="#" onclick="checkSession('reciveddocuments.html')"> <br>
							<br> Received Documents
					        </a>
					     
					        <%if(!noOfAwaitedDocuments.equals("0")){ %>
												  
							     <div class="noti_bubble_right"><%=noOfAwaitedDocuments%></div>
							     
							 <%} %>
					     
					 </li>
					 					
					
					<li id="myMenus4"><a class="myplans" href="#" onclick="checkSession('utilizerPlan.html')"> <br> <br>
							My Plans					
					</a></li>
					
					
					<li id="myMenus6"><a  class="alearticonsfocus" href="#" onclick="checkSession('utilizeralerts.html')"><br>
							<br> Alerts
					</a>
					</li>	
					<li id="myMenus5"><a class="mysettings" href="#"  onclick="checkSession('utilizerSettings.html')"> <br> <br>
							My Settings					
					</a></li>
					
				</ul>
			</div>
			</div>
		</div>
				
				
					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
             
              <input type="hidden" id="signOutFlag" value="0" />
                                
             <a href="#" id="loginButton" onclick="showSignOut()">
                <span></span>
                <em id="loginNameId">${utilizerProfilePic.uti_first_name}</em></a>
                 <div style="clear:both"></div>
            
            
           <div style="clear:both"></div>
           
             <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="searchIndividual()" />
             </span>
             
            </div>
            
            <span class="topprofilestyle" style="display: none;"></span>	
						
						<div id="searchicon" style="display:none;">
																
							 <div id="searchhomepage" style="display:none;">
							
							 </div>
													
				       </div>
						
						
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="utisignOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
 
 
          

		</header>


		<div class="main"  id="mainDivID">
			
			<div class="main_center">
											
            	
                													
				<div class="utileftside">      <!--left menus should go here-->
										
						<div class="user_display">  
																				
								<div id="textBox">
																														
										<div id="button">
											   
											   <ul>
											       <li  id="myactivities"><a href="utilizeralerts.html" id="Hi-B-1" class="myactivitiesactive"><span>My Activity</span></a></li>
												   <li id="mynotification"><a href="#" id="Hi-B-2" onclick="utilizerNotification()"><span>My Notification</span></a></li>													
											   </ul>
											   
										</div>																				
								</div> 			
								
								<!-- End of textBox Div  -->
								
													
						</div>           <!-- End of user_display -->
					
				</div>    <!-- End of left side Div -->


				
																	
				<div id="utiRightSideDivId" class="utirightside">
												       
								<!--center body should go here-->
					
					
						
						<!-- Advertisment Div for Entire Page  -->
																		
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
																
							%>
                                  <div id="ProfileHeadingDiv" style="display:block;">
									
										<marquee behavior="scroll" scrollamount="5" direction="left">
											   <h1 class="rightsideheader">
											       
											        <%=utiAdvertise%>
											        
											   </h1>
									     </marquee>
							        </div>	
							       
							        		
					   <!-- Advertisment Div for Entire Page  -->
					   
					   
						<div id="suggestions1">
																 																
								  <div class="border_line"></div>
								  
								  <div class=" username_bar">
										
										<div id="titleBar">
											
											<h2>
												 	<span>My Activity</span>									    												
											</h2>
											
										</div>     
										
									 <!-- End of tittleBar Div -->
										
								  </div>			<!-- End of username_bar Div -->
									
								
								
							
								<div class="utimodule-body">
									
											<div id="viewUsers" style="height: auto; width: auto;  margin: 0 auto;">
					                           
							                    <!-- Statr Div of viewUsers -->     
							                         
								     								     
													<div id='utiactivityContent'>
													
													<table style='width: 100%' border="0"></table>
												
													<c:if test="${not empty utilizerActivity}">
													<%int i=1;%>
													  
												    <div class="table">
													      
													       <ul>
														    
															    <c:forEach items="${utilizerActivity}" var="det">				   			   
																  		 				  			
																  			<c:set var="activityDateStamp"  value="${det.activity_date}"/>
		
																			 <%
																			     Date activityTime = (Date)pageContext.getAttribute("activityDateStamp");
																			 
																				 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");									 							
																				 String reportDate = df.format(activityTime);
																				 
																				 String input = reportDate;
																    			 DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
																    			 DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd KK:mm a"); 
																    			 
																	            
																	         %>	  		
																		
																		    <c:set var="activityData"  value="${det.activity_details}"/>
																		    <%
																		      String activityData = (String)pageContext.getAttribute("activityData");
																		      String activityArray[] = activityData.split(" on ");
																		      
																		    %>
																		
																		<%if(i%2!=0){ %>
																		
																			<div class="activity3"> <b> <c:out value="<%=outputFormat.format(inputFormat.parse(input))%>"/> </b> : <c:out value="<%=activityArray[0]%>"/></div>							
																		
																		<%}else{ %>
																		
																			<div class="activity1"> <b> <c:out value="<%=outputFormat.format(inputFormat.parse(input))%>"/> </b> : <c:out value="<%=activityArray[0]%>"/></div>
																		
																		<%} %>  
																			
																			<%i++;%>																								
																</c:forEach>
																				    				    
														   </ul>
														  		
												   </div>
												   </c:if>
												   
												   
												   <c:if test="${empty utilizerActivity}">
													            
													                 <div class="notice" style="margin-left: 245px;margin: 72px auto 0;width: 250px;">
																		<b>NOTICE :</b>
																		Nothing is updated in your Profile!
																		</div>
													        
													</c:if>
													
												</div>
																																     
							                    
							                    <!-- End of Div of viewUsers -->        							                            							                          
					
									</div>			
			
								</div>     
								
								 <!-- End of module-body Div -->
							


				  </div>				 <!-- End of rightside div -->
				
				
				<br />


              </div>    <!-- Suggestion div -->
              
              
			</div>            <!-- End of Main Center div -->
			
		</div>          <!-- End of Main div -->









		<!--footer block should go here-->
		
		<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>

		
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="utiContactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	
			<div class="footer_line"></div>

	</div>
	<!------container End-------->
	




<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
								
								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give Us Few Seconds.....
						       </div>
									
                      </div>
                    
	</div>
	
	<div id="waitfade" ></div> 	

	
<!-- EOF Waiting Div -->

<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->
	
			
	
	
	
</body>
</html>
