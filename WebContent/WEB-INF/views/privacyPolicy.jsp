 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="${pageContext.session.maxInactiveInterval};url=outduesession.html">

<title>Clensee Privacy Policy</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/onlineresources/code.jquery.com.ui.1.10.2.jquery-ui.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}




.termsheading
{
    color: #00b5f6;
    font-size: 16px;
    margin-left: 15px !important;
    text-align: left;
}

.termsContent
{
    font-size: 14px;
    padding-left: 15px;
    padding-right: 15px;
    text-align: justify;
    color: #7c7c7c;
}
</style>



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

</style>

<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}


</script>


      
<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>

</head>

<body>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="clenseeHome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			

		</div>
		</div>
 
 <%
 
     //Required file Config for entire Controller 
	
	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 String kycMainDirectory = fileResource.getString("fileDirectory");	
	 File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller 
 
 %>
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
					<div id="helpHeading" style="display: inline-table;margin-left: 5px ! important;margin-top: 18px ! important;">
					
					<h1>Privacy Policy</h1>


					<h3 class="termsheading">We Collect</h3>
					
					<p class="termsContent">We collect the personal information that you provided upon creation of your own account on our website, which includes your profile, key documents etc. You can present all your profile and share documents to our registered Utilizers and other individuals which you intend to share. Some basic profile information is visible to all the users (Both Individual and Utilizers), however, your profile visibility can be controlled by visibility settings.</p>
					<p class="termsContent">If you are not at all intended to provide any information or share any documents, you are not obligated to do so.</p>
					<h3 class="termsheading">Modification of Information</h3>
					<p class="termsContent">Your Information can be modified by only you alone, however latest data and previous data stored in our cloud is visible to users. If you do not wish to share the previous data, please approach us on <strong>support@clensee.com</strong> for any corrections, if you did not share your profile or Docs to Utilizers previously.</p>
					<p class="termsContent">Our terms clearly specify that if you share your data to the Utilizers even if you want your previous data not to present for Utilizers. Correction is only possible by acceptance of all Utilizers that you have given sharing.</p>
					<h3 class="termsheading">Cookies</h3>
					<p class="termsContent">There is a technology called "cookies" which can be used to provide you with tailored information from a Web site. A cookie is an element of data that a Web site can send to your browser, which may then store it on your system. Some CLENSEE pages use cookies so that we can better serve you when you visit and then return to our site. We use cookies to deliver information specific to your interests and to save your password so you don't have to re-enter it each time you visit certain areas of our Web site. Your use of this site constitutes your acceptance.</p>
					<p class="termsContent">If you do not want to accept cookies from our website, you may refuse a cookie, or delete the cookies file from your computer at any time using available methods. Most browsers have a setting to turn off the automatic acceptance of cookies. You should be aware few portions of our website may not function properly if you do not accept cookies.</p>
					<p>&nbsp;</p>
					<h3 class="termsheading">Age Limit</h3>
					<p class="termsContent">Our Clensee product services are applicable to all persons whose age limit is above 18 years.</p>
					<h3 class="termsheading">Security</h3>
					<p class="termsContent">We have implemented technology and current internet security features and strict policy guidelines to safeguard the privacy of your personally identifiable information from unauthorized access and improper use or disclosure.</p>
					<h3 class="termsheading">Law and Jurisdiction</h3>
					<p class="termsContent">We shall be governed by and construed in accordance with the laws of India, without regard to principles of conflicts of law, within the jurisdiction of the Courts of Hyderabad.</p>
					<h3 class="termsheading">Change to Policy</h3>
					<p class="termsContent">We will keep you updated on changes in our policies, if any.</p>
					
					
							
					</div>
			
         		
								<div class="leftside">
								
								
								</div>
					
					
                    
                                                                      
	            </div>

     </div>
	
<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>					
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactusbeforeLogin.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>
<!-- Waiting Div -->
	
	<div id="waitlight">
			
		    <div id="waitdisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:300px;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight" style="display:none;">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	<!-- kycDoc Pop up Div -->
	
	<div id="kycDocPopUplight" style="display:none;">
			
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" style="display:none;" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF kycDoc Pop up Div -->	

	
	
	
	
</body>
</html>
     	