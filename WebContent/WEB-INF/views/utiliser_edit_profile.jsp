
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Edit Profile</title>

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>
<script src="resources/kyc_scripts/jquery-ui-10.js"></script>
<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<script type="text/javascript">

function saveBasic() {
						
	    var formData = new FormData($("#BasicForm")[0]);
	    
	    $.ajax({
	        url: "save_Uti_Basic.html",
	        type: 'POST',
	        data: formData,
	        async: false,
	        success: function (data) {  
	        	
	        	alert("Information Details Updated");
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });

	    return false;
	
	}
	
	
function saveProfessional() {
					
	    var formData = new FormData($("#ProfForm")[0]);
	    
	    $.ajax({
	        url: "save_Uti_Professional.html",
	        type: 'POST',
	        data: formData,
	        async: false,
	        success: function (data) {          
	           
	        	alert("Information Details Updated");
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });

	    return false;

	}



function basicChange(id)
{
	
	    if(id=="Basic1")
		{
		document.getElementById("profileupdate").innerHTML += 'KYCID changed';
		}
	    if(id=="Basic2")
	    {
	    document.getElementById("profileupdate").innerHTML += 'FirstName changed';
		}
   	    if(id=="Basic3")
	    {
	    document.getElementById("profileupdate").innerHTML += 'MiddleName changed';
		}
	    if(id=="Basic4")
	    {
	    document.getElementById("profileupdate").innerHTML += 'LastName changed';
		}
	    if(id=="Basic5")
	    {
	    document.getElementById("profileupdate").innerHTML += 'EmailId changed';
		}
	   
        document.getElementById("profile_update_value").value=document.getElementById("profileupdate").innerHTML;
}

function profChange(id)
{
	
	    if(id=="Prof1")
		{
		document.getElementById("utiliserupdate").innerHTML += 'KYCID changed';
		}
	    if(id=="Prof2")
	    {
	    document.getElementById("utiliserupdate").innerHTML += 'AboutUti changed';
		}
   	    if(id=="Prof3")
	    {
	    document.getElementById("utiliserupdate").innerHTML += 'Company Details changed';
		}
	    if(id=="Prof4")
	    {
	    document.getElementById("utiliserupdate").innerHTML += 'Present Address changed';
		}
	    if(id=="Prof5")
	    {
	    document.getElementById("utiliserupdate").innerHTML += 'Permanent Address changed';
		}
	   
        document.getElementById("utiliser_update_value").value=document.getElementById("utiliserupdate").innerHTML;
}
</script>

</head>
<body>


 <div id="center-body-div">
		
	<div  id="panel-1">
	
 <h3 align="left"><b>Basic Details</b>
 </h3> 
         
 <div>
        
       
         <form id="BasicForm" name="BasicForm">         
          
           <c:if test="${!empty editBasicProfile}">
              <c:forEach items="${editBasicProfile}" var="det">	
           
           <table align="center" border="0" class="tabledesign" style="height:250px">
		   
		   <tr>
		   <th>KYC_ID</th><td><input type="text" id="Basic1" name="kycid" value="${det.kyc_uti_id}" onchange="basicChange(this.id)"/></td>
		   </tr>
		   <tr>
		   <th>First Name</th><td><input type="text" id="Basic2" name="uti_firstName" value="${det.uti_first_name}" onchange="basicChange(this.id)"/></td>
		   </tr>
		   <tr>
		   <th>Middle Name</th><td><input type="text" id="Basic3" name="uti_middleName" value="${det.uti_middle_name}" onchange="basicChange(this.id)"/></td>
		   </tr>
		   <tr>
		   <th>Last Name</th><td><input type="text" id="Basic4" name="uti_lastName" value="${det.uti_last_name}" onchange="basicChange(this.id)"/></td>
		   </tr>
		   <tr>
		   <th>Email Id</th><td><input type="text" id="Basic5" name="uti_EmailId" value="${det.uti_email_id}" onchange="basicChange(this.id)"/></td>		   
		   </tr>
		   <tr>
		   <td><input type="button" value="Submit" onClick="saveBasic()"/></td>
		   </tr>
		   <tr><td>
		   
		   <div  style="display:none;visibility:hidden;" id="profileupdate"></div>
		   <input type="hidden" id="profile_update_value" name="basic_profile_update"/>
		   
		   </td></tr>
	     </table>
	     </c:forEach>		
	     </c:if>  
	     
	     
	      <c:if test="${empty editBasicProfile}">
              
           
         <table align="center" border="0" class="tabledesign" style="height:250px">
		   
		   <tr>
		   <th>KYC_ID</th><td><input type="text" name="kycid" value=""/></td>
		   </tr>
		   <tr>
		   <th>First Name</th><td><input type="text" name="uti_firstName" value=""/></td>
		   </tr>
		   <tr>
		   <th>Middle Name</th><td><input type="text" name="uti_middleName" value=""/></td>
		   </tr>
		   <tr>
		   <th>Last Name</th><td><input type="text" name="uti_lastName" value=""/></td>
		   </tr>
		   <tr>
		   <th>Email Id</th><td><input type="text" name="uti_EmailId" value=""/></td>
		   </tr>
		   <tr>
		   <td><input type="button" value="Submit" onClick="saveBasic()"/></td>
		   </tr>
		   
	     </table>
	     
	     </c:if>  
	     
         </form>    
 </div>
        
        
        
        
  <h3 align="left"><b>Professional Details</b></h3>
       <div >
 
           <form id="ProfForm" name="ProfForm">
              
           <c:if test="${!empty editBasicProfile}">
           <c:forEach items="${editBasicProfile}" var="det">	  
	       <table align="left" border="0" class="tabledesign" style="height:50px">		   
		   <tr>
		   <td style="width:50px"><input type="hidden" name="kycid" value="${det.kyc_uti_id}"/></td>		   
		   </tr>
	       </table>
	       </c:forEach>
	       </c:if>   
              
           
                     
           <c:if test="${!empty editProfessional}">
           <c:forEach items="${editProfessional}" var="det">	  
              
           <table align="left" border="0" class="tabledesign" style="height:450px">
		   		   		   		  
		   <tr>
		   <th>About us</th><td><textarea name="about_uti" id="Prof2" style="width: 300px; height:50px" onchange="profChange(this.id)">${det.about_utiliser}</textarea></td>
		   </tr>
		   <tr>
		   <th>Company Details</th><td><textarea name="about_cmy" id="Prof3" style="width: 300px; height:50px" onchange="profChange(this.id)">${det.about_company}</textarea></td>
		   </tr>
		   <tr>
		   <th>Present Address</th><td><textarea name="present_Address" id="Prof4" style="width: 300px; height:50px" onchange="profChange(this.id)">${det.present_address}</textarea></td>
		   </tr>
		   <tr>
		   <th>Permanent Address</th><td><textarea name="permanent_Address" id="Prof5" style="width: 300px; height:50px" onchange="profChange(this.id)">${det.permanent_address}</textarea></td>
		   </tr>	
		   <tr>
		   <td><input type="button" value="Submit" onClick="saveProfessional()"/></td>
		   </tr>
		   
		   <tr><td>
		   
		   <div  style="display:none;visibility:hidden;" id="utiliserupdate"></div>
		   <input type="hidden" id="utiliser_update_value" name="prof_profile_update"/>
		   
		   </td></tr>
	       </table>  
	      
	       </c:forEach>
	       </c:if>	
	       
	       
	       
	       
	       <c:if test="${empty editProfessional}">               
              
           <table align="left" border="0" class="tabledesign" style="height:450px">		   		   		   		  
		   <tr>
		   <th>About us</th><td><textarea name="about_uti" style="width: 300px; height:50px"></textarea></td>
		   </tr>
		   <tr>
		   <th>Company Details</th><td><textarea name="about_cmy" style="width: 300px; height:50px"></textarea></td>
		   </tr>
		   <tr>
		   <th>Present Address</th><td><textarea name="present_Address" style="width: 300px; height:50px"></textarea></td>
		   </tr>
		   <tr>
		   <th>Permanent Address</th><td><textarea name="permanent_Address" style="width: 300px; height:50px"></textarea></td>
		   </tr>	
		   	<tr>
		   <td><input type="button" value="Submit" onClick="saveProfessional()"/></td>
		   </tr>
	       </table>  	      	     
	       </c:if>		            
           </form> 
   
           </div>
 
 
 
 </div></div> 
</body>
</html> 