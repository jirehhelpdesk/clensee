 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Documents</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />


<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/jscripts/jquery.com.jquery-1.10.2.js"></script>
<script src="resources/jscripts/jquery.com.jqueryUi.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />


<link rel="stylesheet" href="resources/documentResources/kyc_document_css.css" />


<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight
{
    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

</style>



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}





</style>

<script>

function fadeoutReport(action)
{
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
	{
		 window.location="myDocumentsMain.html";
	}
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	    
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	    
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	     
	    });
}

</script>





<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		     $('#searchhomepage').html("");		
		    		     $('#searchicon').hide();
			    		 var values = "No data found";			    		 
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
			    		 
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
}


function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}


function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}

function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}
 
</script>





<script>

/* All Scripts for MyDocument pannel  */

	
	
	
	function myDocuments2(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 window.location = "search_financialDoc.html";
										
				},
				
			});		 		 		 				
	    }
	
	
	function reSelect() {
				
	 $.ajax
	      ({
			type : "post",			
			url : "reselect.html",			
			success : function(response) {
														
				$("#viewUsers1")
					.html(response);
												
			},
			error : function() {
				alert('Error while fetching response');
			}
		});	
	 $("#titleBar").html("<h2><span>DOCUMENTS</span></h2>");

	}  
		
		/* <!-- End of code for noneditable to editabel for document-->  */
 	 	 	 	 		 
</script>

            
<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<!-- Script and CSS for KYC Documents -->
 
<script type="text/javascript">
function validateonEach(urlValue,num,kycformName)
{
	
	var id = document.getElementById("name"+urlValue).value;
	var des = document.getElementById("des"+urlValue).value;
	var file = document.getElementById("file"+urlValue).value;
	var fileback = document.getElementById("fileback"+urlValue).value;
	
	if(id!="")
	{
	   var pattern = /^[a-zA-Z0-9]+$/;
	   if(!id.match(pattern))  
         {  	
		     $("#Id1"+kycformName).show(); 
		     $("#Id1"+kycformName).html("Enter a proper id."); 
         }
	   else
		 {
		     $("#Id1"+kycformName).hide(); 
		 }
	}
	if(id=="")
	{
		$("#Id1"+kycformName).hide(); 
	}

	if(des=="")
	{   
		$("#Id2"+kycformName).hide();	
	}
	
	if(des!="")
	{
		var pattern = /^[a-zA-Z0-9_ ]+$/;
		if(!des.match(pattern))
			{
		       $("#Id2"+kycformName).show();
		       $("#Id2"+kycformName).html("Enter proper description.");
			}
		else
			{
			   $("#Id2"+kycformName).hide();
			}
	}
	
	if(file!="")
	{
	   $("#Id3"+kycformName).hide();
	   $(".frontimgshow").html(file)
	}
	
	if(fileback!="")
	{
	   $("#Id4"+kycformName).hide();
	   $(".frontimgshow1").html(fileback)
	}
}
	

function validateGivenKycId()
{
	var id = document.getElementById("name"+urlValue).value;
	
	$.ajax({
	        url: "checkKYCIdExistOrNot.html",
	        type: 'POST',
	        data: "kycId="+id,
	        success: function (data) {          
	            
	        	if(data=="Yes")
	        		{	        		
	        		$("#Id1"+num).show(); 
	        		$("#Id1"+num).html("Given id already exist by another individual."); 
	        		return false;
	        		}
	        	else
	        		{
	        		$("#Id1"+num).hide(); 
	        		}
	        },		        
	    });			
}


function valiDateKyc(urlValue,num,kycFormName)
{	
	var flag = 'true';
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
    
	var id = document.getElementById("name"+urlValue).value;
	var des = document.getElementById("des"+urlValue).value;
	var file = document.getElementById("file"+urlValue).value;
	var fileback = document.getElementById("fileback"+urlValue).value;
	
	var poi= /^[A-Za-z0-9]*$/;
	
	if(id!="")
	{		
	   if(!id.match(poi))  
          {    
		     document.getElementById('waitlight').style.display='none';
		     document.getElementById('waitfade').style.display='none';
		     
		     flag='false';		     
		     $("#Id1"+kycFormName).show(); 
		     $("#Id1"+kycFormName).html("Enter a valid id."); 
		     return false;
         }
	}
	
	
	if(id=="")
	{
		 document.getElementById('waitlight').style.display='none';
	     document.getElementById('waitfade').style.display='none';
	     
		 flag='false';
		 $("#Id1"+kycFormName).show(); 
	     $("#Id1"+kycFormName).html("Enter a your card id."); 
	     return false;
	}

	if(des=="")
	{   
		 document.getElementById('waitlight').style.display='none';
	     document.getElementById('waitfade').style.display='none';
	     
		   flag='false';
	       $("#Id2"+kycFormName).show();
	       $("#Id2"+kycFormName).html("Enter some description.");
	       return false;
	}
	
	if(des!="")
	{
		var pattern = /^[a-zA-Z0-9_ ]+$/;
		if(!des.match(pattern))
			{
			 document.getElementById('waitlight').style.display='none';
		     document.getElementById('waitfade').style.display='none';
		     
			   flag='false';
		       $("#Id2"+kycFormName).show();
		       $("#Id2"+kycFormName).html("Don't use special characters.");
		       return false;
			}
		else
			{
			   $("#Id2"+kycFormName).hide();
			}
	}
	
	if(file=="")
	{
		 document.getElementById('waitlight').style.display='none';
	     document.getElementById('waitfade').style.display='none';
		flag='false';
		$("#Id3"+kycFormName).show();
		$("#Id3"+kycFormName).html("Please upload front side file.");  	
		return false;
	}

	if(file!="")
		{
		    var ext=file.substring(file.lastIndexOf('.')+1);
		    var extension = new Array("jpg","jpeg","gif","png","pdf");						    
		    var condition = "NotGranted";
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}
		    
			if(condition=="NotGranted")
				{
				 document.getElementById('waitlight').style.display='none';
			     document.getElementById('waitfade').style.display='none';
				   flag = 'false';
				   alert("pdf/image files are only allowed.");
				   return false;
				}
		}
	
	var fileDetails = document.getElementById("file"+urlValue);
	var fileSize = fileDetails.files[0];
	var fileSizeinBytes = fileSize.size;
	var sizeinKB = +fileSizeinBytes / 1024;
	var sizeinMB = +sizeinKB / 1024;
	
	if(sizeinMB>2)
		{
		 document.getElementById('waitlight').style.display='none';
	     document.getElementById('waitfade').style.display='none';
		   flag = 'false';
		   alert("File size should not more then 2 MB.");
		   return false;
		}
	
	
	/* if(fileback=="")
	{
		 document.getElementById('waitlight').style.display='none';
	     document.getElementById('waitfade').style.display='none';
		flag='false';
		$("#Id4"+kycFormName).show();
		$("#Id4"+kycFormName).html("Please upload back side file.");  	
		return false;
	}  */

	if(fileback!="")
		{
		    var ext=fileback.substring(fileback.lastIndexOf('.')+1);
		    
		    var extension = new Array("jpg","jpeg","gif","png","pdf");						    
		    var condition = "NotGranted";
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}
		    
			if(condition=="NotGranted")
				{
				 document.getElementById('waitlight').style.display='none';
			     document.getElementById('waitfade').style.display='none';
				   flag = 'false';
				   alert("pdf/image files are only allowed.");
				   return false;
				}
			
			var fileDetails1 = document.getElementById("fileback"+urlValue);
			var fileSize1 = fileDetails1.files[0];
			var fileSizeinBytes1 = fileSize1.size;
			var sizeinKB1 = +fileSizeinBytes1 / 1024;
			var sizeinMB1 = +sizeinKB1 / 1024;
			
			if(sizeinMB1>2)
				{
				 document.getElementById('waitlight').style.display='none';
			     document.getElementById('waitfade').style.display='none';
				   flag = 'false';
				   alert("File size should not more then 2 MB.");
				   return false;
				}
			
			sizeinKB = sizeinKB+sizeinKB1;
			
		}
	
	
	
if(flag=='true')
{
	
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{			
				
						document.getElementById("kycFormPageLight"+urlValue+"").style.display='none';
						document.getElementById("kycFormPageFade"+urlValue+"").style.display='none';
						document.getElementById("table_pkg"+urlValue+"").style.display ="none";	
						
							$.ajax
						    ({
								type : "post",				
								url : "validateFileStorage.html",		
								data:"givenfileSize="+sizeinKB,
								success : function(response) {
												
									if(response=="withinStorage")
									{
										var formName = "KycForm"+kycFormName;
										
										var formData = new FormData($("#"+formName)[0]);
										
									    $.ajax({
									        url: "kyc_documents.html?path="+urlValue+"",
									        type: 'POST',
									        data: formData,
									        //async: false,
									        success: function (data) {          
									           
									        	      	
									        	if(data=="Yes")
									        		{	
									        		     document.getElementById('waitlight').style.display='none';
									    		         document.getElementById('waitfade').style.display='none';
									    		         
									    		        
									    			     document.getElementById('sociallight').style.display='block';
									    			     document.getElementById('socialfade').style.display='block';
									    			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
									    				 $("#socialdisplaySharedDocDiv").html("Given id already exist by another individual. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
								       		   
									        		     document.getElementById("name"+urlValue).value="";
									        		}
									        	else
									        		{			        		
									        		     document.getElementById('waitlight').style.display='none';
									    		         document.getElementById('waitfade').style.display='none';
									    		     	
									    		         
									    		         document.getElementById("kycFormPageLight"+urlValue).style.display='none';
									    		         
									    			     document.getElementById('sociallight').style.display='block';
									    			     document.getElementById('socialfade').style.display='block';
									    			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
									    				 $("#socialdisplaySharedDocDiv").html("Your documents has been saved successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\"  class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");  		       		   			    				
									        		}
									        	
									        },
									        cache: false,
									        contentType: false,
									        processData: false
									    });
									}
									else
									{
										 document.getElementById('waitlight').style.display='none';
									     document.getElementById('waitfade').style.display='none';
										 alert("Failed due to storage limitation,to upload more documents migrate your plan.");
									}			
								},
						    });
							
				
				}
			else
				{
					document.getElementById("kycFormPageLight"+urlValue+"").style.display='none';
					document.getElementById("kycFormPageFade"+urlValue+"").style.display='none';
					document.getElementById("table_pkg"+urlValue+"").style.display ="none";	
				
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
				   $("#sessionfade").show();
				}
		}
	}); 
					
  }

}


</script>

<script type="text/javascript">
		
function reChooseID() {
	
	 $.ajax
          ({
			type : "post",				
			url : "choose_IDs.html",						
			success : function(response) {
										
				 $("#viewUsers1")
					.html(response);
								
			},
			error : function() {
				alert('Error while fetching response');
			}
		});		 		 		 		
	$("#titleBar").html("<h2><span>DOCUMENTS</span></h2>");
	
}  


function viewHistory(urlLink,docType)
{	
	$.ajax
    ({
		type : "post",				
		url : urlLink,						
		success : function(response) {
									
			 $("#viewUsers1")	
				.html(response);
							  
		},
		error : function() {
			alert('Error while fetching response');
		}
	});	
	
   $("#titleBar").html("<h2><span>"+docType+" History</span></h2>");
}
</script>



<script type = "text/javascript">


function toggle(l) 
{
	document.getElementById("kycFormPageLight"+l+"").style.display='block';
	document.getElementById("kycFormPageFade"+l+"").style.display='block';
	document.getElementById("table_pkg"+l+"").style.display ="block";	
}

function cancelToggle(l)
{	
	document.getElementById("kycFormPageLight"+l+"").style.display='none';
	document.getElementById("kycFormPageFade"+l+"").style.display='none';
	document.getElementById("table_pkg"+l+"").style.display ="none";
	
	$(".frontimgshow").html("Browse");
	$(".frontimgshow1").html("Browse");
	
	document.getElementById("name"+l+"").value = "";
	document.getElementById("des"+l+"").value = "";
	document.getElementById("file"+l+"").value = "";	
	
	
}


function kycHistory(compType)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';	       
    
	 $.ajax({
			type : "post",				
			url : "kychistory.html",				
			data :"fileName="+compType,	  
			success : function(response) {
			
				document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';	       
			    
			    document.getElementById('kycHistoryLight').style.display='block';
				document.getElementById('kycHistoryFade').style.display='block';
				$("#kycHistoryDisplay").html(response);
			    
							
			},
		
		});		 		
}

function fadeOutHistoryDiv()
{
	document.getElementById('kycHistoryLight').style.display='none';
	document.getElementById('kycHistoryFade').style.display='none';
}

</script>


<!-- Script for Reselect KYC Document Type -->

<script>

function chooseIds(handlerToHit) {
		
	x=document.getElementById("Proceed")
    x.disabled = !x.disabled;
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	var click = [];
	$(".checkbox:checked").each(function() {
        click.push($(this).val());
    });
	 	
	if(click.length==0)
		{ 		  
		   document.getElementById('waitlight').style.display='none';
	       document.getElementById('waitfade').style.display='none';	       
	    
			x=document.getElementById("Proceed")
	    	x.disabled = !x.disabled;
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		     
			 $("#socialdisplaySharedDocDiv").html(" Select at least one document type. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div> ");  

            
		}
	else
		{
		
	 $.ajax({
			type : "post",				
			url : handlerToHit,				
			data :"click="+click,	  
			success : function(response) {
			
				document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';	       
			    
				x=document.getElementById("Proceed")
			    x.disabled = !x.disabled;
				
			
			     document.getElementById('sociallight').style.display='block';
			     document.getElementById('socialfade').style.display='block';
			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
			     
			     $("#socialdisplaySharedDocDiv").html("Kyc document has been added successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\" /> </div>");
	        					
			},
		
		});		 		 		 		
	$("#titleBar").html("<h2><span>KYC DOCUMENT</span></h2>");
	
		}//End Of else
}


</script>
 
<style type="text/css"> 
#panel,#flip
{
padding:5px;
text-align:left;
background-color:white;
border:solid 1px #c3c3c3;
width:328px;
text-align: center;
}
#panel
{
  display: none;
    padding-bottom:15px;
    padding-top: 0;
    text-align: left !important;
    width: 80%;
}
.adddocpanel{
 display: inline-table !important;
    height: auto;
    padding-bottom: 15px;
    padding-top: 0;
    text-align: left !important;
    width: 100%;
    }
#panel li{
    cursor: pointer;
    float: left;
    padding:5px;
      width: 30%;
}
.adddocpanel li span{
 padding-left: 3px;
    padding-top: 2px;
}
.checkbox{
    vertical-align: sub;
}
</style>


<!-- End of Script and CSS for KYC Documents -->




<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}

</style>
 
<script>

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewKycDocumnt(fileName)
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						document.getElementById('waitlight').style.display='block';
					    document.getElementById('waitfade').style.display='block';
	
						$.ajax({
							type : "get",				
							url : "viewKycDoc.html",
							data: "fileName="+fileName,
							success : function(response) {
							
								document.getElementById('waitlight').style.display='none';
						        document.getElementById('waitfade').style.display='none';
	
								document.getElementById('kycDocPopUplight').style.display='block';
						        document.getElementById('kycDocPopUpfade').style.display='block';
						        $("#kycDocPopUpDisplay").html(response);
						        			
							},		
						});		 		 		 		
					
					}
				else
				{
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 
	
}


/* Main Header Actions  */

function gotoProfile()
{
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					    window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoMyDocument()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoSharingManager()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoStore()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoAlerts()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
}

function gotoPlans()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
}

function gotoSetings()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}


function ChangeFlipStatus()
{
	var dropId = document.getElementById("flipedId").value;
	if(dropId=="0")
		{
		   document.getElementById("flip").style.color = "#05b7f5";
		   $("#flip").html("Select Kyc Document");
		   document.getElementById("flipedId").value = "1";		  
		}
	else
		{
		   document.getElementById("flip").style.color = "";
		   $("#flip").html("Select Kyc Document");
		   document.getElementById("flipedId").value = "0";		   
		}
}

function slidePanelForBodyAction1()
{
	 document.getElementById("flip").style.color = "";
	 $("#flip").html("Choose Type of  KYC Document");
	 document.getElementById("flipedId").value = "0";
	 $("#panel").slideUp();	 
}

function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}	

function visibleAccordian(accordianId)
{
   $("#"+accordianId).slideToggle("slow");
}

</script> 
 
<style type="text/css">
 
.message 
{
    /* background-color: #ccc; */
    border-radius: 12px;
    left: 8%;
    margin: 1px;
    padding: 22px;
    position: fixed;
    top: 73%;
    width: 200px;
    color:#00b6f5;
}

</style>

</head>

<body onclick="hidesearchDiv(),slidePanelForBodyAction()" onload="hideAddvertise()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocumentsfocus"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br><br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 
 <%
 
     //Required file Config for entire Controller 
	
	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 String kycMainDirectory = fileResource.getString("fileDirectory");	
	 File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller 
 
 %>
 					<!-- Login Starts Here -->
            <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em>
             </a>
             
             <div style="clear:both"></div>
            
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>							
								
						</div>
						
			<!-- End of Search Division -->	
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
         		
					<div class="leftside">
					
							<div id="textBox">
									<div id="button">
									  <ul>
									     <li id="Hi-B-1"><a href="myDocumentsMain.html"  id="documentsactive" ><span>KYC Documents</span></a></li>
										 <li id="academic_document"><a href="getacademicdetails.html"  id="Hi-B-2" ><span>Academic Documents</span></a></li>															
										 <li id="employee_document"><a href="employeeDetails.html"  id="Hi-B-4" ><span>Employment Documnets</span></a></li> <!-- onclick="myDocuments4('employeeDetails.html')" -->
										 <li id="financial_document"><a href="search_financialDoc.html"  id="Hi-B-3" ><span>Financial Documents</span></a></li>
									 </ul>
								   </div>    		
							</div>
					
					        <div class="message">
							<h4><i>Contact us if document type not found in the above list.</i></h4>
							</div>

					 </div>
					
					<div class="rightside">
					
					   
						
					   <!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 								
					  	  %>

								<div id="ProfileHeadingDiv" style="display: block;">
			
									<marquee behavior="scroll" scrollamount="5" direction="left">
										<h1 class="rightsideheader">
			
											<%=indAdvertise%>
											<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
										</h1>
									</marquee>
								</div>

					
							        		
					   <!-- Advertisment Div for Entire Page  -->

					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
										
						 <div id="titleBar">
							
							<h2>
								   <span><%=name%>'s KYC Documents</span>								   
						    </h2>
						    
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
					
					      <div id="center-body-div">
	  							
	  						
										  <div class="centerbodydiv">
									        
									          <c:set var="comp" value="${accordNames}"/>  
									          <c:set var="kycdatas" value="${kycdatas}"/>     
									                     
									         <% 
									         String comp = (String)pageContext.getAttribute("comp");                        
									         String component[] = comp.split(",");        
									         String kycdatas =(String)pageContext.getAttribute("kycdatas");          
									         StringBuilder strKYC= new StringBuilder();
									         String strKYCDATA="";
									         
									         if(kycdatas.contains("/"))
									         {
									        	 String kycFieldNames[] = kycdatas.split("/");
									        	 for(int d=0;d<kycFieldNames.length;d++)
									        	 {
									        		 strKYC.append(kycFieldNames[d]+"%"); 
									        	 }
									        	 strKYCDATA=strKYC.toString();									        	
									         }
									              
									         %>              
									              <span class="reselect button">
										        	          	        	          
										          <%
										String ls = (String)request.getAttribute("comp");
										String comparr[] = ls.split(",");
									
										String oldData = (String)request.getAttribute("OldComp");			
									    
									    boolean match = true;
									
									if(oldData.split(",").length==ls.split(",").length)
									{
										match = false;
									}
									
									if(match==true)
									{	
									%>
									
									<div align="center">
									<input type="hidden" value="0" id="flipedId"/>
									<div id="flip" onclick="ChangeFlipStatus()">Select Kyc Document</div>
									<div id="panel" >
									<div class="adddocpanel">
												<%	if(oldData!=null)				
												{
													String oldComp[] = oldData.split(",");			
													for (int i = 0; i < comparr.length; i++) {
									                         
														boolean status = true;
														
														for (int j = 0; j < oldComp.length; j++) {
															
															if (comparr[i].equals(oldComp[j])) {							
												%>			
												<% status = false;
													}
														}
														if(status){%>
														
													      <ul > 
													      <li>  
													      <input type="checkbox" name="click" value="<%=comparr[i]%>" class="checkbox" id="the-terms"/><span><%=comparr[i]%></span>		
													      </li>
													      </ul>
													<%	
													}
													}
												} else {
													for (int i = 0; i < comparr.length; i++) { %>
									
												   	 <ul>   
													      <li>  
													      <input type="checkbox" name="click" value="<%=comparr[i]%>" class="checkbox" id="the-terms"/><span><%=comparr[i]%></span>		
													      </li>
													 </ul>
													 
												<%}
												} %>
																		
												
									</div>
									<div id="proceedtbutton">
									            <input type="submit"  width="20" id="Proceed" name="Proceed" value="Add" onClick="chooseIds('choose_category_ID.html')"/><br>
									            </div>
									     </div>
									     </div> 
										 <%}%>         
										      <!-- End of Code for Reselect Category -->
										          </span>
										 
										 <%  String pannelWithId = (String)request.getAttribute("pannelWithId");
										     String pannelWithIdArray[] = pannelWithId.split(",");		
										    
										 %>  
										  	  	          	          	          	                                          
									      <div  id="panel-1">  	         	          	
									           <%                                          
									           for(int l=0,q=0;l<component.length;l++,q++)        	   
									        	  { 
									        	  
									        	     String kycId = "";
									        	  %>	
									        	  	
									        	  	<%for(int k=0;k<pannelWithIdArray.length;k++) {
									        	  	      
									        	  		if(pannelWithIdArray[k].contains(component[l]))
									        	  		{
									        	  			String splitWithId[] = pannelWithIdArray[k].split("-");
									        	  			kycId = splitWithId[1];
									        	  		}
									        	  	} %>
									        
									        <%if(!kycId.equals("")) {%>	  
									        
									        <h3 align="left" onclick="visibleAccordian('<%=component[l].replaceAll(" ", "")%>Accordian')"><%=component[l]%> : <b><%=kycId%></b></h3>  
									        
									        <%}else{ %>
									        
									        <h3 align="left" onclick="visibleAccordian('<%=component[l].replaceAll(" ", "")%>Accordian')"><%=component[l]%></h3> 
									        
									        <%} %>						             
									        
									      <div height="50px" id="<%=component[l].replaceAll(" ", "")%>Accordian" style="display:none;">
									      
									               <%
									               String filename ="";
									               String docname="";
									               String filenameback="";
									               String date="";
									               String des="";
									               int k = 0;
									               
									               if(strKYCDATA.contains("%"))
									               {
									            	   String kycarray [] = strKYCDATA.split("%");
									            	   for(int z=0;z<kycarray.length;z++)
									            	   {            		               
									            		   if(kycarray[z].contains(component[l]))
									            		   {           			   
									            			   String str = kycarray[z].substring(kycarray[z].lastIndexOf("$")+1, kycarray[z].length());
									            			   
									            			   System.out.println("So the front back details="+str);
									            			   
									            			   String strkycarray[] = str.split(",");
									            			   filename=strkycarray[0];filenameback=strkycarray[1];docname=strkycarray[2];date=strkycarray[3];des=strkycarray[4];
									            			   
									            			   System.out.println(filename+","+docname+","+date+","+des);
									            		   }
									            	   }
									               }
									                                   
									                 %>
									                 
									           
									      <div id="kycdocumentpage" class="kycdocumentpan">   
									          
									          <a id="editregistrationhelp" href="kyc_document_help.html" target="_blank"><p class="askhelp" tittle="Help For Basic Details"> </p></a>     
									          
									          <table border="0"  cellpadding="1" cellspacing="6" align="center">               
									                <tr>                                                                                      
									                    
									                      <%if(docname.isEmpty()){%>
									                    
									                             <td height="150" width="300" colspan="3" >
									                                
									                                		<div class="alert-box notice" style="width:250px;">
																				
																				 Click on upload to add documents.
																			</div>
									                                									                               
									                             </td>
									                             
									                      <%}else{ %>
									                      
										                          <% if(!filename.contains("_"))
										                        	  {%>
												                            <td height="150" width="300" colspan="3" >
												                             <div  class="panviewinner">
												                             												                             
												                                		<img src="resources/kyc_images/dummyUploadDocuments.png"   width="250" height="150" /></img>									                    									                              
												                                
												                                        <h1>Front View</h1>
												                                        </div>
												                             </td>
										                             
										                             <%}else{ %>
										                             
										                                 <%if(filename.contains("pdf")) {%>
									                                      
										                                        <td height="150" width="300" colspan="3" >
													                             	 <div  class="panviewinner">										                             														                             
												                                             <img src="resources/images/PNA_PDF.png"  width="250" height="150" /></img>
												                                       												                                           
												                                            <a title="Download Document" href="download.html?fileName=<%=filename%>"><button class="mydocumentdownloadpan"></button></a>
												                                       
												                                     
												                                         
												                                         <h1>Front View</h1>
												                                           </div>
													                             </td>
													                             
									                                     <%}else{%>
				                                     
																	        <!-- Front Side -->	
																	         <td height="150" width="300" colspan="3" >
												                                                               	
													                                     <div  class="panviewinner">  
																		                       
																		                         <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+filename+"&docCategory=KYC_DOCUMENT"%>" width="250" height="150" /></img>											                                                                   
																		                 
																		                    																	                 
																		                         <a title="View Document" onclick="viewKycDocumnt('<%=filename%>','<%=date%>','<%=des%>')"><button class="mydocumentviewpan"></button></a>
																		                         <a title="Download Document" href="download.html?fileName=<%=filename%>"><button class="mydocumentdownloadpan"></button></a>
																                      
																                             
																                  		    <h1>Front View</h1>
																                  </div>
												                             </td>
												                             
										                    	          <%}%>
										                             
										                             <%} %>
										                             
										                             
										                             
										                             
										                             
										                             <%if(!filenameback.contains("_"))
										                        	  {%>
										                        	           <%if(filenameback.equals("Not Uploaded")){ %> 
												                        	      
												                        	       <td height="150" width="300" colspan="3" >
														                              <div  class="panviewinner">
														                                 		
														                                		<img src="resources/images/noDocumentFound.png"  width="250" height="150" /></img>									                    									                              
														                                
														                                		<h1>Back View</h1>
														                                		</div>
														                            </td>	
												                        	      
														                       <%}else{ %>
														                       
																                        <td height="150" width="300" colspan="3" >
																                              <div  class="panviewinner">
																                                 		
																                                		<img src="resources/kyc_images/dummyUploadDocuments.png"  width="250" height="150" /></img>									                    									                              
																                                
																                                		<h1>Back View</h1>
																                                		</div>
																                         </td>
														                       <%} %>
														                       
										                             <%}else if(filenameback.equals("Not Uploaded")){%>
										                             
										                             	    <td height="150" width="300" colspan="3" >
												                              <div  class="panviewinner">
												                                 		
												                                		<img src="resources/images/noDocumentFound.png"  width="250" height="150" /></img>									                    									                              
												                                
												                                		<h1>Back View</h1>
												                                		</div>
												                            </td>	
										                        	  
										                             <%}else{ %>
										                             
													                                 <%if(filenameback.contains("pdf")) {%>
												                                      
													                                        <td height="150" width="300" colspan="3" >
																                              <div  class="panviewinner">
																                                 
															                                        <img src="resources/images/PNA_PDF.png"   width="250" height="150" /></img>
															                                    
															                                              <a title="Download Document" href="download.html?fileName=<%=filenameback%>"><button class="mydocumentdownloadpan"></button></a>
															                                     
															                                  		<h1>Back View</h1>
															                                  		</div>
															                                   
																                             </td>
																                             
												                                     <%}else{%>
							                                     
																				        <!-- Back Side -->	
																				         <td height="150" width="300" colspan="3" >
															                             				<!-- <div class="kycdochover"></div> -->
															                             		 <div  class="panviewinner">										                                                                   													                                        								                    
																					                       
																					                         <%-- 
																					                              <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+filenameback+"&docCategory=KYC_DOCUMENT"%>" width="250" height="150" /></img>											                                                                   
																					                         --%>
																					                        <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+filenameback+"&docCategory=KYC_DOCUMENT"%>" width="250" height="150" /></img>											                                                                   
																					                 
																					                   
																						                <!--    <div class="kycdoc_button" > </div> -->
																							                    <a  title="View Document" onclick="viewKycDocumnt('<%=filenameback%>','<%=date%>','<%=des%>')"><button  class="mydocumentviewpan"></button></a>
																							                    <a title="Download Document" href="download.html?fileName=<%=filenameback%>"><button  class="mydocumentdownloadpan"></button></a>
																					                      
																				                       
																			                               <h1>Back View</h1>
																			                               </div>
																			                  
															                             </td>
															                             
													                    	          <%}%>
										                             
										                             <%} %>
									                               
									                      <%} %>
									                      
									                      
									                      
									                      
									    <td height="150" width="300" colspan="3" >
									                
									                             <%				                   
												                    if(date.equals("Captured in Registration")){ %>
												                    												                   
												                        <ul class="panviewinner_button">                                         
												                        <li>
												                        <input class="editinner_button" type="button" value="Change" style="margin-right: 57px;width: 90px !important;" onclick ="toggle('<%=component[l]%>')" />
												                        
												                         <%-- <div id="uploadButton<%=component[l]%>" style="display:block;"><a href="#" onclick ="toggle('<%=component[l]%>')"><button class="documentchange"></button></a></div>
												                        --%>
												                        </li>                     
												                        </ul>        
												                   
												                   <%} else if(date.equals("")){ %>
												                   
												                        <ul>                                         
												                        <li>
												                        <input class="editinner_button" type="button" style="margin-right: 57px;width: 90px !important;" value="Upload"  onclick ="toggle('<%=component[l]%>')" />
												                        
												                         <%-- <div id="uploadButton<%=component[l]%>" style="display:block;"><a href="#" onclick ="toggle('<%=component[l]%>')"><button class="documentchange"></button></a></div>
												                         --%>
												                        </li>                     
												                        </ul>
												                   
												                    <%}else{ %>
												                    
												                    
												                          <ul class="panviewinner_button">                                        
												                        <li>
												                        <input class="editinner_button" type="button" style="margin-right: 57px;width: 90px !important;" value="Change"  onclick ="toggle('<%=component[l]%>')" />
												                         
												                         <%-- <span id="uploadButton<%=component[l]%>" style=" margin-right: 63px;"><a  onclick ="toggle('<%=component[l]%>')"><button class="documentchange"></button></a></span>
												                          --%>
												                         </li>                                                 
												                         <li>
												                         <input class="historyinner_button" type="button" style="margin-right: 57px;width: 90px !important;" value="History"  onclick ="kycHistory('<%=component[l]%>')" />
												                          <%-- <span id="historyButtonId" style=" margin-right: 63px;"><a onclick ="kycHistory('<%=component[l]%>')"><button class="documenthistory"></button></a></span> 
												                         --%>
												                         </li>
												                       </ul>      
												                    
												                    
												              <%} %>   
											                       
								                    <div class="PANCARDAccordian">                                             
                                              <div id="kycFormPageLight<%=component[l]%>" class="kycFormPageLightcss"><!-- popup -->
                                              
                                                     <div id="kycFormDisplay<%=component[l]%>" class="kycFormDisplaycss"> 
                                              
                                                         <h3>KYC document form</h3>
                                              		<div id="table_pkg<%=component[l]%>" style="display:none">
									                               						                               
									                                
									                                
									                               
																	
																	<% String kycFormName = component[l].replaceAll(" ","");%>
																	
																	 <p style="color:red;text-align:right;margin-bottom: 5px;">All fields are mandatory </p>	
														
																	<form  id="KycForm<%=kycFormName%>" style="float: left;" name="KycForm<%=kycFormName%>" enctype="multipart/form-data" >
																		
																		<table border="0" width="100%" height="50" cellspacing="0"  cellpadding="0" class="kycdocumentpanbutton">
																			
																			<tr>
																			   <td>
																			         <div id="Id1<%=kycFormName%>" class="error" style="display:none;margin-left: 127px;margin-top: -24px;"></div>
																			         <div id="Id2<%=kycFormName%>" class="error" style="display:none;margin-left: 127px;margin-top: -24px;"></div>
																			         <div id="Id3<%=kycFormName%>" class="error" style="display:none;margin-left: 127px;margin-top: -24px;"></div>
																			         <div id="Id4<%=kycFormName%>" class="error" style="display:none;margin-left: 127px;margin-top: -24px;"></div>
																			   </td>
																		   </tr>
																		   
																			<tr>
																			<td>
																			<input  autofocus="autofocus" type="text " id="name<%=component[l]%>" maxlength="20" placeholder="<%=component[l]%> ID" value="<%=docname%>" name="name" width="80" onkeyup="validateonEach('<%=component[l]%>','<%=component[l]%>','<%=kycFormName%>')"/>
																			
																			</td>
																			</tr>
																		
																			<tr>
																			<td>
																			<textarea id="des<%=component[l]%>" placeholder="Description in 250 characters" maxlength="250"  name="des" width="80" onkeyup="validateonEach('<%=component[l]%>','<%=component[l]%>','<%=kycFormName%>')"></textarea>
																			
																			</td>
																			</tr>
																		
																			<tr>																		
																			<td>
																			<div id="browseimagebutton">
																			<h1><u>Front Side </u></h1>
																			<div class="btn btn-primary btn-file f-left">
																			 <i class="fa-folder-open"></i>
																			 
																			 <p id="frontId"  class="frontimgshow" >Browse</p> 
																			<!--   <p class="frontimgshow"> Browse</p>  -->
																				
																			<input class="btn button-submit" width="150" type="file" id="file<%=component[l]%>" name="fileUpload"  autofocus="autofocus" onchange="validateonEach('<%=component[l]%>','<%=component[l]%>','<%=kycFormName%>')"/>
																			
																			<input  width="150px" height="2px" type="hidden" id="comp" name="comp" value="<%=component[l]%>"/>
																			
																			</div>
																			
																			
																			
																			<h1><u>Back Side </u></h1>
																			
																			<div class="btn btn-primary btn-file f-left">
																			<i class="fa-folder-open"></i>
																			 
																			 <p id="backId" class="frontimgshow1">Browse</p>  
																			   
																			<input  width="150"  type="file" id="fileback<%=component[l]%>" name="fileUploadback" onchange="validateonEach('<%=component[l]%>','<%=component[l]%>','<%=kycFormName%>')"/>
																			
																			</div>
																			</div>
																			</td>
																			</tr>
																		
																			<tr>																		
																			<td>
																			
																			
																			</td>
																			</tr>
																		
																			<tr>
																			<td>
																		    <a   onclick ="cancelToggle('<%=component[l]%>')"><input class="cancelinner_button" type="button" value="Cancel"/></a>
																		    <input class="saveinner_button"  type="button" value="Save" onClick="valiDateKyc('<%=component[l]%>','<%=component[l]%>','<%=kycFormName%>')"/> <!-- uploadKycDocs('<%-- <%=component[l]%> --%>') -->
																			</td>
																			</tr>
																		
																	    </table>
																	
																	</form>	
																	  																						
															 </div>
															 
														</div>		   
											   </div>
											
											<div id="kycFormPageFade<%=component[l]%>" class="kycFormPageFadecss"></div> 	
											  </div>								
															            
									</td>															
													
														                                              
												                
											                                
									               </tr>               
									            
									            </table>
									        
									          </div>
									      
									       </div>
									      
									    <% } %>
									    
									  </div>
									 
								  </div>
                             
                             </div>
					
								
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	



    
</div> <!--end of right div  -->

	</div>

</div>
	
<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>					
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>
	
	
<!-- Waiting Div -->
	
	<div id="waitlight">
			
		      
		              <div class="kycloader1">
								
								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
									
                      </div>
                    
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->
	
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	
	
	
	
	
	<!-- kycDoc Pop up Div -->
	
	<div id="kycDocPopUplight">
		
		 <div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		         
		       	
			<div id="kycDocPopUpDisplay" style=" border: 2px solid #03b2ee;
			    border-radius: 5px;
			    color: #555;
			    margin: 0 auto;
			    padding:0;
			    text-align: left;
			    width: auto;"> 
					              
					       
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF kycDoc Pop up Div -->	

	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->
	
	
	<!-- Waiting Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 
	
	
   <!-- EOF Waiting Div -->
	
	
	<!-- History POPUP -->
	
	<div id="kycHistoryLight" class="kycHistoryLightcss">
			
			<div class="colsebutton" onclick="fadeOutHistoryDiv()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		 
		    <div id="kycHistoryDisplay" class="kycHistoryDisplaycss"> 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="kycHistoryFade" class="kycHistoryFadecss" onclick="fadeOutHistoryDiv()"></div> 	
	
<!-- EOF  History POPUP -->
	

</body>
</html>
     	