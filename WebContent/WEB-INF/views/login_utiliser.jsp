
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Utilizer</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>
<link href="resources/css/kycstyle.css" rel="stylesheet" type="text/css" />
<link href="resources/css/blue.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<script src="resources/onlineresources/code.jquery.com.jquery-1.9.1.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.10.2.jquery-ui.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{
    background: none repeat scroll 0 0 #fff;
    border: 2px solid #05b7f5;
    border-radius: 5px;
    color: #555;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 0px;
    position: fixed;
    text-align: justify;
    top: 25%;
    width: 800px;
    z-index: 1002;
}


#successfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#successlight{
   background: none repeat scroll 0 0 #fff;
    border: 2px solid #00b6f5;
    display: none;
    height: 46px;
    left: 50%;
    margin-left: -272px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 564px;
    z-index: 1002;
}


</style>

<script>

function successlightbox_close(action){
	
	if(action=='warning')
		{
			document.getElementById('successlight').style.display='none';
		    document.getElementById('successfade').style.display='none';		  
		}
	else
		{
			document.getElementById('successlight').style.display='none';
		    document.getElementById('successfade').style.display='none';
		    /* x=document.getElementById("saveButtonId")
		    x.disabled = !x.disabled; */
		    window.location = "utiliser.html";
		}       
}


function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{

	$("#waitingdivId").show();
	$("#waitingdivContentId").show();
	
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  

	    	 $("#waitingdivId").hide();
	    		$("#waitingdivContentId").hide();
	    		
	 		 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}



function hideAllErrorDiv(tabName)
{		
	$("#loginerrordiv").hide();
	$("#regErrorId").hide();
	$("#loginsuccessdiv").hide();
	
	if(tabName=='signIn')
		{
		   $("#tabs1-js").show();
		   $("#tabs1-html").hide();
		   $("#tabs1-css").hide();
		}
	else if(tabName=='signUp')
		{
		   $("#tabs1-js").hide();
		   $("#tabs1-html").show();
		   $("#tabs1-css").hide();
		}
	else 
		{
		   $("#tabs1-js").hide();
		   $("#tabs1-html").hide();
		   $("#tabs1-css").show();
		}
}

function gotoHomePage()
{
	window.location = "clenseeHome.html";
}



</script>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 


<script src="resources/kyctabs/jquery.easytabs.min.js"></script>
<script src="resources/kyctabs/jquery.hashchange.min.js"></script>
<link href="resources/kyctabs/kyc_tabpsstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/UtiCss/doctable.css"/>
<link rel="stylesheet" href="resources/css/style_common.css" />

<style>

#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

</style>


<script type="text/javascript">

var flag='true';

function searchhomepage(){
	
	var id1=$("#searchvaluetext").val();
	 
	$("#waitingdivId").show();
	$("#waitingdivContentId").show();
	
	 
	if(id1.length>=3)
	{
		
		//$('#hidecenterbody').hide();  //not there
		$(".topdivespan").show();
		$('#searchhomepage').show();
	
		$.ajax({ 
			
		     type : "Post",   
		     url : "search_Outerprofile.html", 	  
		     data : "kycid="+id1,	     	     	     
		     success : function(response) 
		     {   					
		    	 $("#waitingdivId").hide();
		         $("#waitingdivContentId").hide();
		    		
		    		
				 $('#searchhomepage').html(response);						 	    	
		     },  		    
		  }); 	
		
	}	
	else if(id1.length<3)
    {
		$('#searchhomepage').html("");
		$('#searchhomepage').hide();	    
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		
		$(".topdivespan").hide();
    }
	else
    {
		$('#searchhomepage').html("");
		$('#searchhomepage').hide();	   
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
			 	
		$(".topdivespan").hide();
    }
	 
}

function validateOnEach()
{
	var name = /^[a-zA-Z ]*$/;
	var phno = /^[0-9]{10}$/;	
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{1,30}$/;
	
	var fname = document.getElementById("fname").value;
	var mname = document.getElementById("mname").value;
	var lname = document.getElementById("lname").value;
	var mobileno = document.getElementById("phno").value;
	var Id = document.getElementById("kid").value;
	var email = document.getElementById("email").value;
	var pw = document.getElementById("pw").value;
	var repw =document.getElementById("repw").value;
	
	if(pw!="" & repw!="")
	{
	    if(pw!=repw)
	    {	  
	       $("#repw").show();	
	       $("#repw").html("Password and confirm-password are not matched !");			       
	    }
	}
				
	if(Id!="")
	{
		if(!Id.match(poi))  
        {  	
			$("#kidDiv").show();
	        $("#kidDiv").html("Type a proper KYCID");		        
        } 
		else
			{
			$("#kidDiv").hide();
			}
	}
	
	if(Id=="")
	{
		$("#kidDiv").hide();
	}
		
	if(lname!="")
	{
		if(!lname.match(name))  
        {  	
			$("#lnameDiv").show();
	        $("#lnameDiv").html("Type a proper name");		        
        } 
		else
		{
			$("#lnameDiv").hide();
		}
	}
	
	if(lname=="")
	{
		$("#lnameDiv").hide();
	}
	
	if(mname!="")
	{
		if(!mname.match(name))  
        {  	
			$("#mnameDiv").show();
	        $("#mnameDiv").html("Type a proper name");		        
        } 
		else
		{
			$("#mnameDiv").hide();
		}
	}
	
	if(mname!="")
	{
		$("#mnameDiv").hide();
	}
	
	if(fname!="")
	{
		if(!fname.match(name))  
        {  	
			$("#fnameDiv").show();
	        $("#fnameDiv").html("Type a proper name");		        
        } 
		else
			{
			$("#fnameDiv").hide();
			}
	}
	
	if(fname=="")
	{
		$("#fnameDiv").hide();
	}	
}




function forgetPassword()
{	
   
   var emailId = document.getElementById("forgetPasswordId").value;	
   var emailIdRegex = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
 
   $("#waitingdivId").show();
   $("#waitingdivContentId").show();
	
  
  if(emailId!="")
	  {
	  
	  if(!emailId.match(emailIdRegex))  
	    {  		
		    $("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
		    
			$("#regErrorId").show();
	        $("#regErrorId").html("To get Support ,please enter a valid Email Id !");	
	        return false;
	    } 
	 else
		   {
		         
		 $.ajax({
		       url: "checkemailIdforForgetPassword.html",
		       type: 'post',
		       data: "emailId="+emailId,        
		       success: function (data) { 
		       	if(data=='Yes')
		       		{	  
				       		var formData = new FormData($("#forgetPasswordForm")[0]);
						     
						     $.ajax({
									
							        url: "formatpasswordForUtilizer.html",
							        type: 'POST',
							        data: formData,							           
							        success: function (data) { 
							        
							        if(data=="success")
							        {
							        	$("#waitingdivId").hide();
										$("#waitingdivContentId").hide();
																			    
									    document.getElementById('successlight').style.display='block';
							            document.getElementById('successfade').style.display='block';
							            $("#successdisplaySharedDocDiv").html("Password reset link has been sent to your registered email id."+"<input type=\"button\" value=\"OK\" style=\"margin-top: 9px;\" class=\"button_style\" onclick=\"successlightbox_close('success')\" />");	
							            
							            document.getElementById("forgetPasswordId").value = "";
							        }
							        else
							        {
							        	$("#waitingdivId").hide();
										$("#waitingdivContentId").hide();
																			    
							        	alert("Server Failed try again !");		    		
							        }
							        			        	
							    },	        		        
							        cache: false,
							        contentType: false,
							        processData: false
							    });
		       		}
		       	else
		       		{		       		     
		       			$("#waitingdivId").hide();
		       	   		$("#waitingdivContentId").hide();		       		
				    
		       			$("#regErrorId").show();
		        		$("#regErrorId").html(data);			       		
		       		}
		       },       
		    });
		       		
		        
		   }	    
	  }
  else
	  {
	        $("#waitingdivId").hide();
	   		$("#waitingdivContentId").hide();
		      
	        alert("Please Enter Your Registered EmailId !");
	  }
  
}

$(document).ready( function() {
    $('#tab-container').easytabs();
  });
  
function  sendUtilizerInfo()
{
	$("#waitingdivId").show();
	$("#waitingdivContentId").show();
	
	 
	var flag='true';	
	
	var fname = document.getElementById("fname").value;	
	var oname = document.getElementById("oname").value;
	var locname = document.getElementById("locname").value;
	var email = document.getElementById("uemail").value;	
	var mobileno = document.getElementById("phno").value;
	
			
	var name = /^[a-zA-Z ]+$/;
	var phno = /^[0-9]{10}$/;	
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
			
	if(fname=="" || oname=="" || mobileno=="" || locname=="" || email=="")
	{
		flag='false';
		
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
		
		 
    	alert("Every field is required !");
    	return false;
	}
	
	if(fname!="")
	{
		if(!fname.match(name))  
        {  	
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			 
			flag='false';
			$("#fnameDiv").show();
	        $("#fnameDiv").html("Enter Alfabet for Name");	
	        return false;
        } 
		else
		{
			$("#fnameDiv").hide();
		}
	}
	
	if(locname!="")
	{
		if(!locname.match(name))  
        {  	
			$("#waitingdivId").hide();
			$("#waitingdivContentId").hide();
			
			 
			flag='false';
			$("#locnameDiv").show();
	        $("#locnameDiv").html("Enter Alfabet for Name");	
	        return false;
        } 
		else
		{
			$("#locnameDiv").hide();
		}
	}
	
	if(email!="")
		{
		      if(!email.match(emailId))
		    	  {
		    	    $("#waitingdivId").hide();
					$("#waitingdivContentId").hide();
					
		    	  
		    	    flag='false';
		    	    $("#emailDiv").show();
	    	        $("#emailDiv").html("Please enter a proper emailId !");	
	    	        return false;
		    	  }
		      else
		    	  {
		    	    $("#emailDiv").hide();
		    	  }
		}   
	
	if(mobileno != "")
		{			
		   if(!mobileno.match(phno))  
    		{ 
			    $("#waitingdivId").hide();
				$("#waitingdivContentId").hide();
							 
			    flag='false';
				$("#phnoDiv").show();
				$("#phnoDiv").html("Enter a valid mobile number !");
				return false;
    		}
		    else
			{
				$("#phnoDiv").hide();
			}		    
		}
	
	if(flag=='true')
		{		
			var formData = new FormData($("#utilizersignUpForm")[0]);
			     
			    $.ajax({
						
					        url: "notifyToClenseeAdmin.html",
					        type: 'POST',
					        data: formData,
					        //async: false,	        
					        success: function (data) { 
					        
							        if(data=="success")
							        {
							        	$("#waitingdivId").hide();
										$("#waitingdivContentId").hide();
										
							        	
										document.getElementById('successlight').style.display='block';
							            document.getElementById('successfade').style.display='block';
							            $("#successdisplaySharedDocDiv").html("Thank you for Requesting, We will get back to you Shortly!"+"<input type=\"button\" class=\"button_style\" style=\"margin-top: 9px;\" value=\"OK\" onclick=\"successlightbox_close('success')\" />");	
							            
							        	
							    		 
							        }
							        else
							        {
							        	$("#waitingdivId").hide();
										$("#waitingdivContentId").hide();
										
							        	 alert("Server Failed try again !");		    		
							        }
					        			        	
					             },	       
					             
					        cache: false,
					        contentType: false,
					        processData: false
				    });		 
		}
}


function openLogin(id)
{
	$('#searchhomepage').html("");
	$('#searchhomepage').hide();	
	document.getElementById("searchedIndividualId").value = id;
    $('#tab-container').show();
}

function hideSearchBar()
{
	$(".topdivespan").hide();		
	$('#searchhomepage').hide();		
}

</script>



</head>
<body>

<div id="centerbody">
 <div class="container">
		<div class="top_line"></div>
		
			<header class="header" >
			<div class="header_inner">
				<div class="logo" onclick="gotoHomePage()">
				</div>
			  <div class="search-bar">
				 <div id="tfheader">
					
						 <input type="text" id="searchvaluetext" maxlength="50" autofocus="autofocus"  class="tftextinput2 button-submit" name="q" size="21" maxlength="120" title="Enter atleast 3 character for efficent search !" placeholder="Search Individual" onkeyup="searchhomepage()"/>
                         <input type='image' class="search_button-submit" alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="searchhomepage()"/>
						 
						 <div class="tfclear"></div>
				 </div>     
			 </div>
                <div class="tfclear" onclick="hideSearchBar()"></div>
					    <div class=" mainmenu" onclick="hideSearchBar()">
								          
							 <ul class="main_menu">
                             
								<li><a class="individual button-submit" href="individual.html"></a></li>
								<li style="border-right:1px solid #333;width:1px; height: 57px;   background-color: #777;"></li>
								<!-- <li><a class="corporate" href="clenseeHome.html"></a></li>
								<li style="border-right:1px solid #333;width:1px; height: 57px;   background-color: #777;"></li> -->
								<li><a class="utiliseractive button-submit" href="utiliser.html"></a></li>
							   
							 </ul>   
							 
						</div>       				
				 </div>
			</header>
			
			
			<div style="width:100%;height:100%;" class="banner_center_individual">
									
            <div class="main" onclick="hideSearchBar()">
              
                   
                    <div id="searchhomepage" class="searchhomepage" style="display: none;width:76% !important;" ></div>
			
			                      
			<div class="leftside"> 
			<%String status = (String)request.getAttribute("status");
			if(status!=null){
			%>
			
			<%} %>			
			<img src="resources/images/frontpage-kyc.png"></img>
			
			</div>
				
			 <div class="rightside" onclick="hideSearchBar()">
			
			<%String signReport = (String)request.getAttribute("status");
			if(signReport!=null){
			%>
			<div id="loginsuccessdiv" style=" border-radius: 10px;color: green;font-size: 14px;margin: 0;width: 75%;">
    
                  <%=request.getAttribute("status")%>
             
            </div>      
    
			<%} %>
			
			
			<%String authenticateStatus = (String)request.getAttribute("errormessage");
			if(authenticateStatus!=null){%>		
			<div  id="loginerrordiv" style="color:red;						
						font-family:Tahoma,Geneva,Arial,sans-serif;
						font-size: 14px;    					
   						width: 297px;"><%=authenticateStatus%></div>
			<%} %>
			
			<div id="regErrorId" style="color: red;font-family: Tahoma,Geneva,Arial,sans-serif;font-size:15px;margin: 0;width: 56%;">
    	    
			</div>	
					    	
																									
		     <div id="tab-container" class='tab-container'>
				 <ul class='etabs'>				   
				   <li class='tab'><a href="#tabs1-js" class="button-submit" onclick="hideAllErrorDiv('signIn')">Sign In</a></li>
				   <li class='tab'><a href="#tabs1-html" class="button-submit" onclick="hideAllErrorDiv('signUp')">Sign Up</a></li>
				   <li class='tab'><a href="#tabs1-css" class="button-submit" onclick="hideAllErrorDiv('resetPassword')">Reset Password</a></li>
				 </ul>
				<div class='panel-container'>
				
							
				 <div id="tabs1-js" style="display:block;">
				 
				<form action="/KYC/utiAuthenticateLogin.html" method="post" >
				  <p>
					<input class="field" style="width: 266px" name="kycid" required="required" type="text" placeholder="Registered Email Id / KYC Id"/>
				  </p>
				  <p>
					<input class="field" style="width: 266px" name="password" required="required" type="password" placeholder="Password"/>
				  </p>
				  <p>
				     <input type="hidden" id="searchedIndividualId" name="searchedIndividualName"/>						 
				  </p>
				  <p class="keeplogin button-submit">
					
					<!-- <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping"/> Keep me logged in -->
					<input type="submit" class="button-submit" value="Sign In"/>
					
				  </p>
				  <p style="margin-top:18px;text-align: left;color:#7c7c7c">
				   By Logging In, you agree to our <a href="clenseeTermConditions.html" target="_blank">Terms&Conditions</a> and that you understood our <a href="clenseePrivatePolicy.html" target="_blank">Privacy policy</a>
				  </p>
				</form>
				</div>
				
				
				
				
				<div id="tabs1-html" style="width:322px;height:360px;display:none;">
				
				     <form id="utilizersignUpForm" name="utilizersignUpForm" enctype="multipart/form-data">
					     
					    <span style="color:red;">*All fields are required !</span>					     
					    <table>
							 
							  <tbody>
							     
							       <tr>
							          <td>
										<p class="uname" for="usernamesignup">
										<input class="field" id="fname" type="text" placeholder="Full Name" name="fullname"/><div id="fnameDiv" class="error" style="display:none;margin-left:10px;margin-top:39px;"></div>
										</p>
									 </td>				
							       </tr>
									
									<tr>
										<td>
											<p class="uname" for="usernamesignup">
											<input class="field" id="oname" type="text" placeholder="Organization Name"  name="orgname" /><div id="onameDiv" class="error" style="display:none;margin-left:10px;margin-top:39px;"></div>
											</p>
										</td>														
									</tr>
									
									<tr>
										<td>
											<p class="uname" for="usernamesignup">
											<input class="field" id="locname" type="text" placeholder="Location"  name="location" /><div id="locnameDiv" class="error" style="display:none;margin-left:10px;margin-top:39px;"></div>
											</p>
										</td>														
									</tr>
							
									<tr>												
										<td>
											<p class="uname" for="usernamesignup">
											<input class="field" id="uemail" type="text" placeholder="Email" style="text-transform: unset !important;" name="emailid" /><div id="emailDiv" class="error" style="display:none;margin-left:10px;margin-top:39px;"></div>
											</p>
										</td>
									</tr>
							
									<tr>
										<td>
											<p class="uname" for="usernamesignup">   
											<input class="field " id="phno" type="text" maxlength="10" placeholder="Mobile No"  name="mobileno" /><div id="phnoDiv" class="error" style="display:none;margin-left:10px;margin-top:39px;"></div>
											</p>										
										</td>								
									</tr>
									
									<tr>
										<td>
											<p class="uname" for="usernamesignup" style=" width: 87%;">   
											<input type="button"  class="button-submit"  value="Submit" onclick="sendUtilizerInfo()"/>
											</p>										
										</td>								
									</tr>
											
							</tbody>
				      </table>
				    </form>
				    
				</div>
				
				
				
				
				<div id="tabs1-css" style="display:none;">
				  				
				<form name="forgetPasswordForm" id="forgetPasswordForm" >
				<p>
				<input class="field button-submit" type="email" id="forgetPasswordId" placeholder="Your email" required name="emailsignup" />
				</p>
				<p class="signin button ">
				<input type="button" style="margin-left:10px;" class="button-submit" value="Reset" onclick="forgetPassword()" />
				</p>
				
				<p style="margin-top: 60px;text-align: left;color:#7c7c7c">
					    We will send a link on your registered email to reset your password.
			    </p>
					    
				</form>
				
				</div>
				
				
				
			</div>

		</div>
	
					
	      </div>   
		</div>				
		</div>	
		
          <footer class="footer">
		   
		   <%
				ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
				String version=resource.getString("kycversion");
				String versionDate=resource.getString("kyclatestDate");
		    %>
		  
				
			<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p style="margin-top:25px;">Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p style="margin-top:35px;">
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactusbeforeLogin.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 905px !important; margin-top: -1px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		   
			<div class="footer_line">
			</div>

		 </footer>
           
	</div>	
</div>		
<!------center End-------->
		
 
 
 
<!------container End-------->


<!------container End-------->

   <div id="successlight" style="height:auto ! important;">
   
		    <div id="successdisplaySharedDocDiv"  class="successmassage" style="background: none repeat scroll 10px 50% yellowgreen;"> 
		  
		    </div>		
		     
		   
		       
	</div>
	
	<div id="successfade"></div> 	

<!-- Waiting Div -->

	
	<!-- Waiting Div -->
	
	
	 <div id="waitingdivContentId" style="display:none;">
				    
				     <div class="kycloader1">
		
		                   
		                   <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						   </div>
										                   
		                   
		             </div>
		             
		              				    
		    </div>
		    
			<div id="waitingdivId" >
											    
				   
			     
		    </div>
		    
	
<!-- EOF Waiting Div -->
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight">
			
			<div class="colsebutton" onclick="successlightbox_close()"><img height="22" width="22" class="footerpage_links"  src="resources/images/close_button.png" /></div>
		    
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
<!-- EOF kycDiv Div -->	

</body>
</html>
 