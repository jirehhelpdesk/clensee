<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<%@ page language="java" import="java.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Notification</title>

<link href="admin_resources/kyc_css/kyc_admin_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="admin_resources/kyc_css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">

<link rel="stylesheet" href="admin_resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script type="text/javascript"src="admin_resources/kyc_scripts/demo.js"></script>

<link rel="stylesheet" href="admin_resources/css/logout.css">	
	
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

	
<style>

#sessionfade{

    background-color: #000;
    height: 100%;
    left: 0;
    opacity: 0.7;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1001;
}

#sessionlight{

    background: #fff none repeat scroll 0 0;
    border-radius: 24px;
    height: auto;
    left: 43%;
    margin-left: -211px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: auto;
    z-index: 1002;
    right: 400px !important;

}

</style>	


<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}

.adminleftMenuActive
{
	color:#00b6f5 ! important;
}
</style>
	
	
	
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
$(document).ready(function () {
    $("#menu_wrapper").on("click", "ul.main_menu>li", function (event) {
        debugger;
        event.preventDefault();
        $("#menu_wrapper .active").removeClass("active");
        $(this).find('a:first').addClass("active");
    });
});
});//]]>  



function redirectToLogin()
{
	window.location = "logout_admin.html";	
}


</script>

<script type="text/javascript">
	
	function utiNotification()
	{
		$("#waitingdivId").show();
		
		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "adminleftMenuActive";
		document.getElementById("3stId").className = "";
		document.getElementById("4stId").className = "";
		document.getElementById("5stId").className = "";
		document.getElementById("6stId").className = "";
		
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
							type : "post",
							url : "utilizerNotify.html",				
							success : function(response) {
								
								 $("#waitingdivId").hide();
								
								 $("#viewUsers")
									.html(response);
								},	
								
						});
						
						$("#titleBar").html("<h2><span>UTILIZER NOTIFICATION</span></h2>");		
					}
				else
					{
					   $("#waitingdivId").hide();
					   
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
		
		
	}
	
	function sysNotification()
	{
		$("#waitingdivId").show();
		
		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "";
		document.getElementById("3stId").className = "adminleftMenuActive";
		document.getElementById("4stId").className = "";
		document.getElementById("5stId").className = "";
		document.getElementById("6stId").className = "";
		
		$.ajax({
			type : "post",
			url : "systemNotify.html",				
			success : function(response) {
				
				$("#waitingdivId").hide();
				
				 $("#viewUsers")
					.html(response);
				},	
				
		});
		
		$("#titleBar").html("<h2><span>SYSTEM NOTIFICATION</span></h2>");		
	}
	
	function getEmailSentReport()
	{
		
		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "";
		document.getElementById("3stId").className = "";
		document.getElementById("4stId").className = "adminleftMenuActive";
		document.getElementById("5stId").className = "";
		document.getElementById("6stId").className = "";
		
		$("#waitingdivId").show();
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
							type : "post",
							url : "getemailreport.html",				
							success : function(response) {
								
								$("#waitingdivId").hide();
								
								 $("#viewUsers")
									.html(response);
								},	
								
						});
						
						$("#titleBar").html("<h2><span>Email Sent Report</span></h2>");		
					}
				else
					{
					   $("#waitingdivId").hide();
					   
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
		
	}
	
	function getSMSSentReport()
	{
		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "";
		document.getElementById("3stId").className = "";
		document.getElementById("4stId").className = "";
		document.getElementById("5stId").className = "adminleftMenuActive";
		document.getElementById("6stId").className = "";
		
		$("#waitingdivId").show();
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
							type : "post",
							url : "getsmsreport.html",				
							success : function(response) {
								
								   $("#waitingdivId").hide();
								
								   $("#viewUsers").html(response);
									
								},	
								
						});
						
						$("#titleBar").html("<h2><span>SMS Sent Report</span></h2>");	
					}
				else
					{
					
						$("#waitingdivId").hide();
					
					   $("#sessionlight").show();
				           $("#sessionfade").show();
					}
			}
		});
					
	}
	
	
	
	function getAdvertisment()
	{
		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "";
		document.getElementById("3stId").className = "";
		document.getElementById("4stId").className = "";
		document.getElementById("5stId").className = "";
		document.getElementById("6stId").className = "adminleftMenuActive";
		
        $("#waitingdivId").show();
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
							type : "post",
							url : "orgAdertisement.html",				
							success : function(response) {
								
								$("#waitingdivId").hide();
								
								 $("#viewUsers")
									.html(response);
								},	
								
						});
						
						$("#titleBar").html("<h2><span>Add Advertisment</span></h2>");	
					}
				else
					{
					
						$("#waitingdivId").hide();
					
					   $("#sessionlight").show();
				           $("#sessionfade").show();
					}
			}
		});
	}
	
	
	
	
	
	
	
	function viewindividualsection(){ 
		
		$("#waitingdivId").show();
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
							var id=$("#inputString").val();
							var selectedvalue = $("#selectedidindividual").val();
							
							
							if(id.length>0)
							{
							
								$.ajax({  
							     type : "Post",   
							     url : "viewindividualsection.html", 	   	  
							     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	      	     	     
							     success : function(response) 
							     {   
							    	 $("#waitingdivId").hide();
							    	 
							    	 var res=response.length;	    	
							    	 var res1 = response.substring(1, res-1);	    	    	 
							    	 var viewprofile = res1.split(", ");
							    	 var length=viewprofile.length;
							    	
							    	 if(length>1)
							        { 
										var values = "<table align=\"center\" border=\"0\"><tr><th bgcolor=\"#999999\">Profile Pic</th><th bgcolor=\"#999999\">kycid</th><th bgcolor=\"#999999\">Name</th><th bgcolor=\"#999999\">Emailid</th><th bgcolor=\"#999999\">Mobile number</th><th bgcolor=\"#999999\">Plan Available</th><th bgcolor=\"#999999\">Send Notification</th></tr>";
										for ( var i = 0; i < viewprofile .length; i = i + 10) {
										
											if(viewprofile[6 + i]!="No Data")
											{
											
													values +="<tr><td>" + "<img src=\"${pageContext.request.contextPath}<%="/KYC_DOC_REPOSITORY"+"/"%>"+viewprofile[9 + i]+"/"+viewprofile[8 + i]+"/Profile_Photo/"+viewprofile[6 + i]+"\"   width=\"40\" height=\"45\" /></img>" 				
													+ "</td><td> "+ viewprofile[0 + i]+"</td><td>" + viewprofile[1 + i]+ "" + viewprofile[2 + i]
													+ "</td><td>" + viewprofile[3 + i]
													
													+ "</td><td>" + viewprofile[4 + i]
													+ "</td><td>" + viewprofile[5 + i]
													+ "</td><td>" + "<input type=\"button\" value=\"Notify\" id=\""+viewprofile[8 + i]+","+ viewprofile[1 + i]+" "+viewprofile[2 + i]+"\" onclick=\"specificIndNotify(this.id)\" />"
													+ "</td></tr>";
											}
											else if(viewprofile[6 + i]=="No Data")
											{
												
														values +="<tr><td>" + "<img src=\"resources/images/kyc_inner_cons_name.png\" width=\"40\" height=\"45\" /></img>" 				
														+ "</td><td> "+ viewprofile[0 + i]+"</td><td>" + viewprofile[1 + i]+ "" + viewprofile[2 + i]
														+ "</td><td>" + viewprofile[3 + i]
														
														+ "</td><td>" + viewprofile[4 + i]
														+ "</td><td>" + viewprofile[5 + i]
														+ "</td><td>" + "<input type=\"button\" value=\"Notify\" id=\""+viewprofile[8 + i]+","+ viewprofile[1 + i]+" "+viewprofile[2 + i]+"\" onclick=\"specificIndNotify(this.id)\" />"
														+ "</td></tr>";
																		
											}
														
										}   
										values += "</table>";
										$('#applyCodeid').html(values);
										
							     }
							 
							 
							 if(length==1)
							 {
								 $("#waitingdivId").hide();
								 
							var values = "No data found";
							$('#applyCodeid').html(values);
							 }
																										
							     },  
							     
							    });  
							}
							if(id.length==0)
							{		
								$("#waitingdivId").hide();
								var values = "No data found";
								$('#applyCodeid').html(values);
							}
					}
				else
					{
					
					   $("#waitingdivId").hide();
					
					   $("#sessionlight").show();
				           $("#sessionfade").show();
					}
			}
		});
		
		
		
	}


	function searchIndForNotify()
	{
		$("#notifydivId").hide();				
		$("#notifyspecificIndId").show();
	}

	function specificIndNotify(idValue)
	{
		var notifyDetails = idValue.split(",");
		document.getElementById("indToValue").value = notifyDetails[0];	
		document.getElementById("indToNameValue").value = notifyDetails[1];	
		
		$("#notifydivId").show();
	}

	function allIndNotify()
	{
	   $("#notifyspecificIndId").hide(); 
	   
	   document.getElementById("indToValue").value = "All Individual";
	   document.getElementById("indToNameValue").value = "All Individual";
	   
	   $("#notifydivId").show();
	   
	}

	function notifyToIndividual()
	{
		$("#waitingdivId").show();
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						var formData = new FormData($("#notifyForm")[0]);
						
					    $.ajax({
					        url: "notify/toIndividual.html",
					        type: 'POST',
					        data: formData,
					        //async: false,
					        success: function (data) {          
					            
					        	$("#waitingdivId").hide();
					        	
					        	if(data=="success")
					        		{
					        			alert("Admin Notification has sent successfully.");
					        		}
					        	else
					        		{
					        			alert("Admin Notification has failed,try again !");
					        		}
					        	
					        	
					        },
					        cache: false,
					        contentType: false,
					        processData: false
					    });	
					}
				else
					{
					$("#waitingdivId").hide();
					
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
		
	}


	
	
	
	
	
	
	function CheckSessionOfAdmin(hitTheUrl)
	{
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = hitTheUrl;
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
					
	}
	
	function gotoMainPage()
	{
		window.location = "authenticateAdminLogin.html"
	}


	
</script>

<!-- Scripts for admin Pattern -->

</head>
<body>

	<div class="container">

		<!--header block should go here-->

		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner" style="width:1120px !important">
			
			<div class="logo" onclick="gotoMainPage()">MAIN PAGE</div>


			<div class="tfclear"></div>
			<div  class=" mainmenu" >
			<div id="menu_wrapper">
				
				<ul class="main_menu" >
					
					<li></li>
										
					<li id="myMenus1">				
					<a class="myprofile" href="#" onclick="CheckSessionOfAdmin('viewindividual.html')"><span></span> My Individual
					</a>
					</li>
					
					<li id="myMenus2">				
					<a class="mydocuments" href="#" onclick="CheckSessionOfAdmin('createUtiliser.html')">	<span></span>  My Utilizer
					</a>
					</li>
					
					<li id="myMenus5"><a class="mystore" href="#" onclick="CheckSessionOfAdmin('createDomain.html')">
							Domain Hierarchy
					</a>
					</li>
					
					<li id="myMenus6" style="width:140px;"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createKycHierarchy.html')">
							Document Hierarchy
					</a>
					</li>
					
					<li id="myMenus3"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createAdminPattern.html')"> 
							My Patterns
					</a>
					</li>
					
					<li id="myMenus41">
					   <a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createfeature.html')">  My Features
					   </a>
					</li>
					
					<li id="myMenus42">
					 
					  <a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createplan.html')">  My Plans  </a>
					 
					</li> 
					
					<li id="myMenus43"><a class="accessmanager" style="color:#03b2ee" href="#" onclick="CheckSessionOfAdmin('adminNotification.html')">  NOTIFICATION
					</a>
					</li>
					
					<li id="myMenus44"><a  href="#" onclick="CheckSessionOfAdmin('admin_settings.html')">  SETTINGS
					</a>
					</li> 
										
				</ul>
				
             </div>
                                   
			</div>

          
		</div>
   
	</header>


				 <div style="text-align: right; float: right; margin-top: -50px; width: 4%; margin-left: 10px;"> 
	               <a href="logout_admin.html" ><h4 style="color:#05b7f5;">Logout</h4></a>
	             </div>

		<div class="main" ><!-- background-color: #585759; -->
			<div class="main_center">
				<div class="leftside">

					<!--left menus should go here-->
					<div class="user_display">


					 <form>
							<div id="textBox">
							
							        <div id="button">
							        
							            <ul>
							                <li id="Hi-B-1"><a href="#" id="1stId" class="adminleftMenuActive" onClick="CheckSessionOfAdmin('adminNotification.html');"><span>INDIVIDUAL NOTIFICATION</span></a></li>
											<li id="Hi-B-1"><a href="#" id="2stId" onClick="utiNotification();"><span>UTILIZER NOTIFICATION</span></a></li>
										    <li id="Hi-B-1"><a href="#" id="3stId" onClick="sysNotification();"><span>SYSTEM NOTIFICATION</span></a></li>
										    <li id="Hi-B-1"><a href="#" id="4stId" onClick="getEmailSentReport()"><span>FAILED EMAILS STATUS</span></a></li>
										    <li id="Hi-B-1"><a href="#" id="5stId" onClick="getSMSSentReport()"><span>FAILED SMS STATUS</span></a></li>
										    
										    <li id="Hi-B-1"><a href="#" id="6stId" onClick="getAdvertisment()"><span>ADD ADVERTISEMENT</span></a></li>
										    
									    </ul>
									    
									</div>
								
							</div>
						</form>

					</div>




				</div>


				<div class="rightside" >

					<!--center body should go here-->
					<div id="wishAdmin">
						<h1>Welcome To KYC Admin</h1>
					</div>
					
					<div class="border_line"></div>
					<div class=" username_bar">
						 <div id="titleBar">
							<h2>
								<span></span>
								Individual Notification
							</h2>
						</div>
					</div>
				
					<div class="module-body" >
					
					<div id="viewUsers" style="height: auto !important;margin-bottom: 15px"> 
    
						 						 
						 <!-- AdminBody startts Here -->
						      
						      
						                    <div id="notificationpage">
						       
											           <input type="button" value="Notify Specific Individual" onclick="searchIndForNotify()"/>
											           <input type="button" value="Notify All Individual" onclick="allIndNotify()"/>
											      </div>
											
												  <div id="notifyspecificIndId" >
														<table>
															<tr>
																
																<td>
																	<div id="searchicon">
																		<div id="profilesearch">
																			<input type="text" id="inputString" class="profilesearchtextinput2" name="q" size="20" maxlength="80" value="" /> 
																			
																			<input type='image' alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="viewindividualsection()" />
																		</div>
																	</div>
																</td>
																
																<td><select id="selectedidindividual">
																		<option value="name">Name</option>
																		<option value="plan">Plan</option>
																		<option value="kycid">Kycid</option>
																</select></td>
												
															</tr>
														</table>
														
														
														<div id="applyCodeid" class="datagrid" style="margin-left:-45px;px;margin-top:16px;margin-right:40px;">
													
											            </div>
												 </div>
											    
											     <div id="notifydivId" style="display:none;margin-left:74px;margin-top:32px;width:725px;background-color: #fff;border: 2px solid #00b6f5;border-radius: 14px;">
												       
												       <form name="notifyForm" id="notifyForm" >
												       
														      <ul style="text-align:left;width:665px;">
														      
																       <li>
																              <input type="hidden"  value="Individual"   name="notification_type" />
																              <input type="hidden" id="indToValue" value="" name="notification_to" readonly/>
																           To:<input type="text" id="indToNameValue" value=""  readonly/>
																       </li>
																       
																       <li>    
																           Notification Subject:
																           <input type="text" name="notification_subject" maxlength="90" style="width:652px;height:20px;"/>
																       </li>
																       
																       <li>    			           
																           Notification Message:
																           <textarea name="notification_message" style="width:652px;height:60px;">
																           
																           </textarea>
														               </li>	
														               
														               <li>    			           
																            <input type="button" value="Notify" style="align:right;" onclick="notifyToIndividual()"/>
														               </li>               
												              </ul> 
												            
												       </form>
												       	        
												 </div>

						 <!-- Admin Body Ends Here -->
						 
						</div>

					</div>
					

				</div>
				
			</div>
		</div>
</div>
		
		<!--footer block should go here-->
		
		   <%
			  ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			  String version=resource.getString("kycversion");
			  String versionDate=resource.getString("kyclatestDate");
		    %>
		
		
          <footer class="footer">
		   
		  
				<div class="footer_copyrights">
                <div class="copyrights">    
					 <ul >
                     		
							<li class="footer_space01"><p>Design by:<span> <a href="http://jirehsol.com/">Jireh </a></span></p>
                            <p> � All rights Reserved CLENSEE 2014-15</p>
                             <span>
									<a href="#">Version:<%=version%></a>
									</span>
                            </li>
							<li class="footer_space02"> <p><a href="#">About Us</a> |<a href="#"> Terms and Conditions</a> |<a href="#"> Privacy Policy</a> | <a href="#">Contact Us</a></p></li>
							<li class="footer_space03"> 
                            
                           <div class="four columns">
                                <ul class="social_media">
                                    <li>
                                     <a class="fb" href="https://www.facebook.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="google_plus" href="https://plus.google.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="http://www.linkedin.com/company/1872121?trk=tyah" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li>
                                   
                                    
                                </ul>
                              </div>
                            
                            </li>
						   
						</ul>     
					</div>
                
				</div>
		   
			<div class="footer_line">
			</div>

		 </footer>



<!-- Waiting Div -->
	
	<div id="waitingdivId" >
									    
		    <div id="waitingdivContentId">
				    
				     <div class="kycloader">
		
		             </div>
		             <h4>Please wait For the Portal response...   </h4>
		              				    
		    </div>
	     
    </div>
	
<!-- EOF Waiting Div -->




<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#05b7f5;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		        <h3>Your session has expired,Please click on the button to relogin !</h3>      
		         
		         <input type="button" value="Login" onclick="redirectToLogin()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	
	
	<!------container End-------->
</body>
</html>
