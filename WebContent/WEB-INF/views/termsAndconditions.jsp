 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="${pageContext.session.maxInactiveInterval};url=outduesession.html">

<title>Clensee Terms & Conditions</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/onlineresources/code.jquery.com.ui.1.10.2.jquery-ui.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}




.termsheading
{
    color: #00b5f6;
    font-size: 16px;
    margin-left: 15px !important;
    text-align: left;
}

.termsContent
{
    font-size: 14px;
    padding-left: 15px;
    padding-right: 15px;
    text-align: justify;
    color: #7c7c7c;
}
</style>



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}
/* 
#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
} */
</style>

<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>


<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$("#center-body-div").hide();
		$('#suggestions1').hide();
		$("#ProfileHeadingDiv").hide();
		$("#leftsideBasicInfoDiv").hide();
	    $('#searchhomepage').show();
		
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
        $("#waitdisplaySharedDocDiv").html("<img src=\"resources/ajax_loaderimage/registerloading.gif\" style=\"margin-left:121px;height:50px;width:50px;\"></img><h4>Processing please wait for portal response...... </h4>");
        
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		 
						$("#center-body-div").hide();
						$("#ProfileHeadingDiv").hide();
						$("#leftsideBasicInfoDiv").hide();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $("#ProfileHeadingDiv").hide();
			    		 $("#center-body-div").hide();
			    		 $("#leftsideBasicInfoDiv").hide();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		 
		    document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
					    
			$('#searchhomepage').hide();
			$('#searchhomepage').html("");
			 
			$('#suggestions1').show();
			$("#ProfileHeadingDiv").show();
			$("#center-body-div").show();
			$("#leftsideBasicInfoDiv").show();
			
	    }
}


function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}


function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}
</script>
      
<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>

</head>

<body>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="clenseeHome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			
		</div>
 
 <%
 
     //Required file Config for entire Controller 
	
	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 String kycMainDirectory = fileResource.getString("fileDirectory");	
	 File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller 
 
 %>
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
					<div id="helpHeading" style="display: inline-table;margin-left: 5px ! important;margin-top: 18px ! important;">
					
					<h1>Terms & Conditions</h1>
		    
					
							<p class="termsContent">By accessing this website, you agree to be bound by these website Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trade mark laws.</p>
							
							<h3 class="termsheading">Access and Interference or Wrongful Conduct</h3>
							
							<p class="termsContent">User shall not commit or permit wrongful or damaging acts which justify civil action including, but not limited to, posting of defamatory, scandalous, or private information about a person without their consent or intentionally inflicting emotional distress. Violations or attempts to violate&nbsp;<strong>CLENSEE</strong>&nbsp;systems or to interrupt&nbsp;<strong>CLENSEE</strong>&nbsp;services are strictly prohibited, and may result in criminal and civil liability.</p>
							
							<h3 class="termsheading">Passwords</h3>
							
							<p class="termsContent">We are not at all responsible for Misuse of Password and safeguarding the password that you use to access the Services and for any activities or actions under your password. We encourage you to use "strong" passwords (passwords that use a combination of upper and lower case letters, numbers and symbols) with your account. CLENSEE cannot and will not be liable for any loss or damage arising from your failure to comply with the above.</p>
							
							<h3 class="termsheading">Your Rights</h3>
							
							<p class="termsContent">You retain your rights to any Content/Documents that you share, post, and store on or through the Services. By submitting, posting or sharing Content/Documents on or through the Services, you grant us across India, non-exclusive, royalty-free license (with the right to sublicense) to use, copy, reproduce, process, adapt, modify, publish, transmit, display and distribute such Content in any and all media or distribution methods (now known or later developed).</p>
							
							<h3 class="termsheading">Copyright Policy</h3>
							
							<p class="termsContent">We want to ensure that the content that we host can be re-used by other users without fear of liability and that it is not infringing the proprietary rights of others. In fairness to our users, as well as to other creators and copyright holders, our policy is to respond to notices of alleged infringement that comply with the formalities of the Digital Millennium Copyright Act (DMCA). Pursuant to the DMCA, we will terminate, in appropriate circumstances, users and account holders of our system and network who are repeat infringers.</p>
							
							<h3 class="termsheading">Geographical Limit</h3>
							
							<p class="termsContent">Till date our website is limited to INDIA only.</p>
							
							<h3 class="termsheading">Wavier</h3>
							
							<p class="termsContent">The failure of CLENSEE to enforce any right or provision of these Terms will not be deemed a waiver of such right or provision. In the event that any provision of these Terms is held to be invalid or unenforceable, then that provision will be limited or eliminated to the minimum extent necessary, and the remaining provisions of these Terms will remain in full force and effect.</p>
							
							<h3 class="termsheading">Limitation of liability</h3>
							
							<p class="termsContent">We will not be liable to you or to any other party for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data, or other intangible losses, regardless of whether we were advised of the possibility of such damage.</p>
							
					</div>
			
         		
								<div class="leftside">
								
								
								</div>
					
					
                    
                                                                      
	            </div>

     </div>
	
<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>					
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactusbeforeLogin.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>
<!-- Waiting Div -->
	
	<div id="waitlight">
			
		    <div id="waitdisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:300px;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight" style="display:none;">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	<!-- kycDoc Pop up Div -->
	
	<div id="kycDocPopUplight" style="display:none;">
			
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" style="display:none;" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF kycDoc Pop up Div -->	

	
	
	
	
</body>
</html>
     	