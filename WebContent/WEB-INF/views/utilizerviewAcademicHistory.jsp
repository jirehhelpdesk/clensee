<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">



<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>


</head>
<body>
					
								<%
								
								  String ind_id = request.getAttribute("user").toString(); 
								  String year = request.getAttribute("year").toString(); 
								  String docs_data = (String)request.getAttribute("docsData");
								  String docName = (String)request.getAttribute("docName");
								  String doctype[] = docs_data.split("-");
								  String docs_dataArray[] = doctype[1].split(",");
								  
								%>
								       
								       <div id="panel-2" style="width:100%;">
											   			  			   
											   			  			   
											         <div id="kycdocumentpage" class="kycdocumentpage">  	<!-- start of kycdocumentpage -->														
											                         
											                         <div>
														           	      <button class="backbuttonindex_histy"  value="Back" onclick="HistoryDocUti('<%=(String)session.getAttribute("urlValueToGoHistory")%>')" >Back</button> 
														           	 </div>  
											   
												                  <div class="kycdocumentdetails-ext">      <!-- Division for Left side Hierarchy other component with data is in left side -->
												                         
												                         <table>
													                       
													                       <% for(int i=0;i<docs_dataArray.length;i++) {%>
													                         
													                         <tr style="margin-top:15px;">
													                              <td style="width:auto;"><b><%=docs_dataArray[i].split(":")[0]%></b></td>
													                              <td style="width:10px;">:</td>
													                              <td style="width:auto;"><%=docs_dataArray[i].split(":")[1]%></td>			                              
													                         </tr>
													                         
													                       <%} %>
													                       
													                      </table>  					                       					                          
													              </div>      <!-- End of Division for Left side Hierarchy other component with data is in left side -->
													
												                     <div class="academicdocumenttitle">
												                     
																		<h1>
																			<span></span>
																		 Academic Document </h1>
																		</div>    	
													    
																  <div class="utiacademicdetailsimg">     <!-- Division for right side Hierarchy file component with data  -->
																	             
															              <ul>
															               
															               <%if(!docName.equals("No Data")) {
															               
															            	    String docNameArray[] = docName.split(",");							            	    
															            	    for(int dn=0;dn<docNameArray.length;dn++)
															            	    {
															                    %>
															                   <li>
															                    
															                    <%if(docNameArray[dn].contains("pdf")) {%>
															                    
															                  		   <img src="resources/images/PNA_PDF.png"   width="150" height="100" /></img>
																														              
											                                           <a  class="mydocumentdownloadpan" href="downloadDocfromUtilizer.html?fileName=<%=docNameArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ></a>
																	                    
											                                    <%} else {%>
											                                         	  
											                                         	<img src="${pageContext.request.contextPath}<%="/previewThumbNailDocsFromUtilizer.html?fileName="+docNameArray[dn]+"&docCategory=ACADEMIC_DOC"+"&year="+request.getAttribute("year")+"&extp="+session.getAttribute("tempIdasdgthfmUti")%>"   width="150" height="100" /></img>
																																				                                                         
										                                                <a onclick="viewKycDocumnt('<%=docNameArray[dn]%>')"><button class="mydocumentviewpan"></button></a>
											                                            <a href="downloadDocfromUtilizer.html?fileName=<%=docNameArray[dn]%>&extp=<%=session.getAttribute("tempIdasdgthfmUti")%>" ><button class="mydocumentdownloadpan"></button></a>
											                                            
											                                        <%} %>                                        
															                    </li>
															                    <%} %>  
															                <%} else {%> 
															                
															                          <h1> Academic Documents are not uploaded yet !</h1>
															                <%} %>    
															              </ul>
																	   
																	</div>                               <!-- Division for right side Hierarchy file component with data -->
														           	   
														           	   		
														           	    				           	    						           	           	      				                          
											           </div>      <!-- End of kycdocumentpage -->		
											                				 				   					  
									        
									                            
									           
										</div>   <!-- End of panel-1 division  -->
											
								       
								                                       
</body>
</html>