<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<script type="text/javascript" src="resources/js/checkbox_jquery.js"></script>


<script src="resources/onlineresources/ajax.googleapis.com.ajax.lib.jqury.1.11.0-jquery.min.js"></script>

<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>
<script src="resources/kyc_scripts/jquery-ui-10.js"></script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>


<script type="text/javascript">
$(document).ready(function(){
	$(".CheckBoxClass").change(function(){
		if($(this).is(":checked")){
			$(this).next("label").addClass("LabelSelected");
		}else{
			$(this).next("label").removeClass("LabelSelected");
		}
	});
	$(".RadioClass").change(function(){
		if($(this).is(":checked")){
			$(".RadioSelected:not(:checked)").removeClass("RadioSelected");
			$(this).next("label").addClass("RadioSelected");
		}
	});	
});
</script>

<script>

$(document).ready(function(){
  $("#flip").click(function(){
    $("#panel").slideToggle("slow");
  });
});


function chooseIds(handlerToHit) {
			
	var click = [];
	$(".CheckBoxClass:checked").each(function() {
        click.push($(this).val());
    });
	 	
	if(click.length==0)
		{ 		    
            alert("Select at least one checkbox");		
		}
	else
		{
		
	 $.ajax({
			type : "post",				
			url : handlerToHit,				
			data :"click="+click,	  
			success : function(response) {
			
				 $("#viewUsers1")
					.html(response);
								
			},
			error : function() {
				alert('Error while fetching response');
			}
		});		 		 		 		
	$("#titleBar").html("<h2><span>KYC DOCUMENT</span></h2>");
	
		}//End Of else
}


</script>
 




</head>
<body>

<br><br><br><br><br><br>


<%
	String ls = (String) request.getAttribute("comp");
	String comp[] = ls.split(",");

	String oldData = (String) request.getAttribute("OldComp");			
   
			
%>

<div align="center">
<div id="flip" >Choose Type of  KYC Document</div>
<div id="panel" >

<% for(int j=0;j<comp.length;j++)
{  	      	
      boolean status = true;
      
      if(oldData!=null)
      { 
    	  String oldarr[] = oldData.split(",");
      
      for(int i=0;i<oldarr.length;i++)
      { 
    	  if(comp[j].equals(oldarr[i]))
    	  {     		 
    		  status = false;%>    	      		
    	<%  } 
      }   
      
      if(status){ 
%>     
     <input  type="checkbox" name="click" class="CheckBoxClass" value="<%=comp[j]%>" id="<%=comp[j]%>"/>
     <label id="Label1" for="<%=comp[j]%>" class="CheckBoxLabelClass"><%=comp[j]%></label>  
     <br><br>
      
      <%} } else{ %>
      
     <input  type="checkbox" name="click" class="CheckBoxClass" value="<%=comp[j]%>" id="<%=comp[j]%>" />
     <label id="Label1" for="<%=comp[j]%>" class="CheckBoxLabelClass"><%=comp[j]%></label>
     <br><br>
<%}}
%>						
			<div id="proceedtbutton">
<input type="submit"  width="20" id="Proceed" name="Proceed" value="Proceed" onClick="chooseIds('choose_category_ID.html')"/><br>
</div>


</div>
</div> 

</body>
</html>

