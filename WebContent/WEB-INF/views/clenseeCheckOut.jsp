<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Plan Checkout</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/checkout.css" type="text/css" charset="utf-8" />


 <link href="resources/css/rupees/rupees.css" rel="stylesheet" type="text/css" />


<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<style>


#waitfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 5px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

</style>


<!-- Hashing for algo for PayuMoney -->


<%!
public boolean empty(String s)
	{
		if(s== null || s.trim().equals(""))
			return true;
		else
			return false;
	}
%>
<%!
	public String hashCal(String type,String str){
		byte[] hashseq=str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try{
		MessageDigest algorithm = MessageDigest.getInstance(type);
		algorithm.reset();
		algorithm.update(hashseq);
		byte messageDigest[] = algorithm.digest();
            
		

		for (int i=0;i<messageDigest.length;i++) {
			String hex=Integer.toHexString(0xFF & messageDigest[i]);
			if(hex.length()==1) hexString.append("0");
			hexString.append(hex);
		}
			
		}catch(NoSuchAlgorithmException nsae){ }
		
		return hexString.toString();


	}
%>

<%

ResourceBundle resource = ResourceBundle.getBundle("resources/payUMoneyMarchentDetails");
String merchantkey=resource.getString("merchantkey");
String saltValue=resource.getString("salt");
String baseurl=resource.getString("baseurl");

%>	


<% 	
	
    String merchant_key=merchantkey;
	String salt=saltValue;
	String action1 ="";
	String base_url=baseurl;
	int error=0;
	String hashString="";
	
	
	Enumeration paramNames = request.getParameterNames();
	Map<String,String> params= new HashMap<String,String>();
    	while(paramNames.hasMoreElements()) 
	{
      		String paramName = (String)paramNames.nextElement();
      
      		String paramValue = request.getParameter(paramName);

		params.put(paramName,paramValue);
	}
	String txnid ="";
	String udf2 = "";
	if(empty(params.get("txnid"))){
		Random rand = new Random();
		String rndm = Integer.toString(rand.nextInt())+(System.currentTimeMillis() / 1000L);
		txnid=hashCal("SHA-256",rndm).substring(0,20);
	}
	else
		txnid=params.get("txnid");
        udf2 = txnid;
	String txn="abcd";
	String hash="";
	String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
	if(empty(params.get("hash")) && params.size()>0)
	{
		if( empty(params.get("key"))
			|| empty(params.get("txnid"))
			|| empty(params.get("amount"))
			|| empty(params.get("firstname"))
			|| empty(params.get("email"))
			|| empty(params.get("phone"))
			|| empty(params.get("productinfo"))
			|| empty(params.get("surl"))
			|| empty(params.get("furl"))
			|| empty(params.get("service_provider"))
	)
			
			error=1;
		else{
			String[] hashVarSeq=hashSequence.split("\\|");
			
			for(String part : hashVarSeq)
			{
				hashString= (empty(params.get(part)))?hashString.concat(""):hashString.concat(params.get(part));
				hashString=hashString.concat("|");
			}
			hashString=hashString.concat(salt);
			

			 hash=hashCal("SHA-512",hashString);
			action1=base_url.concat("/_payment");
		}
	}
	else if(!empty(params.get("hash")))
	{
		hash=params.get("hash");
		action1=base_url.concat("/_payment");
	}
		

%>

	<!-- End of Hashing Algo for PayuMoney -->							  
		
		


<script>

var hash='<%= hash %>';
function submitPayuForm() {
	
	if (hash == '')
		return;

      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }

function backToMigratePlan()
{
	window.location = "individualgetmigrateplan.html";
}



function getkycAboutUs()
{
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	   
	    });
	
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

</script>


</head>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="java.util.*,java.io.*"%><%@ page isELIgnored="false"%>
<%@ page import="java.util.*" %>
<%@ page import="java.security.*" %>


<body onload="submitPayuForm();">


                               <% 
                                  
                                  String  requestedPlanName = (String)request.getAttribute("requestedPlanName");
								  
								  String LatestPicName = (String)request.getAttribute("LatestPicName");
								  
								  String fullName = (String)request.getAttribute("fullName");
								  
								  String emailId = (String)request.getAttribute("emailId");
								
							   %>	
	

     
		  <div class="main">
				
				<div style=" background-color: #333;width:100%; height:77px;">
				<div style="   background-color: #333;height: 60px;margin: 0 auto 0 110px;width: 1024px;">
				<div onclick="gotoHomePage()" class="checklogo" style="float:left">
				
				</div>
				
				  <h1>Payment Details</h1>
				 
				  </div>
				</div>
				 
				 
				 <p style="width: 100%; color:#00b6f5; text-align: center;margin-top:15px;">You have requested to migrate your plan into <%=requestedPlanName%></p>
				 
				  <section class="shopping-cart">
				    
				    <ul class="ui-list shopping-cart--list" id="shopping-cart--list">
				
				        <li class="_grid shopping-cart--list-item">
				          				      				          
					          <div class="_column product-info">
					            
					            <p class="nameCheck"><%=fullName%></p>
					            <h4 class="product-name">Acknowledgement will be intimate to you as per the contact details present in www.clensee.com. </h4>
					            <p class="nameCheck"><%=emailId%></p>
					            <p class="nameCheck">Mob.no :  +91 &nbsp;<%=request.getAttribute("mobileno")%></p>
					            
					          </div>
				          
					          <div class="_column product-modifiers" data-product-price="dfdgfgd">				            
					            <div class="unitpricecss">Unit Price</div>
					            <div class="price product-total-price"><div class="WebRupee"> Rs.</div> <%=request.getAttribute("planPrice")%> </div>
					          
					          </div>
				          
				        </li>
				      
				    </ul>
				   </section>
				   
				    <ul class="_grid cart-totals" Id="checkoutli">
				    <li>
				      <div class="_column subtotal" id="subtotalCtr" style="
    display: inline-tabl;
    height: 258px !important;
    width: 100%;">
				       
					         <div class="cart-totals-key">Product Name</div>
					       
					       	 <div class="cart-totals-value">Individual plan <%=requestedPlanName%></div>
					       	 		
					       	 		
					       	 		<div class="productfeat"><b><u>Product Features</u></b></div>
					       	 		
							       	  <div style="margin-left:18px;">
								       	  <ul id="checkoutinnerul">
									       	 <c:forEach items="${viewplan}" var="det">   
															  
												    <li>
														<c:out value="${det.feature_name}"/> :: <c:out value="${det.feature_value}"/>																
												    </li>
										   	
											</c:forEach> 
								       	 </ul>
							       	 </div>
							       	 
				      </div>
				      </li>
				     
				     
				      <li>
				      <div class="_column shipping" id="shippingCtr" style="width:100%;height: auto !important;">
				        	<div class="cart-totals-key">Taxes</div>
				        	<div class="cart-totals-value"style="margin-top: 40%;">Inclusive</div>
				      </div>
				      </li>
				      
				      
				      <li>
				     
					      <div class="_column taxes" id="taxesCtr" style="height: auto !important; width:100%;">
					        <div class="cart-totals-key">Final amount</div>
					        <div class="cart-totals-value" style="margin-top: 40%;"><%=request.getAttribute("planPrice")%></div>
					      </div>
					      					      
				      </li>
				     
				     
				      <li style="width: 24%;">
				      <div class="cart-totals-key" style="margin-top: -2px ! important;">Checkout</div>
				      <div class="_column checkout" style=" width: 96%;height: auto !important;  margin-top: 35%;">
					  
					   											
						<%if(action1.equals("")) {%>
						
						<form action="indRequestplanchange.html" method="post" name="payuForm">
						
						<%}else{ %>
						
						<form action="<%=action1%>" method="post" name="payuForm">
						
						<%} %>   
						         
						      <input type="hidden" name="planName" value="<%=requestedPlanName%>" />
						         
						      <input type="hidden" name="key" value="<%= merchant_key %>" />
						      <input type="hidden" name="hash" value="<%= hash %>"/>
						      <input type="hidden" name="txnid" value="<%= txnid %>" />
						      <input type="hidden" name="udf2" value="<%= txnid %>" />
							  <input type="hidden" name="service_provider" value="payu_paisa" />
						      
						      <input name="amount" type="hidden" value="<%=request.getAttribute("planPrice")%>" />
						      <input name="firstname" type="hidden" id="firstname" value="<%=fullName%>" />
						      <input name="email" id="email" type="hidden" value="<%=emailId%>" />
						      <input name="phone" type="hidden" value="<%=request.getAttribute("mobileno")%>" />
						      <input name="productinfo" type="hidden" value="Payment For Plan Change" size="64" />
			 		      
						      <input name="surl" type="hidden" value="http://182.18.162.221:8080/KYC/checkoutSuccess.html" size="60" />
						      <input name="furl" type="hidden" value="http://182.18.162.221:8080/KYC/checkoutFailure.html" size="60" />
						      						       
						      <%if(empty(hash)){%>
						      
						          <input class="_btn checkout-btn entypo-forward" type="submit" value="Pay Now" />
						          
						      <%}%>
						      
						      
						      
						 </form>
						 
						 <div style="margin-top:20px;"><img width="200" height="95" src="resources/images/paymentLogo-1.jpg"></div>			
						 
				      </div>
				            
				     </li>
				      
				 </ul>
							    	
				</section>
				  
				</div>
		    
		      
		      
   
                      <div class="columnback">
				      
				              <input class="_btn checkout-btn entypo-forward" type="button" value="Back to Plan" onclick="backToMigratePlan()"/>
				              
				      </div>
				      
								  



<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
						</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	
	<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
		              
		              			 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									

                      </div>
                  
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" style="margin-top: -10px ! important;" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	

</body>
</html>