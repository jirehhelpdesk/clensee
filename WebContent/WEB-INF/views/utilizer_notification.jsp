<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*"%>

<%@page import="java.text.DateFormat" %> 
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date,java.util.*;"%>

<title>Utilizer Notification</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<head>
		
	<link rel="stylesheet" href="resources/demos/style.css">	
	<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<!-- /* Pop up Script and css*/ -->

<script type="text/javascript">

window.document.onkeydown = function (e)
{
    if (!e)
    {
        e = event;
    }
    if (e.keyCode == 27)
    {
        lightbox_close();
    }
}


function lightbox_close()
{	
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';    
}

</script>	

<style type="text/css">

#fade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#light{
    
    background: none repeat scroll 0 0 #fff;
    border: 2px solid #00b5f6;
    height: auto;
    left: 50%;
    margin-left: -321px;
    margin-top: -230px;
    overflow: visible;
    padding: 0;
    position: fixed;
    top: 68%;
    width: 680px;
    z-index: 1002;
    
}
#light h1{
margin-top:0px;
margin-left: 0px !important;
}
#light p{
padding:10px;
text-align:justify;
margin: 0;
}


</style>

<!-- /* End of Pop up Script and css*/ -->

	
<script src="resources/kyc_scripts/jquery-1.11.1.js"></script>	
<link rel="stylesheet" href="resources/notification-tab/style.css">
<script src="resources/notification-tab/organictabs.jquery.js"></script>
	
<script>

		$(function() {
		
			// Calling the plugin
			$("#example-one").organicTabs();
			$("#example-two").organicTabs({
				"speed": 100,
				"param": "tab"
			});
			
		});


function indNoteDiv()
{
	$("#noteDivId").html("In individual given access then here it will notify.");
}
function adminNoteDiv()
{		
	$("#noteDivId").html("Any notifications sent by Clensee admin will be shown here.");
}

function viewNotifyMessage(notify_Id)
{	
	$.ajax
    ({
		type : "post",				
		url : "getUtiNotifyMessage.html",			
		data :"notify_Id="+notify_Id,	  
		success : function(message) {
			
			$('#displaySharedDocDiv').empty();
			$('#light').show();
			$('#fade').show();			
			$('#displaySharedDocDiv').append("<h1><b>Notification Message</b></h1>");
		    $('#displaySharedDocDiv').append("<p>"+message+"</p>"); 
		    $('#displaySharedDocDiv').append("<p>From,</p>");
		    $('#displaySharedDocDiv').append("<p style=\"margin-top:-20px ! important;\">Clensee Admin.</p>");
		    $('#displaySharedDocDiv').append("<p>Please write a mail to admin@clensee.com for any queries regarding this notification.</p>");
		   
		},
    });
	
}

</script>

</head>
<div id="page-wrap" style="height:200px;display:inline-table !important;">
	
	<div id="example-two" >
		
				<ul class="nav">
					<li class="nav-one"><a href="#featured2" class="current" onclick="indNoteDiv()">Individual</a></li>
					<li class="nav-three"><a href="#jquerytuts2" style="width: 101px;" onclick="adminNoteDiv()">Clensee Admin</a></li>				
				</ul>
		
					<div class="list-wrap" style="padding-bottom: 25px; height: auto;">
					
					
							<ul id="featured2">
							 <b>Individual Notifications</b>
							 <br>
							<p>
							    <div id="History" style="display:inline-table;overflow: visible; margin-top: 25px;width: 100%;">
	
										<c:if test="${!empty ind2utiNotification}">
												
												<ul style="text-align:left;margin-left:-30px;">	
												<%int n=1; %>						       
											
												<c:forEach items="${ind2utiNotification}" var="det">					 
													
																
																<c:set var="activityDateStamp"  value="${det.access_given_date}"/>
		
																 <%
																     Date activityTime = (Date)pageContext.getAttribute("activityDateStamp");
																 
																	 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");									 							
																	 String reportDate = df.format(activityTime);
																	 
																	 String input = reportDate;
													    			 DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
													    			 DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd KK:mm a"); 
													    			 
														         %>	  
																	         
													 <%if(n%2!=0){ %>
																					 
															        <div class="activity2" ><b><%=outputFormat.format(inputFormat.parse(input))%></b> &nbsp; <c:out value="${det.giver_name}"/> <c:out value=" has given access to use thier documents."/></div>
															         
													   <%}else{ %>
															 
															       <div class="activity1" ><b><%=outputFormat.format(inputFormat.parse(input))%></b> &nbsp; <c:out value="${det.giver_name}"/> <c:out value=" has given access to use thier documents."/></div>
															        
													   <%} %>
														
														<%n++;%>					   												
												</c:forEach>	
												</ul>		
													
										</c:if>
										
										<c:if test="${empty ind2utiNotification}">
										
											<div class="notice" style="margin-left: 245px;margin: 72px auto 0;width: 250px;">
											      
											      <b>NOTICE :</b>No Notification from Individual till now !
											      
											</div>
										
										</c:if>
										
									</div>
							    
							</p>
								
							</ul>
																														
							<ul id="jquerytuts2" class="hide">
							
								<b>Clensee Admin Notifications</b>
								
								
									<div id="History" style="display: inline-table;overflow: visible; margin-top: 25px;">
	
										<c:if test="${!empty kycNotification}">
												
												<ul style="text-align:left;margin-left:-30px;">	
												<%int n=1; %>						       
													
														<c:forEach items="${kycNotification}" var="det">					 
															
															
															<c:set var="activityDateStamp"  value="${det.notification_date}"/>
		
																 <%
																     Date activityTime = (Date)pageContext.getAttribute("activityDateStamp");
																 
																	 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");									 							
																	 String reportDate = df.format(activityTime);
																	 
																	 String input = reportDate;
													    			 DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
													    			 DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd KK:mm a"); 													    			 														            
														         %>	  
																			         
															  <%if(n%2!=0){ %>
																							 
																	        <div class="activity2" ><b><%=outputFormat.format(inputFormat.parse(input))%></b> &nbsp; <c:out value="${det.notification_subject}"/> <a href="#" onclick="viewNotifyMessage('${det.notification_id}')">Read</a> </div>
																	         
															   <%}else{ %>
																	 
																	       <div class="activity1" ><b><%=outputFormat.format(inputFormat.parse(input))%></b> &nbsp; <c:out value="${det.notification_subject}"/> <a href="#" onclick="viewNotifyMessage('${det.notification_id}')">Read</a> </div>
																	        
															   <%} %>
																
																<%n++;%>														
															 		
														</c:forEach>	
														
												</ul>		
													
										</c:if>
										
										
									</div>
									
									<c:if test="${empty kycNotification}">
										
											<div class="notice" style="margin-left: 245px;margin: 72px auto 0;width: 250px;">
											      
											      <b>NOTICE :</b>No Notification from Admin till now !
											      
											</div>
										
										</c:if>
										
								 																			
							</ul>
							
				
					</div> <!-- END List Wrap -->
			
	     </div> <!-- END Organic Tabs (Example One) -->
			
</div>

<div id="noteDivId" style="text-align:left;color: #7c7c7c;font-size: 12px;margin-left: 2px;">
        
        In individual given access then here it will notify !!!
        
</div>


     <div id="light">
			<div class="colsebutton" onclick="lightbox_close()"><img class="footerpageinn_links" height="22" width="22" src="resources/images/close_button.png"></div>
		    <div id="displaySharedDocDiv" style="text-align:justify;"> 
		  
		    </div>		   
	</div>
	
	<div id="fade" onClick="lightbox_close();"></div> 	

   
</body>
</html>