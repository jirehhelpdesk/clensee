 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Documents</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/jscripts/jquery.com.jquery-1.10.2.js"></script>
<script src="resources/jscripts/jquery.com.jqueryUi.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}
</style>



<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
    
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
    
}

</style>

<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';
	    			    		
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	 error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    	 	
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

</script>


<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{

    background: none repeat scroll 0 0 #fff;
       border: 2px solid #00b6f5;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}
</style>
 
<script>

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewDocumentInPopUp(fileName)
{
	
	
	$.ajax({
		type : "get",				
		url : "viewAcademic.html",
		data: "fileName="+fileName,
		success : function(response) {
		
			document.getElementById('kycDocPopUplight').style.display='block';
	        document.getElementById('kycDocPopUpfade').style.display='block';
	        $("#kycDocPopUpDisplay").html(response);
	        			
		},		
	});		 		 		 		
	
}

</script> 
 



<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
		
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";			    		 
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    	     $('#searchicon').show();
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length==0)
	    {		 
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
		    $('#searchicon').hide();
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
	 
	 if(id.length<3)
	    {		 			
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
		    $('#searchicon').hide();
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
}


function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}

</script>




       
 <style>
            .notClicked {color: black}
            .Clicked {color: red}
        </style>
        


<script src="resources/js/kyc_side_login.js"></script>

<script>
function viewHomeProfile(url) 
{
	window.location = url;	 
}	
</script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<script type="text/javascript">

function AcaViewMore(urlLink)
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	$.ajax
    ({
		type : "post",				
		url : urlLink,						
		success : function(response) {
				
			document.getElementById('waitlight').style.display='none';
			document.getElementById('waitfade').style.display='none';
			
			 $("#viewUsers1").html(response);
											
		},
		error : function() {
			alert('Problem due to server failed Try again Later !');
		}
	});		 		 		 		
   $("#titleBar").html("<h2><span>Academic Details</span></h2>");
	}

function backtoMain(url)
{			
	window.location = url;
}

function viewDocumentInPopUpfromHistory(fileName)
{

	 $.ajax({
				type : "post",
				url : "checksessionforIndividual.html",
				success : function(response) {
					
					if(response=="Exist")
						{
								document.getElementById('waitlight').style.display='block';
								document.getElementById('waitfade').style.display='block';
								
								$.ajax({
									type : "get",				
									url : "viewAcademic.html",
									data: "fileName="+fileName,
									success : function(response) {
									
										document.getElementById('waitlight').style.display='none';
										document.getElementById('waitfade').style.display='none';
													
										document.getElementById('kycDocPopUplight').style.display='block';
								        document.getElementById('kycDocPopUpfade').style.display='block';
								        
								        $("#kycDocPopUpDisplay").html(response);
								        			
									},		
								});		 		 		 		
						
						}
					else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	 
	
}

function submitForHistory(msg)
{
	window.location = "academicHistorydetail.html?id="+msg;			
}

function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}
 
function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
 
	 document.getElementById("inputString").value = "";	 
}	 

function hideAddvertise()
{
	$('#ProfileHeadingDiv').fadeOut(90000);
}
</script>

</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a onclick="viewHomeProfile('profiletohome.html')"  class="myprofile" ><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a href="myDocumentsMain.html" class="mydocumentsfocus" > <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a href="generateCode.html" class="accessmanager"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore" href="individualstore.html"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a href="myvisitors.html" class="alearticons"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a href="individualviewplan.html" class="myplans"> <br> <br>
							 Plans
					</a></li>
					<li id="settingMenu"><a href="setting.html" class="mysettings">
					 <br> <br>
							 Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 
 
 					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
	        
	        <!-- Search Bar -->   
	        
	            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of Search bar -->
            
            <!-- Search Division  -->
            
            <span class="topprofilestyle" style="display: none;"></span>
					    
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
															
						</div>
						
			<!-- End Search Division  -->
						
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
	            
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
             
        
					
		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
							  <ul>
							     <li id="Hi-B-1"><a href="myDocumentsMain.html"  id="Hi-B-1" ><span>KYC Documents</span></a></li>
								 <li id="academic_document"><a href="getacademicdetails.html"  id="academic_documentactive" ><span>Academic Documents</span></a></li>															
								 <li id="employee_document"><a href="employeeDetails.html"  id="Hi-B-4"><span>Employment Documents</span></a></li>
								 <li id="financial_document"><a href="search_financialDoc.html"  id="Hi-B-3" ><span>Financial Documents</span></a></li>
							 </ul>
						   </div>    		
					</div>
					
					 </div>
					
					<div class="rightside">
					
					
					    
						
					<!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

					<div id="ProfileHeadingDiv" style="display:block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>	
							        		
					   <!-- Advertisment Div for Entire Page  -->
					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span><%=(String)request.getAttribute("TypeOfDoc")%> history details as on <%=session.getAttribute("hitoryDate")%></span>								   
							</h2>
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
				
												
								<%
								
								  String ind_id = request.getAttribute("user").toString(); 
								  String year = request.getAttribute("year").toString(); 
								  String docs_data = (String)request.getAttribute("docsData");
								  String docName = (String)request.getAttribute("docName");
								  String doctype[] = docs_data.split("-");
								  String docs_dataArray[] = doctype[1].split(",");
								  
								%>
								       
								       <div id="panel-2" style="width:100%;">
											   			  			   
											   
											         <div id="kycdocumentpage" style="margin-left: 0px ! important;" class="kycdocumentpage">  	<!-- start of kycdocumentpage -->														
											                           <div style="margin-top:15px;margin-left:0px ! important;">
														           	      <button class="backbuttonindex_histy" id="viewDocument" onclick="submitForHistory('<%=doctype[0]%>')">Back</button>   
														           	    </div>	 
												                  <div class="kycdocumentdetails-ext">      <!-- Division for Left side Hierarchy other component with data is in left side -->
												                         
												                         <table>
													                       
													                       <% for(int i=0;i<docs_dataArray.length;i++) {%>
													                         
													                         <tr style="margin-top:15px;">
													                              <td style="width:auto;"><b><%=docs_dataArray[i].split(":")[0]%></b></td>
													                              <td style="width:10px;">:</td>
													                              <td style="width:auto;"><%=docs_dataArray[i].split(":")[1]%></td>			                              
													                         </tr>
													                         
													                       <%} %>
													                       
													                      </table>  					                       					                          
													              </div>      <!-- End of Division for Left side Hierarchy other component with data is in left side -->
													
												                     <div class="academicdocumenttitle">
												                     
																		<h1>
																			<span></span>
																		 <%=(String)request.getAttribute("TypeOfDoc")%> documents </h1>
																		</div>    	
													    
																  <div class="academicdetailsimg">     <!-- Division for right side Hierarchy file component with data  -->
																	             
															              <ul>
															               
															               <%if(!docName.equals("No Data")) {
															               
															            	    String docNameArray[] = docName.split(",");							            	    
															            	    for(int dn=0;dn<docNameArray.length;dn++)
															            	    {
															                    %>
															                    
															                   <li class="panviewinner_acd">
															                    
															                    <%if(docNameArray[dn].contains("pdf")) {%>
															                    
															                           <b><%=docNameArray[dn].split("-")[1].split("_")[0]%></b> 
															                           
															                  		   <img src="resources/images/PNA_PDF.png"   width="150" height="100" /></img>
																														              
											                                           <a class="mydocumentdownloadpan" href="downloadAcademic.html?fileName=<%=docNameArray[dn]%>" ></a>
																	                    
											                                    <%} else {%>
											                                         	
											                                         	
											                                         	<b><%=docNameArray[dn].split("-")[1].split("_")[0]%></b> 
											                                         	  
											                                         	
											                                         	
																					    <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+docNameArray[dn]+"&docCategory=ACADEMIC_DOC"%>"   width="150" height="100" /></img>
																		                                                         
										                                                <a onclick="viewDocumentInPopUpfromHistory('<%=docNameArray[dn]%>')"><button class="mydocumentviewpan"></button></a>
											                                            <a  href="downloadAcademic.html?fileName=<%=docNameArray[dn]%>" ><button class="mydocumentdownloadpan"></button></a>
											                                            
											                                        <%} %>                                        
															                    </li>
															                    <%} %>  
															                <%} else {%> 
															                
															                          <h1> Academic Documents are not uploaded yet !</h1>
															                <%} %>    
															              </ul>
																	   
																	</div>                               <!-- Division for right side Hierarchy file component with data -->
														           	   
														           		
														           	    				           	    						           	           	      				                          
											           </div>      <!-- End of kycdocumentpage -->		
											                				 				   					  
									        
									                             
									           
										</div>   <!-- End of panel-1 division  -->
											
								       
								                
								                
								               
								                
                                        
				
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

  
	
	
	
	
	
	
	    
</div> <!--end of right div  -->

	</div>

</div>
					
					
					
					
					
					
		<%
		ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
		String version=resource.getString("kycversion");
		String versionDate=resource.getString("kyclatestDate");
		%>			
					
					
					
			
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
                                     
                                 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									     
                                     
                                     
                      </div>
                      
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->
	
	
	
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	

	
	<!-- Document Pop up Div -->
	
	<div id="kycDocPopUplight">
			
			<div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		         		  
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:0; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF Document Pop up Div -->	


</body>

</html>
     	