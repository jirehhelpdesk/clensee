 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Contact Us</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/jscripts/jquery.com.jquery-1.10.2.js"></script>
<script src="resources/jscripts/jquery.com.jqueryUi.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 5px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 5px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}
</style>

<script type="text/javascript">

function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		 window.location = "contactus.html";
		}
}
function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}


function viewPro1(){
		
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
		
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		     $('#searchhomepage').html("");
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {		
		    document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
		    
		    $('#searchhomepage').hide();
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();				 	
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}

function individualsearchhomepage(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}


function ValidateComp()
{	
	var flag = 'true';
	
	var name = document.getElementById("nameId").value;	
	var emailId = document.getElementById("emailIdid").value;	
	var mobileNo = document.getElementById("mobileNoId").value;	
	var modeOfContact = document.getElementById("modeOfContactId").value;
	var description = document.getElementById("descriptionId").value;
		
	var alfabet  = /^[a-zA-Z\s]*$/;
	var num = /^[0-9]+$/;
	var emailIdRegex = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;	
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	if(name=="")
		{
			document.getElementById('waitlight').style.display='none';
	    	document.getElementById('waitfade').style.display='none';
		   flag = 'false';
		   
		    
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please enter your name . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  

			  
		   return false;
		}
	
	if(name!="")
		{
			if(!name.match(alfabet))  
	        { 
				document.getElementById('waitlight').style.display='none';
		    	document.getElementById('waitfade').style.display='none';
				flag = 'false';
				
			     document.getElementById('sociallight').style.display='block';
			     document.getElementById('socialfade').style.display='block';
			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				 $("#socialdisplaySharedDocDiv").html("Only alphabets are allowed for name . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

				return false;
	        }
		}
	
	if(emailId=="")
		{
			document.getElementById('waitlight').style.display='none';
    		document.getElementById('waitfade').style.display='none';
		   flag = 'false';
		   
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please enter a email id . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

		   return false;
		}
	
	if(emailId!="")
		{
			if(!emailId.match(emailIdRegex))  
	        { 
				document.getElementById('waitlight').style.display='none';
		    	document.getElementById('waitfade').style.display='none';
				flag = 'false';
				
			     document.getElementById('sociallight').style.display='block';
			     document.getElementById('socialfade').style.display='block';
			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				 $("#socialdisplaySharedDocDiv").html("Please enter a valid email id . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  

				return false;
	        }
		}
	
	if(mobileNo!="")
		{
			if(!mobileNo.match(num))  
	        { 
				document.getElementById('waitlight').style.display='none';
		    	document.getElementById('waitfade').style.display='none';
				flag = 'false';
				
			     document.getElementById('sociallight').style.display='block';
			     document.getElementById('socialfade').style.display='block';
			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				 $("#socialdisplaySharedDocDiv").html("Please enter a valid mobile number. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

				return false;
	        }
			
			if(mobileNo.length!=10)
			{
				 document.getElementById('waitlight').style.display='none';
		    	 document.getElementById('waitfade').style.display='none';
				 flag = 'false';
				
			     document.getElementById('sociallight').style.display='block';
			     document.getElementById('socialfade').style.display='block';
			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				 $("#socialdisplaySharedDocDiv").html("Please enter a valid mobile number. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
				
				 return false;
			}
		 	
		}
	
	
	if(modeOfContact=="Select Contact Mode")
		{
			document.getElementById('waitlight').style.display='none';
    		document.getElementById('waitfade').style.display='none';
			flag = 'false';
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please select a contact mode. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  

			return false;
		}
	
	if(description=="")
		{
			document.getElementById('waitlight').style.display='none';
    		document.getElementById('waitfade').style.display='none';
			flag = 'false';
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please give some description . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  

			return false;
		}
	
	if(flag=='true')
		{
			$.ajax({  
				     type : "Post",   
				     url : "getNotifyOfContactUs.html", 
				     data :$('#contactUsForm').serialize(),	     	     	     
				     success : function(response) 
				     {  
				    	 document.getElementById('waitlight').style.display='none';
					     document.getElementById('waitfade').style.display='none';
					    	
					     document.getElementById('sociallight').style.display='block';
					     document.getElementById('socialfade').style.display='block';
					     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
					     $("#socialdisplaySharedDocDiv").html(""+response+" <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");   
						 				    	
				     },  				     
			    });  
		}
}



function showSignOutButton()
{
	var flagId = document.getElementById("signoutflag").value;
	if(flagId=="0")
		{
		   $("#loginBox").show();
		   document.getElementById("signoutflag").value = "1";
		}
	else
		{
		   $("#loginBox").hide();
		   document.getElementById("signoutflag").value = "0";
		}
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 
	 document.getElementById("inputString").value = "";	 
}


</script>

</head>

<body  onclick="hidesearchDiv()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<%
	String name = (String) request.getAttribute("individualFirstName");
%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a  href="profiletohome.html"  class="myprofile" ><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a href="myDocumentsMain.html" class="mydocuments"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a href="generateCode.html" class="accessmanager"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore" href="individualstore.html"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a href="myvisitors.html" class="alearticons"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a href="individualviewplan.html" class="myplans"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a href="setting.html" class="mysettings">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 					
 					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
            
            <input type="hidden" id="signoutflag" value="0" />
            
             <a  id="loginButton" onclick="showSignOutButton()">
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
           
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox" >  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                                            
			<div class="main_center">



                    <div id="contactusDiv">
                    
                    
                    
                    
                    
                    </div>




				<div class="leftside">

					
						
						<div id="kyccontacemail">
						<h2>
						<span>
						<img width="25" height="26" src="resources/kyc_images/contact_info.png">
						<p>Contact Info</p>
						</span>
						</h2>
						<ul>	
						<li>
						<span>Customer Support</span>
						<p>support@clensee.com</p>
						
						</li>
						<li>
						
						<span>Technical support</span>
						<p>techsupport@clensee.com</p>
						</li>
						
						<li>
					<span>Marketing support</span>
					<p>marketing@clensee.com</p>
						</li>
						</ul>
						
						
					</div>

				</div>

       <%
			ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");					
			String version = resource.getString("kycversion");
			String versionDate = resource.getString("kyclatestDate");
			String contactMode = resource.getString("contactmode");
			String contactModeArray[] = contactMode.split(",");
		%>			
			

				<div class="rightside">

					

					
								<div id="contactdivStyle" class="kyccontactform">
								<h1><b>Contact Form</b>
								<small></small>
								</h1>
								<div id="kyccontactpage">
								
								<form id="contactUsForm" method="post">
										
											 <table>
											 <tr>
											<td width="100" >Name <span>*</span></td>
											<td>:</td>
											<td>
											<input type="text" placeholder="Full Name" id="nameId" name="name"  maxlength="40" >
											</td>	
											</tr>
											<tr>
											<td width="100" >E - Mail id <span>*</span></td>
											<td>:</td>
											<td>
											<input type="text" placeholder="e - Mail id" id="emailIdid" name="emailId"  maxlength="30" >
											</td>	
											</tr>
												<tr>
											<td width="100" >Mobile No</td>
											<td>:</td>
											<td>
											<input type="text" placeholder="Mobile No" id="mobileNoId" name="mobileNo" maxlength="10">
											</td>	
											</tr>
												<tr>
											<td width="100" >Mode of Contact</td>
											<td>:</td>
											<td>
											<select id="modeOfContactId" name="modeOfContact">
											         <option value="Select Contact Mode">Select Contact Mode</option>
											         <%for(int i=0;i<contactModeArray.length;i++){ %>
											         
											         <option value="<%=contactModeArray[i]%>"><%=contactModeArray[i]%></option>
											         
											         <%} %>
											        
											</select>
											</td>	
											</tr>
											<tr>
											<td valign="top"  width="100" >Descriptions</td>
											<td valign="top" >:</td>
											<td>
											<textarea id="descriptionId" name="description" placeholder="Descriptions with in 500 character"  maxlength="500" ></textarea>
											</td>	
											</tr>
										</table>
										
								</form>
										
										<div class="hscbuttondiv">
										<ul>
										<li>
										
										   <input id="buttonIdHSC"  type="button" onclick="ValidateComp()" value="Send">
										
										</li>
										</ul>
										</div>
									</div>
									
									</div>





					<div id="suggestions1" style="display: block; margin: 0 auto; width: 100%;">


					</div>
					<!-- End of -->


				</div><!--end of right div  -->
				

			</div>     <!--end of main-center div  -->

      </div>     <!--end of main div  -->
					

		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
								
								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give Us Few Seconds.....
						       </div>
									
                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	

	
<!-- EOF Waiting Div -->
	
	
	<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
	

		
    <div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
	
	<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->
	
</body>
</html>
     	