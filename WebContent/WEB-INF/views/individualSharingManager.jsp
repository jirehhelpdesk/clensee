 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Sharing Manager</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/kycslide.js"></script>
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css"/>

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1009;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1011;
}
</style>

<script>

function showHideDiv1(id){
	

	myTextBox1 = document.getElementById("b6");
	value=myTextBox1.value;
	
	  var obj = document.getElementById(id);
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	    } else if(obj.style.display=="block"){
	      obj.style.display='none';
	    }
	}
</script>


<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}



</style>

<script>

function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		   window.location = "generateCode.html";
		}
	else
		{
		    $("#successcode").hide();
		    $("#fade").hide();
		}
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
			 document.getElementById('waitfade').style.display='none';
			    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
			 document.getElementById('waitfade').style.display='none';
			    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         } 
	     
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';
			    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>

<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length==0)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
		 	
	    }
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();	
			$('.topprofilestyle').hide();
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}
</script>





<script>

/*home page*/

function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/
       
	/* All scripts for Access Manager */
	
	function generateCode(handlerToHit) {
		
		document.getElementById("Hi-B-1").className = "generatcodeiconsactive";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "";
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					  
					 $("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Server Problem Try again Later !');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Generate Code</span></h2>");
	}
	
	function allowAccess(handlerToHit) {
 		
		
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "allowaccessiconactive";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "";
		
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {		
					 
					$('#suggestions1').show();
					$("#ProfileHeadingDiv").show();
					$("#c-body-div").show();

							
							
					$("#leftsideBasicInfoDiv").show();					
					$('#searchhomepage').hide();
				 	
					document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';
				    
					$("#viewUsers1").html(response);
					 
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Allow Access</span></h2>");
	}
	
	function revokeAccess(handlerToHit) {
 		
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "revokecodeiconactive";
		document.getElementById("Hi-B-5").className = "";
		
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	    
	    $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
								$.ajax({
									type : "post",
									url : handlerToHit,
									cache : false,
									success : function(response) {		
										 
										document.getElementById('waitlight').style.display='none';
									    document.getElementById('waitfade').style.display='none';
									    
										$('#suggestions1').show();
										$("#ProfileHeadingDiv").show();
										$("#center-body-div").show();
										$("#leftsideBasicInfoDiv").show();					
										$('#searchhomepage').hide();
										
										$("#viewUsers1").html(response);
										
										getIndAccDetails();
										
									},
									error : function() {
										alert('Error while fetching response');
									}
								});
							 		 		 		
							$("#titleBar").html("<h2><span>Revoke Code</span></h2>");
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
		 
	}
	
	function accessHistory(handlerToHit) {
 		
		document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	    
		document.getElementById("Hi-B-1").className = "";
		document.getElementById("Hi-B-2").className = "";
		document.getElementById("Hi-B-3").className = "";
		document.getElementById("Hi-B-4").className = "";
		document.getElementById("Hi-B-5").className = "sharingdocumenthisticonactive";
						
		$.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
								$.ajax({
									type : "post",
									url : handlerToHit,
									cache : false,
									success : function(response) {		
										
										document.getElementById('waitlight').style.display='none';
									    document.getElementById('waitfade').style.display='none';
									    
										$('#suggestions1').show();
										$("#ProfileHeadingDiv").show();
										$("#center-body-div").show();
										$("#leftsideBasicInfoDiv").show();					
										$('#searchhomepage').hide();
										
										
										$("#viewUsers1").html(response);
										
										IndHistory();
										 
									},
									error : function() {
										alert('Error while fetching response');
									}
								});
							 		 		 		
							$("#titleBar").html("<h2><span>Sharing History</span></h2>");
					}
					else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
				}
			}); 		 
     	}
	
	
		
	
	 /* End of AccessManger scripts */

	
</script>


<script type="text/javascript">

function viewHomeProfile(url) 
{
	window.location = url;	 
}	

function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}	


</script>

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 
 
 <!-- Script for sharing Manager -->
 
 <script type="text/javascript">
 
 
function showLatestDoc(type) {  
	
if(type!="Select your Document")	
{
	var labelName = document.getElementById("labelname").value;
	var patternValue = document.getElementById("newPattern").value;		
	var existValue = "";
	var viewValue = "";
	$("#recipentDetailsDivId").hide();
	
	/* if values are already choosen then here we can find those values  */
	
	if(patternValue!="")
		{
	       var existArray1 = patternValue.split(",");
	       for(var e=0;e<existArray1.length;e++)
	    	   {
	    	      var subType = existArray1[e].split("--");	    	    
	    	      existValue += subType[1]+","; 
	    	      viewValue += existArray1[e]+",";
	    	   }		       
		}
	
	existValue = existValue.substring(0,existValue.length-1);
			
	var existValAry = existValue.split(",");
	
	/* EOS code for values are already choosen then here we can find those values  */
	
	$.ajax({
		
			type : "Post",
			url : "latestDocumentType.html",
			data : "doctype=" + type,			
			success : function(response) {     
							
				 if(type=='KYC_DOCUMENT')
			      {
					    $('#contentDiv').html("");					    
						$('#headingSpan').empty();
						$('#headingSpan').html("<h1 class=\"headingh1Span\"><span>Kyc Documents</span></h1>");								
			      }
			     
			     if(type=='ACADEMIC_DOC')
		    	 {
			    	$('#contentDiv').html("");
					$('#headingSpan').empty();
					$('#headingSpan').html("<h1 class=\"headingh1Span\"><span>Academic Documents</span></h1>");							
		    	 }
			     
			     if(type=='Financial_DOC')
		    	 {
			        $('#contentDiv').html("");
					$('#headingSpan').empty();
					$('#headingSpan').html("<h1 class=\"headingh1Span\"><span>Financial Documents</span></h1>");							
		    	 }
			     
			     if(type=='Employment_DOC')
		    	 {
			    	$('#contentDiv').html("");
					$('#headingSpan').empty();
					$('#headingSpan').html("<h1 class=\"headingh1Span\"><span>Employment Documents</span></h1>");							
		    	 }
			     
				if(response=="No Document you have uploaded" || response=="No Data")
					{
					     $('#contentDiv').append("You have not uploaded any documents!");
	    			     
					}
				else if(response=='null')
					{
					     $('#contentDiv').append("You have not uploaded any document!");	    			     
					}
				else
					{						
					    					    					     					     					   					     				
					     if(type=='KYC_DOCUMENT')
					      {					    	 
		 				            var kycdocArray = "";
									var kycdocName = "";
									
									var kycDocNameList = response;
																		
									var kycDocNameListArray = kycDocNameList.split(",");
									
									for(var k=0;k<kycDocNameListArray.length;k++)
										{					      
												var eachfrontone =  kycDocNameListArray[k].indexOf("front");			    	
										    	if(eachfrontone>0)
										    		{
										    		    kycdocName = kycDocNameListArray[k].split("front")[0];
										    		}
										    	
										    	var eachbackone = kycDocNameListArray[k].indexOf("back");		
										    	
										    	if(eachbackone>0)
										    		{			    		    
										    		    if(kycDocNameListArray[k].split("back")[0]==kycdocName)
										    		    	{			    		    					    		    	      		    	
										    		    	   kycdocArray += kycdocName+"-"+kycDocNameListArray[k-1]+"#"+kycDocNameListArray[k]+"/";
										    		    	}							    		    
										    		}
										    	else
										    		{
										    		    var checkBackDoc = kycDocNameListArray[k].indexOf("Not Uploaded");
										    		    if(checkBackDoc==0)
											    		{			    		    											    		    		    		    					    		    	      		    	
											    		     kycdocArray += kycdocName+"-"+kycDocNameListArray[k-1]+"/";											    		    							    		    
											    		}
										    		}
										    	
										} 
									
									kycdocArray = kycdocArray.substring(0,kycdocArray.length-1);
									
									 var obj = kycdocArray.split("/");
									 
									 var docExist = 'true';			
									 
										for ( var k = 0; k < obj.length; k++)
											{
												  var flag = 'true';										    							    
												  for ( var l = 0; l < existValAry.length; l++)
														{								    	       
															if(obj[k]!="No Document")
																 {															  
																     docExist = 'false';
																     
																     if(obj[k]!="")
																    	 {
																	    	 if(obj[k]!="No Data")
																	    	 {
																	    		 docExist = 'false';
																	    		 
																	    		 if(obj[k]==existValAry[l])
																				  {
																				     flag = 'false';
																				     
																				     $('#contentDiv').append("<p><input  name=\"patternType" + k + "\" id=\"patternType" + k + "\" type=\"checkbox\" checked=\"true\" value=\""+obj[k]+"\" onClick=\"addNewListItem(this,"+k+")\"  />" + obj[k].split("-")[0] + "</p>");	
																				  }	
																	    	 }
																	    	
																    	 }												  
																  }										  
														  }		
												    
												    if(flag=='true')
													   {
												    	 if(obj[k]!="No Document")
														   {
												    		  if(obj[k]!="")
												    			  {
												    			   if(obj[k]!="No Data")
															    	 {								    				      							    				    
												    				      $('#contentDiv').append("<p><input  name=\"patternType" + k + "\" id=\"patternType" + k + "\" type=\"checkbox\" value=\""+obj[k]+"\" onClick=\"addNewListItem(this,"+k+")\"  />" + obj[k].split("-")[0] + "</p>");
															    	 }							    			    	
												    			  }								          							   								    
														    }
													   }
												}	
												
												
												if(docExist == 'true')
													{
														 
													     $('#contentDiv').append("You have not uploaded any documents!");
													}
												
												var values = ""; 
												
												var patternValue = document.getElementById("newPattern").value;										
																								
												$("#viewDocId").html(values);						
												
												/* Script for view already selected type in right side div ! */
												
												viewValue = viewValue.substring(0, viewValue.length-1);		
												var viewValueAry = viewValue.split(",");
												
												$("#viewDocId").append("<ul>");
												
												for(var v=0;v<viewValueAry.length;v++)
												{
												   var viewData = viewValueAry[v];				   
												   var checkType = viewData.split("--")[0];
												   
													if(checkType=="KYC")
													   {													   
														   $("#viewDocId").append("<li>"+viewValueAry[v].split(" -")[0]+"</li>");
													   }
													else
														{
														    $("#viewDocId").append("<li>"+viewValueAry[v].split("_")[0]+"</li>");
														}											       
												}
												
												$("#viewDocId").append("</ul>");
												
												/* EOS for view already selected type in right side div ! */
					      }					     
					     else
					      {		
					    	 var obj = response.split(",");
					    	 
							    var docExist = 'true';								     
								for ( var k = 0; k < obj.length; k++)
									{
										  var flag = 'true';										    							    
										  for ( var l = 0; l < existValAry.length; l++)
												{								    	       
													if(obj[k]!="No Document")
														 {															  
														     docExist = 'false';
														     
														     if(obj[k]!="")
														    	 {
															    	 if(obj[k]!="No Data")
															    	 {
															    		 docExist = 'false';
															    		 
															    		 if(obj[k]==existValAry[l])
																		  {
																		     flag = 'false';
																		     
																		     $('#contentDiv').append("<p><input  name=\"patternType" + k + "\" id=\"patternType" + k + "\" type=\"checkbox\" checked=\"true\" value=\""+obj[k]+"\" onClick=\"addNewListItem(this,"+k+")\"  />" + obj[k].split("_")[0] + "</p>");	
																		  }	
															    	 }
															    	
														    	 }												  
														  }										  
												  }		
										    
										    if(flag=='true')
											   {
										    	 if(obj[k]!="No Document")
												   {
										    		  if(obj[k]!="")
										    			  {
										    			   if(obj[k]!="No Data")
													    	 {								    				      							    				    
										    				      $('#contentDiv').append("<p><input  name=\"patternType" + k + "\" id=\"patternType" + k + "\" type=\"checkbox\" value=\""+obj[k]+"\" onClick=\"addNewListItem(this,"+k+")\"  />" + obj[k].split("_")[0] + "</p>");
													    	 }							    			    	
										    			  }								          							   								    
												    }
											   }
										}	
										
										
										if(docExist == 'true')
											{
												
											    $('#contentDiv').append("You have not uploaded any documents!");
											}
										
										var values = ""; 
										
										var patternValue = document.getElementById("newPattern").value;										
																						
										$("#viewDocId").html(values);						
										
										/* Script for view already selected type in right side div ! */
										
										viewValue = viewValue.substring(0, viewValue.length-1);		
										var viewValueAry = viewValue.split(",");
										
										$("#viewDocId").append("<ul>");
										
										for(var v=0;v<viewValueAry.length;v++)
											{
											   var viewData = viewValueAry[v];				   
											   var checkType = viewData.split("--")[0];
											   
												if(checkType=="KYC")
												   {													   
													   $("#viewDocId").append("<li>"+viewValueAry[v].split(" -")[0]+"</li>");
												   }
												else
													{
													    $("#viewDocId").append("<li>"+viewValueAry[v].split("_")[0]+"</li>");
													}											       
											}
										
										$("#viewDocId").append("</ul>");
										
										/* EOS for view already selected type in right side div ! */							    	 
					    	 }
			        }
			},			
		});	   
	
      }
else
	{		    
		$('#headingSpan').empty();
		$('#headingSpan').html("<h1 class=\"headingh1Span\"><span>Select documents by type</span></h1>");
		$('#contentDiv').html("Select documents to be shared.");
	}
  
	}
	
	
function addNewListItem(id,index)
{			
	var type = document.getElementById("labelname").value;
	
	
	if(type=='KYC_DOCUMENT')
	    {	
		  	 var indexValue = "patternType"+index; 
		
		  	 var selectValue = document.getElementById(indexValue).value;			
		  
		  	 var values = "";
		  	 var checkValue = type.split("_")[0]+"--"+selectValue;
		     
		  	if(id.checked == true)			
		    {  	  			  			  
				  values += "<li>";						  
				  values += checkValue.split(" -")[0]; 
				  values += "</li>";	
				  
				  document.getElementById("newPattern").value += checkValue + ","; 	
				  $("#viewDocId").append(values);
				  
				  if(document.getElementById("newPattern").value!="")
					  {
					    //$("#recipentDetailsDivId").show();
					    $("#chooseButtonDiv").show();
					  }
				  else
					  {
					    $("#chooseButtonDiv").hide();
					    //$("#recipentDetailsDivId").hide();
					  }
			} 
		  	
		  	
		  	if(id.checked == false)			
		    {  							
				var patternValue = document.getElementById("newPattern").value;				
				var patArray1 = patternValue.split(",");
				var tempPattern = "";
				
				for(var i=0;i<patArray1.length;i++)
					{						   
					    if(checkValue!=patArray1[i])
					    	{				    	   
					    	   tempPattern += patArray1[i]+",";				    	  
					    	}				   
					}
				
				tempPattern = tempPattern.substring(0,tempPattern.length-1);
				
				var viewArray1 = tempPattern.split(",");
				
				for(var v=0,j=1;v<viewArray1.length;v++,j++)
					{
					   var viewData = viewArray1[v];				   
					   var checkType = viewData.split("--")[0];
					   if(checkType=="KYC")
						   {
							   values += "<li>";					  
							   values += viewArray1[v].split(" -")[0]; 
							   values += "</li>";
						   }
					   else						   
						   {
							   values += "<li>";					  
							   values += viewArray1[v].split("_")[0]; 
							   values += "</li>";
						   }				       	
					}
				
				
				document.getElementById("newPattern").value = "";
				document.getElementById("newPattern").value = tempPattern;	// After deselect  remove the elements from the hidden value !		
				
				$("#viewDocId").html("");
				$("#viewDocId").append(values);
				
				 if(document.getElementById("newPattern").value!="")
				  {
					 //$("#recipentDetailsDivId").show();
				     $("#chooseButtonDiv").show();
				  }
				 
			    else
				  {
			    	 $("#chooseButtonDiv").hide();
			    	 //$("#recipentDetailsDivId").hide();
				  }
			} 
		  	
	    }
	else
		{		    
				var indexValue = "patternType"+index; 
				
				var selectValue = document.getElementById(indexValue).value;			
				  
				var values = "";
				var checkValue = type.split("_")[0]+"--"+selectValue;
				
				if(id.checked == true)			
			    {  	  			  			  
					  values += "<li>";					  
					  values += checkValue.split("_")[0]; 
					  values += "</li>";	
					  document.getElementById("newPattern").value += checkValue + ","; 	
					  $("#viewDocId").append(values);
					  
					  if(document.getElementById("newPattern").value!="")
						  {
						    //$("#recipentDetailsDivId").show();
						    $("#chooseButtonDiv").show();
						  }
					  else
						  {
						    $("#chooseButtonDiv").hide();
						    //$("#recipentDetailsDivId").hide();
						  }
				} 
				
				if(id.checked == false)			
			    {  							
					var patternValue = document.getElementById("newPattern").value;				
					var patArray1 = patternValue.split(",");
					var tempPattern = "";
					
					for(var i=0;i<patArray1.length;i++)
						{				    
						    if(checkValue!=patArray1[i])
						    	{				    	   
						    	   tempPattern += patArray1[i]+",";				    	  
						    	}				   
						}
					
					tempPattern = tempPattern.substring(0,tempPattern.length-1);
					
					var viewArray1 = tempPattern.split(",");
					for(var v=0,j=1;v<viewArray1.length;v++,j++)
					{
					   var viewData = viewArray1[v];				   
					   var checkType = viewData.split("--")[0];
					   if(checkType=="KYC")
						   {
							   values += "<li>";					  
							   values += viewArray1[v].split(" -")[0]; 
							   values += "</li>";
						   }
					   else						   
						   {
							   values += "<li>";					  
							   values += viewArray1[v].split("_")[0]; 
							   values += "</li>";
						   }				       	
					}
					
					document.getElementById("newPattern").value = "";
					document.getElementById("newPattern").value = tempPattern;	// After deselect  remove the elements from the hidden value !		
					
					$("#viewDocId").html("");
					$("#viewDocId").append(values);
					
					 if(document.getElementById("newPattern").value!="")
					  {
						 //$("#recipentDetailsDivId").show();
					     $("#chooseButtonDiv").show();
					  }
				    else
					  {
				    	 $("#chooseButtonDiv").hide();
				    	 //$("#recipentDetailsDivId").hide();
					  }
				} 						
		}

	
}
		
		
 function gen_code() {
				
	var codeValue = document.getElementById("newPattern").value;			
		
	if(codeValue=="")
		{
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please choose some documents before share ! <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
		}
	else
		{			   
		    // window.location = "recipient_details.html?newPatternName="+codeValue;		
		    
	        $.post( "recipient_details_indirect.html", { newPatternName: codeValue, time: "2pm" })
	          .done(function( data ) {

                  $("#viewUsers1").html(data);
                
	          });

	    }
}  


</script>
  
 <!-- End of Script for Sharing Manager -->
 
 
 
 
 
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
 
 
 <!-- Start Script for Recipent Details -->
 
 <script type="text/javascript">
 

 function lightbox_open(){
	    window.scrollTo(0,0);
	    document.getElementById('light').style.display='block';
	    document.getElementById('fade').style.display='block'; 
	}

	function lightbox_close(){
		
		$("#allowAccessPopLight").hide();    
	    document.getElementById('light').style.display='none';
	    document.getElementById('fade').style.display='none';	    
	    $("#successcode").hide();
	    
	}
	
 
 function shareDoc(urlLink)
 {
 	
 	$.ajax
     ({
 		type : "post",				
 		url : urlLink,						
 		success : function(response) {
 									
 			 $("#viewProfile")
 				.html(response);
 							
 		},
 		error : function() {
 			alert('Error while fetching response');
 		}
 	});		 		 		 		
          
 	}

 function gen_rec_details() {
 	
 		var id1 = "";		
 		var id2 = $("#name").val();	
 	
 		var emailCheck = document.getElementById("searchTypeEmail");
 		var nameCheck = document.getElementById("searchTypeName");
 		
 		if(emailCheck.checked)
 			{
 			     id1 = "Email_id"
 			}
 		if(nameCheck.checked)
			{
 			    id1 = "Name";
			}
 		
 	if(id2.length<3)
 		{
 		      $(".topstyle").hide();
 		      $('#searchDetails').hide();
 		      document.getElementById('waitlight').style.display='none';
		      document.getElementById('waitfade').style.display='none';		        
 		}		
 	else
 		{   	  
 		
 		    document.getElementById('waitlight').style.display='block';
	        document.getElementById('waitfade').style.display='block';
	        
 		
 		      $.ajax
 			          ({
 						type : "post",				
 						url : "generateRecipient.html",			
 						data :"searchby="+id1+"&name="+id2 ,	  
 						success : function(response) {
	 							
	 							document.getElementById('waitlight').style.display='none';
	 	 				        document.getElementById('waitfade').style.display='none';
	 	 				        
 	 				        
 								$('#searchDetails').empty();
 								$('#searchDetails').show();
 								$(".topstyle").show();
 								$('#searchDetails').append(response); 	
 						
 							},																	
 						error : function() {
 							
 							document.getElementById('waitlight').style.display='none';
 	 				        document.getElementById('waitfade').style.display='none';
 	 				        
 	 				        
 							   $(".topstyle").hide();
 							   $('#searchDetails').empty();
 							   $('#searchDetails').show();
 							   $('#searchDetails').html("<h>Sorry, no results found for this search !</h>"); 
 						}
 					});		
 				 
 				   $("#titleBar").html("<h2><span>Individual Search Result</span></h2>");
    }
 }  


 function forKycUser()
 {
 	$("#nonKycUserDiv").hide();
 	//$("#searchDetails").show();
 	
 	$("#ModifyDocuments").show();
 	
 	document.getElementById("generatecodesearch_icon").style.display = "block";	
 	
 	document.getElementById("nonclensUser").style.background = "#ebebeb";
	document.getElementById("nonclensUser").style.color = "#4a484b";
	
	document.getElementById("clensUser").style.background = "#00b6f5";
	document.getElementById("clensUser").style.color = "#fff";
	
 }

 function fornonKycUser()
 {
	 $(".topstyle").hide();
	 
	 document.getElementById("nonclensUser").style.background = "#00b6f5";
     document.getElementById("nonclensUser").style.color = "#fff";
		
	 document.getElementById("clensUser").style.background = "#ebebeb";
	 document.getElementById("clensUser").style.color = "#4a484b";
		
	 
 	$("#generatecodesearch_icon").hide();
 	$("#searchDetails").hide();
 	
 	$("#ModifyDocuments").show();
 	
 	document.getElementById("kycnameId").value = "";
 	document.getElementById("kycuserId").value = "";
 	document.getElementById("kycPhnoId").value = "";
 	document.getElementById("CommentshareId").value = "";
 	
 	$("#nonKycName").hide();
 	
 	var codeDetails = document.getElementById("newPattern").value;
 	document.getElementById("nonkycUserCodeValueId").value = codeDetails;
 	var codeArray = codeDetails.split(",");
 	
 	var divValue = "";
 	divValue += "<ul style=\"text-align:left;\">";
 
 	/* 
 	for(var i=0;i<codeArray.length;i++)
 		{
 		divValue += "<li>"+codeArray[i].split("_")[0]+"</li>";	
 		} */
 	
 	
 	for(var i=0;i<codeArray.length;i++)
	{
	   var viewData = codeArray[i];				   
	   var checkType = viewData.split("--")[0];
	   if(checkType=="KYC")
		   {
		   	  divValue += "<li>";					  
		      divValue += codeArray[i].split(" -")[0]; 
		      divValue += "</li>";
		   }
	   else						   
		   {
		      divValue += "<li>";					  
		      divValue += codeArray[i].split("_")[0]; 
		      divValue += "</li>";
		   }				       	
	}
 	
 	divValue += "</ul>";
 	
 	$("#sharecodeId").html(divValue);
 	
    $("#nonKycUserDiv").show();
}

 function shareNonKycOnKeyUp()
 {  
 	var name = /^[a-zA-Z ]+$/;	
 	var num = /^[0-9]+$/;	
 	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
 		
 	var SharedName = document.getElementById("kycnameId").value;
 	var SharedEmailId = document.getElementById("kycuserId").value;
 	var SharedPhno = document.getElementById("kycPhnoId").value;
 		  	
 	if(SharedName=="")
 		{
 		   $("#nonKycName").hide();
 		}
 	
 	if(SharedName!="")
 	{
 		if(!SharedName.match(name))
 		{
 			$("#nonKycName").show();
 			$("#nonKycName").html("Enter only alphabet for name.");
 			return false;
 		}
 		else
 		{
 			$("#nonKycName").hide();	
 		}
 	}
 	
 	if(SharedEmailId=="")
 	{
 	   $("#nonKycName").hide();
 	}
 	
 	if(SharedEmailId!="")
 	{
 		$("#nonKycName").hide();												
 	}
 					
 	if(SharedPhno!="")
 	{
 		if(!SharedPhno.match(num))
 		{
 			$("#nonKycName").show();
 			$("#nonKycName").html("Enter valid mobile number.");
 			 return false;
 		}
 		else
 		{
 			$("#nonKycName").hide();	
 		}
 	}
 	
 	if(SharedPhno=="")
 	{
 		$("#nonKycName").hide();	
 	}
 }

 function checkNonClenseeEmailId(SharedEmailId)
 {
	 $.ajax
 	    ({
 			type : "post",				
 			url : "validateNonClenseeuserEmailId.html",	
 			data : "SharedEmailId="+SharedEmailId,
 			success : function(response) { 
 				
 						 	  	 				
 					if(response=="Exist")
 						{
 						
 							document.getElementById('waitlight').style.display='none';
 				        	document.getElementById('waitfade').style.display='none';
 				        
 							$("#nonKycName").show();
 							$("#nonKycName").html("Given email id is already a existed user.");	
 							 return false;
 						}
 			}
 	    });
 }
 
 function shareNonKyc()
 {	
 	var flag='true';  
 	var name = /^[a-zA-Z ]+$/;	
 	var num = /^[0-9]{10}$/;	
 	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
 		
 	var SharedName = document.getElementById("kycnameId").value;
 	var SharedEmailId = document.getElementById("kycuserId").value;
 	var SharedPhno = document.getElementById("kycPhnoId").value;
 		  	
 	if(SharedName=="")
 		{
 		   flag='false';
 		   $("#nonKycName").show();
 		   $("#nonKycName").html("Enter your sender name.");
 		   return false;
 		}
 	if(SharedName!="")
 	{
 		if(!SharedName.match(name))
 		{
 			flag='false';
 			$("#nonKycName").show();
 			$("#nonKycName").html("Enter only alphabet for name .");
 			return false;
 		}
 		else
 		{
 			$("#nonKycName").hide();	
 		}
 	}
 	
 	if(SharedEmailId=="")
 	{
 	   flag='false';
 	   $("#nonKycName").show();
 	   $("#nonKycName").html("Enter your sender email id.");
 	   return false;
 	}
 	
 	if(SharedEmailId!="")
 	{
 		if(!SharedEmailId.match(emailId))
 		{
 			flag='false';
 			$("#nonKycName").show();
 			$("#nonKycName").html("Enter a valid email id .");	
 			 return false;
 		}
 		else
 		{
 			$("#nonKycName").hide();												
 		}
 	}
 					
 	if(SharedPhno!="")
 	{
 		if(!SharedPhno.match(num))
 		{
 			flag='false';
 			$("#nonKycName").show();
 			$("#nonKycName").html("Enter a valid mobile number !");
 			 return false;
 		}
 		else
 		{
 			$("#nonKycName").hide();	
 		}
 	}
 	
 	if(flag=='true')
 	{
 		    document.getElementById('waitlight').style.display='block';
 	        document.getElementById('waitfade').style.display='block';
 	  $.ajax
 	 	({   
	 	    type : "post",				
	 	  	url : "validategenerateCode.html",	
	 	  	success : function(response) { 
	 	  		
 	  		if(response=="withinLimit")
 	  			{	
 	  			
		 	  			$.ajax
		 	  	 	    ({
		 	  	 			type : "post",				
		 	  	 			url : "validateNonClenseeuserEmailId.html",	
		 	  	 			data : "SharedEmailId="+SharedEmailId,
		 	  	 			success : function(response) { 
		 	  	 				
		 	  	 						 	  	 				
		 	  	 					if(response=="Exist")
		 	  	 						{
		 	  	 						
		 	  	 						document.getElementById('waitlight').style.display='none';
		 	  	 				        document.getElementById('waitfade').style.display='none';
		 	  	 				        
		 	  	 							$("#nonKycName").show();
		 	  	 							$("#nonKycName").html("Given email id is already a existed user.");	
		 	  	 							 return false;
		 	  	 						}
		 	  	 					
		 	  	 					else
		 	  	 						
		 	  	 						{					
		 	  	 							var formData = $('#nonKycUserFormId').serialize();	
		 	  	 							
		 	  	 							$.ajax
		 	  	 						    ({
		 	  	 								type : "post",				
		 	  	 								url : "sendNonKycUser.html",	
		 	  	 								data : formData,
		 	  	 								success : function(response) { 
		 	  	 									
		 	  	 									if(response=="success")
		 	  	 										{											
		 	  	 											 document.getElementById('waitlight').style.display='none';
		 	  	 								       		 document.getElementById('waitfade').style.display='none'; 								        								        
		 	  	 										   
		 	  	 										     document.getElementById('sociallight').style.display='block';
		 	  	 										     document.getElementById('socialfade').style.display='block';
		 	  	 										     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
		 	  	 										     $("#socialdisplaySharedDocDiv").html("Your documents has been sent sucessfully. ! <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\" /> </div>");  
		 	  	 									          										     
		 	  	 									    }
		 	  	 									else
		 	  	 										{
		 	  	 											 document.getElementById('waitlight').style.display='none';
		 	  	 									         document.getElementById('waitfade').style.display='none';
		 	  	 									         
		 	  	 									         document.getElementById('sociallight').style.display='block';
		 	  											     document.getElementById('socialfade').style.display='block';
		 	  											     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		 	  											     $("#socialdisplaySharedDocDiv").html("Server Low Try again Later ! <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  									         
		 	  	 										}
		 	  	 								},
		 	  	 								error : function() 
		 	  	 								{
		 	  	 								    document.getElementById('waitlight').style.display='none';
  	 									            document.getElementById('waitfade').style.display='none';
  	 									         
		 	  	 									alert('Server Problem ,Try Again Later!');
		 	  	 								}
		 	  	 							});		 	
		 	  	 						}
		 	  	 			 },				
		 	  	 		});	
		 	  	 			
 	  			}
 	  		else
 	  			{
 	  			     document.getElementById('waitlight').style.display='none';
			         document.getElementById('waitfade').style.display='none';
			         
			         document.getElementById('sociallight').style.display='block';
				     document.getElementById('socialfade').style.display='block';
				     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				     $("#socialdisplaySharedDocDiv").html("Limit is over for individul sharing as per your plan. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  									         
				
 	  			}
 	  	   },  
 	      
 	   });
 	        
 									
 	}
 	
 }

 function closePopUp()
 {	
 	$("#successabc").hide();
 }

 function div_show(kycid)
 { 
 	
 	$.ajax
     ({
 		type : "post",				
 		url : "sendingPattern.html",	
 		data: "kycid="+kycid,
 		success : function(response) {
 			
 			 window.scrollTo(0,0);
 			 
 			 document.getElementById('light').style.display='block';
 			 document.getElementById('fade').style.display='block'; 
 			    
 			var data = response.split("$");
 			
 			document.getElementById("sshareId").value="";
 			
 			$("#kycUserNameDiv").html("<h1>Sharing Documents to "+data[3]+"</h1>");
 			$("#SuccessUserNameDiv").html(data[3]);
 			$("#kycUserIdDiv").html(data[1]);
 			 			
 			var codeDetails = document.getElementById("newPattern").value;
 		 	document.getElementById("kycUserCodeValueId").value = codeDetails;
 		 	var codeArray = codeDetails.split(",");
 		 	
 		 	var divValue = "";
 		 	divValue += "<ul style=\"text-align:left;\">";
 		 			 
 		 	for(var i=0;i<codeArray.length;i++)
			{
			   var viewData = codeArray[i];				   
			   var checkType = viewData.split("--")[0];
			   if(checkType=="KYC")
				   {
				       divValue += "<li>";					  
				       divValue += codeArray[i].split(" -")[0]; 
				       divValue += "</li>";
				   }
			   else						   
				   {
				   	   divValue += "<li>";					  
				       divValue += codeArray[i].split("_")[0]; 
				       divValue += "</li>";
				   }				       	
			}
 		 	 	 	
 		 	divValue += "</ul>";
 		 	
 		 	$("#sharecodeId-ext").html(divValue);
 						
 			document.getElementById("senderId").value=data[0];
 			document.getElementById("reciverId").value=data[1];
 					  			  			 
 		},
 		error : function() 
 		{
 			alert('Error while fetching response');
 		}
 	});	
 		
 }
 	
 	
 function sendTo(urlLink)
 {	 	
 	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
     
     $("#light").hide();
     $("#fade").hide();
     
 	$.ajax
     ({
 		type : "post",				
 		url : "validategenerateCode.html",	
 		success : function(response) { 
 			
 			if(response=="withinLimit")
 				{	
 				
 				    var fd = $('#sendForm').serialize();
 				
 					$.ajax
 				    ({
 						type : "post",				
 						url : urlLink,	
 						data : fd,
 						success : function(response) { 
 							
 							if(response=="success")
 								{	
 								    
 									document.getElementById('waitlight').style.display='none';
 							    	document.getElementById('waitfade').style.display='none';
 							    								   
 								     // $("#successcode").show();
 								    
 								     document.getElementById('sociallight').style.display='block';
								     document.getElementById('socialfade').style.display='block';
								     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
								     $("#socialdisplaySharedDocDiv").html("Your documents has been sent sucessfully. <div style=\"width:75%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" style=\"margin-left:100px;\" value=\"OK\" onclick=\"fadeoutReport('success')\" /> <input type=\"button\" style=\"width:80px; ! important;\" class=\"sendmoreButton\" value=\"Send More\" onclick=\"fadeoutReport('warning')\" />  </div>");  									         
								     
 							    }
 							else
 								{
	 								 document.getElementById('sociallight').style.display='block';
	 							     document.getElementById('socialfade').style.display='block';
	 							     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
	 							     $("#socialdisplaySharedDocDiv").html("Server Low Please Try Agin Later !  <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  									         	 			 				  
 								}
 						},						
 					});	
 				}
 			else
 				{
 				    document.getElementById('waitlight').style.display='none';
			     	document.getElementById('waitfade').style.display='none';
			    	
			    	
	 				 document.getElementById('sociallight').style.display='block';
				     document.getElementById('socialfade').style.display='block';
				     
				     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				     $("#socialdisplaySharedDocDiv").html("Sharing documents has beyound limit as per plan,to share more documents migrate your plan ! <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  									         				  				     
 				}
 		},
     })
 	
 	
 }	

 
 
 
/* Main Header Actions  */
 
 function gotoProfile()
 {
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoMyDocument()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoSharingManager()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }
 
 function gotoStore()
 {	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
 }
 
 function gotoAlerts()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
 }
 
 function gotoPlans()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
				
			}
		}); 		 
 }
 
 function gotoSetings()
 {
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
 }


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}


function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}
 
 
function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}


function showSharingDocDiv(action)
{
	
	
	if(action=="down")
		{
		    $("#sharingdoc").slideDown("slow");
		    $("#recipentDetailsDivId").hide();
		    $("#ModifyDocuments").hide();
		    
		    document.getElementById("nonclensUser").style.background = "#ebebeb";
			document.getElementById("nonclensUser").style.color = "#4a484b";
			
			document.getElementById("clensUser").style.background = "#ebebeb";
			document.getElementById("clensUser").style.color = "#4a484b";
			
		}
	if(action=="up")
		{
		    $("#sharingdoc").slideUp("slow");
		    $("#recipentDetailsDivId").show();
		    showSearchBar('Name');
		}
}


function showSearchBar(parameter)
{
    $("#generatecodesearch").show();	
    document.getElementById("name").value = "";
    document.getElementById("name").placeholder = "Search individual by "+parameter;
    $('.topstyle').hide();
    $("#searchDetails").hide(); 
}


 </script>

 
 <!-- End Of Script for Recipent Details -->
 
</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocuments"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanagerfocus" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- 
					
					<li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li>
					
					-->
					
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
           
           
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
			
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
					
		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
								  <ul>
								      <li id="generatcodeicons"><a href="generateCode.html" id="Hi-B-1" class="generatcodeiconsactive" ><span> Individual Sharing</span></a></li>
									  <li id="applycodeicon"><a href="applyCode.html" id="Hi-B-2" ><span>Documents Received</span></a></li>
									  <li id="allowaccessicon"><a href="sharingtoUti.html" id="Hi-B-3" ><span> Utilizer Sharing</span></a></li>
									  <li id="revokecodeicon"><a href="#" id="Hi-B-4" onclick="revokeAccess('revokeAccess.html')"><span>Revoke Sharing </span></a></li>
									  <li id="sharingdocumenthisticon"><a href="#" id="Hi-B-5" onclick="accessHistory('accessHistory.html')"><span>Sharing History </span></a></li>
								 </ul>
							</div>     		
					</div>
					
					 </div>
					
					<div class="rightside">
					
					
					<!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

									

					<div id="ProfileHeadingDiv" style="display: block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>

					
							        		
					   <!-- Advertisment Div for Entire Page  -->
					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span>Sharing Documents</span>								   
							</h2>
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
										
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
									
					
					<div id="sharingdoc">
						
								<div class="sharingselectitems">
									<ul>
										<li>
										<select name="doctype" id="labelname" onchange="showLatestDoc(this.value)">
												<option value="Select your Document">Select documents by type</option>
												<option value="KYC_DOCUMENT">Kyc Documents</option>
												<option value="ACADEMIC_DOC">Academic Documents</option>
												<option value="Financial_DOC">Financial Documents</option>
												<option value="Employment_DOC">Employment Documents</option>						
										</select>
										</li>
									</ul>
								</div>		
								
								<div style="width:100%;">
																
											<div id="viewUsers" class="left">		
												
												<div id="headingSpan">
												<h1 class="headingh1Span"><span>Select documents by type</span></h1>	
												</div>
												
												<div id="contentDiv" class="documetlist_share" >Select documents to be shared.</div>
														
											</div>
						
										
											<div  class="right" >
												
												<h1><span>Selected Documents</span></h1>						
												
												<div id="viewDocId" class="documetlist_share">
												
														
												</div>
																		
											</div>
											
											   
										
								</div>
								        <div class="sharingbutton">
								   			
								   		      <form action="recipientdetailsforSharing.html" method="get">
								   		      
											      <input type="hidden" id="newPattern" name="newPatternName"  value=""/>
											    
											      <input align="right" id="nextButtonId" style="display:none;" type="submit" name="Next" value="Next" />
						                      
						                      </form>
						                    
									    </div>
										  									        			                
					</div>
						
					
					
						
									
			 <!-- Div for RecipentDetails -->
										
												           						          						
						         <div id="chooseButtonDiv" style="display:none;">
									 
									 <div id="ModifyDocuments" class="revoke_tbutton" style="display:none;margin-top: 20px;">
									 
									       <input type="button" value="Modify Documents" onclick="showSharingDocDiv('down')"/>
									       
									 </div>
									 									 
									<div id="revoke_tbutton">
									 
									     <h1>Share Documents To</h1>
									 
									     <input type="button" id="clensUser" value=" Clensee User" onclick="forKycUser(),showSharingDocDiv('up')"/>  
									     
									     <input type="button" id="nonclensUser"  value="Non Clensee User" onclick="fornonKycUser(),showSharingDocDiv('up')"/>
									     
									 </div>  
									 
							 
							     </div>
					
					
									 
				<div id="recipentDetailsDivId" >
							   
								<div id="generatecodesearch_icon" style="display:none;">
										
										
										<div id="searchViaParameter">
									      
									       <ul>
									           
									              <li>
									                   Search Individual by
									                   
									                   <INPUT id="searchTypeName" checked="true" TYPE="radio" NAME="mobileno" onclick="showSearchBar('Name')">Name
									                   <INPUT id="searchTypeEmail"  TYPE="radio" NAME="mobileno" onclick="showSearchBar('Email Id')">Email Id
									                   
									              </li>
									       </ul>
									        
									    </div> 
												
										<div id="generatecodesearch" style="display:none;margin-left: 81px;">
												<input  type="text" class="generatecodesearch" placeholder="Enter atleast 3 character" title="Enter atleast 3 character for efficent search" name="name" id="name" onkeyup="gen_rec_details()">
												<input type="image" onClick="gen_rec_details()" src="resources/images/home_search_img.png">
										</div>
										
										
										
								</div>
							
							    
							    <span class="topstyle" style="display:none;"></span>
							    <div id="searchDetails" style="display:none;" class="sharingpagescroll">
									
									
							     </div>
							
							
								<div id="viewProfile"></div>
							
								<input type="hidden" name="pattern2" value=""/>
										
									
										
										
								
							                    
							<div id="light">
							<div class="colsebutton" onclick="lightbox_close()"><img height="22" width="22" src="resources/images/close_button.png"></div>
							<form id="sendForm" name="patternvalue" action="codeDetails.html" method="post">
														
														
														<div id="kycusername_share">
															<div id="kycUserNameDiv"></div>
														<ul>	
														<li>			
														<input readonly type="hidden" name="sender" value="" id="senderId"/>
														<input readonly type="hidden" name="reciver" 	value="" id="reciverId"/>
														
							 							</li>
														
														<li >					
														     <textarea name="des" id="sshareId" placeholder="Sharing Comments" maxlength="500"></textarea>
														</li>
														
														<li>						
														
										  		        <div id="sharecodeId-ext" >
												  		        
												  		        
										  		        </div>			      
										  		     
										  		        <input type="hidden" id="kycUserCodeValueId" value=""  name="codePattern"/>
														</li>	
																	
														<li id="sharecodeIdbutton">
														    <input type="button" value="Send" onclick="sendTo('shareDoctoClenseeUser.html')" />
														    <input type="button" value="Cancel" onClick="lightbox_close();" />							
														</li>
														
														</ul>		
														</div>	
														
													</form>
							
							
							          </div>
							
							 <div id="fade" onClick="lightbox_close();"> </div> 	
																					
							<!-- Division for Non Kyc User -->
										
								<div id="nonKycUserDiv" style="display:none;">		
											
										<div class="nonkycuser">
										
										   <h1 align="left">Sharing documents to a non clensee user</h1>  <br>
										<div class="sharingnon_clensee">
										     <form id="nonKycUserFormId" name="nonKycUserForm" action="sendNonKycUser.html" method="post">
										  
										   <ul>
										       
										  		<li style="float:left;">
										  		     Name<span>*</span><input type="text" name="nonKycName" id="kycnameId" maxlength="30" style="
	border: 1px solid #777;
    border-radius: 5px;
    margin-left: 5px;
  
    width: 150px;" onkeyup="shareNonKycOnKeyUp()"/><div id="nonKycName" class="error" style="display:none;margin-left:280px;margin-top: -58px"></div>
										  		     &nbsp; &nbsp;Email id<span>*</span> <input type="text" maxlength="30" name="nonKycUser" id="kycuserId" style=" border: 1px solid #777;
    border-radius: 5px;
    margin-left: 5px;
   
    width: 150px;" onkeyup="shareNonKycOnKeyUp()" onchange="checkNonClenseeEmailId(this.value)"/> 
										  		     &nbsp; &nbsp;Mobile no<span></span><input type="text" name="nonKycPhno" maxlength="10" id="kycPhnoId" style=" border: 1px solid #777;
    border-radius: 5px;
    margin-left: 5px;
 
    width: 150px;" onkeyup="shareNonKycOnKeyUp()"/>		  		
										  		     <input type="hidden" name="shareSubject" id="shareId" value="Sharing KYC Documents !" />			  		     
										  		</li>
										  			  		   		  		
										  		<li>
										  		     <textarea name="shareComment" id="CommentshareId" placeholder="Sharing Comments" maxlength="500" style="">
										  		     
										  		     </textarea>
										  		     
										  		     
										  		     <li>
										  		     <div id="sharecodeId" >
											  		    
											  		   
										  		     </div>	
										  		     </li>											  												  		
										  		
										  </ul>
										  
										    <div class="sharingbuttonclass">
										  		
										  		     <input type="hidden" id="nonkycUserCodeValueId" value="" name="sahredCode"/>
							 			  		     <input type="button" value="Share"  style="margin-left:-104px;margin-top:10px;" onclick="shareNonKyc()" />
							 			  		     
										    </div>
										  
										</form>
										  </div>
										</div>			
										</div>
							            <!-- End of Division for Non Kyc User -->
							            
							            
							            
							               
							
							               
							                  <div id="successcode" style="display:none;"> 
																			      
													      <h1>Your documents has been successfully sent to </h1> <div id="SuccessUserNameDiv"></div><br>
													      <input type="button" value="OK" onclick="lightbox_close()" />
													     						
											  </div> 
											   
						                  												
						     </div>
						
						
						 <div style="text-align:left;margin-left:30px;margin-top:75px;color:dodgerblue;margin-bottom: 28px;">
							<p style="color:#7c7c7c">
								Here is the place where you can share your Kyc, Academic, Employment and Financial documents to other individuals in three simple steps.
								<br>
								    <br>1. Select Documents
								    <br>2. Search Individual
								    <br>3. Share
								    <br><br>
								Documents that are shared will be available for 24 hrs only for the recipient.
							</p>
					   </div>
						
						
						
						
								
						            <!-- End of DIv for Recipent Details -->
																		
								
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			       
			</div>	
			
	    </div>	<!-- End of -->	

 	
	    
</div> <!--end of right div  -->

	</div>

</div>
					
					
					
					
					
		<%
			ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			String version=resource.getString("kycversion");
			String versionDate=resource.getString("kyclatestDate");
			%>			
					
					
					
					
			
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">

							 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									

                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->
	
	
	
	
	
	
   <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->

   
</body>
</html>
     	