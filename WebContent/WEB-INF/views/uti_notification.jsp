<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Notification</title>

</head>
<body>


<c:if test="${!empty UtiNotification}">		                                                
	<table class='CSSTableGenerator' align="left" border="1" cellspacing="1" align="center">
		<tr> 
		    <th height="10">Serial No</th>			
			<th height="10">What</th> 
			<th height="10">When</th> 
			<th height="10">Type of Alert</th> 
			<th height="10">Things Updated</th> 
		</tr> 
        <%int i=0; %>
		<c:forEach items="${UtiNotification}" var="det">					 
			 <tr>
			    <td height="10" width="120"><c:out value="<%=++i%>"/></td>				
				<td width="230"><c:out value="${det.uti_update_details}"/></td>
				<td width="270"><c:out value="${det.uti_update_datetime}"/></td>
				<td width="270"><c:out value="${det.type_of_alert}"/></td>
				<td width="270"><c:out value="${det.things_updated}"/></td>							
			 </tr>			
		</c:forEach>		
	</table>	
</c:if>


</body>
</html>