<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Insert title here</title>
</head>
<body>
<div id='activityContent'>
	 <table style="width:100%;" border="0" >
			   
			
	 </table>
	
	<c:if test="${!empty utilizerActivity}">
	<%int i=0;%>
	<div class="table" style="width:100%;height:155px;" >
	      
	       <ul>
		    <li  class="title">Sl.No</li>	
		    <c:forEach items="${utilizerActivity}" var="det">				   			   
			  		<li class="even"><%=++i%></li>    								
			</c:forEach>				    				    
		   </ul>
		   
		    <ul>			
			<li class="title">Activity Details</li>
			<c:forEach items="${utilizerActivity}" var="det">						    			
			<li style='width: 200px' class="even"><c:out value="${det.activity_details}"/></li>											   	
            </c:forEach> 
			</ul>
			
			<ul>			
			<li class="title">Activity Time</li>										
			<c:forEach items="${utilizerActivity}" var="det">				  
			<li style='width: 180px'class="even"><c:out value="${det.activity_date}"/></li>								
            </c:forEach> 
			</ul>
			
														
   </div>
   </c:if>
   
   
   <c:if test="${empty utilizerActivity}">
						<div class="notice" style="margin-left: 245px;margin: 72px auto 0;width: 250px;">
						 <b>NOTICE :</b>
						    No Activity is done till now !
						</div>	        
	</c:if>
	
	</div>
</body>
</html>