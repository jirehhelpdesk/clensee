<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<script type="text/javascript">

function empHistory()
{
	var backval = document.getElementById("backId").value;
	
	window.location = "employement_history.html?cmyName="+backval;
	
}


function viewDocumentInPopUpfromHistory(fileName)
{
	$("#waitingdivId").show();
	
	$.ajax({
		type : "get",				
		url : "view/Employee/Doc.html",
		data: "fileName="+fileName,
		success : function(response) {
		
			$("#waitingdivId").hide();
			
			document.getElementById('kycDocPopUplight').style.display='block';
	        document.getElementById('kycDocPopUpfade').style.display='block';
	        $("#kycDocPopUpDisplay").html(response);
	        			
		},		
	});		 		 		 		
	
}

</script>

</head>
<body >

<br> <br> 

   <% String empDetails = (String)request.getAttribute("employeeDetails"); 
      
      System.out.println("Emp Details String "+empDetails);
   
     String data[] =  empDetails.split("@@@");
     
     String empdata[] = data[0].split("&&");
     
     String empdocname[] = data[1].split(",");
     
     
   %>
   
   <input type="hidden" value="<%=empdata[0]%>" id="backId"/>
   
      <div align="center" class="employeedetails" >
        
      <div id="viewempdeatls">
          <table >

            <tr>
            <td>
                    <b>Organization Name </b></td>
                    <td>:</td>
                    <td>
                    <c:out value="<%=empdata[0]%>"></c:out></td>
            </tr>
            
            <tr>
              <td>
            <b>Job Designation </b></td>
              <td>
            :</td>
              <td>
              <c:out value="<%=empdata[1]%>"></c:out></td>
            </tr>

            <tr class="profiletextscroll" >
              <td>
            <b>Job Profile </b></td>
              <td>:</td>
                <td>
              <c:out value="<%=empdata[2]%>"></c:out></td>
            </tr>

            <tr>
            <td>
            <b>Job Duration </b></td>
            <td>:</td>
            <td>
             <b>form</b><c:out value="<%=empdata[3]%>"></c:out> <b>to</b> <c:out value="<%=empdata[4]%>"></c:out>
             </td>
            </tr>
         
         </table>
        </div> 
          
       <div id="ImageSlider" class="kycemployeedetailimg"> 
       <div class="employeedocumentdetails">
       <span></span>
          <h1>Employment Documents </h1> 
          </div>
          <div class="employmentdocument">
          <div  style="float:left; position: relative;">
                     
					 <div id="ShowOfferDiv" style="display:block;">
					 <ul>
					 <li>
					 <b>Offer Letter:</b>
					 
					 <%if(!empdocname[0].equals("No Document")){ %>
					 
					 		<%if(!empdocname[0].contains("jpeg") && !empdocname[0].contains("jpg") && !empdocname[0].contains("png") && !empdocname[0].contains("gif")) {%>
					 		
						 		  <%if(empdocname[0].contains("pdf")) {%>
																																										                        
		                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
		                            		
		                        <%}else if(empdocname[0].contains("doc")) {%>
		                        
		                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
		                        
		                        <%}else if(empdocname[0].contains("docx")) {%>
		                        
		                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
		                        
		                        <%}else if(empdocname[0].contains("xls")) {%>
		                        
		                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
		                         
		                        <%}else { %>
	                        
	                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
	                        
	                           <%} %>		
							 
							 <br>							 	         
					         <a href="download/Employee/Doc.html?fileName=<%=empdocname[0]%>">Download</a>
					         
					 		
					 		<%} else {%>
					 		
					 		<img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[0]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
							 
							 <br>
							 <a  onclick="viewDocumentInPopUpfromHistory('<%=empdocname[0]%>')">View</a>		         
					         <a href="download/Employee/Doc.html?fileName=<%=empdocname[0]%>">Download</a>
					         
					 		<%} %>
					 		
					
					 <%} else{%>
					 
					 <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
					 
					 
					 <%} %>		
							 
					 		
							 
					 </li>		
					 </ul>
					 </div>
		 </div>
		 
		 <div  style="float:left; position: relative;">
		 
					<div id="ShowHikeDiv" style="display:block;"> 
					<ul>
					
					<li>
					<b>Hike Letter:</b>
					
					<%if(!empdocname[1].equals("No Document")){ %>
					
					     <%if(!empdocname[1].contains("jpeg") && !empdocname[1].contains("jpg") && !empdocname[1].contains("png") && !empdocname[1].contains("gif")) {%>
					 		
					 		   <%if(empdocname[1].contains("pdf")) {%>
																																										                        
		                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
		                            		
		                        <%}else if(empdocname[1].contains("doc")) {%>
		                        
		                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
		                        
		                        <%}else if(empdocname[1].contains("docx")) {%>
		                        
		                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
		                        
		                        <%}else if(empdocname[1].contains("xls")) {%>
		                        
		                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
		                         
		                        <%}else { %>
	                        
	                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
	                        
	                           <%} %>		
							 
							 <br>							 	         
					         <a href="download/Employee/Doc.html?fileName=<%=empdocname[1]%>">Download</a>
					         					 		
					 		<%} else {%>
					 		
					         <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[1]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
					         
					        <br>
					        
					         <a  onclick="viewDocumentInPopUpfromHistory('<%=empdocname[1]%>')">View</a>
			         
			                 <a href="download/Employee/Doc.html?fileName=<%=empdocname[1]%>">Download</a>	
			                  
			                  <%} %>
			                  
			         <%} else {%>
			         
			                 <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
			                 
			         <%} %>
			           
					</li>		
					</ul>
					</div>
        </div>
		 
		 <div  style="float:left; position: relative;">
		  
					<div id="ShowExpDiv" >
					<ul>
					
					<li>
					<b>Experience Letter :</b>
					     <%if(!empdocname[2].equals("No Document")){ %>
					     
					     		<%if(!empdocname[2].contains("jpeg") && !empdocname[2].contains("jpg") && !empdocname[2].contains("png") && !empdocname[2].contains("gif")) {%>
					 		
							 		   <%if(empdocname[2].contains("pdf")) {%>
																																												                        
				                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
				                            		
				                        <%}else if(empdocname[2].contains("doc")) {%>
				                        
				                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
				                        
				                        <%}else if(empdocname[2].contains("docx")) {%>
				                        
				                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
				                        
				                        <%}else if(empdocname[2].contains("xls")) {%>
				                        
				                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
				                         
				                        <%}else { %>
			                        
			                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
			                        
			                           <%} %>		
							 
							 		 <br>							 	         
					                 <a href="download/Employee/Doc.html?fileName=<%=empdocname[2]%>">Download</a>
					         					 		
					 		  <%} else {%>
					     		
					                <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[2]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
					         
					                <br><a  onclick="viewDocumentInPopUpfromHistory('<%=empdocname[2]%>')">View</a>
			                         <a href="download/Employee/Doc.html?fileName=<%=empdocname[2]%>">Download</a>
			                     
			                     <%} %>
			                       
			              <%} else{%>
			              
			                      <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
			                      
			               <%} %> 
					</li>					
					</ul>
					</div>		
		</div>
		 
		<div  style="float:left; position: relative;">		 
					<div id="ShowRelDiv" >
					<ul> 
					
					 <li>
					 <b>Reliving Letter:</b>
					 <%if(!empdocname[3].equals("No Document")){ %>
					 
					          <%if(!empdocname[3].contains("jpeg") && !empdocname[3].contains("jpg") && !empdocname[3].contains("png") && !empdocname[3].contains("gif")) {%>
					 		
					 		 		  <%if(empdocname[3].contains("pdf")) {%>
																																												                        
				                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
				                            		
				                        <%}else if(empdocname[3].contains("doc")) {%>
				                        
				                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
				                        
				                        <%}else if(empdocname[3].contains("docx")) {%>
				                        
				                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
				                        
				                        <%}else if(empdocname[3].contains("xls")) {%>
				                        
				                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
				                         
				                        <%}else { %>
			                        
			                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
			                        
			                           <%} %>		
							 		 <br>							 	         
					                 <a href="download/Employee/Doc.html?fileName=<%=empdocname[3]%>">Download</a>
					         					 		
					 		  <%} else {%>
					 		  
					 		       <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[3]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
					          
						           <br><a  onclick="viewDocumentInPopUpfromHistory('<%=empdocname[3]%>')">View</a>
				                       <a href="download/Employee/Doc.html?fileName=<%=empdocname[3]%>">Download</a>
				                     
					 		  <%} %>
					 		  
					            
			          <%} else{%>
			          
			                   <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
			                   
			           <%} %> 
			           
					</li>		
					</ul>
					</div>
		</div>
		
		<div  style="float:left; position: relative;">							
					<div id="ShowFormDiv" >
					<ul>
					
					 <li>
					 <b>Form 16:</b>
					 <%if(!empdocname[4].equals("No Document")){ %>
					 
					           <%if(!empdocname[4].contains("jpeg") && !empdocname[4].contains("jpg") && !empdocname[4].contains("png") && !empdocname[4].contains("gif")) {%>
					 		
					 		 		 <%if(empdocname[4].contains("pdf")) {%>
																																												                        
				                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
				                            		
				                        <%}else if(empdocname[4].contains("doc")) {%>
				                        
				                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
				                        
				                        <%}else if(empdocname[4].contains("docx")) {%>
				                        
				                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
				                        
				                        <%}else if(empdocname[4].contains("xls")) {%>
				                        
				                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
				                         
				                        <%}else { %>
			                        
			                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
			                        
			                           <%} %>		
							 		 <br>							 	         
					                 <a href="download/Employee/Doc.html?fileName=<%=empdocname[4]%>">Download</a>
					         					 		
					 		  <%} else {%>
					 		  
					 		        <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[4]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
					           
						            <br><a  onclick="viewDocumentInPopUpfromHistory('<%=empdocname[4]%>')">View</a>
				                        <a href="download/Employee/Doc.html?fileName=<%=empdocname[4]%>">Download</a>
				                      					 		  
					 		  <%} %>
					            
			            <%} else{%>
			            
			                      <img src="resources/images/noDocumentFound.png" width="120" height="80"/>
			                      
			             <%} %>
			             
					 </li>					
					 </ul>
					 </div>
					 
		</div>
		
	
	 </div>
	 
		<div id="empsalaryslip">
		 <div class="employeedocumentdetails">
       <span></span>
		  <h1>SalarySlips :</h1> 
		  </div>
		  <div class="employmentdocument">
     <div id="PayslipSlider" class="epmloyeedocimgdet"> 
       	
         <%if(empdocname.length>5) { 
			  
				  for(int p=5;p<empdocname.length;p++)
		   			{
			      %>      
		
		     
         <div  style="float: left; position: relative;">
			      <ul>
			          
				      <li>
				      <%if(!empdocname[p].contains("jpeg") && !empdocname[p].contains("jpg") && !empdocname[p].contains("png") && !empdocname[p].contains("gif")) {%>
					 		
					 		 		  <%if(empdocname[p].contains("pdf")) {%>
																																												                        
				                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
				                            		
				                        <%}else if(empdocname[p].contains("doc")) {%>
				                        
				                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
				                        
				                        <%}else if(empdocname[p].contains("docx")) {%>
				                        
				                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
				                        
				                        <%}else if(empdocname[p].contains("xls")) {%>
				                        
				                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
				                         
				                        <%}else { %>
			                        
			                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
			                        
			                           <%} %>		
							 		 <br>							 	         
					                 <a href="download/Employee/Doc.html?fileName=<%=empdocname[p]%>">Download</a>
					         					 		
					 		  <%} else {%>
					 		  
					 		  
								      <%=empdocname[p].split("_")[0]%><br>
								      <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+empdocname[p]+"&docCategory=Employment_DOC"%>"   width="120" height="80" /></img>
								      
								      <br><a  onclick="viewDocumentInPopUpfromHistory('<%=empdocname[p]%>')">View</a>
								      <a href="download/Employee/Doc.html?fileName=<%=empdocname[p]%>">Download</a>
								       		
				       		  <%} %>
				       				      
				      </li>
			      </ul>
			       			
		 </div>
		 
		   <%}
			  
			  } else {%>
			  			  
			     <h1>Salary slips are not added in this record !</h1>
			      			       					  		  			  
			  <%} %>
		 
	  </div>
        </div>
        </div>
   
             <div align="right" style="height:52px;"><input type="button" value="Back to History" onclick="empHistory()" /></div>
  </div>                                        
</body>
</html>