<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<script>

function getPlanNameByCategory(category)
{
	
	if(category=="Individual")
		{
		    $("#planDomainSpanId").hide();
		    
			$.ajax({  
			    type : "Post",   
			    url : "get/plan/name/bycategory.html", 
			    data :"planCategory="+category,	     	     	     
			    success : function(response) 
			    {  		    		    		    	
			    	if(response!="No Data")
				   	   {
				   		   var planName = response.split(",");
				   		   var values = "";
				   		   values += "<option value=\"Select Plan Name\">Select Plan Name</option>";
				   		  	
				   		   for(var i=0;i<planName.length;i++)
				   			   {
				   			       values += "<option value=\""+planName[i]+"\">"+planName[i]+"</option>";
				   			   }	
				   		   
				   		   $("#planNameSpanId").show();	   		   		   		   
				   		   $("#plan_nameId").html(values);		   		   
				   	   }	
			    	else
			    		{	    		   
			    		  	 alert("For "+category+" no plans created !");   
			    		  	 $("#planNameSpanId").hide();	
			    		  	 $('#utilizerFeatureDivId').hide();
			    		}
			    },  		   
		    });  	
		}
	
	if(category=="Utilizer")
	{
		$("#planDomainSpanId").show();
	}
		
}

function getPlanDomainByCategory(domain_category)
{
	var planCategory = document.getElementById("plan_categoryId").value;
	var plandomain = document.getElementById("plan_domainId").value;
	
	$.ajax({  
	    type : "Post",   
	    url : "get/plan/name/bycategory4Uti.html", 
	    data :"planCategory="+planCategory+"&planDomain="+plandomain,	     	     	     
	    success : function(response) 
	    {  		    		    		    	
	    	if(response!="No Data")
		   	   {
		   		   var planName = response.split(",");
		   		   var values = "";
		   		   values += "<option value=\"Select Plan Name\">Select Plan Name</option>";
		   		   for(var i=0;i<planName.length;i++)
		   			   {
		   			       values += "<option value=\""+planName[i]+"\">"+planName[i]+"</option>";
		   			   }	
		   		   
		   		   $("#planNameSpanId").show();	   		   		   		   
		   		   $("#plan_nameId").html(values);		   		   
		   	   }	
	    	else
	    		{	    		   
	    		  	 alert("For "+planCategory+" no plans created !");   
	    		  	 $("#planNameSpanId").hide();	
	    		  	 $('#utilizerFeatureDivId').hide();
	    		}
	    },  		   
    });  	
}

function getPlandetails(selectedvalue)
  {
	
	var planCategory = document.getElementById("plan_categoryId").value;
	var plandomain = document.getElementById("plan_domainId").value;
		
	
		$.ajax({  
				    type : "Post",   
				    url : "selectedviewplan.html", 
				    data :"plan_name="+selectedvalue+"&planCategory="+planCategory+"&plandomain="+plandomain,	     	     	     
				    success : function(response) 
				    {  		
				    	 var result = response.split("#");
				    	 var planPrice = result[1];
					   	 var res=result[0].length; 	
					 	 var res1 = result[0].substring(1, res-1);	
					 	 var featuredetails = res1.split(", ");
					 	 
						 var values = "<table border=\"1\" style=\"width:800px;\"><tr><th bgcolor=\"#787878\">Feature Name</th><th bgcolor=\"#787878\">Feature Data</th><th bgcolor=\"#787878\">Feature Price</th></tr>";									
						 for (var i = 0; i <featuredetails.length; i = i + 3)								
						       {
									values +="<tr><td>"	+featuredetails[0 + i]	
									+ "</td><td>"+featuredetails[1 + i]	
									+"</td><td>"+featuredetails[2 + i]	
									+"</td></tr>";																			
					 	       }	
						 
						 values += "</table>";
						 $('#planPriceSpanId').show();
						 document.getElementById("plan_priceId").value = planPrice;
						 $('#utilizerFeatureDivId').html(values);				   	   	 
				   },  		   
		   });  						
  }
  
</script>

</head>

<body>

	<form name="showform" class="viewplanepage">
		
		<table align="center">
		  
		   <tr>			  
				<td style="width:100px;">
				<b>Plan Category</b>
				</td>
				<td style="width:10px;">:</td>
				<td style="width:200px;">
					<select name="Plan_name" id="plan_categoryId" onchange="getPlanNameByCategory(this.value)">
							<option value="Select Category">Select Category</option>						
							<option value="Individual">Individual</option>
							<option value="Utilizer">Utilizer</option>
							<option value="Corporate">Corporate</option>
					</select>
				</td>
			</tr>
			
			
			<tr id="planDomainSpanId" style="display:none;width:100%;">			  
				
				<td style="width:100px;">
				<b>Plan Domain</b>
				</td>
				
				<td style="width:10px;">:</td>
				
				<td style="width:200px;">
					<select name="Plan_Domain" id="plan_domainId" onchange="getPlanDomainByCategory(this.value)">
							 <option value="Select Domain">Select Domain</option>
					         <option value="Recruiter">Recruiter</option>
					         <option value="Banks">Banks</option>
					         <option value="Insurance">Insurance</option>
					         <option value="Travel Agent">Travel Agent</option>
					         <option value="Govt-Department">Govt-Department</option>
					         <option value="Telecommunication">Telecommunication</option>
					         <option value="Money Transfer Agents">Money Transfer Agents</option>
					         <option value="Hotel and Lodging">Hotel & Lodging</option>
					         <option value="Corporate">Corporate</option>
					         <option value="Other">Other</option>
					</select>
				</td>
				
			</tr>
			
			
			<tr id="planNameSpanId" style="display:none;width:100%;">						  
				
				<td style="width:100px;">
				
				 <b>Plan Name</b></td>
				 <td style="width:10px;">:</td>
				 <td style="width:200px;">
				 
					 <select name="Plan_name" id="plan_nameId"  onchange="getPlandetails(this.value)">
								
					 </select>
				 	        
				 </td>
			</tr>
			
			<tr id="planPriceSpanId" style="display:none;width:100%;">						  
				<td style="width:100px;">
				
				<b>Plan Price</b>
				</td>
				<td style="width:10px;">:</td>
				<td style="width:200px;">
				<input name="Plan_price" disabled="true" id="plan_priceId"  />											             
				         
				</td>
				
			</tr>
				
		</table>
		
		
	</form> 
				
				
				
				<div id="utilizerFeatureDivId" class="datagrid" style="width: 100%;">	
				
				
				
				</div>
</body>
</html>