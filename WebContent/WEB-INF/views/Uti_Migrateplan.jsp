<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="resources/css/PlanDesignResources/css/planmaster.css" media="screen">

<script>
function migrateUtilizerPlan(planName)
{
	$.ajax({
        url: "changeUti_plan.html",
        type: 'POST',	
        data: "planName="+planName,
        success: function (data) {   
	         
        	if(data=="success")
        		{
        			document.getElementById('sociallight').style.background = "none repeat scroll 0 0 #e9ffd9";
		   		     document.getElementById('sociallight').style.border= "1px solid #a6ca8a";
		   		     document.getElementById('sociallight').style.display='block';
		   		     document.getElementById('socialfade').style.display='block';
		   		     $("#socialdisplaySharedDocDiv").html("<h3> Your plan Migrate to "+planName+"  <input type=\"button\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </h3>");   
		   			 
        		    
        		   
        		}
        },
	});	
}

</script>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

<div id="planmain">
<div id="plancontainer">
		
  <div id="plantablecontainer" >
  
	<table class="planpricingtablecontent">
	
				<tbody>

				<!-- Table Content -->
				<tr class="plantablecell">
				
					<th class="planHeading">
					<h1><b>Plan Features</b></h1>
					</th> <!-- end tableHeading -->		
					<th class="tableHeading01">
					<h3>Basic</h3>
					</th> 
					<th class="tableHeading02">
					<h3>Silver</h3>
					</th> 
					<th class="tableHeading03">
					<h3>Gold</h3>
					</th> 
					<th class="tableHeading04">
					<h3>Platinum</h3>   
					</th> 
					
				</tr>
				
		<%String  featureName = (String)request.getAttribute("featurecategory");
		  String featureArray[] = featureName.split(",");
		  
		  String basicDetails = (String)request.getAttribute("basicDetails");
		  String basicDetailsArray[] = basicDetails.split(",");
		  
		  String silverDetails = (String)request.getAttribute("silverDetails");
		  String silverDetailsArray[] = silverDetails.split(",");
		  
		  String goldDetails = (String)request.getAttribute("goldDetails");
		  String goldDetailsArray[] = goldDetails.split(",");
		  
		  String platinumDetails = (String)request.getAttribute("platinumDetails");
		  String platinumDetailsArray[] = platinumDetails.split(",");
		  
		  String currentPlan = (String)request.getAttribute("planname");
			 
			
		  %>	
					
				<%for(int i=0,j=1;i<featureArray.length;i++,j++) {%>	<!-- Start of Feature Name for-loop -->
						
						<%if(j%2==0){%>
						<tr class="panaltrow">
						<%}else{%>
						<tr>						
						<%}%>	
											    
							<td><strong><%=featureArray[i]%></strong> </td>
							
							
							
							
							<%boolean basiccondition = true;
							for(int k=0;k<basicDetailsArray.length;k++) {
							    String basicDataArray[] = basicDetailsArray[k].split("##");
							      %>
							    
									<%if(featureArray[i].equals(basicDataArray[0])){ 
									      
										basiccondition = false;
									        %>
									
											<td><%=basicDataArray[1]%></td>
											
									<% }%>								     								      	
							  <%} %>
							  
							  <%if(basiccondition == true) {%>
							  
							                 <td>NA</td>
							              
							   <%} %>
							   
							   
							   
							
							<%boolean silvercondition = true;
							for(int k=0;k<silverDetailsArray.length;k++) {
							    String silverDataArray[] = silverDetailsArray[k].split("##");
							      %>
							    
									<%if(featureArray[i].equals(silverDataArray[0])){ 
									      
										silvercondition = false;
									        %>
									
											<td><%=silverDataArray[1]%></td>
											
									<% }%>								     								      	
							  <%} %>
							  
							  <%if(silvercondition == true) {%>
							  
							              <td>NA</td>
							              
							  <%} %>
							
							
							<%boolean goldcondition = true;
							for(int k=0;k<goldDetailsArray.length;k++) {
							    String goldDataArray[] = goldDetailsArray[k].split("##");
							      %>
							    
									<%if(featureArray[i].equals(goldDataArray[0])){ 
									      
										goldcondition = false;
									        %>
									
											<td><%=goldDataArray[1]%></td>
											
									<% }%>								     								      	
							  <%} %>
							  
							  <%if(goldcondition == true) {%>
							  
							              <td>NA</td>
							              
							  <%} %>
							
							<%boolean platinumcondition = true;
							for(int k=0;k<platinumDetailsArray.length;k++) {
							    String platinumDataArray[] = platinumDetailsArray[k].split("##");
							      %>
							    
									<%if(featureArray[i].equals(platinumDataArray[0])){ 
									      
										platinumcondition = false;
									        %>
									
											<td><%=platinumDataArray[1]%></td>
											
									<% }%>								     								      	
							  <%} %>
							  
							  <%if(platinumcondition == true) {%>
							  
							              <td>NA</td>
							              
							  <%} %>
						</tr>
						
                <%} %>													<!-- End of Feature Name for-loop -->

				
				<!-- Table Footer -->
				</tbody>
				
				<tfoot>
					<tr>
						<td>
							<span>per Annum</span>
						</td>

						<td>
							<h4>Free<sup></sup></h4>
							<span></span>
							
							<%if(currentPlan.equals("Platinum")) {%>
							
							<a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Basic')">Down Grade</a>
							
							<%}else if(currentPlan.equals("Gold")){ %>
							
							<a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Basic')">Down Grade</a>
							
							<%}else if(currentPlan.equals("Silver")){ %>
							
							<a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Basic')">Down Grade</a>
							
							<%}else{ %>
							
							       <h1>Active Plan </h1>
							       
							<%} %>
							
						</td>
						<td>
							<h4><%=request.getAttribute("silverprice")%><sup></sup></h4>
							
							<%if(currentPlan.equals("Platinum")) {%>
							
							<a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Silver')">Down Grade</a>
							
							<%}else if(currentPlan.equals("Gold")){ %>
							
							<a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Silver')">Down Grade</a>
							
							<%}else if(currentPlan.equals("Silver")){ %>
							
							<h1>Active Plan</h1>
							
							<%}else{ %>
							
							     <a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Silver')">Upgrade</a>
							       
							<%} %>
														
							
							
						</td>

						<td >
							<h4><%=request.getAttribute("goldprice")%><sup></sup></h4>
							
							<%if(currentPlan.equals("Platinum")) {%>
							
							<a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Gold')">Down Grade</a>
							
							<%}else if(currentPlan.equals("Gold")){ %>
							
							<h1>Active Plan</h1>
							
							<%}else if(currentPlan.equals("Silver")){ %>
							
							<a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Gold')">Upgrade</a>
							
							<%}else{ %>
							
							    <a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Gold')">Upgrade</a>
							       
							<%} %>
									
																
							
						</td>

						<td class="noBorder">
							<h4><%=request.getAttribute("platinumprice")%><sup></sup></h4>
							
							<%if(currentPlan.equals("Platinum")) {%>
							
							     <h1>Active Plan</h1>
							
							<%}else {%>
							
							      <a href="#" class="signUpButton" onclick="migrateUtilizerPlan('Platinum')">Upgrade</a>
																					
							<%} %>
							
							
							
						</td>
					</tr>
				</tfoot>
				
			</table>
			
		</div> <!-- end tableContainer -->

	</div> <!-- end pageContainer -->
	</div>
</body>
</html>