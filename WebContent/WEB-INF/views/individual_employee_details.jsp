 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Documents</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>
<!-- Employee Date Picker  -->


<link rel="stylesheet" type="text/css" href="/css/result-light.css">

<script type='text/javascript' src="resources/onlineresources/code.jquery.com.jquery-1.9.1.js"></script>
<script type='text/javascript' src="resources/onlineresources/code.jquery.com.ui.1.10.3.jqury-ui.js"></script>

 
<style type='text/css'>

    .calendar {
				font-family: 'Trebuchet MS', Tahoma, Verdana, Arial, sans-serif;
				font-size: 0.9em;
				background-color: #c4c0c0;
				color: #333;
				border: 1px solid #DDD;
				-moz-border-radius: 4px;
				-webkit-border-radius: 4px;
				border-radius: 4px;
				padding: 0.2em;
				width: 14em;
                border: 1px solid #595858;
			}			
			.calendar .months {
				background-color: #918f8f;
				border: 1px solid #595858;
				-moz-border-radius: 4px;
				-webkit-border-radius: 4px;
				border-radius: 4px;
				color: #FFF;
				padding: 0.2em;
				text-align: center;
			}
			
			.calendar .prev-month,
			.calendar .next-month {
				padding: 0;
			}
			
			.calendar .prev-month {
				float: left;
			}
			
			.calendar .next-month {
				float: right;
			}
			
			.calendar .current-month {
				margin: 0 auto;
			}
			
			.calendar .months .prev-month,
			.calendar .months .next-month {
				color: #FFF;
				text-decoration: none;
				padding: 0 0.4em;
				-moz-border-radius: 4px;
				-webkit-border-radius: 4px;
				border-radius: 4px;
				cursor: pointer;
			}
			
			.calendar .months .prev-month:hover,
			.calendar .months .next-month:hover {
				background-color: #FDF5CE;
				color: #C77405;
			}
			
			.calendar table {
				border-collapse: collapse;
				padding: 0;
				font-size: 0.8em;
				width: 100%;
			}
			
			.calendar th {
				text-align: center;
			}
			
			.calendar td {
				text-align: center;
				padding: 1px;
				width: 14.3%;
			}
			
			.calendar td span {
				display: block;
				color: #000;
				background-color: #dfdddd;
				border: 1px solid #CCC;
				text-decoration: none;
				padding: 0.2em;
				cursor: pointer;
			}
			
			.calendar td span:hover {
				color: #C77405;
				background-color: #FDF5CE;
				border: 1px solid #FBCB09;
			}
			
			.calendar td.today span {
				background-color: #FFF0A5;
				border: 1px solid #FED22F;
				color: #363636;
			}
  </style>
  


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
$(".datepick2").datepicker({'dateFormat': 'd/m/y',maxDate:"0d",changeMonth: true,
	changeYear: true});
$("#datepick_id").datepicker({'dateFormat': 'd/m/y',maxDate:"0d",changeMonth: true,
	changeYear: true});

});  

</script>

<!-- End Of Employee Date Picker -->


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">
<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
 
<script src="resources/jscripts/jquery.com.jquery-1.10.2.js"></script>
<script src="resources/jscripts/jquery.com.jqueryUi.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/js/kyc_side_login.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
	width: 329px;
	z-index: 1002;
	}
</style>

<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}


</style>



<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{
border: 2px solid #03b2ee;
    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}



</style>
 
<script>

function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		  window.location = "employeeDetails.html";
		}
}

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewDocumentInPopUp(fileName)
{
	 $.ajax({
				type : "post",
				url : "checksessionforIndividual.html",
				success : function(response) {
					
					if(response=="Exist")
						{
							document.getElementById('waitlight').style.display='block';
							document.getElementById('waitfade').style.display='block';
							 
							$.ajax({
								type : "get",				
								url : "viewEmployeeDocument.html",
								data: "fileName="+fileName,
								success : function(response) {
								
									document.getElementById('waitlight').style.display='none';
									document.getElementById('waitfade').style.display='none';
									 
									document.getElementById('kycDocPopUplight').style.display='block';
							        document.getElementById('kycDocPopUpfade').style.display='block';
							        $("#kycDocPopUpDisplay").html(response);
							        			
								},		
							});		
						}
					else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 		 		 			
}

</script> 
 


<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    		 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>








<script type="text/javascript">
function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length==0)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}
</script>





<script>

/* All Scripts for MyDocument pannel  */

	
	function myDocuments2(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 window.location = "search_financialDoc.html";
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});		 		 		 				
	    }
	
	function myDocuments4(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>EMPLOYEMENT DETAILS</span></h2>");
	}
	
	
		
		/* <!-- End of code for noneditable to editabel for document-->  */
 	 	 	 	 		 
</script>

<!-- Script and CSS for Employement Documents -->

<script type="text/javascript">


function createEmployee()
{	
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	var cmyNameRegex = /^[a-zA-Z0-9 ]*$/;
	
	var flag = 'true';
   	
	   var orgName = document.getElementById("cmyNameId").value;
	   
	   if(orgName=="")
		   {
		       document.getElementById('waitlight').style.display='none';
			   document.getElementById('waitfade').style.display='none';
			   
		       flag='false';
		       $("#orgNameDiv").show();
		       $("#orgNameDiv").html("Enter your Organization Name !");
		       return false;
		   }
	   if(orgName!="")
	   {
	       if(!orgName.match(cmyNameRegex))
	    	   {
	    	   		document.getElementById('waitlight').style.display='none';
			   		document.getElementById('waitfade').style.display='none';
			   
		    	   flag='false';
			       $("#orgNameDiv").show();
			       $("#orgNameDiv").html("Please don't mention special character in organization name .");
			       return false;
	    	   }
	       else
	    	   {
	    	       $("#orgNameDiv").hide();  
	    	   }
		       
	   }
	   
	  	
	   if(flag=='true')
		   {	
		   
		     var replaceSpaceEnd = orgName.replace(/(^[\s]+|[\s]+$)/g, '');
		  
		     $.ajax({
		        url: "checkorgnameexistornot.html",
		        type: 'POST',
		        data: "orgName="+replaceSpaceEnd,
		        success: function (data) {          
		          			        	
			        	if(data!="Exist")
			        		{  			   
							    $.ajax({
							        url: "checkLastEmployeeDetails.html",
							        type: 'POST',
							        data: "orgName="+replaceSpaceEnd,
							        success: function (data) {          
							          		        
							        	if(data!="No Data")
							        		{		        		
									        		 $.ajax({
													        url: "createEmployerName.html",
													        type: 'POST',
													        data: "cmyName="+replaceSpaceEnd,													       
													        success: function (data) {          
													            
													        	document.getElementById('waitlight').style.display='none';
																document.getElementById('waitfade').style.display='none';
																																
															    document.getElementById('sociallight').style.display='block';
															    document.getElementById('socialfade').style.display='block';
															    document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
															    $("#socialdisplaySharedDocDiv").html("Your employer name has been added successfully. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");   
																 
													        },								        
													    });
									        		 
							        		}
							        	else
							        		{
							        			document.getElementById('waitlight').style.display='none';
							 			   		document.getElementById('waitfade').style.display='none';
							 			   
							        		    alert("Before add one more employer name,Please fill last company details.");
							        		}
							        },		        
							    });
							 
		        		}
		        	else
		        		{
		        		     document.getElementById('waitlight').style.display='none';
		 			         document.getElementById('waitfade').style.display='none';
		        		     alert("This employer name is already Exist or contains in previous record .");
		        		}
		        	},								        
			    });
    		   		   				   				    
		}		
}


function EditEmpDoc(IdValue)
{
	var divIdArray = IdValue.split("%");
	
	if(document.getElementById(divIdArray[0]).style.display == "block")
		{	
			document.getElementById(divIdArray[0]).style.display = "none";
		    document.getElementById(divIdArray[1]).style.display = "block";
		}
	else
		{
			document.getElementById(divIdArray[1]).style.display = "none";
		    document.getElementById(divIdArray[0]).style.display = "block";
		    			    
		    if(divIdArray[1].indexOf("Offer")>1)
		    	{
		    	    var panel = divIdArray[1].split("h");		    	    
		    	    document.getElementById("offerfile"+panel[0]).value = "";
		    	}
		    else if(divIdArray[1].indexOf("Hike")>1)
		    	{		    	    
		    		var panel = divIdArray[1].split("h");
	    	    	document.getElementById("hikefile"+panel[0]).value = "";
		    	}
		    else if(divIdArray[1].indexOf("Exp")>1)
		    	{
		    		var panel = divIdArray[1].split("h");
    	    		document.getElementById("expfile"+panel[0]).value = "";
		    	}
		    else if(divIdArray[1].indexOf("Rel")>1)
		    	{
		    		var panel = divIdArray[1].split("h");
	    			document.getElementById("relfile"+panel[0]).value = "";
		    	}
		    else
		    	{
		    		var panel = divIdArray[1].split("h");
	    			document.getElementById("formfile"+panel[0]).value = "";
		    	}
		}
}



function hideErrormessage(idVal)
{
	$("#"+idVal+"jobDesigDiv").hide();
	$("#"+idVal+"jobProDiv").hide();
	$("#"+idVal+"jobDurDiv").hide();
}

function saveEmployee(idVal)
{
	
	var panel = idVal.split("#");
	var IdValue = panel[0];
	var empfmName = panel[2];
	
	var panelName = panel[3];
	
	var buttonValue = document.getElementById(idVal).value;
		
	if(buttonValue == "Edit")
		{
		
		
		if(idVal.indexOf("saveIdUpper")>0)
			{
				document.getElementById(idVal).value = "Save";
				document.getElementById(idVal).className = "saveinner_button"; 		
				
				var anothrId = idVal.replace("saveIdUpper","saveId");
				
				document.getElementById(anothrId).value = "Save";
				document.getElementById(anothrId).className = "saveinner_button"; 		
				
			}
		else
			{
				document.getElementById(idVal).value = "Save";
				document.getElementById(idVal).className = "saveinner_button"; 	
				
                var anothrId = idVal.replace("saveId","saveIdUpper");
				
				document.getElementById(anothrId).value = "Save";
				document.getElementById(anothrId).className = "saveinner_button"; 		
				
			}
		
        x = document.getElementById(IdValue+"cmyNameId");
        x.disabled = !x.disabled;
        x = document.getElementById(IdValue+"jobDesigId");
        x.disabled = !x.disabled;
        x = document.getElementById(IdValue+"jobProId");
        x.disabled = !x.disabled;
        x = document.getElementById(IdValue+"jobfromId");
        x.disabled = !x.disabled;
        x = document.getElementById(IdValue+"jobtoId");
        x.disabled = !x.disabled;
        x = document.getElementById(IdValue+"current");
        x.disabled = !x.disabled;
       
        $("#"+empfmName+"addsalBtn").show();
        $("#orgNameDiv").hide();    
        
        $("#"+panelName+"offerEdit").show();
        $("#"+panelName+"HikeEdit").show();
        $("#"+panelName+"ExpEdit").show();
        $("#"+panelName+"RelEdit").show();
        $("#"+panelName+"FormEdit").show();
        
		}
				
	if(buttonValue == "Save")
		{
			
		     document.getElementById('waitlight').style.display='block';
		     document.getElementById('waitfade').style.display='block';
		     
		   var flag = 'true';
	   	
		   var orgName = document.getElementById(IdValue+"cmyNameId").value;
		   var jobDesignation = document.getElementById(IdValue+"jobDesigId").value;
		   var jobProfile = document.getElementById(IdValue+"jobProId").value;		   
		   var from =  document.getElementById(IdValue+"jobfromId").value;
		   var to = document.getElementById(IdValue+"jobtoId").value;
					 		   
		   if(orgName=="")
		   {
			   document.getElementById('waitlight').style.display='none';
			   document.getElementById('waitfade').style.display='none';
			     
		       flag='false';
		       var divId = IdValue+"orgNameDiv";
		       document.getElementById(divId).style.display="block";
		       document.getElementById(divId).innerHTML = "Enter your organization name.";
		       return false;
		   }
		   if(orgName!="")
		   {
			   var divId = IdValue+"orgNameDiv";
			   document.getElementById(divId).style.display="none";
		      
		   }
		   
		   if(jobDesignation=="")
		   {
			   document.getElementById('waitlight').style.display='none';
			   document.getElementById('waitfade').style.display='none';
			   
			   flag='false';
		       var divId = IdValue+"jobDesigDiv";
		       document.getElementById(divId).style.display="block";
		       document.getElementById(divId).innerHTML = "Enter your job designation.";
		       return false;
		   }
		   if(jobDesignation!="")
		   {
			   var divId = IdValue+"jobDesigDiv";
			   document.getElementById(divId).style.display="none";
		   }
		   
		   if(jobProfile=="")
			   {
			  	 document.getElementById('waitlight').style.display='none';
			  	 document.getElementById('waitfade').style.display='none';
			   
				   flag='false';
				   var divId = IdValue+"jobProDiv";
				   document.getElementById(divId).style.display="block";
			       document.getElementById(divId).innerHTML = "Enter your job profile.";
			       return false;
			       
			   }
		   if(jobProfile!="")
		   {			  
			   var divId = IdValue+"jobProDiv";
			   document.getElementById(divId).style.display="none";
		   }
		   
			
		   if(from=="")
		      {
			 	  document.getElementById('waitlight').style.display='none';
			  	  document.getElementById('waitfade').style.display='none';
			   
				   flag='false';
				   var divId = IdValue+"jobDurDiv";
				   document.getElementById(divId).style.display="block";
			       document.getElementById(divId).innerHTML = "Select your job starting date.";		
			       return false;
			  }
			if(from!="")
			{
				var divId = IdValue+"jobDurDiv";
				document.getElementById(divId).style.display="none";
			}
		   
			if(to=="")
		    {
				   document.getElementById('waitlight').style.display='none';
				   document.getElementById('waitfade').style.display='none';
				   
				   flag='false';
				   var divId = IdValue+"jobDurDiv";
				   document.getElementById(divId).style.display="block";
			       document.getElementById(divId).innerHTML = "Select your job end date.";		
			       return false;
			}
			
			if(to!="")
			{
				var divId = IdValue+"jobDurDiv";
				document.getElementById(divId).style.display="none";
			}
		   
			var offerletter = document.getElementById("offerfile"+IdValue).value;
			var offerLettrSize = document.getElementById("offerfile"+IdValue);
						
			var hikeletter = document.getElementById("hikefile"+IdValue).value
			var hikeLetterSize = document.getElementById("hikefile"+IdValue);
			
			var expLetter = document.getElementById("expfile"+IdValue).value;
			var expLetterSize = document.getElementById("expfile"+IdValue);
			
			var relivingLetter = document.getElementById("relfile"+IdValue).value;
			var relivingLetterSize = document.getElementById("relfile"+IdValue);
			
			var form16 = document.getElementById("formfile"+IdValue).value;
			var form16Size = document.getElementById("formfile"+IdValue);
			
			var sizeinKB1 = 0;
			var sizeinKB2 = 0;
			var sizeinKB3 = 0;
			var sizeinKB4 = 0;
			var sizeinKB5 = 0;
			
			if(offerletter!="")
				{
					var ext=offerletter.substring(offerletter.lastIndexOf('.')+1);
				    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf");						    
				    var condition = "NotGranted";
				    for(var m=0;m<extension.length;m++)
						{
						    if(ext==extension[m])
						    	{				    	    
						    	   condition="Granted";				    	    
						    	}				    
						}
					if(condition=="NotGranted")
						{
						   document.getElementById('waitlight').style.display='none';
						   document.getElementById('waitfade').style.display='none';
						   
						   flag = 'false';
						   						  
		    			   document.getElementById('sociallight').style.display='block';
		    			   document.getElementById('socialfade').style.display='block';
		    			   document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		    			   $("#socialdisplaySharedDocDiv").html(" pdf/office/image files are only allowed for offer letter. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

						   return false;
						}
					
					var fileSize = offerLettrSize.files[0];
			    	var fileSizeinBytes = fileSize.size;
			    	sizeinKB1 += +fileSizeinBytes / 1024;
			    	var sizeinMB = +sizeinKB1 / 1024;
			    	if(sizeinMB>2)
			    		{
			    		   document.getElementById('waitlight').style.display='none';
						   document.getElementById('waitfade').style.display='none';
						   
			    			flag = 'false';
			    			
			    			document.getElementById('sociallight').style.display='block';
			    			document.getElementById('socialfade').style.display='block';
			    			document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			    			$("#socialdisplaySharedDocDiv").html("Offer letter size should not more then 2 MB. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" value=\"OK\" class=\"button_style\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			 		   		return false;
			    		}
				}
			
			if(hikeletter!="")
			{
				var ext=hikeletter.substring(hikeletter.lastIndexOf('.')+1);
			    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf");						    
			    var condition = "NotGranted";
			    for(var m=0;m<extension.length;m++)
					{
					    if(ext==extension[m])
					    	{				    	    
					    	   condition="Granted";				    	    
					    	}				    
					}
				if(condition=="NotGranted")
					{
					   document.getElementById('waitlight').style.display='none';
					   document.getElementById('waitfade').style.display='none';
					   
					   flag = 'false';
					   
					   
		    			document.getElementById('sociallight').style.display='block';
		    			document.getElementById('socialfade').style.display='block';
		    			document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		    			$("#socialdisplaySharedDocDiv").html("pdf/office/image files are only allowed for hike letter. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

		 		   		return false;
					   
					}
				
				var fileSize = hikeLetterSize.files[0];
		    	var fileSizeinBytes = fileSize.size;
		    	sizeinKB2 += +fileSizeinBytes / 1024;
		    	var sizeinMB = +sizeinKB2 / 1024;
		    	if(sizeinMB>2)
		    		{
		    		document.getElementById('waitlight').style.display='none';
					   document.getElementById('waitfade').style.display='none';
		    			flag = 'false';
		    				    		
		    			document.getElementById('sociallight').style.display='block';
		    			document.getElementById('socialfade').style.display='block';
		    			document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		    			$("#socialdisplaySharedDocDiv").html(" Hike letter size should not more then 2 MB . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

		    			return false;
		 		   		
		    		}
			}
			
			if(expLetter!="")
			{
				var ext=expLetter.substring(expLetter.lastIndexOf('.')+1);
			    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf");						    
			    var condition = "NotGranted";
			    for(var m=0;m<extension.length;m++)
					{
					    if(ext==extension[m])
					    	{				    	    
					    	   condition="Granted";				    	    
					    	}				    
					}
				if(condition=="NotGranted")
					{
					document.getElementById('waitlight').style.display='none';
					   document.getElementById('waitfade').style.display='none';
					   
					   flag = 'false';
					   					  
		    			document.getElementById('sociallight').style.display='block';
		    			document.getElementById('socialfade').style.display='block';
		    			document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		    			$("#socialdisplaySharedDocDiv").html("pdf/office/image files are only allowed for experience letter. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

		    			return false;
		    			
					}
				
				var fileSize = expLetterSize.files[0];
		    	var fileSizeinBytes = fileSize.size;
		    	sizeinKB3 += +fileSizeinBytes / 1024;	
		    	var sizeinMB = +sizeinKB3 / 1024;
		    	if(sizeinMB>2)
		    		{
		    		document.getElementById('waitlight').style.display='none';
					document.getElementById('waitfade').style.display='none';
		    		flag = 'false';
		    			

			    	     document.getElementById('sociallight').style.display='block';
			    		 document.getElementById('socialfade').style.display='block';
			    		 document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			    		 $("#socialdisplaySharedDocDiv").html("Experience letter size should not more then 2 MB. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			    		 return false;			    		
		    		}
			}
			
			if(relivingLetter!="")
			{
				var ext=relivingLetter.substring(relivingLetter.lastIndexOf('.')+1);
			    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf");						    
			    var condition = "NotGranted";
			    for(var m=0;m<extension.length;m++)
					{
					    if(ext==extension[m])
					    	{				    	    
					    	   condition="Granted";				    	    
					    	}				    
					}
				if(condition=="NotGranted")
					{
					document.getElementById('waitlight').style.display='none';
					   document.getElementById('waitfade').style.display='none';
					   flag = 'false';
					   
			    	     document.getElementById('sociallight').style.display='block';
			    		 document.getElementById('socialfade').style.display='block';
			    		 document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			    		 $("#socialdisplaySharedDocDiv").html("pdf/office/image files are only allowed reliving letter . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" style=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			    		 return false;			    		
					}
				
				var fileSize = relivingLetterSize.files[0];
		    	var fileSizeinBytes = fileSize.size;
		    	sizeinKB4 += +fileSizeinBytes / 1024;
		    	var sizeinMB = +sizeinKB4 / 1024;
		    	if(sizeinMB>2)
		    		{
		    		    document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
		    			flag = 'false';
		    			    		
			    	     document.getElementById('sociallight').style.display='block';
			    		 document.getElementById('socialfade').style.display='block';
			    		 document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			    		 $("#socialdisplaySharedDocDiv").html("Reliving letter size should not more then 2 MB. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			    		return false;
		 		   		
		    		}
			}
			
			
			if(form16!="")
			{
				var ext=form16.substring(form16.lastIndexOf('.')+1);
			    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf","xls","xlsx");						    
			    var condition = "NotGranted";
			    for(var m=0;m<extension.length;m++)
					{
					    if(ext==extension[m])
					    	{				    	    
					    	   condition="Granted";				    	    
					    	}				    
					}
				if(condition=="NotGranted")
					{
					document.getElementById('waitlight').style.display='none';
					   document.getElementById('waitfade').style.display='none';
					   flag = 'false';
					   
					   
			    	      document.getElementById('sociallight').style.display='block';
			    		  document.getElementById('socialfade').style.display='block';
			    		  document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			    		  $("#socialdisplaySharedDocDiv").html("pdf/office/image files are only allowed for form-16. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			    		  return false;
			    							   
					}
				
				var fileSize = form16Size.files[0];
		    	var fileSizeinBytes = fileSize.size;
		    	sizeinKB5 += +fileSizeinBytes / 1024;
		    	var sizeinMB = +sizeinKB5 / 1024;
		    	if(sizeinMB>2)
		    		{
		    		document.getElementById('waitlight').style.display='none';
					   document.getElementById('waitfade').style.display='none';
		    			flag = 'false';
		    			
			    	     document.getElementById('sociallight').style.display='block';
			    		 document.getElementById('socialfade').style.display='block';
			    		 document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			    		 $("#socialdisplaySharedDocDiv").html("Form-16 size should not more then 2 MB. <div style=\"width:97%;margin 0 auto;\"><input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			    		return false;
			
		    		}
			}
			
			var sizeinKB = sizeinKB1 + sizeinKB2 + sizeinKB3 + sizeinKB4 + sizeinKB5;
								
			if(flag=='true')
				{									
				
						$.ajax({
							type : "post",
							url : "checksessionforIndividual.html",
							success : function(response) {
								
								if(response=="Exist")
									{
											$.ajax
										    ({
												type : "post",				
												url : "validateFileStorage.html",	
												data:"givenfileSize="+sizeinKB,
												success : function(response) {
																
													if(response=="withinStorage")
													{
											
													      	 	var empFormData = new FormData($("#Employeeform"+empfmName)[0]);
													      	 		      	       	 	
															    $.ajax({
															        url: "editEmployeeDetail.html",
															        type: 'POST',
															        data: empFormData,
															        //async: false,
															        success: function (data) {          
															            
															        	document.getElementById('waitlight').style.display='none';
																		document.getElementById('waitfade').style.display='none';
																		   
															        	$("#"+empfmName+"addsalBtn").show();
															        	
															        	 
																	     document.getElementById('sociallight').style.display='block';
																	     document.getElementById('socialfade').style.display='block';
																	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
																	     $("#socialdisplaySharedDocDiv").html("Your employee details has been saved successfully .  <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");
															        	
															        	
															        },
															        
															        cache: false,
															        contentType: false,
															        processData: false,
															        error : function(e) 
															        	{  
																  	      alert('Error: ' + e);
																  	     } 
															    });
															    			    
															    document.getElementById(IdValue+"dateformat").style.display = "none";
															    
													}
													else
													{
														document.getElementById('waitlight').style.display='none';
														document.getElementById('waitfade').style.display='none';
														alert("Failed due to storage limitation,to upload more documents migrate your plan!");
													}			
												},
										    });	    
									}
								else
									{
									   $("#sessionlight").show();
								       $("#sessionfade").show();
									}
							}
						}); 
							    
		      }
	}
}


function showEmpHistory(IdValue)
{	
	var id = IdValue+"cmyNameId";
	var cmyName = document.getElementById(id).value;
	
	window.location = "employement_history.html?cmyName="+cmyName;	
}

function validateJobDurationDateForFromDate(id)
{
     	var check = document.getElementById(id).value;   	
     	var panelName = id.split("j");     	
     	var cmyName = document.getElementById(panelName[0]+"cmyNameId").value;
     
     	$.ajax
        ({
    		type : "post",				
    		url : "validateJobduration.html",
    		data:"checkDate="+check+"&cmyName="+cmyName,
    		success : function(response) {
    			   			
    			if(response=="Available")
    				{
    				    alert("Date is available in before selected duration .");
					    document.getElementById(id).value = "";
					    return false;
    				}
    			else
    				{
    					return true;
    				}
    		 },
        });
}

function validateJobDurationDateForToDate(panelName)
{	 
	var fromDate = document.getElementById(panelName+"jobfromId").value;
	var toDate = document.getElementById(panelName+"jobtoId").value;
    
	var companyName = document.getElementById(panelName+"cmyNameId").value;
	
	if(fromDate!="")
		{
			$.ajax
	        ({
	    		type : "post",				
	    		url : "validateJobdurationForToDate.html",
	    		data:"fromDate="+fromDate+"&toDate="+toDate+"&companyName="+companyName,
	    		success : function(response) {
	    			   			
	    			if(response=="Available")
	    				{
	    				    alert("Your job duartion is avilable previous companys job duration.");
						    document.getElementById(panelName+"jobtoId").value = "";
						    
						    if(document.getElementById(panelName+"current").checked)
							{
						    	document.getElementById(panelName+"current").checked = false;
							}
						    
						    return false;
	    				}
	    			else
	    				{
	    					return true;
	    				}
	    		 },
	        });
		}
	else
		{
		    alert("Select your from date first !");
		    document.getElementById(panelName+"jobtoId").value = "";
		}
     	
}


function checkJobDurationIsInProperOrderOrNot(fmName,orgName)
{				  
	
	  var salarySlip = "";
	   
	   var months = new Array("  ","January", "February", "March","April","May","June","July","August","September","October","November","December");	
	    
	   var fromDate = document.getElementById(orgName+"jobfromId").value;
	   var toDate = document.getElementById(orgName+"jobtoId").value;
	   
	   var fromDateConfirmation = "";
	   var toDateConfirmation = "";
	 
	
	  if(fromDate!="" && toDate!="")				 
		 {	 								
			 
					   var fmonth = "";
					   var tmonth = "";
					   var yearDiff = "";
					   var monthDiff = "";		   
					   var fromYear = "";
					   var toYear = "";
						   
					 if(toDate=='Presently Working')
						 {
						    var fromYearValue = fromDate.split("/");
						    var currentDate = new Date();
						    
						    var currentMonth = currentDate.getMonth(); 
						    var currentYear = currentDate.getFullYear();
						
						    currentYear = currentYear % 100;
						    						    
						    currentMonth = +currentMonth + 1;
						    
						    var diffYear = currentYear - fromYearValue[2];	
							var diffMonth = currentMonth - fromYearValue[1];
							
							fromYear = fromYearValue[2];
							toYear = currentYear;
							   
							 yearDiff = +diffYear;
							 monthDiff = +diffMonth;	
							 
							 fmonth = fromYearValue[1];
							   
							 fmonth = +fmonth + 1;
							 fmonth = +fmonth - 1;
							 
							 tmonth = currentMonth;
							   
							 tmonth = +tmonth + 1;
							 tmonth = +tmonth - 1;
							 
						 }
					 else
						 {
						    var fromYearValue = fromDate.split("/");
						    var toYearValue = toDate.split("/");		
						 
						    var diffYear = toYearValue[2] - fromYearValue[2];	
							var diffMonth = toYearValue[1] - fromYearValue[1];
							   
							   fromYear = fromYearValue[2];
							   toYear = toYearValue[2];							   
							   
							   fmonth = fromYearValue[1];
							   
							   fmonth = +fmonth + 1;
							   fmonth = +fmonth - 1;
							   
							   tmonth = toYearValue[1];
							   
							   tmonth = +tmonth + 1;
							   tmonth = +tmonth - 1;
							   
							   yearDiff = 	+diffYear;
							   monthDiff =  +diffMonth;
						 }
				   	 	   
				   var monthString = "";
				   	  
				   if(yearDiff==0)
					   {
						   for(fmonth;fmonth<=tmonth;fmonth++)
							   {
							       monthString += months[fmonth] + "-"+fromYear+",";
							   }
					   }
				   
				   if(yearDiff>0)
					   {
						   for(fmonth;fmonth<13;fmonth++)
							   {				      
							      monthString +=  months[fmonth] + "-"+fromYear+ ",";
							   }
						  
						   if(yearDiff>1)
							   {
							     var start = ++fromYear;
							    			     
							      for(var yr=start;yr<toYear;yr++)
							    	  {
							    	     for(var m=1;m<13;m++)
							    	    	 {
							    	    	    monthString += months[m]+"-"+yr+",";
							    	    	 }
							    	  }					     
							   }
						   
						   for(var m=1;m<=tmonth;m++)
						      {
						         monthString +=  months[m] + "-"+toYear+ ",";
						      }
						   	   		   
						   monthString = monthString.replace(",undefined,",",");
					   }
				    
				   monthString = monthString.substring(0, monthString.length-1);	   
				   var total = 12*yearDiff+monthDiff;	   	   
				      
				  
					   if(total<0)
						   {								        
						           alert("Your Job duration criteria is wrong,Please select again.");	
						           document.getElementById(orgName+"jobfromId").value = "";
						           document.getElementById(orgName+"jobtoId").value = "";
						   }
					   if(total==0)
						   {								        
						            alert("As per your job duration selection salary slip won't generate,Minimum 1 month duration is required.");	
							        document.getElementById(orgName+"jobfromId").value = "";
							        document.getElementById(orgName+"jobtoId").value = "";							        						        
						   }					   
				   }				   
				  
}




function hidePaySlip(fmName,orgName)
{
	$("#"+fmName+"PayslipSlider").hide();
	$("#"+fmName+"cancelBtn").hide();
}


function generatePayslip(fmName,orgName)
{		
	
	   var diviD = fmName+"payslipDropDownDiv";
	   var salarySlip = "";
	   
	   var months = new Array("  ","January", "February", "March","April","May","June","July","August","September","October","November","December");	
	    
	   var fromDate = document.getElementById(orgName+"jobfromId").value;
	   var toDate = document.getElementById(orgName+"jobtoId").value;
	   
	   var fromDateConfirmation = "";
	   var toDateConfirmation = "";
	  	  	  
	   if(fromDate!="" || toDate!="")				 
		 {	 											 
					   var fmonth = "";
					   var tmonth = "";
					   var yearDiff = "";
					   var monthDiff = "";		   
					   var fromYear = "";
					   var toYear = "";
						   
					 if(toDate=='Presently Working')
						 {
						   
						    var fromYearValue = fromDate.split("/");
						    var currentDate = new Date();
						    
						    var currentMonth = currentDate.getMonth(); 
						    var currentYear = currentDate.getFullYear();
						
						    currentYear = currentYear % 100;
						    						    
						    currentMonth = +currentMonth + 1;
						    
						    var diffYear = currentYear - fromYearValue[2];	
							var diffMonth = currentMonth - fromYearValue[1];
							
							fromYear = fromYearValue[2];
							toYear = currentYear;
							   
							 yearDiff = +diffYear;
							 monthDiff = +diffMonth;	
							 
							 fmonth = fromYearValue[1];
							   
							 fmonth = +fmonth + 1;
							 fmonth = +fmonth - 1;
							 
							 tmonth = currentMonth;
							   
							 tmonth = +tmonth + 1;
							 tmonth = +tmonth - 1;
							 
						 }
					 else
						 {
						    var fromYearValue = fromDate.split("/");
						    var toYearValue = toDate.split("/");		
						 
						    var diffYear = toYearValue[2] - fromYearValue[2];	
							var diffMonth = toYearValue[1] - fromYearValue[1];
							   
							   fromYear = fromYearValue[2];
							   toYear = toYearValue[2];							   
							   
							   fmonth = fromYearValue[1];
							   
							   fmonth = +fmonth + 1;
							   fmonth = +fmonth - 1;
							   
							   tmonth = toYearValue[1];
							   
							   tmonth = +tmonth + 1;
							   tmonth = +tmonth - 1;
							   
							   yearDiff = 	+diffYear;
							   monthDiff =  +diffMonth;
						 }
				   	 	   
				   var monthString = "";
				   	  
				   if(yearDiff==0)
					   {
						   for(fmonth;fmonth<=tmonth;fmonth++)
							   {
							       monthString += months[fmonth] + "-"+fromYear+",";
							   }
					   }
				   
				   if(yearDiff>0)
					   {
						   for(fmonth;fmonth<13;fmonth++)
							   {				      
							      monthString +=  months[fmonth] + "-"+fromYear+ ",";
							   }
						  
						   if(yearDiff>1)
							   {
							     var start = ++fromYear;
							    			     
							      for(var yr=start;yr<toYear;yr++)
							    	  {
							    	     for(var m=1;m<13;m++)
							    	    	 {
							    	    	    monthString += months[m]+"-"+yr+",";
							    	    	 }
							    	  }					     
							   }
						   
						   for(var m=1;m<=tmonth;m++)
						      {
						         monthString +=  months[m] + "-"+toYear+ ",";
						      }
						   	   		   
						   monthString = monthString.replace(",undefined,",",");
					   }
				    
				   monthString = monthString.substring(0, monthString.length-1);	   
				   var total = 12*yearDiff+monthDiff;	   	   
				   $("#"+diviD).html("");
				   	   
					   if(total>0)
						   {	
						   
						       var diffMonthName = monthString.split(",");		       
						       salarySlip += "<select style=\"width:165px;\" id=\"selected"+fmName+"\" onchange=\"createBrowse(this.id)\">";		       
							  
						       salarySlip += "<option value=\"Select Salary Slip Month\">Select Salary Slip Month</option>";
						       
						       for(var n=0,j=0;n<diffMonthName.length;n++,j++)
								   {				      
								      salarySlip += "<option value=\""+diffMonthName[n]+"\">"+diffMonthName[n]+"</option>";
								   }
							   salarySlip += "</select>";
							   
							   $("#"+fmName+"PayslipSlider").show();
							   $("#"+fmName+"cancelBtn").show();
								 							    		   
							   $("#"+diviD).html(salarySlip);
							   
						   }
					   else
						   {
							  // alert("Your Job Duration criteria is wrong,Please Select again !");	
							   alert("As per your job duration selection salary slip won't generate,Minimum 1 month duration is required !");	
						        /* document.getElementById(orgName+"jobfromId").value = "";
						        document.getElementById(orgName+"jobtoId").value = ""; */
						   }				   				   
				   }
				   
				   else
					   {
					      alert("To add salary slip, Set job duration first.");
					   }
}
	 

function createBrowse(val)
{		
	if(val!="Select Salary Slip Month")
		{
			var idValue = val.replace("selected","");  
			var value = idValue.split("/");
			
			
			$("#payslipsBrowseId"+value[1]).html("");
			
		   	var selectMonth = document.getElementById(val).value;
		   	var paySlipValue = "";
		   			   	
		   	paySlipValue += "<input type=\"file\" name=\"salarySlip\" id=\"salarySlipId\" style=\"width:121px;;\" />";
		   	paySlipValue += "<input type=\"button\" value=\"ADD\" id=\""+val+"\" style=\"width:30px;margin-top:10px;height: 24px;\" onclick=\"addSalarySlip(this.id)\" />";
		   	
		   	$("#payslipsBrowseId"+value[0]).show();
		   	$("#payslipsBrowseId"+value[0]).html(paySlipValue);
		}	
}


function addSalarySlip(val)
{	
	
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	 
	var idValue = val.replace("selected","");  
				
    $("#payslipsDocId").html("");
      
    var flag = 'true';
    
    var fileBrowse = document.getElementById("salarySlipId").value;
   
    if(fileBrowse=="")
    	{
    	 	document.getElementById('waitlight').style.display='none';
    		document.getElementById('waitfade').style.display='none';
    		
    	     flag = 'false';
    	     
    	    
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please browse your file. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  
			 
			 return false;
    	}
    
    if(fileBrowse!="")
    	{  
    	    var ext=fileBrowse.substring(fileBrowse.lastIndexOf('.')+1);
		    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf","xlsx","xls");						    
		    var condition = "NotGranted";
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}
			if(condition=="NotGranted")
				{
				document.getElementById('waitlight').style.display='none';
	    		document.getElementById('waitfade').style.display='none';
	    		
				   flag = 'false';
				   
  			     document.getElementById('sociallight').style.display='block';
  			     document.getElementById('socialfade').style.display='block';
  			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
  				 $("#socialdisplaySharedDocDiv").html("pdf/office/image files are only allowed. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  
	
  				 return false;
				}
    	}
    
    var fileDetails = document.getElementById("salarySlipId");
	var fileSize = fileDetails.files[0];
	var fileSizeinBytes = fileSize.size;
	var sizeinKB = +fileSizeinBytes / 1024;
	var sizeinMB = +sizeinKB / 1024;
	
	if(sizeinMB>2)
		{
		     document.getElementById('waitlight').style.display='none';
		     document.getElementById('waitfade').style.display='none';
		
		     flag = 'false';
		   
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Salary slip size should not more then 2 MB . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
		  
			 return false;
		}
	
    if(flag=='true')
	    {      
   	    	     	
		    	$.ajax({
					type : "post",
					url : "checksessionforIndividual.html",
					success : function(response) {
						
						if(response=="Exist")
							{							     
											$.ajax
										    ({
												type : "post",				
												url : "validateFileStorage.html",
												data:"givenfileSize="+sizeinKB,
												success : function(response) {
																
													if(response=="withinStorage")
													{						     
														var formData = new FormData($("#Employeeform"+idValue)[0]);				    		    	
													    formData.append("month_year",document.getElementById(val).value);				    		 
													    formData.append("orgName",document.getElementsByName("cmyName").value);
													 			
													   
													    
													 		$.ajax({
													 			
													 	        url: "saveSalarySlip.html",
													 	        type: 'POST',
													 	        data: formData,
													 	        //async: false,
													 	        success: function (data) { 
													 	        	
													 	        	
													 	        	document.getElementById('waitlight').style.display='none';
													 	    		document.getElementById('waitfade').style.display='none';
													 	    		
													 	        	var salSlipdoc = data.split("$$$");
													 	        	
													 	        	var salSlipName = "";
													 	        	
													 	        	if(salSlipdoc[0]!="No Data")
													 	        		{
															 	        	var salSlipMonthYear = salSlipdoc[0];			 	        	
															 	        	var salSlipDocName = salSlipdoc[1];	
															 	        	var salSlipCrDate = salSlipdoc[2];	
															 	        	
															 	        	var salSlipCrDateArray = salSlipCrDate.split(",");
															 	        	
															 	        	salSlipMonthYear = salSlipMonthYear.substring(0,salSlipMonthYear.length-1);			 	        	
															 	        	var salSlipMonthYearArray = salSlipMonthYear.split(",");			 	        				 	        
															 	        	var paySlipMothValues = "";			 	        				 	        	
															 	        							 	        	
															 	            paySlipMothValues += "<h1>Your salary slip has been added successfully .To save the details please click on save before leaving this page.</h1>";			 	        		  			 	        		   
															 	             
															 	            $("#payslipsDocId"+idValue).fadeIn(500);  	
															 	        	$("#payslipsDocId"+idValue).html(paySlipMothValues);							 	        	
																	       // $("#payslipsDocId"+idValue).fadeOut(6000);
																	         
																	        document.getElementById("salarySlipId").value = "";
																	        
																	        $("#payslipsBrowseId"+idValue).hide();
																	        
															 	        	document.getElementById("paySlipNames"+idValue).value += salSlipdoc[2]+",";
															 	        	
															 	        	
															 	        	// display all salary slips in div
															 	        	
															 	        	var divValue = "";
															 	        	divValue += "<h1><u>Your added Salary Slips</u></h1><br>"
															 	        	divValue += "<ul>";
															 	        	for(var s=salSlipMonthYearArray.length-1,n=1;s<salSlipMonthYearArray.length;s++,n++)
															 	        		{
															 	        		     //divValue += "<li>"+salSlipMonthYearArray[s]+" salary slip was added on "+salSlipCrDateArray[0]+".</li>";
															 	        		     document.getElementById("uploadedSlipNames"+idValue).value += salSlipMonthYearArray[s] + "TME" + salSlipCrDate +",";
															 	        		}
															 	        	
															 	        	
															 	        	var listOfSalSlips = document.getElementById("uploadedSlipNames"+idValue).value;
															 	        	listOfSalSlips = listOfSalSlips.substring(0,listOfSalSlips.length-1);
															 	        	var listOfSalSlipsArray = listOfSalSlips.split(",");
															 	        	for(var s=0,n=1;s<listOfSalSlipsArray.length;s++,n++)
														 	        		{
														 	        		     divValue += "<li>"+listOfSalSlipsArray[s].split("TME")[0]+" salary slip has been added successfully on "+listOfSalSlipsArray[s].split("TME")[1]+".</li>";														 	        		     
														 	        		}
															 	        	
															 	        	divValue += "</ul>";
															 	        	$("#displayAllSalarySlipDiv"+idValue).show();
															 	        	$("#displayAllSalarySlipDiv"+idValue).html(divValue);
															 	        	
															 	        	$("#PayslipShow"+idValue).html("<h1>Your salary slips added once you will save that will appear here.</h1>")
															 	        	
													 	        		}	 	        	
													 	        },
													 	        cache: false,
													 	        contentType: false,
													 	        processData: false
													 	    });
													}
													else
													{
														document.getElementById('waitlight').style.display='none';
											    		document.getElementById('waitfade').style.display='none';
											    		
														alert("Failed due to storage limitation,to upload more document migrate your Plan!");
													}			
												},
										    });
							
							}
						else
							{
							   $("#sessionlight").show();
						       $("#sessionfade").show();
							}
					}
				}); 
 
    	}
    
}

function currentWorking(fmName,panelId)
{		
	if(document.getElementById(panelId+"current").checked)
		{					 		 		    			    	    		    	     
				    $.ajax
				        ({
				    		type : "post",				
				    		url : "checkEmpEndDate.html",	
				    		success : function(response) {
				    			
				    		if(response=='Yes')
				    				{
				    				    alert("Give a date to previous company 'to date' of job duration !");	
				    				    document.getElementById(panelId+"current").checked = false;				    				    
				    				}
				    		else
				    				{
				    					if(document.getElementById(panelId+"current").checked)
				    					{
				    						document.getElementById(panelId+"jobtoId").value  =  "Presently Working";
				    						
				    						var presentStatus = validateJobDurationDateForToDate(panelId);
				    						if(presentStatus=="Available")
				    							{
				    							    alert("Your job duartion is avilable previous companys job duration !");
				    							    document.getElementById(panelId+"jobtoId").value  =  "";
				    							    document.getElementById(panelId+"current").checked = false;	
				    							}
				    						else
				    							{
				    							    document.getElementById(panelId+"jobtoId").value  =  "Presently Working";
				    							}
				    						
				    					}
				    					else
				    					{
				    					    document.getElementById(panelId+"jobtoId").value  = "";	
				    					}				    					
				    					//generatePayslip(fmName,panelId);
				    				}
				    		},
				    		
				    	});		    	
		    			
		}	
	else
		{
		    document.getElementById(panelId+"jobtoId").value = "";
		}
}




/* Main Header Actions  */

function gotoProfile()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoMyDocument()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoSharingManager()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoStore()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoAlerts()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
}

function gotoPlans()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
}

function gotoSetings()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}


function signOutviaFun(url)
{
	 window.location=url;
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}	

function hideAddvertise()
{
   $("#ProfileHeadingDiv").fadeOut(90000);	
}

function visibleAccordian(accordianId)
{
   $("#"+accordianId).slideToggle("slow");
}


// Later on edited script for design


function uploadEmpDoc(contentDivId,successDivId,fileId)
{
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	
	var fileName = document.getElementById(fileId).value;
	
	if(fileName!="")
	{
		    var fileBrowse = fileName;
		    var flag = 'true';
		    
		        if(fileBrowse!="")
		    	{  
		    	    var ext=fileBrowse.substring(fileBrowse.lastIndexOf('.')+1);
				    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf","xlsx","xls");						    
				    var condition = "NotGranted";
				    for(var m=0;m<extension.length;m++)
						{
						    if(ext==extension[m])
						    	{				    	    
						    	   condition="Granted";				    	    
						    	}				    
						}
					if(condition=="NotGranted")
						{
							document.getElementById('waitlight').style.display='none';
			    			document.getElementById('waitfade').style.display='none';
			    		
							 flag = 'false';
						   
		  			    	 document.getElementById('sociallight').style.display='block';
		  			     	document.getElementById('socialfade').style.display='block';
		  			    	 document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		  					 $("#socialdisplaySharedDocDiv").html("pdf/office/image files are only allowed. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  
			
		  					 return false;
						}
		    	}
		    
		    var fileDetails = document.getElementById(fileId);
			var fileSize = fileDetails.files[0];
			var fileSizeinBytes = fileSize.size;
			var sizeinKB = +fileSizeinBytes / 1024;
			var sizeinMB = +sizeinKB / 1024;
			
			if(sizeinMB>2)
			{
				     document.getElementById('waitlight').style.display='none';
				     document.getElementById('waitfade').style.display='none';
				
				     flag = 'false';
				   
				     document.getElementById('sociallight').style.display='block';
				     document.getElementById('socialfade').style.display='block';
				     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
					 $("#socialdisplaySharedDocDiv").html("File size should not more then 2 MB . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
				  
					 return false;
			}
		
		if(flag=='true')
			{
			   document.getElementById('waitlight').style.display='block';
		       document.getElementById('waitfade').style.display='block';
			   setTimeout(function(){ successMessageFun(contentDivId,successDivId); }, 10000);			
			}
		
	}
	else
	{
		 document.getElementById('waitlight').style.display='none';
		 document.getElementById('waitfade').style.display='none';
		
	     document.getElementById('sociallight').style.display='block';
	     document.getElementById('socialfade').style.display='block';
	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		 $("#socialdisplaySharedDocDiv").html("Please browse your file. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  		 
	}
	
}

function successMessageFun(contentDivId,successDivId)
{
	document.getElementById('waitlight').style.display='none';
	document.getElementById('waitfade').style.display='none';
    
	$("#"+contentDivId).hide();
    $("#"+successDivId).html("<br><b>Upload successfully</b>");
}
</script>
 
 <!-- End Of  -->
 
</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocumentsfocus"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettings" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 
 <!-- Login Starts Here -->
 
 
        <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
            
            
           <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                
                <div id="loginBox">  
                          
                    <span></span>   
                    
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="#" onclick="signOutviaFun('signOut.html')"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                    
                </div>
                
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			             		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
							  <ul>
							     <li id="Hi-B-1"><a href="myDocumentsMain.html"  id="Hi-B-1" ><span>KYC Documents</span></a></li>
								 <li id="academic_document"><a href="getacademicdetails.html"  id="Hi-B-2" ><span>Academic Documents</span></a></li>															
								 <li id="employee_document"><a href="employeeDetails.html"  id="employee_documentactive"><span>Employment Documents</span></a></li>
								 <li id="financial_document"><a href="#"  id="Hi-B-3" onclick="myDocuments2('search_financialDoc.html')"><span>Financial Documents</span></a></li>
							  </ul>
						   </div>    		
					</div>
					
					 </div>
					
					<div class="rightside">
					
					    
					<!-- Advertisment Div for Entire Page  -->
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
							%>

									<div id="ProfileHeadingDiv" style="display:block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>	
							        		
					   <!-- Advertisment Div for Entire Page  -->
					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span><%=name%>'s Employment Documents</span>								   
							</h2>
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
						
						
						<%
					       String monthArray[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
					     
					       String panel = (String)request.getAttribute("panel"); 
					       String empDetails = (String)request.getAttribute("employeeDetails");
					         
					       String panel_ary[] = panel.split("/");                                // List of Pannel i.e Company Names in an array 
					       
					       String empDetails_ary[] = empDetails.split("#");                     // List of All company Details(Contain basic info and Document Name of emp and salary slips with # Seperator)
					         
					       System.out.println("Employement Details  are  "+empDetails);
					       
					     %>
											
						
								
							<div id="organizationname">
                                  
                                    <input class="utivieweddocinput" type="text" name="cmyName" id="cmyNameId" style="width:100px;" title="Organization Name limit for 40 Character" maxlength="40" placeholder="Enter full name of your Organization." /><div id="orgNameDiv" class="error" style="display:none;margin-left:24px;margin-top:-64px;"></div>
								
								    <input type="button" value="ADD" onclick="createEmployee()" />
								    
							</div>
								
							<div  id="panel-1" style="width:100%;">  
								
								   <%if(!panel.equals(""))     // IF Panel Are Exist or Not
								       {%>
								            <%for(int i=0;i<panel_ary.length;i++)    // As per the Panel name We generate Employee details
								              {%>  
								                    <% String cmyWithDuration = "";
                                                       String durArray[] = empDetails.split("#"); %>
                                                       
	                                                       <%for(int c=0;c<durArray.length;c++)
	                                                       {
	                                                     	  if(durArray[c].startsWith(panel_ary[i]))
	                                                     	  {
	                                                     		  String durArray2[] = durArray[c].split("@@@");
	                                                     		  String durArray3[] = durArray2[0].split("&&");
	                                                     		  if(!durArray3[3].equals("No Data"))
	                                                     		  {
	                                                     			  cmyWithDuration += ""+panel_ary[i]+"" + "<b> : "+ durArray3[3]+"-"+durArray3[4] +"</b>";       			 
	                                                     		  }  
	                                                     		  else
	                                                     		  {
	                                                     			  cmyWithDuration += ""+panel_ary[i]+"" +  "<b> : Not Filled yet !</b>";        			  
	                                                     		  }
	                                                     	  }
	                                                       }%>
                                                    
                                                    
                                                    <h3 align="left" onclick="visibleAccordian('<%=cmyWithDuration.replaceAll(" ","")%>accordianClass')"><%=cmyWithDuration%></h3>                                <!-- /* displaying the company Name with duartion in the Accordian Pannel   */ -->
                                                    
		                                                    <%for(int j=0;j<empDetails_ary.length;j++){%>    	 <!-- for Loop for Employee details -->     
		                                                          
		                                                          <%
		                                                            String empdata[] = empDetails_ary[j].split("@@@");  		   	
		                                              	   			String docsData = empdata[0];  		 
		                                            	   			String str = panel_ary[i]+"Pannel";  		  		
		                                            	   			String docsData_ary[] = docsData.split("&&"); 
		                                            	   		  %>
		                                                          
		                                                          
				                                                          <%if(panel_ary[i].equals(docsData_ary[0]))      // If pannel is matched with emp data so differentiate the values
				                                              	   		     {%>
				                                              	   		        
				                                              	   		                  <%String imageData = "";				                                              	 	   		    
							                                              	   		   		String offerLetter = "";
							                                              	   		   		String hikeLetter = "";
							                                              	   		   		String expLetter = "";
							                                              	   		   		String relLetter = "";
							                                              	   		   		String formLetter = "";
							                                              	   		   		String salarySlip = "";
							                                              	   		   		   		
							                                              	   		   		if(empdata[1].contains(","))
							                                              	   		   		{  			
							                                              	   		   			imageData = empdata[1];
							                                              	   		   			String strAry[] = imageData.split(",");
							                                              	   		   		    offerLetter = strAry[0];
							                                              	   		   			hikeLetter = strAry[1];
							                                              	   		   			expLetter = strAry[2];
							                                              	   		   			relLetter = strAry[3];
							                                              	   		   			formLetter = strAry[4]; 
							                                              	   		   			
							                                              	   		   			if(strAry.length>5)
							                                              	   		   			{
							                                              	   			   			for(int p=5;p<strAry.length;p++)
							                                              	   			   			{
							                                              	   			   				salarySlip += strAry[p]+",";   							
							                                              	   			   			}	   			
							                                              	   			   			salarySlip = salarySlip.substring(0, salarySlip.length()-1);	   			
							                                              	   		   			}
							                                              	   		   			
							                                              	   		   		}%>
							                                              	   		   		
							                                              	   		   		
							                                              	   		   		
							                                              	   		   		 <div align="left" id="<%=cmyWithDuration.replaceAll(" ","")%>accordianClass" style="display:none;">   <!-- Each Employee Details Div -->
        
																							       
																							       <%String formName = panel_ary[i];
																							        formName = formName.replaceAll(" ","");       
																							        %> 
																							        
																							         <a id="editregistrationhelp" href="employee_details_help.html" target="_blank">
																									 	   	<p class="askhelp" tittle="Help For Basic Details"> </p>
																								     </a>
																											
																							          <form name="Employeeform<%=formName%>" id="Employeeform<%=formName%>" enctype="multipart/form-data">
                                                                                                          
                                                                                                          <% String panelExeptSpace = panel_ary[i].replaceAll(" ", ""); %>
                                                                                                          
                                                                                                          <!-- Div For Submit and History button -->
                                                                                                               
                                                                                                                <div  id="kycdocumentpage" style="margin-top: 10px;">   
               	  	  
																													    <input type="hidden" value="" id="paySlipNames<%=formName%>" name="paySlipsName" />
																													  	
																													  	<%if(!docsData.contains("No Data")){ %> 	
																													  	                                                                                 
																												              <input  class="historyinner_button" type="button" value="History" id="<%=panel_ary[i]%>" onclick="showEmpHistory(this.id)" />
																												              
																												        <%} %>&nbsp;
																												        
																												        <input class="editinner_button" type="button"  value="Edit"  id="<%=panel_ary[i]%>#saveIdUpper#<%=formName%>#<%=panelExeptSpace%>" onclick="saveEmployee(this.id)" />
																												     																												     																												      
																										       </div> 
                                                                                                          
                                                                                                          
                                                                                                          
                                                                                                          
                                                                                                          
                                                                                                          
                                                                                                          
                                                                                                               <div id="employeedetails">        <!-- Employee details Information Division -->
                                                                                                               
                                                                                                                      <div id="<%=panel_ary[i]%>orgNameDiv" class="error" style="display:none;margin-left:237px;margin-top:-25px;"></div>
                                                                                                                      <div id="<%=panel_ary[i]%>jobDesigDiv" class="error" style="display:none;margin-left:237px;margin-top:-25px;"></div>
                                                                                                                      <div id="<%=panel_ary[i]%>jobProDiv" class="error" style="display:none;margin-left:237px;margin-top:-25px;"></div>
                                                                                                                      <div id="<%=panel_ary[i]%>jobDurDiv" class="error" style="display:none;margin-left:237px;margin-top:-25px;"></div>
                                                                                                                       
 																														<ul>

																													            <li>
																													                 <!-- <h4>Organization Name :</h4> -->
																													                 <input type="hidden" style="width:300px;" disabled="true" value="<%=docsData_ary[0]%>" name="cmyName" id="<%=panel_ary[i]%>cmyNameId" />																											               																											               
																													            </li>
																													            
																													            <li>
																															            <%if(docsData_ary[1].equals("No Data")) {%>
																															                <h4>Job designation </h4>
																															                <input type="text" style="width:716px;" disabled="true" maxlength="30" placeholder="Job Designation"  name="jobDesignation" id="<%=panel_ary[i]%>jobDesigId" onkeyup="hideErrormessage('<%=panel_ary[i]%>')"/>
																															            <%} else { %>
																															                 <h4>Job designation </h4> 
																															                  <input type="text" style="width:716px;" disabled="true" maxlength="30"  value="<%=docsData_ary[1]%>" name="jobDesignation" id="<%=panel_ary[i]%>jobDesigId" onkeyup="hideErrormessage('<%=panel_ary[i]%>')"/>
																															            <%} %>
																															    </li>
																															    
																															    <li>
																															            <%if(docsData_ary[2].equals("No Data")) {%>
																															                <h4>Job summary </h4>
																															                <textarea disabled="true" style="width:721px; height:70px;" maxlength="300"  name="jobPro" placeholder="Job Profile" id="<%=panel_ary[i]%>jobProId" onkeyup="hideErrormessage('<%=panel_ary[i]%>')"></textarea> 
																															            <%} else { %>
																															                <h4>Job summary </h4>
																															                <textarea disabled="true" style="width:721px;height:70px;" maxlength="300"   name="jobPro" id="<%=panel_ary[i]%>jobProId" onkeyup="hideErrormessage('<%=panel_ary[i]%>')"><%=docsData_ary[2]%></textarea>        
																															            <%} %>
																															            
																															    </li>
																															    
																															    <li>
																															            <%if(docsData_ary[3].equals("No Data")) {%>
																															                <b>Job Duration </b>from <input style="width:90px;margin:5px;" class="datepick2" placeholder="From Date" name="jobfrom" id="<%=panel_ary[i]%>jobfromId" disabled="true" readonly onchange="validateJobDurationDateForFromDate(this.id),hideErrormessage('<%=panel_ary[i]%>')"/> 
																															                                       to <input style="width:90px;margin:5px;" placeholder="To Date" class="datepick2" disabled="true" name="jobto" id="<%=panel_ary[i]%>jobtoId" readonly onchange="validateJobDurationDateForToDate('<%=panel_ary[i]%>'),checkJobDurationIsInProperOrderOrNot('<%=formName%>','<%=panel_ary[i]%>'),hideErrormessage('<%=panel_ary[i]%>')"/>
																															                                          <input type="checkbox" style="width:7px;margin-left:3px; margin:0 10px;padding: 5px;vertical-align:middle;" id="<%=panel_ary[i]%>current" readonly disabled="true" onclick="currentWorking('<%=formName%>','<%=panel_ary[i]%>')"/>Currently Working
																															            <%}
																															        
																															            else{%>
																															                <b>Job Duration </b>from <input style="width:90px;margin:5px;" class="datepick2" placeholder="From Date" value="<%=docsData_ary[3]%>" name="jobfrom" id="<%=panel_ary[i]%>jobfromId" readonly disabled="true" onchange="validateJobDurationDateForFromDate(this.id),hideErrormessage('<%=panel_ary[i]%>')"/>
																															                                        to <input style="width:90px;margin:5px;" placeholder="To Date" class="datepick2" disabled="true" value="<%=docsData_ary[4]%>" name="jobto" id="<%=panel_ary[i]%>jobtoId" readonly onchange="validateJobDurationDateForToDate('<%=panel_ary[i]%>'),checkJobDurationIsInProperOrderOrNot('<%=formName%>','<%=panel_ary[i]%>'),hideErrormessage('<%=panel_ary[i]%>')"/>
																															                                        
																															                                        <%if(!docsData_ary[4].equals("Presently Working")){%>
																															                                              
																															                                               <input type="checkbox" style="width:7px; margin:5px;margin-left:3px;" id="<%=panel_ary[i]%>current" disabled="true" onclick="currentWorking('<%=formName%>','<%=panel_ary[i]%>')"/>
																															                                               
																															                                       <%}else{%>
																															                                       
																															                                               <input type="checkbox" style="width:7px;" id="<%=panel_ary[i]%>current" checked="true" disabled="true" onclick="currentWorking('<%=formName%>','<%=panel_ary[i]%>')"/>
																															                                               
																															                                       <%}%>Currently Working 
																															                
																															            <%} %>
																															                      
																															   </li>
																														    
                                                                                                                        </ul>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                                                                                               </div>   			<!-- End of Employee details Information Division -->
                                                                                                               
                                                                                                               
                                                                                                               <div id="ImageSlider<%=formName%>" class="kycemployeedetailimg">  <!-- Division of Employee Document -->
                                                                                                               
                                                                                                                            <div class="documenttitle">
                                                                                                                            <span></span>
                                                                                                                            <h1>Employment Documents</h1></div> 
                                                                                                                            
                                                                                                                            <div id="kycemployeedetaildocumentDiv">	  	
                                                                                                                            
                                                                                                                            
                                                                                                                                      <!-- Div for Offer letter Document -->
                                                                                                                                     
                                                                                                                                    
                                                                                                                                     
                                                                                                                                      <div  style="float:left; position: relative;margin-top:0px;" >
		 
																																				 <div id="<%=panel_ary[i]%>ShowOfferDiv" style="display:block;" >
																																						 <ul>
																																						 
																																							 <li class="kyc_empviewinner">
																																							 <b>Offer letter</b>
																																							
																																							       <%if(!offerLetter.equals("No Document")){ %>
																																							                
																																									             <%if(!offerLetter.contains("jpeg") && !offerLetter.contains("jpg") && !offerLetter.contains("gif") && !offerLetter.contains("png")) { %>
																																									             
																																									                        <%if(offerLetter.contains("pdf")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																																									                            		
																																									                        <%}else if(offerLetter.contains("doc")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(offerLetter.contains("docx")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(offerLetter.contains("xls")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                         
																																									                        <%}else { %>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%} %>
																																									                        
																																									                        	             
																																					                     					<a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=offerLetter%>"></a>
																																							            					
																																							            					<div  id="<%=panelExeptSpace%>offerEdit" style="display:none;float:right;">
																																							            					     <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowOfferDiv%<%=panel_ary[i]%>hideOfferDiv')">  </a> 
																																							            					</div> 
																																							            					
																																									             <%}else{ %> 
																																									             
																																									           			 <%--  <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+offerLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img> --%>
																																									 	   		                <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+offerLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img>
																																									 	   		                
																																									 	   					 <a   class="mydocumentviewpan" onclick="viewDocumentInPopUp('<%=offerLetter%>')" ></a>
																																					                    					 <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=offerLetter%>"></a>
																																							            					
																																							            					<div  id="<%=panelExeptSpace%>offerEdit" style="display:none;float:right;">
																																							            					     <a   class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowOfferDiv%<%=panel_ary[i]%>hideOfferDiv')">  </a> 
																																							            					</div> 
																																									             <%} %>  
																																							 			 
																																							             
																																							       
																																							       <%}else{%> 
																																							
																																										<img src="resources/images/noDocumentFound.png" width="150" height="80"/>
																																							 
																																							            <div id="<%=panelExeptSpace%>offerEdit" style="display:none;float:right; margin-top: 2px;">
																																							            
																																							                <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowOfferDiv%<%=panel_ary[i]%>hideOfferDiv')" > </a>
																																							            </div>
																																							       <%}%>	
																																							       	
																																							 </li>	
																																							 				
																																						 </ul>
																																				 </div>
																																				 
																																				 <div id="<%=panel_ary[i]%>hideOfferDiv" style="display:none;float:right; margin-top: 2px;">
																																						 <ul>
																																						
																																							 <li class="kyc_empviewinner"> 
																																									 <b>Offer letter</b>
																																									 
																																									 <div id="<%=panel_ary[i].replaceAll(" ", "")%>contentOfferDiv">
																																									 
																																										 <input style="width:120px;padding:5px" id="offerfile<%=panel_ary[i]%>" type="file" name="offletter"/>
																																										 <br>
																																										 <a  class="cancelbule_button"  onclick="EditEmpDoc('<%=panel_ary[i]%>ShowOfferDiv%<%=panel_ary[i]%>hideOfferDiv')"></a>
																																										 
																																										 <a  class="uploadimgbutton"  onclick="uploadEmpDoc('<%=panel_ary[i].replaceAll(" ", "")%>contentOfferDiv','<%=panel_ary[i].replaceAll(" ", "")%>successOfferDiv','offerfile<%=panel_ary[i]%>')"></a> 
																																										 
																																									 </div>
																																									 
																																									 <div id="<%=panel_ary[i].replaceAll(" ", "")%>successOfferDiv">
																																									 
																																									 </div>
																																							 </li>	
																																						 
																																						 </ul>
																																				 </div>
																																	 
																																	 </div>          
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             <!-- Div for Hike letter Document -->   
                                                                                                                                      
                                                                                                                                      
                                                                                                                                      
                                                                                                                                      <div  id="emmployeeletter" style="float: left; position: relative;">
		  
																																				<div id="<%=panel_ary[i]%>ShowHikeDiv" style="display:block; margin-top: 7px"> 
																																							
																																							<ul>
																																							
																																									<li class="kyc_empviewinner">
																																											<b>Hike letter</b>
																																										
																																											<%if(!hikeLetter.equals("No Document")){ %>
																																											   
																																											   		 <%if(!hikeLetter.contains("jpeg") && !hikeLetter.contains("jpg") && !hikeLetter.contains("gif") && !hikeLetter.contains("png")) { %>
																																									             
																																									                      <%if(hikeLetter.contains("pdf")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																																									                            		
																																									                        <%}else if(hikeLetter.contains("doc")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(hikeLetter.contains("docx")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(hikeLetter.contains("xls")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                         
																																									                        <%}else { %>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%} %>																																						    		
																																									             
																																									             			
																																															<a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=hikeLetter%>"> </a>
																																															<div id="<%=panelExeptSpace%>HikeEdit" style="display:none;float:right; margin-top: 2px;">
																																									         				 
																																									         				    <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowHikeDiv%<%=panel_ary[i]%>hideHikeDiv')"> </a>
																																									         		          </div>
																																									                 <%}else{ %> 
																																									                 
																																											      		    <%--  <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+hikeLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img> --%>
																																														     <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+hikeLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img>
																																														     
																																															<a  class="mydocumentviewpan" onclick="viewDocumentInPopUp('<%=hikeLetter%>')"> </a>
																																															<a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=hikeLetter%>"> </a>
																																									         				
																																									         				<div id="<%=panelExeptSpace%>HikeEdit" style="display:none;float:right; margin-top: 2px;">
																																									         				   
																																									         				      <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowHikeDiv%<%=panel_ary[i]%>hideHikeDiv')"> </a>
																																									         		       
																																									         		        </div>
																																													 <%} %>
																																											
																																											    	
																																									         
																																									        <%} else {%>
																																									        
																																									               <img src="resources/images/noDocumentFound.png" width="150" height="80"/>
																																											
																																											       <div id="<%=panelExeptSpace%>HikeEdit" style="display:none;float:right; margin-top: 2px;">
																																											        
																																											           <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowHikeDiv%<%=panel_ary[i]%>hideHikeDiv')"> </a>
																																											       </div>
																																											<%} %>
																																									</li>					
																																							</ul>
																																							
																																				</div>
																																				
																																				 <div id="<%=panel_ary[i]%>hideHikeDiv" style="display:none; margin-top: 7px">
																																						 
																																						 <ul>
																																						
																																								 <li class="kyc_empviewinner">
																																										 <b>Hike letter</b>
																																										
																																										 <div id="<%=panel_ary[i].replaceAll(" ", "")%>contentHikeDiv">
																																										  
																																											 <input style="width:120px;padding:5px" id="hikefile<%=panel_ary[i]%>" type="file" name="hikeletter"/>
																																											 <br>
																																											 <a class="cancelbule_button"  onclick="EditEmpDoc('<%=panel_ary[i]%>ShowHikeDiv%<%=panel_ary[i]%>hideHikeDiv')"></a>
																																											 <a  class="uploadimgbutton"  onclick="uploadEmpDoc('<%=panel_ary[i].replaceAll(" ", "")%>contentHikeDiv','<%=panel_ary[i].replaceAll(" ", "")%>successHikeDiv','hikefile<%=panel_ary[i]%>')"></a> 
																																											 
    																																									 </div>
																																									 
																																										<div id="<%=panel_ary[i].replaceAll(" ", "")%>successHikeDiv"></div>
																																									 
																																											

																																								 </li>
																																						
																																						 </ul>
																																						 
																																				 </div>
																																	 </div>  
																																	 
																																	 
																																	 
																																	         <!-- Div for Experience letter Document -->        
                                                                                                                                      
                                                                                                                                      <div id="emmployeeletter" style="float: left; position: relative;">
					
																																					<div id="<%=panel_ary[i]%>ShowExpDiv" style="display:block; margin-top: 7px">
																																								
																																								<ul>
																																								
																																									<li class="kyc_empviewinner">
																																											<b>Experience letter </b>
																																											<br>
																																											<%if(!expLetter.equals("No Document")){ %>
																																											
																																											      <%if(!expLetter.contains("jpeg") && !expLetter.contains("jpg") && !expLetter.contains("gif") && !expLetter.contains("png")) { %>
																																									             
																																									                        <%if(expLetter.contains("pdf")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																																									                            		
																																									                        <%}else if(expLetter.contains("doc")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(expLetter.contains("docx")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(expLetter.contains("xls")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                         
																																									                        <%}else { %>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%} %>																																							    		
																																									             
																																									                       <br> 																																											      			
																																									              			<a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=expLetter%>"> </a>&nbsp;&nbsp;
																																									             		  
																																									             		   <div id="<%=panelExeptSpace%>ExpEdit" style="display:none;float:right; margin-top: 2px;">
																																									             		     
																																									             		         <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowExpDiv%<%=panel_ary[i]%>hideExpDiv')"> </a>
																																									             		         
																																									                       </div>
																																									                 <%}else{ %>
																																											      
																																											      			<%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+expLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img> --%>
																																											         
																																											               <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+expLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img>
																																											               
																																											      			<a class="mydocumentviewpan" onclick="viewDocumentInPopUp('<%=expLetter%>')"> </a>
																																									              			<a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=expLetter%>"> </a>&nbsp;&nbsp;
																																									             		    
																																									             		    <div id="<%=panelExeptSpace%>ExpEdit" style="display:none;float:right; margin-top: 2px;">
																																									             		    
																																									             		        <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowExpDiv%<%=panel_ary[i]%>hideExpDiv')"> </a>
																																									                        
																																									                        </div>
																																											         
																																											         
																																											          <%} %>
																																											     
																																									        <%} else{%>
																																									        
																																									             <img src="resources/images/noDocumentFound.png" width="150" height="80"/>
																																									             
																																									             <div id="<%=panelExeptSpace%>ExpEdit" style="display:none;float:right; margin-top: 2px;">
																																									             
																																									                   <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowExpDiv%<%=panel_ary[i]%>hideExpDiv')"> </a>
																																									                   
																																									             </div>
																																									             
																																									             
																																									        <%} %> 
																																									</li>										
																																								</ul>
																																								
																																					</div>
																																					
																																					 <div id="<%=panel_ary[i]%>hideExpDiv" style="display:none; margin-top: 7px">
																																							 
																																							 <ul>
																																						
																																									 <li class="kyc_empviewinner">
																																									 <b>Experience letter</b>
																																									 
																																									    <div id="<%=panel_ary[i].replaceAll(" ", "")%>ExpOfferDiv">
																																									    
																																											 <input style="width:120px;padding:5px" id="expfile<%=panel_ary[i]%>" type="file" name="expletter"/>
																																											 <br> 
																																											 <a class="cancelbule_button"  onclick="EditEmpDoc('<%=panel_ary[i]%>ShowExpDiv%<%=panel_ary[i]%>hideExpDiv')"></a>
																																											 
																																											 <a  class="uploadimgbutton"  onclick="uploadEmpDoc('<%=panel_ary[i].replaceAll(" ", "")%>ExpOfferDiv','<%=panel_ary[i].replaceAll(" ", "")%>successExpDiv','expfile<%=panel_ary[i]%>')"></a> 
																																										
																																										</div>
																																									 
																																										<div id="<%=panel_ary[i].replaceAll(" ", "")%>successExpDiv"></div>
																																									 
										
																																									 </li>
																																							
																																							 </ul>
																																							 
																																					 </div>
																																					 
																																	</div>		
                                                                                                                            
                                                                                                                            
                                                                                                                            					 <!-- Div for Reliving letter Document -->        
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    <div  id="emmployeeletter" style="float: left; position: relative;">
		  
																																				<div id="<%=panel_ary[i]%>ShowRelDiv" style="display:block; margin-top: 7px">
																																						
																																						<ul> 
																																						
																																							<li class="kyc_empviewinner">
																																									<b>Reliving letter</b>
																																									<br>
																																									
																																									<%if(!relLetter.equals("No Document")){ %>
																																									
																																									 				<%if(!relLetter.contains("jpeg") && !relLetter.contains("jpg") && !relLetter.contains("gif") && !relLetter.contains("png")) { %>
																																									             
																																									                         <%if(relLetter.contains("pdf")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																																									                            		
																																									                        <%}else if(relLetter.contains("doc")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(relLetter.contains("docx")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(relLetter.contains("xls")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                         
																																									                        <%}else { %>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%} %>																																								    		
																																									             
																																									                             <br> 
																																									     							
																																							             							<a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=relLetter%>"> </a>
																																							            						   
																																							            						   <div id="<%=panelExeptSpace%>RelEdit" style="display:none;float:right; margin-top: 2px;">
																																							            						    
																																							            						    <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowRelDiv%<%=panel_ary[i]%>hideRelDiv')"> </a>
																																									                               </div>
																																									             
																																									                 <%}else{ %>
																																									                 
																																									    					 	
																																									     					<img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+relLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img>
																																									     							
																																									     						<br> 
																																									     							<a class="mydocumentviewpan" onclick="viewDocumentInPopUp('<%=relLetter%>')"> </a>
																																							             							<a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=relLetter%>"> </a>
																																							            						   
																																							            						    <div id="<%=panelExeptSpace%>RelEdit" style="display:none;float:right; margin-top: 2px;">
																																							            						    
																																							            						        <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowRelDiv%<%=panel_ary[i]%>hideRelDiv')"> </a>
																																									                                
																																									                                </div>
																																									     			  <%} %>
																																									     
																																							             
																																							       <%} else{%>
																																							          
																																							             <img src="resources/images/noDocumentFound.png" width="150" height="80"/>
																																							             
																																							             <div id="<%=panelExeptSpace%>RelEdit" style="display:none;float:right; margin-top: 2px;">
																																							             
																																							                 <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowRelDiv%<%=panel_ary[i]%>hideRelDiv')"> </a>
																																							             
																																							             </div>
																																							       <%} %> 
																																							       
																																							</li>	
																																											
																																						</ul>
																																						
																																				</div>
																																				
																																				<div id="<%=panel_ary[i]%>hideRelDiv" style="display:none; margin-top:7px">
																																						
																																						 <ul>
																																						 
																																							 <li class="kyc_empviewinner">
																																									 <b>Reliving Letter</b>
																																									 <br>
																																									
																																									<div id="<%=panel_ary[i].replaceAll(" ", "")%>contentRelDiv">
																																									 
																																										 <input style="width:120px;padding:5px" id="relfile<%=panel_ary[i]%>" type="file" name="relletter"/>
																																										 <br>
																																										
																																										 <a class="cancelbule_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowRelDiv%<%=panel_ary[i]%>hideRelDiv')"></a>
																																										 
																																										 <a  class="uploadimgbutton"  onclick="uploadEmpDoc('<%=panel_ary[i].replaceAll(" ", "")%>contentRelDiv','<%=panel_ary[i].replaceAll(" ", "")%>successRelDiv','relfile<%=panel_ary[i]%>')"></a> 
																																											
																																									</div>
																																									 
																																									<div id="<%=panel_ary[i].replaceAll(" ", "")%>successRelDiv"></div>
																																									 
																																							 </li>
																																							
																																						 </ul>
																																						 
																																				 </div>
																																	 </div>			 
	
	
																																			 <!-- Div for Form-16  Document -->        
																																			 
																																			 
																																	 <div id="emmployeeletter" style="float: left; position: relative;">
																																					
																																					<div id="<%=panel_ary[i]%>ShowFormDiv" style="display:block; margin-top:7px">
																																							
																																							<ul>
																																						
																																								<li class="kyc_empviewinner">
																																										<b>Form 16</b>
																																										<br>
																																										<%if(!formLetter.equals("No Document")){ %>
																																													
																																													<%if(!formLetter.contains("jpeg") && !formLetter.contains("jpg") && !formLetter.contains("gif") && !formLetter.contains("png")) { %>
																																									             
																																									                          <%if(formLetter.contains("pdf")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																																									                            		
																																									                        <%}else if(formLetter.contains("doc")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(formLetter.contains("docx")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(formLetter.contains("xls")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                         
																																									                        <%}else { %>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%} %>																																									    		
																																									             
																																									                             <br> 
																																															     
																																													             <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=formLetter%>"> </a>&nbsp;&nbsp;
																																													            <div id="<%=panelExeptSpace%>FormEdit" style="display:none;float:right; margin-top: 2px;">
																																							            						    
																																													             <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowFormDiv%<%=panel_ary[i]%>hideFormDiv')"> </a>
																																									                            </div>
																																									                 <%}else{%>
																																									                 
																																										     					<%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+formLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img> --%>
																																										     
																																										     					 <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+formLetter+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img>
																																										     
																																															     <br> 
																																															     <a class="mydocumentviewpan" onclick="viewDocumentInPopUp('<%=formLetter%>')"> </a>
																																													             <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=formLetter%>"> </a>&nbsp;&nbsp;
																																													            
																																													             <div id="<%=panelExeptSpace%>FormEdit" style="display:none;float:right; margin-top: 2px;">
																																							            						     <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowFormDiv%<%=panel_ary[i]%>hideFormDiv')"> </a>
																																							            						 </div>
																																													             
																																										              <%} %>
																																										     
																																								       <%} else{%>
																																								       
																																								             <img src="resources/images/noDocumentFound.png" width="150" height="80"/>
																																								             
																																								             <div id="<%=panelExeptSpace%>FormEdit" style="display:none;float:right; margin-top: 2px;">
																																								                 
																																								                 <a class="editacod_button" onclick="EditEmpDoc('<%=panel_ary[i]%>ShowFormDiv%<%=panel_ary[i]%>hideFormDiv')"> </a>
																																								             
																																								             </div>
																																								       <%} %>
																																								</li>	
																																													
																																							</ul>
																																							
																																					</div>
																																		
																																				     <div id="<%=panel_ary[i]%>hideFormDiv" style="display:none; margin-top: 7px">
																																							 
																																							 <ul>
																																							
																																								 <li class="kyc_empviewinner">
																																										 <b>Form 16</b>
																																									
																																									<div id="<%=panel_ary[i].replaceAll(" ", "")%>contentFormDiv"> 
																																										 <input style="width:120px;padding:5px" id="formfile<%=panel_ary[i]%>" type="file" name="formletter"  />
																																										 <br>
																																										 <a class="cancelbule_button"  onclick="EditEmpDoc('<%=panel_ary[i]%>ShowFormDiv%<%=panel_ary[i]%>hideFormDiv')"></a>
																																										 <a  class="uploadimgbutton"  onclick="uploadEmpDoc('<%=panel_ary[i].replaceAll(" ", "")%>contentFormDiv','<%=panel_ary[i].replaceAll(" ", "")%>successFormDiv','formfile<%=panel_ary[i]%>')"></a> 
																																											
																																									</div>
																																									 
																																									<div id="<%=panel_ary[i].replaceAll(" ", "")%>successFormDiv"></div>
																																									 
																																								 </li>
																																							
																																							 </ul>
																																							 
																																					 </div>
																																					 
																																    </div>
																																    
																																    
                                                                                                                     
                                                                                                                            </div>
                                                                                                               
                                                                                                               
                                                                                                               </div>		<!-- End of  Division of Employee Document -->
                                                                                                               
                                                                                                               
                                                                                                               
                                                                                                               
                                                                                                                <% int year = (Integer)request.getAttribute("year"); %>
                                                                                                               
                                                                                                               
                                                                                                               
                                                                                                               <div id="empsalaryslip">   <!-- Start Division for Salary Slip -->
                                                                                                               
                                                                                                                       
                                                                                                                       <div class="documenttitle">
                                                                                                                       <span></span>
                                                                                                                       <h1>SalarySlips</h1></div>
                                                                                                                       
                                                                                         
                                                                                                                       <div id="kycemployeedetailimgbg">	    
                                                                                                                         
                                                                                                                               <%--   <%if(!salarySlip.isEmpty()) { %> --%>   <!-- I have hidden the If condition for being now because of unnecessary  -->
                                                                                                                                 
                                                                                                                                        <div id="<%=formName%>PayslipShow" class="epmloyeedocimg">
        
																																				     <%if(!salarySlip.isEmpty()) {      //    If(cond) Salary Slip are there or nor
																																				         
																																				    	   
																																				    	 
																																				    	 String monthYearName = "";
																																				    	 
																																				    	 if(empdata[2].length()>0)
																																				    	     {
																																				    	    	 String monthNameArray[] = {"January", "February", "March", "April", "May",
																																				    	    				                "June", "July", "August", "September", "October",
																																				    	    				                "November", "December"};
																																				    	    	 
																																				    	    	 
																																				    	    	 String monthYearArray[] = empdata[2].split(","); 
																																				    	    	 
																																				    	    	 for(int mn=0;mn<monthYearArray.length;mn++)    // outer monthYear array
																																				    	    	 {
																																				    	    		 String monthValue = monthYearArray[mn].split("-")[0];
																																				    	    		 int outerindex = 0;
																																	    	    					 int innerindex = 0;
																																				    	    				 for(int imn=0;imn<monthYearArray.length;imn++)    // inner monthYear array
																																							    	    	 {
																																				    	    					 String imonthValue = monthYearArray[imn].split("-")[0];
																																					    	    				 for(int imName=0;imName<monthNameArray.length;imName++)
																																							    	    		 {
																																					    	    					 if(monthValue.equals(monthNameArray[imName]))
																																					    	    					 {
																																					    	    						 outerindex = imName;
																																					    	    					 }
																																					    	    					 if(imonthValue.equals(monthNameArray[imName]))
																																					    	    					 {
																																					    	    						 innerindex = imName;
																																					    	    					 }
																																							    	    		 }
																																					    	    				 if(outerindex>innerindex)
																																					    	    				 {
																																					    	    					 String temp = monthYearArray[mn];
																																					    	    					 monthYearArray[mn] = monthYearArray[imn];
																																						    	    				 monthYearArray[imn] = temp;
																																					    	    				 }
																																							    	    	 }
																																				    	    																														    	    		 
																																				    	    	 }
																																				    	    	 
																																				    	    	 for(int k=0;k<monthYearArray.length;k++)
																																				    	    	 {																															    	    		 
																																				    	    		 int yearValue = Integer.parseInt(monthYearArray[k].split("-")[1]);
																																				    	    		 for(int l=0;l<monthYearArray.length;l++)
																																				    	    		 {
																																				    	    			 int tempyearValue = Integer.parseInt(monthYearArray[l].split("-")[1]);
																																				    	    			 if(yearValue>tempyearValue)
																																				    	    			 {
																																				    	    				 String temp = monthYearArray[k];
																																				    	    				 monthYearArray[k] = monthYearArray[l];
																																				    	    				 monthYearArray[l] = temp;
																																				    	    			 }
																																				    	    		 }																																    	    		 
																																				    	    	 }
																																				    	    	 
																																				    	    	
																																				    	    	 for(int my=0;my<monthYearArray.length;my++)
																																				    	    	 {
																																				    	    		 monthYearName += monthYearArray[my] + ",";																														    	    		 
																																				    	    	 }
																																				    	    	 
																																				    	    	 if(monthYearName.length()>0)
																																				    	    	 {
																																			    	    		     monthYearName = monthYearName.substring(0, monthYearName.length()-1);
																																				    	    	 }
																																				    	    	
																																				    	     }
																																				     
																																				     
																																						     String monthName[] = monthYearName.split(","); 
																																						     String salSlipArray1[] = salarySlip.split(","); 
																																						     String sortedSlipName = "";
																																						     for(int sort=0;sort<monthName.length;sort++)
																																			    	    	 {
																																						    	 for(int sorti=0;sorti<salSlipArray1.length;sorti++)
																																				    	    	 {
																																						    		 if(salSlipArray1[sorti].contains(monthName[sort]))
																																						    		 {
																																						    			 sortedSlipName += salSlipArray1[sorti] + ",";	
																																						    		 }																										    	    		 
																																				    	    	 }																												    	    		 
																																			    	    	 }
																																						     
																																						     if(sortedSlipName.length()>0)
																																						     {
																																						    	 sortedSlipName =sortedSlipName.substring(0, sortedSlipName.length()-1);
																																						     }
																																						     
																																						     String salSlipArray[] = sortedSlipName.split(","); 
																																						     
																																						    %>
																																						    
																																						    <div class=salaryslipdetails-ext>
																																											
																																											<ul>
																																											
																																						     <%for(int s=0;s<salSlipArray.length;s++) { %>   <!-- Start of for(;;) loop For no.of Salary Slip -->
																																				     																																				    																																																																														
																																										
																																											
																																												<li class="panviewinner_acad">
																																														<h4><%=monthName[s]%></h4>
																																														
																																														<%if(!salSlipArray[s].contains("jpeg") && !salSlipArray[s].contains("jpg") && !salSlipArray[s].contains("gif") && !salSlipArray[s].contains("png")) { %>
																																									             
																																									                           <%if(salSlipArray[s].contains("pdf")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
																																									                            		
																																									                        <%}else if(salSlipArray[s].contains("doc")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(salSlipArray[s].contains("docx")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%}else if(salSlipArray[s].contains("xls")) {%>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                         
																																									                        <%}else { %>
																																									                        
																																									                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
																																									                        
																																									                        <%} %>																																							    		
																																									                          
																																									                           <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=salSlipArray[s]%>"> </a>
																																									             
										  																															                    <%}else{ %>
																																														
																																														        <%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+salSlipArray[s]+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img> --%>
																																																
																																																<img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+salSlipArray[s]+"&docCategory=Employment_DOC"%>"   width="150" height="80" /></img>
																																																
																																																<a class="mydocumentviewpan" onclick="viewDocumentInPopUp('<%=salSlipArray[s]%>')"> </a>
																																												                <a class="mydocumentdownloadpan" href="downloadEmployeeDocument.html?fileName=<%=salSlipArray[s]%>"> </a>
																																												                
																																														  <%} %>
																																														
																																														
																																												</li>
																																																	
																																											
																																																
																																					     
																																				                 <%} %>   <!-- End Of for(;;) loop For no.of Salary Slip -->
																																				                    
																																				                    </ul>
																																											
																																										</div>
																																				      <%}else {%>         <!-- End Of If(cond) Salary Slip are there or nor -->
																																				      
																																				              	<div id="PayslipShow<%=formName%>" ><h1>Please upload your salary slips.</h1></div>
																																				      	
																																				      <%} %>																																                
																																	    </div>
                                                                                                                                        
                                                                                                                                        
                                                                                                                                <%--  <%}%> --%>
                                                                                                                       
                                                                                                                       </div>
                                                                                                                      
                                                                                                               </div> 		<!-- End of Division Salary Slip -->
                                                                                                                
                                                                                                               
                                                                                                               
                                                                                                               
                                                                                                               <!-- Div For Generating Salary Slip Drop down -->
                                                                                                               
                                                                                                               
                                                                                                               <input class="addsalaryslip" type="button" id="<%=formName%>addsalBtn" style="display:none;margin-top:10px;width:120px;" value="Add Salary Slip" onclick="generatePayslip('<%=formName%>','<%=panel_ary[i]%>')" />
                                                                                                               
                                                                                                               <input class="addsalaryslip" type="button" id="<%=formName%>cancelBtn" value="Cancel" style="display:none;margin-top:10px;margin-left:10px;width:82px;" onclick="hidePaySlip('<%=formName%>','<%=panel_ary[i]%>')" />
                                                                                                               
                                                                                                                                                                                                                              
                                                                                                               <div id="<%=formName%>PayslipSlider" style="margin-top:25px;overflow-x:hidden;width:811px;display:none;height:auto;min-height:86px;" class="empsalaryslippayslip_div"> 
         
																											        <div  id="<%=formName%>payslipDropDownDiv" style="margin-left:27px;margin-top:20px;">  </div>      <!--  This Div will show the Month Year Drop Down of Salary Slip  -->
																											    
																											        <div id="payslipsBrowseId<%=formName%>" style="float: left;height: auto;margin-left:0px;margin-top:5px;position: relative;" > </div>   <!--  This Div will show  a browse component as per the Month-Year of Salary Slip  -->
																											    
																											        <div id="payslipsDocId<%=formName%>"  class="paysliplistdoc1">     </div>    <!-- This will Show a Success Message After upload the Message -->
																											        
																											        
																											        <input type="hidden" id="uploadedSlipNames<%=formName%>" />
																											        
																											        <div id="displayAllSalarySlipDiv<%=formName%>" style="display:none;" class="smalldivstyle">     </div>    <!-- This will Show a Success Message After upload the Message -->
																											        
																																			    																								        																											        																											              	 		 		 		 		 		 
																											   </div>	 																												                                                                                                                 
                                                                                                                                                                                                                                                                                                                                          
                                                                                                               
                                                                                                               
                                                                                                                <!-- End of Div For Generating Salary Slip Drop down -->
                                                                                                               
                                                                                                               
                                                                                                               <!-- Div For Submit and History button -->
                                                                                                               
                                                                                                                <div  id="kycdocumentpage" >   
               	  	  
																													    <input type="hidden" value="" id="paySlipNames<%=formName%>" name="paySlipsName" />
																													  	
																													  	<%if(!docsData.contains("No Data")){ %> 	
																													  	                                                                                 
																												              <input  class="historyinner_button" type="button" value="History" id="<%=panel_ary[i]%>" onclick="showEmpHistory(this.id)" />
																												              
																												        <%} %>&nbsp;
																												        
																												        <input class="editinner_button" type="button"  value="Edit"  id="<%=panel_ary[i]%>#saveId#<%=formName%>#<%=panelExeptSpace%>" onclick="saveEmployee(this.id)" />
																												     																												     																												      
																										       </div> 
                                                                                                               
                                                                                                     </form>
                                                                                                     
                                                                                                     
																							        
																							 </div>       <!-- End of Each Employee Details Div -->
																							 
																							 
																							 
																							 
				                                              	   		     
				                                              	   		    <%}%>    <!-- // End of If(cond) pannel is matched with emp data so differentiate the values -->
		                                              	   		  
		       
		                                                    <%}%>		<!-- End of for Loop for Employee details -->  
		                                                    
                                                    
                                                       
								                    
								             <%}%>   <!-- End of for loop Panel Array -->
								             
								     <%}%>     <!-- End of If(cond) Panel -->
								   
								       
								       
							</div>	
								
										                        
								
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

  
	
	
	
	
	
	
	    
</div> <!--end of right div  -->

	</div>

</div>
	
<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>					
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>
<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">

								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
									
                      </div>
                    
		   	   
	</div>
	
	<div id="waitfade" ></div>
	
<!-- EOF Waiting Div -->


    <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
		
	
	<!-- Document Pop up Div -->
	
	<div id="kycDocPopUplight">
		
		<div class="colsebutton"  onclick="fadeoutPopUp()"><img height="22" width="22" class="mydocumentpage_clbtn"  src="resources/images/close_button.png" /></div>
		         		  	
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:0; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF Document Pop up Div -->	

	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->
	
	
	<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
    <!-- EOF report Div -->
	
</body>
</html>
     	