<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script>

function viewDocumentInPopUpfromHistory(fileName)
{
	$.ajax({
		type : "get",				
		url : "viewAcademic.html",
		data: "fileName="+fileName,
		success : function(response) {
		
			document.getElementById('kycDocPopUplight').style.display='block';
	        document.getElementById('kycDocPopUpfade').style.display='block';
	        $("#kycDocPopUpDisplay").html(response);
	        			
		},		
	});		 		 		 		
	
}

function submitForHistory(docType)
{
	window.location = "academicHistorydetail.html?id="+docType;
}
</script>


</head>
<body >

<%

  String ind_id = request.getAttribute("user").toString(); 
  String year = request.getAttribute("year").toString(); 
  String docs_data = (String)request.getAttribute("docsData");
  String docName = (String)request.getAttribute("docName");
  String doctype[] = docs_data.split("-");
  String docs_dataArray[] = doctype[1].split(",");
  
%>
       
       <div id="panel-1" style="width:100%;">
			   			  			   
			   
			         <div id="kycdocumentpage" class="kycdocumentpage">  	<!-- start of kycdocumentpage -->														
			                        
				                  <div class="kycdocumentdetails-ext">      <!-- Division for Left side Hierarchy other component with data is in left side -->
				                         
				                         <table>
					                       
					                       <% for(int i=0;i<docs_dataArray.length;i++) {%>
					                         
					                         <tr style="margin-top:15px;">
					                              <td style="width:auto;"><b><%=docs_dataArray[i].split(":")[0]%></b></td>
					                              <td style="width:10px;">:</td>
					                              <td style="width:auto;"><%=docs_dataArray[i].split(":")[1]%></td>			                              
					                         </tr>
					                         
					                       <%} %>
					                       
					                      </table>  					                       					                          
					              </div>      <!-- End of Division for Left side Hierarchy other component with data is in left side -->
					
				                     <div class="academicdocumenttitle">
				                     
										<h1>
											<span></span>
										 Academic Document </h1>
										</div>    	
					    
								  <div class="academicdetailsimg">     <!-- Division for right side Hierarchy file component with data  -->
									             
							              <ul>
							               
							               <%if(!docName.equals("No Data")) {
							               
							            	    String docNameArray[] = docName.split(",");							            	    
							            	    for(int dn=0;dn<docNameArray.length;dn++)
							            	    {
							                    %>
							                   <li>
							                    
							                    <%if(docNameArray[dn].contains("pdf")) {%>
							                    
							                  		   <img src="resources/images/PNA_PDF.png"   width="150" height="100" /></img>
																						              
			                                           <a href="downloadAcademic.html?fileName=<%=docNameArray[dn]%>" >Download</a>
									                    
			                                    <%} else {%>
			                                         	  
			                                         	<img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+docNameArray[dn]+"&docCategory=ACADEMIC_DOC"%>"   width="150" height="100" /></img>
													
										                                                         
		                                                <a onclick="viewDocumentInPopUpfromHistory('<%=docNameArray[dn]%>')">View</a>
			                                            <a href="downloadAcademic.html?fileName=<%=docNameArray[dn]%>" >Download</a>
			                                            
			                                        <%} %>                                        
							                    </li>
							                    <%} %>  
							                <%} else {%> 
							                
							                          <h1> Academic Documents are not uploaded yet !</h1>
							                <%} %>    
							              </ul>
									   
									</div>                               <!-- Division for right side Hierarchy file component with data -->
						           	    <div>
						           	  <button id="viewDocument" onClick="submitForHistory('<%=doctype[0]%>')">Back To History</button>   
						           	    </div>						           	    						           	           	      				                          
			           </div>      <!-- End of kycdocumentpage -->		
			                				 				   					  
	        
	                             
	           
		</div>   <!-- End of panel-1 division  -->
			
       
                
                
               
                
                                                    
</body>
</html>