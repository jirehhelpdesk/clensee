 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Documents</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script> 

<script src="resources/onlineresources/code.jquery.com.jquery-1.10.2.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page language="java" import="java.util.*" %>
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}
</style>




<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}



</style>

<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>




<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 10px;
    display: none;
    height: auto;
    width: auto;
   left: 50%;
    margin-left: -350px;
    margin-top: -220px;
    position: fixed;
    top: 50%;
    z-index: 1002;

}

</style>
 
<script>

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function viewDocumentInPopUp(fileName)
{
	$.ajax({
		type : "get",				
		url : "viewfinancial.html",
		data: "fileName="+fileName,
		success : function(response) {
		
			document.getElementById('kycDocPopUplight').style.display='block';
	        document.getElementById('kycDocPopUpfade').style.display='block';
	        $("#kycDocPopUpDisplay").html(response);
	        			
		},		
	});		 		 		 		
	
}

</script> 
 



<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
        $("#waitdisplaySharedDocDiv").html("<img src=\"resources/ajax_loaderimage/registerloading.gif\" style=\"margin-left:121px;height:50px;width:50px;\"></img><h4>Processing please wait for portal response...... </h4>");
        
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
		    $('#searchhomepage').html("");		 
		    $('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();
			$('.topprofilestyle').hide();
	    }
}

function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}

</script>





<script>
/*home page*/

function myhome(handlerToHit) {
	 
	
	 $.ajax({
			type : "post",
			url : handlerToHit,
			cache : false,
			success : function(response) {
				     $("#homepage")
					.html(response);
				     location.reload();
			},
			error : function() {
				alert('Error while fetching response');
			}
		});
	 		 		 		
	
} 

/*end home page*/

/*profile page*/


/*end profile*/



/* All Scripts for MyDocument pannel  */

	
	function myDocuments1(handlerToHit) {
		 
		var v= '1stUser';
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				data : "status=" +v,
				success : function(response) {
					$("#center-body-div").hide();					
					$("#searchicon").hide();          					
					 $("#viewUsers1")
						.html(response);
					 					   
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>KYC DOCUMENTS</span></h2>");
	}
	
	function myDocuments2(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 window.location = "search_financialDoc.html";
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		
	}
	
	function myDocuments3(handlerToHit) {
		 
		
		 $.ajax({
				type : "get",
				url : handlerToHit,
				cache : false,
				success : function(response) {
					
					$("#center-body-div").hide();
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>EMPLOYMENT DOCUMENTS</span></h2>");
	}
	
	function myDocuments4(handlerToHit) {
		 
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				cache : false,
				success : function(response) {
										
					 $("#viewUsers1")
						.html(response);
										
				},
				error : function() {
					alert('Error while fetching response');
				}
			});
		 		 		 		
		$("#titleBar").html("<h2><span>Employment Details</span></h2>");
	}
	

	/* End of MyDocument Scripts  */
 
	 	 	 	 	 	 		 
</script>



<script src="resources/js/kyc_side_login.js"></script>



<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>
 

<!-- Script for Reselect Financial Document Type -->

 
<script>
$(function() {
  $( "#datepicker" ).datepicker();
});
</script>

<!-- Script for Validation -->
<script type="text/javascript">


function showModeAlert(docMode)
{
	if(docMode=="Select Mode")
		{
			$("#fileModeDiv").show();
			$("#fileModeDiv").html("Select Document Mode !");
			return false;
		}
	else
		{		
			$("#fileModeDiv").hide();		
		}
}
</script>
<!-- End of Script for Validation -->

<script> 
$(document).ready(function(){
  $("#financialAddDocumentFlip").click(function(){
    $("#financialAddDocumentPannel").slideToggle("slow");
  });
});
</script>
 
 <!-- End of Script and CSS for Financial Documents -->
 
 
 <script type="text/javascript">

function editFinancial()
{	
	
	document.getElementById('waitlight').style.display='block';
	document.getElementById('waitfade').style.display='block';
	$("#waitdisplaySharedDocDiv").html("<img src=\"resources/ajax_loaderimage/registerloading.gif\" style=\"margin-left:121px;height:50px;width:50px;\"></img><h4>Processing please wait for portal response...... </h4>");
	 
	var flag = 'true';
	
	var filedetails = document.getElementById("financialfileId").value;
	
	if(filedetails=="")
		{
			document.getElementById('waitlight').style.display='none';
			document.getElementById('waitfade').style.display='none';
			
		    flag='false';
		    alert("Select a file !");
		}
	
	if(filedetails!="")
	{
		
		    var ext=filedetails.substring(filedetails.lastIndexOf('.')+1);
		    var extension = new Array("jpg","jpeg","gif","png","doc","docx","pdf");						    
		    var condition = "NotGranted";
		   
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}
			if(condition=="NotGranted")
				{
				   flag = 'false';
				   alert("pdf/office/image files are only allowed!");
				}
			
	}
		
	 var fileDetails1 = document.getElementById("financialfileId");
	 var fileSize = fileDetails1.files[0];
	 var fileSizeinBytes = fileSize.size;
	 var sizeinKB = +fileSizeinBytes / 1024;
	 var sizeinMB = +sizeinKB / 1024;
	 
	 if(sizeinMB>2)
		 {
		 	flag = 'false';
		    alert("File size should not more then 2 MB !");
		 }
	 
	if(flag=='true')
		{
		 		 		 			
			$.ajax
		    ({
				type : "post",				
				url : "validateFileStorage.html",	
				data: "givenfileSize="+sizeinKB,
				success : function(response) {
					
					if(response=="withinStorage")
						{

						   var formData = new FormData($("#FinancialEditForm")[0]);
					    	
							formData.append("fromYear",document.getElementById("fyid").value);
							formData.append("toYear",document.getElementById("tyid").value);
								
								$.ajax({
									type : "POST",
									url : "financialDocument.html",
									data : formData,
									async: false,
									success : function(response) {
						               
										alert("Financial Document added Successfully .");
										window.location = "search_financialDoc.html";
						           
									},
									cache: false,
							        contentType: false,
							        processData: false,
									error : function() {
										alert('Error while fetching response');
									}
								});
								$("#titleBar").html("<h2><span>FINANCIAL DOCUMENT</span></h2>");
						}
					else
						{
						      alert("Failed due to storage limitation,to Upload more document migrate your Plan!");
						}
				},
				
			});	
		 
		}	
}

function cancelForm()
{
	$("#viewDiv").show();
	$("#formDiv").hide();
	$("#viewFinDoc").show();
	$("#viewBrowseDoc").hide();
	$("#editId").show();
	$("#fileChangeId").hide();
}

function showBrowse(id)
{
	if(id=="upload")
		{
			$("#viewFinDoc").hide();
			$("#viewBrowseDoc").show();
		}
	else
		{
			$("#viewFinDoc").show();
			$("#viewBrowseDoc").hide();
		}
	
}

function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(9000);	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}	

</script>
 
</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<%
	String months[] = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October",
			"November", "December" };
%>

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a href="profiletohome.html"  class="myprofile" ><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a href="myDocumentsMain.html" class="mydocumentsfocus"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a href="generateCode.html" class="accessmanager"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore" href="individualstore.html"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a href="myvisitors.html" class="alearticons"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a href="individualviewplan.html" class="myplans"> <br> <br>
							 Plans
					</a></li>
					<li id="settingMenu"><a href="setting.html" class="mysettings">
					 <br> <br>
							 Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 
 
 					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
            
            
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
			
			
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
            
            
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
             
        
					
		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
							  <ul>
							     <li id="Hi-B-1"><a href="myDocumentsMain.html"  id="Hi-B-1" ><span>KYC Documents</span></a></li>
								 <li id="academic_document"><a href="getacademicdetails.html"  id="Hi-B-2" ><span>Academic Documents</span></a></li>															
								 <li id="employee_document"><a href="employeeDetails.html"  id="Hi-B-4"><span>Employment Documents</span></a></li>
								 <li id="financial_document"><a href="#"  id="financial_documentactive" onclick="myDocuments2('search_financialDoc.html')"><span>Financial Documents</span></a></li>
							 </ul>
						   </div>    		
					</div>
					
					 </div>
					
					<div class="rightside">
					
					   
						
					    <!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

									

					<div id="ProfileHeadingDiv" style="display: block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>

					
							        		
					   <!-- Advertisment Div for Entire Page  -->
					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span>Financial Details</span>								   
							</h2>
						 </div>
					
									<div class="module-body">
										
										<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
											
											    <!-- Start of Required Division for viewUser1 not go beyond  -->	
																					
															
														<% String docsData = (String)request.getAttribute("docsData");
						         
						            int indId = (Integer)request.getAttribute("indId");
						            
						           String finHierarchy =  (String)request.getAttribute("finHierarchy");
						           
						           String heirarchyArray[] = finHierarchy.split(",");
						           
						           String fromYear = docsData.split("&&")[0].split("-")[0];
						           String toYear = docsData.split("&&")[0].split("-")[1];
						         %>
						         
						         <div class="FinancialEditForm">
						          
						          <form name="FinancialEditForm" id="FinancialEditForm"  enctype="multipart/form-data">                     
						                              
						       
						        <div id="viewDiv" class="viewdiv">
						             <table>
						                 <tr>
						                 <td style="width:auto;"><b>Financial Year</b></td>
								         <td style="width:10px;">:</td>
								         <td style="width:auto;"> <%=fromYear%>-<%=toYear%></td>		
								         <input type="hidden" id="fyid" name="fromYear" value="<%=fromYear%>"/>
						                 <input type="hidden" id="tyid" name="toYear" value="<%=toYear%>" />             		            		           
								         </tr>
								      
								         <tr>
								         <td style="width:auto;"><b>Financial Document Type</b></td>
								          <td style="width:10px;">:</td>
								          <td style="width:auto;"><%=docsData.split("&&")[1]%>
								          </td><input type="hidden" id="finDocType" name="docType" value="<%=docsData.split("&&")[1]%>"/>
								          </tr> 
								     
								          <tr >
								          <td style="width:auto;">
								          <b>Document Mode</b></td> 
								          <td style="width:10px;">:</td>
								          <td style="width:auto;">
								          <%=docsData.split("&&")[2]%>
								          </td>
								          <input type="hidden" id="finDocMode" name="docMode" value="<%=docsData.split("&&")[2]%>"/>
								          </tr> 		
						           		  <tr >
						           		  <td style="width:auto;"><b>Added on:</b> </td>
							                      <td style="width:10px;">:</td>
							                      <td style="width:auto;">
							                      <%=docsData.split("&&")[3]%></td>
							              </tr>			             		  
						              </table>
						              </div>
						             
						               
						             
									      
								       <div id="viewBrowseDoc" class="viewBrowseDoc" style="display:none;"> 			          			              
								                <input type="file" name="upl" id="financialfileId"/>
								                <br>
								                <a href="#" id="cancel" onclick="showBrowse(this.id)">Cancel</a>
								                <a href="#"  onclick="editFinancial()">Save</a>
								                <div id="fileDiv" class="error" style="display:none;margin-left: 283px;margin-top: -22px;"></div>			       
								       </div>  	
									       		       
								
									   
											<div id="viewFinDoc" class="viewFinDoc">
								                  		
								                  		<ul><li class="panviewinner_acad">
								                  		
								                  			 <b>Financial Document</b>
										                     
										                     <%if(!docsData.split("&&")[4].contains("jpeg") && !docsData.split("&&")[4].contains("jpg") && !docsData.split("&&")[4].contains("gif") && !docsData.split("&&")[4].contains("png")) { %>
																																																	             
											                            <%if(docsData.split("&&")[4].contains("pdf")) {%>
																																																		                        
												                           <img src="resources/images/PNA_PDF.png"   width="150" height="80" /></img>
												                            		
												                        <%}else if(docsData.split("&&")[4].contains("doc")) {%>
												                        
												                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
												                        
												                        <%}else if(docsData.split("&&")[4].contains("docx")) {%>
												                        
												                            <img src="resources/images/PNA_DOC.png"   width="150" height="80" /></img>
												                        
												                        <%}else if(docsData.split("&&")[4].contains("xls")) {%>
												                        
												                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
												                         
												                        <%}else { %>
											                        
											                            <img src="resources/images/PNA_XLS.png"   width="150" height="80" /></img>
											                        
											                           <%} %>																																								    		
											                          <br>
											                           <a class="mydocumentdownloadpan" href="downloadfinancial.html?fileName=<%=docsData.split("&&")[4]%>"> </a>
											                           <a class="editacod_button" href="#" id="upload" onclick="showBrowse(this.id)"> </a> 
											                           
											                <%}else{ %>
										           				                     
												                     <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+docsData.split("&&")[4]+"&docCategory=Financial_DOC"%>"   width="200" height="150" /></img>    			                
													                 <br>					          
													                 <a class="mydocumentviewpan" onclick="viewDocumentInPopUp('<%=docsData.split("&&")[4]%>')"> </a>
													                 <a class="mydocumentdownloadpan" href="downloadfinancial.html?fileName=<%=docsData.split("&&")[4]%>"> </a> 
													                 <a class="editacod_button" href="#" id="upload" onclick="showBrowse(this.id)"> </a>  
											                 
											                 <%}%>	  
											                 
											                 </li></ul>	       
											 </div>
									   
									  </form>   
									     </div> 
			               		
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

  
	
	
	
	
	
	
	    
</div> <!--end of right div  -->

	</div>

</div>
					
					
					
		<%
			ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			String version=resource.getString("kycversion");
			String versionDate=resource.getString("kyclatestDate");
			%>			
					
			
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		    <div id="waitdisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:300px;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		               <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
		    </div>		   
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->
	

   <!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
		<!-- Document Pop up Div -->
	
	<div id="kycDocPopUplight">
			
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:0; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF Document Pop up Div -->	

	
</body>
</html>
     	