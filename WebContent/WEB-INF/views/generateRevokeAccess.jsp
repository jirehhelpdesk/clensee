<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<!-- <script src="resources/table/jquery-1.11.1.js"></script> -->

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<style>
.backspace { margin-left: 100px; }
</style>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>
<script type="text/javascript">


function getIndAccDetails() {
	
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
	
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
					document.getElementById("utiButId").style.background = "#ebebeb";
					document.getElementById("utiButId").style.color = "#4a484b";
					
					document.getElementById("utiIndId").style.background = "#00b6f5";
					document.getElementById("utiIndId").style.color = "#fff";
					$.ajax({
						
							type : "Post",
							url : "revokeAccessOfIndividual.html",				
							success : function(response) {

								   document.getElementById('waitlight').style.display='none';
							       document.getElementById('waitfade').style.display='none';
							       
								if(response.length>0)
								{
								var accdetails = response.split(",");
								
								var index = 0;
								var usersTableData = "<table class='CSSTableGenerator' align=\"center\" style=\"width:98% !important;\"><tr><th align=\"center\">S.NO</th><th align=\"center\">Name</th><th align=\"center\">Date</th><th align=\"center\">Description</th><th align=\"center\">Access Status</th></tr>";
								
								for ( var i = 0; i < accdetails.length; i = i + 5) {
									index += 1;
									var buttonValue = "";
									if(accdetails[3 + i]==1)
										{
											buttonValue = buttonValue+"<input type=\"button\" value=\"Revoke\" id=\""+accdetails[0 + i]+","+accdetails[1 + i]+"\" onClick=\"Inactive_Access(this.value,this.id)\"/>";
										}
									else
										{
											buttonValue = buttonValue+"Inactive";
											/* buttonValue = buttonValue+"<input type=\"button\" id=\""+accdetails[0 + i]+","+accdetails[1 + i]+"\" value=\"INACTIVE\" onClick=\"Active_Access(this.value,this.id)\"/>"; */
							 			} 
							 usersTableData += "<tr><td>" + (index)
							 				+ "</td><td>" + accdetails[4 + i]
											+ "</td><td>" + accdetails[1 + i]
											+ "</td><td style=\"width: 309px;\">" + accdetails[2 + i]											
											+ "</td><td>"+buttonValue
											+ "</td></tr>";								
								}
								usersTableData += "</table>";
								$('#History').html(usersTableData);
								}
								else
									{
									     $('#History').html("</h1>No documents you have shared till now!</h1>");
									}
								$("#titleBar").html("<h2><span>Individual Access Status</span></h2>");
							},
							
						});
				}
			else
				{
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 
  }

function Inactive_Access(value,id)
{	
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
     
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
						 var status = value;
						 var datval = id.split(",");
						 var kycid = datval[0];
						 var time = datval[1];
						
						$.ajax({
							type : "Post",
							url : "InactiveAccess.html",	
							data: "kycid="+kycid+"&date="+time+"&status="+status,
							success : function(response) {
								
								  document.getElementById('waitlight').style.display='none';
							       document.getElementById('waitfade').style.display='none';
							       
								 document.getElementById('sociallight').style.display='block';
								 document.getElementById('socialfade').style.display='block';
								 document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
								 $("#socialdisplaySharedDocDiv").html(response+" <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  
							    
								 getIndAccDetails();
								 
							},
							error : function(e) {
								alert('Error: ' + e);
							}
						});
				}
			else
				{
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 
	
	
}

function Active_Access(value,id)
{	
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
     
	 var status = value;
	 var datval = id.split(",");
	 var kycid = datval[0];
	 var time = datval[1];
	$
	.ajax({
		type : "Post",
		url : "Active_Access.html",	
		data: "kycid="+kycid+"&date="+time+"&status="+status,
		success : function(response) {
			
			   document.getElementById('waitlight').style.display='none';
		       document.getElementById('waitfade').style.display='none';
		       
			 document.getElementById('sociallight').style.background = "none repeat scroll 0 0 #e9ffd9";
		     document.getElementById('sociallight').style.border= "1px solid #a6ca8a";
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     $("#socialdisplaySharedDocDiv").html("<h3> "+response+"  <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </h3> </div>");   

			getIndAccDetails();
			
		},
		
	});
	
}

function getUtiAccessDetails()
{		
	
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
     
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
						document.getElementById("utiButId").style.background = "#00b6f5";
						document.getElementById("utiButId").style.color = "#fff";
						
						document.getElementById("utiIndId").style.background = "#ebebeb";
						document.getElementById("utiIndId").style.color = "#4a484b";
						
						$.ajax({
							type : "Post",
							url : "revokeAccessFromUtiliser.html",				
							success : function(response) {
		
								   document.getElementById('waitlight').style.display='none';
							       document.getElementById('waitfade').style.display='none';
							       
								if(response!="No Data")
								{
								var accdetails = response.split(",");
								
								var index = 0;
								var usersTableData = "<table class='CSSTableGenerator' align=\"center\" style=\"width:98% !important;\"><tr><th align=\"center\">S.NO</th><th align=\"center\">Name</th><th align=\"center\">Date</th><th align=\"center\">Description</th><th align=\"center\">Access Status</th></tr>";
								
								for ( var i = 0; i < accdetails.length; i = i + 5) {
									index += 1;
									var buttonValue = "";
									
									if(accdetails[4 + i]==1)
										{
										  buttonValue = buttonValue+"<input type=\"button\" value=\"Active\" id=\""+accdetails[0 + i]+","+accdetails[2 + i]+","+index+"\" onClick=\"changeUtiliserStatus(this.value,this.id)\"/>";
										}
									else
										{
										buttonValue = buttonValue+"Inactive";
										  /* buttonValue = buttonValue+"<input type=\"button\" id=\""+accdetails[0 + i]+","+accdetails[2 + i]+"\" value=\"INACTIVE\" onClick=\"changeUtiliserStatus(this.value,this.id)\"/>"; */
										} 
									
									usersTableData += "<tr><td>" + (index)
											+ "</td><td>" + accdetails[1 + i] 
											+ "</td><td>" + accdetails[2 + i]
											+ "</td><td style=\"width: 309px;\">" + accdetails[3 + i]											
											+ "</td><td>"+buttonValue
											+ "</td></tr>";								
								}
								
								usersTableData += "</table>";
								
								  $('#History').html(usersTableData);
								
								}
								else
									{
									     $('#History').html("</h1>You have not given access to anyone !</h1>");
									}
								
								$("#titleBar").html("<h2><span>Utilizer Access Status</span></h2>");
							},
							
						});   
				}
			else
				{
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 
	
	
}

function changeUtiliserStatus(statusValue,utiId)
{
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
     
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
						 var status = statusValue;
						 var datval = utiId.split(",");
						 var kycid = datval[0];
						 var time = datval[1];
						 var i = datval[2];
						 
						 $.ajax({
							 
								type : "Post",
								url : "validateRevokeAccess.html",
					            success : function(response) {
		
					        	 var j = response;
					        	
					        	 if(j > i)
					        		 {
						        		 $.ajax({
						
						        				type : "Post",
						        				url : "changeutiliseraccess.html",	
						        				data: "kycid="+kycid+"&date="+time+"&status="+status,
						        				success : function(response) {
						        					
						        					   document.getElementById('waitlight').style.display='none';
						        				       document.getElementById('waitfade').style.display='none';
						        				       
						        					 document.getElementById('sociallight').style.display='block';
													 document.getElementById('socialfade').style.display='block';
													 document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
													 $("#socialdisplaySharedDocDiv").html(response+" <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\" /> </div>");  
												    
						        					getUtiAccessDetails();
						        					
						        				},
						        				
						        			});       			
					        		 }
					        	 else
					        		 {
					        		     document.getElementById('waitlight').style.display='none';
								         document.getElementById('waitfade').style.display='none';
								       
					        		     alert("To revoke access,Migrate your plan!");
					        		 }
					         },
						 });										   
				}
			else
				{				
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 
	
  }

</script>

</head>

<body>

<!-- <table align="center">
<tr>
<td><input type="button" value="Revoke Access to Individual"/><span class="backspace"></span> -->


<div id="revoke_tbutton" style="height: 60px;
    margin-top: 20px;
    width: 100%;">

    <input id="utiIndId" type="button" value="Revoke access from Individual" onClick='getIndAccDetails()'/>

	<input id="utiButId" type="button" value="Revoke access from Utilizer" onClick='getUtiAccessDetails()' />
	

</div>


<!-- </td>
</tr>
</table> -->

<div id="History" class="revokeCodeidscroll" >

    
    
</div>
    


</body>
</html>