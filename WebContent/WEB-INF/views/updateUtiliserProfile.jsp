<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<script type="text/javascript">
	
function update_Utiliser() {
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var domain = document.getElementById("domainPatternId").value;
						
						var formData = new FormData($("#createUtiForm")[0]);
						    
						    formData.append("domain",domain);
						    $.ajax({
						        url: "updateUtiliser.html",
						        type: 'POST',
						        data: formData,
						        //async: false,
						        success: function (data) {          
						            
						        	if(data=="success")
						        		{
						        		  alert("Utilizer information has successfully updated !");
						        		  organiseUti('organiseUtiliser.html');
						        		}
						        	else
						        		{
						        		   alert("Failed due to some reason so try again Later !");
						        		}
						        	
						        },
						        cache: false,
						        contentType: false,
						        processData: false
						 });
				}
			else
				{
				      $("#sessionlight").show();
			          $("#sessionfade").show();
				}
		}
	});
	
}
	
	
function getSubDomainValue(id)
{
	var values = "";
	var parentValue = document.getElementById(id).value; 
	
	for(var d=1;d<=10;d++)
		{
		    $("#childRow"+d).hide();
		}
	$.ajax
    ({
		type :"post",				
		url : "subdomain/createutiliser.html",			
		data :"domainValue="+parentValue,	  
		success : function(response) {
			
			var result = response.split(",");
			values += "<option value=\"Select Sub-Domain\">Select Sub-Domain</option>";
			for(var i=0;i<result.length;i++)
				{
				    var optionarray = result[i];
				    var optionValue = optionarray.split(":");				    
				    values += "<option value=\""+optionValue[0]+"\">"+optionValue[1]+"</option>";
				}
			
			$("#childRow1").show();
			$("#parentId1").html(parentValue);
			$("#cb1").html(values);	
		},
		
	});		
		  	
}

function getHeirarchyFromDomain(id)
{
	var choiceValue = document.getElementById(id).value;
	
	if(choiceValue!="Select Sub-Domain")
		{
				
		        var index = id.charAt(id.length-1);
				var nextIndex = +index + 1;
				
				var rowId = "childRow"+nextIndex;
				var parentId = "parentId"+nextIndex;
				var optionId = "cb"+nextIndex;
			   			  
				var values = "";
								
				$.ajax
			    ({
					type :"post",				
					url : "heirarchysubdomain.html",			
					data :"domainValue="+choiceValue+"&currentLevel="+id,	  
					success : function(response) {
						
						var result  = response.split("#");
						var parentValue = result[0];
						
						if(result[1]!="NoChild")
						{
							var optionResult = result[1].split(",");
							values += "<option value=\"Select Sub-Domain\">Select Sub-Domain</option>";
							
							for(var i=0;i<optionResult.length;i++)
								{
								    var optionarray = optionResult[i];
								    var optionValue = optionarray.split(":");
								    values += "<option value=\""+optionValue[0]+"\">"+optionValue[1]+"</option>";							    
								}
							
							$("#"+rowId).show();
							$("#"+parentId).html(parentValue);
							$("#"+optionId).html(values);	
							
						}
					},
					
				});					
		}
	else
		{
		        alert("Choose Sub Domain !");
		}	
}

function domainSlectionOpeation(id)
{
	if(id=="close")
		{
			var obj = document.getElementById('abc'); 	
			obj.style.display='none';
		}
	if(id=="submit")
		{		
		   var numOfElements = document.domainSelectionForm.elements.length - 1;		
		   var domainLevelValues = "";
		   for(var i=0;i<numOfElements;i++)
			   {
			       var elementValue = document.domainSelectionForm.elements[i].value;			       	      
			       if(elementValue!="")
			    	   {
			               domainLevelValues += elementValue + ",";
			    	   }			       
			   }
		   
		   domainLevelValues = domainLevelValues.substring(0, domainLevelValues.length-1);
		   
		   $.ajax
		    ({
				type :"post",				
				url : "getselectiondomainName.html",			
				data :"selectedList="+domainLevelValues,	  
				success : function(response) {
					
					var values = "";
					var heirarchyNames = response.split(",");
					var domainDirectory = "";
					
					for(var d=0;d<heirarchyNames.length;d++)
					{					  
						domainDirectory += heirarchyNames[d]+">";
					}
					domainDirectory = domainDirectory.substring(0,domainDirectory.length-1);					
					
					values += "<ul style=\"margin-left:-20px;margin-top: 10px;\"><li>";
					values += "--><b>"+heirarchyNames[0]+"       (Main Domain)</b>";
					
					for(var t=1;t<heirarchyNames.length;t++)
						{
						    values += "<ul style=\"margin-left:-20px;margin-top: 10px;\"><li>";
						    values += "-->"+heirarchyNames[t];						    
						}	
					
					for(var t=0;t<heirarchyNames.length;t++)
						{
						    values += "</li></ul>";					  	    
						}	
					
					$("#displayselectedDomainDiv").html(values);
					
					document.getElementById("domainPatternId").value = domainDirectory;
						
					var obj = document.getElementById('abc'); 	
					obj.style.display='none';
				},
				
			});	
		  
		}
	
}

function getPlanNamesAsPerDomain(planDomain)
{
	 if(planDomain!="Select a Plan Domain")
		 {
			 $.ajax
			    ({
					type :"post",				
					url : "get/planname/as/per/plandomain.html",			
					data :"plandomain="+planDomain,	  
					success : function(response) {
						
						if(response!="")
							{
								var values = ""; 
								var planName = response.split(",");
								values += "<option value=\"Select Plan\">Select Plan</option>";
								for(var i=0;i<planName.length;i++)
									{
									    values += "<option value=\""+planName[i]+"\">"+planName[i]+"</option>";
									}
								
								$("#plan_name").html(values);
								$("#plan_name").show();
							}
						else
							{
								$("#plan_name").html("");
								$("#plan_name").hide();
							}
						
					},
			    });
		 }
	 else
		 {
			  $("#plan_name").hide();
		 }
}


function getKycUserDetails()
{
	var kycId = document.getElementById("regkycId").value;
	
	$.ajax
    ({
		type :"post",				
		url : "getdtailsviakycId.html",			
		data :"kycId="+kycId, 
		success : function(response) {
			
			if(response!="")
				{
					var regdata = response.split(",");
					
					document.getElementById("firstName").value = regdata[0];
					document.getElementById("middleName").value = regdata[1];
					document.getElementById("lastName").value = regdata[2];
					document.getElementById("emailid").value = regdata[3];
					
					//gender
				}
			
			
		},
    });
	
}

</script>


</head>

<body>

<%String patt_name = (String)request.getAttribute("patt_name"); 
  String Pattern_arr[] = patt_name.split(",");
 %>

<%String domain = (String)request.getAttribute("domainType").toString();
String domainType[] = domain.split(",");
%>

<%String plan = (String)request.getAttribute("plan_name").toString();
String planType[] = plan.split(",");
%>
<div>
	 <form name="createUtiForm" id="createUtiForm" enctype="multipart/form-data"> 
           
		<table border="0" align="center">
			<tr>
				<td>
					<div class="left" align="left">
					
					 <h1><span>Basic Information</span></h1><br><br>
					 
<table align="left" class="createutiform">

<tr>
<td>
<input type=hidden id="utiUniqueId" name="utiUnqId" value="<%=(Integer)request.getAttribute("uti_ind_id")%>" />
KYC Id<input type="text" id="regkycId" name="utiregkycId" value="<%=(String)request.getAttribute("kycId")%>" /></td>
</tr>
<tr>
<td>First Name<input type="text" id ="firstName" value="<%=(String)request.getAttribute("firstName")%>" name="firstName"></input></td>
</tr>
<tr>
<td>Middle Name<input type="text" id="middleName" value="<%=(String)request.getAttribute("middleName")%>" name="middleName"/></td>
</tr>
<tr>
<td>Last Name<input type="text" id="lastName" value="<%=(String)request.getAttribute("lastName")%>" name="lastName"/></td>
</tr>

<tr>
<td>Gender<select  id="gender" name="gender" style="">
          <option value="Select Gender">Select Gender</option>   
          <%if(((String)request.getAttribute("gender")).equals("Female")) {%>
          
                <option value="Female" selected="selected">Female</option>
                <option value="Male">Male</option>
                
          <%}else if(((String)request.getAttribute("gender")).equals("Male")){%>
                
                <option value="Female">Female</option>
                <option value="Male" selected="selected">Male</option>
          
          <%} %>       
          
          
          </select>
          </td>
</tr>
<tr>
<td>Personal Email ID<input type="text" id="emailid" value="<%=(String)request.getAttribute("utiEmailId")%>" name="emailid"/></td>
</tr>
<tr>
<td>Office Email ID<input type="text" id="ofcemailId" value="<%=(String)request.getAttribute("utiOfcEmailId")%>" name="officeEmailId"/></td>
</tr>
<tr>
<td>Office 1st Ph.No<input type="text" id="ofcphno1" value="<%=(String)request.getAttribute("utiOfcNo1")%>" name="ofcphno1"/></td>
</tr>
<tr>
<td>Office 2nd Ph.No<input type="text" id="ofcphno2" value="<%=(String)request.getAttribute("utiOfcNo2")%>" name="ofcphno2"/></td>
</tr>
<tr>
<td>Branch Office Address<textarea id="ofcaddress"  name="ofcaddress" style="width: ; height: 46px;"><%=(String)request.getAttribute("ofcAddress")%></textarea>
</tr>
<tr>
<td>Head Office Address<textarea id="ofcaddress" name="headofcaddress" style="width: ; height: 46px;"><%=(String)request.getAttribute("headOfcAddress")%></textarea>
</tr>

<tr>
<td><!-- User Photo<input type="file" id="userPhoto" name="userPhoto"/> --></td>
</tr>

</table>
					</div>
				</td>
				
				<td>
					<div class="right" >
					<h1><span></span></h1><br><br>
						
						<table align="right">
						
						<div id="pattern_name_td">
						Pattern Name:			
						<select name="pattern_name" id="pattern_name">
												<option value="Select Pattern Name">Select Pattern Name</option>
												<%for(int i=0;i<Pattern_arr.length;i++)
													{%>
													 <%if(((String)request.getAttribute("patternName")).equals(Pattern_arr[i])) {%>
													 
													 		<option value="<%=Pattern_arr[i]%>" selected="selected"><%=Pattern_arr[i]%></option>
													 
													 <%}else{ %>
												           
												           	<option value="<%=Pattern_arr[i]%>"><%=Pattern_arr[i]%></option>
												     
												      <%} %>
												<%}%>
						</select>
						</div>
						
						<div id="pattern_name_td">	
						<br>
						<b>Current Plan Domain:</b> <%=(String)request.getAttribute("plandomain")%><br>
						<b>Current Plan : </b><%=(String)request.getAttribute("planname")%>
						<br><br>
						Plan Domain:	
						<select name="plan_domain" id="plan_domain" onchange="getPlanNamesAsPerDomain(this.value)">
												<option value="Select a Plan Domain">Select a Plan Domain</option>
										         <option value="Recruiter">Recruiter</option>
										         <option value="Banks">Banks</option>
										         <option value="Insurance">Insurance</option>
										         <option value="Travel Agent">Travel Agent</option>
										         <option value="Govt-Department">Govt-Department</option>
										         <option value="Telecommunication">Telecommunication</option>
										         <option value="Money Transfer Agents">Money Transfer Agents</option>
										         <option value="Hotel and Lodging">Hotel & Lodging</option>
										         <option value="Corporate">Corporate</option>
										         <option value="Other">Other</option>
						</select>
																						
						<select name="plan_name" id="plan_name" style="display:none;">
												
						</select>
						</div>

						<tr>
						<td>
						<br></br>
						
						<b>Current Domain :</b> <%=(String)request.getAttribute("domain")%>
						
						<br></br>
						
						<div id="ChooseShowDomain" align="left" style=" margin-right:-99px;margin-top:10px">						
						
						<a href="#" id="popup" onclick ="div_show()"><b>SELECT DOMAIN</b></a>
						
						</div>
						
						<div id="displayselectedDomainDiv" style=" margin-right: -99px;margin-top: 41px;width: 420px;">						
						
								
						</div>
						
						</td>
						</tr>


</table>
					</div>
				</td>
			</tr>
		</table>
          
          
          <input type="hidden"  id="domainPatternId" name="domainPattern" value=""/>
          
        

</form> 
</div>
  <div id="Utiliser">
		<input align="right" type="button" name="Next" value="Update Utilizer" onClick="update_Utiliser()"/>
		</div>

<div id="abc" style="display:none;">
 
	 <!-- Popup div starts here -->
 <div id="popupContact"> 

	<!-- contact us form -->
		<form id="domainForm" name="domainSelectionForm" id="form" >
			<img src="admin_resources/domainTree/images/3.png" id="close" onclick="domainSlectionOpeation(this.id)"/>	
			<h3>Select Domain</h3><hr/>
			
			<form id="form1"  >

    <div style="border: thin solid #c8c8c8;width:500px;">

        Select Domain <select id="cb" onchange="getSubDomainValue(this.id)">
            <option value="Select Your Domain">Select Your Domain</option>
						<%for(int i=0;i<domainType.length;i++){ %>
						<option value=<%=domainType[i]%>><%=domainType[i]%></option>
						<%}%>
             </select>

        <table id="table1">

            <tbody>

                <tr><th>Sub Domain Level</th>
                    <th>Parent Name</th>
                    <th>Sub Domain Value</th>
                </tr>
              
			<tr id="childRow1" style="display:none;"><td>1</td><td id="parentId1"></td>			 
			 <td>
				<select id="cb1" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>
           
           <tr id="childRow2" style="display:none;"><td>2</td><td id="parentId2"></td>			 
			 <td>
				<select id="cb2" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>
            
            <tr id="childRow3" style="display:none;"><td>3</td><td id="parentId3"></td>			 
			 <td>
				<select id="cb3" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>
            
            <tr id="childRow4" style="display:none;"><td>4</td><td id="parentId4"></td>			 
			 <td>
				<select id="cb4" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>
            
            <tr id="childRow5" style="display:none;"><td>5</td><td id="parentId5"></td>			 
			 <td>
				<select id="cb5" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>
              
            <tr id="childRow6" style="display:none;"><td>6</td><td id="parentId6"></td>			 
			 <td>
				<select id="cb6" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>   
             
            <tr id="childRow7" style="display:none;"><td>7</td><td id="parentId7"></td>			 
			 <td>
				<select id="cb7" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>   
             
            <tr id="childRow8" style="display:none;"><td>8</td><td id="parentId8"></td>			 
			 <td>
				<select id="cb8" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>  
            
            <tr id="childRow9" style="display:none;"><td>9</td><td id="parentId9"></td>			 
			 <td>
				  <select id="cb9" onchange="getHeirarchyFromDomain(this.id)"></select>
             </td>
            <tr>  
            
            <tr id="childRow10" style="display:none;"><td>10</td><td id="parentId10"></td>			 
			 <td>
				  <select id="cb10"></select>
             </td>
             
            <tr>  
                 		              
           </tbody>

        </table>
           
          <span >
          <input type="button"  id="submit"  value="Done"  onclick="domainSlectionOpeation(this.id)"/>	          
          </span>
    </div>
         
    </form>										
		</form>
		
 </div> 
 <!-- Popup div ends here -->
 </div>
 <!-- display popup button -->




</body>
</html>