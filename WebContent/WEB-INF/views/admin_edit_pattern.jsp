<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<style type="text/css">
.wfm {
    height: 307px;
    overflow-x: auto;
    width: 323px;
     }

.wfm ul { list-style:none; float:left;width:100% ;}

.wfm li { padding-top:10px;float:left;width:100% ;}

.wfm div { float:left }

.expand { width:15px;height:15px; }

.collapse { width:15px;height:15px;display:none }
  

.right {
	background-color: #fff;
    border-color: #999;
    border-radius: 5px;
    border-style: solid;
    border-width: 1px;
    float: left;
    height: 308px;
    width: 325px;
}
.left {
	background-color: #fff;
    border-color: #999;
    border-radius: 5px;
    border-style: solid;
    border-width: 1px;
    float: left;
    height: 308px;
    width: 325px;
}

</style>
    
<script type="text/javascript">

	function madeAjaxCall(type) {  
		
		$("#waitingdivId").show();
    	
		var labelName = document.getElementById("labelname").value;
		var patternValue = document.getElementById("newPattern").value;		
		var existValue = "";
		var viewValue = "";
		
		/* if values are already choosen then here w can find those values  */
		
		if(patternValue!="")
			{
		       var existArray1 = patternValue.split(",");
		       for(var e=0;e<existArray1.length;e++)
		    	   {
		    	      var subType = existArray1[e].split("-");
		    	      if(subType[0]==labelName)
		    	    	  {
		    	    	      existValue += subType[1]+","; 
		    	    	      viewValue += existArray1[e]+",";
		    	    	  }
		    	   }		       
			}
		
		existValue = existValue.substring(0,existValue.length-1);
				
		var existValAry = existValue.split(",");
		
		/* EOS code for values are already choosen then here we can find those values  */
		 
		if(labelName!="Select your Document")			 
		  
		  {
			 			 
			$.ajax({
				type : "Post",
				url : "edit_grp.html",
				data : "doctype=" + type,			
				success : function(response) {     
								
					var obj = response.split(",");
	
						$('#SubGroup').empty();
						$('#viewUsers12').empty();																		
						$('#viewUsers12').append("<h1><span>" + labelName + "</span></h1><br><br><br>");			
						
						for ( var k = 0; k < obj.length; k++)
							{
							var flag = 'true';
							
							    for ( var l = 0; l < existValAry.length; l++)
									  {								  
										  if(obj[k]==existValAry[l])
											  {
											     flag = 'false';
											     $('#viewUsers12').append("<input  name=\"patternType" + k + "\" id=\"patternType" + k + "\" type=\"checkbox\" checked=\"true\" value=\""+obj[k]+"\" onClick=\"addNewListItem(this,"+k+")\"  />" + obj[k] + "<br />");	
											  }								    							    
									  }		
								  if(flag=='true')
									  {
									    $('#viewUsers12').append("<input  name=\"patternType" + k + "\" id=\"patternType" + k + "\" type=\"checkbox\" value=\""+obj[k]+"\" onClick=\"addNewListItem(this,"+k+")\"  />" + obj[k] + "<br />");								   
									  }
							}
						
						var values = ""; 
						
						var patternValue = document.getElementById("newPattern").value;										
																		
						$("#showpattern").html(values);						
						
						/* Script for view already selected type in right side div ! */
						
						viewValue = viewValue.substring(0, viewValue.length-1);		
						var viewValueAry = patternValue.split(",");
						
						$("#waitingdivId").hide();
			        							
						$("#showpattern").append("<ul>");
						for(var v=0;v<viewValueAry.length;v++)
							{
							   $("#showpattern").append("<li>"+viewValueAry[v]+"</li>");
							}
						$("#showpattern").append("</ul>");
						
						/* EOS for view already selected type in right side div ! */
				},
							
			});		
	    }	
		else
			{
			   $("#waitingdivId").hide();        	
			}
	}
	 
	
	function addNewListItem(id,index)
	{
		
		var type = document.getElementById("labelname").value;
		
		var indexValue = "patternType"+index; 
		
		var selectValue = document.getElementById(indexValue).value;			
		  
		var values = "";
		var checkValue = type+"-"+selectValue;
		
		if(id.checked == true)			
	    {  	  			  			  
			  values += "<li>";					  
			  values += checkValue; 
			  values += "</li>";	
			  document.getElementById("newPattern").value += checkValue + ","; 	
			  $("#showpattern").append(values);
		} 
		
		if(id.checked == false)			
	    {  							
			var patternValue = document.getElementById("newPattern").value;				
			var patArray1 = patternValue.split(",");
			var tempPattern = "";
			var viewPattern = "";
			
			for(var i=0;i<patArray1.length;i++)
				{				    
				    if(checkValue!=patArray1[i])
				    	{				    	   
				    	   tempPattern += patArray1[i]+",";				    	  
				    	}				   
				}
			
			tempPattern = tempPattern.substring(0,tempPattern.length-1);
			document.getElementById("newPattern").value = "";
			document.getElementById("newPattern").value = tempPattern;	// After deselect  remove the elements from the hidden value !		
			
			var latest = document.getElementById("newPattern").value;		
			
			var viewArray1 = tempPattern.split(",");
			for(var v=0,j=1;v<viewArray1.length;v++,j++)
				{
				   var viewData = viewArray1[v].split("-");				  
				   				      
					      values += "<li>";					  
						  values += viewArray1[v]; 
						  values += "</li>";	
					   
				}
			
			
			$("#showpattern").empty();
			$("#showpattern").append(values);
		} 				
	}
		
function gen_code() {
	
	$("#waitingdivId").show();
	
	var p_Name = $("#pattern_name").val();
	var patternValue = document.getElementById("newPattern").value;			
	
	 $.ajax
          ({
			type : "post",				
			url : "patterninfo.html",			
			data :"newPattern="+patternValue+"&p_Name="+p_Name,	  
			success : function(response) {
				
				$("#waitingdivId").hide();
	        					
				alert("Pattern Created Successfully !");
				CreatePattern('createAdminPattern.html');
				    
			},
			
		});		 		 		 		
	$("#titleBar").html("<h2><span>PATTERN</span></h2>");
	
}  

function editPattrDetails(patternName)
{
	
	if(patternName=="Select Pattern Name")
		{
		    $("#editPatternDivId").hide();  
		    alert("Select a Pattern Name !");
		}
	else
		{
		$("#waitingdivId").show();
    	
				$.ajax
			    ({
					type : "post",				
					url : "getPatternDetailsviaName.html",			
					data :"patternName="+patternName,	  
					success : function(response) {
						
						document.getElementById("newPattern").value = "";
						
						document.getElementById("pattern_name").value = patternName;
							
						document.getElementById("newPattern").value = response;
						var patternValue = document.getElementById("newPattern").value;
						var viewValueAry = patternValue.split(",");
						
						$("#showpattern").append("<ul>");
						for(var v=0;v<viewValueAry.length;v++)
							{
							   $("#showpattern").append("<li>"+viewValueAry[v]+"</li>");
							}
						$("#showpattern").append("</ul>");
						
						$("#waitingdivId").hide();
			        	
						$("#editPatternDivId").show();  
						
					},
					
				});		 
		}
	
}

function editPattrn()
{
	var existPatternName = document.getElementById("patternNameId").value;
	var patternName = document.getElementById("pattern_name").value;
	var patternValue = document.getElementById("newPattern").value;
	
	$("#waitingdivId").show();
	
	 $.ajax
     ({
		type : "post",				
		url : "updatePatternDetails.html",			
		data :"existPatternName="+existPatternName+"&patternName="+patternName+"&patternValue="+patternValue,	  
		success : function(response) {
			
			$("#waitingdivId").hide();
        	
			alert("Pattern Updated Successfully !");
			EditPattern('editAdminPattern.html');
			    
		},
		
	});		 	
}

</script> 

</head>

<body>

 <%
  String patternNames = (String)request.getAttribute("patt_name");
  String patternNameArray[] = patternNames.split(",");
 %>

  <div>
  
         <select id="patternNameId" onchange="editPattrDetails(this.value)">
               
                 <option value="Select Pattern Name">Select Pattern Name</option>
                 <%
                   for(int i=0;i<patternNameArray.length;i++){
                 %>
                     <option value="<%=patternNameArray[i]%>"><%=patternNameArray[i]%></option>
                   
                 <%} %>
         </select>
  
  </div>


  <div id="editPatternDivId" style="display:none;" >

				<div id="admincreatepattern" >
				
					<!-- <form name="Docvalue" action="recipient_details.html" method="post"> -->
				
						<table border="0" align="center">
						
							<tr>
							    <th align="left"></th>
								
								<td>
								Pattern Name<br>
								<input type="text" name="pattern_name" id="pattern_name"/>
								</td>	
								
								<th align="right"></th>
								<td>
								Pattern Category
								<br>
								<select name="doctype" id="labelname" onchange="madeAjaxCall(this.value)">
										<option value="Select your Document">Select your Document</option>
										<option value="REGISTRATION">REGISTRATION DETAIL</option>
										<option value="BASIC">BASIC DETAIL</option>
										<option value="FAMILY">FAMILY DETAIL</option>
										<option value="KYC">KYC</option>
										<option value="ACADEMIC">ACADEMIC</option>
										<option value="FINANCIAL">FINANCIAL</option>
										<option value="EMPLOYEMENT">EMPLOYMENT</option>						
								</select>
								
								</td>				
							</tr>
							
						</table>
				               <div id="SubGroup"></div>
						<br> <br> <br>
						<table border="0" align="center">
							<tr>
								<td>
									<div id="viewUsers12" class="left" align="left">
									<h1><span>Pattern Type</span></h1>
									
									</div>
								</td>
								<td></td>
								<td></td>
								<td></td>
								<td>
								
									<div class="right" >						    
											<h1><span>Selected Documents</span></h1>						
										    <div id="showpattern" size="5" style="height:250px;margin-left:5px;width: 315px;overflow-y: scroll;" >
												  								   				                              
									        </div>						
									</div>
									
								</td>
							</tr>
						</table>
				        
				       <input type="hidden" id="newPattern"  value=""/>
						
						<div id="Utiliser">		 
						 <input align="right" type="submit" name="Next" value="UPDATE" onclick="editPattrn()"/>
						</div>
						
					<!-- </form> -->
				
				    </div>
				    
    </div>
    
    
    
</body>

</html>