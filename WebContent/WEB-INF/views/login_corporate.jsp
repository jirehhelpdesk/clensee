 <!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clensee</title>

<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link href="resources/css/kycstyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<script src="resources/onlineresources/code.jquery.com.jquery-1.9.1.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.10.2.jquery-ui.js"></script>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 

<link rel="stylesheet" href="resources/UtiCss/doctable.css"/>
<link rel="stylesheet" href="resources/css/style_common.css">

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css"/>

 <link rel="stylesheet" href="resources/ImageSlider/slider.css"/>

<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 5px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 0px;
    position: absolute;
    top: 25%;
    width: 800px;
    z-index: 1002;
}
</style>

<script>


function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>

<script type="text/javascript">
function searchouterpage(){
	
	var id1=$("#searchvaluetext").val();
	
	if(id1.length>=3)
	{				
		    $(".topdivespan").show();
			$('#searchhomepage').show();
				
			 $.ajax({  
			     type : "Post",   
			     url : "search_Outerprofile.html", 	  
			     data : "kycid="+id1,	     	     	     
			     success : function(response) 
			     {   	    		    				    	
					$('#searchhomepage').html(response);						
				 },  					    
			}); 			 
	 }
	 else if(id1.length<3)
	 {		
		$('#searchhomepage').html("");
		$('#searchhomepage').hide();	
		$(".topdivespan").hide();
	 }	
	else 
	 {	
		$('#searchhomepage').html("");
		$('#searchhomepage').hide();	  
		$(".topdivespan").hide();
	 }	
}

function openLogin(id)
{	
	window.location = "individual.html#tabs1-js";	
	document.getElementById("searchedIndividualId").value = id;
}

function gotoHomePage()
{
	window.location = "clenseeHome.html";
}



function hideSearchBar()
{
	$(".topdivespan").hide();		
	$('#searchhomepage').hide();		
}


function hitService(action)
{
	var win = window.open(action, '_blank');
	win.focus();	
}



</script>
      
</head>

<body>
<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>

<div id="centerbody">
    <div class="container">
		<div class="top_line"></div>
		
			<header class="header">
			<div class="header_inner">
				<div class="logo" onclick="gotoHomePage()">
				
				</div>
				
			  <div class="search-bar">			 
				 <div id="tfheader">
							 				 
						 <input type="text" id="searchvaluetext" maxlength="50" class="tftextinput2 button-submit" name="q" size="21" maxlength="120" title="Enter atleast 3 character for efficent search !" autofocus="autofocus"  placeholder="Search Individual" onkeyup="searchouterpage();"/>
                        
						 <input type='image' class="search_button-submit" alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="searchouterpage()"/>
						  
						        
						        
							<div class="tfclear" onclick="hideSearchBar()">
                                
						        </div>
				 </div>     
				</div>
                <div class="tfclear" onclick="hideSearchBar()"></div>
					 <div class=" mainmenu" onclick="hideSearchBar()">
					 
							 <ul class="main_menu">
                              
                                
								<li><a class="individual button-submit" href="individual.html"></a></li>
								<li style="border-right:1px solid #333;width:1px; height: 57px;   background-color: #777;"></li>
								<!-- <li><a class="corporate" href="clenseeHome.html"></a></li> 
								<li style="border-right:1px solid #333;width:1px; height: 57px;   background-color: #777;"></li>-->
								<li><a class="utiliser button-submit" href="utiliser.html"></a></li>
							   
							 </ul>   
						</div>       
				
				 </div>

			</header>
			
			
			
			<div class="banner_center_individual">
            <div class="index_main">
            
                  
				<div id="searchhomepage" style="display: none;width:100%;"></div> 	
				</div>
					  <!-- <div class="slider_iamge" id="hidecenterbody"></div>  -->
					  
					  
					  <!-- home page Slider -->



				<section style="width: 100%;" >
					<div id="sliderBannerId" class="buttonnextprev" onclick="hideSearchBar()">
						
						<!-- <button class="next" style="display: none;"></button>
						<button class="prev" style="display: none;"></button> -->
						
						<div class="slidercontainer">
							<div style="display: inline-block;">
								<img src="resources/ImageSlider/images/1.png">
							</div>
							<div style="display: none;">
								<img src="resources/ImageSlider/images/2.png">
							</div>
							<div style="display: none;">
								<img src="resources/ImageSlider/images/3.png">
							</div>
							<div style="display: none;">
								<img src="resources/ImageSlider/images/4.png">

							</div>
						</div>
					</div>
				</section>



				<!-- End of Home Page Slider -->
					  
					  				  					  					                   

			 </div>
				
		<!------center End-------->
		<footer class="footer" onclick="hideSearchBar()">
		   
		   <div class="footer_center">
           <div class="footer_topline"></div>
				<div class="bottom-icons">    
				 <ul id="bottom-icons-img">
						<li onclick="hitService('individualshare.html')"><a href="#"><img src="resources/images/why_kyc_icon.png" width="61" height="43" /></a>
                        <h2>Individual Sharing</h2>
                        <p>Enables individual to send documents to other individuals instantly.</p>
                        </li>
						<li class="bottom-icons-leftline" onclick="hitService('utilizershare.html')" ><a href="#"><img src="resources/images/kyc_store_icon.png" width="61" height="43" /></a>
                        <h2>Utilizer Sharing</h2>
                        <p>Enables individual to send documents to Utilizers instantly.</p>
                        </li>
						<li class="bottom-icons-leftline" onclick="hitService('kycService.html')"><a href="#"><img src="resources/images/kyc_answers_icon.png" width="61" height="43" />  </a>
                        <h2>Kyc Services</h2>
                        <p>Clensee provides e-kyc documentation services which enables many Utilizers to meet Kyc requirements.</p>
                        </li>
						<li class="bottom-icons-leftline" onclick="hitService('easyaccess.html')"><a href="#"><img src="resources/images/meet_members_icon.png" width="61" height="43" /></a>
                        <h2>Easy Access</h2>
                        <p>Never ever easy way to access and share most important personal documents.</p>
                        </li>
					   
					</ul>     
				</div>
			</div>
			
			
			<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p  style="margin-top:25px;">Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p  style="margin-top:35px;">
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactusbeforeLogin.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-left: 905px !important; margin-top: -1px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
				
		   
			<div class="footer_line">
			</div>

		 </footer>

    </div>
    </div>
<!------container End-------->



<!-- Progrss Bar -->



<!-- EOF Progress Bar -->


<!-- kycDiv Div -->
	
	<div id="kycdivlight">
			
					<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageblogin_links"  src="resources/images/close_button.png" /></div>
		   
		
		    <div id="kycdivdisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:800px;margin-left:-99px;						
						color:#555;
						border-radius:5px;	
						padding:0; 
						margin:0 auto;"> 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
<!-- EOF kycDiv Div -->	

<script src="resources/ImageSlider/indexslider/jquery.js"></script>
 
</body>
</html>
 