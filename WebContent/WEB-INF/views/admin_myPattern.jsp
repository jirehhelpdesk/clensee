<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<%@ page language="java" import="java.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Pattern</title>

<link href="admin_resources/kyc_css/kyc_admin_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="admin_resources/kyc_css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">

<link rel="stylesheet" href="admin_resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script type="text/javascript"src="admin_resources/kyc_scripts/demo.js"></script>

<link rel="stylesheet" href="admin_resources/css/logout.css">	
	
	
	
<script src="resources/kyc_scripts/jquery-ui-10.js"></script>
<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>
	
<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">
<link rel="stylesheet" href="resources/kyc_css/kycslide.css">

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />
	
	
<style>

#sessionfade{

    background-color: #000;
    height: 100%;
    left: 0;
    opacity: 0.7;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1001;
}

#sessionlight{

    background: #fff none repeat scroll 0 0;
    border-radius: 24px;
    height: auto;
    left: 43%;
    margin-left: -211px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: auto;
    z-index: 1002;
    right: 400px !important;

}


.adminleftMenuActive
{
	color:#00b6f5 ! important;
}

</style>	


<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}
</style>
	
	
	<style type="text/css">
.wfm {
    height: 307px;
    overflow-x: auto;
    width: 323px;
     }

.wfm ul { list-style:none; float:left;width:100% ;}

.wfm li { padding-top:10px;float:left;width:100% ;}

.wfm div { float:left }

.expand { width:15px;height:15px; }

.collapse { width:15px;height:15px;display:none }
  

.right {
	background-color: #fff;
    border-color: #999;
    border-radius: 5px;
    border-style: solid;
    border-width: 1px;
    float: left;
    height: 308px;
    width: 325px;
}
.left {
	background-color: #fff;
    border-color: #999;
    border-radius: 5px;
    border-style: solid;
    border-width: 1px;
    float: left;
    height: 308px;
    width: 325px;
}

</style>

	
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
$(document).ready(function () {
    $("#menu_wrapper").on("click", "ul.main_menu>li", function (event) {
        debugger;
        event.preventDefault();
        $("#menu_wrapper .active").removeClass("active");
        $(this).find('a:first').addClass("active");
    });
});
});//]]>  



function redirectToLogin()
{
	window.location = "logout_admin.html";	
}

function gotoMainPage()
{
	window.location = "authenticateAdminLogin.html"
}

</script>

<script type="text/javascript">



/* Script For Pattern */


function madeAjaxCall(type) {  
				
		var labelName = document.getElementById("labelname").value;
		var patternValue = document.getElementById("newPattern").value;		
		var existValue = "";
		var viewValue = "";
		
		/* if values are already choosen then here w can find those values  */
		
		if(patternValue!="")
			{
		       var existArray1 = patternValue.split(",");
		       for(var e=0;e<existArray1.length;e++)
		    	   {
		    	      var subType = existArray1[e].split("-");
		    	      if(subType[0]==labelName)
		    	    	  {
		    	    	      existValue += subType[1]+","; 
		    	    	      viewValue += existArray1[e]+",";
		    	    	  }
		    	   }		       
			}
		
		existValue = existValue.substring(0,existValue.length-1);
				
		var existValAry = existValue.split(",");
		
		/* EOS code for values are already choosen then here we can find those values  */
		 
		if(labelName!="Select your Document")			 
		  
		  {
			
			$("#waitingdivId").show();
			
			$.ajax({
				type : "Post",
				url : "edit_grp.html",
				data : "doctype=" + type,			
				success : function(response) {     
					
					$("#waitingdivId").hide();
										
					if(response!="")
						{
								var obj = response.split(",");
								
								$('#SubGroup').empty();
								$('#viewUsers12').empty();																		
								$('#viewUsers12').append("<h1><span>" + labelName + "</span></h1><br><br><br>");			
								
								for ( var k = 0; k < obj.length; k++)
									{
									var flag = 'true';
									
									    for ( var l = 0; l < existValAry.length; l++)
											  {								  
												  if(obj[k]==existValAry[l])
													  {
													     flag = 'false';
													     $('#viewUsers12').append("<input  name=\"patternType" + k + "\" id=\"patternType" + k + "\" type=\"checkbox\" checked=\"true\" value=\""+obj[k]+"\" onClick=\"addNewListItem(this,"+k+")\"  />" + obj[k] + "<br />");	
													  }								    							    
											  }		
										  if(flag=='true')
											  {
											    $('#viewUsers12').append("<input  name=\"patternType" + k + "\" id=\"patternType" + k + "\" type=\"checkbox\" value=\""+obj[k]+"\" onClick=\"addNewListItem(this,"+k+")\"  />" + obj[k] + "<br />");								   
											  }
									}
								
								var values = ""; 
								
								var patternValue = document.getElementById("newPattern").value;										
																				
								$("#showpattern").html(values);						
								
								/* Script for view already selected type in right side div ! */
								
								viewValue = viewValue.substring(0, viewValue.length-1);		
								var viewValueAry = patternValue.split(",");
								
								$("#showpattern").append("<ul>");
								for(var v=0;v<viewValueAry.length;v++)
								{
									$("#showpattern").append("<li>"+viewValueAry[v]+"</li>");
								}
								$("#showpattern").append("</ul>");
								
								/* EOS for view already selected type in right side div ! */
						}
					else
						{
						   alert("As per Pattern name hierarchy not added.");
						}
					
				},
							
			});		
	    }	  
	}
	 
	
	function addNewListItem(id,index)
	{
		
		var type = document.getElementById("labelname").value;
		
		var indexValue = "patternType"+index; 
		
		var selectValue = document.getElementById(indexValue).value;			
		  
		var values = "";
		var checkValue = type+"-"+selectValue;
		
		if(id.checked == true)			
	    {  	  			  			  
			  values += "<li>";					  
			  values += checkValue; 
			  values += "</li>";	
			  document.getElementById("newPattern").value += checkValue + ","; 	
			  $("#showpattern").append(values);
		} 
		
		if(id.checked == false)			
	    {  							
			var patternValue = document.getElementById("newPattern").value;				
			var patArray1 = patternValue.split(",");
			var tempPattern = "";
			var viewPattern = "";
			
			for(var i=0;i<patArray1.length;i++)
				{				    
				    if(checkValue!=patArray1[i])
				    	{				    	   
				    	   tempPattern += patArray1[i]+",";				    	  
				    	}				   
				}
			
			tempPattern = tempPattern.substring(0,tempPattern.length-1);
			document.getElementById("newPattern").value = "";
			document.getElementById("newPattern").value = tempPattern;	// After deselect  remove the elements from the hidden value !		
			
			var latest = document.getElementById("newPattern").value;		
			
			var viewArray1 = tempPattern.split(",");
			for(var v=0,j=1;v<viewArray1.length;v++,j++)
				{
				   var viewData = viewArray1[v].split("-");				  
				   				      
					      values += "<li>";					  
						  values += viewArray1[v]; 
						  values += "</li>";	
					   
				}
			
			
			$("#showpattern").empty();
			$("#showpattern").append(values);
		} 				
	}
		
function gen_code() {
	
	$("#waitingdivId").show();
	
	var p_Name = $("#pattern_name").val();
	var patternValue = document.getElementById("newPattern").value;			
	
	var flag = 'true';
	
	if(p_Name=="")
	{
		flag = 'false';
		$("#waitingdivId").hide();
		alert("Please enter Pattern name !");
	}
	if(patternValue=="")
	{
		flag = 'false';
		$("#waitingdivId").hide();
		alert("Before save the pattern please arrange some pattern !");
	}
	
	if(flag=='true')
		{
				$.ajax
		        ({
					type : "post",				
					url : "patterninfo.html",			
					data :"newPattern="+patternValue+"&p_Name="+p_Name,	  
					success : function(response) {
						
						$("#waitingdivId").hide();
						
						alert("Pattern Created Successfully !");
						CheckSessionOfAdmin('createAdminPattern.html');
						    
					},
					
				});		 		 		 		
			$("#titleBar").html("<h2><span>PATTERN</span></h2>");
		}
	 
}  



function EditPattern(urlLink)
{
	$("#waitingdivId").show();
	
	document.getElementById("1stId").className = "";
	document.getElementById("2stId").className = "adminleftMenuActive";
	document.getElementById("3stId").className = "";
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
					 $.ajax({
					        url: urlLink,
					        type: 'POST',	       
					        success: function (data) {       
					        	
					        	$("#waitingdivId").hide();
					        	
					            $("#viewUsers")
								.html(data);
								$("#titleBar").html("<h2><span>Edit Pattern</span></h2>");
					        },
					        
					    });
				}
			else
				{
				$("#waitingdivId").hide();
				
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});	
}
	
	
function DeletePattern(urlLink)
{
	$("#waitingdivId").show();
	
	document.getElementById("1stId").className = "";
	document.getElementById("2stId").className = "";
	document.getElementById("3stId").className = "adminleftMenuActive";
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
					$.ajax({
				        url: urlLink,
				        type: 'POST',	       
				        success: function (data) {      
				        	
				        	$("#waitingdivId").hide();
				        	
				            $("#viewUsers")
							.html(data);
							$("#titleBar").html("<h2><span>Delete Pattern</span></h2>");
				        },
				        
				    });
				}
			else
				{
				    $("#waitingdivId").hide();
				
				    $("#sessionlight").show();
			        $("#sessionfade").show();
				}
		}
	});
	
	 
	}
	
	
	
	

/* End Of Script for Pattern */

	function CheckSessionOfAdmin(hitTheUrl)
	{
		$("#waitingdivId").show();
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				$("#waitingdivId").hide();
				
				if(response=="Exist")
					{
						window.location = hitTheUrl;
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
					
	}
	
</script>

<!-- Scripts for admin Pattern -->

</head>
<body>

	<div class="container">

		<!--header block should go here-->

		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner" style="width:1120px !important">
			
			<div class="logo" onclick="gotoMainPage()">MAIN PAGE</div>
			
			<div class="tfclear"></div>
			<div  class=" mainmenu" >
			<div id="menu_wrapper">
				
				<ul class="main_menu" >
					
					<li></li>
										
					<li id="myMenus1">				
					<a class="myprofile" href="#" onclick="CheckSessionOfAdmin('viewindividual.html')"><span></span> My Individual
					</a>
					</li>
					
					<li id="myMenus2">				
					<a class="mydocuments" href="#" onclick="CheckSessionOfAdmin('createUtiliser.html')">	<span></span>  My Utilizer
					</a>
					</li>
					
					<li id="myMenus5"><a class="mystore" href="#" onclick="CheckSessionOfAdmin('createDomain.html')">
							Domain Hierarchy
					</a></li>
					
					<li id="myMenus6" style="width:140px;"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createKycHierarchy.html')">
							Document Hierarchy
					</a>
					</li>
					
					<li id="myMenus3"><a class="myplans" style="color:#03b2ee" href="#" onclick="CheckSessionOfAdmin('createAdminPattern.html')"> 
							My Patterns
					</a></li>
					
					<li id="myMenus41"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createfeature.html')">  My Features
					</a></li>
					
					<li id="myMenus42"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createplan.html')">  My Plans
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('adminNotification.html')">  NOTIFICATION
					</a></li> 
						
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('admin_settings.html')">  SETTINGS
					</a></li>				
				</ul>
				
             </div>
                                   
			</div>

          
		</div>
   
	</header>


				 <div style="text-align: right; float: right; margin-top: -50px; width: 4%; margin-left: 10px;"> 
	               <a href="logout_admin.html" ><h4 style="color:#05b7f5;">Logout</h4></a>
	             </div>

		<div class="main" ><!-- background-color: #585759; -->
			<div class="main_center">
				<div class="leftside">

					<!--left menus should go here-->
					<div class="user_display">


					 <form>
							<div id="textBox">
							
							        <div id="button">
							        
							            <ul>
							                <li id="Hi-B-1"><a href="#" id="1stId" class="adminleftMenuActive" onClick="CheckSessionOfAdmin('createAdminPattern.html')"><span>CREATE PATTERN</span></a></li>
											<li id="Hi-B-1"><a href="#"  id="2stId" onClick="EditPattern('editAdminPattern.html')"><span>EDIT PATTERN</span></a></li>
											<li id="Hi-B-1"><a href="#"  id="3stId" onClick="DeletePattern('deleteAdminPattern.html')"><span>DELETE PATTERN</span></a></li>
									    </ul>
									    
									</div>
								
							</div>
						</form>

					</div>




				</div>


				<div class="rightside" >

					<!--center body should go here-->
					<div id="wishAdmin">
						<h1>Welcome To KYC Admin</h1>
					</div>
					
					<div class="border_line"></div>
					<div class=" username_bar">
						 <div id="titleBar">
							<h2>
								<span></span>
								 Create Pattern
							</h2>
						</div>
					</div>
				
					<div class="module-body" >
						<div id="viewUsers" style="height: auto !important;margin-bottom: 15px"> 
    
						 
						 
						 <!-- AdminBody startts Here -->
						 
									<div id="admincreatepattern">
									
												<!-- <form name="Docvalue" action="recipient_details.html" method="post"> -->
											
													<table border="0" align="center">
													
														<tr>
														    <th align="left"></th>
															<td>
															Pattern Name<br>
															<input type="text" name="pattern_name" id="pattern_name"/></td>	
															
															<th align="right"></th>
															<td>
															Pattern Category
															<br>
															<select name="doctype" id="labelname" onchange="madeAjaxCall(this.value)">
																	<option value="Select your Document">Select your Document</option>
																	<option value="REGISTRATION">REGISTRATION DETAIL</option>
																	<option value="BASIC">BASIC DETAIL</option>
																	<option value="FAMILY">FAMILY DETAIL</option>
																	<option value="KYC">KYC</option>
																	<option value="ACADEMIC">ACADEMIC</option>
																	<option value="FINANCIAL">FINANCIAL</option>
																	<option value="EMPLOYEMENT">EMPLOYMENT</option>						
															</select>
															
															</td>				
														</tr>
														
													</table>
											               <div id="SubGroup"></div>
													<br> <br> <br>
													<table border="0" align="center">
														<tr>
															<td>
																<div id="viewUsers12" class="left" align="left">
																<h1><span>Pattern Type</span></h1>
																
																</div>
															</td>
															<td></td>
															<td></td>
															<td></td>
															<td>
															
																<div class="right" >						    
																		<h1><span>Selected Documents</span></h1>						
																	    <div id="showpattern" size="5" style="height:250px;margin-left:5px;width: 315px;overflow-y: scroll;" >
																			  								   				                              
																        </div>						
																</div>
																
															</td>
														</tr>
													</table>
											        
											       <input type="hidden" id="newPattern"  value=""/>
													
													<div id="Utiliser">		 
													 <input align="right" type="submit" name="Next" value="SAVE" onClick="gen_code()"/>
													</div>
													
												<!-- </form> -->
											
											    </div>
											    
						 
						 
						 <!-- Admin Body Ends Here -->
						 
						</div>

					</div>
					

				</div>
				
			</div>
		</div>
</div>
		
		<!--footer block should go here-->
		
		   <%
			  ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			  String version=resource.getString("kycversion");
			  String versionDate=resource.getString("kyclatestDate");
		    %>
		
		
          <footer class="footer">
		   
		  
				<div class="footer_copyrights">
                <div class="copyrights">    
					 <ul >
                     		
							<li class="footer_space01"><p>Design by:<span> <a href="http://jirehsol.com/">Jireh </a></span></p>
                            <p> � All rights Reserved CLENSEE 2014-15</p>
                             <span>
									<a href="#">Version:<%=version%></a>
									</span>
                            </li>
							<li class="footer_space02"> <p><a href="#">About Us</a> |<a href="#"> Terms and Conditions</a> |<a href="#"> Privacy Policy</a> | <a href="#">Contact Us</a></p></li>
							<li class="footer_space03"> 
                            
                            <div class="four columns">
                                <ul class="social_media">
                                    <li>
                                     <a class="fb" href="https://www.facebook.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="google_plus" href="https://plus.google.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="http://www.linkedin.com/company/1872121?trk=tyah" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li>
                                   
                                    
                                </ul>
                              </div>
                            
                            </li>
						   
						</ul>     
					</div>
                
				</div>
		   
			<div class="footer_line">
			</div>

		 </footer>



<!-- Waiting Div -->
	
	<div id="waitingdivId" >
									    
		    <div id="waitingdivContentId">
				    
				     <div class="kycloader">
		
		             </div>
		             <h4>Please wait For the Portal response...   </h4>
		              				    
		    </div>
	     
    </div>
	
<!-- EOF Waiting Div -->




<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#05b7f5;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		        <h3>Your session has expired,Please click on the button to relogin !</h3>      
		         
		         <input type="button" value="Login" onclick="redirectToLogin()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	
	
	<!------container End-------->
</body>
</html>
