<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

 <!-- <script src="resources/table/jquery-1.11.1.js"></script> -->

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<style>
.backspace { margin-left: 100px; }
</style>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>




<script type="text/javascript">
function IndHistory() {
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
	
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
						document.getElementById("utiButId").style.background = "#ebebeb";
						document.getElementById("utiButId").style.color = "#4a484b";
						
						document.getElementById("utiIndId").style.background = "#00b6f5";
						document.getElementById("utiIndId").style.color = "#fff";
						
								$.ajax({
									type : "Post",
									url : "accIndHistory.html",				
									success : function(response) {
										
										   document.getElementById('waitlight').style.display='none';
									       document.getElementById('waitfade').style.display='none';
									       
					                        if(response.length==0)
					                        {
					                        	$('#History').html("<h1>No Access History is there for Individuals !</h1>");
					                        	$('#titleBar').empty();
					     						$('#titleBar').append("<h2><span>Sharing history of Individual</span></h2>");
					                        }
					                        else
					                        {
					                        var IndHisData = response.split(",");
					 						var index = 0;
					 						var IndTableData = "<table class='CSSTableGenerator' style=\"width:98% !important;\"><tr><th align=\"center\">S.NO</th><th align=\"center\">Name</th><th align=\"center\">Description</th><th align=\"center\">Date</th><th align=\"center\">View Documents</th></tr>";
					 						
					 						for ( var i = 0; i < IndHisData.length; i = i + 4) {
					 							index += 1;
					 							
					 							IndTableData += "<tr><td>" + (index)
					 									+ "</td><td>" + IndHisData[1 + i]
					 									+ "</td><td style=\"width: 309px;\">" + IndHisData[2 + i]
					 									+ "</td><td>" + IndHisData[3 + i]
					 									+ "</td><td><input  id=\"viewDocument\" type=\"button\" value=\"View More\" onClick=\"viewIndDocList('"+IndHisData[0 + i]+"','"+IndHisData[1 + i]+"')\" /></td>" 
					 									+ "</td></tr>";
					 						}
					 						
					 						IndTableData += "</table>";
					 						$('#History').empty();
					 						$('#titleBar').empty();
					 						$('#History').append(IndTableData);
					 						$('#titleBar').empty();
					 						$('#titleBar').append("<h2><span>List of documents shared to </span></h2>");
					 						
					                        }	
					 						                         
									},
									
								});
				}
			else
				{
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
			       $("#sessionfade").show();
			       
				}
		}
	}); 
	
	
}


function UtiHistory() {
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
						document.getElementById("utiButId").style.background = "#00b6f5";
						document.getElementById("utiButId").style.color = "#fff";
						
						document.getElementById("utiIndId").style.background = "#ebebeb";
						document.getElementById("utiIndId").style.color = "#4a484b";
						
						
								$.ajax({
									type : "Post",
									url : "accUtiHistory.html",				
									success : function(response) {
										
										document.getElementById('waitlight').style.display='none';
									    document.getElementById('waitfade').style.display='none';
									       
									if(response.length==0)
									{
										$('#History').html("<h1>No Access history is there for Utilizer's !</h1>");
										$('#titleBar').empty();
										$('#titleBar').html("<h2><span>Access history of Utilizer</span></h2>");
									}	
									else
										{                                                 
									   
										var UtiHisData = response.split(",");               
										var index = 0;                                     
										var UtiTableData = "<table class='CSSTableGenerator' style=\"width:98% !important;\"><tr><th align=\"center\">S.NO</th><th align=\"center\">Name</th><th align=\"center\">Description</th><th align=\"center\">Date</th><th align=\"center\">View Documents</th></tr>";
										for ( var i = 0; i < UtiHisData.length; i = i + 3) { 
											index += 1; 
											
											UtiTableData += "<tr><td>" + (index)
													+ "</td><td>" + UtiHisData[0 + i]
													+ "</td><td style=\"width: 309px;\">" + UtiHisData[1 + i]
													+ "</td><td>" + UtiHisData[2 + i]
													+ "</td><td><input  id=\"viewDocument\" type=\"button\" value=\"View More\" onclick=\"viewUtiDocList('"+UtiHisData[0 + i]+"','"+UtiHisData[2 + i]+"')\" /></td>" 
													+ "</td></tr>";
													
										}
										UtiTableData += "</table>";
										$('#History').empty();
										$('#titleBar').empty();
										$('#titleBar').html("<h2><span>Access history of Utilizer</span></h2>");
										$('#History').html(UtiTableData);
										}
									},
									
								});
				}
			else
				{
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 

}

function viewIndDocList(code_id,kycid)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
				   
					 $.ajax
					    ({
							type : "post",				
							url : "viewIndividualHistoryDocumentlist.html",	
							data: "kycid="+kycid+"&id="+code_id,
							success : function(response) {
									
								 document.getElementById('waitlight').style.display='none';
							     document.getElementById('waitfade').style.display='none';
							       
								$("#History").html(response);		
								
							},
							
						}); 
				}
			else
				{
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 	
}

function viewUtiDocList(kycid,time)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({
		type : "post",
		url : "checksessionforIndividual.html",
		success : function(response) {
			
			if(response=="Exist")
				{
				  
			       
					$.ajax({
						type : "Post",
						url : "viewUtiDocList.html",
						data: "kycid="+kycid+"&date="+time,
						success : function(response) {
				            	
							   document.getElementById('waitlight').style.display='none';
						       document.getElementById('waitfade').style.display='none';
						       
							   $('#History').html(response);
							
							},
						error : function(e) {
							
							alert('Error: ' + e);			
						}
							
						});					
				}
			else
				{
				   document.getElementById('waitlight').style.display='none';
			       document.getElementById('waitfade').style.display='none';
			       
				   $("#sessionlight").show();
			       $("#sessionfade").show();
				}
		}
	}); 	
}
</script>
</head>
<body>


<div id="access_history">

		
							<div id="revoke_tbutton">
									<input id="utiIndId" type="button" value="Individual sharing history" onClick="IndHistory()"/>
							</div>
						
							<div id="revoke_tbutton">
									<input id="utiButId" type="button" value="Utilizer sharing history" onClick="UtiHistory()"/>
							</div>
			
</div>

<div id="History" class="revokeCodeidscroll" >

</div>

<div id="titleBar">

</div>

	
</body>
</html>