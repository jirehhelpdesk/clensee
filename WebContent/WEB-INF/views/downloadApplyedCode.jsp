<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<%@page import="java.io.File" %><%   
%><%@page import="java.io.*" %><%
%><%@page import="java.util.*" %><% 
%><%@page language="java" trimDirectiveWhitespaces="true"%><%
%><%@page import="java.awt.Image.*" %><%
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>


<% String zipFile = "C:/FileIO/zipdemo.zip"; %>

<%

//Required file Config for entire Controller 

ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
String kycMainDirectory = fileResource.getString("fileDirectory");		
File filePath = new File(kycMainDirectory);

//End of Required file Config for entire Controller 
	


%>

<%    
String docname=(String)request.getAttribute("docname");
int user = (Integer)request.getAttribute("user");
int year = (Integer)request.getAttribute("year"); 
String docType = (String)request.getAttribute("docType");

response.setContentType("image/jpeg");
response.setHeader("Content-Disposition", "attachment;filename="+docname);


if(docType.equals("KYC_DOCUMENT"))
{		 	 
	    response.setContentType("text/html");  
		PrintWriter out1 = response.getWriter();  
		String filename = docname;   
		String filepath = filePath.getPath()+"/"+year+"/"+user+"/Document/"+"KYC_DOCUMENT"+"/";   
		response.setContentType("APPLICATION/OCTET-STREAM");   
		response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
		  
		FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
		            
		int i;   
		while ((i=fileInputStream.read()) != -1) {  
		    out1.write(i);   
		}   
		fileInputStream.close();   
		out1.close(); 
	 	 
}
    
if(docType.equals("ACADEMIC_DOC"))
{		
        response.setContentType("text/html");  
		PrintWriter out1 = response.getWriter();  
		String filename = docname;   
		String filepath = filePath.getPath()+"/"+year+"/"+user+"/Document/"+"ACADEMIC_DOC"+"/";   
		response.setContentType("APPLICATION/OCTET-STREAM");   
		response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
		  
		FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
		            
		int i;   
		while ((i=fileInputStream.read()) != -1) {  
		    out1.write(i);   
		}   
		fileInputStream.close();   
		out1.close(); 
			
}

if(docType.equals("Financial_DOC"))
{			
	response.setContentType("text/html");  
	PrintWriter out1 = response.getWriter();  
	String filename = docname;   
	String filepath = filePath.getPath()+"/"+year+"/"+user+"/Document/"+"Financial_DOC"+"/";   
	response.setContentType("APPLICATION/OCTET-STREAM");   
	response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
	  
	FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
	            
	int i;   
	while ((i=fileInputStream.read()) != -1) {  
	    out1.write(i);   
	}   
	fileInputStream.close();   
	out1.close(); 	
}

if(docType.equals("Employement_DOC"))
{
	
	response.setContentType("text/html");  
	PrintWriter out1 = response.getWriter();  
	String filename = docname;   
	String filepath = filePath.getPath()+"/"+year+"/"+user+"/Document/"+"Employement_DOC"+"/";   
	response.setContentType("APPLICATION/OCTET-STREAM");   
	response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
	  
	FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
	            
	int i;   
	while ((i=fileInputStream.read()) != -1) {  
	    out1.write(i);   
	}   
	fileInputStream.close();   
	out1.close();
		
}
%>   
</body>
</html>