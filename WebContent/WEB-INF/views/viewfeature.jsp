<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/css/kyc_admin_style.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<script>
function getFeaturedetails(featureName)
{
	var featureCategory = document.getElementById("featureCategoryId").value;
	
	

	if(featureName!="Select feature" && featureCategory!="Select Feature Category")
		{
				$.ajax({  
					
				    type : "Post",   
				    url : "selectedviewfeature.html", 
				    data :"featureCategory="+featureCategory+"&featureName="+featureName,	     	     	     
				    success : function(response) 
				         {    			   	 			 	
				    		
				    	     var featuredetails = response.split(",");
						 	 		
				    	     var values = "<table border=\"0\"><tr><th bgcolor=\"#999\">Feature Name</th><th bgcolor=\"#999\">Feature Data</th><th bgcolor=\"#999\">Feature Price</th></tr>";			
							 for(var i=0;i<featuredetails.length; i = 4 + i)
								 {				 				    		     
								     values +="<tr><td>"+featuredetails[1 + i]	
											+"</td><td>"+featuredetails[2 + i]	
											+"</td><td>"+featuredetails[3 + i]	
											+"</td></tr>";
											
								    
								 }
							 
							 values += "</table>";
												
							 $('#viewfeatureDetailsDiv').html(values);   
					 
				           }, 
				           
				       }); 
		}
	else
		{
		   alert("Select a Category !");
		}
  	
}
</script>
</head>
<body>

  <!-- Starts from here Need to choose the domain 1st -->
 <div id="Featureshowform">
	
	<form name="showform">
		
	
	
		<table align="center">
			<tr>
			  
				<th>Feature Category :
				
				<select name="featureCategory" id="featureCategoryId" >
						<option value="Select Feature Category">Select Feature Category</option>
						<option value="Individual">Individual</option>
						<option value="Utilizer">Utilizer</option>
						<option value="Corporate">Corporate</option>
				</select>
				
			</tr>
			
			<tr>
			  
				<th>Feature Name :
				
				<select  name="feature_name" id="featNameId" onchange="getFeaturedetails(this.value)">
												   <option value="Select feature">Select feature</option>
		                                           <option value="Profile Viewing">Profile Viewing</option>
		                                           <option value="Individual Sharing">Individual Sharing</option>
		                                           <option value="Documents Received">Documents Received</option>
		                                           <option value="Receiving Mails">Receiving Mails</option>
		                                           <option value="Utilizer Sharing / Removing">Utilizer Sharing / Removing</option>
		                                           <option value="SMS">SMS</option>
		                                           <option value="Files Storage">Files Storage</option>
		                                           <option value="Doc  Accessory of Employees and customers">Doc  Accessory of Employee's and customers</option>		                                        
		                                           <option value="Advanced Filters">Advanced Filters </option>
		                                           </select>
				
			   </tr>
			
			
		                                       	
	    </table>
	    
	 </form> 
				
	 <div id="viewfeatureDetailsDiv" style="width: 95% !important;" class="datagrid">
	 
	 
	 </div>
				</div>
</body>
</html>