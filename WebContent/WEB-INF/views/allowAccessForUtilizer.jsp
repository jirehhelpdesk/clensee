<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.sun.mail.imap.protocol.Status"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="resources/css/allowpoppage.css" />


<title>Allow Access To Utilizer</title>
	
	<script>
	
	function addMoreDocument(docType)
	{
		var eachDefaultPatternValue = "";
		
		$.ajax({
			
			type : "Post",
			url : "latestDocumentType.html",
			data : "doctype=" + docType,			
			success : function(response) {     
				
				
				if(response=="No Document you have uploaded")
					{
					     alert("You have not uploaded any document!");
					}
				else
					{															    
						var addingStatus = "";	
			    
					    var divValues = "<ul>";
						var defaultPattern = document.getElementById("defaultPattern").value;
						var defaultPatternArry = defaultPattern.split("/");
																								
						$("#allowAccessPopLight").show();		
						
						if(docType=="KYC_DOCUMENT")
							{
							
								   /* Extract the existed value */
						  			    
									    var existPattrn = document.getElementById("addedPattern").value;
								        
									    var existPatternArray = existPattrn.split("/");
									    var existKycDoc = "";
									    for(var ex=0;ex<existPatternArray.length;ex++)
									    	{
									    	    var eachExistValue = existPatternArray[ex].split("--");
									    	    if(eachExistValue[0]=="KYC")
									    	    	{
									    	    	   existKycDoc += eachExistValue[1] +"/";
									    	    	}
									    	}
									    
									    existKycDoc = existKycDoc.substring(0,existKycDoc.length-1);
									    				    
								    /* End of Extract the existed value */
						    
					    
							    document.getElementById("popDocType").value = "KYC";
							    
							    var docTypeArray = "";
							    var kycdocName = "";
							    var kycdocArray = "";
							    
							   
							    var makeitOne = response.split(",");
							    
							    for(var m=0;m<makeitOne.length;m++)
							     {							    	
							    	var eachfrontone = makeitOne[m].indexOf("front");
							    	
							    	if(eachfrontone>0)
							    		{
							    		    kycdocName = makeitOne[m].split("front")[0];
							    		}							    	
							    	var eachbackone = makeitOne[m].indexOf("back")
							    	
							    	if(eachbackone>0)
							    		{
							    		    if(makeitOne[m].split("back")[0]==kycdocName);
							    		    	{
							    		    	   kycdocArray += kycdocName+"-"+makeitOne[m-1]+","+makeitOne[m]+"/";
							    		    	}							    		    
							    		}
							    	else
							    		{
								    		var checkBackDoc = makeitOne[m].indexOf("Not Uploaded");
							    		    if(checkBackDoc==0)
								    		{			    		    											    		    		    		    					    		    	      		    	
								    		     kycdocArray += kycdocName+"-"+makeitOne[m-1]+"/";											    		    							    		    
								    		}
							    		}
							     }
							    
							    kycdocArray = kycdocArray.substring(0,kycdocArray.length-1);
							    
							    var latestDocArray = kycdocArray.split("/");
							    
								for(var i=0;i<latestDocArray.length;i++)
								{
									var flag = 'true';
									
	                                for(var j=0;j<defaultPatternArry.length;j++)
	                                	 {
	                                	     var eachdefailtValue = defaultPatternArry[j].split(":");	                                	     
	                                	     if(eachdefailtValue[0]=="KYC")
	                                	    	 {
	                                	    	     docTypeArray = eachdefailtValue[1];	                                	    	                                     	    	     
	                                	    	 }		                                	     
	                                	 }
	                                
	                                if(docTypeArray!="")
	                                	{
		                                	 var defaultDocArray = docTypeArray.split(",")                       	    	    
		                            	      for(var k=0;k<defaultDocArray.length;k++)
		                   	    	    	  {
		                   	    	    	     var containFlag = latestDocArray[i].indexOf(defaultDocArray[k]);
		                   	    	    	     if(containFlag>=0)
		                   	    	    	    	 {
		                   	    	    	    		 flag = 'false';
		                   	    	    	    	 }                              	    	    	    
		                   	    	    	  }	
	                                	}
	                                if(flag=='true')
	                                	{
	                                	 
	                                	      var existKycDocArray = existKycDoc.split("/");
	                                	      var existFlag = 'true';
	                                	      for(var ex=0;ex<existKycDocArray.length;ex++)
	                                	    	  {
	                                	    	       if(latestDocArray[i].split("-")[1]==existKycDocArray[ex])
	                                	    	    	   {
	                                	    	    	       existFlag = 'false';
	                                	    	    	       divValues += "<li><input id=\"checkedId"+latestDocArray[i]+"\" checked=\"true\" type=\"checkbox\" class=\"addmoreDoc\" value=\""+latestDocArray[i].split("-")[1]+"\" onclick=\"removeFromNew(this.id,this.value)\"/>" + latestDocArray[i].split("-")[0] + "</li>";
	                                	    	    	   }	                                	    	       
	                                	    	  }
	                                	      if(existFlag=='true')
	                       	    	    	   {
	                       	    	    	       divValues += "<li><input id=\"checkedId"+latestDocArray[i]+"\"  type=\"checkbox\" class=\"addmoreDoc\" value=\""+latestDocArray[i].split("-")[1]+"\" onclick=\"removeFromNew(this.id,this.value)\"/>" + latestDocArray[i].split("-")[0] + "</li>";
	                       	    	    	   }
		                                	      
	                                	}
								}
								
								divValues += "</ul>";
								
								$("#allowAccessPopContent").html(divValues);
							}
						
						
						
						/* For Academic */
						
						if(docType=="ACADEMIC_DOC")
							{
							
							    document.getElementById("popDocType").value = "ACADEMIC";
							    
							    							 
								 /* Extract the existed value */
					  			    
								    var existPattrn = document.getElementById("addedPattern").value;
								    var existPatternArray = existPattrn.split("/");
								    var existKycDoc = "";
								    for(var ex=0;ex<existPatternArray.length;ex++)
								    	{
								    	    var eachExistValue = existPatternArray[ex].split("--");
								    	    if(eachExistValue[0]=="ACADEMIC")
								    	    	{
								    	    	   existKycDoc += eachExistValue[1] +",";
								    	    	}
								    	}
								    
								    existKycDoc = existKycDoc.substring(0,existKycDoc.length-1);
								    				    
							    /* End of Extract the existed value */
						    
							    var docTypeArray = "";
							    
							    var latestDocArray = response.split(",");
							    
								for(var i=0;i<latestDocArray.length;i++)
								{
									var flag = 'true';
									
	                                for(var j=0;j<defaultPatternArry.length;j++)
	                                	 {
	                                	     var eachdefailtValue = defaultPatternArry[j].split(":");	                                	     
	                                	     if(eachdefailtValue[0]=="ACADEMIC")
	                                	    	 {
	                                	    	     docTypeArray = eachdefailtValue[1];	                                	    	                                     	    	     
	                                	    	 }		                                	     
	                                	 }
	                                
	                                if(docTypeArray!="")
                                	{
	                                	 var defaultDocArray = docTypeArray.split(",")                       	    	    
	                            	      for(var k=0;k<defaultDocArray.length;k++)
	                   	    	    	  {
	                   	    	    	     var containFlag = latestDocArray[i].indexOf(defaultDocArray[k]);
	                   	    	    	     if(containFlag>=0)
	                   	    	    	    	 {
	                   	    	    	    		 flag = 'false';
	                   	    	    	    	 }                              	    	    	    
	                   	    	    	  }	
                                	}
	                                if(flag=='true')
	                                	{
				                              if(latestDocArray[i]=="No Data")
			                             		   {
			                             		   
			                             		   }
			                             	   else
			                             		   {
				                             		  var existKycDocArray = existKycDoc.split(",");
			                                	      var existFlag = 'true';
			                                	      for(var ex=0;ex<existKycDocArray.length;ex++)
			                                	    	  {			                                	    	     
				                                	    	  if(latestDocArray[i]==existKycDocArray[ex])
		                                	    	    	   {
		                                	    	    	       existFlag = 'false';
		                                	    	    	       divValues += "<li><input id=\"checkedId"+latestDocArray[i]+"\" checked=\"true\" type=\"checkbox\" class=\"addmoreDoc\" value=\""+latestDocArray[i]+"\" onclick=\"removeFromNew(this.id,this.value)\"/>" + latestDocArray[i].split("_")[0] + "</li>";
		                                	    	    	   }			                                	    	       
			                                	    	  }		
			                                	      if(existFlag=='true')
	                               	    	    	   {
	                               	    	    	       divValues += "<li><input id=\"checkedId"+latestDocArray[i]+"\"  type=\"checkbox\" class=\"addmoreDoc\" value=\""+latestDocArray[i]+"\" onclick=\"removeFromNew(this.id,this.value)\"/>" + latestDocArray[i].split("_")[0] + "</li>";
	                               	    	    	   }
				                                   }
	                                	}
								}
								
								divValues += "</ul>";
								
								$("#allowAccessPopContent").html(divValues);
							}
						
						
						/* End of Academic */
						
						
						
                      /* For Financial */
						
						if(docType=="Financial_DOC")
							{
							
							
									/* Extract the existed value */
						  			    
									    var existPattrn = document.getElementById("addedPattern").value;
									    var existPatternArray = existPattrn.split("/");
									    var existKycDoc = "";
									    for(var ex=0;ex<existPatternArray.length;ex++)
									    	{
									    	    var eachExistValue = existPatternArray[ex].split("--");
									    	    if(eachExistValue[0]=="Financial")
									    	    	{
									    	    	   existKycDoc += eachExistValue[1] +",";
									    	    	}
									    	}
									    
									    existKycDoc = existKycDoc.substring(0,existKycDoc.length-1);
									    				    
								      /* End of Extract the existed value */
								   
							    document.getElementById("popDocType").value = "FINANCIAL";
							    var docTypeArray = "";
							
							    var latestDocArray = response.split(",");
							    
								for(var i=0;i<latestDocArray.length;i++)
								{
									var flag = 'true';
									
	                                for(var j=0;j<defaultPatternArry.length;j++)
	                                	 {
	                                	     var eachdefailtValue = defaultPatternArry[j].split(":");	                                	     
	                                	     if(eachdefailtValue[0]=="FINANCIAL")
	                                	    	 {
	                                	    	     docTypeArray = eachdefailtValue[1];	                                	    	                                     	    	     
	                                	    	 }		                                	     
	                                	 }   
	                                
	                                if(docTypeArray!="")
                                	{
	                                	 var defaultDocArray = docTypeArray.split(",")                       	    	    
	                            	      for(var k=0;k<defaultDocArray.length;k++)
	                   	    	    	  {
	                   	    	    	     var containFlag = latestDocArray[i].indexOf(defaultDocArray[k]);
	                   	    	    	     if(containFlag>=0)
	                   	    	    	    	 {
	                   	    	    	    		 flag = 'false';
	                   	    	    	    	 }                              	    	    	    
	                   	    	    	  }	
                                	}
	                                if(flag=='true')
	                                	{
	                                	      var existKycDocArray = existKycDoc.split(",");
		                              	      var existFlag = 'true';
		                              	      
		                              	      for(var ex=0;ex<existKycDocArray.length;ex++)
		                              	    	  {
		                              	    	
			                                	    if(latestDocArray[i]==existKycDocArray[ex])
		                          	    	    	   {
		                          	    	    	       existFlag = 'false';
		                          	    	    	       divValues += "<li><input id=\"checkedId"+latestDocArray[i]+"\" checked=\"true\" type=\"checkbox\" class=\"addmoreDoc\" value=\""+latestDocArray[i]+"\" onclick=\"removeFromNew(this.id,this.value)\"/>" + latestDocArray[i].split("_")[0] + "</li>";
		                          	    	    	   }		                              	    	      
		                              	    	  }	
		                              	      
		                              	    if(existFlag=='true')
	                       	    	    	   {
	                       	    	    	       divValues += "<li><input id=\"checkedId"+latestDocArray[i]+"\"  type=\"checkbox\" class=\"addmoreDoc\" value=\""+latestDocArray[i]+"\" onclick=\"removeFromNew(this.id,this.value)\"/>" + latestDocArray[i].split("_")[0] + "</li>";
	                       	    	    	   }
		                              	   	                                	   
	                                	}
								}
								
								divValues += "</ul>";
								
								$("#allowAccessPopContent").html(divValues);
							}
						
						
						/* End of Financial */
						
						
						
						
                      /* For Employee */
						
						if(docType=="Employment_DOC")
							{
							
									/* Extract the existed value */
					  			    
								    var existPattrn = document.getElementById("addedPattern").value;
								    var existPatternArray = existPattrn.split("/");
								    var existKycDoc = "";
								    for(var ex=0;ex<existPatternArray.length;ex++)
								    	{
								    	    var eachExistValue = existPatternArray[ex].split("--");
								    	    if(eachExistValue[0]=="Employment")
								    	    	{
								    	    	   existKycDoc += eachExistValue[1] +",";
								    	    	}
								    	}
								    
								    existKycDoc = existKycDoc.substring(0,existKycDoc.length-1);
								    				    
							      /* End of Extract the existed value */
					      
					      
							    document.getElementById("popDocType").value = "EMPLOYMENT";
							    var docTypeArray = "";
							
							    var latestDocArray = response.split(",");
							    
								for(var i=0;i<latestDocArray.length;i++)
								{
									var flag = 'true';
									
	                                for(var j=0;j<defaultPatternArry.length;j++)
	                                	 {
	                                	     var eachdefailtValue = defaultPatternArry[j].split(":");	                                	     
	                                	     if(eachdefailtValue[0]=="EMPLOYEMENT")
	                                	    	 {	                                	    	     
	                                	    	     docTypeArray = eachdefailtValue[1];	                                	    	                                     	    	     
	                                	    	 }	 	                                	     
	                                	 }
	                                
	                               
	                                if(docTypeArray!="")
	                                	{
		                                	 var defaultDocArray = docTypeArray.split(",")                       	    	    
		                            	      for(var k=0;k<defaultDocArray.length;k++)
		                   	    	    	  {		                            	    	 
		                            	    	 if(defaultDocArray[k]=="PaySlip")
		                            	    		 {
			                            	    		 var containFlag = latestDocArray[i].indexOf(defaultDocArray[k]);
				                   	    	    	     if(containFlag>=0)
				                   	    	    	    	 {
				                   	    	    	    		 flag = 'false';
				                   	    	    	    	 }			                   	    	    	     
		                            	    		 }
		                            	    	 if(defaultDocArray[k]=="Employment")
		                            	    		 {
		                            	    		     var containFlag = latestDocArray[i].indexOf("PaySlip");
		                            	    		     if(containFlag>=0)
				                   	    	    	    	 {
				                   	    	    	    		 flag = 'true';
				                   	    	    	    	 }
		                            	    		     else
		                            	    		    	 {
		                            	    		    	     flag = 'false';
		                            	    		    	 }
		                            	    		 }
		                   	    	    	  }	
	                                	}
	                               
	                                
	                                if(flag=='true')
	                                	{
			                                if(latestDocArray[i]=="No Document")
		                             		   {
		                             		   
		                             		   }
		                             	   else if(latestDocArray[i]=="")
		                             		   {
		                             		   
		                             		   }
		                             	   else
		                             		   {		   
		                             		         addingStatus = "Required";
		                             		         
				                             		  var existKycDocArray = existKycDoc.split(",");
				                              	      var existFlag = 'true';				                              	      
				                              	      for(var ex=0;ex<existKycDocArray.length;ex++)
				                              	    	  {				                              	    	
					                                	    if(latestDocArray[i]==existKycDocArray[ex])
				                          	    	    	   {
				                          	    	    	       existFlag = 'false';
				                          	    	    	       divValues += "<li><input id=\"checkedId"+latestDocArray[i]+"\" checked=\"true\" type=\"checkbox\" class=\"addmoreDoc\" value=\""+latestDocArray[i]+"\" onclick=\"removeFromNew(this.id,this.value)\"/>" + latestDocArray[i].split("_")[0] + "</li>";
				                          	    	    	   }		                              	    	      
				                              	    	  }	
				                              	      
					                              	    if(existFlag=='true')
				                       	    	    	  {
				                       	    	    	      divValues += "<li><input id=\"checkedId"+latestDocArray[i]+"\"  type=\"checkbox\" class=\"addmoreDoc\" value=\""+latestDocArray[i]+"\" onclick=\"removeFromNew(this.id,this.value)\"/>" + latestDocArray[i].split("_")[0] + "</li>";
				                       	    	    	  }		                              	    
		                             		   }
                             		   }
	                               
								}
								
								divValues += "</ul>";
								
								if(addingStatus=="")
									{
									    $("#allowAccessPopContent").html("No more Documents to add.");
									}
								else
									{
									    $("#allowAccessPopContent").html(divValues); 
									}
								
								  
							}
						
						
						/* End of Employee */
					}
				
			}
		});				
	}
	
	function closeAllowPopUp()
	{
		var click = [];
		$(".addmoreDoc:checked").each(function() {
	        click.push($(this).val());
	    });
		
		
		
		var popDocType = document.getElementById("popDocType").value;
		var divValues = "";		
		
		var existValueInAddedPattern = document.getElementById("addedPattern").value;
		
		if(popDocType=="KYC")
			{			   			  			    
				var kycdocArray = "";
				var kycdocName = "";
				
				var kycDocNameList = "";
				for(var m=0;m<click.length;m++)
					{
					   kycDocNameList += click[m] + ",";
					}
				
				kycDocNameList = kycDocNameList.substring(0,kycDocNameList.length-1);
				
				var kycDocNameListArray = kycDocNameList.split(",");
				
				for(var k=0;k<kycDocNameListArray.length;k++)
					{		
					        var flagValue = "";
							var eachfrontone =  kycDocNameListArray[k].indexOf("front");			    	
					    	if(eachfrontone>0)
					    		{
					    		    kycdocName = kycDocNameListArray[k].split("front")[0];	
					    		    flagValue = kycdocName+"-"+kycDocNameListArray[k]+"/";
					    		    kycdocArray += flagValue;
					    		}							    	
					    	
								    var eachbackone = kycDocNameListArray[k].indexOf("back");		
							    	
							    	if(eachbackone>0)
							    		{			    		    
							    		    if(kycDocNameListArray[k].split("back")[0]==kycdocName)
							    		    	{		
							    		    	   var tempValue = kycdocName+"-"+kycDocNameListArray[k-1]+","+kycDocNameListArray[k]+"/";
							    		    	   kycdocArray = kycdocArray.replace(flagValue, tempValue);
							    		    	   //kycdocArray += kycdocName+"-"+kycDocNameListArray[k-1]+","+kycDocNameListArray[k]+"/";							    		    	   
							    		    	}							    		    
							    		}
							    	else
							    		{
								    		var checkBackDoc = kycDocNameListArray[k].indexOf("Not Uploaded");
							    		    if(checkBackDoc==0)
								    		{			    		    											    		    		    		    					    		    	      		    	
							    		    	 var tempValue = kycdocName+"-"+kycDocNameListArray[k-1]+","+kycDocNameListArray[k]+"/";
							    		    	 kycdocArray = kycdocArray.replace(flagValue, tempValue);											    		    							    		    
								    		}						    		   
							    		}
								
					}
				
				
				    if(kycdocArray!="")
						{					    	
							kycdocArray = kycdocArray.substring(0,kycdocArray.length-1);
							
						    var attachedcDoc = kycdocArray.split("/");	
						    
							for(var i=0;i<attachedcDoc.length;i++)
								{	
								    for(var clk=0;clk<click.length;clk++)
								    	{
								    	    if(click[clk].indexOf(attachedcDoc[i].split("-")[0])>=0)
								    	    	{
								    	    	 if(i<click.length)
											    	{
											    		divValues += "<p title=\"Added Document\" class=\"aatucross\" onclick=\"removeDoc('"+click[clk]+"','"+popDocType+"')\">"+attachedcDoc[i].split("-")[0]+"</p>";							    					
											    	}	
								    	    	}
									    	
								    	}
								   						    			    							    
								}	
														
                            for(var c=0;c<click.length;c++)
                            	{
                            	   if(existValueInAddedPattern=="")
                            		   {
                            		      
                            		   		document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "KYC--"+click[c]+"/";
                            		   }
                            	   else
                            		   {                           		        
                            		        var newPatternValue = "KYC--"+click[c];                            		    	
                            		    	if(existValueInAddedPattern.indexOf(newPatternValue)<0)
                            		    		{
                            		    		    document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "KYC--"+click[c]+"/";
                            		    		}                           		    	
                            		   }
                            	        							
                            	}
						       
						}
														
				    $("#allowAccessPopLight").hide();	
				    $("#kycDiv").html(divValues);
			}	
		
		
		  /* Academic Selection  */
		 
			if(popDocType=="ACADEMIC")
			{
				for(var i=0;i<click.length;i++)
				{
				    divValues += "<p title=\"Added Document\" class=\"aatucross\" onclick=\"removeDoc('"+click[i]+"','"+popDocType+"')\">"+click[i].split("_")[0]+"</p>";
				}			
			   
				if(click!="")
					{
						 for(var c=0;c<click.length;c++)
		                 	{
							   if(existValueInAddedPattern=="")
		                  		   {
								   		document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "ACADEMIC--"+click[c]+"/"; 
		                  		   }
		                  	   else
		                  		   {		                  		        
		                  		        var newPatternValue = "ACADEMIC--"+click[c];                            		    	
		                  		    	if(existValueInAddedPattern.indexOf(newPatternValue)<0)
		                  		    		{
		                  		    		   document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "ACADEMIC--"+click[c]+"/"; 
		                  		    		}                           		    	
		                  		   }                 	        							   					   
		                 	}					  
					}
				
			    $("#allowAccessPopLight").hide();	
			    $("#academicDiv").html(divValues);
			}
		
	      /* End Of  Academic Selection  */
	      
			/* Financial Selection  */
			 
			if(popDocType=="FINANCIAL")
			{
				for(var i=0;i<click.length;i++)
				{
				    divValues += "<p title=\"Added Document\" class=\"aatucross\" onclick=\"removeDoc('"+click[i]+"','"+popDocType+"')\">"+click[i].split("_")[0]+"</p>";
				}			
			    
				if(click!="")
				{
					 for(var c=0;c<click.length;c++)
	                 	{						    
								 if(existValueInAddedPattern=="")
		                		   {
									 document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "Financial--"+click[c]+"/"; 	
		                		   }
		                	   else
		                		   {		                  		        
		                		        var newPatternValue = "Financial--"+click[c];                            		    	
		                		    	if(existValueInAddedPattern.indexOf(newPatternValue)<0)
		                		    		{
		                		    		    document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "Financial--"+click[c]+"/"; 	
		                		    		}                           		    	
		                		   } 
						  				   
	                 	}	
				}
			    $("#allowAccessPopLight").hide();	
			    $("#financialDiv").html(divValues);
			}
		
	      /* End Of Financial Selection  */
	      
	      
			/* Emp Selection  */
			 
			if(popDocType=="EMPLOYMENT")
			{
				for(var i=0;i<click.length;i++)
				{					
				    divValues += "<p title=\"Added Document\" class=\"aatucross\" onclick=\"removeDoc('"+click[i]+"','"+popDocType+"')\">"+click[i].split("_")[0]+"</p>";
				}			
			    
				if(click!="")
				{
					 for(var c=0;c<click.length;c++)
	                 	{
							 if(existValueInAddedPattern=="")
			              		   {
								 		document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "Employment--"+click[c]+"/";
			              		   }
			              	   else
			              		   {		                  		        
			              		        var newPatternValue = "Employment--"+click[c];                            		    	
			              		    	if(existValueInAddedPattern.indexOf(newPatternValue)<0)
			              		    		{
			              		    			document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "Employment--"+click[c]+"/";
			              		    		}                           		    	
			              		   } 						    					   
	                 	}	
				}
				
			    $("#allowAccessPopLight").hide();	
			    $("#empDiv").html(divValues);
			}
		
	      /* End Of  Emp Selection  */
	}
	 
	
	function removeFromNew(id,value)
	{		
		var checkStatus = document.getElementById(id).checked;
		
		if(checkStatus==false)
			{
			    var addedPattern = document.getElementById("addedPattern").value;
			    var newPattern = "";
	          
			    var addedDocArray = addedPattern.split("/");
			    var removeValueArray = value.split(",");
			    
			    for(var i=0;i<addedDocArray.length;i++)
			    {
			    	var eachOneArray = addedDocArray[i].split("--");			    	
			    	if(eachOneArray[1]!=value)
			    		{
			    		    newPattern += addedDocArray[i]+"/";			
			    		}			    	    	
			    }
			    newPattern = newPattern.substring(0,newPattern.length-1);			   
			    document.getElementById("addedPattern").value = newPattern;
			    
			}
	}
	
	function removeDoc(value,type)
	{				
		    var addedPattern = document.getElementById("addedPattern").value;
		    var newPattern = "";
		  
		    var addedDocArray = addedPattern.split("/");
		    var removeValueArray = value.split(",");
		    
		    for(var i=0;i<addedDocArray.length;i++)
		    {
		    	var eachOneArray = addedDocArray[i].split("--");		    	
		    	if(eachOneArray[1]!=value)
		    		{
		    		    newPattern += addedDocArray[i]+"/";			
		    		}			    	    	
		    }
		    
		    newPattern = newPattern.substring(0,newPattern.length-1);			    
		    document.getElementById("addedPattern").value = newPattern;		
		    
		    refreshAddedDiv(value,type);
	}
	
			
	function refreshAddedDiv(value,type)
	{		
		var popDocType = type;		
		var divValues = "";		
		var newPattern = document.getElementById("addedPattern").value;				
		var newPatternArray = newPattern.split("/");
		
		if(popDocType=="KYC")
			{	
				
				var existcontainValue = "";
				for(var exc=0;exc<newPatternArray.length;exc++)
					{
					    var eachOneArray = newPatternArray[exc].split("--");	
					    if(eachOneArray[0]=="KYC")
					    	{
					    	   existcontainValue += eachOneArray[1]+"#"
					    	}
					}
				existcontainValue = existcontainValue.substring(0,existcontainValue.length-1);
				
				var click = existcontainValue.split("#");
			
				var kycdocArray = "";
				var kycdocName = "";
				
				var kycDocNameList = "";
				for(var m=0;m<click.length;m++)
					{
					   kycDocNameList += click[m] + ",";
					}
				
				kycDocNameList = kycDocNameList.substring(0,kycDocNameList.length-1);
				
				var kycDocNameListArray = kycDocNameList.split(",");
				
				for(var k=0;k<kycDocNameListArray.length;k++)
					{			
					        var flagValue = "";
							var eachfrontone =  kycDocNameListArray[k].indexOf("front");			    	
					    	if(eachfrontone>0)
					    		{
					    		    kycdocName = kycDocNameListArray[k].split("front")[0];
					    		    flagValue = kycdocName+"-"+kycDocNameListArray[k]+"/";
					    		    kycdocArray += flagValue;
					    		}							    	
					    	var eachbackone = kycDocNameListArray[k].indexOf("back");		
					    	
					    	if(eachbackone>0)
					    		{			    		    
					    		    if(kycDocNameListArray[k].split("back")[0]==kycdocName)
					    		    	{			    		    					    		    	      		    	
					    		    	   var tempValue = kycdocName+"-"+kycDocNameListArray[k-1]+","+kycDocNameListArray[k]+"/";
					    		    	   kycdocArray = kycdocArray.replace(flagValue,tempValue);
					    		    	   //kycdocArray += kycdocName+"-"+kycDocNameListArray[k-1]+","+kycDocNameListArray[k]+"/";					    		    	   
					    		    	}							    		    
					    		}
					}
						
				
			
				    if(kycdocArray!="")
						{
							kycdocArray = kycdocArray.substring(0,kycdocArray.length-1);	
							
						    var attachedcDoc = kycdocArray.split("/");						    
							for(var i=0;i<attachedcDoc.length;i++)
								{
										for(var clk=0;clk<click.length;clk++)
								    	{
								    	    if(click[clk].indexOf(attachedcDoc[i].split("-")[0])>=0)
								    	    	{
														if(i<click.length)
												    	{
													       divValues += "<p  class=\"aatucross\" onclick=\"removeDoc('"+click[i]+"','"+popDocType+"')\">"+attachedcDoc[i].split("-")[0]+"</p>";								    
												    	}
								    	    	}
								    	}
							    }			                
						}
														
				    $("#allowAccessPopLight").hide();	
				    $("#kycDiv").html(divValues);
			}	
		
		
		  /* Academic Selection  */
		 
			if(popDocType=="ACADEMIC")
			{
				
				var existcontainValue = "";
				for(var exc=0;exc<newPatternArray.length;exc++)
					{
					    var eachOneArray = newPatternArray[exc].split("--");	
					    if(eachOneArray[0]=="ACADEMIC")
					    	{
					    	   existcontainValue += eachOneArray[1]+","
					    	}
					}
				
				  existcontainValue = existcontainValue.substring(0,existcontainValue.length-1);
				
				if(existcontainValue!="")
				{
						var click = existcontainValue.split(",");
												
						for(var i=0,j=1;i<click.length;i++,j++)
						{							
						    divValues += "<p  class=\"aatucross\" onclick=\"removeDoc('"+click[i]+"','"+popDocType+"')\">"+click[i].split("_")[0]+"</p>";
						}			
				}
				
				if(click!="")
					{
						/*  for(var c=0;c<click.length;c++)
		                 	{
							   alert(click[c]);
							   document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "ACADEMIC--"+click[c]+"/"; 					   
		                 	}	 */				  
					}
				
			    $("#allowAccessPopLight").hide();	
			    $("#academicDiv").html(divValues);
			}
		
	      /* End Of  Academic Selection  */
	      
			/* Financial Selection  */
			 
			if(popDocType=="FINANCIAL")
			{
				var existcontainValue = "";
				for(var exc=0;exc<newPatternArray.length;exc++)
					{
					    var eachOneArray = newPatternArray[exc].split("--");	
					    if(eachOneArray[0]=="Financial")
					    	{
					    	   existcontainValue += eachOneArray[1]+","
					    	}
					}
				existcontainValue = existcontainValue.substring(0,existcontainValue.length-1);
				
				if(existcontainValue!="")
				{				
					var click = existcontainValue.split(",");
										
					for(var i=0;i<click.length;i++)
					{
					    divValues += "<p  class=\"aatucross\" onclick=\"removeDoc('"+click[i]+"','"+popDocType+"')\">"+click[i].split("_")[0]+"</p>";
					}			
				}
				
				if(click!="")
				{
					 /* for(var c=0;c<click.length;c++)
	                 	{
						   document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "Financial--"+click[c]+"/"; 					   
	                 	} */	
				}
				
			    $("#allowAccessPopLight").hide();	
			    $("#financialDiv").html(divValues);
			}
		
	      /* End Of Financial Selection  */
	      
	      
			/* Emp Selection  */
			 
			if(popDocType=="EMPLOYMENT")
			{
				var existcontainValue = "";
				for(var exc=0;exc<newPatternArray.length;exc++)
					{
					    var eachOneArray = newPatternArray[exc].split("--");	
					    if(eachOneArray[0]=="Employment")
					    	{
					    	   existcontainValue += eachOneArray[1]+","
					    	}
					}
				existcontainValue = existcontainValue.substring(0,existcontainValue.length-1);
				
				if(existcontainValue!="")
				{				
						var click = existcontainValue.split(",");												
						for(var i=0;i<click.length;i++)
						{					
						    divValues += "<p  class=\"aatucross\" onclick=\"removeDoc('"+click[i]+"','"+popDocType+"')\">"+click[i].split("_")[0]+"</p>";
						}			
				}
				
				if(click!="")
				{
					 /* for(var c=0;c<click.length;c++)
	                 	{
						   document.getElementById("addedPattern").value = document.getElementById("addedPattern").value + "Employment--"+click[c]+"/"; 					   
	                 	} */	
				}
				
			    $("#allowAccessPopLight").hide();	
			    $("#empDiv").html(divValues);
			}
		
	      /* End Of  Emp Selection  */
	}
	

	
	</script>
	
	
	
</head>
<body>

<% 

   String utiName = (String)request.getAttribute("utiName");
   String utiAddress = (String)request.getAttribute("utiAddress");
   String domainPattern = (String)request.getAttribute("domainPattern");
   String patternDetails = (String)request.getAttribute("patternDetails");
   String profileDetails = (String)request.getAttribute("profileDetails");
   String utiliserId = (String)request.getAttribute("utiliserId");
   
   String availabilityStatus = (String)request.getAttribute("availabilityStatus");
   String availabilityStatusArray[] = availabilityStatus.split(",");
%>
  <%String patternArray[] = patternDetails.split("/"); %>



<div id="aatubody">
<div class="closepop_button1"></div>
<div id="aatu">
<h1><p>Allow Access To <%=utiName%></p></h1>

<table width="100%">
  
  <tr>
    <td align="left" width="150">Address</td>
    <td align="left" width="10">:</td>
    <td align="left"><%=utiAddress%></td>
  </tr>
 
  <tr>
    <td align="left" width="150">Domain Pattern</td>
    <td align="left" width="10">:</td>
    <td align="left"> <%=domainPattern%></td>
  </tr>
  
  <tr>
    <td align="left" width="150">Sharing Content</td>
    <td align="left" width="10">:</td>
    <td align="left"><textarea id="allowComment" placeholder="Sharing Comments" name="description"></textarea></td>
  </tr>
  
</table>

<!-- <h1>Default Sharing Details:</h1> -->

<div style="border-bottom: 1px solid #ccc; margin:10px;"></div>


</div>

<div id="aatu" class="aatuinnerdiv"> 
<div class="aatuleft">


<div class="aatuprofile">
<h1><p>Profile details</p></h1>

<div class="allowprofile">
<table width="100%" >
 
  <%if(!profileDetails.equals("")) {%>
  
  <%if(profileDetails.length()>3) {%>
  
     <%String profileArray[] = profileDetails.split("@@@"); %>
     
     <%String profileInfo[] =  profileArray[0].split("PATTERNSEPERETOR");%>
         
     <%for(int j=0;j<profileInfo.length;j++){ %>
     
     <%
       
       String eachArayVal[] = profileInfo[j].split("SEPERETOR");%>
     
      
	  <tr>
	  
	    <th align="left" width="120"><%=eachArayVal[1]%></th>
	    <td align="left" width="10">:</td>
	    
	   <%if(eachArayVal.length==3){ %> 
	   	    	      
	     <%if(eachArayVal[2].equals(" ")) {%>
	     
	          <td align="left">Not Available</td>
	          
	     <%}else{ %>
	        
	          <td align="left"><%=eachArayVal[2]%></td>
	          
	      <%} %>
	    
	    <%}else{ %>   
	    
	           <td align="left">Not Available</td>
	    
	    <%} %>
	    
	  </tr>
	  
       <%} %>
      
      <%} %>
    
     <%} %> 
     
</table>

</div>
</div>
</div>

 <%String status = ""; %>

<div class="aaturight">

<ul>

<li>
<h1><p>KYC Documents</p></h1>

<div  class="aatudocumentsinner">
	
	<%for(int p=0;p<patternArray.length;p++){ %>
	
	<%String eachPattern[]  =  patternArray[p].split(":");%>
	
		<%if(eachPattern[0].equals("KYC")) {%>
			
			<%if(eachPattern.length>1){ %>
			
			     <%String eachPatternArray[] = eachPattern[1].split(",");%>
			     
			     <%for(int k=0;k<eachPatternArray.length;k++) {%>
			     
			         <!-- To check the document availability -->
			         
				           <%for(int d=0;d<availabilityStatusArray.length;d++){ %>
				                				               				                
				                <%if(availabilityStatusArray[d].startsWith(eachPatternArray[k])){%>
				                      
				                      <%String eachAvailable[] = availabilityStatusArray[d].split("-"); %>
				                      
				                      <%status = eachAvailable[1];%>
				                <%} %>
				           
				           <%}%>
			        
			        <!-- To check the document availability -->   
			        
					            <%if(status.equals("Yes")){ %>
					               
					                <p title="Document Available" class="aatutick"><%=eachPatternArray[k]%></p>
					            
					            <%}else{%>
					            
					            	<p title="Document Not Available" class="imgnotavldoc"><%=eachPatternArray[k]%> <!-- <img title="Document Not Available" height="15px;" src="resources/kyc_images/document_notAvailbe.png"></img> --></p> 
					            	
					            <%}%>
			     <%} %>
			     
			<%} %>
			
		<%} %>
		
	<%} %>
	<div id="kycDiv"></div>
	
</div>

<input  class="aatuaddmore" type="button" style="width:10px;" title="Select documents to add" value="+" onclick="addMoreDocument('KYC_DOCUMENT')"/>

</li>

<li>

<h1><p>Academic Documents</p></h1>

<div class="aatudocumentsinner">

   <%for(int p=0;p<patternArray.length;p++){ %>
	
	<%String eachPattern[]  =  patternArray[p].split(":");%>
	
		<%if(eachPattern[0].equals("ACADEMIC")) {%>
			
			<%if(eachPattern.length>1){ %>
			
			     <%String eachPatternArray[] = eachPattern[1].split(",");%>
			     
			     <%for(int k=0;k<eachPatternArray.length;k++) {%>
			            
			            <!-- To check the document availability -->
			         
				           <%for(int d=0;d<availabilityStatusArray.length;d++){ %>
				                				               				                
				                <%if(availabilityStatusArray[d].startsWith(eachPatternArray[k])){%>
				                      
				                      <%String eachAvailable[] = availabilityStatusArray[d].split("-"); %>
				                      
				                      <%status = eachAvailable[1];%>
				                <%} %>
				           
				           <%}%>
			        
			           <!-- To check the document availability -->   
			        
			                 <%if(status.equals("Yes")){ %>
					               
					                <p title="Document Available" class="aatutick"><%=eachPatternArray[k]%></p>
					            
					            <%}else{%>
					            
					            	<p title="Document Not Available" class="imgnotavldoc"><%=eachPatternArray[k]%> <!-- <img title="Document Not Available" height="15px;" src="resources/kyc_images/document_notAvailbe.png"></img> --></p> 
					            	
					           <%}%>
			           
			     <%} %>
			     
			<%} %>
			
		<%} %>
		
	<%} %>
	<div id="academicDiv"></div>
</div>

<input  class="aatuaddmore" type="button" title="Select documents to add" value="+" onclick="addMoreDocument('ACADEMIC_DOC')"/>

</li>

<li>

<h1><p>Employment Documents</p></h1>

<div  class="aatudocumentsinner">
	
	<%for(int p=0;p<patternArray.length;p++){ %>
		
		<%String eachPattern[]  =  patternArray[p].split(":");%>
		
			<%if(eachPattern[0].equals("EMPLOYEMENT")) {%>
				
				<%if(eachPattern.length>1){ %>
				
				     <%String eachPatternArray[] = eachPattern[1].split(",");%>
				     
				     <%for(int k=0;k<eachPatternArray.length;k++){%>
				     
				          <!-- To check the document availability -->
			         
				           <%for(int d=0;d<availabilityStatusArray.length;d++){ %>
				                				               				                
				                <%if(availabilityStatusArray[d].startsWith(eachPatternArray[k])){%>
				                      
				                      <%String eachAvailable[] = availabilityStatusArray[d].split("-"); %>
				                      
				                      <%status = eachAvailable[1];%>
				                      
				                <%} %>
				           
				           <%}%>
			        
			           <!-- To check the document availability -->   
			        
			                 <%if(status.equals("Yes")){ %>
					               
					                <p title="Document Available" class="aatutick"><%=eachPatternArray[k]%></p>
					            
					            <%}else{%>
					            
					            	<p title="Document Not Available" class="imgnotavldoc"><%=eachPatternArray[k]%> <!-- <img title="Document Not Available" height="15px;" src="resources/kyc_images/document_notAvailbe.png"></img> --></p> 
					            	
					           <%}%>
				           
				     <%}%>
				     
				<%} %>
				
			<%} %>
			
		<%} %>
		<div id="empDiv"></div>
</div>

<input  class="aatuaddmore" type="button" title="Select documents to add" value="+" onclick="addMoreDocument('Employment_DOC')"/>

</li>

<li>

   <h1><p>Financial Documents</p></h1>

   <div class="aatudocumentsinner">
	
   <%for(int p=0;p<patternArray.length;p++){ %>
	
	<%String eachPattern[]  =  patternArray[p].split(":");%>
	
		<%if(eachPattern[0].equals("FINANCIAL")) {%>
			
			<%if(eachPattern.length>1){ %>
			
			     <%String eachPatternArray[] = eachPattern[1].split(",");%>
			     
			     <%for(int k=0;k<eachPatternArray.length;k++) {%>
			     
			           <!-- To check the document availability -->
			         
				           <%for(int d=0;d<availabilityStatusArray.length;d++){ %>
				                				               				                
				                <%if(availabilityStatusArray[d].startsWith(eachPatternArray[k])){%>
				                      
				                      <%String eachAvailable[] = availabilityStatusArray[d].split("-"); %>
				                      
				                      <%status = eachAvailable[1];%>
				                <%} %>
				           
				           <%}%>
			        
			            <!-- To check the document availability -->   
			        
			                 <%if(status.equals("Yes")){ %>
					               
					                <p title="Document Available" class="aatutick"><%=eachPatternArray[k]%></p>
					            
					            <%}else{%>
					            
					            	<p title="Document Not Available" class="imgnotavldoc"><%=eachPatternArray[k]%> <!-- <img title="Document Not Available" height="15px;" src="resources/kyc_images/document_notAvailbe.png"></img> --></p> 
					            	
					           <%}%>
			           
			     <%} %>
			     
			<%} %>
			
		<%} %>
		
	<%} %>
	
		<div id="financialDiv"></div>
		
   </div>

   <input  class="aatuaddmore" type="button" title="Add More Document" value="+" onclick="addMoreDocument('Financial_DOC')"/>

</li>
</ul>

</div>
</div>

</div>

<div style="display: block;
	    margin:0 -10px 0 auto;
	    padding: 0;
	    position: relative;float:left;">
   <ul>
	   <li style="float: left;">
	       <p class="aatutick" style="height:22px;">Document Available</p>
	   </li>
       <li style="float: left;">
            <p class="imgnotavldoc" style="height:22px;">Document Not Available</p>
       </li>
   </ul>
   
</div>


<div style="display: block;
	    margin:0 -10px 0 auto;
	    padding: 0;
	    position: relative;float:right;">
   
  <input type="hidden" id="utiliserId" value="<%=utiliserId%>" /> 
  <input type="hidden" id="defaultPattern" value="<%=patternDetails%>" />
     
  <input type="hidden" id="addedPattern" value="" />
  
    
  <input class="aatu_button" type="button" onclick="sharetoutilizer()" value="SHARE" />
  
</div>


   <!-- Allow Access Small Pop uP -->

   <div id="allowAccessPopLight">
			<div class="allowaccessPop">
			<h1><p>Select documents to add</p></h1>
		    <div id="allowAccessPopContent" > 
 	              
		    </div>	
		    <input type="hidden" id="popDocType" value="" />
		    <div style="text-align:right;margin-right:23px;margin-top: 5px;"><input type="button" value="Ok" onclick="closeAllowPopUp()"/>	 </div>  
	</div>
	</div>
	<div id="allowAccessPopfade" onclick="allowAccessfadeout()"></div> 
	
	
	 <!-- End of Allow Access Small Pop uP -->
	 
	
</body>
</html>
