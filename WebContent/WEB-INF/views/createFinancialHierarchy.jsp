<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Financial Hierarchy</title>

<script>
function newHierarchy()
	{
		$("#newFinHierId").show();
		$("#orgFinHierId").hide();
	}

function orgHierarchy()
	{
		$("#orgFinHierId").show();
		$("#newFinHierId").hide();

	}
	
function saveFinHierarchy()
{
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				
				$("#waitingdivId").show();
				
					var docType = document.getElementById("docTypeId").value;
					if(docType!="")
						{
							var docmode = ""; 
							var both = document.getElementById("docBothId");
							var original = document.getElementById("docOriId");
							var revised = document.getElementById("docRevId");
									
							if(both.checked==true)
								{
								  docmode += document.getElementById("docBothId").value;
								}
							if(original.checked==true)
								{
								    docmode += document.getElementById("docOriId").value;
								}
							if(revised.checked==true)
								{
								    docmode += document.getElementById("docRevId").value;
								}
							
							$.ajax({
								
							    type : "post",				
								url : "saveFinancialHierarchy.html",				
								data :"docType="+docType+"&docMode="+docmode,	  
								success : function(response) {
										
									$("#waitingdivId").hide();
									
									 alert("Financial hierarchy has Saved Successfully !");
													
									$("#viewUsers")
										.html(response);
													
								},
								error : function() {
									alert('Error while fetching response');
								}
							 });
						}
					else
						{
						
						$("#waitingdivId").hide();
						
						   alert("Please Enter Financial Document Type !");
						}
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
	
}

function deleteFinHierarchy()
{
	
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
				
					$("#waitingdivId").show();
				
					var docType = document.getElementById("orgdocTypeId").value;					
					if(docType!="Select Type")
						  {
								$.ajax({
										
									    type : "post",				
										url : "delteFinancialHierarchy.html",				
										data: "docType="+docType,
										success : function(response) {
												
											$("#waitingdivId").hide();
											
											 alert("Choosen financial hierarchy has deleted Successfully !");
															
											$("#viewUsers")
												.html(response);
															
										},
										
									 });	
						  }
					  else
						  {
						  
						  $("#waitingdivId").hide();
						  
						      alert("Select a Document type !!");
						  }
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
}

function updateFinHierarchy(id)
{
	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						if(id=="save")
						{
							
							$("#waitingdivId").show();
							
						var docType = document.getElementById("orgdocTypeId").value;
						      
						  if(docType!="Select Type")
							  {
							  
									var docmode = ""; 
									var both = document.getElementById("orgdocBothId");
									var original = document.getElementById("orgdocOriId");
									var revised = document.getElementById("orgdocRevId");
											
									if(both.checked==true)
										{
										  docmode += document.getElementById("docBothId").value;
										}
									if(original.checked==true)
										{
										    docmode += document.getElementById("docOriId").value;
										}
									if(revised.checked==true)
										{
										    docmode += document.getElementById("docRevId").value;
										}
									   
									var updateData = docType+":"+docmode;
									
									   $.ajax({
												
											    type : "post",				
												url : "updateFinancialHierarchy.html",				
												data: "updateData="+updateData,
												success : function(response) {
														
													$("#waitingdivId").hide();
													
													 alert("Financial hierarchy has Updeted Successfully !");
																	
													$("#viewUsers")
														.html(response);
																	
												},
												error : function() {
													
													$("#waitingdivId").hide();
													
													alert('Error while fetching response');
												}
											 });	
							 		   
							  }
						  else
							  {
							  
							  $("#waitingdivId").hide();
							  
							      alert("Select a Document type !!");
							  }
						}
					else
						{
							$("#displayMode").show();
							$("#deleteId").show();
							$("#docmodeId").hide();
						   	$("#updateId").hide();
						}
				}
			else
				{
				   $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
	
	
}
	
function displayUpdateMode()
{
   	$("#displayMode").hide();
	$("#deleteId").hide();
	$("#docmodeId").show();
   	$("#updateId").show();
}

function showOption()
{
   $("#divaddOption").show();	
}

</script>
</head>
<body>
<%
    String financialHierarchy = (String)request.getAttribute("currentHierarchy");
    String finHierarchyArray[] = financialHierarchy.split(",");  
%>


     <div>
          <span><input type="button" value="Create Hierarchy" onclick="newHierarchy()"/></span><span><input type="button" value="Organize Hierarchy" onclick="orgHierarchy()"/></span>
     </div>
     
     
     <div id="newFinHierId" style="display:block;margin-left: 30px;background-color: #fff;border-color: #999;border-radius: 5px;border-style: solid;border-width: 1px;float: left;width:800px;height:360px;">
          
         <div id="leftHierDiv" Style="margin-top:10px;margin-left: 10px;border-color: #999;border-radius: 5px;border-style: solid;border-width: 1px;float: left;width: 450px;height:339px;">
         
         <form>
		          <ul style="float:left;"> 
		              <li style="margin: 0 0 10px 0;"> 
		                   Document Type :<input type="text" name="docType" id="docTypeId"/>
		              </li>
		              <li style="margin: 0 0 10px 0;"> 
		                   Document Mode :<input type="radio" name="docmode" value="Original" id="docOriId"/>Original
		                                  <input type="radio" name="docmode" value="Revised" id="docRevId"/>Revised
		                                  <input type="radio" name="docmode" value="Both" id="docBothId"/>Both
		              </li>            
		              <li style="margin: 0 0 10px 0;">
		                   <input type="button" value="SUBMIT" onclick="saveFinHierarchy()"/>
		              </li>
		          </ul>                       
         </form> 
                
          </div>         
                   
     </div>
    
    
          
     <div id="orgFinHierId" style="display:none;margin-left: 30px;background-color: #fff;border-color: #999;border-radius: 5px;border-style: solid;border-width: 1px;float: left;width:800px;height:360px;">
          
          <div Style="float:left;">
          <ul>
              <li>
                 Document Type :
                  <select id="orgdocTypeId">
                         <option value="Select Type">Select Type</option>
                         <%for(int f=0;f<finHierarchyArray.length;f++){ %>                        
                         <option value="<%=finHierarchyArray[f].split(":")[0]%>"><%=finHierarchyArray[f].split(":")[0]%></option>
                         <%} %>
                 </select>
              </li>  
              <li style="margin: 0 0 10px 0;display:none;" id="docmodeId"> 
                    Document Mode :<input type="radio" name="docmode" value="Original" id="orgdocOriId"/>Original
                                   <input type="radio" name="docmode" value="Revised" id="orgdocRevId"/>Revised
                                   <input type="radio" name="docmode" value="Both" id="orgdocBothId"/>Both
              </li>      
              <li>
                   <span id="displayMode"><input type="button" value="Update" onclick="displayUpdateMode()"/></span>
                   <span id="deleteId"><input type="button" value="Delete" onclick="deleteFinHierarchy()"/></span>
                   <span id="updateId" style="display:none;">
                   <input type="button" value="Save" id="save" onclick="updateFinHierarchy(this.id)"/>
                   <input type="button" value="Cancel" id="cancel" onclick="updateFinHierarchy(this.id)"/>
                   </span>
              </li>        
          </ul>
          </div>
                           
     </div>
     
</body>
</html>