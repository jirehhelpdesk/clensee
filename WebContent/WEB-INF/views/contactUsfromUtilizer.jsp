<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Contact Us</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>	

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 
<%@ page import="java.text.SimpleDateFormat"%>  

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">

<link rel="stylesheet" href="resources/kyc_css/tableAdmin.css" type="text/css" />

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/style_common.css">
<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />


<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
 
<script src="resources/texteditor/nicEdit.js" type="text/javascript"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />

<style>

#sessionfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#sessionlight
{
    background: none repeat scroll 0 0 #fff;
    border: 2px solid #00b6f5;
    display: none;
    height: 46px;
    left: 50%;
     border-radius: 5px;
    margin-left: -272px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
      position: fixed;
    top: 50%; 
    width: 564px;
    z-index: 1002;
     right: -10px;  
}
</style>

<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
    
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 33%;
    width: 800px;
    z-index: 1002;
    
}


#waitfade
{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
    
}

#waitlight
{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
    
}

</style>


<script>

function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		   window.location = "utiContactus.html";
		}
}

function redirectToLoginIndividual()
{
	window.location = "utilizersessionOut.html";	
}

 </script>

<script type="text/javascript">
			function resolveSrcMouseover(e) {
				alert("Hello mouse over");
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				if (node.nodeName != "UL") {
					node.style.fontWeight= "bold";
					showRollover(e, node.innerHTML);
				}
			}
			function resolveSrcMouseout(e) {
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				node.style.fontWeight = "normal";
				clearRollover(e);
			}
			function takeAction(e) {
				var node = e.srcElement == undefined ? e.target : e.srcElement;
									
				document.getElementById("DisplayInfo").innerHTML = "Clicked " + node.innerHTML;
				
				var id = node.getAttribute("id"); 
				if (id != null && id.indexOf("F") > -1) {
					if (node.innerHTML == "-") {
						node.innerHTML = "+";
						document.getElementById("EC" + id).style.display = "none";
					} else if (node.innerHTML == "+") {
						node.innerHTML = "-";
						document.getElementById("EC" + id).style.display = "block";
					}
				}
			}
</script>

<script type="text/javascript">

	function userHome()
	{
		window.open("UtiHome.html",true);			     			
	}

</script>

<script>


function getUtiAboutUs()
{	
	
	 $.ajax({
	    	
	    	type: "post",	   
	    	url: "getutiAboutUsInfo.html",	             	 	    		       
	        success: function (response) {
	        	 
	        	var data = response.split("/");
		        
	        	if(data[0]!="No Data")
	        		{
	        		   $("#myArea1").html(data[0]);
	        		}
		       	        		        		        	
	        },	        	        
	     }); 
}

function searchIndividual(){
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{			
		
		 $('#searchhomepage').show();
			
		    $('#searchicon').show();
		    $('.topprofilestyle').show();
		    
		 $.ajax({  
		     type : "Post",   
		     url : "indsearchprofilefromUtilizer.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		 			
		    		 document.getElementById('waitlight').style.display='none';
			         document.getElementById('waitfade').style.display='none';

						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {
		    		 
			    		 var values = "No data found";			    		
			    		 
			    		 document.getElementById('waitlight').style.display='none';
				         document.getElementById('waitfade').style.display='none';

			    		 
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	else
	    {
		    $('#searchhomepage').html("");
		    document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	         $('#searchhomepage').hide();
			 $('#searchicon').hide();
			 $('.topprofilestyle').hide();	
	    }
}


function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}

function individualsearchhomepage(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}

function gotoHome()
{
    window.location = "utimyProfile.html";	
}

function  opensignOut()
{
	alert(document.getElementById("flagbutton").value);
	
	if(document.getElementById("flagbutton").value=='true')
		{
		   $("#loginBox").show();	
		   document.getElementById("flagbutton").value='false';
		}
	if(document.getElementById("flagbutton").value=='false')
		{
		   $("#loginBox").hide();	
		   document.getElementById("flagbutton").value='true';
		}
   
}

function ValidateComp()
{	
	var flag = 'true';
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	var name = document.getElementById("nameId").value;	
	var emailId = document.getElementById("emailIdid").value;	
	var mobileNo = document.getElementById("mobileNoId").value;	
	var modeOfContact = document.getElementById("modeOfContactId").value;
	var description = document.getElementById("descriptionId").value;
		
	var alfabet  = /^[a-zA-Z\s]*$/;
	var num = /^[0-9]+$/;
	var emailIdRegex = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;	
	
	if(name=="")
	{
		document.getElementById('waitlight').style.display='none';
    	document.getElementById('waitfade').style.display='none';
	   flag = 'false';
	   
	    
	     document.getElementById('sociallight').style.display='block';
	     document.getElementById('socialfade').style.display='block';
	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		 $("#socialdisplaySharedDocDiv").html("Please enter your name . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  

		  
	   return false;
	}

if(name!="")
	{
		if(!name.match(alfabet))  
        { 
			document.getElementById('waitlight').style.display='none';
	    	document.getElementById('waitfade').style.display='none';
			flag = 'false';
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Only alphabets are allowed for name . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			return false;
        }
	}

if(emailId=="")
	{
		document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
	   flag = 'false';
	   
	     document.getElementById('sociallight').style.display='block';
	     document.getElementById('socialfade').style.display='block';
	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		 $("#socialdisplaySharedDocDiv").html("Please enter a email id . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

	   return false;
	}

if(emailId!="")
	{
		if(!emailId.match(emailIdRegex))  
        { 
			document.getElementById('waitlight').style.display='none';
	    	document.getElementById('waitfade').style.display='none';
			flag = 'false';
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please enter a valid email id . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  

			return false;
        }
	}

if(mobileNo!="")
	{
		if(!mobileNo.match(num))  
        { 
			document.getElementById('waitlight').style.display='none';
	    	document.getElementById('waitfade').style.display='none';
			flag = 'false';
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please enter a valid mobile number. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			return false;
        }
		
		if(mobileNo.length!=10)
		{
			 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
			 flag = 'false';
			
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please enter a valid mobile number. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
			
			 return false;
		}
	 	
	}


if(modeOfContact=="Select Contact Mode")
	{
		document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
		flag = 'false';
		
	     document.getElementById('sociallight').style.display='block';
	     document.getElementById('socialfade').style.display='block';
	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		 $("#socialdisplaySharedDocDiv").html("Please select a contact mode. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  

		return false;
	}

   if(description=="")
	{
		document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
		flag = 'false';
		
	     document.getElementById('sociallight').style.display='block';
	     document.getElementById('socialfade').style.display='block';
	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		 $("#socialdisplaySharedDocDiv").html("Please give some description . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  />  </div>");  

		return false;
	}
	
	if(flag=='true')
		{
			$.ajax({  
				     type : "Post",   
				     url : "getNotifyOfContactUs.html", 
				     data :$('#contactUsForm').serialize(),	     	     	     
				     success : function(response) 
				     {  
				    	 document.getElementById('waitlight').style.display='none';
				         document.getElementById('waitfade').style.display='none';

				         document.getElementById('sociallight').style.display='block';
					     document.getElementById('socialfade').style.display='block';
					     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
					     $("#socialdisplaySharedDocDiv").html(""+response+" <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");   
						 
				     },  				     
			    });  
		}
}

function showSignOut()
{
	 
	var signOutFlag = document.getElementById("signOutFlag").value;
	
	if(signOutFlag=='0')
		{
		   $("#loginBox").show();
		   document.getElementById("signOutFlag").value = "1";
		}
	if(signOutFlag=='1')
		{
		   $("#loginBox").hide();
		   document.getElementById("signOutFlag").value = "0";
		}
		 
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  	
	  });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	  
	    });
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}	
</script>

</head>
<body onload="getUtiAboutUs()" onclick="hidesearchDiv()"> 

<%
	String months[] = { "January", "February", "March", "April", "May","June", "July", "August", "September", "October","November", "December" };
%>

	<div class="container">
		
		<div class="top_line"></div>
		
		
		<header class="header">
		
		<%
		   String noOfAwaitedDocuments = (String)request.getAttribute("noOfAwaitedDocuments");
		   String noOfRecivedDocuments = (String)request.getAttribute("noOfRecivedDocuments");
		%>
		
		
		<div class="header_inner">
			<div class="logo" onclick="gotoHome()"></div>

			<div class="tfclear"></div>
			<div class="utimainmenu">
			<div id="utimenu_wrapper">
				
				<ul class="uti_main_menu">
					
					<li></li>
					<li id="myMenus1"><a class="myprofilefocus" href="utimyProfile.html"> <br>
							<br> My Profile
					</a></li>
					
					<li id="myMenus2" style="width:140px">
					     
					        <a class="mydocuments" href="reciveddocuments.html" > <br>
							<br> Received Documents
					        </a>
					     
					        <%if(!noOfAwaitedDocuments.equals("0")){ %>
												  
							     <div class="noti_bubble_right"><%=noOfAwaitedDocuments%></div>
							     
							 <%} %>
					 </li>
					 
					<li id="myMenus4"><a class="myplans" href="utilizerPlan.html"> <br> <br>
							My Plans					
					</a></li>
					
					<li id="myMenus6"><a  class="alearticons" href="utilizeralerts.html"><br>
							<br> Alerts
					</a>
					</li>	
					
					<li id="myMenus5"><a class="mysettings" href="utilizerSettings.html"  > <br> <br>
							My Settings					
					</a></li>
					
				</ul>
				
			</div>
			</div>
		</div>
			
          	<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
             
              <input type="hidden" id="signOutFlag" value="0" />
                                
             <a href="#" id="loginButton" onclick="showSignOut()">
                <span></span>
                <em id="loginNameId">${utilizerProfilePic.uti_first_name}</em></a>
                 <div style="clear:both"></div>
            
            
           <div style="clear:both"></div>
          
           <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="searchIndividual()" />
             </span>
             
            </div>
            
            
            <span class="topprofilestyle" style="display: none;"></span>		
						<div id="searchicon" style="display:none;">
																
							 <div id="searchhomepage" style="display:block;">
							
							 </div>
													
				        </div>
				        
				          
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            
            
				        
            </div>
            
               <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="utisignOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
 
 
          
		</header>


		<div class="utimain"  id="mainDivID">
			
			<div class="main_center">
											
            
                													
				<div id="utileftsideId" class="utileftside">      <!--left menus should go here-->
										
						<div id="kyccontacemail">
						<h2>
						<span>
						<img width="25" height="26" src="resources/kyc_images/contact_info.png">
						<p>Contact Info</p>
						</span>
						</h2>
						<ul>	
						<li>
						<span>Customer Support</span>
						<p>support@clensee.com</p>
						
						</li>
						<li>
						
						<span>Technical support</span>
						<p>techsupport@clensee.com</p>
						</li>
						
						<li>
					<span>Marketing support</span>
					<p>marketing@clensee.com</p>
						</li>
						</ul>
						
						
					</div>
					
				</div>    <!-- End of left side Div -->
				
						
				 <%
					ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");					
					String version = resource.getString("kycversion");
					String versionDate = resource.getString("kyclatestDate");
					String contactMode = resource.getString("contactmode");
					String contactModeArray[] = contactMode.split(",");
				%>	
									
			
				<div id="utiRightSideDivId" class="utirightside">
								
									
						
				       
								       
								<!--center body should go here-->
						
						<div id="contactDivStyle" class="kyccontactform">
								<h1 style="margin-left: 0 !important; margin-top: 0 !important;">
								<b>Contact Form</b>
								<small></small>
								</h1>
								<div id="kyccontactpage">
								
								<form id="contactUsForm" method="post">
										
											 <table>
											 <tr>
											<td width="100" >Name <span>*</span></td>
											<td>:</td>
											<td>
											<input type="text" placeholder="Full Name" id="nameId" name="name">
											</td>	
											</tr>
											<tr>
											<td width="100" >E - Mail id <span>*</span></td>
											<td>:</td>
											<td>
											<input type="text" placeholder="e - Mail id" id="emailIdid" name="emailId">
											</td>	
											</tr>
												<tr>
											<td width="100" >Mobile No</td>
											<td>:</td>
											<td>
											<input type="text" placeholder="Mobile No" id="mobileNoId" name="mobileNo" maxlength="10">
											</td>	
											</tr>
												<tr>
											<td width="100">Mode of Contact</td>
											<td>:</td>
											<td>
											<select id="modeOfContactId" name="modeOfContact">
											         <option value="Select Contact Mode">Select Contact Mode</option>
											         <%for(int i=0;i<contactModeArray.length;i++){ %>
											         
											         <option value="<%=contactModeArray[i]%>"><%=contactModeArray[i]%></option>
											         
											         <%} %>
											        
											</select>
											</td>	
											</tr>
											<tr>
											<td width="100"  valign="top" >Descriptions</td>
											<td  valign="top">:</td>
											<td>
											<textarea id="descriptionId" name="description" maxlength="500" placeholder="Descriptions with in 500 character"></textarea>
											</td>	
											</tr>
										</table>
										
								</form>
										
										<div class="hscbuttondiv">
										<ul>
										<li>
										
										   <input id="buttonIdHSC" type="button" onclick="ValidateComp()" value="Send">
										
										</li>
										</ul>
										</div>
									</div>
									
									</div>
									
						<div id="suggestions1">
																 																
								 
								
                        </div>    <!-- Supports for when we search the individual -->


				  </div>				 <!-- End of rightside div -->
				
				
				<br />

			</div>            <!-- End of Main Center div -->
			
		</div>          <!-- End of Main div -->

		<!--footer block should go here-->
		
		  
		
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="utiContactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	
			<div class="footer_line"></div>

	</div>
	<!------container End-------->
	
	

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
							
							 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give Us Few Seconds.....
						       </div>
									
                      </div>
                       
	</div>
	
	<div id="waitfade" ></div> 
	
<!-- EOF Waiting Div -->

<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->


	
	<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->
			
	
	
	
</body>
</html>
