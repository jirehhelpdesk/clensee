 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Documents</title>
 
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />

</head>

<body>

 <% 
     //Required file Config for entire Controller 
	
	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 String kycMainDirectory = fileResource.getString("fileDirectory");	
	 File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller  
 %>
 			
 			
 			
 					<!-- Login Starts Here -->
           
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
						
						<div class="popheaderstyle">
							<h1>
							   Previously uploaded documents
							</h1>
						</div>
						
						
						<div style="height:477px;overflow-y:scroll;" id="historyviewpan">
						
							<c:if test="${!empty kyc_history}">
						
						 		
								<table style="align:center;">
																		
									<%int i=1; %>
									
									<c:forEach items="${kyc_history}" var="det">					 
										
										<c:set var="docsData"  value="${det.docs_data}"/>
										
					                    <% String docsData = (String)pageContext.getAttribute("docsData");
					                    
					                       String docsDataArray[] = docsData.split(",");
					                    
					                    %>
					                    
										<%if(!docsDataArray[3].equals("Captured in Registration")){ %>

											 <tr>
											  
											    <td style="magin-top:10px;margin-left:10px;">
											    
											    <p>  <b><%=docsDataArray[0].split(" front side")[0]%> :</b> <%=docsDataArray[2]%></p>
											    
											   
											    
											    <div  class="historyviewpaninner">
											   
											   
											       
											       <%if(docsDataArray[0].contains("pdf")) {%>
									                        
							                                <p> <img src="resources/images/PNA_PDF.png"   width="250" height="150" /></img></p>
							                                 <p>
							                                        <a href="download.html?fileName=<%=docsDataArray[0]%>">
							                                        <button class="documentdownloadpan"></button></a>		</p>					                                   
													                      
									                 <%}else{%>
									                            
									                              
									                                <p><%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+docsDataArray[0]+"&docCategory=KYC_DOCUMENT"%>" width="250" height="150" /></img> --%>	
									                                    <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+docsDataArray[0]+"&docCategory=KYC_DOCUMENT"%>" width="250" height="150" /></img>
									                                </p>
									                                   
									                                <p> 
									                                  <b>Front View</b>  										                          
									                                <a href="download.html?fileName=<%=docsDataArray[0]%>"><button class="documentdownloadpan"></button></a>	</p>
									                               
									                 <%} %>
											  
											
												   </div>					                      											     
											    </td>
											    
											   
											    <td style="magin-top:10px;margin-left:10px;">
											     <p><b>Modified Date:</b><%=docsDataArray[4]%></p>
											    <div class="historyviewpaninner">
											    
											    
											       
											 
											       
											        <%if(docsDataArray[1].contains("pdf")) {%>
									                                
									                          <p>  <img src="resources/images/PNA_PDF.png"  width="250" height="150"/></img></p>
									                          <p><b>Back View</b>	
							                                    <a href="download.html?fileName=<%=docsDataArray[1]%>"><button class="documentdownloadpan"></button></a></p>
									                 
									                 <%}else if(docsDataArray[1].equals("Not Uploaded")) {%> 
									                
									                         <p>  <img src="resources/images/noDocumentFound.png"  width="250" height="150"/></img></p>
									                         
									                         <p><b>Back View</b>	</p>
							                                 
									                 
									                 <%}else{%>
									                       
											                    <p><%-- <img src="${pageContext.request.contextPath}<%="/previewDocs.html?fileName="+docsDataArray[1]+"&docCategory=KYC_DOCUMENT"%>" width="250" height="150" /></img> --%>
											                        <img src="${pageContext.request.contextPath}<%="/previewThumbNailDocs.html?fileName="+docsDataArray[1]+"&docCategory=KYC_DOCUMENT"%>" width="250" height="150" /></img></p>
											                     									                                                                   
												               <p> <b>Back View</b>	
												                <a href="download.html?fileName=<%=docsDataArray[1]%>"><button class="documentdownloadpan"></button></a></p>
												       
												     <%} %>
												     
												 
												 		</div>		           
												              											     
											    </td>	
											    		
												<td class="pandescrword" style="magin-top:10px;margin-left:10px;width:250px;">
												    <p><b>Description:</b></p>
												    <p><c:out value="<%=docsDataArray[3]%>"/></p>
												
												</td>
																								
											 </tr>	
										 
										 <%} %>
										 <%i++;%>		
									</c:forEach>		
									
								</table>	
									
							</c:if>
						
						
						</div>
								
						<!-- End of Required Division for viewUser1 not go beyond  -->	
						
						
						
									
			 </body>
	 </html>