<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sms Sent Report</title>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*;" %>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">


<script>
function resendSMS(smsId)
{
	$("#waitingdivId").show();
	
	$.ajax({
		type : "post",
		url : "getresendesms.html",	
		data : "smsId="+smsId,
		success : function(response) {
			
			$("#waitingdivId").hide();
			
			alert("Successfully Sent.");
			
			 $("#viewUsers")
				.html(response);
			},	
			
	});
}

</script>

</head>
<body>


<div id="History" style="display: inline-table;
    overflow: visible; margin-top: 25px;">

<c:if test="${!empty smsDetails}">
		
	<table class='CSSTableGenerator' align="left" cellspacing="0" align="center" width="600px">
		<tr>
		    <th height="10">Serial No</th>			
			<th height="10">Mobile No</th>
			<th height="10">sent Date</th>			
			<th height="10">Job</th>
		</tr>
        <%int i=0; %>
		
		<c:forEach items="${smsDetails}" var="det">					 
								
			 <tr style="background-color:;">
    			    
			    <td height="10" width="120"><c:out value="<%=++i%>"/></td>											
				<td width="270"><c:out value="${det.mobile_no}"/></td>							
				<td width="270"><c:out value="${det.sent_date}"/></td>									
				<td align="center" width="210"><a href="#" onClick="resendSMS('${det.sms_unique_id}')"><button id="viewhistory">Send</button></a></td>
			    				
             </tr>							
								    
		</c:forEach>	
			
	</table>	
		
</c:if>


	<c:if test="${empty smsDetails}">
			
		<h1>No Data Found !</h1>
			
	</c:if>

</div>

</body>
</html>