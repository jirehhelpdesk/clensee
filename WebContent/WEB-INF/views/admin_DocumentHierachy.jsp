<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<%@ page language="java" import="java.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Document Hierarchy</title>

<link href="admin_resources/kyc_css/kyc_admin_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="admin_resources/kyc_css/kyc_Admin_LeftMenu.css">
<link rel="stylesheet" href="resources/table/table.css">

<link rel="stylesheet" href="admin_resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script type="text/javascript"src="admin_resources/kyc_scripts/demo.js"></script>

<link rel="stylesheet" href="admin_resources/css/logout.css">	
	
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />	
	
<style>


#sessionfade{

    background-color: #000;
    height: 100%;
    left: 0;
    opacity: 0.7;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1001;
    
}

#sessionlight{

    background: #fff none repeat scroll 0 0;
    border-radius: 24px;
    height: auto;
    left: 43%;
    margin-left: -211px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: auto;
    z-index: 1002;
    right: 400px !important;

}

.adminleftMenuActive
{
	color:#00b6f5 ! important;
}
</style>	


<style>

#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}
</style>
	
	
	
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
$(document).ready(function () {
    $("#menu_wrapper").on("click", "ul.main_menu>li", function (event) {
        debugger;
        event.preventDefault();
        $("#menu_wrapper .active").removeClass("active");
        $(this).find('a:first').addClass("active");
    });
});
});//]]>  



function redirectToLogin()
{
	window.location = "logout_admin.html";	
}


function gotoMainPage()
{
	window.location = "authenticateAdminLogin.html"
}

</script>

<script type="text/javascript">
	
	
	/* Script for Kyc Hierarchy  */
	
	
	
	function createAcademicHierachy()
	{
		$("#waitingdivId").show();

		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "adminleftMenuActive";
		document.getElementById("3stId").className = "";
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
							type : "get",
							url : "createAcademicHierarchy.html",				
							success : function(response) {
								
								$("#waitingdivId").hide();
					
								 $("#viewUsers")
									.html(response);
								},							
						});
						
						$("#titleBar").html("<h2><span>ACADEMIC HIERARCHY</span></h2>");	
					}
				else
					{
					       $("#waitingdivId").hide();

					       $("#sessionlight").show();
				           $("#sessionfade").show();
					}
			}
		});
		
			
	}
	
	function createFinancialHierachy()
	{
		$("#waitingdivId").show();

		document.getElementById("1stId").className = "";
		document.getElementById("2stId").className = "";
		document.getElementById("3stId").className = "adminleftMenuActive";
		
		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						$.ajax({
							type : "get",
							url : "createFinancialHierarchy.html",				
							success : function(response) {
								
									$("#waitingdivId").hide();
	
									$("#viewUsers")
										.html(response);
								},
							
						});
						
						$("#titleBar").html("<h2><span>FINANCIAL HIERARCHY</span></h2>");	
					}
				else
					{
					   $("#waitingdivId").hide();

					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
			
	}
	

function addHierarchy()
{
	$("#waitingdivId").show();

	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
					var doctype = document.getElementById("docType").value;
				 	if(doctype!="")
				 		{
					 		$.ajax({
						        url: "addkycHierarchy.html",
						        type: 'POST',	 
						        data: "docType="+doctype,
						        success: function (data) {          
						        	
						        	$("#waitingdivId").hide();

						        	CheckSessionOfAdmin('createKycHierarchy.html');
						        	
						        },
						        
						    });
				 		}
				 	else
				 		{
				 		    $("#waitingdivId").hide();

				 		    alert("Please Enter KYC Document Type !");
				 		}
				}
			else
				{
				       $("#waitingdivId").hide();

				       $("#sessionlight").show();
			           $("#sessionfade").show();
				}
		}
	});
		  
}

function showHierarchy()
{
	$("#orgKycHierDivId").show();	
}

function removeKycHierarchy()
{
	$("#waitingdivId").show();

	$.ajax({
		type : "post",
		url : "sessioncheck.html",
		cache : false,
		success : function(response) {
			
			if(response=="Exist")
				{
						var flag = 'true';
						
						var click = [];
						$(".CheckBoxClass:checked").each(function() {
					        click.push($(this).val());
					    });
						 
						if(click.length==0)
							{
							   $("#waitingdivId").hide();

							   flag='false';
							   alert("For Removing the KYC Hierarchy select at least one!");
							}
						
						
						if(flag=='true')
							{
								$.ajax({
									type : "post",				
									url : "removeKycHierarchy.html",				
									data :"click="+click,	  
									success : function(response) {
										
										$("#waitingdivId").hide();

										alert("Selected KYC Document Type has removed Successfully !");
														
										CheckSessionOfAdmin('createKycHierarchy.html');
														
									},
									error : function() {
										alert('Error while fetching response');
									}
								 });		
							}
				}
			else
				{
				    $("#waitingdivId").hide();

				    $("#sessionlight").show();
			        $("#sessionfade").show();
				}
		}
	});
	
	
}
	
	
	/* End of Script for Kyc hierarchy  */
	
	
	function CheckSessionOfAdmin(hitTheUrl)
	{
		$("#waitingdivId").show();

		$.ajax({
			type : "post",
			url : "sessioncheck.html",
			cache : false,
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = hitTheUrl;
					}
				else
					{
					    $("#waitingdivId").hide();

					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		});
					
	}
	
</script>

<!-- Scripts for admin Pattern -->

</head>
<body onload="aa()">

	<div class="container">

		<!--header block should go here-->

		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner" style="width:1120px !important">
			
			<div class="logo" onclick="gotoMainPage()">MAIN PAGE</div>

			<div class="tfclear"></div>
			<div  class=" mainmenu" >
			<div id="menu_wrapper">
				
				<ul class="main_menu" >
					
					<li></li>
										
					<li id="myMenus1">				
					<a class="myprofile" href="#" onclick="CheckSessionOfAdmin('viewindividual.html')"><span></span> My Individual
					</a>
					</li>
					
					<li id="myMenus2">				
					<a class="mydocuments" href="#" onclick="CheckSessionOfAdmin('createUtiliser.html')">	<span></span>  My Utilizer
					</a>
					</li>
					
					<li id="myMenus5"><a class="mystore" href="#" onclick="CheckSessionOfAdmin('createDomain.html')">
							Domain Hierarchy
					</a></li>
					
					<li id="myMenus6" style="width:140px;"><a class="myplans" style="color:#03b2ee" href="#" onclick="CheckSessionOfAdmin('createKycHierarchy.html')">
							Document Hierarchy
					</a>
					</li>
					
					<li id="myMenus3"><a class="myplans" href="#" onclick="CheckSessionOfAdmin('createAdminPattern.html')"> 
							My Patterns
					</a></li>
					
					<li id="myMenus41"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createfeature.html')">  My Features
					</a></li>
					
					<li id="myMenus42"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('createplan.html')">  My Plans
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('adminNotification.html')">  NOTIFICATION
					</a></li> 
					
					<li id="myMenus43"><a class="accessmanager" href="#" onclick="CheckSessionOfAdmin('admin_settings.html')"> SETTINGS </a></li> 
					
								
				</ul>
				
             </div>
                                   
			</div>

          
		</div>
   
	</header>


				 <div style="text-align: right; float: right; margin-top: -50px; width: 4%; margin-left: 10px;"> 
	               <a href="logout_admin.html" ><h4 style="color:#05b7f5;">Logout</h4></a>
	             </div>

		<div class="main" ><!-- background-color: #585759; -->
			<div class="main_center">
				<div class="leftside">

					<!--left menus should go here-->
					<div class="user_display">


					 <form>
							<div id="textBox">
							
							        <div id="button">
							        
							            <ul>
							                <li id="Hi-B-1"><a href="#" id="1stId" class="adminleftMenuActive" onClick="CheckSessionOfAdmin('createKycHierarchy.html')"><span>KYC HIERARCHY</span></a></li>
											<li id="Hi-B-1"><a href="#" id="2stId" onClick="createAcademicHierachy();"><span>ACADEMIC HIERARCHY</span></a></li>
											<li id="Hi-B-1" ><a href="#" id="3stId" onClick="createFinancialHierachy();"><span>FINANCIAL HIERARCHY</span></a></li>															
									    </ul>
									    
									</div>
								
							</div>
						</form>

					</div>




				</div>


				<div class="rightside" >

					<!--center body should go here-->
					<div id="wishAdmin">
						<h1>Welcome To KYC Admin</h1>
					</div>
					
					<div class="border_line"></div>
					<div class=" username_bar">
						 <div id="titleBar">
							<h2>
								<span></span>
								Create KYC Hierarchy 
							</h2>
						</div>
					</div>
				
					<div class="module-body" >
						<div id="viewUsers" style="height: auto !important;margin-bottom: 15px"> 
    
						 
						 
						 <!-- AdminBody startts Here -->
						 
																 
												<div id="adminkychierarchy">
												<div class="adminkychierarchy">
												      <span>Add KYC Document Type:</span><input type="text" name="kycdoctype" id="docType" /><input type="button" value="ADD" onclick="addHierarchy()"/>
												</div>
											
												<div id="viewDivision" style="margin-left: 91px;background-color: #fff;border-color: #999;border-radius: 5px;border-style: solid;border-width: 1px;float: left;height: 360px; width: 227px;">
												     
												     
												             <div id="KycHierDivId" style="height:350px;overflow-y: scroll;">
												                    <ul>
												                    <% String kycHierarchy = (String)request.getAttribute("kycHierarchy");
												        
																	        if(!kycHierarchy.equals("No Data"))
																	        {
																	      		String kycHierarchyArray[] = kycHierarchy.split(",");
																	      		for(int i=0,j=1;i<kycHierarchyArray.length;i++,j++)
																	      		{ %>
																	      		
															                        <li style="float:left;margin: 0 0 10px 0;">
															                             <%=j%>.<%=kycHierarchyArray[i]%>
															                        </li>
															                        
															                        
															                  <% } //Enf of For Loop
												                             }  //End of If%>
												                    </ul>		             
												             </div>
												     
												    
												</div>
		
													<div id="deleteDivision" style="background-color: #fff;width: 429px;border-color: #999;border-radius: 5px;border-style: solid;border-width: 1px;float: left;height: 360px;margin-left: 18px;">
													
													      <input type="button" value="Organize Hierarchy" onclick="showHierarchy()"/>
													       
													       <div id="orgKycHierDivId" style="display:none;height:312px;overflow-y: scroll;">
													               
													                <ul>
													                    <% String organisekycHierarchy = (String)request.getAttribute("kycHierarchy");
													        
																		        if(!organisekycHierarchy.equals("No Data"))
																		        {
																		      		String orgkycHierarchyArray[] = organisekycHierarchy.split(",");
																		      		for(int i=0;i<orgkycHierarchyArray.length;i++)
																		      		{ %>
																		      			 
															                 <li>  			                        
																		                       <input style="float:left;" type="checkbox" class="CheckBoxClass" value="<%=orgkycHierarchyArray[i]%>" /><span style="float:left;"><%=orgkycHierarchyArray[i]%></span>
																		                       		
																		     </li>
													               			                        					                        
																                  <% } //Enf of For Loop
													                             }  //End of If%>
													               </ul>
													                <input type="button" style="float:right;" value="REMOVE" onclick="removeKycHierarchy()"/>               
													       </div>
													       
													</div>
											</div>
																	 
						 
						 <!-- Admin Body Ends Here -->
						 
						</div>

					</div>
					

				</div>
				
			</div>
			
		</div>
		
</div>
		
		<!--footer block should go here-->
		
		   <%
			  ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			  String version=resource.getString("kycversion");
			  String versionDate=resource.getString("kyclatestDate");
		    %>
		
		
          <footer class="footer">
		   
		  
				<div class="footer_copyrights">
                <div class="copyrights">    
					 <ul >
                     		
							<li class="footer_space01"><p>Design by:<span> <a href="http://jirehsol.com/">Jireh </a></span></p>
                            <p> � All rights Reserved CLENSEE 2014-15</p>
                             <span>
									<a href="#">Version:<%=version%></a>
									</span>
                            </li>
							<li class="footer_space02"> <p><a href="#">About Us</a> |<a href="#"> Terms and Conditions</a> |<a href="#"> Privacy Policy</a> | <a href="#">Contact Us</a></p></li>
							<li class="footer_space03"> 
                            
                           <div class="four columns">
                                <ul class="social_media">
                                    <li>
                                     <a class="fb" href="https://www.facebook.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="google_plus" href="https://plus.google.com/" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="http://www.linkedin.com/company/1872121?trk=tyah" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li>
                                   
                                    
                                </ul>
                              </div>
                            
                            </li>
						   
						</ul>     
					</div>
                
				</div>
		   
			<div class="footer_line">
			</div>

		 </footer>



<!-- Waiting Div -->
	
	<div id="waitingdivId" >
									    
		    <div id="waitingdivContentId">
				    
				     <div class="kycloader">
		
		             </div>
		             <h4>Please wait For the Portal response...   </h4>
		              				    
		    </div>
	     
    </div>
	
<!-- EOF Waiting Div -->




<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv" style="text-align:left;margin-top:27px;width: auto;margin-left:-99px;						
						color:#05b7f5;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              
		        <h3>Your session has expired,Please click on the button to relogin !</h3>      
		         
		         <input type="button" value="Login" onclick="redirectToLogin()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->

	
	
	<!------container End-------->
</body>
</html>
