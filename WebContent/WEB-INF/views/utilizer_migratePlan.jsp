<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>My Plans</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 
<%@ page import="java.text.SimpleDateFormat"%>  

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">

<link rel="stylesheet" href="resources/kyc_css/tableAdmin.css" type="text/css" />
<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
 
<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />
 
 <link rel="stylesheet" href="resources/planCss/planCss.css" />
 
 <link href="resources/css/rupees/rupees.css" rel="stylesheet" type="text/css" />
 
<style>


#waitfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}


#sessionfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#sessionlight
{
    background: none repeat scroll 0 0 #fff;
    border: 2px solid #00b6f5;
    display: none;
    height: 46px;
    left: 50%;
     border-radius: 5px;
    margin-left: -272px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
      position: fixed;
    top: 50%; 
    width: 564px;
    z-index: 1002;
     right: -10px;  
}
</style>

<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 33%;
    width: 800px;
    z-index: 1002;
}

</style>

<script>
function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		 window.location = "utilizerPlan.html";
		}
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	 document.getElementById('waitlight').style.display='block';
     document.getElementById('waitfade').style.display='block';
     
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		        
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	   
	    });
	
}

function openbrowse()
 { 	 
	 var obj = document.getElementById('openbrowse');
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	
	    }	  
	   	  
 }
 </script>

<script type="text/javascript">
			function resolveSrcMouseover(e) {
				alert("Hello mouse over");
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				if (node.nodeName != "UL") {
					node.style.fontWeight= "bold";
					showRollover(e, node.innerHTML);
				}
			}
			function resolveSrcMouseout(e) {
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				node.style.fontWeight = "normal";
				clearRollover(e);
			}
			function takeAction(e) {
				var node = e.srcElement == undefined ? e.target : e.srcElement;
									
				document.getElementById("DisplayInfo").innerHTML = "Clicked " + node.innerHTML;
				
				var id = node.getAttribute("id"); 
				if (id != null && id.indexOf("F") > -1) {
					if (node.innerHTML == "-") {
						node.innerHTML = "+";
						document.getElementById("EC" + id).style.display = "none";
					} else if (node.innerHTML == "+") {
						node.innerHTML = "-";
						document.getElementById("EC" + id).style.display = "block";
					}
				}
			}
</script>


<script type="text/javascript">

	
	function migratePlan(handlerToHit) {
		
		 $.ajax({
				type : "post",
				url : handlerToHit,
				success : function(response) {
					
					document.getElementById("Hi-B-1").className = "";
					document.getElementById("Hi-B-2").className = "migrateplanactive";
					
					$('#suggestions1').show();		
					$("#utileftsideId").show();
				    $('#searchhomepage').hide();
				    
					 $("#viewUsers").html(response);
						
					},				
			});
		 		 		
		$("#titleBar").html("<h2><span>Migrate Plan</span></h2>");
		
		
		 
	}
	
	function migrateUtilizerPlan(planName)
	{
		$.ajax({
	        url: "changeUti_plan.html",
	        type: 'POST',	
	        data: "planName="+planName,
	        success: function (data) {   
		         
	        	if(data=="success")
	        		{
	        			document.getElementById('sociallight').style.background = "none repeat scroll 0 0 #e9ffd9";
			   		     document.getElementById('sociallight').style.border= "1px solid #a6ca8a";
			   		     document.getElementById('sociallight').style.display='block';
			   		     document.getElementById('socialfade').style.display='block';
			   		     $("#socialdisplaySharedDocDiv").html("<h3> Your plan Migrate to "+planName+"  <input type=\"button\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </h3>");   
			   			 
	        		    
	        		   
	        		}
	        },
		});	
	}

	
	function gotoHome()
	{
	    window.location = "utimyProfile.html";	
	}
	
	
	
	function searchIndividual(){
		
		
		var selectedvalue = $("#selectedid").val();
	    var id=$("#inputString").val();
		if(id.length>=3)
		{					
		    $('#searchhomepage').show();
			
		    $('#searchicon').show();
		    $('.topprofilestyle').show();
		    
		    document.getElementById('waitlight').style.display='block';
	        document.getElementById('waitfade').style.display='block';
	       
			 $.ajax({  
			     type : "Post",   
			     url : "indsearchprofilefromUtilizer.html", 	  
			     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
			     success : function(response) 
			     {   	    	
			    	 if(response.length>3)
			    		 {		    		 												
							document.getElementById('waitlight').style.display='none';
						    document.getElementById('waitfade').style.display='none';
							$('#searchhomepage').html(response);
							
							 var obj = document.getElementById(searchhomepage);
							 
						    if (obj.style.display=="none"){
						      obj.style.display='block';
						    } else if(obj.style.display=="block"){
						      obj.style.display='none';
						    }
			    		 }
			    	 
			    	 else if(res==2)
			    		 {
			    		     $('#searchicon').show();
				    		 var values = "No data found";			    					    		
				    		 document.getElementById('waitlight').style.display='none';
				    	     document.getElementById('waitfade').style.display='none';
				    		 $('#searchhomepage').html(response);
				    		 $('.topprofilestyle').show();
			    		 }
			         },  	    
			    }); 
			}
		
		 if(id.length<3)
		    {
			    $('#searchhomepage').html("");		 
				document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';
				$('#searchhomepage').hide();
				 $('#searchicon').hide();
				 $('.topprofilestyle').hide();
		    }
	}

function showSignOut()
{
	 
	var signOutFlag = document.getElementById("signOutFlag").value;
	
	if(signOutFlag=='0')
		{
		   $("#loginBox").show();
		   document.getElementById("signOutFlag").value = "1";
		}
	if(signOutFlag=='1')
		{
		   $("#loginBox").hide();
		   document.getElementById("signOutFlag").value = "0";
		}
		 
}



function checkSession(hittheUrl)
{
	 $.ajax({
			type : "post",
			url : "checksessionforUtilizer.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = hittheUrl;
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			   }
		}); 	
}

function redirectToLoginIndividual()
{
	window.location = "utisignOut.html";	
}
	
function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}
</script>


<link rel="stylesheet" href="resources/css/PlanDesignResources/css/planmaster.css" media="screen">


</head>

<body onclick="hidesearchDiv()"> 

	<div class="container">
		
		<div class="top_line"></div>
		
		
		<header class="header">
		
		<%
		   String noOfAwaitedDocuments = (String)request.getAttribute("noOfAwaitedDocuments");
		   String noOfRecivedDocuments = (String)request.getAttribute("noOfRecivedDocuments");
		%>
		
		
		<div class="header_inner">
			<div class="logo"  onclick="gotoHome()"></div>

			<div class="tfclear"></div>
			<div class="utimainmenu">
			<div id="utimenu_wrapper">
				<ul class="uti_main_menu">
					<li></li>
					<li id="myMenus1"><a class="myprofile" href="#" onclick="checkSession('utimyProfile.html')"> <br>
							<br> My Profile
					</a></li>
					
					
					<li id="myMenus2" style="width:140px">
					     
					        
					        <a class="mydocuments" href="#" onclick="checkSession('reciveddocuments.html')"> <br>
							<br> Received Documents
					        </a>
					     
					        <%if(!noOfAwaitedDocuments.equals("0")){ %>
												  
							     <div class="noti_bubble_right"><%=noOfAwaitedDocuments%></div>
							     
							 <%} %>
					     
					 </li>
					 
					
					<li id="myMenus4"><a class="myplansfocus" href="#" onclick="checkSession('utilizerPlan.html')"> <br> <br>
							My Plans					
					</a></li>
					
					<li id="myMenus6"><a  class="alearticons" href="#" onclick="checkSession('utilizeralerts.html')"><br>
							<br> Alerts
					</a>
					</li>	
					
					<li id="myMenus5"><a class="mysettings" href="#"  onclick="checkSession('utilizerSettings.html')"> <br> <br>
							My Settings					
					</a></li>
					
				</ul>
			</div>
			</div>
		</div>
		
		
					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
             
              <input type="hidden" id="signOutFlag" value="0" />
                                
             <a href="#" id="loginButton" onclick="showSignOut()">
               
                <span></span>
                <em id="loginNameId">${utilizerProfilePic.uti_first_name}</em></a>
                <div style="clear:both"></div>
            
            
           <div style="clear:both"></div>
           
             <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="searchIndividual()" />
             </span>
             
            </div>
            
             <span class="topprofilestyle" style="display: none;"></span>	
					   <div id="searchicon" style="display:none;">
																
							 <div id="searchhomepage" style="display:block;">
							
							 </div>
													
				       </div>
								
								
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            
            </div>
            
               <div style="clear:both"></div>
                <div id="loginBox">  
                          
                    <span></span>   
                   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        	
	                        <fieldset>                       
	                        	<a href="utisignOut.html"><input type="button" id="login" value="Sign out"/></a>                       
	                        </fieldset>    
	                                                                    
                        </fieldset>
                       
                    </form>
                    
                </div>
            </div>
            <!-- Login Ends Here -->
 
 
          
		</header>


		<div class="main"  id="mainDivID">
			
			<div class="main_center">
											
            	
                													
				<div class="utileftside">      <!--left menus should go here-->
										
						<div class="user_display">  
																				
								<div id="textBox">
																														
										<div id="button">
											   
											   <ul>
											       <li id="myplanext"><a href="utilizerPlan.html" id="Hi-B-1" ><span>My Plan</span></a></li>
												   <li id="migrateplan"><a href="Uti_Migrateplan.html" id="Hi-B-2" class="migrateplanactive"><span>Migrate Plan</span></a></li>																									 														
											   </ul>
											   
										</div>																				
								</div> 			
								
								<!-- End of textBox Div  -->
								
													
						</div>           <!-- End of user_display -->
					
				</div>    <!-- End of left side Div -->


							
				<div id="utiRightSideDivId" class="utirightside">
							
							
					  
				       					       
								<!--center body should go here-->
						
						
						
						
						
						
						<!-- Advertisment Div for Entire Page  -->
											
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
																
							%>
                                  <div id="ProfileHeadingDiv" style="display:block;">
									
										<marquee behavior="scroll" scrollamount="5" direction="left">
											   <h1 class="rightsideheader">
											       
											        <%=utiAdvertise%>
											        
											   </h1>
									     </marquee>
							        </div>	
							       							        		
					   <!-- Advertisment Div for Entire Page  -->
					   
					   
					   
						<div id="suggestions1">
												 																
								  <div class="border_line"></div>
								  
								  <div class=" username_bar">
										
										<div id="titleBar">
											
											<h2>
												 <span>Migrate Plan</span> 									    												
											</h2>
											
										</div>     
										
									 <!-- End of tittleBar Div -->
										
								  </div>			<!-- End of username_bar Div -->
									
								
								
								<div class="utimodule-body">
									
											<div id="viewUsers" style="height: auto; width: auto;  margin: 0 auto;">
					                           
							                    <!-- Statr Div of viewUsers -->     
							                         
							                    		<div id="planmain">
							                    		
							                    		
	
		<%String  featureName = (String)request.getAttribute("featurecategory");
		  String featureArray[] = featureName.split(",");
		  
		  String basicDetails = (String)request.getAttribute("basicDetails");
		  String basicDetailsArray[] = basicDetails.split(",");
		  
		  String silverDetails = (String)request.getAttribute("silverDetails");
		  String silverDetailsArray[] = silverDetails.split(",");
		  
		  String goldDetails = (String)request.getAttribute("goldDetails");
		  String goldDetailsArray[] = goldDetails.split(",");
		  
		  String platinumDetails = (String)request.getAttribute("platinumDetails");
		  String platinumDetailsArray[] = platinumDetails.split(",");
		  
		  String currentPlan = (String)request.getAttribute("planname");
			 
		  String extrafeatureNames = (String)request.getAttribute("extrafeatureNames");
		  
		  %>	
		  
		  

							                    		
   
   <div id="kcyplanContainer">
		
	  <div id="tableContainer">
		
			<div class="tableCell highlight ">
				<div class="tableHeading" style="margin-left:0px;">
					<h2>Features</h2>
					
				<div class="kycplanprice1 kycplanpriceContainer">
						<p><img src="resources/images/cloud_kyc.png" /><sup></sup></p>
						<span></span>
					</div> 	<!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>
		
		
			<div class="tableCell">
				<div class="tableHeading">
					<h2>Basic</h2>
					
					<div class="kycplanprice4 kycplanpriceContainer">
						<p>Free<sup></sup></p>
						<!-- <span>per month</span> -->
					</div> <!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>

			
			<div class="tableCell noBorder">
				<div class="tableHeading">
					<h2>Silver</h2>
					
					<div class="kycplanprice3 kycplanpriceContainer">
						<div class="WebRupee"> Rs.</div><%=request.getAttribute("silverprice")%><br>
						<span>per annum</span>
					</div> <!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>
            
		  
		  <div class="tableCell">
				<div class="tableHeading">
					<h2>Gold</h2>
					
					<div class="kycplanprice2 kycplanpriceContainer">
						<div class="WebRupee"> Rs.</div><%=request.getAttribute("goldprice")%><br>
						<span>per annum</span>
					</div> <!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>
			
			
			
			<div class="tableCell noBorder">
				<div class="tableHeading">
					<h2>Platinum</h2>
					
					<div class="kycplanprice5 kycplanpriceContainer">
						<div class="WebRupee"> Rs.</div><%=request.getAttribute("platinumprice")%><br>
						<span>per annum</span>
					</div> <!-- end kycplanprice1 -->
				</div> <!-- end tableHeading -->
			</div>
			
			
		<div class="cl"><!-- --></div>
				
			
			
			<table class="pricingTableContent" style="margin-left: 0;width:795px;">
				
				<tr class="LearnMoreRow">
					<td class="highlight" width="202"></td>
					<td width="202"></td>
                    <td width="202"></td>
					<td width="200" class="noBorder"></td>
					<td width="200 " class="noBorder"></td>
				</tr>
				
				
				<!-- Table Content -->
				
				
				
				                            <%for(int i=0,j=1;i<featureArray.length;i++,j++) {%>	<!-- Start of Feature Name for-loop -->
															
															<%if(j%2==0){%>
															<tr>
															<%}else{%>
															<tr class="altRow">						
															<%}%>	
																			
																<%if(!extrafeatureNames.contains(featureArray[i])) {%>				    
																
																    <td class="highlight"><strong><%=featureArray[i]%></strong> </td>
																
																<%} %>
																
																
																
																<%boolean basiccondition = true;
																for(int k=0;k<basicDetailsArray.length;k++) {
																    String basicDataArray[] = basicDetailsArray[k].split("##");
																      %>
																       
																       <%if(k==1){ %>
																       
																               
																               <%if(featureArray[i].equals(basicDataArray[0])){ 
																		      
																			     basiccondition = false;
																		        %>
																		
																				<td><strong><%=basicDataArray[1]%></strong></td>
																				
																		       <% }%>	
																		
																       <%}else{ %>
																       
																	             <%if(featureArray[i].equals(basicDataArray[0])){ 
																			      
																				     basiccondition = false;
																			        %>
																			
																					<td><strong><%=basicDataArray[1]%></strong></td>
																					
																			     <% }%>	
																		
																       <%}%>
																       
																									     								      	
																  <%} %>
																  
																<%--   <%if(basiccondition == true) {%>
																  
																                 <td><strong>NA</strong></td>
																              
																   <%} %> --%>
																   
																   
																   
																
																<%boolean silvercondition = true;
																for(int k=0;k<silverDetailsArray.length;k++) {
																    String silverDataArray[] = silverDetailsArray[k].split("##");
																      %>
																    
																		<%if(k==1){ %>	
																				
																					<%if(featureArray[i].equals(silverDataArray[0])){ 
																		      
																			        silvercondition = false;
																		           %>
																		
																				  <td><strong><%=silverDataArray[1]%></strong></td>
																				
																		           <% }%>	
																		
																		<%}else{ %>
																					
																					<%if(featureArray[i].equals(silverDataArray[0])){ 
																		      
																			        silvercondition = false;
																		           %>
																		
																				    <td><strong><%=silverDataArray[1]%></strong></td>
																				
																		           <% }%>	
																		<%}%>						     								      	
																  <%} %>
																
																 <%--  
																  <%if(silvercondition == true) {%>
																  
																              <td><strong>NA</strong></td>
																              
																  <%} %> --%>
																
																
																<%boolean goldcondition = true;
																for(int k=0;k<goldDetailsArray.length;k++) {
																    String goldDataArray[] = goldDetailsArray[k].split("##");
																      %>
																    
																      <%if(k==1){ %>
																      
																      				<%if(featureArray[i].equals(goldDataArray[0])){ 
																		      
																						goldcondition = false;
																					        %>
																					
																							<td><strong><%=goldDataArray[1]%></strong></td>
																							
																					 <% }%>
																		 
																      <%}else{ %>
																      				
																      				<%if(featureArray[i].equals(goldDataArray[0])){ 
																		      
																						goldcondition = false;
																					        %>
																					
																							<td><strong><%=goldDataArray[1]%></strong></td>
																							
																					 <% }%>
																      <%} %>
																		
																										     								      	
																  <%} %>
																  
																  <%-- <%if(goldcondition == true) {%>
																  
																              <td><strong>NA</strong></td>
																              
																  <%} %>
																 --%>
																
																<%boolean platinumcondition = true;
																for(int k=0;k<platinumDetailsArray.length;k++) {
																    String platinumDataArray[] = platinumDetailsArray[k].split("##");
																      %>
																    
																       <%if(k==1){ %>
																       
																       					<%if(featureArray[i].equals(platinumDataArray[0])){ 
																		      
																							platinumcondition = false;
																						        %>
																						
																								<td><strong><%=platinumDataArray[1]%></strong></td>
																								
																						<%}%>	
																		
																       <%}else{ %>
																       
																       					<%if(featureArray[i].equals(platinumDataArray[0])){ 
																		      
																							platinumcondition = false;
																						        %>
																						
																								<td><strong><%=platinumDataArray[1]%></strong></td>
																								
																						<%}%>	
																       <%} %>
																									     								      	
																  <%} %>
																  
																 <%--  <%if(platinumcondition == true) {%>
																  
																              <td><strong>NA</strong></td>
																              
																  <%} %> --%>
															</tr>
															
									                <%} %>													<!-- End of Feature Name for-loop -->
									
				
		
						
				                               <tfoot>
														<tr>
															<td class="highlight">
																  <h1 style="margin-left:37px;"> Apply Plan  </h1>
															</td>
									                                      
									                       <!--  <td>
															     	<h1 style="margin-left:60px;">Active</h1>									
															</td>
															
															<td>
																    <h1 style="margin-left:37px;">Coming Soon </h1>	
															</td>
									
															<td >
																    <h1 style="margin-left:37px;">Coming Soon </h1>	
															</td>
									
															<td class="noBorder">
																
																    <h1 style="margin-left:37px;">Coming Soon </h1>	
															</td> 
															 -->
													
													 <td>
															
																
																<%if(currentPlan.equals("Basic")) {%>
																
																  <a class="LearnMoreButton" style="background-color: #00b6f5 ! important;">Active Plan</a>
																
																<%} %>													
															</td>
															
															<td>
																								
																<%if(currentPlan.equals("Platinum")) {%>
																	 <form action="utiRequestplanchange.html" class="formDisable"  method="post">
																       <input type="hidden" name="planName" value="Silver" />
																       <input type="submit" disabled="true" class="planbutton"  value="Downgrade" />
																     </form>
																<%}else if(currentPlan.equals("Gold")) {%>
																	 <form action="utiRequestplanchange.html" class="formDisable"  method="post">
																       <input type="hidden" name="planName" value="Silver" />
																       <input type="submit" disabled="true" class="planbutton"  value="Downgrade" />
																     </form>
																<%}else if(currentPlan.equals("Silver")) {%>
																
																	 <a class="LearnMoreButton" style="background-color: #00b6f5 ! important;">Active Plan</a>
																	
																<%}else { %>
																
																     <form action="utiRequestplanchange.html" method="post">
																       <input type="hidden" name="planName" value="Silver" />
																       <input type="submit" class="planbutton"  value="Upgrade" />
																     </form>
																  
																<%}%>
															</td>
									
															<td >
																
																<%if(currentPlan.equals("Platinum")) {%>
																     
																  <form action="utiRequestplanchange.html" class="formDisable"  method="post">
																       <input type="hidden" name="planName" value="Gold" />
																       <input type="submit" disabled="true" class="planbutton"  value="Downgrade" />
																  </form>															
																
																<%}else if(currentPlan.equals("Gold")) {%>
																        
																        <a class="LearnMoreButton" style="background-color: #00b6f5 ! important;">Active Plan</a>
																        
																<%}else{ %>
																   
																  <form action="utiRequestplanchange.html" method="post">
																       <input type="hidden" name="planName" value="Gold" />
																       <input type="submit" class="planbutton"  value="Upgrade" />
																  </form>
																
																<%} %>
																
																
															</td>
									
															<td class="noBorder">
																
																<%if(currentPlan.equals("Platinum")) {%>
																
																     <a class="LearnMoreButton" style="background-color: #00b6f5 ! important;">Active Plan</a>
																
																<%}else{ %>
																
																  <form action="utiRequestplanchange.html" method="post">
																       <input type="hidden" name="planName" value="Platinum" />
																       <input type="submit" class="planbutton"  value="Upgrade" />
																  </form>	
																  
																<%} %> 
																
															</td>
															
															
															
															
														</tr>
													</tfoot>
													
													
			</table>
			
		</div> 
		
		
		<div style="margin-left:3px;text-align:left;">
		    
		    <ul>  
		    
		        <li style="margin-top:0px;">
					<h1 style="font-size: 18px;"></h1>
					
					<span style="color: #7c7c7c;font-size: 12px;">
						<img src="resources/images/paymentLogo.jpg" width="800" height="80"></img>
					</span>
		        </li>
		        
			      <li style="margin-top:0px;">
				      
				      <h1 style="font-size: 18px;">Profile Viewing</h1>
				        
				        <span style="color: #7c7c7c;font-size: 12px;">
						This feature enables Utilizer to view individuals profile based on the plan selected.
						</span>
						
						<div>
						
						   <ul><li>
						   
							<h1 style="font-size: 12px !important; margin-left: 40px;">Basic Profile</h1>
							
							<p style="color: #7c7c7c;font-size: 12px;text-align:left ; margin-left: 40px;">
							Profile picture,About me, Registration details, Basic details, Family details and Social account
							details of other individuals can be seen
							</p>
							
							<h1 style="font-size: 12px !important; margin-left: 40px;">Full Profile</h1>
							
							<p style="color: #7c7c7c;font-size: 12px; margin-left: 40px;">
							 Basic Profile, Current company and Education details of other individuals can be seen.
							</p>
							</li></ul>
					   </div>
					
				  </li>
			  	
			  	
								
				<li style="margin-top:0px;">
					<h1 style="font-size: 18px;">SMS</h1>
					
					   <span style="color: #7c7c7c;font-size: 12px;">
					    This feature enables to send sms alerts to individuals and utilizers,when someone share documents to someone.
						<!-- This feature enables to receive sms alerts when you receive the documents from other individuals. -->
						</span>
			    </li>
			    
			   
			</ul>
		</div>
		
		
		<!-- end tableContainer -->

	</div> <!-- end pageContainer -->	
									                    		
							                    		
												                    								                   
	</div>
							                    
							                    <!-- End of Div of viewUsers -->        							                            							                          
					
											</div>			
											
											
									
			
								</div>     
								
								 <!-- End of module-body Div -->
							
                           </div>

				  </div>				 <!-- End of rightside div -->
				
				
				<br />

			</div>            <!-- End of Main Center div -->
			
		</div>          <!-- End of Main div -->









		<!--footer block should go here-->
		
		<%
ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
String version=resource.getString("kycversion");
String versionDate=resource.getString("kyclatestDate");
%>

		
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="utiContactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	
			<div class="footer_line"></div>

	</div>
	<!------container End-------->
	




<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
								
								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give Us Few Seconds.....
						       </div>
									
                      </div>
                     
	</div>
	
	<div id="waitfade" ></div> 	

	
<!-- EOF Waiting Div -->

<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
			
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->
		
<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->
	
	
	
</body>
</html>
