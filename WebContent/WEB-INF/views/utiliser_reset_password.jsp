
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reset Password</title>

<link href="resources/css/kycstyle.css" rel="stylesheet" type="text/css" />
<link href="resources/css/blue.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>
<script src="resources/onlineresources/code.jquery.com.jquery-1.9.1.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.10.2.jquery-ui.js"></script>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 


<script src="resources/kyctabs/jquery.easytabs.min.js"></script>
<script src="resources/kyctabs/jquery.hashchange.min.js"></script>
<link href="resources/kyctabs/kyc_tabpsstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/UtiCss/doctable.css"/>

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />
<link rel="stylesheet" href="resources/documentResources/kyc_document_css.css" />

<link rel="stylesheet" href="resources/css/style_common.css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css"/>


<!-- /* Pop up Script and css*/ -->

<script type="text/javascript">

window.document.onkeydown = function (e)
{
    if (!e)
    {
        e = event;
    }
    if (e.keyCode == 27)
    {
    	successlightbox_close();
    }
}


function successlightbox_close(action){
	
	if(action=='warning')
		{
			document.getElementById('successlight').style.display='none';
		    document.getElementById('successfade').style.display='none';		  
		}
	else
		{
			document.getElementById('successlight').style.display='none';
		    document.getElementById('successfade').style.display='none';
		    /* x=document.getElementById("saveButtonId")
		    x.disabled = !x.disabled; */
		    window.location = "utiliser.html";
		}       
}

</script>	

<style type="text/css">

#successfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#successlight{
   background: none repeat scroll 0 0 #fff;
    border: 2px solid #00b6f5;
    display: none;
    height: 53px;
    left: 50%;
    margin-left: -272px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 564px;
    z-index: 1002;
}


</style>

<!-- /* End of Pop up Script and css*/ -->



<script type="text/javascript">

function searchhomepage(){
	
	var id1=$("#searchvaluetext").val();
	
	if(id1.length>=3)
	{
		$(".topdivespan").show();		
		$('#searchhomepage').show();	
		$("#waitingdivId").show();
		$("#waitingdivContentId").show();
		 
		$.ajax({ 
			
		     type : "Post",   
		     url : "search_Outerprofile.html", 	  
		     data : "kycid="+id1,	     	     	     
		     success : function(response) 
		     {   					
					$("#waitingdivId").hide();		
					$("#waitingdivContentId").hide();
					 
					$('#searchhomepage').html(response);						 	    	
		     },  
		    
		    }); 		
	}	
	else 		
	{	
		$(".topdivespan").hide();	
		$('#searchhomepage').html("");
		$('#searchhomepage').hide();		    		
		$("#waitingdivId").hide();
		$("#waitingdivContentId").hide();
	}	 
}



function resetPassword()
{	
	var flag = 'true';
	
	var password = document.getElementById("newpwid").value;
	var confPassword = document.getElementById("confirmpwid").value;
	var uniqueVariable = document.getElementById("indId").value;
	
	if(password=="" & confPassword=="")
	{
		   flag='false';
	       $("#regErrorId").show();
	       $("#regErrorId").html("Please enter password and confirm the password.");
	       document.getElementById("newpwid").value = "";
	   	   document.getElementById("confirmpwid").value = "";
	   	   return false;
	}
	
	else if(password!="" & confPassword!="")
	{		
		if(!password.match(".*[a-z].*"))
		{			  
		   flag='false';
		   
		   $("#regErrorId").show();
		   $("#regErrorId").html("Password must contain, <br>"+
	    		   "1. atleast one lowercase letter [a-z]<br>"+
	    		   "2. atleast one uppercase letter [A-Z]<br>"+
	    		   "3. atleast one number [0-9]<br>"+
	    		   "4. minimum of 6 character length");
	       document.getElementById("newpwid").value = "";
	   	   document.getElementById("confirmpwid").value = "";
	       return false;
		}
		
		if(!password.match(".*[A-Z].*"))
		{			
		   flag='false';
		   $("#regErrorId").show();
		   $("#regErrorId").html("Password must contain, <br>"+
	    		   "1. atleast one lowercase letter [a-z]<br>"+
	    		   "2. atleast one uppercase letter [A-Z]<br>"+
	    		   "3. atleast one number [0-9]<br>"+
	    		   "4. minimum of 6 character length");
		   document.getElementById("newpwid").value = "";
	   	   document.getElementById("confirmpwid").value = "";
	   		return false;
		}
		
		if(!password.match(".*\\d.*"))
		{			 
		   flag='false';
		   $("#regErrorId").show();
		   $("#regErrorId").html("Password must contain, <br>"+
	    		   "1. atleast one lowercase letter [a-z]<br>"+
	    		   "2. atleast one uppercase letter [A-Z]<br>"+
	    		   "3. atleast one number [0-9]<br>"+
	    		   "4. minimum of 6 character length");
		   document.getElementById("newpwid").value = "";
	   	   document.getElementById("confirmpwid").value = "";
	  	 	return false;
		}
		
		if(password.length<6)
		{
			   flag='false';
			   $("#regErrorId").show();
			   $("#regErrorId").html("Password must contain, <br>"+
		    		   "1. atleast one lowercase letter [a-z]<br>"+
		    		   "2. atleast one uppercase letter [A-Z]<br>"+
		    		   "3. atleast one number [0-9]<br>"+
		    		   "4. minimum of 6 character length");
			   document.getElementById("newpwid").value = "";
		   	   document.getElementById("confirmpwid").value = "";
		  	   return false;
		}
		
	    if(password!=confPassword)
	    {	  
	       flag='false';
	       $("#regErrorId").show();
	       $("#regErrorId").html("Given password and confirm password not matched.");
	       document.getElementById("newpwid").value = "";
	   	   document.getElementById("confirmpwid").value = "";
	   	   return false;
	    }
	    
	}
	else 
	{
		flag='false';
		$("#regErrorId").show();
	    $("#regErrorId").html("Fill the required fields.");
		return false;
	}
	
	if(flag=='true')
		{
			$.ajax({
			       url: "utiResetPassword.html",
			       type: 'post',
			       data: "newPassword="+confPassword+"&uniqueVariable="+uniqueVariable,        
			       success: function (data) { 
			       	if(data=='success')
			       		{
				       		document.getElementById('successlight').style.display='block';
				            document.getElementById('successfade').style.display='block';
				            $("#successdisplaySharedDocDiv").html("Your password has been reseted successfully."+"<input type=\"button\" value=\"OK\" style=\"margin-top: 9px;\" class=\"button_style\" onclick=\"successlightbox_close('success')\" />");
			       		}
			       	else
			       	{
			       		alert("Failed due to some Server Problem Try again ! ");
			       	}
			       },
			});
		}
	else
		{
		         alert("Newpassword and Confirmpassword are not matched ! ");
		}
	
  
}


function gotoHomePage()
{
	window.location = "clenseeHome.html";
}

function hideSearchBar()
{
	$(".topdivespan").hide();		
	$('#searchhomepage').hide();		
}

</script>


  <script type="text/javascript">
    $(document).ready( function() {
      $('#tab-container').easytabs();
    });
  </script>

</head>
<body>
<%String uniqueVariable = (String)request.getAttribute("uniqueVariable"); %>
<div id="centerbody">
 <div class="container">
		<div class="top_line"></div>
		
			<header class="header">
			<div class="header_inner">
				<div class="logo"  onclick="gotoHomePage()">
				</div>
			  
			  <div class="search-bar">
				 <div id="tfheader">
					
						 <input type="text" id="searchvaluetext" maxlength="50" class="tftextinput2 button-submit" name="q" size="21" maxlength="120" title="Enter atleast 3 character for efficent search !" autofocus="autofocus" placeholder="Search Individual" onkeyup="searchhomepage()"/>
                         <input type='image' class="search_button-submit" alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="searchhomepage()"/>
						 						
						 <div class="tfclear" ></div>
				 </div>     
			  </div>                
			  
			  <div class="tfclear" onclick="hideSearchBar()"></div>
					 <div class=" mainmenu">
								          
							 <ul class="main_menu" onclick="hideSearchBar()">
                             
								<li><a class="individual" href="individual.html"></a></li>
								<!-- <li><a class="corporate" href="corporate.html"></a></li> -->
								<li><a class="utiliser" href="utiliser.html"></a></li>
							   
							 </ul>   
						</div>       				
				 </div>
			</header>
			
			
			<div style="width:100%;height:100%;" class="banner_center_individual">
									
            <div class="main" onclick="hideSearchBar()">
                           
                           <div id="searchhomepage"  style="display: none;width:75% !important">
                        
                        
                         
                          </div>
                                  
			<div class="leftside"> 
			<img src="resources/images/frontpage-kyc.png">
			
			
			<div id="regErrorId" style="display:none;color:red;					
						font-family:Tahoma,Geneva,Arial,sans-serif;font-size:13px;
						padding:6px; 
					    margin: -359px auto 0 816px; 
					    width:500px;text-align: left;"></div><!-- width:417px; -->
					    
			</div>
				
					<div class="rightside">
					
					
                <%String resetStatus = (String)request.getAttribute("resetStatus"); %>               
				
				<%if(resetStatus.equals("Allow")) {%>
																									
		        <div id="tab-container" class='tab-container' style="margin-top:50px;">
				
				         <ul class='etabs'>						   			   
						   <li class='tab'><a href="#tabs1-css">Reset Password</a></li>
						 </ul>
						 
						<div class='panel-container'>
					
								<div id="tabs1-js">
								 
										<form >
												<p>
												<input class="field" type="hidden" value="<%=uniqueVariable%>" id="indId"  name="uniqueVariable"/>
												</p>
												<p>
												<input class="field" type="password" placeholder="New Password" id="newpwid" required name="newPassword"/>
												</p>
												<p>
												<input class="field" type="password" placeholder="Confirm Password" id="confirmpwid" required name="confirmpassword"/>
												</p>
												<p class="keeplogin">
												
												<input type="button" value="Save" onclick="resetPassword()"   />
												
												</p>
										</form>
								
								</div>
																		
					    </div>

		       </div>
	
				<%} else if(resetStatus.equals("Not Allow"))  { %>
				
				<div id="tab-container" class='tab-container' style="margin-top:110px;">
				
						<div class='panel-container'>
												
								<h3>Your reset password link has already expired !</h3>	
																																
					    </div>

		       </div>
				<%}else{ %>	
			
				<div id="tab-container" class='tab-container' style="margin-top:110px;">				
						<div class='panel-container'>
												
							<h3>Your Account has Blocked by KYC Admin,Contact to Support team for further process !</h3>	
																																
					    </div>
		        </div>
				<%} %>
	      </div>   
		</div>				
		</div>	
		
          <footer class="footer">
		   
		  
				
			<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactusbeforeLogin.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 905px !important; margin-top: -1px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>
					
				</ul>
			</div>

		</div>
		   
			<div class="footer_line">
			</div>

		 </footer>
           
	</div>	
</div>		
<!------center End-------->
		
 
<!------container End-------->
<div id="successlight">
			<div class="colsebutton" onclick="successlightbox_close()"><img height="22" width="22" style="margin-left:568px;margin-top:-24px;" src="resources/images/close_button.png" /></div>
		    <div id="successdisplaySharedDocDiv" style="text-align:left;margin-top:27px;width:529px;margin-left:-99px;background:#00b6f5 url('../kyc_images/success.png') no-repeat 10px 50%;
						border:1px solid #00b6f5;
						color:#555;
						border-radius:10px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		  
		    </div>		   
	</div>
	
	<div id="successfade" onclick="successlightbox_close();"></div> 
	
	
	<!-- Waiting Div -->
	
	
	
	 <div id="waitingdivContentId" style="display:none;">
				    
				     <div class="kycloader1">
		
		                   
		                   <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						   </div>
										                   
		                   
		             </div>
		             
		              				    
		    </div>
		    
			<div id="waitingdivId" >
											    
				   
			     
		    </div>
		    
		    
	<div id="waitfade" ></div> 	
	
</body>
</html>
 