<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment Failure</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link rel="stylesheet" href="resources/css/checkout.css" type="text/css" charset="utf-8" />

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<style>


#waitfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 5px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

</style>

<script>

function backToMigratePlan()
{
	window.location = "individualgetmigrateplan.html";
}


</script>


</head>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="java.util.*,java.io.*"%><%@ page isELIgnored="false"%>
<%@ page import="java.util.*" %>

<body>


	

     
		  <div class="main" style=" height: 517px !important;">
				
				<div style=" background-color: #333;width:100%; height:77px;">
				<div style="   background-color: #333;height: 60px;margin: 0 auto 0 110px;width: 1024px;">
				<div onclick="gotoHomePage()" class="checklogo" style="float:left">
				
				</div>
				
				   <h1>Payment Failure Report</h1>
				  
				  </div>
				</div>
				
				  <p style="width: 100%; color: #ff8080; text-align: center;margin-top:15px;">Due to some internal problem payment transaction has been failed please try again later.</p>
				 
				 
				  <section class="shopping-cart">
				    
				     <ul class="ui-list shopping-cart--list" id="shopping-cart--list">
				
				        <li class="_grid shopping-cart--list-item" style="border: 0px ! important;">
				          				      
				          
				          <div class="_column product-info">
				            
				            <p class="nameCheck">Your transaction has been failed.</p>
				            <h4 class="product-name"></h4>
				            <p class="product-desc"></p>
				            <p class="product-desc"></p>
				            
				          </div>
				          
				          <div class="_column product-modifiers" style="border-left:0px ! important;">				            
				            
				          </div>
				          
				        </li>
				      
				    </ul>
				   </section>
				   
				  
				
				  </section>
				</div>
		    
		      
		      
   
    <div class="columnback">
				      
				              <input class="_btn checkout-btn entypo-forward" type="button" value="Back to Plan" onclick="backToMigratePlan()"/>
				              
				      </div>
								  
<div style="float: right; font-size: 14px; margin-right: 110px;">For any further clarification please drop a mail to <a href="#"><b>payments@clensee.com</b></a></div>
				    



	
	<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
		              
		              			 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									

                      </div>
                  
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" style="margin-top: -10px ! important;" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	

	
</body>
</html>



<%-- <h1>Failure Report:</h1>

<h1>Response=<%=response.toString()%></h1>

<h1>Amount=<%=request.getParameter("amount")%></h1>
<h1>Production info=<%=request.getParameter("productinfo")%></h1>
<h1>TXN ID=<%=request.getParameter("txnid")%></h1>
<h1>Status=<%=request.getParameter("status")%></h1>
 --%>