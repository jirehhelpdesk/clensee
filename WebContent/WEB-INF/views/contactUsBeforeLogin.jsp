 <!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clensee Contact us</title>

<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<link href="resources/css/kycstyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>


<script src="resources/onlineresources/code.jquery.com.jquery-1.9.1.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.10.2.jquery-ui.js"></script>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@page import="java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 

<link rel="stylesheet" href="resources/UtiCss/doctable.css"/>
<link rel="stylesheet" href="resources/css/style_common.css">

<link rel="stylesheet" href="resources/kyc_css/contactUs.css">

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css"/>

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />


<SCRIPT LANGUAGE="javascript">
var width = screen.width
var height = screen.height
document.write("<BODY onUnload=window.resizeTo("+width+ "," +height+")>")
</SCRIPT> 



<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{
background: none repeat scroll 0 0 #fff;
    border: 2px solid #05b7f5;
    border-radius: 5px;
    color: #555;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding:0px;
    position: fixed;
    text-align: justify;
    top: 25%;
    width: 800px;
    z-index: 1002;
}
</style>

<style>
#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 122px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}
</style>


<script>

function fadeoutReport(action)
{
	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{		 
   	         window.location = "contactusbeforeLogin.html";
		}
}


function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
       
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    	    	    	    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    	    
	    	    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	    	 document.getElementById('waitfade').style.display='none';
	    	    
	    	    
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>


<script type="text/javascript">

function searchouterpage()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
    
    
	var id1=$("#searchvaluetext").val();
	
	if(id1.length>=3)
	{	
			$('#hidecenterbody').show();
			$('#searchhomepage').show();
			$('.topdivespan').show();
			
			 $.ajax({  
			     type : "Post",   
			     url : "search_Outerprofile.html", 	  
			     data : "kycid="+id1,	     	     	     
			     success : function(response) 
			     {   	    		    	
			    	 document.getElementById('waitlight').style.display='none';
			    	 document.getElementById('waitfade').style.display='none';
			    	    
			    	 $("#contactDiv").show();   
					 $('#searchhomepage').html(response);
						
				  },  
					    
		    }); 
			 
	 }
	 else 
	 {	
		document.getElementById('waitlight').style.display='none';
		document.getElementById('waitfade').style.display='none';
		$('#searchhomepage').html("");   
		$("#contactDiv").show();
		 
	    $('#hidecenterbody').show();
		$('#searchhomepage').hide();
		$('.topdivespan').hide();
	 }		 
}



function openLogin(id)
{    
	window.location = "individual.html#tabs1-js";	
}



function ValidateComp()
{	
	var flag = 'true';
	
	var name = document.getElementById("nameId").value;	
	var emailId = document.getElementById("emailIdid").value;	
	var mobileNo = document.getElementById("mobileNoId").value;	
	var modeOfContact = document.getElementById("modeOfContactId").value;
	var description = document.getElementById("descriptionId").value;
		
	var alfabet  = /^[a-zA-Z\s]*$/;
	var num = /^[0-9]+$/;
	var emailIdRegex = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;	
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
    
   // $("#waitdisplaySharedDocDiv").html("<img src=\"resources/ajax_loaderimage/registerloading.gif\" style=\"margin-left:121px;height:50px;width:50px;\"></img><h4>Processing please wait for portal response...... </h4>");
    
    
	if(name=="")
		{
		
		document.getElementById('waitlight').style.display='none';
    	document.getElementById('waitfade').style.display='none';
    	
	     document.getElementById('sociallight').style.display='block';
	     document.getElementById('socialfade').style.display='block';
	     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
		 $("#socialdisplaySharedDocDiv").html("Please enter your name . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

		  	   
		    return false;
		}
	
	if(name!="")
		{
			if(!name.match(alfabet))  
	        { 
				document.getElementById('waitlight').style.display='none';
		    	document.getElementById('waitfade').style.display='none';
		    	
				flag = 'false';
				
			    document.getElementById('sociallight').style.display='block';
			    document.getElementById('socialfade').style.display='block';
			    document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				$("#socialdisplaySharedDocDiv").html("Only alphabets are allowed for name . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

				return false;
	        }
		}
	
	if(emailId=="")
		{
			document.getElementById('waitlight').style.display='none';
    		document.getElementById('waitfade').style.display='none';
		   flag = 'false';
		   
		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please enter a email id . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

		   
		   return false;
		}
	
	if(emailId!="")
		{
			if(!emailId.match(emailIdRegex))  
	        { 
				document.getElementById('waitlight').style.display='none';
		    	document.getElementById('waitfade').style.display='none';
				flag = 'false';
				
			     document.getElementById('sociallight').style.display='block';
			     document.getElementById('socialfade').style.display='block';
			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				 $("#socialdisplaySharedDocDiv").html("Please enter a valid email id . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

				
				return false;
	        }
		}
	
	if(mobileNo!="")
		{
			if(!mobileNo.match(num))  
	        { 
				document.getElementById('waitlight').style.display='none';
		    	document.getElementById('waitfade').style.display='none';
				flag = 'false';
				
			     document.getElementById('sociallight').style.display='block';
			     document.getElementById('socialfade').style.display='block';
			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				 $("#socialdisplaySharedDocDiv").html("Please enter a valid mobile number. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
				
				 return false;
	        }
			
			if(mobileNo.length!=10)
			{
				 document.getElementById('waitlight').style.display='none';
		    	 document.getElementById('waitfade').style.display='none';
				 flag = 'false';
				
			     document.getElementById('sociallight').style.display='block';
			     document.getElementById('socialfade').style.display='block';
			     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
				 $("#socialdisplaySharedDocDiv").html("Please enter a valid mobile number. <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  
				
				 return false;
			}
		}
	
	
 	
	if(modeOfContact=="Select Contact Mode")
		{
			document.getElementById('waitlight').style.display='none';
    		document.getElementById('waitfade').style.display='none';
			flag = 'false';

		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please select a contact mode.<div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			return false;
		}
	
	if(description=="")
		{
			document.getElementById('waitlight').style.display='none';
    		document.getElementById('waitfade').style.display='none';
			flag = 'false';

		     document.getElementById('sociallight').style.display='block';
		     document.getElementById('socialfade').style.display='block';
		     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% #ff8080";
			 $("#socialdisplaySharedDocDiv").html("Please give some description . <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\"value=\"OK\" onclick=\"fadeoutReport('warning')\"  /> </div>");  

			return false;
		}
	
	if(flag=='true')
		{
			$.ajax({  
				     type : "Post",   
				     url : "getNotifyOfContactUs.html", 
				     data :$('#contactUsForm').serialize(),	     	     	     
				     success : function(response) 
				     {  
				    	 document.getElementById('waitlight').style.display='none';
					     document.getElementById('waitfade').style.display='none';
					    	
					     document.getElementById('sociallight').style.display='block';
					     document.getElementById('socialfade').style.display='block';
					     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
					     $("#socialdisplaySharedDocDiv").html(""+response+" <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");   
				    	
				     },  				     
			    });  
		}
}

function gotoHomePage()
{
	window.location = "clenseeHome.html";
}

function hideSearchBar()
{
	$(".topdivespan").hide();		
	$('#searchhomepage').hide();		
}

</script>


</head>

<body onclick="hideSearchBar()">


<div id="centerbody">
    <div class="container">
		<div class="top_line"></div>
		
			<header class="header">
			<div class="header_inner">
				<div class="logo" onclick="gotoHomePage()">
				
				</div>
				
			  <div class="search-bar">			 
				 <div id="tfheader">
							 				 
						 <input type="text" id="searchvaluetext" maxlength="50" class="tftextinput2 button-submit" name="q" size="21" maxlength="120" title="Enter atleast 3 character for efficent search !" autofocus="autofocus" placeholder="Search Individual of our website" onkeyup="searchouterpage();"/>
                        
						 <input type='image' class="search_button-submit" alt="Text will be shown if pics are disabled" src='resources/images/home_search_img.png' onclick="searchouterpage()"/>
						  
						        
						        
							<div class="tfclear">
                                
						        </div>
				 </div>     
				</div>
                <div class="tfclear"></div>
					 <div class=" mainmenu">
					 
							 <ul class="main_menu">
                              
                                
								<li><a class="individualactive" href="individual.html"></a></li>
								<!-- <li><a class="corporate" href="corporate.html"></a></li> -->
								<li><a class="utiliser" href="utiliser.html"></a></li>
							   
							 </ul>   
						</div>       
				
				 </div>

			</header>
			
			<%
			ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");					
			String version = resource.getString("kycversion");
			String versionDate = resource.getString("kyclatestDate");
			String contactMode = resource.getString("contactmode");
			String contactModeArray[] = contactMode.split(",");
		%>
			
		<div class="conatctmain">
<div class="main_center">

                 
				<div id="searchhomepage"></div> 	
								
					  
					<!-- Div For KYC Contact Us -->
					
				
			<div id="contactDiv">
			
					<div id="kyccontacemail">
						<h2>
						<span>
						<img width="25" height="26" src="resources/kyc_images/contact_info.png">
						<p>Contact Info</p>
						</span>
						</h2>
						<ul>	
						<li>
						<span>Customer Support</span>
						<p>support@clensee.com</p>
						
						</li>
						<li>
						
						<span>Technical support</span>
						<p>techsupport@clensee.com</p>
						</li>
						
						<li>
					<span>Marketing support</span>
					<p>marketing@clensee.com</p>
						</li>
						</ul>
						
						
					</div>
					
					
						<div class="kyccontactform">
								<h1>
								<b>Contact Form</b>
								<small></small>
								</h1>
								<div id="kyccontactpage">
								
								<form id="contactUsForm" method="post">
										
											 <table>
											 <tr>
											<td width="100" >Name <span>*</span></td>
											<td>:</td>
											<td>
											<input type="text" placeholder="Full Name" id="nameId" maxlength="40" name="name">
											</td>	
											</tr>
											<tr>
											<td width="100" >E - Mail id <span>*</span></td>
											<td>:</td>
											<td>
											<input type="text" placeholder="e - Mail id" id="emailIdid"  maxlength="30"  name="emailId">
											</td>	
											</tr>
												<tr>
											<td width="100" >Mobile No</td>
											<td>:</td>
											<td>
											<input type="text" placeholder="Mobile No" id="mobileNoId" name="mobileNo" maxlength="10">
											</td>	
											</tr>
												<tr>
											<td width="100"  >Mode of Contact</td>
											<td>:</td>
											<td>
											<select id="modeOfContactId" name="modeOfContact">
											         <option value="Select Contact Mode">Select Contact Mode</option>
											         <%for(int i=0;i<contactModeArray.length;i++){ %>
											         
											         <option value="<%=contactModeArray[i]%>"><%=contactModeArray[i]%></option>
											         
											         <%} %>
											        
											</select>
											</td>	
											</tr>
											<tr>
											<td valign="top"  width="100" >Descriptions</td>
											<td valign="top" >:</td>
											<td>
											<textarea id="descriptionId" name="description" placeholder="Descriptions with in 500 character"  maxlength="500" ></textarea>
											</td>	
											</tr>
										</table>
										
								</form>
										
										<div class="hscbuttondiv">
										<ul>
										<li>
										
										   <input id="buttonIdHSC" type="button" onclick="ValidateComp()" value="Send">
										
										</li>
										</ul>
										</div>
									</div>
									
									</div>
				
					 </div>	
					
					 <!-- End of Div for KYC Contact Us --> 				  					  					                   

			 </div>
			  </div>
				
		<!------center End-------->
		<footer class="footer">
		   
		   <div class="footer_center" style="display:none;">
           <div class="footer_topline"></div>
				
				
				<div class="bottom-icons" >    
				 <ul id="bottom-icons-img">
						<li ><a href="#"><img src="resources/images/why_kyc_icon.png" width="61" height="43" /></a>
                        <h2>WHY CLENSEE</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing.</p>
                        </li>
						<li class="bottom-icons-leftline" ><a href="#"><img src="resources/images/kyc_store_icon.png" width="61" height="43" /></a>
                        <h2>CLENSEE STORE</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing.</p>
                        </li>
						<li class="bottom-icons-leftline"><a href="#"><img src="resources/images/kyc_answers_icon.png" width="61" height="43" />  </a>
                        <h2>CLENSEE ANSWERS</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing.</p>
                        </li>
						<li class="bottom-icons-leftline"><a href="#"><img src="resources/images/meet_members_icon.png" width="61" height="43" /></a>
                        <h2>MEET MEMBERS</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing.</p>
                        </li>
					   
					</ul>     
				</div>
				
				
			</div>
				
			<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p style="margin-top:25px;">Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p style="margin-top:35px;">
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactusbeforeLogin.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 905px !important; margin-top: -1px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		   
			<div class="footer_line">
			</div>

		 </footer>

    </div>
    </div>
<!------container End-------->

<!-- kycDiv Div -->
	
	<div id="kycdivlight">
			
			<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpage_links"  src="resources/images/close_button.png" /></div>
		    
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
<!-- EOF kycDiv Div -->	


<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">

                                <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give Us Few Seconds.....
						       </div>
									

                      </div>
                     
		   	   
	</div>
	
	<div id="waitfade" ></div> 	
	
<!-- EOF Waiting Div -->
	
	
	<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->
	
</body>
</html>
 