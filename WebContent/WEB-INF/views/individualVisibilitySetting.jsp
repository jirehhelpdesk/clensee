 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Settings</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>


<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">

<link rel="stylesheet" href="resources/kyc_css/jquery-ui.css" />

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-style.css">
<link rel="stylesheet" href="resources/kyc_css/Accordian-ui.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />

<link rel="stylesheet" href="resources/kyc_css/kycslide.css">
<script src="resources/kyc_scripts/jquery-1.9.1.min.js"></script>

<script src="resources/kyc_scripts/Accordion.js"></script>
<script src="resources/kyc_scripts/kycslide.js"></script>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<link rel="stylesheet" href="resources/css/genderstyle.css">
<link rel="stylesheet" href="resources/css/style_common.css">
 
<script src="resources/js/placeholderjquery.js"></script>
<script src="resources/js/placeholdermodernizr.js"></script>

<script src="resources/onlineresources/code.jquery.com.jquery-1.10.2.js"></script>
<script src="resources/onlineresources/code.jquery.com.ui.1.11.0.jquery-ui.js"></script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.SimpleDateFormat"%>  
<%@ page import="java.util.Date"%>
<%@ page isELIgnored="false" %>
<%@ page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%>

<script src="resources/EmpDatePicker/allEmploymentScript.js" ></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />


<style>

#waitfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{
   background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}
</style>


<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 800px;
    z-index: 1002;
}



</style>

<script>

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

function getPolicy()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
	         document.getElementById('waitfade').style.display='none';

	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	        
	     error : function(e) {  
	           alert('Error: ' + e);
	         }  
	    });
}

</script>





<script type="text/javascript">

function showHideDiv1(id){
	

	myTextBox1 = document.getElementById("b6");
	value=myTextBox1.value;
	
	  var obj = document.getElementById(id);
	    if (obj.style.display=="none"){
	      obj.style.display='block';
	    } else if(obj.style.display=="block"){
	      obj.style.display='none';
	    }
	}

</script>


<script type="text/javascript">

function viewPro1(){
	
	
	var selectedvalue = $("#selectedid").val();
    var id=$("#inputString").val();
	if(id.length>=3)
	{	
		$('#searchicon').show();
	    $('#searchhomepage').show();
	    $('.topprofilestyle').show();
	    
	    document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
        
		 $.ajax({  
		     type : "Post",   
		     url : "indsearch_profile.html", 	  
		     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
		     success : function(response) 
		     {   	    	
		    	 if(response.length>3)
		    		 {
		    		 
		    		    $('#searchicon').show();
						document.getElementById('waitlight').style.display='none';
					    document.getElementById('waitfade').style.display='none';
						$('#searchhomepage').html(response);
						
						 var obj = document.getElementById(searchhomepage);
						 
					    if (obj.style.display=="none"){
					      obj.style.display='block';
					    } else if(obj.style.display=="block"){
					      obj.style.display='none';
					    }
		    		 }
		    	 
		    	 else if(res==2)
		    		 {		    		 
			    		 var values = "No data found";
			    		 $('#searchicon').show();
			    		 document.getElementById('waitlight').style.display='none';
			    	     document.getElementById('waitfade').style.display='none';
			    		 $('#searchhomepage').html(response);
		    		 
		    		 }
		         },  	    
		    }); 
		}
	
	 if(id.length<3)
	    {
			$('#searchhomepage').html("");		 
		 	$('#searchicon').hide();
			document.getElementById('waitlight').style.display='none';
		    document.getElementById('waitfade').style.display='none';
			$('#searchhomepage').hide();		 
			$('.topprofilestyle').hide();
	    }
}


function openSearchedProfileinNewTab(url)
{
     var win = window.open(url, '_blank');
	 win.focus();		
}


function individualSearchProfile(url)
{
	var win = window.open(url, '_blank');
	win.focus(); 	
}
</script>


       
<script src="resources/texteditor/nicEdit.js" type="text/javascript"></script>

<script>

function passwordsetting()
{
   document.getElementById("Hi-B-1").className = "passwordsettingactive";
   document.getElementById("Hi-B-2").className = "";
   document.getElementById("Hi-B-3").className = "";
   
	 $.ajax({
			type : "post",
			url : "setting.html",
			cache : false,
			success : function(response) {		
		
				$("#viewUsers1").html(response);
				
			},
			
		});
	 		 		 		
	$("#titleBar").html("<h2><span>Change Password</span></h2>");
	
}




function myUsageDetails()
{	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';

	document.getElementById("Hi-B-1").className = "";
    document.getElementById("Hi-B-2").className = "";
    document.getElementById("Hi-B-3").className = "usageactive";
    
	 $.ajax({
			type : "post",
			url : "usageDetails.html",
			success : function(response) {		
				
				document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';

				$('#suggestions1').show();
				$("#ProfileHeadingDiv").show();
				$("#center-body-div").show();
				$("#leftsideBasicInfoDiv").show();					
				$('#searchhomepage').hide();
				
				$("#viewUsers1").html(response);								
			},			
		});	 		 		 		
	$("#titleBar").html("<h2><span>Usage Details</span></h2>");
	
}



function showSave()
{
	var obj1 = document.getElementById("saveimg_button_wrapper"); 
	var obj2 = document.getElementById("file_browse_wrapper");
	var obj3 = document.getElementById("removeaddphoto");
	var obj4 = document.getElementById("removeaddphotoDiv");
	var file = document.getElementById("file_browse").value;
	
    if (file!="")
    {
      obj4.style.display="none";	 	
      obj3.style.display="none";	
      obj2.style.display="none";	
      obj1.style.display='block';            
    }
    
}
	
</script>

<script src="resources/js/kyc_side_login.js"></script>

<script type="text/javascript">

function searchedProfileDetails()
{	
    var beforeSearchedIndId = document.getElementById("beforeSearchedIndId").value;
    if(beforeSearchedIndId!='null')
    	{    	  
    	   window.location = "viewSearchedProfile.html?asdgth="+beforeSearchedIndId+"";
    	   document.getElementById("beforeSearchedIndId").value = "";
    	}    		
}

function viewHomeProfile(url) 
{
	window.location = url;	 
}	

function viewOtherProfile(url) 
{
	  var win = window.open(url, '_blank');
	  win.focus();	
}	

function saveVisibility(){ 
	

	var visibility = "";
	var basic_visibility = "";
	var family_visibility = "";
	var social_visibility = "";
		
	var emailPri = document.getElementById("emailPri");
	var mobilePri = document.getElementById("mobilePri");
	
	var emailPub = document.getElementById("emailPub");
	var mobilePub = document.getElementById("mobilePub");
     

	if(emailPri.checked)
	{
		visibility = visibility + "0";
	}
	if(emailPub.checked)
	{
		visibility = visibility + "1";
	}
	
	if(mobilePri.checked)
	{
		visibility = visibility + "0";
	}
	if(mobilePub.checked)
	{
		visibility = visibility + "1";
	}
	
			
	var othermailPri = document.getElementById("othereMailIdPri"); 
	var dobPri = document.getElementById("DOBPri"); 
	var tele_offPri = document.getElementById("tele-OffPri"); 
	var tele_resPri = document.getElementById("tele-ResPri"); 
	var NationPri = document.getElementById("NationalityPri"); 
	var MaritalPri = document.getElementById("maritalStatusPri"); 
	var langPri = document.getElementById("languagePri"); 
	var hobbiesPri = document.getElementById("hobbiesPri"); 
	var preAddPri = document.getElementById("preAddPri"); 
	var perAddPri = document.getElementById("perAddPri"); 
	
	
	var othermailPub = document.getElementById("othereMailIdPub"); 
	var dobPub = document.getElementById("DOBPub"); 
	var tele_offPub = document.getElementById("tele-OffPub"); 
	var tele_resPub = document.getElementById("tele-ResPub"); 
	var NationPub = document.getElementById("NationalityPub"); 
	var MaritalPub = document.getElementById("maritalStatusPub"); 
	var langPub = document.getElementById("languagePub"); 
	var hobbiesPub = document.getElementById("hobbiesPub"); 
	var preAddPub = document.getElementById("preAddPub"); 
	var perAddPub = document.getElementById("perAddPub"); 
	
	
	
	if(othermailPub.checked)
	{		
		basic_visibility = basic_visibility + "1";
	}
	if(othermailPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	if(dobPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}
	if(dobPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	if(tele_offPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}
	if(tele_offPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	if(tele_resPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}
	if(tele_resPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	if(NationPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}	
	if(NationPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	
	if(MaritalPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}
	if(MaritalPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	
	if(langPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}
	if(langPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	
	if(hobbiesPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}
	if(hobbiesPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	if(preAddPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}
	
	if(preAddPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
	
	if(perAddPub.checked)
	{
		basic_visibility = basic_visibility + "1";
	}
	
	if(perAddPri.checked)
	{
		basic_visibility = basic_visibility + "0";
	}
		
	var fnamepri = document.getElementById("fatherNamePri");
	var mnamepri = document.getElementById("motherNamePri");
	var snamepri = document.getElementById("siblingNamePri");
	var spousepri = document.getElementById("spouseNamePri");
	var foodpreferedpri = document.getElementById("foodPreferedPri");
	var horoscopepri = document.getElementById("horoscopePri");
	
	var fnamepub = document.getElementById("fatherNamePub");
	var mnamepub = document.getElementById("motherNamePub");
	var snamepub = document.getElementById("siblingNamePub");
	var spousepub = document.getElementById("spouseNamePub");
	var foodpreferedpub = document.getElementById("foodPreferedPub");
	var horoscopepub = document.getElementById("horoscopePub");
	
	if(fnamepri.checked)
	{
		family_visibility = family_visibility + "0";
	}
	if(fnamepub.checked)
	{
		family_visibility = family_visibility + "1";
	}
	
	if(mnamepri.checked)
	{
		family_visibility = family_visibility + "0";
	}
	if(mnamepub.checked)
	{
		family_visibility = family_visibility + "1";
	}
	
	if(snamepri.checked)
	{
		family_visibility = family_visibility + "0";
	}			
	if(snamepub.checked)
	{
		family_visibility = family_visibility + "1";
	}
	
	if(spousepri.checked)
	{
		family_visibility = family_visibility + "0";
	}			
	if(spousepub.checked)
	{
		family_visibility = family_visibility + "1";
	}
	
	if(foodpreferedpri.checked)
	{
		family_visibility = family_visibility + "0";
	}			
	if(foodpreferedpub.checked)
	{
		family_visibility = family_visibility + "1";
	}
	
	if(horoscopepri.checked)
	{
		family_visibility = family_visibility + "0";
	}			
	if(horoscopepub.checked)
	{
		family_visibility = family_visibility + "1";
	}
	
	var facebookpri = document.getElementById("facebookPri");
	var twitterpri = document.getElementById("twitterPri");
	var linkedinpri = document.getElementById("linkedinPri");
	
	var facebookpub = document.getElementById("facebookPub");
	var twitterpub = document.getElementById("twitterPub");
	var linkedinpub = document.getElementById("linkedinPub");
	
	
	if(facebookpri.checked)
	{
		social_visibility = social_visibility + "0";
	}
	if(facebookpub.checked)
	{
		social_visibility = social_visibility + "1";
	}
	
	if(twitterpri.checked)
	{
		social_visibility = social_visibility + "0";
	}
	if(twitterpub.checked)
	{
		social_visibility = social_visibility + "1";
	}
	
	if(linkedinpri.checked)
	{
		social_visibility = social_visibility + "0";
	}			
	if(linkedinpub.checked)
	{
		social_visibility = social_visibility + "1";
	}
			
	 $.ajax({  
	     type : "Post",   
	     url : "visibilitysetting.html", 
	     data : "visible="+visibility+"&basic_vis="+basic_visibility+"&family_vis="+family_visibility+"&social_vis="+social_visibility,	     	     	     
	     success : function(response) 
	     {  
	    	 $("#visibilityDiv").fadeIn(500); 
	         $("#visibilityDiv").fadeOut(5000);	    														
	     },  
	       
	    });  
}

</script>

<script language="javascript">

function radiofncSubmit()
{

var payment=" ";
var len = document.visibility.kycid.length;
for (var i = 0; i < len; i++) {

	if ( document.visibility.kycid[i].checked ) {

	var payment = document.visibility.kycid[i].value;

	}

	}


var len1 = document.visibility.emialid.length;
for (var i = 0; i < len1; i++) {

	if ( document.visibility.emailid[i].checked ) {
    var payment1 = document.visibility.emailid[i].value;
 	
	}

	}

var len2 = document.visibility.mobileno.length;
for (var i = 0; i < len2; i++) {

	if ( document.visibility.mobileno[i].checked ) {
    var payment2 = document.visibility.mobileno[i].value;
	}

	}
}


</script>

<!-- Style and Script for Setting  -->

<link rel="stylesheet" href="resources/profilepopup/style.css" />
<script type="text/javascript" src="resources/profilepopup/popup.js"></script>


<script type="text/javascript">


/* Main Header Actions  */

function gotoProfile()
{
	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "profiletohome.html";	
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoMyDocument()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "myDocumentsMain.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoSharingManager()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "generateCode.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}

function gotoStore()
{	
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 window.location = "individualstore.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
}

function gotoAlerts()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "myvisitors.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	 
}

function gotoPlans()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
						window.location = "individualviewplan.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 		 
}

function gotoSetings()
{
	 $.ajax({
			type : "post",
			url : "checksessionforIndividual.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = "setting.html";
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 
	
}


function redirectToLoginIndividual()
{
	window.location = "outduesession.html";	
}


function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}

function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}


</script>

</head>

<body onclick="hidesearchDiv()" onload="hideAddvertise()">

<input type="hidden" id="Ind_id" value="<%=session.getAttribute("Ind_id")%>"/>
<input type="hidden" id="beforeSearchedIndId" value="<%=session.getAttribute("beforeSearchedIndId")%>"/>

<% String name = (String)request.getAttribute("individualFirstName");%>

	<div class="container">
	
		<div class="top_line"></div>
		<header class="header">
		<div class="header_inner">
			<a href="profiletohome.html"><span class="logo"></span></a>

			<div class="tfclear"></div>
			<div class="mainmenu">
				<div id="menu_wrapper">
				
				<ul class="main_menu" >
					<li></li>
					<li id="myMenus0"><a class="myprofile"   onclick="gotoProfile()"><br>
							<br> My Profile
					</a>
					</li>	
					<li id="myMenus2"><a class="mydocuments"  onclick="gotoMyDocument()"> <br>
							<br> My Documents
					</a></li>
				
					<li id="myMenus5"><a class="accessmanager" onclick="gotoSharingManager()"> <br>
							<br>Sharing Manager
					</a></li>
					<!-- <li id="myMenus3"><a class="mystore"  onclick="gotoStore()"> <br> <br>
							 Store
					</a></li> -->
					<li id="myMenus1"><a  class="alearticons" onclick="gotoAlerts()"><br>
							<br> Alerts
					</a>
					</li>																				
					<li id="myMenus4"><a class="myplans" onclick="gotoPlans()"><br><br>
							 Plans
					</a></li>
					<li id="settingMenu"><a class="mysettingsfocus" onclick="gotoSetings()">
					 <br> <br>
							Settings
					</a></li>
				</ul>
				
			</div>

		</div>
		</div>
 				<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
           
             <a href="#" id="loginButton" >
                <span></span>
                <em><%=name%></em></a>
                 <div style="clear:both"></div>
            
            
            <!-- Search bar -->
            
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" maxlength="50" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="viewPro1()" />
             </span>
             
            </div>
            
            <!-- End of search Bar -->
            
            <!-- Search Division -->
            
             <span class="topprofilestyle" style="display: none;"></span>
					
						<div id="searchicon" style="display:none;">
							
							<div id="profilesearch">
													
							</div>
																		
							<div id="searchhomepage" style="display:none;">
							
							</div>
							
								
						</div>
						
			<!-- End of Search Division -->	
            
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               
                <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="signOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
		</header>
		
	<div class="main">
				
                       
			<div class="main_center">
			
					
		
					<div class="leftside">
					
					<div id="textBox">
							<div id="button">
							   <ul>
							      <li id="passwordsetting" class="" ><a href="#" id="Hi-B-1"  onclick="gotoSetings()"><span>Change Password</span></a></li>												        
								  <li id="visibility" ><a href="indVisibilitySetting.html" id="Hi-B-2" class="visibilityactive" ><span>Visibility Status</span></a></li>
								  <li id="usage" ><a href="#" id="Hi-B-3" onclick="myUsageDetails()"><span>My Usage</span></a></li>
								</ul>
							</div>     		
					</div>
					
					 </div>
					
					<div class="rightside">
					
					
					     <!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

					<div id="ProfileHeadingDiv" style="display: block;">

						<marquee behavior="scroll" scrollamount="5" direction="left">
							<h1 class="rightsideheader">

								<%=indAdvertise%>
								<!-- <a href="#" onclick="hideAddvertise()"><img src="resources/kyc_images/addmegcld_btn.png" /></a> -->
							</h1>
						</marquee>
					</div>



					<!-- Advertisment Div for Entire Page  -->
					   
					
	          <div id="suggestions1" style="display: block;  margin: 0 auto; width: 100%;">	
		 
					
					<div class="border_line"></div>
				
						
						 <div id="titleBar">
							<h2>
								    <span>Visibility Settings</span>								   
							</h2>
						 </div>
					
			<div class="module-body">
				
				<div id="viewUsers1" style=" height: auto !important;margin-bottom: 15px;">
					
					    <!-- Start of Required Division for viewUser1 not go beyond  -->	
					    
							<div id="visibility">
									 
									 
									  
									  <div id="visibilityDiv" style="width:600px;border:1px solid #222;margin-left:100px;height:25px;background-color:yellowgreen;color:#fff;display:none;font-size:16px;">Visibility setting has been changed successfully.</div>
									     
									  
									   <form id="visibility"  method="post" >
									   
									   <%String visibility = (String)session.getAttribute("visibility"); 
									   
									   String reg_visibility = (String)request.getAttribute("reg_Visibility");
									   String basic_visibility = (String)request.getAttribute("basic_visibility");
									   String family_visibility = (String)request.getAttribute("family_visibility");
									   String social_visibility = (String)request.getAttribute("social_visibility");
									   
									   %>
									   
									  <table  align="left">
									  
									<tr style="height: 28px;"><th><h1>Registration Details:</h1></th></tr>  
									
				
									
									<tr style="height: 28px;">
									<th style=" text-align:left;">Email-id</th><td>:</td>
									
									<%if(reg_visibility.charAt(0)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="emailPri" TYPE="radio" NAME="emailid" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="emailPub" TYPE="radio" NAME="emailid" checked></td>
									   <%} %>
									   
									   <%if(reg_visibility.charAt(0)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="emailPri" TYPE="radio" NAME="emailid" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="emailPub" TYPE="radio" NAME="emailid" ></td>
									   <%} %>
									   
									</tr>
									
									<tr style="height: 28px;">
									<th style=" text-align:left;">Contact Number</th><td>:</td>
									
									 <%if(reg_visibility.charAt(1)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="mobilePri" TYPE="radio" NAME="mobileno" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="mobilePub" TYPE="radio" NAME="mobileno" checked></td>
									   <%} %>
									   
									   <%if(reg_visibility.charAt(1)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="mobilePri" TYPE="radio" NAME="mobileno" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="mobilePub" TYPE="radio" NAME="mobileno" ></td>
									   <%} %>
									</tr>
									</table>
									<table align="right">
									<tr style="height: 30px;"><th><h1>Basic Details:</h1></th></tr>       
									
									
									<tr style="height: 43px;" >
									<th style=" text-align:left;">Alternate Email Id</th><td>:</td>
									
									 <%if(basic_visibility.charAt(0)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="othereMailIdPri" TYPE="radio" NAME="othereMailId"></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="othereMailIdPub" TYPE="radio" NAME="othereMailId"  checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(0)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="othereMailIdPri" TYPE="radio" NAME="othereMailId" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="othereMailIdPub" TYPE="radio" NAME="othereMailId" ></td>
									   <%} %>
									</tr>
									
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Date of Birth</th><td>:</td>
									
									 <%if(basic_visibility.charAt(1)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="DOBPri" TYPE="radio" NAME="DOB" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="DOBPub" TYPE="radio" NAME="DOB" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(1)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="DOBPri" TYPE="radio" NAME="DOB" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="DOBPub" TYPE="radio" NAME="DOB" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Telephone-Office</th><td>:</td>
									
									 <%if(basic_visibility.charAt(2)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="tele-OffPri" TYPE="radio" NAME="tele-Off" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="tele-OffPub" TYPE="radio" NAME="tele-Off" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(2)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="tele-OffPri" TYPE="radio" NAME="tele-Off" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="tele-OffPub" TYPE="radio" NAME="tele-Off" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Telephone-Residence</th><td>:</td>
									
									 <%if(basic_visibility.charAt(3)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="tele-ResPri" TYPE="radio" NAME="tele-Res" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="tele-ResPub" TYPE="radio" NAME="tele-Res" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(3)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="tele-ResPri" TYPE="radio" NAME="tele-Res" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="tele-ResPub" TYPE="radio" NAME="tele-Res" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Nationality</th><td>:</td>
									
									 <%if(basic_visibility.charAt(4)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="NationalityPri" TYPE="radio" NAME="Nationality" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="NationalityPub" TYPE="radio" NAME="Nationality" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(4)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="NationalityPri" TYPE="radio" NAME="Nationality" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="NationalityPub" TYPE="radio" NAME="Nationality" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Marital Status</th><td>:</td>
									
									 <%if(basic_visibility.charAt(5)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="maritalStatusPri" TYPE="radio" NAME="maritalStatus" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="maritalStatusPub" TYPE="radio" NAME="maritalStatus" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(5)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="maritalStatusPri" TYPE="radio" NAME="maritalStatus" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="maritalStatusPub" TYPE="radio" NAME="maritalStatus" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Languages</th><td>:</td>
									
									 <%if(basic_visibility.charAt(6)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="languagePri" TYPE="radio" NAME="language" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="languagePub" TYPE="radio" NAME="language" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(6)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="languagePri" TYPE="radio" NAME="language" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="languagePub" TYPE="radio" NAME="language" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Hobbies</th><td>:</td>
									
									 <%if(basic_visibility.charAt(7)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="hobbiesPri" TYPE="radio" NAME="hobbies" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="hobbiesPub" TYPE="radio" NAME="hobbies" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(7)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="hobbiesPri" TYPE="radio" NAME="hobbies" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="hobbiesPub" TYPE="radio" NAME="hobbies" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Present Address</th><td>:</td>
									
									 <%if(basic_visibility.charAt(8)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="preAddPri" TYPE="radio" NAME="preAdd" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="preAddPub" TYPE="radio" NAME="preAdd" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(8)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="preAddPri" TYPE="radio" NAME="preAdd" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="preAddPub" TYPE="radio" NAME="preAdd" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 43px;">
									<th style=" text-align:left;">Permanent Address</th><td>:</td>
									
									 <%if(basic_visibility.charAt(9)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="perAddPri" TYPE="radio" NAME="perAdd" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="perAddPub" TYPE="radio" NAME="perAdd" checked></td>
									   <%} %>
									   
									   <%if(basic_visibility.charAt(9)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="perAddPri" TYPE="radio" NAME="perAdd" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="perAddPub" TYPE="radio" NAME="perAdd" ></td>
									   <%} %>
									</tr>
									</table>
									<table align="left">
									
									<tr style="height: 30px;"><th><h1>Family Details:</h1></th></tr>  
									
									<tr style="height: 30px;">
									<th style=" text-align:left;">Father Name</th><td>:</td>
									
									 <%if(family_visibility.charAt(0)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="fatherNamePri" TYPE="radio" NAME="fatherName" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="fatherNamePub" TYPE="radio" NAME="fatherName" checked></td>
									   <%} %>
									   
									   <%if(family_visibility.charAt(0)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="fatherNamePri" TYPE="radio" NAME="fatherName" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="fatherNamePub" TYPE="radio" NAME="fatherName" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 30px;">
									<th style=" text-align:left;">Mother Name</th><td>:</td>
									
									 <%if(family_visibility.charAt(1)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="motherNamePri" TYPE="radio" NAME="motherName" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="motherNamePub" TYPE="radio" NAME="motherName" checked></td>
									   <%} %>
									   
									   <%if(family_visibility.charAt(1)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="motherNamePri" TYPE="radio" NAME="motherName" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="motherNamePub" TYPE="radio" NAME="motherName" ></td>
									   <%} %>
									</tr>
									
									
									<tr style="height: 30px;">
									<th style=" text-align:left;">Sibling Name</th><td>:</td>
									
									 <%if(family_visibility.charAt(2)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="siblingNamePri" TYPE="radio" NAME="siblingName" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="siblingNamePub" TYPE="radio" NAME="siblingName" checked></td>
									   <%} %>
									   
									   <%if(family_visibility.charAt(2)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="siblingNamePri" TYPE="radio" NAME="siblingName" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="siblingNamePub" TYPE="radio" NAME="siblingName" ></td>
									   <%} %>
									</tr>
									
									
									<tr style="height: 30px;">
										<th style=" text-align:left;">Spouse Name</th><td>:</td>
										
										 <%if(family_visibility.charAt(3)=='1') 
										    {%>
										<td style=" text-align:left;">Private<INPUT style="width:25px;" id="spouseNamePri" TYPE="radio" NAME="spouseName" ></td>
										<td style=" text-align:left;">Public<INPUT style="width:25px;" id="spouseNamePub" TYPE="radio" NAME="spouseName" checked></td>
										   <%} %>
										   
										   <%if(family_visibility.charAt(3)=='0') 
										    {%>
										<td style=" text-align:left;">Private<INPUT style="width:25px;" id="spouseNamePri" TYPE="radio" NAME="spouseName" checked></td>
										<td style=" text-align:left;">Public<INPUT style="width:25px;" id="spouseNamePub" TYPE="radio" NAME="spouseName" ></td>
										   <%} %>
										</tr>
										
										
										<tr style="height: 30px;">
										<th style=" text-align:left;">Food Prefered</th><td>:</td>
										
										 <%if(family_visibility.charAt(4)=='1') 
										    {%>
										<td style=" text-align:left;">Private<INPUT style="width:25px;" id="foodPreferedPri" TYPE="radio" NAME="foodPrefered" ></td>
										<td style=" text-align:left;">Public<INPUT style="width:25px;" id="foodPreferedPub" TYPE="radio" NAME="foodPrefered" checked></td>
										   <%} %>
										   
										   <%if(family_visibility.charAt(4)=='0') 
										    {%>
										<td style=" text-align:left;">Private<INPUT style="width:25px;" id="foodPreferedPri" TYPE="radio" NAME="foodPrefered" checked></td>
										<td style=" text-align:left;">Public<INPUT style="width:25px;" id="foodPreferedPub" TYPE="radio" NAME="foodPrefered" ></td>
										   <%} %>
										</tr>
										
										
										<tr style="height: 30px;">
										<th style=" text-align:left;">Horoscope</th><td>:</td>
										
										 <%if(family_visibility.charAt(5)=='1') 
										    {%>
										<td style=" text-align:left;">Private<INPUT style="width:25px;" id="horoscopePri" TYPE="radio" NAME="horoscope" ></td>
										<td style=" text-align:left;">Public<INPUT style="width:25px;" id="horoscopePub" TYPE="radio" NAME="horoscope" checked></td>
										   <%} %>
										   
										   <%if(family_visibility.charAt(5)=='0') 
										    {%>
										<td style=" text-align:left;">Private<INPUT style="width:25px;" id="horoscopePri" TYPE="radio" NAME="horoscope" checked></td>
										<td style=" text-align:left;">Public<INPUT style="width:25px;" id="horoscopePub" TYPE="radio" NAME="horoscope" ></td>
										   <%} %>
										</tr>
									
									</table>
									<table align="left">
									<tr style="height: 28px;"><th><h1>Social Account Details:</h1></th></tr>  
									
									
									<tr style="height: 28px;">
									<th style=" text-align:left;">Facebook</th><td>:</td>
									
									 <%if(social_visibility.charAt(0)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="facebookPri" TYPE="radio" NAME="facebook" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="facebookPub" TYPE="radio" NAME="facebook" checked></td>
									   <%} %>
									   
									   <%if(social_visibility.charAt(0)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="facebookPri" TYPE="radio" NAME="facebook" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="facebookPub" TYPE="radio" NAME="facebook" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 28px;">
									<th style=" text-align:left;">Twitter</th><td>:</td>
									
									 <%if(social_visibility.charAt(1)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="twitterPri" TYPE="radio" NAME="twitter" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="twitterPub" TYPE="radio" NAME="twitter" checked></td>
									   <%} %>
									   
									   <%if(social_visibility.charAt(1)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="twitterPri" TYPE="radio" NAME="twitter" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="twitterPub" TYPE="radio" NAME="twitter" ></td>
									   <%} %>
									</tr>
									
									<tr style="height: 28px;">
									<th style=" text-align:left;">LinkedIn</th><td>:</td>
									
									 <%if(social_visibility.charAt(2)=='1') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="linkedinPri" TYPE="radio" NAME="linkedin" ></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="linkedinPub" TYPE="radio" NAME="linkedin" checked></td>
									   <%} %>
									   
									   <%if(social_visibility.charAt(2)=='0') 
									    {%>
									<td style=" text-align:left;">Private<INPUT style="width:25px;" id="linkedinPri" TYPE="radio" NAME="linkedin" checked></td>
									<td style=" text-align:left;">Public<INPUT style="width:25px;" id="linkedinPub" TYPE="radio" NAME="linkedin" ></td>
									   <%} %>
									</tr>
									
									
									</table>
									  </form>
									    
						</div>
									   
									<div style=" clear: both;
									    display: inline-table;
									    height: 25px;
									    position: relative;
									    width: 100%;" id="visibility">  <input style="width:65px;" class="saveinner_button" type="button" name="Submit" value="Save" onclick="saveVisibility()"></div>
									 
										
					<div style="margin-left:45px;text-align:left;">
					
					    <p>
					        <ul>
						        <li style="margin-top:20px;">
							        <h1 style="font-size: 18px;">How does visibility settings work?</h1><br><br><br>
							        
							        <span style="color: #7c7c7c;font-size: 12px;">It is Individuals responsibility to ensure they set proper visibility options.</span>
						        </li>
						        
						        <li style="margin-top:20px;">
									
									<h1 style="font-size: 14px;">Public</h1><br><br><br>
									
									<span style="color: #7c7c7c;font-size: 12px;">
									By default all settings are set to public.
									Any thing that is set to public will be visible to every Individual and Utilizer when they search you.
								    </span>
								    
								</li>
								
								<li style="margin-top:20px;">
									
									<h1 style="font-size: 14px;">Private</h1><br><br><br>
									
									<span style="color: #7c7c7c;font-size: 12px;">
									Any thing that is set to private will not be visible to any one by any chance.
									But Information that is shared via "Utilizer Sharing" option will be visible for whom you have shared
									irrespective of your visibility settings.
									</span>
									
								</li>
							</ul>
					   
					   </p>
					
					</div>
								
						<!-- End of Required Division for viewUser1 not go beyond  -->	
									
			    </div>
			 
			</div>	
			
	    </div>	<!-- End of -->	

  
	
	
	
	
	
	
	    
</div> <!--end of right div  -->

	</div>

</div>
					
					
					
					
					
					
		<%
		ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
		String version=resource.getString("kycversion");
		String versionDate=resource.getString("kyclatestDate");
		%>			
					
					
					
			
	
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="contactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	<div class="footer_line"></div>
	
	</div>

<!-- Waiting Div -->
	
	<div id="waitlight">
			
		 
		              <div class="kycloader1">
								
								 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give us few seconds.....
						       </div>
									
								
                      </div>
                     
	</div>
	
	<div id="waitfade" ></div> 	
	
	
<!-- EOF Waiting Div -->
	
	
	
<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
   <!-- EOF session Div -->

	
	
	
	
</body>
</html>
     	