<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<link rel="stylesheet" href="resources/UtiCss/doctable.css">
<script src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<script type="text/javascript">

window.document.onkeydown = function (e)
{
    if (!e)
    {
        e = event;
    }
    if (e.keyCode == 27)
    {
    	lightbox_closePopUp();
    }
}

function lightbox_open(){
    window.scrollTo(0,0);
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block'; 
}

function lightbox_closePopUp(){
	
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none';
    
}

</script>	

<style type="text/css">

#fade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#light{
    background: none repeat scroll 0 0 #fff;
    border: 6px solid #ccc;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -321px;
    margin-top: -297px;
    overflow: visible;
    padding: 10px;
    position: absolute;
    top: 50%;
    width: 409px;
    z-index: 1002;
}


</style>




<script type="text/javascript">
function viewDocUti(docname,kycid)
{
	$('#displaySharedDocDiv').empty();
	$('#light').show();
	$('#fade').show();
	
	  $.ajax({
	    	
	    	type: "post",	   
	    	url: "viewDocforUti.html",	    	
	    	data: "docname="+docname+ "&kycid="+kycid,	 	    		       
	        success: function (response) {
	        	
	        	$('#displaySharedDocDiv').html(response);
	        	
	        },
	        error : function() {
				alert('Error while fetching response');
			}	        
	    });
	}
	
function downloadDocUti(docname,kycid)
{
	$.ajax({
    	
    	type: "post",	   
    	url: "downloadDocforUti.html",	    	
    	data: "docname="+docname+ "&kycid="+kycid,	 	    		       
        success: function (response) {
        	
        	$('#viewUsers').html(response);			
        },
        error : function() {
			alert('Error while fetching response');
		}	        
    });
	
	}
	
	
function HistoryDocUti(docname,kycid)
{
	
	$.ajax({
    	
    	type: "post",	   
    	url: "historyDocforUti.html",	    	
    	data: "docname="+docname+"&kycid="+kycid,	 	    		       
        success: function (response) {       	
        	$('#viewUsers').html(response);			
        },
        error : function() {
			alert('Error while fetching response');
		}	        
    });
	
	}
	
function accessIndDocumentDetail()
{	
	$("#accessIndProfileDetailId").hide();
	$("#accessIndDocumentDetailId").show();		
}

function accessIndProfileDetail()
{
	$("#accessIndDocumentDetailId").hide();
    $("#accessIndProfileDetailId").show();
}
	
function openSearchedProfileinNewTab(url)
{		
     var win = window.open(url, '_blank');
	 win.focus();		
}


</script>



</head>
<body>
<%

	String acc_pattern = (String)request.getAttribute("accessPattern");
    String kycid = (String)request.getAttribute("kycid");
    System.out.println(acc_pattern);
		
%>

<div id="revoke_tbutton" style="width:100%;margin-top:18px;margin-left:1px;">

	<input type="button" value="Access Document Detail" onClick='accessIndDocumentDetail()'/>
	<input type="button" value="Access Profile Detail" onClick='accessIndProfileDetail()'/>

</div>

<br><br>

<div  id="accessIndDocumentDetailId" style="display:none;margin-left:10px;">

		<%  
		    String documentNamelist = "";
			String documentArray[] = acc_pattern.split(",");
			for(int v=0;v<documentArray.length;v++)
			{	
				if(!documentArray[v].equals("No Document"))
				    {						
					   documentNamelist += documentArray[v] +"&";
				    }
			}						
		%>	
    
     <div class="showcodepage">
        <ul>
            <li>
            <table>
            <tr>
            <td>
	            <img src="resources/kyc_images/kyczipicon.jpg" height="50px" width="50px" />
	            </td>
	            <td>
	            	            
		        <form name="downloadAllFormName" action="downlaodalldocumentsinzip.html" method="post">
		        
		            <input type="hidden" value="accessDocumentinZip" name="codeName" />
		            <input type="hidden" name="docnames" value="<%=documentNamelist%>" />
		            <input type="hidden" name="kycId" value="<%=kycid%>" /> 
		            <input type="submit" value="Click here to download all access given Document in zip." />
		            
		        </form>
		        </td>
		        </tr>	
		        </table>
	        </li>
	    </ul>
   </div>
	
	<div class="datagrid">
              
<table class='CSSTableGenerator' align="left" border="1" cellspacing="1" align="center">
		<tr>
		    <th height="10">Serial No</th>			
			<th height="10">DOC Name</th>
			<th height="10">View</th>
			<th height="10">Download</th>
			<th height="10">History</th>
		</tr>
        
		<%int k = 0;
		String doc1[] = acc_pattern.split(",");
		for(int v=0;v<doc1.length;v++){	
		if(!doc1[v].equals("No Document")){
			
		%>			 
		
			 <tr>
			   		
				<td width="270"><%=++k%></td>
				<td width="270"><%=doc1[v].split("_")[0]%></td>
				<td width="270"><a href="#" onclick="viewDocUti('<%=doc1[v]%>','<%=kycid%>')">View</a></td>
				<td width="270"><a href="downloadDocforUti.html?docname=<%=doc1[v]%>&kycid=<%=kycid%>">Download</a></td>
				<td width="270"><a href="#" onclick="HistoryDocUti('<%=doc1[v]%>','<%=kycid%>')">History</a></td>			
				
			 </tr>	
			 
			 <%} 
		
		}%>		
		
</table>
</div>
</div>





<div class="datagrid" id="accessIndProfileDetailId" style="display:none;">
            
            <% 
            
             int individualId = (Integer)request.getAttribute("indIndId");
			 String indRegYear = (String)request.getAttribute("year");
			 String profilePhoto = (String)request.getAttribute("profilePhoto");
      
			 System.out.println(individualId+"/"+indRegYear+"/"+profilePhoto);
			 %>
       <div>
   
       <ul>
       		
       			<li>
			       			<div  style="float:left;margin-left:41px;margin-top:15px;">
			       			
			       			<%if(!profilePhoto.equals("no image")) {%>
		       					
			       					<img src="${pageContext.request.contextPath}<%="/previewprofileOther.html?fileName="+profilePhoto+"&docCategory=Profile_Photo&year="+indRegYear+"&indId="+individualId+""%>" width="90px" height="95px" ></img>
			       					
			       			<%}else{ %>
			       					
			       					<img src="resources/kyc_images/Koala.jpg" width="90px" height="95px" ></img>
			       			
			       			<%} %>
			       			
			       			</div>
		       					
		       					
       					<div  style="float:left;margin-left:117px;margin-top:-85px;">
       					      
       					        <%String profileAccessData = (String)request.getAttribute("ProfileAccessData"); %>
      
							      <%if(!profileAccessData.equals("No Data")) {%>
							           
							           <div style="text-align:left; width:100%;">
							           
							                <ul>
								             <%String profileAccessDataArrat[] = profileAccessData.split("##");%>
								             <%for(int p=0;p<2;p++){ %> <!-- profileAccessDataArrat.length -->
								                     
								                        
								                    <li style="clear: both;padding: 5px;position: relative;text-align: left;width: 800px;">
								                    <h1><%=profileAccessDataArrat[p]%></h1>
								                    </li>      
								                      
								            <%} %>
							                </ul>
							                
							           </div>
							           
							      <%}else{%>
							          
							              <h1>Profile Access Not Given !</h1>
							      <%}%>
       					</div>
       			
       					<div  style="float:left;margin-left:536px;margin-top:-66px;"><input type="button" value="For More Details" onclick="openSearchedProfileinNewTab('validateprofileviewingForutilizer.html?asdgth=<%=individualId%>')" /></div>
       					
       			</li>
       			      
       </ul>
   
   </div>
</div>




	
<br><br><br>

<div id="ViewImageID" width="90" height="90"></div>

<div id="light">
<div class="colsebutton" onclick="lightbox_closePopUp()"><img height="22" width="22" src="resources/images/close_button.png"></div>
		   <div id="displaySharedDocDiv"> 
		   
		           
		   </div>
		   
	</div>

	<div id="fade" onClick="lightbox_closePopUp();"></div> 
</body>
</html>