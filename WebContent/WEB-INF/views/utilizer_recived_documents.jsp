<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Received Documents</title>
<link rel="icon" href="resources/kyc_images/favicon.ico" type="images/x-icon"/>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page isELIgnored="false" %>
<%@page import="org.apache.commons.fileupload.*,java.util.*,java.io.*"%><%@ page isELIgnored="false"%> 
<%@ page import="java.text.SimpleDateFormat"%>  

<link rel="stylesheet" href="resources/css/kycstyle_profile.css">

<link rel="stylesheet" href="resources/kyc_css/BorderDiv.css">

<link rel="stylesheet" href="resources/kyc_css/tableAdmin.css" type="text/css" />
<link href="resources/css/admin_kycstyle_profile.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/kyc_css/AdminLeftMenu.css">
<link rel="stylesheet" href="resources/UtiCss/doctable.css">

<link rel="stylesheet" href="resources/css/header_menu_font/headermenufont-style.css" type="text/css" charset="utf-8" />


<script type="text/javascript" src="resources/kyc_scripts/jquery-1.11.1.js"></script>

<link rel="stylesheet" href="resources/ajax_loaderimage/kycloadercss.css" />

<link rel="stylesheet" href="resources/ajax_loaderimage/errorsuccessalerts.css" />


<style>

#kycDocPopUpfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycDocPopUplight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: auto;
    left: 50%;
    margin-left: -272px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: auto;
    z-index: 1002;
}

</style>
<style>


#waitfade
{
    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#waitlight{

    background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    display: none;
    height: 115px;
    left: 50%;
    margin-left: -173px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 50%;
    width: 329px;
    z-index: 1002;
}

#sessionfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#sessionlight
{
    background: none repeat scroll 0 0 #fff;
    border: 2px solid #00b6f5;
    display: none;
    height: 46px;
    left: 50%;
     border-radius: 5px;
    margin-left: -272px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
      position: fixed;
    top: 50%; 
    width: 564px;
    z-index: 1002;
     right: -10px;  
}

</style>

<style>
#kycdivfade{

    display: none;
    position: fixed;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #000;
    z-index:1001;
    -moz-opacity: 0.7;
    opacity:.70;
    filter: alpha(opacity=70);
}

#kycdivlight{

     background: none repeat scroll 0 0 #fff;
    border-radius: 24px;
    height: auto;
    left: 50%;
    margin-left: -363px;
    margin-top: -108px;
    overflow: visible;
    padding: 10px;
    position: fixed;
    top: 33%;
    width: 800px;
    z-index: 1002;
}
</style>

<script>

function fadeoutReport(action)
{	
	document.getElementById('sociallight').style.display='none';
	document.getElementById('socialfade').style.display='none';
	$("#socialdisplaySharedDocDiv").html("");
	
	if(action=="success")
		{
		checkSession('reciveddocuments.html');
		}
}

function fadeout()
{
	document.getElementById('kycdivlight').style.display='none';
	document.getElementById('kycdivfade').style.display='none';
	$("#kycdivdisplaySharedDocDiv").html("");
}

function getkycAboutUs()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCAboutUs.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		       
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	   
	    });
	
}

function getTermsConditions()
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCTermConditions.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		       
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     }, 
	});
					    					   
}

function getPolicy()
{	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({  
	     type : "Post",   
	     url : "getKYCPrivatePolicy.html", 	     
	     success : function(response) 
	     {  
	    	 document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		       
	    	 document.getElementById('kycdivlight').style.display='block';
	    	 document.getElementById('kycdivfade').style.display='block';
	    	 $("#kycdivdisplaySharedDocDiv").html(response);
	    	    	    														
	     },  
	    
	    });									
}


function backtoMainAccessHistory(docName,kycId)
{	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({
    	
    	type: "post",	   
    	url: "historyDocforUti.html",	    	
    	data: "docname="+docName+ "&kycid="+kycId,	 	    		       
        success: function (response) {
        	
        	document.getElementById('waitlight').style.display='none';
	        document.getElementById('waitfade').style.display='none';
	       
        	$('#viewUsers').html(response);
        	
        },
                
    });	
}


function viewDocumentInPopUpfromHistory(fileName)
{
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({
		type : "Post",				
		url : "viewDocumentsFromUtilizerInPopUp.html",
		data: "docName="+fileName,
		success : function(response) {
		
			document.getElementById('waitlight').style.display='none';
	        document.getElementById('waitfade').style.display='none';
	       
			document.getElementById('kycDocPopUplight').style.display='block';
	        document.getElementById('kycDocPopUpfade').style.display='block';
	        
	        $("#kycDocPopUpDisplay").html(response);
	        			
		},		
	});		 		 		 		

}

function fadeoutPopUp()
{
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");
}

function lightbox_close()
{		
	document.getElementById('kycDocPopUplight').style.display='none';
	document.getElementById('kycDocPopUpfade').style.display='none';
	$("#kycDocPopUpDisplay").html("");	
}



function openbrowse()
 { 	 
	var obj = document.getElementById('openbrowse');
    if (obj.style.display=="none")
    {
        obj.style.display='block';	
    }	  	   	  
 }
 
 </script>

<script type="text/javascript">
			function resolveSrcMouseover(e)
			{				
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				if (node.nodeName != "UL") {
					node.style.fontWeight= "bold";
					showRollover(e, node.innerHTML);
				}
			}
			
			function resolveSrcMouseout(e) 
			{
				var node = e.srcElement == undefined ? e.target : e.srcElement;
				node.style.fontWeight = "normal";
				clearRollover(e);
			}
			
			function takeAction(e) 
			{
				var node = e.srcElement == undefined ? e.target : e.srcElement;
									
				document.getElementById("DisplayInfo").innerHTML = "Clicked " + node.innerHTML;
				
				var id = node.getAttribute("id"); 
				if (id != null && id.indexOf("F") > -1) {
					if (node.innerHTML == "-") {
						node.innerHTML = "+";
						document.getElementById("EC" + id).style.display = "none";
					} else if (node.innerHTML == "+") {
						node.innerHTML = "-";
						document.getElementById("EC" + id).style.display = "block";
					}
				}
			}
</script>


<script type="text/javascript">
	

function acceptDoc(id,date) {
	
	document.getElementById('waitlight').style.display='block';
    document.getElementById('waitfade').style.display='block';
   
	$.ajax({
		type : "Post",
		url : "validateDocViewingViaAllowAccessPerMonth.html",		
		success : function(response) {
				
			if(response=="withinLimit")
				
				{				
					$.ajax({
						type : "Post",
						url : "acceptDocs.html",
						data: "id=" +id+ "&date=" +date,
						success : function(response) {
				                 
							document.getElementById('waitlight').style.display='none';
					        document.getElementById('waitfade').style.display='none';
						       
						         document.getElementById('sociallight').style.display='block';
							     document.getElementById('socialfade').style.display='block';
							     document.getElementById('socialdisplaySharedDocDiv').style.background= "none repeat scroll 10px 50% yellowgreen";
								 $("#socialdisplaySharedDocDiv").html(""+response+" <div style=\"width:97%;margin 0 auto;\"> <input type=\"button\" class=\"button_style\" value=\"OK\" onclick=\"fadeoutReport('success')\"  /> </div>");  
						   	    
							       
							      							 							
							},		
						});		
				}
			else
				{
					alert("As per your plan you can't accept/moved these Document,To Accept Migrate your Plan !");			
				}
				
		},
	});		
	
    }
		

	function awaitedDocument() {
		
		document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		
		$.ajax({
				type : "Post",
				url : "awaitedDocs.html",				
				success : function(response) {
                    
					document.getElementById('waitlight').style.display='none';
			        document.getElementById('waitfade').style.display='none';
			       
			        
					var viewedId = document.getElementById("documentsactive");  
					var awaitedId = document.getElementById("awaitedId");  
					
					viewedId.id = "viewedId";  //set the id
					awaitedId.id = "employee_documentactive";  //set the id
					
					$('#suggestions1').show();		
					$("#utileftsideId").show();
				    $('#searchhomepage').hide();
					
					    $('#viewUsers').html(response);										
			            $("#titleBar").html("<h2><span>List of Awaited Documents</span></h2>");
					
					},
				error : function(e) {
					
					alert('Error: ' + e);
					
				}
				});				
	}

	/* Script for view the documents */
	
	function viewDetailsDoc(kycid,time,index)
	{	
		document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		$.ajax({
			type : "Post",
			url : "viewDetailsDocs.html",
			data: "kycid="+kycid+"&date="+time,
			success : function(response) {
                
				document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		       
		        
				    $('#viewUsers').html(response);
				
				},		
			});	
		
	}
	
		
	function downloadDoc(kycid,time)
	{
		document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		$.ajax({
			type : "Post",
			url : "downloadAllDocs.html",
			data: "kycid="+kycid+"&date="+time,
			success : function(response) {
				
				document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		       	
				alert(response);				
				},			
			});	
	}
	
	//Script for Profile
	
	function viewUsers()
	{
		document.getElementById('waitlight').style.display='block';
        document.getElementById('waitfade').style.display='block';
       
		$.ajax({
			type : "Post",
			url : "viewUtiProfile.html",
			success : function(response) {
                
				document.getElementById('waitlight').style.display='none';
		        document.getElementById('waitfade').style.display='none';
		       
					$('#viewUsers').html(response);
					$("#titleBar").html("<h2><span>View Profile</span></h2>");
				},
			error : function(e) 
			   {				
				  alert('Error: ' + e);				
			   }				
		  });	
	}
	
	
	function userHome()
	{
		window.open("UtiHome.html",true);			     			
	}
	
	function gotoHome()
	{
	    window.location = "utimyProfile.html";	
	}
	
	
function searchIndividual(){
		
		
		var selectedvalue = $("#selectedid").val();
	    var id=$("#inputString").val();
		if(id.length>=3)
		{			
			$("#searchicon").show();
		    $('#searchhomepage').show();
		    $('.topprofilestyle').show();
		    
		    document.getElementById('waitlight').style.display='block';
	        document.getElementById('waitfade').style.display='block';
	       
			 $.ajax({  
			     type : "Post",   
			     url : "indsearchprofilefromUtilizer.html", 	  
			     data : "kycid="+id+ "&selectedvalue="+selectedvalue,	     	     	     
			     success : function(response) 
			     {   	    	
			    	 if(response.length>3)
			    		 {
			    		 													
							document.getElementById('waitlight').style.display='none';
						    document.getElementById('waitfade').style.display='none';
							$('#searchhomepage').html(response);
							
							 var obj = document.getElementById(searchhomepage);
							 
						    if (obj.style.display=="none"){
						      obj.style.display='block';
						    } else if(obj.style.display=="block"){
						      obj.style.display='none';
						    }
			    		 }
			    	 
			    	 else if(res==2)
			    		 {
			    		 
				    		 var values = "No data found";			    		
				    		 $("#searchicon").show();
				    		 document.getElementById('waitlight').style.display='none';
				    	     document.getElementById('waitfade').style.display='none';
				    		 $('#searchhomepage').html(response);
			    		 
			    		 }
			         },  	    
			    }); 
			}
		
		 if(id.length<3)
		    {
			    $('#searchhomepage').html("");				
				document.getElementById('waitlight').style.display='none';
			    document.getElementById('waitfade').style.display='none';
				$('#searchhomepage').hide();
				$("#searchicon").hide();
				$('.topprofilestyle').hide();
		    }
	}

function searchAccessDetails()
{
	
   var searchParameter = document.getElementById("accessSearchId").value;	
   if(searchParameter!="")
	   {
	   document.getElementById('waitlight').style.display='block';
	    document.getElementById('waitfade').style.display='block';
	   
		   $.ajax({
				type : "Post",
				url : "searchUtilizerAccessDetails.html",
				data : "searchedData="+searchParameter,
				success : function(response) {
		           	
					document.getElementById('waitlight').style.display='none';
				    document.getElementById('waitfade').style.display='none';
				   
					     $("#searchedAccessCodeDiv").show();			     
					     $("#searchedAccessCodeDiv").html(response);			     
					     $("#listofAccessCodeDiv").hide();
					     
					},
				error : function(e) 
				   {				
					  alert("Due to some problem your request failed please try again later !");				
				   }				
			  });
	   }
   else
	   {
	          $("#listofAccessCodeDiv").show();
	          $("#searchedAccessCodeDiv").hide();			
	   }
  
}
	
function showSignOut()
{
	 
	var signOutFlag = document.getElementById("signOutFlag").value;
	
	if(signOutFlag=='0')
		{
		   $("#loginBox").show();
		   document.getElementById("signOutFlag").value = "1";
		}
	if(signOutFlag=='1')
		{
		   $("#loginBox").hide();
		   document.getElementById("signOutFlag").value = "0";
		}
		 
}


function checkSession(hittheUrl)
{
	 $.ajax({
			type : "post",
			url : "checksessionforUtilizer.html",
			success : function(response) {
				
				if(response=="Exist")
					{
					 	window.location = hittheUrl;
					}
				else
					{
					   $("#sessionlight").show();
				       $("#sessionfade").show();
					}
			}
		}); 	
}

function redirectToLoginIndividual()
{
	window.location = "utisignOut.html";	
}
	
function hideAddvertise()
{
    $("#ProfileHeadingDiv").fadeOut(90000);	 
}


function hidesearchDiv()
{
	 $('#searchicon').hide();
	 $('#searchhomepage').hide();
	 //$('#closediv').hide();
	 $('.topprofilestyle').hide();
	 document.getElementById("inputString").value = "";	 
}

function openSenderProfile(url)
{
	window.open(url,'_blank');
}

</script>

</head>

<body onclick="hidesearchDiv()"> 


	<div class="container">
		
		<div class="top_line"></div>
		
		
		<header class="header">
		
		<%
		   String noOfAwaitedDocuments = (String)request.getAttribute("noOfAwaitedDocuments");
		   String noOfRecivedDocuments = (String)request.getAttribute("noOfRecivedDocuments");
		%>
		
		
		
		<div class="header_inner">
			<div class="logo" onclick="gotoHome()"></div>

			<div class="tfclear"></div>
			<div class="utimainmenu">
			<div id="utimenu_wrapper">
				<ul class="uti_main_menu">
					<li></li>
					<li id="myMenus1"><a class="myprofile" href="#" onclick="checkSession('utimyProfile.html')"> <br>
							<br> My Profile
					</a></li>
					
					
					<li id="myMenus2" style="width:140px">
					     					        					        
					        <a class="mydocumentsfocus" href="#" onclick="checkSession('reciveddocuments.html')"> <br>
							<br> Received Documents
					        </a>
					     					        
					         <%if(!noOfAwaitedDocuments.equals("0")){ %>
												  
							     <div class="noti_bubble_right"><%=noOfAwaitedDocuments%></div>
							     
							 <%} %>
							 
					 </li>
					 
					
					<li id="myMenus4"><a class="myplans" href="#" onclick="checkSession('utilizerPlan.html')"> <br> <br>
							My Plans					
					</a></li>
					
					<li id="myMenus6"><a  class="alearticons" href="#"  onclick="checkSession('utilizeralerts.html')"><br>
							<br> Alerts
					</a>
					</li>	
					<li id="myMenus5"><a class="mysettings" href="#"  onclick="checkSession('utilizerSettings.html')"> <br> <br>
							My Settings					
					</a></li>
					
				</ul>
			</div>
			</div>
		</div>
		
		
					<!-- Login Starts Here -->
             <div id="loginContainer">
            
            <div class="sidesearchbar">
             
              <input type="hidden" id="signOutFlag" value="0" />
                                
             <a href="#" id="loginButton" onclick="showSignOut()">
                <span></span>
                <em id="loginNameId">${utilizerProfilePic.uti_first_name}</em></a>
                 <div style="clear:both"></div>
            
            
           <div style="clear:both"></div>
            <div class="searchboxhome">
            
             <span onclick="showsearchDiv()">
                 <input class="searchtextleft" type="search" id="inputString" title="Enter atleast 3 character for efficient search !" placeholder="Search Individual" onkeyup="searchIndividual()" />
             </span>
             
            </div>
            
            <span class="topprofilestyle" style="display: none;"></span>
					  <div id="searchicon" style="display:none;">
																
							 <div id="searchhomepage" style="display:block;">
							
							 </div>
													
				       </div>
				       
				       
            <div id="closediv" style="display:block;" class="closebtn_searchindex" onclick="hidesearchDiv()"></div>
            </div>
            
               <div style="clear:both"></div>
                <div id="loginBox">  
                          <span></span>   
                   
                    <form id="loginForm">
                                          
                        <fieldset id="body">
                        
                        <fieldset>                       
                        <a href="utisignOut.html"><input type="button" id="login" value="Sign out"/></a>                       
                        </fieldset>                                                
                        </fieldset>
                       
                    </form>
                </div>
            </div>
            <!-- Login Ends Here -->
 
 
          

		</header>


		<div class="main" id="mainDivID">
			
			<div class="main_center">
					
																	
				<div class="utileftside">      <!--left menus should go here-->
										
						<div class="user_display">  
																				
								<div id="textBox">
																														
										<div id="button">
											   
											   <ul>
											      <li id="Hi-B-1"><a id="documentsactive" href="reciveddocuments.html"><span>Viewed Documents</span></a>
											      
											            <div class="noti_bubble_left"><%=noOfRecivedDocuments%></div>
											      
											      </li>
											      
											      
												  <li id="employee_document"><a href="#" id="awaitedId" onclick="awaitedDocument()"><span>Awaited Documents</span></a>
												  
												  <%if(!noOfAwaitedDocuments.equals("0")){ %>
												  
												     <div class="noti_bubble"><%=noOfAwaitedDocuments%></div>
												     
												  <%} %>
												  
												  </li>														
											   </ul>
											   
										</div>																				
								</div> 			
								
								<!-- End of textBox Div  -->
								
													
						</div>           <!-- End of user_display -->
					
				</div>    <!-- End of left side Div -->


											
				<div id="utiRightSideDivId" class="utirightside">
					
					  
				       
				       <!-- Advertisment Div for Entire Page  -->
						
						
						
						  <%
							   //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");		   
								 String allAdvertise = addResource.getString("allKycUser");		
								
								// End of Required file Config for Advertisement 
								
								
							%>

									<div id="ProfileHeadingDiv" style="display:block;">
									
										<marquee behavior="scroll" scrollamount="5" direction="left">
											   <h1 class="rightsideheader">
											       
											        <%=utiAdvertise%>
											        
											   </h1>
									     </marquee>
									     
							        </div>	
							       
							        		
					   <!-- Advertisment Div for Entire Page  -->
				       
				       
				       	<div id="suggestions1">
				       							       
								<!--center body should go here-->
																 																
								  <div class="border_line"></div>
								  
								  <div class=" username_bar">
										
										<div id="titleBar">
											
											<h2>
												 <span>List of Viewed Documents</span>										    												
											</h2>
											
										</div>     
										
									 <!-- End of tittleBar Div -->
										
								  </div>			<!-- End of username_bar Div -->
									
								
								<div class="utimodule-body">
									
											<div id="viewUsers" style="height: auto; width: auto;  margin: 0 auto;">
					                           
							                <!-- Statr Div of viewUsers -->     
							                 
							                <c:if test="${!empty viewedData}">
							                          
								                 <div ID="utimoduleinpu" style="display:block;margin-left:156px;">
								                      <ul>
								                         
								                          <li style="float:left !important;">
								                        		<input type="text" class="utivieweddocinput" placeholder="Search Access via Name" id="accessSearchId"  onkeyup="searchAccessDetails()"/>
								                          </li>
								                         
								                          <li style="float:left !important;">			
								                         		<input type="button" value="Search" onclick="searchAccessDetails()"/>
								                          </li>
								                          
								                      </ul>           	                 
								                 </div>
							                 
							                 </c:if>
							                 
							                 <div style="margin-top:70px;text-align: left;color:#7c7c7c;">
							                 <p><i>Below are the list of individuals shared documents with you.</i></p>
							                 <p><i>Click on view to access the documents.</i> </p>
							                 </div>
							                 
							                 <div id="searchedAccessCodeDiv" style="display:none;margin-top: 31px;">
							                      
							                 
							                 </div>
							                 
							                 <div id="listofAccessCodeDiv" style="display:block;margin-top: 31px;">
							                 
									                 <c:if test="${!empty viewedData}">
									                 
												                 <table class='CSSTableGenerator' style="width:860px">
												                    
												                          <tr>
												                               <!-- <th>S.NO</th>	 -->									                               
												                               <th>Name</th>
												                               <th>Access Date</th>
												                               <th>Description</th>
												                               <th>View Details</th>												                               
												                          </tr>
												                    
												                         <%int i=0; %>
												                         
												                             <c:forEach items="${viewedData}" var="det">	
												                            
														                            <tr>
															                              
																                          <%-- <td><c:out value="<%=++i%>"/></td>	 --%>																	
																						  <td style="width:180px;"><c:out value="${det.giver_name}"/></td>
																						  <td style="width:130px;"><c:out value="${det.access_given_date}"/></td>
																						  <td style="width:300px;"><c:out value="${det.access_description}"/></td>
																						  <td style="width:130px;"><a class="utiviewdocument" href="#" onclick="openSenderProfile('validateProfileViewingForUtilizerFromRecDocuments.html?accessGiver=${det.access_giver}')">View</a></td>    <!-- onclick="viewDetailsDoc('${det.access_giver}','${det.access_given_date}','i')" -->
																						  																																								
																					</tr>
																					
												                             </c:forEach>	
												                          
												                    </table>
									                    
									                 </c:if>   
									                    
									               <c:if test="${empty viewedData}">
		                                            
				                                            <div class="notice" style="margin-left: 245px;margin: 72px auto 0;width: auto;">
															      
															      <b>NOTICE :</b> Nothing is there in the Viewed Access !
															      
															</div>
														        
		                                           </c:if>   
		                                           
                                           </div>
                                           
                                           
                                           
                                           
                                           <div style="color:#7c7c7c;">
							                 <p><b>Note  : </b>     &nbsp;Individual list may disappear if individual revoked access from you.</p>
							                 
							               </div>
							                 
							                 
							                 
							                    <!-- End of Div of viewUsers -->        							                            							                          
					
											</div>			
																				
								</div>     
								
								 <!-- End of module-body Div -->
							
                        </div>

				  </div>				 <!-- End of rightside div -->
								
				<br />

			</div>            <!-- End of Main Center div -->
			
		</div>          <!-- End of Main div -->



		<!--footer block should go here-->
		
		<%
			ResourceBundle resource = ResourceBundle.getBundle("resources/KYCversion");
			String version=resource.getString("kycversion");
			String versionDate=resource.getString("kyclatestDate");
		%>

		
		<div class="footer">

		<div class="footer_copyrights">
			<div class="copyrights">
				<ul>
					<li class="footer_space01">
						<p>Copyright � 2015 All rights reserved</p><p>by Wired Document Processing Pvt Ltd .</p>
						
						</li>
						
					<li class="footer_space02">
						<p>
							<a href="#" onclick="getkycAboutUs()">About Us</a> |<a href="clenseeTermConditions.html" target="_blank" > Terms & Conditions</a> |<a
								href="clenseePrivatePolicy.html" target="_blank"> Privacy Policy</a> | <a href="utiContactus.html">Contact Us</a>
						</p>
						
						<!-- onclick="getTermsConditions()" -->
						
					</li>
					<li class="footer_space03">

						<div class="four columns">
                                <ul class="social_media">
                                     <li>
                                     <a class="google_plus" href="https://www.facebook.com/pages/Clensee/1006044356090416" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="twitter" href="https://twitter.com/clensee" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="fb" href="https://plus.google.com/117596930649780597529/about/p/pub" target="_blank"></a>
                                    </li>
                                    <li>
                                    <a class="ln" href="https://www.linkedin.com/pub/clensee/b5/700/15" target="_blank"></a>
                                    </li>
                                    <!-- <li>
                                    <a class="rss" href="http://feeder.co/" target="_blank"></a>
                                    </li> -->
                                </ul>
                                
                                
						
                              </div>
                              
                              

					</li>
					
					<li>
					
					    <p style="color: #ccc;font-family: Segoe UI;font-size: 12px; margin-bottom: 0; margin-left: 134px !important; margin-top: -7px ! important;">
 
									Powered by <span> <a style="color: #ccc ! important;" href="http://jirehsol.com/" target="_blank">Jireh
									</a></span>
								</p>
								
					</li>

				</ul>
			</div>

		</div>
		
		
		</div>
	
			<div class="footer_line"></div>

	</div>
	<!------container End-------->
	




<!-- Waiting Div -->
	
	 <div id="waitlight">
			
		 
		              <div class="kycloader1">
							
							 <div align="center">
	
									<div id="logoDiv"><span class="logo"></span></div>
									
										<div id="circular3dG">
										
										    
										
											<div id="circular3d_1G" class="circular3dG"></div>
											<div id="circular3d_2G" class="circular3dG"></div>
											<div id="circular3d_3G" class="circular3dG"></div>
											<div id="circular3d_4G" class="circular3dG"></div>
											<div id="circular3d_5G" class="circular3dG"></div>
											<div id="circular3d_6G" class="circular3dG"></div>
											<div id="circular3d_7G" class="circular3dG"></div>
											<div id="circular3d_8G" class="circular3dG"></div>
											
										</div>
										Give Us Few Seconds.....
						       </div>
									
                      </div>
                      
	</div>
	
	<div id="waitfade" ></div> 	

	
<!-- EOF Waiting Div -->

<!-- kycDiv Div -->
	
	<div id="kycdivlight">
				<div class="colsebutton" onclick="fadeout()"><img height="22" width="22" class="footerpageinn_links"  src="resources/images/close_button.png" /></div>
		   
		    <div id="kycdivdisplaySharedDocDiv" > 
		              
		              
		    </div>		   
	</div>
	
	<div id="kycdivfade" onclick="fadeout()"></div> 	
	
	
	<!-- EOF kycDiv Div -->	
		
	
	
	
	<!-- session Div -->
	
	<div id="sessionlight">
			
		    <div id="sessiondisplaySharedDocDiv"> 
		              
		        Your session has expired,Please click on the button to relogin !      
		         
		         <input type="button" value="Login" onclick="redirectToLoginIndividual()"/>     
		    </div>		   
	</div>
	
	<div id="sessionfade" ></div> 	
	
<!-- EOF session Div -->
	
			
	
	
	
		
	
	<!-- Document Pop up Div -->
	
	<div id="kycDocPopUplight">
			
		    <div id="kycDocPopUpDisplay" style="text-align:left;margin-top:27px;width:auto;margin-left:-99px;						
						color:#555;
						border-radius:24px;	
						padding:7px 12px; 
						margin:0 auto;"> 
		              		              
		    </div>	
		    	   
	</div> 
	
	<div id="kycDocPopUpfade" onclick="fadeoutPopUp()"></div> 	
	
  <!-- EOF Document Pop up Div -->	
	
		
		<!-- report Div -->
	
	<!-- report Div -->
	
	<div id="sociallight">
			
		    <div id="socialdisplaySharedDocDiv" > 
		         
		         
		              
		    </div>		   
	</div>
	
	<div id="socialfade"></div> 	
	
<!-- EOF report Div -->

<!-- EOF report Div -->
		
</body>
</html>
