<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<%@ page language="java" import="java.util.*" %>



<script>
function saveIndAd(adFor,id)
{
	$("#waitingdivId").show();
	 
	var adInfo = document.getElementById(id).value;
		
	$.ajax({
		type : "post",
		url : adFor,
		data : "addInfo="+adInfo,
		success : function(response) {
			
			  $("#waitingdivId").hide();			
			  if(response="success")
				  {
					  alert("Ad has successfully saved .");					  
					  getAdvertisment();					   
				  }
			  else
				  {
				  	  alert("Failed due to some reason so please try again later .");					  
				  	  getAdvertisment();	
				  }
			  
			},				
	});	
}

</script>

</head>

<body>


<div>
 
     <h1><u>Add Advertisement</u></h1><br><br>
      
         <table>
      
               <tr>
               
	               <td>
	               <span style="float:left;"><b>Individual Advertisement Text :</b></span><br>
	               </td>
	               
	               <td>
	               <textarea id="indAd" rows="" cols="" placeholder="Give your Advertisement message , please limit upto 30-40 characters "  style="width:600px;;height:45px;"></textarea>
	               </td>
	               
	               <td>
	               <input type="button" value="Save" onclick="saveIndAd('setIndAdertisement.html','indAd')"/>
	               </td>
               
               </tr>
               
         </table>
           
           
           <br><br>

           <table>
           
               <tr>
               
                   <td>
	               <span style="float:left;"><b>Utilizer Advertisement Text :</b></span><br>
	               </td>
	               
	               <td>
	               <textarea id="utiAd" rows="" cols="" placeholder="Give your Advertisement message , please limit upto 30-40 characters "  style="width:600px;height:45px;"></textarea>
	               </td>
	                
	                <td>
	                <input type="button" value="Save" onclick="saveIndAd('setUtiAdertisement.html','utiAd')"/>
	                </td>
	                
	           </tr>
           
          </table>

</div>

<div style=""></div>



                      <!-- Advertisment Div for Entire Page  -->
																	
						    <%
							 //Required file Config for Advertisement 
							
								 ResourceBundle addResource = ResourceBundle.getBundle("resources/kycaddvertisment");
								
							     String indAdvertise = addResource.getString("individualAdd");		
								 String utiAdvertise = addResource.getString("utilizerAll");	
				  			  								 
							// End of Required file Config for Advertisement 																
						%>

					   <!-- Advertisment Div for Entire Page  -->


<div>
 
     <h1><u>Current Advertisement</u></h1><br><br>
     <br>
     
     <table>
            <tr>
              
                <td><span><b>Individual Advertisement :</b></span></td>
            
                <td>  <%=indAdvertise%> </td>
                
            </tr>     
     </table>
               
       <br>        
              
      <table>
            <tr>
              
                <td>
                  <span><b> Utilizer Advertisement :</b></span></td>
            
                <td>  <%=utiAdvertise%> </td>
                
            </tr>
     
     </table>
               
         
</div>


</body>
</html>