function report(type) {

	window.location.href = "latestDocumentType.html?type=" + type;

}

function removeListItem() {
	
	var htmlSelect = document.getElementById('selectYear');

	if (htmlSelect.options.length == 0) {
		alert('You have removed all options');
		return false;
	}
	var optionToRemove = htmlSelect.options.selectedIndex;
	htmlSelect.remove(optionToRemove);
	alert('The selected option has been removed successfully');
	return true;
}

function addNewListItem(id) {
	
	var htmlSelect = document.getElementById('selectYear');
	var optionValue = document.getElementById('txtYearValue_'+id);
	var optionDisplaytext = document.getElementById('txtYearDisplayValue');

	
	if (isOptionAlreadyExist(htmlSelect, optionValue.value)) {
		alert('Option value already exists');
		optionValue.focus();
		return false;
	}
	
	
	var selectBoxOption = document.createElement("option");
	selectBoxOption.value = optionValue.value;
	selectBoxOption.text = selectBoxOption.value;
	htmlSelect.add(selectBoxOption, null);
	alert("Option has been added successfully");
	return true;
}

function isOptionAlreadyExist(listBox, value) {
	var exists = false;
	for ( var x = 0; x < listBox.options.length; x++) {
		if (listBox.options[x].value == value
				|| listBox.options[x].text == value) {
			exists = true;
			break;
		}
	}
	return exists;
}
