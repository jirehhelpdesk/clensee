package com.kyc.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.FileAppender;
import org.apache.log4j.SimpleLayout;

import com.kyc.controller.JIREncryptionUtils;

public class FileEncryptionDecryptionUtil {

	   
	 // Required file Config for entire Controller 
	
	 static ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 static String kycMainDirectory = fileResource.getString("fileDirectory");	
	 static File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller 
	 
	 
	public static void getOwnFileDecryption(String OwnIndId,String OwnRegYear,HttpServletRequest request)
	{			 
		File targetFile = new File((filePath.getPath()+"/"+OwnRegYear+"/"+OwnIndId+"/Document"));														
		JIREncryptionUtils.encryptAllDocs(targetFile,"dec");
		
		targetFile = null;		
		System.gc();
	}
	
	
	public static void getDecryptSpecificFileOFOwn(String OwnRegYear,String OwnIndId,String Subdirectory,String fileName,HttpServletRequest request)
	{			
			 File targetFile = new File((filePath.getPath()+"/"+OwnRegYear+"/"+OwnIndId+"/Document/"+Subdirectory+"/"+fileName));														
			/* while((targetFile.length()) % 16 == 0)
			 {*/							 
				 JIREncryptionUtils.encryptAllDocs(targetFile,"dec");
			/* }*/
			
	}
	
	
	public static void getFileDecryption(String OtherIndId,String otherRegYear,HttpServletRequest request)
	{			 
		String OtherIndIdArray[] = OtherIndId.split(",");
		String otherRegYearArray[] = otherRegYear.split(",");
		
		for(int id=0;id<OtherIndIdArray.length;id++)
		{
			File targetFile = new File((filePath.getPath()+"/"+otherRegYearArray[id]+"/"+OtherIndIdArray[id]+"/Document"));															
			JIREncryptionUtils.encryptAllDocs(targetFile,"dec");	
			
			targetFile = null;			
			System.gc();
		}
	}
	
	
	public static void getFileEncryption(String OtherIndId,String otherRegYear,HttpServletRequest request)
	{		
		String OtherIndIdArray[] = OtherIndId.split(",");
		String otherRegYearArray[] = otherRegYear.split(",");
		
		for(int id=0;id<OtherIndIdArray.length;id++)
		{
			File targetFile = new File((filePath.getPath()+"/"+otherRegYearArray[id]+"/"+OtherIndIdArray[id]+"/Document"));
			JIREncryptionUtils.encryptAllDocs(targetFile,"enc");	
			
			targetFile = null;			
			System.gc();
		}
	}
	
	
	public static void getOtherFileEncryption(String OtherIndId,String otherRegYear,HttpServletRequest request)
	{		
			File targetFile = new File((filePath.getPath()+"/"+OtherIndId+"/"+otherRegYear+"/Document"));
			JIREncryptionUtils.encryptAllDocs(targetFile,"enc");	
			
			targetFile = null;			
			System.gc();
	}
	
	public static void getOtherFileEncryptionFromAdmin(String otherRegYear,int OtherIndId,String Subdirectory)
	{		
		if(Subdirectory.equals("All Documents"))
		{
			File targetFile = new File((filePath.getPath()+"/"+otherRegYear+"/"+OtherIndId+"/Document"));
			JIREncryptionUtils.encryptAllDocs(targetFile,"enc");	
			
			targetFile = null;			
			System.gc();
		}
		else
		{
			System.out.println("It hits-"+filePath.getPath()+"/"+otherRegYear+"/"+OtherIndId+"/Document/"+Subdirectory);
			
			File targetFile = new File((filePath.getPath()+"/"+otherRegYear+"/"+OtherIndId+"/Document/"+Subdirectory));			
			JIREncryptionUtils.encryptAllDocs(targetFile,"enc");				
			targetFile = null;			
			System.gc();
		}
			
	}
	
	public static void EncryptAllDocumentsForRequestOfAdmin()
	{
		File targetFile = new File((filePath.getPath()));
		JIREncryptionUtils.encryptAllDocs(targetFile,"enc");	
		
		targetFile = null;			
		System.gc();
	}
	
}
