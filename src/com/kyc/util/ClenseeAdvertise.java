package com.kyc.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ClenseeAdvertise {
	
	public String advertiseConfig(String addInfo) {
		
		String status = "";
		try {
			
			Properties properties = new Properties();
			properties.setProperty("individualAdd", addInfo);
			
			File file = new File("/KYC/src/resources/kycaddvertisment.properties");
			FileOutputStream fileOut = new FileOutputStream(file);
			properties.store(fileOut, "Favorite Things");
			fileOut.close();
			status = "success";	
		}
		catch (FileNotFoundException e) 
		{
			status = "failure";
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			status = "failure";
			e.printStackTrace();
		}

		return status;
	}
}