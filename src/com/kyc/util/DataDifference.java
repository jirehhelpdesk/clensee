package com.kyc.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
 
public class DataDifference {
 
	public int getDateDifference(Date activeDate,Date todaysDate) throws ParseException	
	{
		  long diff = 0;
		   
		   Calendar c1 = Calendar.getInstance();		   
		   c1.setTime(activeDate);		 
		   Calendar c2 = Calendar.getInstance();		  
		   c2.setTime(todaysDate);		  
		   long ms1 = c1.getTimeInMillis();
		   long ms2 = c2.getTimeInMillis();		  
		   diff = ms2 - ms1;
		  
		  int diffInDays = (int) (diff / (24 * 60 * 60 * 1000));
		  System.out.println("Number of days difference is: " + diffInDays);
		  
		  return diffInDays;
	}
	
}
