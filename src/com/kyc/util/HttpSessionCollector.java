package com.kyc.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.FileAppender;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.SimpleLayout;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.controller.JIREncryptionUtils;
import com.kyc.controller.MyDocumentController;

public class HttpSessionCollector implements ServletContextListener, HttpSessionListener, ServletRequestListener {

    private static final String ATTRIBUTE_NAME = "com.kyc.util.HttpSessionCollector";
    private static final Map<String,HttpSession> sessions = new ConcurrentHashMap<String,HttpSession>();
    private static final Map<String,String> userSessions = new ConcurrentHashMap<String,String>();
    private static final Map<String,String> userFiles = new ConcurrentHashMap<String,String>();
    private String fileStoragePath;

   
    
    @Override
    public void contextInitialized(ServletContextEvent event) {
        event.getServletContext().setAttribute(ATTRIBUTE_NAME, this);
        
        // initialize log4j here to track every action in logger file in logger directory
        
        ServletContext context = event.getServletContext();
        String log4jConfigFile = context.getInitParameter("log4j-config-location");
        String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;
       
        PropertyConfigurator.configure(fullPath);
         
        // End of initialize log4j here to track every action in logger file in logger directory
        
        
		        // Intialize the scheduler for File Encryption And Decryption
		        
				     try
				        {
				        	JobDetail job = JobBuilder.newJob(FileEncryptionScheduler.class).withIdentity("schedulerJobName", "fileEncryption").build();
				    				   		      
				    		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("schedulerTriggerName", "fileEncryption").withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(3600).repeatForever()).build();
				    		
							// schedule it
				    		
							Scheduler scheduler = new StdSchedulerFactory().getScheduler();
							scheduler.start();
														
							RecordScedulerTime recSchTime = new RecordScedulerTime();
							recSchTime.calendarTime = Calendar.getInstance().getTime().toString();
														
							scheduler.scheduleJob(job, trigger);
							
				        }
				        catch(Exception e)
				        {
				        	System.out.println(e.getMessage());
				        }
				        
		        // End of Scheduler File Encryption and Decryption 
                
    }

    @Override
    public void requestInitialized(ServletRequestEvent event) {
    	 	
       HttpServletRequest request = (HttpServletRequest) event.getServletRequest();
        HttpSession session = request.getSession(false);
        if(session != null)
        {       
        	System.out.println("Session is Exist !");           			   		
        }
        else
        {
        	System.out.println("Session is Destroyed !");           		
        }
    }

  
    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
    	
    	System.out.println("Session was Destroyed !");
    	       
    	//String USER_ID = userSessions.get(event.getSession().getId());
       	
    	sessions.remove(event.getSession().getId());
  
    }

    @Override
    public void sessionCreated(HttpSessionEvent event) {
    	// No logic needed.
    }

    @Override
    public void requestDestroyed(ServletRequestEvent event) {
        // No logic needed.
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        // NOOP. No logic needed. Maybe some future cleanup?
    	
    	
    }

    public static HttpSessionCollector getInstance(ServletContext context) {
        return (HttpSessionCollector) context.getAttribute(ATTRIBUTE_NAME);
    }

    public static Map<String,HttpSession> getAllSession(String myUser) {
        return sessions;
    }
    
    public static HttpSession find(String sessionId) {
        return sessions.get(sessionId);
    }
    
    public static void updateReqDetails(String sessionId,String userId) {
    	userSessions.put(sessionId,userId);
    }
    
    public static String getUserSessions(String myUser,String sessionId){
    	
    	if("KYC_USER_FILE_P7".equals(myUser))
    	{
    		return userSessions.get(sessionId);
    	}
    	else
    	{
    		return null;
    	}
    }

}