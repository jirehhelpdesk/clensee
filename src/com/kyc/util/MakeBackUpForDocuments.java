package com.kyc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MakeBackUpForDocuments {
	
        /*public static void main(String[] args) {
                try 
                {
                        File sourceFile = new File("D:/Test_Repository/2015/137/Document/KYC_DOCUMENT/1024_1.png");
                        File destinationFile = new File("E:/Test_Repository/2015/137/Document/KYC_DOCUMENT");
                        if(!destinationFile.exists())
                        {
                        	destinationFile.mkdirs();
                        }                        
                        File destinationFile1 = new File("E:/Test_Repository/2015/137/Document/KYC_DOCUMENT/" + sourceFile.getName());
                        MakeBackUpForDocuments copyFileExample = new MakeBackUpForDocuments();
                        copyFileExample.copyFile(sourceFile, destinationFile1);
                } 
                catch (Exception e) 
                {
                        e.printStackTrace();
                }
        }
*/
        public void copyFile(File sourceFile, File destinationFile) {
                try {
                        FileInputStream fileInputStream = new FileInputStream(sourceFile);
                        FileOutputStream fileOutputStream = new FileOutputStream(
                                        destinationFile);

                        int bufferSize;
                        byte[] bufffer = new byte[512];
                        while ((bufferSize = fileInputStream.read(bufffer)) > 0) {
                                fileOutputStream.write(bufffer, 0, bufferSize);
                        }
                        fileInputStream.close();
                        fileOutputStream.close();
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }
}
