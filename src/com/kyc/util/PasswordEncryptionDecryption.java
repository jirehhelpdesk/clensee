package com.kyc.util;

import org.jasypt.util.password.PasswordEncryptor;
import org.jasypt.util.password.BasicPasswordEncryptor;
 
public class PasswordEncryptionDecryption {
	
	
	public static String getEncryptPassword(String givenPassword)
	{
		PasswordEncryptor encryptor = new BasicPasswordEncryptor();
		String encrypted = encryptor.encryptPassword(givenPassword);
		System.out.println("encrypted = " + encrypted);
		
		return encrypted;
	}
	
	public static String getValidatePassword(String givenPassword,String ExistPassword)
	{
		 PasswordEncryptor encryptor = new BasicPasswordEncryptor();
		
		 System.out.println("OldPassword="+givenPassword);
	     System.out.println("existPassword="+ExistPassword);
	       		
		 if (encryptor.checkPassword(givenPassword, ExistPassword)) 
	        {			  
			   return "True";
	        } 
	        else 
	        {
	        	System.out.println("Invalid secret word, access denied!");
	        	return "False";
	        }
	}
	
	/*public static void main(String[] args) {
        
		PasswordEncryptionDecryption.getEncryptPassword("Dhoni7");
	
    }*/
	
}
