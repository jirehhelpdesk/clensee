package com.kyc.util;

import java.io.File;

import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;


public class JIRCryptoUtils {
	
	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES";

	public static void encrypt(String key, File inputFile, File outputFile) throws CryptoException
	{
		//System.out.println(inputFile.length() + "-"+inputFile.getName()+" -is File size before encrypt");
		
		doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
	}

	public static void decrypt(String key, File inputFile, File outputFile) throws CryptoException {
		
		//System.out.println(inputFile.length() + "-"+inputFile.getName()+" is File size before decrypt");
		
		doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);
	}

	@SuppressWarnings("resource")
	private static void doCrypto(int cipherMode, String key, File inputFile, File outputFile) throws CryptoException {
		
		// To encrypt a file
		
		if(cipherMode==1)
    	{
    		if ((inputFile.length()) % 16 != 0) 
			{	
    			//System.out.println(inputFile.getName() + " is going to be encrypt.");
    			
    			FileInputStream inputStream = null;
    			FileOutputStream outputStream = null;
    			
    			try{						
						Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
						Cipher cipher = Cipher.getInstance(TRANSFORMATION);
						cipher.init(cipherMode, secretKey);
	
						inputStream = new FileInputStream(inputFile);
						byte[] inputBytes = new byte[(int) inputFile.length()];
						
						
						inputStream.read(inputBytes);
	
						byte[] outputBytes = cipher.doFinal(inputBytes);
	
							outputStream = new FileOutputStream(outputFile);
							outputStream.write(outputBytes);
	
							inputStream.close();
							outputStream.close();							
					} 
					catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException
							| IOException ex) 
    			    {
						throw new CryptoException("Error encrypting/decrypting file", ex);
					}
    			
			}
    		else
    		{
    			/*if(inputFile.getName().contains("pdf"))
    			{
    				System.out.println(inputFile.getName() + " is sdfsdfsd."+((inputFile.length()) % 16));    				
    			}  	*/		
    		}   	  		   		
    	}
		
		
		// To decrypt a file
		
		if(cipherMode==2)
    	{
    		if ((inputFile.length()) % 16 == 0) 
			{
    			//System.out.println(inputFile.getName() + " is going to be decrypt.");
    			
    			FileInputStream inputStream = null;
    			FileOutputStream outputStream = null;
    			
				try {						
						Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
						Cipher cipher = Cipher.getInstance(TRANSFORMATION);
						cipher.init(cipherMode, secretKey);
	
						inputStream = new FileInputStream(inputFile);
						byte[] inputBytes = new byte[(int) inputFile.length()];
												
						inputStream.read(inputBytes);
	
						byte[] outputBytes = cipher.doFinal(inputBytes);
	
						outputStream = new FileOutputStream(outputFile);
						outputStream.write(outputBytes);
	
						inputStream.close();
						outputStream.close();
							
					} 
					catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException
							| IOException ex) {
						throw new CryptoException("Error encrypting/decrypting file", ex);
					}					
			}
    		else
    		{
    			//System.out.println(inputFile.getName() + " is already encryptd.");
    		}
    	  
    	}
	
	}
}