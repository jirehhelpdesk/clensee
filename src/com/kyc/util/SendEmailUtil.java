package com.kyc.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import javax.mail.AuthenticationFailedException;
import javax.mail.IllegalWriteException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.kyc.controller.MyDocumentController;
import com.precisonline.smtp.SMTPClient;

public class SendEmailUtil {  
  
	private static Logger log = Logger.getLogger(MyDocumentController.class);
	FileAppender loggerfile;	 
	FileHandler fh;
	
	ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
	String logDirectory=loggerFileDirectory.getString("loggerDirectoryEmail");
	
	public String getEmailSent1(String emailId,String subject,String messageBody,JavaMailSender mailSender)throws AuthenticationFailedException,SendFailedException,IllegalWriteException, IOException
 	{ 		   
    	  String resetStatus = "";
 		     	     	      	    
    	   String DATE_FORMAT_NOW = "dd-MM-yyyy";
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
			String stringDate = sdf.format(date );
			stringDate += "-Clensee-Email-Logger";
			 
			   
		  loggerfile = new FileAppender(new SimpleLayout(),""+logDirectory+"/"+stringDate+".log",false);
		
		  log.addAppender(loggerfile);
		  loggerfile.setLayout(new SimpleLayout());
		    
 	       try
 	       {  	  	    	  
 		        MimeMessage message = mailSender.createMimeMessage();   		  
 		        MimeMessageHelper helper = new MimeMessageHelper(message,true);
 		        
 		        helper.setTo(emailId);          
 		        helper.setSubject(subject);      
 		        
 		        StringBuilder text = new StringBuilder();		        
 		        text.append(messageBody);
 		        
 		        helper.setText(text.toString(), true);			 	
 		        
 		        mailSender.send(message);		
 		        
 		        resetStatus = "success";	   			    
 		       
 		       // Logger File  for current user's mail status with Name Date and Time
		        
 			    log.info("-"+date+"--"+emailId+"--"+subject+"--"+resetStatus);
 			      
 			   // Logger Config who has sent mail			       
		  } 	        
 		  catch(Exception e)
 		  {
 			  resetStatus = "failure";
 			  System.out.println("Due to certain reason it has not worked");
 			  e.printStackTrace();	
 			  
 			  // Logger File  for current user's mail status with Name Date and Time
		        
			    log.info("-"+date+"--"+emailId+"--"+subject+"--"+resetStatus);
			      
			  // Logger Config who has sent mail		
			    
			    log.error("-Error due to-"+e.toString());			
 		  }  					
 		   	      
 		 return resetStatus;
 	}
	
	
	public String getEmailSent(String emailId,String subject,String messageBody) throws IOException
	{
		 
		  String resetStatus = "";
		  
		    String DATE_FORMAT_NOW = "dd-MM-yyyy";
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
			String stringDate = sdf.format(date );
			stringDate += "-Clensee-Email-Logger";
			 
			   
		  loggerfile = new FileAppender(new SimpleLayout(),""+logDirectory+"/"+stringDate+".log",false);
		
		  log.addAppender(loggerfile);
		  loggerfile.setLayout(new SimpleLayout());
			
		  ResourceBundle smsresource = ResourceBundle.getBundle("resources/emailConfig");
			
		    String auth=smsresource.getString("mail.smtp.auth");
			String starttls = smsresource.getString("mail.smtp.starttls.enable");
			String host = smsresource.getString("mail.smtp.host");			
			String port = smsresource.getString("mail.smtp.port");
			String emailusername = smsresource.getString("emailusername");
			String emailpassword = smsresource.getString("emailpassword");
			
			
			
	      String to = emailId;
	      String from = emailusername;
	      final String username = from;//change accordingly
	      final String password = emailpassword;//change accordingly
	      Properties props = new Properties();
	      
			props.put("mail.smtp.auth", auth);
			props.put("mail.smtp.starttls.enable", starttls);
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
	 
			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
	 
			try {
	 
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(from));
				message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
				message.setSubject(subject);
				message.setContent(messageBody, "text/html");
	 
				Transport.send(message);
	 
				resetStatus = "success";	
				 
			   
 		       // Logger File  for current user's mail status with Name Date and Time
		        
 			    log.info("-"+date+"--"+emailId+"--"+subject+"--"+resetStatus);
 			      
 			   // Logger Config who has sent mail	
 
			} catch (MessagingException e) {
				
				resetStatus = "failure";
	 			  System.out.println("Due to certain reason it has not worked");
	 			  e.printStackTrace();	
	 		 
	 			  // Logger File  for current user's mail status with Name Date and Time
			        
				    log.info("-"+date+"--"+emailId+"--"+subject+"--"+resetStatus);
				      
				  // Logger Config who has sent mail		
				    
				    log.error("-Error due to-"+e.toString());		
			}
	      
	      return resetStatus;
	}
	
	
	/*public static void main(String arg[])
	{
		SendEmailUtil obj = new SendEmailUtil();
		try {
			obj.sendMailInUnix("raulaabinash@gmail.com", "Abc", "def");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
}  

