package com.kyc.util;

import java.io.File;
import java.util.ResourceBundle;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.kyc.controller.JIREncryptionUtils;

public class FileEncryptionScheduler implements Job {

	 // Required file Config for entire Controller 
	
	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 String kycMainDirectory = fileResource.getString("fileDirectory");	
	 File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller 
	 
	public void execute(JobExecutionContext context) throws JobExecutionException 
	{				 
			File targetFile = new File((filePath.getPath()));				
			JIREncryptionUtils.encryptAllDocs(targetFile,"enc");		
			
			targetFile = null;		
			System.gc();
	}
		 
}
