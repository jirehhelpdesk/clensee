package com.kyc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

import com.kyc.controller.MyDocumentController;

public class SendSms {
public static String retval="";
    

		
    public static String SMSSender(String user,String password,String msg,String sid,String number,String fl) throws IOException    
    {
    	
        String rsp="";
        String smsStatus = "";
        
         Logger log = Logger.getLogger(MyDocumentController.class);
		FileAppender loggerfile;	 
		FileHandler fh;
		
		ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
		String logDirectory=loggerFileDirectory.getString("loggerDirectorySMS");

		   String DATE_FORMAT_NOW = "dd-MM-yyyy";
		   Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
			String stringDate = sdf.format(date );
			stringDate += "-Clensee-SMSLogger";
			 
			   
		  loggerfile = new FileAppender(new SimpleLayout(),""+logDirectory+"/"+stringDate+".log",false);
		
		  log.addAppender(loggerfile);
		  loggerfile.setLayout(new SimpleLayout());
		
        try {
           
        	// Construct The Post Data
            String data = "";
            
            String smsMessage = URLEncoder.encode(msg, "UTF-8");
            
            //Push the HTTP Request
            URL url = new URL("http://tran.bulksmshyderabad.co.in/pushsms.php?username="+user+"&password="+password+"&message="+smsMessage+"&sender="+sid+"&numbers="+number+"");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
        
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
           
            //Read The Response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                // Process line...
                retval += line;
            }
            wr.close();
            rd.close();
                       
            rsp = retval;
            smsStatus = "success";
            
            // Logger File  for current user's mail status with Name Date and Time
	        
			    log.info("-"+date+"--"+number+"--SMS Sent--"+smsStatus);
			      
			// Logger Config who has sent mail	

            
        } catch (Exception e) 
        {
        	smsStatus = "failure";
            e.printStackTrace();
            e.printStackTrace();		
            
         // Logger File  for current user's mail status with Name Date and Time
	        
		    log.info("-"+date+"--"+number+"--SMS not Sent--"+smsStatus);
		      
		// Logger Config who has sent mail	
			
			log.error("SMS Cause Clues-2="+e.toString());
        }
        return  smsStatus;
    }
        
   /*public static void main(String[] args) throws IOException {        
       
    	String response = SMSSender("kyc","85941","Hello how r u-4","CLNSE","9008435960","0");   
        
        System.out.println(response);
    }
    */
}