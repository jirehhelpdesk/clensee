package com.kyc.service;

import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kyc.dao.IndSignupDao;
import com.kyc.model.DocumentModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.VisibilityModel;


@Service("indSignupService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class IndSignupServiceImpl implements IndSignupService {

	@Autowired
	private IndSignupDao signupDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public String addIndividual(IndSignupModel individual) {
		
		return signupDao.addIndividual(individual);
	}
    
	public int getLastIndId()
	{
		return signupDao.getLastIndId();
	}
	
	public String checkIndividual(IndSignupModel individual) throws Exception
	{
		return signupDao.checkIndividual(individual);
	}
	
	public String getInd_id(IndSignupModel individual)
	{
		return signupDao.getInd_id(individual);
	}
	
	public List<IndSignupModel> getInd_idmax(IndSignupModel individual)
	{
		return signupDao.getInd_idmax(individual);
	}
	
	public String getcheckpassword(IndSignupModel indsignupmodel)
	{
		return signupDao.getcheckpassword(indsignupmodel);
	}
	
	public String savestatusreg(IndSignupModel indsignupmodel)
	{
		return signupDao.savestatusreg(indsignupmodel);
	}
	
	public String getchangepassword(IndSignupModel indsignupmodel)
	{
		return signupDao.getchangepassword(indsignupmodel);
	}
	
	public String editIndividual(RegHistoryModel indHistory) {
		return signupDao.editIndividual(indHistory);
			 
		}
	
	public String reghistory(int ind_id)
	{
		List<RegHistoryModel> regHistory  = signupDao.reghistory(ind_id);
		
		List<String> history = new ArrayList<String>();
		
		for(int i=0;i<regHistory.size();i++)
		{
			history.add(regHistory.get(i).getFirst_name());
			history.add(regHistory.get(i).getMiddle_name());
			history.add(regHistory.get(i).getLast_name());
			history.add(regHistory.get(i).getGender());
			history.add(regHistory.get(i).getEmail_id());
			String mobile_no = Long.toString(regHistory.get(i).getMobile_no());
			history.add(mobile_no);
			DateFormat df = new SimpleDateFormat("YYYY/mm/dd/ HH:mm:ss");
			Date today = regHistory.get(i).getCr_date();  			
			String reportDate = df.format(today);
			history.add(reportDate);
			
		}
		return history.toString();
			
		}
	
	
	public IndSignupModel getname(int reg_id) {
		return signupDao.getname(reg_id);
		
	}

	public String getUtilizerName(int utiIndId)
	{
		return signupDao.getUtilizerName(utiIndId);
	}
	
	@Override
	public void getname(IndSignupModel indSignupModel) {
		// TODO Auto-generated method stub
		
	}

	public String saveRegHistory(RegHistoryModel regmodel)
	{
		return signupDao.saveRegHistory(regmodel);	
	}
	
	public RegHistoryModel getRegistrationDetails(int ind_id)
	{
		return signupDao.getRegistrationDetails(ind_id);
	}
	
	public String findyear(String kycId)
	{
		return signupDao.findyear(kycId);
	}
	
	public String getVisibility(int ind_id)
	{
		return signupDao.getVisibility(ind_id);
	}
	
	public String saveVisibilitySetting(VisibilityModel visibility)
	{
		return signupDao.saveVisibilitySetting(visibility);
	}
	
	public String getActiveStatus(String kycId)
	{
		return signupDao.getActiveStatus(kycId);
	}
	
	public String getnamefromRegHistory(int ind_id)
	{
		return signupDao.getnamefromRegHistory(ind_id);
	}
	
	public String checkStatus(String kycId)
	{
		return signupDao.checkStatus(kycId);
	}
	
	public int getindividualIndId(String uniqueVariable)
	{
		return signupDao.getindividualIndId(uniqueVariable);
	}
	
	public RegHistoryModel getLatestRegData(int ind_id)
	{
		return signupDao.getLatestRegData(ind_id);
	}
	
	public String getCheckStatus(String reserVariable)
	{
		return signupDao.getCheckStatus(reserVariable);
	}
	
	public String getDeleteResetPaswordInfo(String uniqueVariable)
	{
		return signupDao.getDeleteResetPaswordInfo(uniqueVariable);
	}
	
	public String getActiveValidityStatus(String uniqueVariable)
	{
		return signupDao.getActiveValidityStatus(uniqueVariable);	
	}
	
	public String getUpdateAppliedCodeStatus(String uniqueVariable)
	{
		return signupDao.getUpdateAppliedCodeStatus(uniqueVariable);
	}
	
	public String getKYCId(String uniqueId)
	{
		return signupDao.getKYCId(uniqueId);
	}
	
	public String statusOfIndividualForLogin(IndSignupModel individual)
	{
		return signupDao.statusOfIndividualForLogin(individual);
	}
	
	public String getKYCIDFromCookiesValue(String indId)
	{
		return signupDao.getKYCIDFromCookiesValue(indId);
	}
}
