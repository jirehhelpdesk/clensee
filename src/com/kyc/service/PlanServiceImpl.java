package com.kyc.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kyc.dao.PlanDao;
import com.kyc.model.ClenseeInvoiceModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.FeaturesDetailsModel;
import com.kyc.model.PaymentDetailsModel;
import com.kyc.model.PlanDetailsModel;
import com.kyc.util.SendEmailUtil;

@Service("PlanService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)


public class PlanServiceImpl implements PlanService{

	@Autowired
	private PlanDao PlanDao;
	
	@Override
	public String addFeaturesDetail(FeaturesDetailsModel featuresDetailsModel) {
		return PlanDao.addFeaturesDetail(featuresDetailsModel);
		
	}
	public String getfeaturename() {
		return PlanDao.featurename();
		
	}
	public String getplannameview() {
		
		return PlanDao.getplannameview();		
	}
	
	public String getfeaturecategory(String featureCategory) {
		

		String featureNames = "";
		
		List<String> featureNameList = PlanDao.featurecategory(featureCategory);	
												
		if(featureNameList.size()>0)
		{
			for(int i=0;i<featureNameList.size();i++)
			{
			    featureNames += featureNameList.get(i) + ",";
			}
			
			if(featureNames.length()>0)
			{
				featureNames = featureNames.substring(0,featureNames.length()-1);
			}
			
		}	
				
		return featureNames;		
	}
	
	public String getFeatureIdByFeatureNames(String featureCategory,String featurecategory)
	{
	    return PlanDao.getFeatureIdByFeatureNames(featureCategory,featurecategory);	
	}
	
	public String getPlanFeatureId(String featureCategory,String planName)
	{
		return PlanDao.getPlanFeatureId(featureCategory,planName);
	}
	
	public String getPlanDetails(String basicFeatures_Id,String featurecategory)
	{
		return PlanDao.getPlanDetails(basicFeatures_Id,featurecategory);
	}
	
	public List<FeaturesDetailsModel> getallfeatures(FeaturesDetailsModel FeaturesDetailsModel)
	{
		return PlanDao.getallfeatures(FeaturesDetailsModel);		
	}
	
	public List<FeaturesDetailsModel> selectedviewfeature(FeaturesDetailsModel FeaturesDetailsModel)
	{
		return PlanDao.selectedviewfeature(FeaturesDetailsModel);		
	}

	public List<FeaturesDetailsModel> selectedviewplan(FeaturesDetailsModel FeaturesDetailsModel)
	{
		return PlanDao.selectedviewplan(FeaturesDetailsModel);		
	}
	
	public List<FeaturesDetailsModel> getAllPlanDetailsBytheDetails(String plan_category,String plan_domain,String plan_name)
	{
		return PlanDao.getAllPlanDetailsBytheDetails(plan_category,plan_domain,plan_name);
	}
	
	
	public float getPlanPricAsPerPlanCategory(String plan_category,String plan_domain,String plan_name)
	{
		return PlanDao.getPlanPricAsPerPlanCategory(plan_category,plan_domain,plan_name);
	}
	
	public float getPlanPricAsPrCategory(String planCategory)
	{
		return PlanDao.getPlanPricAsPrCategory(planCategory);
	}
	
	public List<PlanDetailsModel> getorganiseplandetail(PlanDetailsModel PlanDetailsModel)
	{
		return PlanDao.getorganiseplandetail(PlanDetailsModel);		
	}
	
	@Override
	public String addPlanDetail(PlanDetailsModel PlanDetailsModel) {
		
		return PlanDao.addPlanDetail(PlanDetailsModel);		
	}
	
	public String getplanname(FeaturesDetailsModel FeaturesDetailsModel) {
		return PlanDao.getplanname(FeaturesDetailsModel);
		
	}   
	public List<FeaturesDetailsModel> getcurrentplan(FeaturesDetailsModel FeaturesDetailsModel) {
		return PlanDao.getcurrentplan(FeaturesDetailsModel);
		
	}
	public List<String> getplanhasfeature(PlanDetailsModel planDetailsModel) {
		return PlanDao.getplanhasfeature(planDetailsModel);
		
	}
	public List<FeaturesDetailsModel> getplanhasfeatureorganise(PlanDetailsModel planDetailsModel) {
		return PlanDao.getplanhasfeatureorganise(planDetailsModel);
		
	}
	public String deleteplanhasfeature(FeaturesDetailsModel FeaturesDetailsModel) {
		return PlanDao.deleteplanhasfeature(FeaturesDetailsModel);
		
	}
	public String deletePlanDetails(String planType,String PlanName) {
		return PlanDao.deletePlanDetails(planType,PlanName);
		
	}
	public String rangeselectedFeaturesDetail(FeaturesDetailsModel FeaturesDetailsModel) {
		return PlanDao.rangeselectedFeaturesDetail(FeaturesDetailsModel);
		
	}
	
	public String changeplan(int ind_id,String requestedPlanName)
	{
		return PlanDao.changeplan(ind_id,requestedPlanName);
		
	}
	
	public String getFeatureDetailsByCategory(String featureCategory,String featureName)
	{
		String fetureDetails = "";
		List<FeaturesDetailsModel> featureDetailsModelList =  PlanDao.getFeatureDetailsByCategory(featureCategory,featureName);
		List<String> featureDetailsList = new ArrayList<String>();
		
		for(int i=0;i<featureDetailsModelList.size();i++)
		{
			int featureId = featureDetailsModelList.get(i).getFeature_id();
			String feature_id = Integer.toString(featureId);
			featureDetailsList.add(feature_id);
			featureDetailsList.add(featureDetailsModelList.get(i).getFeature_name());
			featureDetailsList.add(featureDetailsModelList.get(i).getFeature_value());
			int price = featureDetailsModelList.get(i).getFeature_price();
			String priceValue = Integer.toString(price);
			featureDetailsList.add(priceValue);
		}
		
		for(int j=0;j<featureDetailsList.size();j++)
		{
			fetureDetails += featureDetailsList.get(j) + ",";
		}
		
		if(fetureDetails.length()>0)
		{
			fetureDetails = fetureDetails.substring(0, fetureDetails.length()-1);
		}
		
		
		return fetureDetails;
	}
	
	public List<FeaturesDetailsModel> getallfeaturesbyCategory(String featureCategory)
	{
		return PlanDao.getFeaturesThroughCategory(featureCategory);
	}
	
	public List<FeaturesDetailsModel> getallfeaturesbyCategoryAndName(String featureCategory,String featureName)
	{
		return PlanDao.getallfeaturesbyCategoryAndName(featureCategory,featureName);
	}
	
	public String getPlanNamebyCategory(String planCategory)
	{
		String searchResult = "";
		
		List<String> planNameList = PlanDao.getPlanNameByCategory(planCategory);
		
		if(planNameList.size()>0)
		{
			for(int i=0;i<planNameList.size();i++)
			{
				searchResult += planNameList.get(i) + ",";
			}
			
			if(searchResult.length()>0)
			{
				searchResult = searchResult.substring(0,searchResult.length()-1);
			}
						
			return searchResult;
		}
		else
		{			
			return "No Data";
		}
		
	}
	
	
	public String getPlanNameforUtilizer(String planDomain)
	{
         String searchResult = "";
		
		List<String> planNameList = PlanDao.getPlanNameforUtilizer(planDomain);
		
		if(planNameList.size()>0)
		{
			for(int i=0;i<planNameList.size();i++)
			{
				searchResult += planNameList.get(i) + ",";
			}
			
			if(searchResult.length()>0)
			{
				searchResult = searchResult.substring(0,searchResult.length()-1);
			}
						
			return searchResult;
		}
		else
		{			
			return "No Data";
		}
		
	}
	
	public String getPlanFeatureIdDetails(String planName,String planType)
	{
		return PlanDao.getPlanFeatureIdDetails(planName,planType);
	}
	
	
	public String getFeaturesForUtilizer(String planName,String planType,String planDomain)
	{
		return PlanDao.getFeaturesForUtilizer(planName,planType,planDomain);
	}
	
	
	public int getPlanIdByNameAndType(String plan_name,String plan_type,String plan_domain)
	{
		return PlanDao.getPlanIdByNameAndType(plan_name,plan_type,plan_domain);
	}
	
	public float  getPlanPrice(String featureCategory,String planName)
	{
		return PlanDao.getPlanPrice(featureCategory,planName);
	}
	
	public float getPlanPriceforUtilizer(String featureCategory,String planDomain,String planName)
	{
		return PlanDao.getPlanPriceforUtilizer(featureCategory,planDomain,planName);
	}
	
	public String getUsageDetails(String featurecategory)
	{
		String searchResult = "";
		
		
		
		return searchResult;
	}
	
	public String getKycId(int ind_id)
	{
		return PlanDao.getKycId(ind_id);
	}
	
	public int getCountGeneratedCode(String kyc_id)
	{
		return PlanDao.getCountGeneratedCode(kyc_id);
	}
	
	public int getCountApplyCode(String kyc_id)
	{
		return PlanDao.getCountApplyCode(kyc_id);
	}
	
	public int getCountAllowAccess(String kyc_id)
	{
	    return  PlanDao.getCountAllowAccess(kyc_id);	
	}
	
	public int getCountRevokeAccess(String kyc_id)
	{
		return PlanDao.getCountRevokeAccess(kyc_id);
	}
	
	public float getTotalFileStorage(int ind_id)
	{
	    return PlanDao.getTotalFileStorage(ind_id);    	
	}
	
	public String getCurrentPlanName(int ind_id)
	{
		return PlanDao.getCurrentPlanName(ind_id);
	}
	
	public String getFeatureNameById(int feature_id)
	{
		return PlanDao.getFeatureNameById(feature_id);
	}
	
	public String getFeatureValue(int feature_id)
	{
		return PlanDao.getFeatureValue(feature_id);
	}
	
	public int noOfFullProfileAsOfNow(String kyc_id)
	{
		return PlanDao.noOfFullProfileAsOfNow(kyc_id);
	}
	
	public List<FeaturesDetailsModel> getUtilizerCurrentplan(FeaturesDetailsModel FeaturesDetailsModels)
	{
		return PlanDao.getUtilizerCurrentplan(FeaturesDetailsModels);
	}
	
	public String getUtilizerplanname(int uti_Ind_id)
	{
		return PlanDao.getUtilizerplanname(uti_Ind_id);	
	}
	
	public String getUtilizerPlanDomain(int uti_Ind_id)
	{
		return PlanDao.getUtilizerPlanDomain(uti_Ind_id);
	}
	
	public String getUtilizerPlanFeatureId(String featureCategory,String planName,String planDomain)
	{
		return PlanDao.getUtilizerPlanFeatureId(featureCategory,planName,planDomain);
	}
	
	public String getUtilizerUniqueId(int uti_Ind_id)
	{
		return PlanDao.getUtilizerUniqueId(uti_Ind_id);
	}
	
	public String getUtilizerFeatureIdsAsPerPlan(String utilizerCategory,String planDomain,String planName)	
	{
		return PlanDao.getUtilizerFeatureIdsAsPerPlan(utilizerCategory,planDomain,planName);
	}
	
	public int getCountAllowAccessByUtilizer(String utiUniqueId)
	{
		return PlanDao.getCountAllowAccessByUtilizer(utiUniqueId);
	}
	
	public String migrateUtilizerPlan(String requestedPlanName,int uti_Ind_id)
	{
		return PlanDao.migrateUtilizerPlan(requestedPlanName,uti_Ind_id);
	}
	
	public int findUtiid(String uti_kycid)
	{
		return PlanDao.findUtiid(uti_kycid);
	}
	
	public String getCurrentPlanNameOfUtilizer(int uti_indid)
	{
		return PlanDao.getCurrentPlanNameOfUtilizer(uti_indid);
	}
	
	public String getPlanFeatureIdDetailsForUtilizer(String planName,String domianName,String category)
	{
		return PlanDao.getPlanFeatureIdDetailsForUtilizer(planName,domianName,category);
	}
	
	public int noOfFullProfileAsOfNowOfUtilizer(int uti_indid)
	{
		return PlanDao.noOfFullProfileAsOfNowOfUtilizer(uti_indid);
	}
	
	public int noOfSMSAsOfNowAsOfNowOfUtilizer(int uti_Indid)
	{
		return PlanDao.noOfSMSAsOfNowAsOfNowOfUtilizer(uti_Indid);
	}
	
	
	public float getPlanPriceAsPerRequest(String requestedPlanName)
	{
		return PlanDao.getPlanPriceAsPerRequest(requestedPlanName);
	}
	
	public String getCurrentMobileNo(int ind_id)
	{
		return PlanDao.getCurrentMobileNo(ind_id);
	}
	 
	public String getCurrentEmailId(int ind_id)
	{
		return PlanDao.getCurrentEmailId(ind_id);
	}
	
	public String getFullName(int ind_id)
	{
		return PlanDao.getFullName(ind_id);
	}
	
	public String getCheckPlanExistOrNot4Ind(String planName,String planType)
	{
		return PlanDao.getCheckPlanExistOrNot4Ind(planName,planType);
	}
	
	public String getCheckPlanExistOrNot4Uti(String planName,String planDomain,String planType)
	{
		return PlanDao.getCheckPlanExistOrNot4Uti(planName,planDomain,planType);
	}
	
	public String getCheckPlanExistOrNot4Cor(String planName,String planType)
	{
		return PlanDao.getCheckPlanExistOrNot4Cor(planName,planType);
	}
	
	public String getPlanNameForUti(String planCategory,String planDomain)
	{
        String searchResult = "";
		
		List<String> planNameList = PlanDao.getPlanNameForUti(planCategory,planDomain);
		
		if(planNameList.size()>0)
		{
			for(int i=0;i<planNameList.size();i++)
			{
				searchResult += planNameList.get(i) + ",";
			}
			
			if(searchResult.length()>0)
			{
				searchResult = searchResult.substring(0,searchResult.length()-1);
			}
			
			
			return searchResult;
		}
		else
		{			
			return "No Data";
		}
	}
	
	public String getAllFeatureDetailsByPlanName(String planType)
	{
		String featureDetails = "";
		List<FeaturesDetailsModel> featureDetailsModelList =  PlanDao.getAllFeatureDetailsByPlanType(planType);
		List<String> featureDetailsList = new ArrayList<String>();
		
		for(int i=0;i<featureDetailsModelList.size();i++)
		{
			int featureId = featureDetailsModelList.get(i).getFeature_id();
			String feature_id = Integer.toString(featureId);
			featureDetailsList.add(feature_id);
			featureDetailsList.add(featureDetailsModelList.get(i).getFeature_name());
			featureDetailsList.add(featureDetailsModelList.get(i).getFeature_value());
			int price = featureDetailsModelList.get(i).getFeature_price();
			String priceValue = Integer.toString(price);
			featureDetailsList.add(priceValue);
		}
		
		for(int j=0;j<featureDetailsList.size();j++)
		{
			featureDetails += featureDetailsList.get(j) + ",";
		}
		
		if(featureDetails.length()>0)
		{
			featureDetails = featureDetails.substring(0, featureDetails.length()-1);
		}
		
		
		return featureDetails;		
	}
	
	public String getAllfeatureDetailsByPlanDetails(String planName,String planType,String planDomain)
	{
		String featureDetails = "";
		List<FeaturesDetailsModel> featureDetailsModelList =  PlanDao.getAllfeatureDetailsByPlanDetails(planName,planType,planDomain);
		List<String> featureDetailsList = new ArrayList<String>();
		
		for(int i=0;i<featureDetailsModelList.size();i++)
		{
			int featureId = featureDetailsModelList.get(i).getFeature_id();
			String feature_id = Integer.toString(featureId);
			featureDetailsList.add(feature_id);
			featureDetailsList.add(featureDetailsModelList.get(i).getFeature_name());
			featureDetailsList.add(featureDetailsModelList.get(i).getFeature_value());
			int price = featureDetailsModelList.get(i).getFeature_price();
			String priceValue = Integer.toString(price);
			featureDetailsList.add(priceValue);
		}
		
		for(int j=0;j<featureDetailsList.size();j++)
		{
			featureDetails += featureDetailsList.get(j) + ",";
		}
		
		if(featureDetails.length()>0)
		{
			featureDetails = featureDetails.substring(0, featureDetails.length()-1);
		}
		
		
		return featureDetails;		
	}
	
	
	public int noOfSMSAsOfNow(int ind_id)
	{
		return PlanDao.noOfSMSAsOfNow(ind_id);
	}
	
	public Date getUtiActivatedPlanDate(int uti_Ind_id)
	{
		return PlanDao.getUtiActivatedPlanDate(uti_Ind_id);
	}
	
	
	public String savePaymentRecords(PaymentDetailsModel payu)
	{
		return PlanDao.savePaymentRecords(payu);
	}
	
	public int getUniquePaymentId(PaymentDetailsModel payu)
	{
		return PlanDao.getUniquePaymentId(payu);
	}
	
	public String updatePaymentStatus(int paymentId,String paymentAmount,String paymentStatus,String paymentTranId)
	{
		return PlanDao.updatePaymentStatus(paymentId,paymentAmount,paymentStatus,paymentTranId);
	}
	
	public String saveInvoiceDetails(ClenseeInvoiceModel invoiceData)
	{
		return PlanDao.saveInvoiceDetails(invoiceData);
	}
	
	public int getLastInvoiceNumber()
	{
		return PlanDao.getLastInvoiceNumber();
	}
	
	
	
	
	
	
	
	
	
}
