package com.kyc.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kyc.bean.IndSignupBean;
import com.kyc.dao.AdminPanelDao;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentHierarchyModel;
import com.kyc.model.DomainHierarchyModel;
import com.kyc.model.DomainLevelModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.PatternDetailsModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.SubDomainLevel10Model;
import com.kyc.model.SubDomainLevel1Model;
import com.kyc.model.SubDomainLevel2Model;
import com.kyc.model.SubDomainLevel3Model;
import com.kyc.model.SubDomainLevel4Model;
import com.kyc.model.SubDomainLevel5Model;
import com.kyc.model.SubDomainLevel6Model;
import com.kyc.model.SubDomainLevel7Model;
import com.kyc.model.SubDomainLevel8Model;
import com.kyc.model.SubDomainLevel9Model;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;

@Service("adminpanelservice")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AdminPanelServiceImpl implements AdminPanelService {

	@Autowired
	private AdminPanelDao apdao;

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<String> getAllUsersDetails() {
		List<String> usersList = new ArrayList<String>();
		List<IndSignupModel> usrLst = apdao.getAllUsersDetails();
		for (int i=0;i<usrLst.size();i++) {
			usersList.add(usrLst.get(i).getKycid());
			usersList.add(usrLst.get(i).getFirstname());
			usersList.add(usrLst.get(i).getLastname());
			usersList.add(usrLst.get(i).getEmailid());
			usersList.add(usrLst.get(i).getMobileno());

		}
		return usersList;
	}
	
	
	public String getUtiRefId(int utiUnqId)
	{
		return apdao.getUtiRefId(utiUnqId);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<String> searchindividualkycid(IndSignupModel searchprofile) {
		List<String> usersList = new ArrayList<String>();
		
		List<IndSignupModel> usrLst = apdao.searchindividualkycid(searchprofile);
		for (int i=0;i<usrLst.size();i++) {
			
			usersList.add(usrLst.get(i).getKycid());
			usersList.add(usrLst.get(i).getFirstname());
			usersList.add(usrLst.get(i).getLastname());
			usersList.add(usrLst.get(i).getEmailid());
			usersList.add(usrLst.get(i).getMobileno());
			usersList.add(usrLst.get(i).getPlan_available());
		    searchprofile.setIndid(usrLst.get(i).getIndid());
		    String usrLst1 = apdao.searchindividualkycidpic(searchprofile);
			usersList.add(usrLst1);
			usersList.add(usrLst.get(i).getStatus());
			usersList.add(usrLst.get(i).getIndid().toString());
			int ind_id = usrLst.get(i).getIndid();
		    int registerYear = apdao.getIndRegisteredYear(ind_id);
		    usersList.add(Integer.toString(registerYear));
		    
		}
		return usersList;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<String> searchindividualplan(IndSignupModel searchprofile) {
		List<String> usersList = new ArrayList<String>();
		
		List<IndSignupModel> usrLst = apdao.searchindividualplan(searchprofile);
		for (int i=0;i<usrLst.size();i++) {
			
			usersList.add(usrLst.get(i).getKycid());
			usersList.add(usrLst.get(i).getFirstname());
			usersList.add(usrLst.get(i).getLastname());
			usersList.add(usrLst.get(i).getEmailid());
			usersList.add(usrLst.get(i).getMobileno());
			usersList.add(usrLst.get(i).getPlan_available());
			searchprofile.setIndid(usrLst.get(i).getIndid());
			String usrLst1 = apdao.searchindividualkycidpic(searchprofile);
		    usersList.add(usrLst1);
		    usersList.add(usrLst.get(i).getStatus());
		    usersList.add(usrLst.get(i).getIndid().toString());
		    int ind_id = usrLst.get(i).getIndid();
		    int registerYear = apdao.getIndRegisteredYear(ind_id);
		    usersList.add(Integer.toString(registerYear));
		}
		return usersList;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<String> searchindividualname(IndSignupModel searchprofile) {
		List<String> usersList = new ArrayList<String>();
		
		List<IndSignupModel> usrLst = apdao.searchindividualname(searchprofile);
		for (int i=0;i<usrLst.size();i++) {
			
			usersList.add(usrLst.get(i).getKycid());
			usersList.add(usrLst.get(i).getFirstname());
			usersList.add(usrLst.get(i).getLastname());
			usersList.add(usrLst.get(i).getEmailid());
			usersList.add(usrLst.get(i).getMobileno());
			usersList.add(usrLst.get(i).getPlan_available());			
			searchprofile.setIndid(usrLst.get(i).getIndid());
			String usrLst1 = apdao.searchindividualkycidpic(searchprofile);
		    usersList.add(usrLst1);
		    usersList.add(usrLst.get(i).getStatus());
		    usersList.add(usrLst.get(i).getIndid().toString());
		    int ind_id = usrLst.get(i).getIndid();
		    int registerYear = apdao.getIndRegisteredYear(ind_id);
		    usersList.add(Integer.toString(registerYear));
		}
		return usersList;
	}
	
	
	public String changestatus(IndSignupModel changestatus)
	{
		return apdao.changestatus(changestatus);
	}
	
	public String findtypeGroup(String type)
	{		
		String searchResult = "";		
		if(!type.equals("PROFILE"))
		{
			if(type.equals("EMPLOYEMENT"))
			{
				searchResult = "Employment,PaySlip";
			}
			else
			{
				List<String> lsdata = apdao.findtypeGroup(type);				
				for(int i=0;i<lsdata.size();i++)
				{
					searchResult += lsdata.get(i)+",";
				}		
				searchResult = searchResult.substring(0, searchResult.length()-1);
			}
			
		}
		else
		{
			searchResult = "Basic Profile";
		}
		return searchResult;
		
	}
	
	public String find_acctypeGroup(String type)
	{
		return apdao.find_acctypeGroup(type);
	}

	public String findSub_category(String type)
	{
		return apdao.findSub_category(type);
	}
	
	public String savePattern(String pattern,String patternName)	
	{
        PatternDetailsModel patternDetails = new PatternDetailsModel();
		
        String profilePattern = "PROFILE:";
        
		String kycPattern = "KYC:";
		String academicPattern = "ACADEMIC:";
		String financialPattern = "FINANCIAL:";		
		String employmentPattern = "EMPLOYEMENT:";
		
        pattern = pattern.substring(0, pattern.length()-1);
        String patternArray[] = pattern.split(",");
       
        for(int i=0;i<patternArray.length;i++)
        {
        	String arr1[] = patternArray[i].split("-");
        	
        	if(arr1[0].equals("EMPLOYEMENT"))
        	{
        		employmentPattern += arr1[1]+",";
        	}
        	
        	else if(arr1[0].equals("KYC"))
        	{
        		kycPattern += arr1[1]+",";
        	}
        	
        	else if(arr1[0].equals("ACADEMIC"))
        	{
        		academicPattern += arr1[1]+",";
        	}
        	
        	else if(arr1[0].equals("FINANCIAL"))
        	{
        		financialPattern += arr1[1]+",";
        	}        	
        	else
        	{
        		profilePattern += patternArray[i]+",";
        	}
        	        	                                    
        }
        
        String patternLogic = "";
                                                             
        if(!profilePattern.equals("PROFILE:"))
        {
        	profilePattern = profilePattern.substring(0, profilePattern.length()-1);
        	patternLogic += profilePattern + "/";
        }
        
        if(!kycPattern.equals("KYC:"))
        {
        	kycPattern = kycPattern.substring(0, kycPattern.length()-1);
        	patternLogic += kycPattern + "/";
        }
        
        if(!academicPattern.equals("ACADEMIC:"))
        {
        	academicPattern = academicPattern.substring(0, academicPattern.length()-1);
        	patternLogic += academicPattern + "/";
        }
        
        if(!financialPattern.equals("FINANCIAL:"))
        {
        	financialPattern = financialPattern.substring(0, financialPattern.length()-1);
        	patternLogic += financialPattern + "/";
        }
        
        if(!employmentPattern.equals("EMPLOYEMENT:"))
        {
        	employmentPattern = employmentPattern.substring(0, employmentPattern.length()-1);
        	patternLogic += employmentPattern + "/";
        }
        
        patternLogic = patternLogic.substring(0, patternLogic.length()-1);
        
        patternDetails.setPattern_name(patternName);
        patternDetails.setCr_date(new Date());
        patternDetails.setPattern_logic(patternLogic);
        patternDetails.setCr_by("Admin");
		
		return apdao.savePattern(patternDetails);
	}
	

	public String getPatternNames()
	{
		List<String> lsdata = apdao.getPatternNames();
		
		String patt_name = "";
		
		Iterator iter = lsdata.iterator();
		
		while(iter.hasNext())
		{
			patt_name = patt_name + iter.next() +",";
		}
		
		System.out.println(patt_name);
		
		return patt_name;
	}
	
	public String getPlan_Name()
	{
		List<String> lsdata = apdao.getPlanName();
		
		String plan_name = "";
		
		Iterator iter = lsdata.iterator();
		
		while(iter.hasNext())
		{
			plan_name = plan_name + iter.next() +",";
		}
		
		
		
		return plan_name;
	}
	
	
	public String findPattern(String Pattern_Name)
	{
		return apdao.findPattern(Pattern_Name);
	}
	
	public List<PatternDetailsModel> patternforDelete()
	{
		return apdao.patternforDelete();
	}
	
	public String deletePattern(int p_id)
	{
		return apdao.deletePattern(p_id);
	}
	
	public String searchUtiliser(String searchData,String searchType)
	{
		List<UtiliserProfileModel> lsdata = apdao.searchUtiliser(searchData,searchType);
		
		List<String> lsd = new ArrayList<String>();
		
		for(int i=0;i<lsdata.size();i++)
		{
			
			lsd.add(lsdata.get(i).getKyc_uti_id());
			lsd.add(lsdata.get(i).getUti_first_name());
			lsd.add(lsdata.get(i).getUti_last_name());
			lsd.add(lsdata.get(i).getUti_email_id());
			lsd.add(lsdata.get(i).getUtiliser_plan());
			lsd.add(lsdata.get(i).getUti_dom_hierarchy());
			lsd.add(lsdata.get(i).getUti_office_first_no());
			lsd.add(Integer.toString(lsdata.get(i).getUti_ind_id()));
			
		}
		
		String search1 = lsd.toString();		
		String searchResult = search1.substring(1,search1.length()-1);
						
 		return searchResult;
	}
	
	public String upDatePattern(String pattern_name,String pattern)
	{		
		String p_logic = apdao.findLogic(pattern_name);
		
		String str = pattern.replaceFirst("/,","/");
		String str2 =str.replaceFirst("/","");
		
		String str3[] = p_logic.split(",/,");
		String str4[] = str2.split(",/,");
		
		for(int i=0;i<str3.length;i++)
		{								
			for(int j=0;j<str4.length;j++)
			{
				String str5[] = str3[i].split(":");
				String str6[] = str4[j].split(":");
				
				if(str5[0].equals(str6[0]))
				{  
				   System.out.println(str3[i]);
				   System.out.println(str4[j]);
				   
				   p_logic = p_logic.replaceAll(str3[i],str4[j]);
				   
				   
				}
			}						
		}
		
		String pattern_update = apdao.updatePattern(pattern_name,p_logic);
		return pattern_update;
	}
	
	public String saveUtiliserDetails(UtiliserProfileModel utiProfile)
	{
		return apdao.saveUtiliserDetails(utiProfile);
	}

	public int findPatternId(String pattern_name)
	{
		return apdao.findPatternId(pattern_name);
	}
	
	
	public List<UtiliserProfileModel> UtiliserProfileDetails(String utiid)
	{
		return apdao.UtiliserProfileDetails(utiid);
	}
	
	public int findUtiId(String utiid)
	{
		return apdao.findUtiId(utiid);
	}
	
    public String findKycid(int id)
    {
    	return apdao.findKycid(id);
    }
	
	public String findPassword(int id)
	{
		return apdao.findPassword(id);
	}
	
	public String findDomain(int id)
	{
		return apdao.findDomain(id);
	}
	
	public String getkycHierarchy()
	{
		List<String> kycList =  apdao.getkycHierarchy();
		String kycListStr = "";
		
		if(!kycList.isEmpty())
		{
			for(int i=0;i<kycList.size();i++)
			{
				kycListStr += kycList.get(i)+",";
			}
			
			return kycListStr;
		}
		
		return "No Data";
	}
	
	 public String saveKycHierarchy(DocumentHierarchyModel kycHierarchy)
	 {
		 return apdao.saveKycHierarchy(kycHierarchy);
	 }
	 
	 public int getHierId(String kycHierarchy)
	 {
		 return apdao.getHierId(kycHierarchy);
	 }
	 
	 public String getFinancialHierarchy()
	 {
		 return apdao.getFinancialHierarchy();
	 }
	 
	 public String getAcademicHierarchy(String acaDocType)
	 {
		 return apdao.getAcademicHierarchy(acaDocType);
	 }
	 
	 public String getAcademicDocType()
	 {
		 List<String> lsdata = apdao.getAcademicDocType();
		 
		 String academicDocType = "";
		 
		 if(lsdata.size()>0)
		 {
			 for(int i=0;i<lsdata.size();i++)
			 {
				 academicDocType = academicDocType + lsdata.get(i) + ",";
			 }
			 
			 academicDocType = academicDocType.substring(0, academicDocType.length()-1);
			 
			 return academicDocType;
		 }
		 else
		 {			 			 
			 return "No Data";
		 }
		 
	 }
	 
	 public String saveDomain(DomainLevelModel domainModel)
	 {
		 return apdao.saveDomain(domainModel);
	 }
	 
	 public String getDomainName()
	 {
		 List<String> domainList = apdao.getDomainName();
		 String allDomain = "";
		 if(domainList.size()>0)
		 {
			 for(int i=0;i<domainList.size();i++)
			 {
				  allDomain = allDomain + domainList.get(i) + ",";
			 }
			 allDomain = allDomain.substring(0,allDomain.length()-1);
			 return allDomain;
		 }
		 else
		 {
			 return "No Data";
		 }		 
	 }
	 
	 public String getDomainValue(String domainType)
	 {
		 String domainNames = "";
		 List<String> domainHierarchy = new ArrayList<String>();
					 
		 domainHierarchy = apdao.getDomainName();			     
							 		 
		 if(domainHierarchy.size()>0)
		 {			 
			 for(int i=0;i<domainHierarchy.size();i++)
			 {
				 domainNames = domainNames + domainHierarchy.get(i) + ",";				 
			 }
			 domainNames = domainNames.substring(0, domainNames.length()-1);			 
		 }	
		 else
		 {
			 domainNames = "No Data";
		 }
		 
		 return domainNames;
	 }
	 
	 public String getSubDomainValue(String domainType)
	 {	
		 String domainNames = "";
		 String domainHierarchy = "";

		 if(domainType.equals("subDomainlevel1"))
		 {			 
			 String columnId = "sub_domain_level_1_id";
		     String tableName = "SubDomainLevel1Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		 
		 if(domainType.equals("subDomainlevel2"))
		 {			 
			 String columnId = "sub_domain_level_2_id";
		     String tableName = "SubDomainLevel2Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		
		 if(domainType.equals("subDomainlevel3"))
		 {			 
			 String columnId = "sub_domain_level_3_id";
		     String tableName = "SubDomainLevel3Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		 
		 if(domainType.equals("subDomainlevel4"))
		 {			 
			 String columnId = "sub_domain_level_4_id";
		     String tableName = "SubDomainLevel4Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		 
		 if(domainType.equals("subDomainlevel5"))
		 {			 
			 String columnId = "sub_domain_level_5_id";
		     String tableName = "SubDomainLevel5Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		 
		 if(domainType.equals("subDomainlevel6"))
		 {			 
			 String columnId = "sub_domain_level_6_id";
		     String tableName = "SubDomainLevel6Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		 
		 if(domainType.equals("subDomainlevel7"))
		 {			 
			 String columnId = "sub_domain_level_7_id";
		     String tableName = "SubDomainLevel7Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 } 
		 
		 if(domainType.equals("subDomainlevel8"))
		 {			 
			 String columnId = "sub_domain_level_8_id";
		     String tableName = "SubDomainLevel8Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		 
		 if(domainType.equals("subDomainlevel9"))
		 {			 
			 String columnId = "sub_domain_level_9_id";
		     String tableName = "SubDomainLevel9Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		 
		 if(domainType.equals("subDomainlevel10"))
		 {			 
			 String columnId = "sub_domain_level_10_id";
		     String tableName = "SubDomainLevel10Model";
			 domainHierarchy = apdao.getDomainHierarchy(columnId,tableName);			 
		 }
		 
		 return domainHierarchy;
	 }
	 		 
	 //
	 //
	 // These code area contains all Saving operation of All Domain hierarchy  	 
	 //
	 //
	 //
	 
	 public String saveDomainHierarchy(String domainLavel,String domainType,String enterValue)
	 {
		
		 if(domainLavel.equals("domainName"))
		 {
			 SubDomainLevel1Model subDomainModel = new SubDomainLevel1Model();
			 
			 String parentTableName = "DomainLevelModel";
			 String parentIdColumn = "domain_id";
			 String parentTypeColum = "domain_name";
			 int parent_id = apdao.getParentId(parentIdColumn,parentTypeColum,domainType,parentTableName);
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel1(subDomainModel);
		 }
		 
		 if(domainLavel.equals("subDomainlevel1"))
		 {
			 SubDomainLevel2Model subDomainModel = new SubDomainLevel2Model();
			 
			 String parentTableName = "SubDomainLevel1Model";
			 String parentIdColumn = "sub_domain_level_1_id";
			 String parentTypeColum = "sub_domain_name";
			 
			 int parent_id = Integer.parseInt(domainType);			 
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel2(subDomainModel);
		 }
		 
		 if(domainLavel.equals("subDomainlevel2"))
		 {
			 SubDomainLevel3Model subDomainModel = new SubDomainLevel3Model();
			 
			 String parentTableName = "SubDomainLevel2Model";
			 String parentIdColumn = "sub_domain_level_2_id";
			 String parentTypeColum = "sub_domain_name";
			 int parent_id = Integer.parseInt(domainType);	
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel3(subDomainModel);
		 }
		 
		 if(domainLavel.equals("subDomainlevel3"))
		 {
			 SubDomainLevel4Model subDomainModel = new SubDomainLevel4Model();
			
			 String parentTableName = "SubDomainLevel3Model";
			 String parentIdColumn = "sub_domain_level_3_id";
			 String parentTypeColum = "sub_domain_name";
			 
			 int parent_id = Integer.parseInt(domainType);	
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel4(subDomainModel);
		 }
		 
		 if(domainLavel.equals("subDomainlevel4"))
		 {
			 SubDomainLevel5Model subDomainModel = new SubDomainLevel5Model();
			
			 String parentTableName = "SubDomainLevel4Model";
			 String parentIdColumn = "sub_domain_level_4_id";
			 String parentTypeColum = "sub_domain_name";
			 
			 int parent_id = Integer.parseInt(domainType);	
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel5(subDomainModel);
		 }
		 
		 if(domainLavel.equals("subDomainlevel5"))
		 {
			 SubDomainLevel6Model subDomainModel = new SubDomainLevel6Model();
			
			 String parentTableName = "SubDomainLevel5Model";
			 String parentIdColumn = "sub_domain_level_5_id";
			 String parentTypeColum = "sub_domain_name";
			 
			 int parent_id = Integer.parseInt(domainType);	
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel6(subDomainModel);
		 }
		
		 if(domainLavel.equals("subDomainlevel6"))
		 {
			 SubDomainLevel7Model subDomainModel = new SubDomainLevel7Model();
			
			 String parentTableName = "SubDomainLevel6Model";
			 String parentIdColumn = "sub_domain_level_6_id";
			 String parentTypeColum = "sub_domain_name";
			 
			 int parent_id = Integer.parseInt(domainType);	
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel7(subDomainModel);
		 }
		
		 if(domainLavel.equals("subDomainlevel7"))
		 {
			 SubDomainLevel8Model subDomainModel = new SubDomainLevel8Model();
			 
			 String parentTableName = "SubDomainLevel7Model";
			 String parentIdColumn = "sub_domain_level_7_id";
			 String parentTypeColum = "sub_domain_name";
			 
			 int parent_id = Integer.parseInt(domainType);	
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel8(subDomainModel);
		 }
		 
		 if(domainLavel.equals("subDomainlevel8"))
		 {
			 SubDomainLevel9Model subDomainModel = new SubDomainLevel9Model();
			 
			 String parentTableName = "SubDomainLevel8Model";
			 String parentIdColumn = "sub_domain_level_8_id";
			 String parentTypeColum = "sub_domain_name";
			 
			 int parent_id = Integer.parseInt(domainType);	
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel9(subDomainModel);
		 }
		
		 if(domainLavel.equals("subDomainlevel9"))
		 {
			 SubDomainLevel10Model subDomainModel = new SubDomainLevel10Model();
			 
			 String parentTableName = "SubDomainLevel9Model";
			 String parentIdColumn = "sub_domain_level_9_id";
			 String parentTypeColum = "sub_domain_name";
			 
			 int parent_id = Integer.parseInt(domainType);	
			 
			 subDomainModel.setParent_domain_id(parent_id);
			 subDomainModel.setSub_domain_name(enterValue);
			 			 
			 apdao.saveSubDomainLevel10(subDomainModel);
		 }
		 
		
		 return "";
	 }
	
	 
	//
	//
	// End of These code area contains all Saving operation of All Domain hierarchy  
	//
	//
	// 
	 
	 public String getEntireHierarchy(String domainLevel,String domainValue)
	 {
		 String entireDomainHierarchy = "";		 
		 entireDomainHierarchy = apdao.getEntireHierarchy(domainLevel,domainValue);
		 return entireDomainHierarchy;
	 }
	 
	 public String getDeleteHierarchy(String domainLevel,String domainValue)
	 {
		 String statusDomainHierarchy = "";	
		 int domainTableStatus ; 
		 
		 if(domainLevel.equals("domainName"))
		 {			
			 String tableName = "DomainLevelModel";
			 String primaryColumn = "domain_name";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		 
		 }
		 
		 if(domainLevel.equals("subDomainlevel1"))
		 {
			 String tableName = "SubDomainLevel1Model";
			 String primaryColumn = "sub_domain_level_1_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel2"))
		 {
			 String tableName = "SubDomainLevel2Model";
			 String primaryColumn = "sub_domain_level_2_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 if(domainLevel.equals("subDomainlevel3"))
		 {
			 String tableName = "SubDomainLevel3Model";
			 String primaryColumn = "sub_domain_level_3_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel4"))
		 {
			 String tableName = "SubDomainLevel4Model";
			 String primaryColumn = "sub_domain_level_4_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel5"))
		 {
			 String tableName = "SubDomainLevel5Model";
			 String primaryColumn = "sub_domain_level_5_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel1"))
		 {
			 String tableName = "SubDomainLevel1Model";
			 String primaryColumn = "sub_domain_level_1_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel6"))
		 {
			 String tableName = "SubDomainLevel6Model";
			 String primaryColumn = "sub_domain_level_6_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel7"))
		 {
			 String tableName = "SubDomainLevel7Model";
			 String primaryColumn = "sub_domain_level_7_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel8"))
		 {
			 String tableName = "SubDomainLevel8Model";
			 String primaryColumn = "sub_domain_level_8_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel9"))
		 {
			 String tableName = "SubDomainLevel9Model";
			 String primaryColumn = "sub_domain_level_9_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 if(domainLevel.equals("subDomainlevel10"))
		 {
			 String tableName = "SubDomainLevel10Model";
			 String primaryColumn = "sub_domain_level_10_id";
			 domainTableStatus = apdao.getDeleteDomain(tableName,primaryColumn,domainValue);		
		 }
		 
		 return statusDomainHierarchy;
	 }
	 
	 public String getDomainStatus(String domainLevel,String domainValue)
	 {
		
		 String domainStatus = "";
		 if(domainLevel.equals("domainName"))
		 {			
			 domainStatus = apdao.getMainDomainStatus(domainValue);
		 }
		 else if(domainLevel.equals("subDomainlevel1"))
		 {			 
			 String child_table_name = "SubDomainLevel2Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel2"))
		 {			 
			 String child_table_name = "SubDomainLevel3Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel3"))
		 {			 
			 String child_table_name = "SubDomainLevel4Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel4"))
		 {			 
			 String child_table_name = "SubDomainLevel5Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel5"))
		 {			 
			 String child_table_name = "SubDomainLevel6Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel6"))
		 {			 
			 String child_table_name = "SubDomainLevel7Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel7"))
		 {			 
			 String child_table_name = "SubDomainLevel8Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel8"))
		 {			 
			 String child_table_name = "SubDomainLevel9Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel9"))
		 {			 
			 String child_table_name = "SubDomainLevel10Model";
			 domainStatus = apdao.getSubDomainStatus(domainValue,child_table_name); 
		 }
		 else if(domainLevel.equals("subDomainlevel10"))
		 {
			 domainStatus += "NoChild";
		 }
		 
		 return domainStatus;
	 }
	 
	 public String getSubDomainForCreateUtiliser(String domainName)
	 {
		 String searchResult = "";		 
		 int domainId = apdao.getDomainIdbyDomainName(domainName);
		 
		 searchResult = apdao.subDomainByDomainId(domainId);
		 
		 return searchResult;
	 }
	 
	 public String getHeirarchyFromSubDomain(int domainId,String currentLevel)
	 {
		 String searchResult = "";
		 String childTablesName = "";
		 String parentTableName = "";
		 String parentIdColumn = "";
		 String childIdColumn = "";
		 
		 
		 if(currentLevel.equals("cb1"))
		 {			
			 parentTableName = "SubDomainLevel1Model";
			 parentIdColumn = "sub_domain_level_1_id";
			 
			 childTablesName = "SubDomainLevel2Model";
			 childIdColumn = "sub_domain_level_2_id";
			 
			 String parentName = apdao.getParentNameById(parentTableName,parentIdColumn,domainId);
			 
			 String childValue = apdao.getHierarchyByParentId(childTablesName,childIdColumn,domainId);
			 
			 searchResult += parentName+"#"+childValue;
			 
		 }
		 
		 if(currentLevel.equals("cb2"))
		 {			
			 parentTableName = "SubDomainLevel2Model";
			 parentIdColumn = "sub_domain_level_2_id";
			 
			 childTablesName = "SubDomainLevel3Model";
			 childIdColumn = "sub_domain_level_3_id";
			 
			 String parentName = apdao.getParentNameById(parentTableName,parentIdColumn,domainId);
			 
			 String childValue = apdao.getHierarchyByParentId(childTablesName,childIdColumn,domainId);
			 
			 searchResult += parentName+"#"+childValue;
			 
		 }
		 
		 if(currentLevel.equals("cb3"))
		 {			
			 parentTableName = "SubDomainLevel3Model";
			 parentIdColumn = "sub_domain_level_3_id";
			 
			 childTablesName = "SubDomainLevel4Model";
			 childIdColumn = "sub_domain_level_4_id";
			 
			 String parentName = apdao.getParentNameById(parentTableName,parentIdColumn,domainId);
			 
			 String childValue = apdao.getHierarchyByParentId(childTablesName,childIdColumn,domainId);
			 
			 searchResult += parentName+"#"+childValue;
			 
		 }
		 
		 if(currentLevel.equals("cb4"))
		 {			
			 parentTableName = "SubDomainLevel4Model";
			 parentIdColumn = "sub_domain_level_4_id";
			 
			 childTablesName = "SubDomainLevel5Model";
			 childIdColumn = "sub_domain_level_5_id";
			 
			 String parentName = apdao.getParentNameById(parentTableName,parentIdColumn,domainId);
			 
			 String childValue = apdao.getHierarchyByParentId(childTablesName,childIdColumn,domainId);
			 
			 searchResult += parentName+"#"+childValue;
			 
		 }
		 
		 if(currentLevel.equals("cb5"))
		 {			
			 parentTableName = "SubDomainLevel5Model";
			 parentIdColumn = "sub_domain_level_5_id";
			 
			 childTablesName = "SubDomainLevel6Model";
			 childIdColumn = "sub_domain_level_6_id";
			 
			 String parentName = apdao.getParentNameById(parentTableName,parentIdColumn,domainId);
			 
			 String childValue = apdao.getHierarchyByParentId(childTablesName,childIdColumn,domainId);
			 
			 searchResult += parentName+"#"+childValue;
			 
		 }
		 
		 if(currentLevel.equals("cb6"))
		 {			
			 parentTableName = "SubDomainLevel6Model";
			 parentIdColumn = "sub_domain_level_6_id";
			 
			 childTablesName = "SubDomainLevel7Model";
			 childIdColumn = "sub_domain_level_7_id";
			 
			 String parentName = apdao.getParentNameById(parentTableName,parentIdColumn,domainId);
			 
			 String childValue = apdao.getHierarchyByParentId(childTablesName,childIdColumn,domainId);
			 
			 searchResult += parentName+"#"+childValue;
			 
		 }
		 
		 if(currentLevel.equals("cb7"))
		 {			
			 parentTableName = "SubDomainLevel7Model";
			 parentIdColumn = "sub_domain_level_7_id";
			 
			 childTablesName = "SubDomainLevel8Model";
			 childIdColumn = "sub_domain_level_8_id";
			 
			 String parentName = apdao.getParentNameById(parentTableName,parentIdColumn,domainId);
			 
			 String childValue = apdao.getHierarchyByParentId(childTablesName,childIdColumn,domainId);
			 
			 searchResult += parentName+"#"+childValue;
			 
		 }
		 
		 if(currentLevel.equals("cb8"))
		 {			
			 parentTableName = "SubDomainLevel8Model";
			 parentIdColumn = "sub_domain_level_8_id";
			 
			 childTablesName = "SubDomainLevel9Model";
			 childIdColumn = "sub_domain_level_9_id";
			 
			 String parentName = apdao.getParentNameById(parentTableName,parentIdColumn,domainId);
			 
			 String childValue = apdao.getHierarchyByParentId(childTablesName,childIdColumn,domainId);
			 
			 searchResult += parentName+"#"+childValue;
			 
		 }
		 
		 if(currentLevel.equals("cb9"))
		 {			
			parentTableName = "SubDomainLevel9Model";
			parentIdColumn = "sub_domain_level_9_id";

			childTablesName = "SubDomainLevel10Model";
			childIdColumn = "sub_domain_level_10_id";

			String parentName = apdao.getParentNameById(parentTableName,
					parentIdColumn, domainId);

			String childValue = apdao.getHierarchyByParentId(childTablesName,
					childIdColumn, domainId);

			searchResult += parentName + "#" + childValue;
			 
		 }
		 
		 return searchResult;
	 }
	 
	 public String getSelectedNames(String selectedList)
	 {
		 String searchResult = "";
		 
		 String selectValue[] = selectedList.split(",");
		 String selectStr = "";
		 for(int i=1;i<selectValue.length;i++)
		 {
			 if(!selectValue[i].equals("Select Sub-Domain"))
			 {
				 String tablesName = "SubDomainLevel"+i+"Model";
				 String columnName = "sub_domain_level_"+i+"_id";
				 String name = apdao.getdomainName(tablesName,columnName,selectValue[i]);
				
				 selectStr = selectStr + name + ","; 				
			 }			 			 
		 }
		 
		 selectStr = selectStr.substring(0, selectStr.length()-1);
		 
		 searchResult = selectValue[0]+","+selectStr;
		 
		 return searchResult;
		 
	 }
	 
	 public int getUtiliserId(UtiliserProfileModel utiProfile)
	 {
		 return apdao.getUtiliserId(utiProfile);
	 }
	 
	 public String getfinancialSubType()
	 {
		 return apdao.getfinancialSubType();
	 }
	 
	 public String getHierarchyComponents(String hierarchyType)
	 {
		 return apdao.getHierarchyComponents(hierarchyType);
	 }
	 
	 public String getPlanNameAsPerPlanDomain(String planDomain)
	 { 
		 List<String>  planName = apdao.getPlanNameAsPerPlanDomain(planDomain);
		 
		 if(planName.size()>0)
		 {
			 String searchResult = "";
			 for(int i=0;i<planName.size();i++)
			 {
				 searchResult += planName.get(i) + ",";
			 }
			 searchResult = searchResult.substring(0,searchResult.length()-1);
			 
			 return searchResult;
		 }
		 else
		 {
			 return "";
		 }
		 
	 }
	 
	 public String saveNotifyDetails(KycNotificationModel notifyModel)
	 {
		 return apdao.saveNotifyDetails(notifyModel);
	 }
	 
	 public List<IndSignupModel> getIndividualDetailsViaKycid(String searchedValue)
	 {
		 return apdao.getIndividualDetailsViaKycid(searchedValue);
	 }
	    
	 public List<IndSignupModel> getIndividualDetailsViaPlan(String searchedValue)
	 {
		 return apdao.getIndividualDetailsViaPlan(searchedValue);
	 }
	    
	 public List<IndSignupModel> getIndividualDetailsViaName(String searchedValue)
	 {
		 return apdao.getIndividualDetailsViaName(searchedValue);
	 }
	 
	 public List<EmailSentStatusModel> getPendingEmail()
	 {
		 return apdao.getPendingEmail();
	 }
	    
	 public List<SmsSentStatusModel> getPendingSMS()
	 {
		 return apdao.getPendingSMS();
	 }
	 
	 public List<EmailSentStatusModel> getPendingEmailDetails(int email_uniqueId)
	 {
		 return apdao.getPendingEmailDetails(email_uniqueId);
	 }
		
	 public List<SmsSentStatusModel> getPendingSMSDetails(int sms_uniqueId)
	 {
		 return apdao.getPendingSMSDetails(sms_uniqueId);
	 }
	 
	 public String getUpdateEmailDetails(int email_uniqueId)
	 {
		 return apdao.getUpdateEmailDetails(email_uniqueId);
	 }
	    
	 public String getUpdateSmSDetails(int sms_uniqueId)
	 {
		 return apdao.getUpdateSmSDetails(sms_uniqueId);
	 }
	 
	 public String getdtailsviakycId(String kycId)
	 {
		 List<RegHistoryModel> regModel = apdao.getdtailsviakycId(kycId);
		 
		 if(regModel.size()>0)
		 {
			 String regData = "";
			 for(int i=0;i<regModel.size();i++)
			 {
				 regData += regModel.get(i).getFirst_name()+","+regModel.get(i).getMiddle_name()+","+regModel.get(i).getLast_name()+","+regModel.get(i).getEmail_id()+","+regModel.get(i).getGender();
			 }
			 System.out.println("data="+regData);
			 return regData;
		 }
		 else
		 {
			 return "";
		 }		
	 }
	 
	 public String getPatternName(int patternId)
	 {
		 return apdao.getPatternName(patternId);
	 }
	 
	 public String getPassword(int utiUnqId)
	 {
		 return apdao.getPassword(utiUnqId);
	 }
	 
	 public String getPlanDomain(int utiUnqId)
	 {
		 return apdao.getPlanDomain(utiUnqId);
	 }
	 
	  public String getUtilizerPlanName(int utiUnqId)
	  {
		  return apdao.getUtilizerPlanName(utiUnqId);
	  }
	  
	  public String getUtilizerDomain(int utiUnqId)
	  {
		  return apdao.getUtilizerDomain(utiUnqId);
	  }
	  
	  public Date getUtiRegDate(int utiUnqId)
	  {
		  return apdao.getUtiRegDate(utiUnqId);
	  }
	   	
		public String getUtiProfilePic(int utiUnqId)
		{
			return apdao.getUtiProfilePic(utiUnqId);
		}
		
		public String saveUtilizerPlanDetails(UtilizerPlanDetailsModel utiPlanModel)
		{
			return apdao.saveUtilizerPlanDetails(utiPlanModel);
		}
		
		public String saveUtilizerProfilePhoto(UtilizerProfilePhotoModel utiProfilePhoto)
		{
			return apdao.saveUtilizerProfilePhoto(utiProfilePhoto);
		}
		
		public String getPatternDetailAsperPatterName(String patternName)
		{
			String patternDetails = apdao.getPatternDetailAsperPatterName(patternName);
			
			String pattrnArray[] = patternDetails.split("/");
			
			String profilePattern = "";
			String docPattern = "";
			
			for(int i=0;i<pattrnArray.length;i++)
			{
				if(pattrnArray[i].startsWith("PROFILE"))
				{
					profilePattern = pattrnArray[i].split(":")[1];
				}
				else
				{
					String pattNam1[] = pattrnArray[i].split(":");
					String pattNam2[] = pattNam1[1].split(",");
					for(int j=0;j<pattNam2.length;j++)
					{
						docPattern += pattNam1[0]+"-"+pattNam2[j]+",";
					}
				}
			 }
			
			String finalResult = "";
			
			if(profilePattern.length()>1)
			{				
				finalResult += profilePattern;
			}
			if(docPattern.length()>1)
			{	
				//docPattern = docPattern.substring(0, docPattern.length()-1);
				finalResult += ","+docPattern;
			}
			
			
			return finalResult;
		}
		
		public String updatePattern(String existPatternName,String newpattern,String patternDetails)
		{
			PatternDetailsModel patternModel = new PatternDetailsModel();
			
	        String profilePattern = "PROFILE:";
	        
			String kycPattern = "KYC:";
			String academicPattern = "ACADEMIC:";
			String financialPattern = "FINANCIAL:";		
			String employmentPattern = "EMPLOYEMENT:";
			
			patternDetails = patternDetails.substring(0, patternDetails.length()-1);
	        String patternArray[] = patternDetails.split(",");
	       
	        for(int i=0;i<patternArray.length;i++)
	        {
	        	String arr1[] = patternArray[i].split("-");
	        	
	        	if(arr1[0].equals("EMPLOYEMENT"))
	        	{
	        		employmentPattern += arr1[1]+",";
	        	}
	        	
	        	else if(arr1[0].equals("KYC"))
	        	{
	        		kycPattern += arr1[1]+",";
	        	}
	        	
	        	else if(arr1[0].equals("ACADEMIC"))
	        	{
	        		academicPattern += arr1[1]+",";
	        	}
	        	
	        	else if(arr1[0].equals("FINANCIAL"))
	        	{
	        		financialPattern += arr1[1]+",";
	        	}        	
	        	else
	        	{
	        		profilePattern += patternArray[i]+",";
	        	}
	        	        	                                    
	        }
	        
	        String patternLogic = "";
	                                                             
	        if(!profilePattern.equals("PROFILE:"))
	        {
	        	profilePattern = profilePattern.substring(0, profilePattern.length()-1);
	        	patternLogic += profilePattern + "/";
	        }
	        
	        if(!kycPattern.equals("KYC:"))
	        {
	        	kycPattern = kycPattern.substring(0, kycPattern.length()-1);
	        	patternLogic += kycPattern + "/";
	        }
	        
	        if(!academicPattern.equals("ACADEMIC:"))
	        {
	        	academicPattern = academicPattern.substring(0, academicPattern.length()-1);
	        	patternLogic += academicPattern + "/";
	        }
	        
	        if(!financialPattern.equals("FINANCIAL:"))
	        {
	        	financialPattern = financialPattern.substring(0, financialPattern.length()-1);
	        	patternLogic += financialPattern + "/";
	        }
	        
	        if(!employmentPattern.equals("EMPLOYEMENT:"))
	        {
	        	employmentPattern = employmentPattern.substring(0, employmentPattern.length()-1);
	        	patternLogic += employmentPattern + "/";
	        }
	        
	        patternLogic = patternLogic.substring(0, patternLogic.length()-1);
	        
	        int pattern_id = apdao.findPatternId(existPatternName);
	        
	        patternModel.setPattern_id(pattern_id);
	        patternModel.setPattern_name(newpattern);
	        patternModel.setCr_date(new Date());
	        patternModel.setPattern_logic(patternLogic);
	        patternModel.setCr_by("Admin");
			
			return apdao.savePattern(patternModel);
		}
		
		public int getLastUtilizerIndId()
		{
			return apdao.getLastUtilizerIndId();
		}
		
		public String deleteAcademicHierarchy(String hierarchyName)
		{
			return apdao.deleteAcademicHierarchy(hierarchyName);  
		}
		
		public String updateUtilizerDetails(UtiliserProfileModel utiProfile)
		{
			return apdao.updateUtilizerDetails(utiProfile);
		}
		
		public String updateUtiliserProfilePhoto(UtiliserProfileModel utiProfile)
		{
			return apdao.updateUtiliserProfilePhoto(utiProfile);
		}
		
		public String getClenseeYearList()
		{
			List<Integer> yearList = apdao.getClenseeYearList();
			String allyearList = "";
			
			if(yearList.size()>0)
			{
				for(int i=0;i<yearList.size();i++)
				{
					allyearList += yearList.get(i) + ",";
				}
				if(allyearList.length()>0)
				{
					allyearList = allyearList.substring(0, allyearList.length()-1);
				}
			}
			else
			{
				allyearList = "No Data";
			}
			
			return allyearList;
		}
		
		
		
		public List<ContactUsModel> getAllContactUsDetails(String todaysDateFormat)
		{
			return apdao.getAllContactUsDetails(todaysDateFormat);
		}
		
		public String getContactMssageByConId(int contactId)
		{
			return apdao.getContactMssageByConId(contactId);
		}
		
		public List<ContactUsModel> getSearchContactUsDetails(String searchType,String searchValue)
		{
			return apdao.getSearchContactUsDetails(searchType,searchValue);
		}
}
