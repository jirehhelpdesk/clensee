package com.kyc.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kyc.dao.UtiliserDao;
import com.kyc.model.AccessDetailsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.DocumentModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.UtiliserDetailsModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtiliserUpdateModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;

@Service("utiservice")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UtiliserServiceImpl implements UtiliserService{
	
	@Autowired
	private UtiliserDao utidao;
	
	
	public String utiliserAuthenticate(String kycid,String password)
	{
	   return utidao.utiliserAuthenticate(kycid,password);	
	}
	
	public String getUtilizerFullName(int uti_Indid)
	{
		return utidao.getUtilizerFullName(uti_Indid);
	}
	
	public String findAwaitedDocDetails(String uti_kycid)
	{
		
		List<AccessDetailsModel> lsdata = utidao.findAwaitedDocDetails(uti_kycid);
				
		List<String> accdata = new ArrayList<String>();
		
		for(int i=0;i<lsdata.size();i++)
		{
			accdata.add(lsdata.get(i).getAccess_giver());
			accdata.add(lsdata.get(i).getGiver_name());
			SimpleDateFormat dateformatJava = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String cr_date = dateformatJava.format(lsdata.get(i).getAccess_given_date());
			accdata.add(cr_date);
			accdata.add(lsdata.get(i).getAccess_description());
		    
		}
		return accdata.toString();
				
	}
	
	public List<AccessDetailsModel> findAwaitedDocDetailsForUtilizer(String uti_kycid)
	{		
		return utidao.findAwaitedDocDetails(uti_kycid);
	}
	
	public String findViewedDocDetails(String uti_kycid)
	{		
		List<AccessDetailsModel> lsdata = utidao.findViewedDocDetails(uti_kycid);
        List<String> accdata = new ArrayList<String>();
		
		for(int i=0;i<lsdata.size();i++)
		{
			accdata.add(lsdata.get(i).getAccess_giver());
			accdata.add(lsdata.get(i).getGiver_name());
			SimpleDateFormat dateformatJava = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String cr_date = dateformatJava.format(lsdata.get(i).getAccess_given_date());
			accdata.add(cr_date);
			accdata.add(lsdata.get(i).getAccess_description());
		    
		}
		return accdata.toString();
	}
	
	public List<AccessDetailsModel> viewedDocDtailsForUtilizer(String uti_kycid)
	{		
		return utidao.findViewedDocDetails(uti_kycid);        
	}

	public List<AccessDetailsModel> viewedSearchedDocDetailsForUtilizer(String uti_kycid,String searchedIndex)
	{
		return utidao.viewedSearchedDocDetailsForUtilizer(uti_kycid,searchedIndex);		
	}
	
	
	public String acceptDocs(String ind_Id,Date date)
	{
		return utidao.acceptDocs(ind_Id,date);
	}
	
	public String findAccessPattern(String kycid,String date)
	{
		int ind_id = utidao.findid(kycid);
		String patternNames = "";
		
		List<String> accDocumentList =  utidao.findAccessPattern(kycid,date);
		String docNames = "";
		
		String accessDetailsInString = ""; 
		for(int d=0;d<accDocumentList.size();d++)
		{
			accessDetailsInString += accDocumentList.get(0);
		}		
		String accessDetailsArray[] = accessDetailsInString.split("#");
		
		if(accessDetailsArray[0].contains("PROFILE"))
		{
			String parrernArr1[] = accessDetailsArray[0].split("/");
			
			for(int i=1;i<parrernArr1.length;i++)		
			{
			   String parrernArr2[] = parrernArr1[i].split(":");
			   String parrernArr3[] = parrernArr2[1].split(",");
			   for(int j=0;j<parrernArr3.length;j++)
			   {
				  patternNames += parrernArr3[j]+",";
				  String DocumentName = "";
				  
				  if(parrernArr3[j].equals("Employment"))
				  {
					  DocumentName = utidao.findDocs("PaySlip",ind_id);
				  }
				  else
				  {
					  DocumentName = utidao.findDocs(parrernArr3[j],ind_id);	
				  }	
								  
				  if(!DocumentName.equals(""))
				  {
					  docNames = docNames + DocumentName + ",";
				  }			  
			   }
			}			
		}
		
		
		if(!accessDetailsArray[0].contains("PROFILE"))
		{
			String parrernArr1[] = accessDetailsArray[0].split("/");
			
			for(int i=0;i<parrernArr1.length;i++)		
			{
			   String parrernArr2[] = parrernArr1[i].split(":");
			   String parrernArr3[] = parrernArr2[1].split(",");
			   for(int j=0;j<parrernArr3.length;j++)
			   {
				  String DocumentName = "";
				  
				  patternNames += parrernArr3[j]+",";
				  
				  if(parrernArr3[j].equals("Employment"))
				  {
					  DocumentName = utidao.findDocs("PaySlip",ind_id);
				  }
				  else
				  {
					  DocumentName = utidao.findDocs(parrernArr3[j],ind_id);	
				  }				  		  
				  if(!DocumentName.equals(""))
				  {
					  docNames = docNames + DocumentName + ",";
				  }			  
			   }
			}			
		}
		
	    if(accessDetailsArray.length>1)
	    {
	    	String attachedDocArray[] = accessDetailsArray[1].split("/");
	    	for(int a=0;a<attachedDocArray.length;a++)
	    	{
	    		String attachedDocArray1[] = attachedDocArray[a].split(":");
	    		if(attachedDocArray1.length>1)
	    		{
	    			String attachedDocArra2[] = attachedDocArray1[1].split(",");		    		
		    		for(int b=0;b<attachedDocArra2.length;b++)
		    		{
		    		   docNames = docNames+attachedDocArra2[b]+",";
		    		}
	    		}	    		
	    	}
	    }		
	    		
		docNames = docNames.substring(0,docNames.length()-1);		
		patternNames = patternNames.substring(0, patternNames.length()-1);
		
		String finalDocNames = "";
		
		/* Remove Duplicacy */
		
		if(!docNames.equals(""))
		{
			String currentDocNameArray[] = docNames.split(",");				
			for(int i=0;i<currentDocNameArray.length;i++)
			{				
				String tempDocName = currentDocNameArray[i];				
				if(!finalDocNames.contains(currentDocNameArray[i]))
				{
					finalDocNames += tempDocName+",";
				}								
			}			
			finalDocNames = finalDocNames.substring(0,finalDocNames.length()-1);		
			
			System.out.println("finalDocNames - 1 - ="+finalDocNames);
		}

		/* End of Remove Duplicacy */
		
		/* Remove unwanted Doc  */
		
		if(!patternNames.contains("PaySlip"))
		{
			String excludDoc[] = finalDocNames.split(",");
			finalDocNames = "";
			for(int i=0;i<excludDoc.length;i++)
			{				
				if(!excludDoc[i].contains("PaySlip"))
				{
					finalDocNames += excludDoc[i]+ ",";
				}
			}			
			finalDocNames = finalDocNames.substring(0,finalDocNames.length()-1);			
		}
		
		/* End of Remove unwanted Doc  */
		
		return finalDocNames;
	}
	
	
	
	public String getprofileAccessDetails(String kycid,String date)
	{
        int ind_id = utidao.findid(kycid);
		
		List<String> accDocumentList =  utidao.findAccessPattern(kycid,date);
		String profileAccessDetails = "";
		
		String profileAccessDetailValue = "";
		
		String accessDetailsInString = ""; 
		
		for(int d=0;d<accDocumentList.size();d++)
		{
			accessDetailsInString += accDocumentList.get(0);
		}	
		
		String accessDetailsArray[] = accessDetailsInString.split("#");
		
		if(accessDetailsArray[0].contains("PROFILE"))
		{
			String parrernArr1[] = accessDetailsArray[0].split("/");	
			profileAccessDetails += parrernArr1[0];
			
			String profileAccessArray[] = profileAccessDetails.split(":");
			String profileAccessArray1[] = profileAccessArray[1].split(",");
			
			for(int p=0;p<profileAccessArray1.length;p++)
			{
				String profileAccessArray2[] = profileAccessArray1[p].split("-");
				
				if(profileAccessArray2[0].equals("REGISTRATION"))
				{
					String tableName = "RegHistoryModel";
					String column1 = "Email Id";
					String column2 = "Gender";
					String column3 = "Mobile No";
					
					if(profileAccessArray2[1].equals(column1))
					{
						String emailId = utidao.getRegistartionDetail("email_id",tableName,ind_id);
						if(emailId.length()>0)
						{
							profileAccessDetailValue += "Email Id"+"-->"+emailId+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column2))
					{
						String gender = utidao.getRegistartionDetail("gender",tableName,ind_id);
						if(gender.length()>0)
						{
							profileAccessDetailValue += "Gender"+"-->"+gender+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column3))
					{
						String mobile_no = utidao.getRegistartionDetail("mobile_no",tableName,ind_id);																		
						if(mobile_no.length()>0)
						{
							profileAccessDetailValue += "Mobile No"+"-->"+mobile_no+"##";
						}
					}
					
					
					
				}
				
				if(profileAccessArray2[0].equals("BASIC"))
				{
					String tableName = "BasicDetail";
					
					String column1 = "Alternate Email Id";
					String column2 = "Date of Birth";
					String column3 = "Residence Contact no";
					String column4 = "Office Contact no";
					String column5 = "Marital Status";
					String column6 = "Present Address";
					String column7 = "Permanent Address";
					
					if(profileAccessArray2[1].equals(column1))
					{
						String altEmailId = utidao.getBasicDetail("alternate_email",tableName,ind_id);
						if(altEmailId.length()>0)
						{
							profileAccessDetailValue += "Alternate Email Id"+"-->"+altEmailId+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column2))
					{
						String dob = utidao.getBasicDetail("date_of_birth",tableName,ind_id);
						if(dob.length()>0)
						{
							profileAccessDetailValue += "Dat of Birth"+"-->"+dob+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column3))
					{
						String rescontactNo = utidao.getBasicDetail("tell_residence",tableName,ind_id);
						if(rescontactNo.length()>0)
						{
							profileAccessDetailValue += "Residence Contact No"+"-->"+rescontactNo+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column4))
					{
						String offContactNo = utidao.getBasicDetail("tell_office",tableName,ind_id);
						if(offContactNo.length()>0)
						{
							profileAccessDetailValue += "Office Contact No"+"-->"+offContactNo+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column5))
					{
						String marital_status = utidao.getBasicDetail("marital_status",tableName,ind_id);
						if(marital_status.length()>0)
						{
							profileAccessDetailValue += "Marital Status"+"-->"+marital_status+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column6))
					{
						String pre_Address = utidao.getBasicDetail("present_address",tableName,ind_id);
						if(pre_Address.length()>0)
						{
							profileAccessDetailValue += "Present Address"+"-->"+pre_Address+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column7))
					{
						String per_Address = utidao.getBasicDetail("permanent_address",tableName,ind_id);
						if(per_Address.length()>0)
						{
							profileAccessDetailValue += "Permanent Address"+"-->"+per_Address+"##";
						}
					}	
				}
				
				if(profileAccessArray2[0].equals("FAMILY"))
				{
					String tableName = "FamilyDetail";
					
					String column1 = "Father Name";
					String column2 = "Mother Name";
					String column3 = "Sibling Name";
					String column4 = "Spouse Name";
					
					if(profileAccessArray2[1].equals(column1))
					{
						String fatherName = utidao.getFamilyDetail("father_name",tableName,ind_id);
						if(fatherName.length()>0)
						{
							profileAccessDetailValue += "Father Name"+"-->"+fatherName+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column2))
					{
						String motherName = utidao.getFamilyDetail("mother_name",tableName,ind_id);
						if(motherName.length()>0)
						{
							profileAccessDetailValue += "Mother Name"+"-->"+motherName+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column3))
					{
						String sibling_name = utidao.getFamilyDetail("sibling_name",tableName,ind_id);
						if(sibling_name.length()>0)
						{
							profileAccessDetailValue += "Sibling Name"+"-->"+sibling_name+"##";
						}
					}
					
					if(profileAccessArray2[1].equals(column4))
					{
						String spouse_name = utidao.getFamilyDetail("spouse_name",tableName,ind_id);
						if(spouse_name.length()>0)
						{
							profileAccessDetailValue += "Spouse Name"+"-->"+spouse_name+"##";
						}
					}
					
				}
				
			}   //End of For
			
			
			profileAccessDetailValue = profileAccessDetailValue.substring(0,profileAccessDetailValue.length()-2);
			
		}     // End of If
		
		else
		{
			profileAccessDetails += "No Data";
		}
		
		return profileAccessDetailValue;
		
	}
	
	
	
	
	public String findYear(String kycid)
	{
		return utidao.findYear(kycid);
	}
	
	public String findDirName(String docname)
	{
		return utidao.findDirName(docname);
	}
	
	public List<DocumentDetailModel> getHistory(String docname,int Ind_id)
	{
		return utidao.getHistory(docname,Ind_id);
	}
	
	public List<UtiliserProfileModel> getUtiBasicDetails(String utikycid)
	{
		return utidao.getUtiBasicDetails(utikycid);
	}
	
	
	public int findid(String kycid)
	{
		return utidao.findid(kycid);
	}
	
	public List<UtiliserDetailsModel> getUtiProfDetails(String utikycid)
	{
		return utidao.getUtiProfDetails(utikycid);
	}
	
	public int findUtiid(String utiKycid)
	{
		return utidao.findUtiid(utiKycid);
	}
	
	public String updateProfile(UtiliserProfileModel profile)
	{
		return utidao.updateProfile(profile);
	}
	
	public String updateProfDetails(UtiliserDetailsModel Prof)
	{
		return utidao.updateProfDetails(Prof);
	}
	
	public List<UtiliserUpdateModel> getUtiNotifications(String uti_kycid)
	{
		return utidao.getUtiNotifications(uti_kycid);
	}
	
	public String setNotificationdetails(UtiliserUpdateModel uti_update)
	{
		return utidao.setNotificationdetails(uti_update);
	}
	
	public String changePassword(String OldPassword, String Newpass, String kycid)
	{
		return utidao.changePassword(OldPassword,Newpass,kycid);
	}
	
	public String checkUtiPassword(String OldPassword,String newPassword,String kycid)
	{
		return utidao.checkUtiPassword(OldPassword,newPassword,kycid);
	}
	
	public int findUti_kycid(String utiKyc_id)
	{
		return utidao.findUti_kycid(utiKyc_id);
	}
	
	public String insertProfDetails(UtiliserDetailsModel Prof)
	{
		return utidao.insertProfDetails(Prof);
	}
	
	public List<DocumentModel> getDocActivity(String kycid)
	{
		return utidao.getDocActivity(kycid);
	}
	
	public String getUtilizerKycId(String kycid)
	{
		return utidao.getUtilizerKycId(kycid);
	}
	
	public int getUtiIndId(String kycId)
	{
		return utidao.getUtiIndId(kycId);
	}
	
	public UtiliserProfileModel getLatestProfilePic(int uti_Indid)
	{
		return utidao.getLatestProfilePic(uti_Indid);
	}
	
	public String getUtiAboutUsInfo(int uti_Indid)
	{
		return utidao.getUtiAboutUsInfo(uti_Indid);
	}
	
	public int getUtiDetailsId(int uti_Indid)
	{
		return utidao.getUtiDetailsId(uti_Indid);
	}
	
	public UtiliserDetailsModel getUtilizerDetail(int uti_Indid)
	{
		return utidao.getUtilizerDetail(uti_Indid);
	}
	
	public String saveActivityDetails(UtilizerActivityModel actModel)
	{
		return utidao.saveActivityDetails(actModel);
	}
	
	public List<UtilizerActivityModel> getUtilizerActivity(int uti_indid)
	{
		return utidao.getUtilizerActivity(uti_indid);
	}
	
	public List<KycNotificationModel> getNotificationDetails(int uti_indid)
	{
		return utidao.getNotificationDetails(uti_indid);
	}
	
	public String getMessagefromNotifyId(int notifyId)
	{
		return utidao.getMessagefromNotifyId(notifyId);
	}
	
    public int viewedDocumentinthisMonth(String uti_kycid)
    {
    	return utidao.viewedDocumentinthisMonth(uti_kycid);
    }
	
	public int getviewedDocumentAsofNow(String uti_kycid)
	{
		return utidao.getviewedDocumentAsofNow(uti_kycid);
	}
	
	public List<AccessDetailsModel> getAccessNotificationDetails(String uti_kycid)
	{
		List<AccessDetailsModel> accesDetailsForNotify = utidao.getAccessNotificationDetails(uti_kycid);
		
		return accesDetailsForNotify;
	}
	
	public String getDocumentComponentValue(String docname,int ind_id)
	{
		return utidao.getDocumentComponentValue(docname,ind_id);
	}
	
	public String checkEmailIdAvailabilty(String requestedEmailId)
	{
		return utidao.checkEmailIdAvailabilty(requestedEmailId);
	}
	
	public String getRefIdViaEmailId(String emailId)
	{
		return utidao.getRefIdViaEmailId(emailId);
	}
	
	public String checkPasswordStatus(String reserVariable)
	{
		return utidao.checkPasswordStatus(reserVariable);
	}
	
	public int getindividualIndId(String uniqueVariable)
	{
		return utidao.getindividualIndId(uniqueVariable);
	}
	
	public String getDeleteUtiResetPaswordInfo(String uniqueVariable)
	{
		return utidao.getDeleteUtiResetPaswordInfo(uniqueVariable);
	}
	
	public String getChangeUtiPassword(UtiliserProfileModel utiModel)
	{
		return utidao.getChangeUtiPassword(utiModel);
	}
	
	public String migrateUtilizerPlanDetails(UtilizerPlanDetailsModel utiPlanModel)
	{
		return utidao.migrateUtilizerPlanDetails(utiPlanModel);
	}
	
	public String saveProfilePhotoFromUtilizer(UtilizerProfilePhotoModel profilePhotoModel)
	{
		return utidao.saveProfilePhotoFromUtilizer(profilePhotoModel);
	}	
	
	public String updateProfPhotoDetails(String documentName,int utiIndId)
	{
		return utidao.updateProfPhotoDetails(documentName,utiIndId);
	}
	
	public String getProfilePhotoHistory(int uti_ind_id)
	{
		return utidao.getProfilePhotoHistory(uti_ind_id);
	}
	
	public String updateProfilePic(String picName,int uti_ind_id)
	{
		return utidao.updateProfilePic(picName,uti_ind_id);
	}
	
	public String getListOfSenderKycId(String utiKycId)
	{
		List<String> listOfKYcId = utidao.getListOfSenderKycId(utiKycId);		
		String listOfKycId = "";
		
		if(listOfKYcId.size()>0)
		{
			for(int i=0;i<listOfKYcId.size();i++)
			{
				listOfKycId += listOfKYcId.get(i) + ",";
			}
			listOfKycId = listOfKycId.substring(0,listOfKycId.length()-1);
		}		
		return listOfKycId;
	}
	
	
	public int getUtilizerIdVaiUniqueId(String resetVariable)
	{
		return utidao.getUtilizerIdVaiUniqueId(resetVariable);
	}
	
	public String updateVisibilityUrl(int uti_Ind_id,String encryptedUrl)
	{
		return utidao.updateVisibilityUrl(uti_Ind_id,encryptedUrl);
	}
	
	
	public String getAccessPatternViaKycIds(String uti_kycid,String indKycId,String accordname)
	{
		String accessKycDocuments = "";
		String tempKycDocuments = "";
		String accessPattern = utidao.getAccesPatternViaKycId(uti_kycid,indKycId);
		String defaultKycDocs = utidao.getdefaultKycDocuments("KYC");
		
		if(accessPattern.length()>0)
		{
			String accessPatternArray[] = accessPattern.split("#");
			for(int i=0;i<accessPatternArray.length;i++)
			{
				String accessArray[] = accessPatternArray[i].split("/");
				for(int j=0;j<accessArray.length;j++)
				{
					String eachAccessArray[] = accessArray[j].split(":");
					if(eachAccessArray[0].equals("KYC"))
					{
						if(eachAccessArray.length==2)
						{
							tempKycDocuments += eachAccessArray[1] + ",";
						}						
					}
				}
			}
			
			
			if(tempKycDocuments.length()>0)
			{
				tempKycDocuments = tempKycDocuments.substring(0,tempKycDocuments.length()-1);				
				String defaultKycArray[] = defaultKycDocs.split(",");
				for(int k=0;k<defaultKycArray.length;k++)
				{
					if(tempKycDocuments.contains(defaultKycArray[k]))
					{
						accessKycDocuments += defaultKycArray[k] + ",";
					}
				}
			}
			
			
			if(accessKycDocuments.length()>0)
			{
				accessKycDocuments = accessKycDocuments.substring(0,accessKycDocuments.length()-1);
			}
		
		}
		else
		{
			accessKycDocuments = "No Data";
		}
		
		
		return accessKycDocuments;
	}
	
	
	
	public String getAcademicStatusPattern(String uti_kycid,String indKycId,String accordname)
	{
		String accessAcademicDocuments = "";
		String tempDocuments = "";
		String accessPattern = utidao.getAccesPatternViaKycId(uti_kycid,indKycId);
		String defaultDocs = utidao.getdefaultKycDocuments("ACADEMIC");
		
		String extraDocsAdded = "";
		
		if(accessPattern.length()>0)
		{
			String accessPatternArray[] = accessPattern.split("#");
			for(int i=0;i<accessPatternArray.length;i++)
			{
				String accessArray[] = accessPatternArray[i].split("/");
				for(int j=0;j<accessArray.length;j++)
				{
					String eachAccessArray[] = accessArray[j].split(":");
					if(eachAccessArray[0].equals("ACADEMIC"))
					{
						if(eachAccessArray.length==2)
						{
							if(eachAccessArray[1].contains("_"))
							{
								extraDocsAdded += eachAccessArray[1]+",";								
							}
							tempDocuments += eachAccessArray[1] + ",";
						}
						
					}
				}
			}
			
			if(tempDocuments.length()>0)
			{
				tempDocuments = tempDocuments.substring(0,tempDocuments.length()-1);				
				String defaultDocArray[] = defaultDocs.split(",");
				for(int k=0;k<defaultDocArray.length;k++)
				{
					
					if(tempDocuments.contains(defaultDocArray[k]))
					{
						accessAcademicDocuments += defaultDocArray[k] + ",";
					}
				}
			}
			
			if(accessAcademicDocuments.length()>0)
			{
				accessAcademicDocuments = accessAcademicDocuments.substring(0,accessAcademicDocuments.length()-1);
				if(extraDocsAdded.length()>0)
				{
					extraDocsAdded = extraDocsAdded.substring(0,extraDocsAdded.length()-1);			
					accessAcademicDocuments += "ACADEMICSEPERATOR" + extraDocsAdded;					
				}							
			}
			
		}
		else
		{
			accessAcademicDocuments = "No Data";
		}
		
		
		return accessAcademicDocuments;
	}
	
	
	
	public String getDocumentNamesWithDetails(String indKycId,String academicAccordian,String extraAddedDocuments)
	{
		return utidao.getDocumentNamesWithDetails(indKycId,academicAccordian,extraAddedDocuments);
	}
	
	
	public String getEmployeeStatusPattern(String uti_kycid,String indKycId)
	{
		String accessEmployeeDocuments = "";
		String tempDocuments = "";
		String accessPattern = utidao.getAccesPatternViaKycId(uti_kycid,indKycId);
		
		String extraDocsAdded = "";
		
		if(accessPattern.length()>0)
		{
			String accessPatternArray[] = accessPattern.split("#");
			for(int i=0;i<accessPatternArray.length;i++)
			{
				String accessArray[] = accessPatternArray[i].split("/");
				for(int j=0;j<accessArray.length;j++)
				{
					String eachAccessArray[] = accessArray[j].split(":");
						
						if(eachAccessArray[0].equals("EMPLOYEMENT"))
							{
								if(eachAccessArray[1].contains("_"))
								{
									extraDocsAdded += eachAccessArray[1]+",";
								}
								
								tempDocuments += eachAccessArray[1] + ",";								
							}
					    else if(eachAccessArray[0].equals("Employment"))
							{
					    	    if(accessArray[j].contains("Employment:"))
					    	    {
					    	    	if(eachAccessArray[1].contains("_"))
									{
										extraDocsAdded += eachAccessArray[1]+",";
									}
									
									tempDocuments += eachAccessArray[1] + ",";
					    	    }								
							}
						else
							{
								
							}
				}
			}	
		}
		
		if(tempDocuments.length()==0)
		{
			tempDocuments = "No Status";
		}
		
		if(extraDocsAdded.length()==0)
		{
			extraDocsAdded = "Not Added";
		}
		
		accessEmployeeDocuments = tempDocuments+"EMPSEP"+extraDocsAdded;
		
		return accessEmployeeDocuments;
	}
	
	public String getEmployeeDetailsWithDocName(int ind_id,String getEmployeeStatus)
	{
		return utidao.getEmployeeDetailsWithDocName(ind_id,getEmployeeStatus);
	}
	
	public String getFinancialStatusPattern(String uti_kycid,String indKycId,String accordname)
	{
		String accessFinancialDocuments = "";
		String tempDocuments = "";
		String accessPattern = utidao.getAccesPatternViaKycId(uti_kycid,indKycId);
		
		String extraDocsAdded = "";
		
		if(accessPattern.length()>0)
		{
			String accessPatternArray[] = accessPattern.split("#");
			for(int i=0;i<accessPatternArray.length;i++)
			{
				String accessArray[] = accessPatternArray[i].split("/");
				for(int j=0;j<accessArray.length;j++)
				{
					String eachAccessArray[] = accessArray[j].split(":");
						
						if(eachAccessArray[0].equals("FINANCIAL"))
							{
								if(eachAccessArray[1].contains("_"))
								{
									extraDocsAdded += eachAccessArray[1]+",";
								}
								
								tempDocuments += eachAccessArray[1] + ",";								
							}
					    else if(eachAccessArray[0].equals("Financial"))
							{
						    	 if(eachAccessArray.length==2)
						    	 {
						    		 if(eachAccessArray[1].contains("_"))
										{
											extraDocsAdded += eachAccessArray[1]+",";
										}										 
						    	 }
								
								//tempDocuments += eachAccessArray[1] + ",";
							}
						else
							{
								
							}
				}
			}	
		}
		
		if(tempDocuments.length()==0)
		{
			tempDocuments = "No Status";
		}
		
		if(extraDocsAdded.length()==0)
		{
			extraDocsAdded = "Not Added";
		}
		
		accessFinancialDocuments = tempDocuments+"FINSEP"+extraDocsAdded;
		
		return accessFinancialDocuments;
	}
	
	public String getFinancialDetailsBythePattern(int ind_id,String getFinancialStatusAsPerPattern)
	{
		return utidao.getFinancialDetailsBythePattern(ind_id,getFinancialStatusAsPerPattern);
	}
	
    public int getNoOfAwaitedRequets(String uti_kycid)
    {
    	return utidao.getNoOfAwaitedRequets(uti_kycid);
    }
	
	public int getNoOfRecivedDocuments(String uti_kycid)
	{
		return utidao.getNoOfRecivedDocuments(uti_kycid);
	}

}
