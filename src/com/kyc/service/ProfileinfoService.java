package com.kyc.service;

import java.util.Date;
import java.util.List;

import com.kyc.model.BasicDetail;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.AccessDetailsModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.IndSignupModel;
import com.kyc.model.IndividualPlanDetailModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.Matrial;
import com.kyc.model.ProfilePictureModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.ResetPasswordModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.Socialacc;
import com.kyc.model.Summarydetail;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPasswordRequestModel;

public interface ProfileinfoService {
	
	public String addFamilyDetail(FamilyDetail FamilyDetail);
	
	public String profileupdate(Profileupdatemodel Profileupdatemodel);
	public String addBasicDetail(BasicDetail BasicDetail);
	public String visibilityBasicDetail(BasicDetail BasicDetail);
	
	public String addMatrial(Matrial matrial);
	public String addSocialacc(Socialacc socialacc);
	public String addSummarydetail(Summarydetail summarydetail);
	public FamilyDetail getfamilyvalue(int fam_details_id);
	public  List<IndSignupModel> searchprofile(IndSignupModel searchprofile);
	public  List<IndSignupModel> searchhomepage(IndSignupModel searchhomepage);
	
	public  String searchhomepageforUti(String searchvalue);
	
	public  List<IndSignupModel> searchprofilekycid(IndSignupModel searchprofile);
	public  List<IndSignupModel> viewmore(IndSignupModel viewmore);
	public String getkycid(int ind_kyc_id); 
	public String getfirstname(String ind_kyc_id1);
	
	public  String getprofilepic(IndSignupModel searchprofile);
	
	public  String getsearchedPresentAddress(IndSignupModel searchprofile);
	
	public  List<Profileupdatemodel> profilenotification(Profileupdatemodel profilenotification);
	public  String profilevisitors(Profileupdatemodel profilenotification);
	public  List<Profileupdatemodel> profileactivities(Profileupdatemodel profilenotification);
	public  String Getacademicdetail(DocumentDetailModel docdetails);     
	public  List<DocumentDetailModel> Getemployeedetail(DocumentDetailModel docdetails);
	public Socialacc getsocialaccvalue(int social_det_id);
	public Summarydetail getSummaryvalue(int sum_det_id);
	public Matrial getMatrimonyvalue(int matrimony_details_id);
	public   BasicDetail getBasicDetailvalue(int BasicDetailid);
	public  Socialacc getsocialdetails(String ind_kyc_id);
	public  IndSignupModel getregistrationdetails(String ind_kyc_id);
	public String Getregid(Summarydetail summary);
	public String Getmatid(Matrial matrial);
	public void setregid(String ind_kyc_id);
	public String Getbasicid(BasicDetail BasicDetail);
	public String Getfamid(String ind_kyc_id);
	public String GetSocialaccid(String ind_kyc_id);
	public String Getsocialaccid(String ind_kyc_id);
	public String geteditfamilyvalue(FamilyDetail FamilyDetail);
	public String editSocialacc(Socialacc socialacc);
	public String editBasicDetail(BasicDetail BasicDetail);
	public String editmatrial(Matrial matrial);
	public String editsummary(Summarydetail summary);
	public List<Summarydetail> getsummarydetail(Summarydetail summary);
	public String getdescription(String ind_kyc_id);

	public int findInd_id(String ind_kyc_id);
	
	public String getBasicHistory(int ind_id);
	
	public String getFamilyHistory(int ind_id);
	
	public String getsocialHistory(int ind_id);
	
	public String getIndNotify(String kyc_id);
	
	public String getSum_det_id(String id);
	
	public String saveAboutUsInfo(Summarydetail sumDetail);
	
	public String getAboutUsInfo(Summarydetail sumDetail);
	
	public String getPicName(int ind_id);
	
	public String saveProfilePicInfo(ProfilePictureModel profileModel);
	
	public String getLatestPicName(int ind_id);
	
	public ProfilePictureModel getPrifilePicInfo(int ind_id);
	
	public int getPrifilePicId(String picName);
	
	public String GetacademicPostDate(DocumentDetailModel docdetails);
	
	public String GetEmpPostDate(DocumentDetailModel docdetails);
	
	public String getPermanentAddress(int id);
	
	public String getPresentAddress(int id);
	
	public String getRegVisisbility(int ind_id);
	
	public String getBasicVisisbility(int ind_id);
	
	public String getFamilyVisisbility(int ind_id);
	
	public String getSocialVisisbility(int ind_id);
	
	public String updateRegVisibility(int ind_id,String val);
	
	public String setBasicVisibility(int ind_id,String val);
		
	public String setFamilyVisibility(int ind_id,String val);
	
	public String setSocialVisibility(int ind_id,String val);
	
	public String getBasicDetail(int ind_id);
	
	public String getFamilyDetail(int ind_id);
	
	public String getsocialdetails(int ind_id);
	
	public String getProfilePhoto(int ind_id);
	
	public String updateProfilePic(String picName,int ind_id);
	
	public String getIndividualDetails(int ind_id);
	
	public int getYear(int ind_id);
	
	public String getSearchKycInd(String searchedValue);
	
	public String getSearchedAboutUsInfo(int ind_id);
	
	public String getValidateEmailId(String emailId);
	
	public String getViewedStatus(String kycId,String ownKycId);
	
	public String getIndividualIdVaiEmailId(String emailId);
	
	public List<KycNotificationModel> getNotificationDetails(int ind_id);
	
	public String getMessagefromNotifyId(int notifyId);
	
	public int findUtiid(String uti_kycid);
	
	public String saveActivityDetails(UtilizerActivityModel actModel);
	
	public List<AccessDetailsModel> getuti2IndNotification(String kyc_id);
	
	public String getValidateMobileNo(String mobileNo);
	
	public String getValidateEmailIdForgotPassword(String emailId);
	
	public String getUpdateEmailId(int ind_id,String EmailId);
	
	public String getKycIdOfAccessGiven(String IndKycId);
	
	public String getYearFormIndId(String OtherIndId);
	
	public String saveResetPasswordModel(ResetPasswordModel resetPW);
	
	public String getCurrentPlanName(int ownInd_id);
	
	public Date getRegisterDate(int ownInd_id);
	
	public String getEntryInPlanDetails(IndividualPlanDetailModel indPlanModel);
	
	public String getvalidateRegistrationEmailId(String emailId,int ind_id);
	
	public String getvalidateRegistrationMobileNo(String mobileNo,int ind_id);
	
	public String setEmailStatusReport(EmailSentStatusModel emailReport);
	
	public String setSmsStatusReport(SmsSentStatusModel smsReport);
	
	public String migratePlanAfterOneYear(int ownInd_id);
	
	public String saveUtiResetPasswordModel(UtilizerPasswordRequestModel resetPW);
	
	public  List<RegHistoryModel>  getIndividualDetailsviaIndId(int indId);
	
	public String getUpdateRegTableWithLastRecord(RegHistoryModel indHistory,String fulleName);
	
	public String getUtilizerAccessForIndividual(String individualKycId,String utilizerKycId);
	
	public List<BasicDetail> getLatestBasicDetailsInfo(int basicId);
	
	public int getIndividualIdVaiVisibility(String resetVariable);
	
	public String updateVisibilityUrl(int ind_id,String resetVariable,String encryptedUrl);
	
	public String getLatestProfilePitureName(int individualId);
	
	public String saveClenseeContactDetails(ContactUsModel contactus);
	
	public int getIndIdViaEmailId(String emailId);
	
	
	public String sendMailForPlanChange(String reciverName,String reciverEmailId,String requestedPlanName);
	
	public String sendEmailForInvoice(int day,int month,int year,String uniqueInvoiceId,int randomNumber,String paymentAmount,String currentPlanName,String requestedPlanName,String reciverName,String reciverEmailId);
	
	
	public String utilizerPlanChangeMail(String reciverName,String reciverEmailId,String requestedPlanName);
	
	public String utilizerInvoiceMail(int day,int month,int year,String uniqueInvoiceId,String paymentAmount,String currentPlanName,String reciverName,String reciverEmailId,String requestedPlanName);
	
}  

