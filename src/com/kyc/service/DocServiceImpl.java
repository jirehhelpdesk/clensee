package com.kyc.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.kyc.bean.DocumentHierarchyBean;
import com.kyc.bean.IndSignupBean;
import com.kyc.dao.DocDao;
import com.kyc.model.AcademicFileUploadModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.DocumentModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.SalarySlipModel;
import com.kyc.model.SscDocModel;



@Service("docservice")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DocServiceImpl implements DocService{
	
	@Autowired
	private DocDao docdao;
	
	@Autowired
	private IndSignupService signupService;
	
	@Autowired
	private ProfileinfoService profileinfoService;

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public String uploadSSC(SscDocModel sscdocmodel)
	{
		return docdao.uploadSSC(sscdocmodel);
	}
	
	public int getIndIdviaDocId(int doc_id)
	{
		return docdao.getIndIdviaDocId(doc_id);
	}
	
	public String insertKYCDocDetailsInRegn(DocumentDetailModel docdetailmodel)
	{
	    return docdao.insertKYCDocDetailsInRegn(docdetailmodel);	
	}
	
	public String getKycIdViaEmailId(String emailId)
	{
		return docdao.getKycIdViaEmailId(emailId);
	}
	
	public String uploadDocument(DocumentModel docmodel)
	{
		return docdao.uploadDocument(docmodel);
	}
	
	public DocumentModel getDetail(String kycid) {
		return docdao.getDetail(kycid);
	}
	
	
	public List<DocumentModel> listDocuments() {
		return docdao.listDocuments();
	}
	
	public String getAcademicComp()
	{
		return docdao.getAcademicComp();
	}
	
	public String getAcademicComp2()
	{
		return docdao.getAcademicComp2();
	}
	
	public String getAcademicComp3()
	{
		return docdao.getAcademicComp3();
	}
	
	public String getFinancialComp()
	{
		return docdao.getFinancialComp();
	}
	
	public String getEmployementComp()
	{
		return docdao.getEmployementComp();
	}
	
	public String getBusinessComp()
	{
		return docdao.getBusinessComp();
	}
	
	public String uploadDocumentDetail(DocumentDetailModel docdetailmodel)
	{
		return docdao.uploadDocumentDetail(docdetailmodel);
	}
	
	public String uploadotherDocumentDetail(DocumentDetailModel docdetailmodel)
	{
		return docdao.uploadotherDocumentDetail(docdetailmodel);
	}
	
	public String updateDocumentDetail(DocumentDetailModel docdetailmodel)
	{
		return docdao.updateDocumentDetail(docdetailmodel);
	}
	
	public String getDocsDataSSC(DocumentDetailModel docdetailmodel)
	{
		return docdao.getDocsDataSSC(docdetailmodel);
	}
	
	public String getDocsDataInter(DocumentDetailModel docdetailmodel)
	{
		return docdao.getDocsDataInter(docdetailmodel);
	}
	
	public String getDocsDataGraduate(DocumentDetailModel docdetailmodel)
	{
		return docdao.getDocsDataGraduate(docdetailmodel);
	}

	public String getDocsDataFinancial(DocumentDetailModel docdetailmodel)
	{
		return docdao.getDocsDataFinancial(docdetailmodel);
	}
	
	public String getDocsDataEmployement(DocumentDetailModel docdetailmodel)
	{
		return docdao.getDocsDataEmployement(docdetailmodel);
	}
	
	public String getDocsDataBusiness(DocumentDetailModel docdetailmodel)
	{
		return docdao.getDocsDataBusiness(docdetailmodel);
	}
	
	public List<String> getAcademicDocComp()
	{
		return docdao.getAcademicDocComp();
	}
	
	
	public String getDocsVeiwOrNew(DocumentHierarchyBean docbean)
	{		
		return docdao.getDocsVeiwOrNew(docbean);
	}
	
	public List<DocumentDetailModel> getHistory(DocumentDetailModel docdetails,HttpSession session)
	{
		List<DocumentDetailModel> lst = docdao.getHistory(docdetails,session);
		
		List<Integer> docidlist = new ArrayList<Integer>();
		Iterator<DocumentDetailModel> iter = lst.iterator();
		
		while(iter.hasNext())
		{
		  docidlist.add(iter.next().getDoc_id());
		}
		
		session.setAttribute("docidlist", docidlist);
		
		return lst;
	}
	
	public String getViewMore(DocumentDetailModel docdetails)
	{
		return docdao.getViewMore(docdetails);
	}
	
	public String getDocName(DocumentDetailModel docdetails)
	{
		return docdao.getDocName(docdetails);
	}
	
	public String getOtherDocsVeiwOrNew(DocumentHierarchyBean docbean)
	{
		return docdao.getOtherDocsVeiwOrNew(docbean);
	}
	
	public List<Integer> getDocIds(DocumentDetailModel docdetails)
	{
		return docdao.getDocIds(docdetails);
	}
	
	public String getKYCComp()
	{
		return docdao.getKYCComp();
	}
	
	public String getstatus(DocumentDetailModel docdetails)
	{
		return docdao.getstatus(docdetails);
	}
	
	public String getHierarchy(DocumentDetailModel docdetails)
	{
		return docdao.getHierarchy(docdetails);
	}
	
	public String getfinDocName(DocumentDetailModel docdetailmodel)
	{
		return docdao.getfinDocName(docdetailmodel);
	}
	
	public String showUserProfile(HttpServletRequest request,HttpSession session,
			@ModelAttribute("command") IndSignupBean signupBean,BindingResult result) throws Exception
	{
        IndSignupModel individual = new IndSignupModel();
		
		individual.setKycid(signupBean.getKycid());
		individual.setPassword(signupBean.getPassword());
		
		String ind_id=signupService.getInd_id(individual);
		
		session.setAttribute("Ind_id", ind_id);
		session.setAttribute("Kyc_id", signupBean.getKycid());
				
		if ("success".equals(signupService.checkIndividual(individual)))
		{
			
			return "individualProfile";
		} else {

			return "errorLogin";
		}
	}
	
	public List<DocumentDetailModel> getfinHistory(DocumentDetailModel docdetails,HttpSession session)
	{
		return docdao.getfinHistory(docdetails,session);
	}
	
	public List<DocumentDetailModel> searchfinancial(String doc_year,String docType,String docMode,int user)
	{
		return docdao.searchfinancial(doc_year,docType,docMode,user);
	}
	
	public List<DocumentDetailModel> backFinancial(String backValue,int user)
	{
		return docdao.backFinancial(backValue,user); 
	}
	
	public String getKYCdatas(DocumentDetailModel docdetails)
	{
		return docdao.getKYCdatas(docdetails);
	}
		
	public List<DocumentDetailModel> getEmpHistory(DocumentDetailModel docdetails)
	{
		return docdao.getEmpHistory(docdetails);
	}
	
	public String getrecentDocName(DocumentDetailModel docdetails)
	{
		return docdao.getrecentDocName(docdetails);
	}
	
	public String findLatestDocName(int id,String docname)
	{
		return docdao.findLatestDocName(id,docname);
	}
	
	public Integer findYear(String Ind_id)
	{
		return docdao.findYear(Ind_id);
	}
	
	public String getAcademicStatus(int id)
	{
		return docdao.getAcademicStatus(id);
	}
	
	public String getOldComp(int indId)
	{
		return docdao.getOldComp(indId);
	}
	
	public String getIndName(int id)
	{
		return docdao.getIndName(id);
	}
	
	public String setPannelDocDetails(DocumentDetailModel docmodel)
	{
		return docdao.setPannelDocDetails(docmodel);
	}
	
	public String getId(int id)
	{
		return docdao.getId(id);
	}
	
	public String updateKycId(String latest_kyc,int id)
	{
		return docdao.updateKycId(latest_kyc,id);
	}
	
	public String getFinancialDoc(int id)
	{
		return docdao.getFinancialDoc(id);
	}
	
	public String getEmpHierarchy()
	{
		return docdao.getEmpHierarchy();
	}
	
	public String getEmpPanelStatusfromEmpDetails(int ind_id)
	{
		return docdao.getEmpPanelStatusfromEmpDetails(ind_id);
	}
	
	public String getEmpPanelStatus(int ind_id)
	{
		return docdao.getEmpPanelStatus(ind_id);
	}
	
	public String getEmpDetails(String panel,int ind_id)
	{
		return docdao.getEmpDetails(panel,ind_id);
	}
	
	public String getEmpInfo(int ind_id)
	{
		return docdao.getEmpInfo(ind_id);
	}
	
	public String getEmpDocNames(int Individual_Id,String orgName)
	{
		return docdao.getEmpDocNames(Individual_Id,orgName);
	}
	
	public String getEmpHistoryDetails(int doc_id)
	{
		return docdao.getEmpHistoryDetails(doc_id);
	}
	
	public String savePaySlips(SalarySlipModel salSlipModel)
	{
		return docdao.savePaySlips(salSlipModel);
	}
	
	public String getSalarySlipsNameAsPerUpload(SalarySlipModel salSlipModel)
	{
        List<String> paySlipList =  docdao.getSalarySlipsNameAsPerUpload(salSlipModel);
		
		String payslipName = "";
		if(paySlipList.size()>0)
		{
		    for(int i=0;i<paySlipList.size();i++)
		    {
		    	payslipName +=  paySlipList.get(i) + ",";
		    }
		    
		    payslipName = payslipName.substring(0,payslipName.length()-1);
		    
		    return payslipName;
		}
		else
		{
		    return "No Data";
		}
	}
	
	public String getPaySlipsName(SalarySlipModel salSlipModel)
	{
		
		List<String> paySlipList =  docdao.getPaySlipsName(salSlipModel);
		
		String payslipName = "";
		if(paySlipList.size()>0)
		{
		    for(int i=0;i<paySlipList.size();i++)
		    {
		    	payslipName +=  paySlipList.get(i) + ",";
		    }
		    
		    payslipName = payslipName.substring(0,payslipName.length()-1);
		    
		    return payslipName;
		}
		else
		{
		    return "No Data";
		}
		
	}
	
	public String getPaySlipsMonthYear(SalarySlipModel salSlipModel)
	{
		List<String> payListMonthYear = docdao.getPaySlipsMonthYear(salSlipModel);
		
		String paySlipMonthYear = "";
		
		if(payListMonthYear.size()>0)
		{
			for(int i=0;i<payListMonthYear.size();i++)
			{
				paySlipMonthYear += payListMonthYear.get(i) + ",";
			}
			
			return paySlipMonthYear;
		}
		else
		{
			return "No Data";
		}
	}
	
	public String getCreatedDateOfPaySlips(String payslipsName,SalarySlipModel salSlipModel)
	{		
		return docdao.getCreatedDateOfPaySlips(payslipsName,salSlipModel);		
	}
	
	public String getFinancialData(int id)
	{
		List<String> finData = docdao.getFinancialData(id);		
		String financialData = "";		
		String resultData = "";		
		if(finData.size()>0)		
			{
					for(int i=0;i<finData.size();i++)
					{
						financialData += finData.get(i) + "/";
					}
									
					financialData = financialData.substring(0, financialData.length()-1);
					String financialDataArray[] = financialData.split("/");
					
					String finalData = financialData;
					String finalDataArray[] = financialData.split("/");
					
				      /* Removing of Repeate value */
				
					    int i=0;
					    
						for(int j=0;j<financialDataArray.length;j++)
						{
							String eachFinancialDataArray[] = financialDataArray[j].split("&&");
							String compareValue = eachFinancialDataArray[0]+"&&"+eachFinancialDataArray[1]+"&&"+eachFinancialDataArray[2];
																				
							if(!resultData.contains(compareValue))
							{
								resultData += financialDataArray[j] + "/";																					
							}									
														
						}						
						
						
			}		
		
	      return resultData;		
	}
	
	
	
	public String getFinancialDataviaDocName(int i,String docName)
	{
		return docdao.getFinancialDataviaDocName(i,docName);
	}
	
	public String uploadEmpDocumentDetail(DocumentDetailModel docdetailmodel)
	{
		return docdao.uploadEmpDocumentDetail(docdetailmodel);
	}
	
	public String getFinancialDocName(int i,String finName)
	{
		return docdao.getFinancialDocName(i,finName);
	}
	
	public List<DocumentDetailModel> getFinancialHistory(int i,String docName)
	{
		return docdao.getFinancialHistory(i,docName);
	}
	
	public String getMonthSalarySlip(int i1,String orgName)
	{
		List<String> monthList = docdao.getMonthSalarySlip(i1,orgName);
		String monthYear = "";
		
		if(monthList.size()>0)
		{
		    for(int i=0;i<monthList.size();i++)
		    {
		    	monthYear = monthYear + monthList.get(i) + ",";
		    }
		    
		    return monthYear;
		}
		else
		{
	     	return "No Data";
		}
	}
	
	public String getEmpDataofAllCompany(int Individual_Id)
	{
		List<String> empDataList = docdao.getEmpDataofAllCompany(Individual_Id);
		String status = "";
		
		if(empDataList.size()>0)
		{			
			 if(empDataList.toString().contains("Presently Working"))
			 {
				 status += "Yes";
			 }
			 else
			 {
				 status += "No";
			 }
		}
		else
		{
			status += "No";
		}
		
		return status;
	}
	
	public String getKycRegistrationStatus(String kycId)
	{		
		List<String> kycList =  docdao.getKycRegistrationStatus(kycId);
		
		String kycStatus = "";
		if(kycList.size()>0)
		{			
			kycStatus = "Yes";			
		}
		else
		{
			List<String> kycListFromDoc =  docdao.getKycIdFromDocumentDetails(kycId);
			if(kycListFromDoc.size()>0)
			{				
				kycStatus = "Yes";				
			}
			else
			{
				kycStatus = "No";
			}	
		}		
		return kycStatus;
	}
	
	
	
	public String academicFileUpload(AcademicFileUploadModel academicFileDetails)
	{
		return docdao.academicFileUpload(academicFileDetails);
	}
	
	public String getAcademicUploadedDocName(int kyc_ind_id)
	{
		List<String> filenameList = docdao.getAcademicUploadedDocName(kyc_ind_id);		
		
		String fileName = "";
		
		if(filenameList.size()>0)
		{
			for(int i=0;i<filenameList.size();i++)
			{
				fileName += filenameList.get(i) + ",";
			}		
			fileName = fileName.substring(0, fileName.length()-1);
							
			return fileName;
		}
		else
		{
			return "No Data";
		}
		
		
	}
	
	public float getAcademicUploadedDocSize()
	{
		List<Float>  filesizeList = docdao.getAcademicUploadedDocSize();
        float filesize=(float) 0.0;	
		
        if(filesizeList.size()>0)
        {
        	for(int i=0;i<filesizeList.size();i++)
    		{
    			filesize += filesizeList.get(i);
    		}		
    		return filesize;
        }
        else
        {
        	return filesize;
        }
	}
	
	public String getAcademicUploadedDocDes()
	{
      List<String> fileDescList = docdao.getAcademicUploadedDocDes();		
		
		String fileDesc = "";
		if(fileDescList.size()>0)
		{
			for(int i=0;i<fileDescList.size();i++)
			{
				fileDesc += fileDescList.get(i) + ",";
			}				
			fileDesc = fileDesc.substring(0, fileDesc.length()-1);						
			return fileDesc;
		}
		else
		{
			return fileDesc += "Nothing Updated";
		}
	}
	
	public String getDeleteAllFromUploadTableDetails(int kyc_ind_id)
	{
		return docdao.getDeleteAllFromUploadTableDetails(kyc_ind_id);
	}
	
	public String getoldDocumentList(int ind_id,String docType)
	{
		return docdao.getoldDocumentList(ind_id,docType);
	}
	
    public String getAcademicDataById(int doc_id)
    {
    	return docdao.getAcademicDataById(doc_id);
    }
	
	public String getAcademicDocNameById(int doc_id)
	{
		return docdao.getAcademicDocNameById(doc_id);
	}
	
	public String employerStatus(int ind_id)
	{		
		String statusResult = "";
		String companyName = docdao.getEmpPanelStatus(ind_id);
		
		if(companyName.length()>0)
		{
			String companyNameArray[] = companyName.split("/");
			String latestCompanyData = "";
			for(int i=0;i<companyNameArray.length;i++)
			{
				latestCompanyData += docdao.getEmployementLatestData(ind_id,companyNameArray[i]) + "/";
			}
							
			if(!latestCompanyData.equals("No Record"))
			{
				if(latestCompanyData.contains("No Data"))
				{
					statusResult += "No Data";
				}
			}
		}
		else
		{
			statusResult += "No Record";
		}
				
		return statusResult;
	}
	
	public String getKYCLatestDocsData(int ind_id)
	{
		return docdao.getKYCLatestDocsData(ind_id);
	}
	
	public String getKYCLatestDocName(int ind_id)
	{
		return docdao.getKYCLatestDocName(ind_id);
	}
	
	public String uploadKYCDocumentDetail(DocumentDetailModel docdetailmodel)
	{
		return docdao.uploadKYCDocumentDetail(docdetailmodel);
	}
	
	public String getLatestKYCPannel(int i)
	{
		return docdao.getLatestKYCPannel(i);
	}
	
	public String setKYCPannelDocDetails(DocumentDetailModel docmodel)
	{
		return docdao.setKYCPannelDocDetails(docmodel);
	}
	
	public String getcheckKYCIdExistOrNot(int ownIndId,String KycId)
	{
		String status = "";
		boolean condition = false;
		
		List<String> allListKycDocsData = docdao.getAllListKYCDocsData(ownIndId);
		if(allListKycDocsData.size()>0)
		{
			for(int i=0;i<allListKycDocsData.size();i++)
			{
				if(allListKycDocsData.get(i).contains(KycId))
				{
					status = "Yes";
					condition = true;
				}
			}
			if(condition==false)
			{
				status = "No";
			}
		}
		else
		{
			status = "No";
		}
		return status;
	}
	
	public String getAcademicFaultFileName(int ownIndId)
	{
		List<String> fileNames = docdao.getAcademicFaultFileName(ownIndId); 
		String fileNameList = "";
		
		if(fileNames.size()>0)
		{
			for(int i=0;i<fileNames.size();i++)
			{
				fileNameList += fileNames.get(i) + ",";
			}
			fileNameList = fileNameList.substring(0,fileNameList.length()-1);
		}
		
		return fileNameList;
	}
	
	public String deleteAcademicFaultFileDetails(int ownIndId)
	{
		return docdao.deleteAcademicFaultFileDetails(ownIndId);  
	}
	
	public String getEmployeeDetailsEachCompnay(String panelNames,int individualId)
	{
		return docdao.getEmployeeDetailsEachCompnay(panelNames,individualId);
	}
	
	public String getEmpDetailsExceptThisCompany(String panel1,int individualId,String companyName)
	{
		return docdao.getEmpDetailsExceptThisCompany(panel1,individualId,companyName);
	}
	
	public String checkOrgNameExistOrNot(int ind_id,String orgName)
	{
		return docdao.checkOrgNameExistOrNot(ind_id,orgName);
	}
	
	public String getEmpPanelStatusAsofNow(int i)
	{
		return docdao.getEmpPanelStatusAsofNow(i);
	}
	
	public String getPannelWithId(int ind_id,String old_selComp)
	{
		return docdao.getPannelWithId(ind_id,old_selComp);
	}
	
	public String getKYCdataAsUtilizerDetails(DocumentDetailModel docdetails)
	{
		return docdao.getKYCdataAsUtilizerDetails(docdetails);
	}
	
	public String getLatestKycBackSideDoc(String ind_id,String kycDocType)
	{
		return docdao.getLatestKycBackSideDoc(ind_id,kycDocType);
	}
	
	public String getFinancialTypeFromHierarchy()
	{
		return docdao.getFinancialTypeFromHierarchy();
	}
		
	public  Date getHitoryDateOfThisId(int doc_id)
	{
		return docdao.getHitoryDateOfThisId(doc_id);
	}
	
	public Date getHistoryCreatedDatebyDocId(int doc_id)
	{
		return docdao.getHitoryDateOfThisId(doc_id);
	}
	
	
}
