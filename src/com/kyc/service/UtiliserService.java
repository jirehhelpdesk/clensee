package com.kyc.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.kyc.model.AccessDetailsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.DocumentModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.UtiliserDetailsModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtiliserUpdateModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;

public interface UtiliserService {

	public int findid(String kycid);
	
	public String utiliserAuthenticate(String kycid,String password);
	
	public String findAwaitedDocDetails(String uti_kycid);
	
	public String findViewedDocDetails(String uti_kycid);
	
	public String acceptDocs(String ind_Id,Date date);
	
	public String findAccessPattern(String kycid,String date);
	
	public List<AccessDetailsModel> findAwaitedDocDetailsForUtilizer(String uti_kycid);
	
	public String getprofileAccessDetails(String kycid,String date);
	
	public List<AccessDetailsModel> viewedDocDtailsForUtilizer(String uti_kycid);
	
	public List<AccessDetailsModel> viewedSearchedDocDetailsForUtilizer(String uti_kycid,String searchedIndex);
	
	public String findYear(String kycid);
	
	public String findDirName(String docname);
	
	public List<DocumentDetailModel> getHistory(String docname,int Ind_id);
	
	public List<UtiliserProfileModel> getUtiBasicDetails(String utikycid);
	
	public List<UtiliserDetailsModel> getUtiProfDetails(String utikycid);
	
	public int findUtiid(String utiKycid);
	
	public String updateProfile(UtiliserProfileModel profile);	
	
	public String updateProfDetails(UtiliserDetailsModel Prof);
	
	public List<UtiliserUpdateModel> getUtiNotifications(String uti_kycid);
	
	public String setNotificationdetails(UtiliserUpdateModel uti_update);
	
	public String changePassword(String OldPassword, String Newpass, String kycid);
	
	public String checkUtiPassword(String OldPassword,String newPassword,String kycid);
	
	public int findUti_kycid(String utiKyc_id);
	
	public String insertProfDetails(UtiliserDetailsModel Prof);
	
	public List<DocumentModel> getDocActivity(String kycid);
	
	public String getUtilizerKycId(String kycid);
	
	public int getUtiIndId(String kycId);
	
	public UtiliserProfileModel getLatestProfilePic(int uti_Indid);
	
	public UtiliserDetailsModel getUtilizerDetail(int uti_Indid);
	
	public String getUtiAboutUsInfo(int uti_Indid);
	
	public int getUtiDetailsId(int uti_Indid);
	
	public String saveActivityDetails(UtilizerActivityModel actModel);
	
	public List<UtilizerActivityModel> getUtilizerActivity(int uti_indid);
	
	public List<KycNotificationModel> getNotificationDetails(int uti_indid);
	
	public String getMessagefromNotifyId(int notifyId);
	
	public int viewedDocumentinthisMonth(String uti_kycid);
	
	public int getviewedDocumentAsofNow(String uti_kycid);
	
	public List<AccessDetailsModel> getAccessNotificationDetails(String uti_kycid);
	
	public String getDocumentComponentValue(String docname,int ind_id);
	
	public String checkEmailIdAvailabilty(String requestedEmailId);
	
	public String getRefIdViaEmailId(String emailId);
	
	public String checkPasswordStatus(String reserVariable);
	
	public int getindividualIndId(String uniqueVariable);
	
	public String getDeleteUtiResetPaswordInfo(String uniqueVariable);
	
	public String getChangeUtiPassword(UtiliserProfileModel utiModel);
	
	public String migrateUtilizerPlanDetails(UtilizerPlanDetailsModel utiPlanModel);
	
	public String saveProfilePhotoFromUtilizer(UtilizerProfilePhotoModel profilePhotoModel);
	
	public String updateProfPhotoDetails(String documentName,int utiIndId);
	
	public String getProfilePhotoHistory(int uti_ind_id);
	
	public String updateProfilePic(String picName,int uti_ind_id);
	
	public String getListOfSenderKycId(String utiKycId);
	
	public int getUtilizerIdVaiUniqueId(String resetVariable);
	
	public String updateVisibilityUrl(int uti_Ind_id,String encryptedUrl);
	
	public String getAccessPatternViaKycIds(String uti_kycid,String indKycId,String accordname);
	
	public String getAcademicStatusPattern(String uti_kycid,String indKycId,String accordname);
	
	public String getDocumentNamesWithDetails(String indKycId,String academicAccordian,String extraAddedDocuments);
	
	public String getEmployeeStatusPattern(String uti_kycid,String indKycId);	
	
	public String getEmployeeDetailsWithDocName(int ind_id,String getEmployeeStatus);
	
	public String getFinancialStatusPattern(String uti_kycid,String indKycId,String accordname);
	
	public String getFinancialDetailsBythePattern(int ind_id,String getFinancialStatusAsPerPattern);
	
	public String getUtilizerFullName(int uti_Indid);
	
	public int getNoOfAwaitedRequets(String uti_kycid);
	
	public int getNoOfRecivedDocuments(String uti_kycid);
}
