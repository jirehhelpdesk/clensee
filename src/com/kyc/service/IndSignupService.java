package com.kyc.service;


import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.kyc.model.IndSignupModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.VisibilityModel;

/**
 * 
 *  
 */
public interface IndSignupService {
	
	public int getLastIndId();
	
	public String addIndividual(IndSignupModel individual);

	public String checkIndividual(IndSignupModel individual) throws Exception;
	
	public String getInd_id(IndSignupModel individual);
	public List<IndSignupModel> getInd_idmax(IndSignupModel individual);
	public String getcheckpassword(IndSignupModel indsignupmodel);
	
	public String savestatusreg(IndSignupModel indsignupmodel);
	public String getchangepassword(IndSignupModel indsignupmodel);
	public String editIndividual(RegHistoryModel indHistory);
	
	public  String reghistory(int ind_id);
		
	public IndSignupModel getname(int reg_id);
	
	public String getUtilizerName(int utiIndId);
	
	public void getname(IndSignupModel indSignupModel);

	public String saveRegHistory(RegHistoryModel regmodel);	
	
	public RegHistoryModel getRegistrationDetails(int ind_id);
	
	public String findyear(String kycId);
	
	public String getVisibility(int ind_id);
	
	public String saveVisibilitySetting(VisibilityModel visibility);
	
	public String getActiveStatus(String kycId);
	
	public String getnamefromRegHistory(int ind_id);
	
	public String checkStatus(String kycId);
	
	public int getindividualIndId(String uniqueVariable);
	
	public RegHistoryModel getLatestRegData(int ind_id);
	
	public String getCheckStatus(String reserVariable);
	
	public String getDeleteResetPaswordInfo(String uniqueVariable);
	
	public String getActiveValidityStatus(String uniqueVariable);	
	
	public String getUpdateAppliedCodeStatus(String uniqueVariable);
	
	public String getKYCId(String uniqueId);
	
	public String statusOfIndividualForLogin(IndSignupModel individual);
	
	public String getKYCIDFromCookiesValue(String indId);
}
