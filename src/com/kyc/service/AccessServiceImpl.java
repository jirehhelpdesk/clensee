package com.kyc.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.bean.DocumentDetailsBean;
import com.kyc.dao.AccessDao;
import com.kyc.model.AccessDetailsModel;
import com.kyc.model.BasicDetail;
import com.kyc.model.CodesDetailsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.IndSignupModel;
import com.kyc.model.NonKycUserCodeDetailsModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.UtiliserProfileModel;


@Service("accservice")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AccessServiceImpl implements AccessService{
	
	@Autowired
	private AccessDao accdao;
	
	

	public String findUser(IndSignupModel codedetail)
	{
				
		List<IndSignupModel> indList =  accdao.findUser(codedetail);						
		List<String> indStringList = new ArrayList<String>();
		String searchResult = "";
		
		for(int i=0;i<indList.size();i++)
		{
			
			int ind_id = indList.get(i).getInd_id();
			int year = accdao.findyear(" ", ind_id);
			String cr_year = Integer.toString(year);
			
			indStringList.add(cr_year);
			indStringList.add(Integer.toString(ind_id));
			indStringList.add(accdao.getLatestProfilePicture(ind_id));			
			indStringList.add(indList.get(i).getFirstname());
			indStringList.add(indList.get(i).getMiddlename());
			indStringList.add(indList.get(i).getLastname());
			
			indStringList.add(indList.get(i).getKycid());
			indStringList.add(indList.get(i).getEmailid());		
			
		}
		
		for(int j=0;j<indStringList.size();j++)
		{
			searchResult += indStringList.get(j) + ",";
		}
		
		if(searchResult.length()>0)
		{
			searchResult = searchResult.substring(0, searchResult.length()-1);
		}
		
		
		return searchResult;
	}
	
	public List<String> findDocumentforKYC(HttpServletRequest request)
	{
		return accdao.findDocumentforKYC(request);
	}
	
	public String checkIndAccessStatus(String viewedkycid,String ownKycId)
	{
		return accdao.checkIndAccessStatus(viewedkycid,ownKycId);
	}
	
	public List<String> findDocumentforAcademic(HttpServletRequest request)
	{
		return accdao.findDocumentforAcademic(request);
	}
	
	public List<String> findDocumentforEmployement(HttpServletRequest request)
	{
		return accdao.findDocumentforEmployement(request);	
	}
	
	public List<String> findDocument(HttpServletRequest request)
	{
		return accdao.findDocument(request);
	}
	
	public List<String> findRecipentDetail(HttpServletRequest request)
	{
		return accdao.findRecipentDetail(request);
	}
	
	public String findCrName(String kycid)
	{
		return accdao.findCrName(kycid);
	}
	
	public List<String> findAsperType(IndSignupModel codedetail)
	{
		return accdao.findAsperType(codedetail);
	}
	
	public String saveCodedetails(CodesDetailsModel codemodel)
	{
		return accdao.saveCodedetails(codemodel);
	}
	
	
	public List<IndSignupModel> viewUser(IndSignupModel individual)
	{
		return accdao.viewUser(individual);
									
	}
	
	public int findInd_Id(String kycid)
	{
		return accdao.findInd_Id(kycid);
	}
	
	public String findDocView(String docname)
	{
		return accdao.findDocView(docname);
	}
	
	
	public List<CodesDetailsModel> findShareedCode(IndSignupModel individual)
	{
		String kycid=individual.getKycid(); 
		List<CodesDetailsModel> applyList  = new  ArrayList<CodesDetailsModel>(); 
		
		applyList = accdao.findApplyedCode(kycid); 
		String str = "";
		return applyList;		
	}
	
	
	public String servicePatternforNonKycUsr(int non_kyc_code)
	{
		StringBuilder docs = new StringBuilder();
		String docsdata = accdao.findCodePatternForNonUser(non_kyc_code);						
		String components[] = docsdata.split("/");
										
		for(int i=0;i<components.length;i++)
		{
			String labelWthDoc[] = components[i].split(":");
			for(int j=1;j<labelWthDoc.length;j++)
			{
				String doc[] = labelWthDoc[j].split(",");
				for(int k=0;k<doc.length;k++)
				{
				    docs.append(doc[k]+"&");
				}
			}
		}
		return docs.toString();
	}
	
	public String servicePattern(int code) 
	{
		StringBuilder docs = new StringBuilder();
		String docsdata = accdao.findCodePattern(code);						
		String components[] = docsdata.split("/");
										
		for(int i=0;i<components.length;i++)
		{
			String labelWthDoc[] = components[i].split(":");
			for(int j=1;j<labelWthDoc.length;j++)
			{
				String doc[] = labelWthDoc[j].split(",");
				for(int k=0;k<doc.length;k++)
				{
				    docs.append(doc[k]+"&");
				}
			}
		}
		return docs.toString();
	}
	
	public int findid(String kycid)
	{
		return accdao.findid(kycid);
	}
	
	public int findyear(String docname,int ind_id)
	{
		return accdao.findyear(docname,ind_id);
	}
	
	public String getDomainHir(String domtype)
	{
		return accdao.getDomainHir(domtype);
	}
	
	public String getDomain()
	{
		return accdao.getDomain();
	}
	
	public String saveAccessDetails(AccessDetailsModel accdetail)
	{
		return accdao.saveAccessDetails(accdetail);
	}
	
	public String findUtiKycId(String ptname)
	{
		List<UtiliserProfileModel> utiliserList =  accdao.findUtiKycId(ptname);
		
		List<String> utiliserDataList = new ArrayList<String>();
		
		String searchResult = "";
		
		if(utiliserList.size()>0)
		{
			for(int ulist=0;ulist<utiliserList.size();ulist++)
			{
				int id = utiliserList.get(ulist).getUti_ind_id();
				String utiId = Integer.toString(id);
				
				int p_id = utiliserList.get(ulist).getPattern_id();
				String pattern_id = Integer.toString(p_id);
				
				utiliserDataList.add(utiId);
				utiliserDataList.add(utiliserList.get(ulist).getUti_profile_pic());	
				utiliserDataList.add(utiliserList.get(ulist).getUti_first_name()); 
				utiliserDataList.add(utiliserList.get(ulist).getUti_middle_name());
				utiliserDataList.add(utiliserList.get(ulist).getUti_last_name());
				utiliserDataList.add(utiliserList.get(ulist).getUti_office_Address());	
				utiliserDataList.add(pattern_id);
			}
			
			for(int udata=0;udata<utiliserDataList.size();udata++)
			{
				searchResult += utiliserDataList.get(udata) + "SEPSEP";
			}
			searchResult = searchResult.substring(0, searchResult.length()-6);
		}
		else			
		{
			searchResult += "No Data";
		}
		
		return searchResult;
	}
	
	public String findIndKycID(int acc_giver)
	{
		return accdao.findIndKycID(acc_giver);
	}
	
	public int getPatternId(String acc_reciver)
	{
		return accdao.getPatternId(acc_reciver);
	}
	
	public String getPatternLogic(int prtn_id)
	{
		return accdao.getPatternLogic(prtn_id);
	}
	
	public List<CodesDetailsModel> getAccessDetails(String ownId)
	{
		return accdao.getAccessDetails(ownId);
	}
	
	public List<CodesDetailsModel> getIndAccessDetails(String ownId)
	{
		return accdao.getIndAccessDetails(ownId);
	}
	
	public List<CodesDetailsModel> findHisInd(String kycid)
	{
		return accdao.findHisInd(kycid);
	}
	
	public List<NonKycUserCodeDetailsModel> getCodeDetailsforNonKycUser(int ind_Id)
	{
		return accdao.getCodeDetailsforNonKycUser(ind_Id);
	}
	
	public List<AccessDetailsModel> findHisUti(String kycid)
	{
		return accdao.findHisUti(kycid);
	}
	
	public String revokeUtiliser(String kycid,Date date,String access_status)
	{
		return accdao.revokeUtiliser(kycid,date,access_status);
	}
	
	public String findName(String acc_sender)
	{
		return accdao.findName(acc_sender);
	}
	
	public String getUti_HisDoclist(String kycid,String Date,int ind_Id)
	{
        int ind_id = ind_Id;
		
		List<String> accDocumentList =  accdao.findutiPattern(kycid,Date);
		String docNames = "";
		
	
		String accessDetailsInString = ""; 
		for(int d=0;d<accDocumentList.size();d++)
		{
			accessDetailsInString += accDocumentList.get(0);
		}		
		String accessDetailsArray[] = accessDetailsInString.split("#");
				
		String parrernArr1[] = accessDetailsArray[0].split("/");
				
		for(int i=0;i<parrernArr1.length;i++)		
		{
			
		   String parrernArr2[] = parrernArr1[i].split(":");
		   String parrernArr3[] = parrernArr2[1].split(",");
		   
		   for(int j=0;j<parrernArr3.length;j++)
		   {
			  String DocumentName = accdao.findDocs(parrernArr3[j],ind_id);			  
			  if(!DocumentName.equals(""))
			  {
				  docNames = docNames + DocumentName + ",";
			  }			  
		   }
		}
		
	    if(accessDetailsArray.length>1)
	    {
	    	String attachedDocArray[] = accessDetailsArray[1].split("/");
	    	for(int a=0;a<attachedDocArray.length;a++)
	    	{
	    		String attachedDocArray1[] = attachedDocArray[a].split(":");
	    		if(attachedDocArray1.length>1)
	    		{
	    			String attachedDocArra2[] = attachedDocArray1[1].split(",");		    		
		    		for(int b=0;b<attachedDocArra2.length;b++)
		    		{
		    		   docNames = docNames+attachedDocArra2[b]+",";
		    		}
	    		}	    		
	    	}
	    }		
	    		
		docNames = docNames.substring(0,docNames.length()-1);
			
		return docNames;
	}
	public String getRecevierStatus(String receiver,String sender)
	{
		return accdao.getRecevierStatus(receiver,sender);
	}
	
	public String findDocType(String docname,int ind_id)
	{
		return accdao.findDocType(docname,ind_id); 
	}
	
	public String getProfilePic(int kycInd_id)
	{
		return accdao.getProfilePic(kycInd_id);
	}
	
	public String getFirstName(int kycInd_id)
	{
		return accdao.getFirstName(kycInd_id);
	}
	
	public String getUtiAccessDetails(String ownId)
	{
		List<AccessDetailsModel> lsdata  = accdao.getUtiAccessDetails(ownId);		
		List<String> listString = new ArrayList<String>();
		
		String utiliserAccessDetails = "";
		
		if(lsdata.size()>0)
		{
			for(int i=0;i<lsdata.size();i++)
			{
				
				listString.add(lsdata.get(i).getAccess_taker());
				listString.add(accdao.getUtiliserName(lsdata.get(i).getAccess_taker()));
				
				SimpleDateFormat dateformatJava = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String cr_date = dateformatJava.format(lsdata.get(i).getAccess_given_date());
				listString.add(cr_date);
				
				listString.add(lsdata.get(i).getAccess_description());
				listString.add(lsdata.get(i).getStatus());
				
			}
			
			for(int j=0;j<listString.size();j++)
			{
				utiliserAccessDetails += listString.get(j) + "," ;
			}
			
			utiliserAccessDetails = utiliserAccessDetails.substring(0, utiliserAccessDetails.length()-1);
		}
		else
		{
			utiliserAccessDetails += "No Data";
		}
		
		return utiliserAccessDetails;
	}
	
	public String changeStatusOfUtiliser(String uti_unique_id,Date date,String access_status)
	{
		return accdao.changeStatusOfUtiliser(uti_unique_id,date,access_status);
	}
	
	public String getPatternfromId(int patternId)
	{
		return accdao.getPatternfromId(patternId);
	}
	
	public String getUtiliserAccessId(int utiliserId)
	{
		return accdao.getUtiliserAccessId(utiliserId); 
	}
	
	public String getValidateNonKycUserEmailId(String SharedEmailId)
	{
		//String validateStatus = "";
		
		//List<String> regtrationEmailIdList = accdao.getAllRegEmailId();
		//List<String> regHistoryEmailIdList = accdao.getAllRegHistoryEmailId();	
		String emailIdExistanceFromHistory = accdao.checkEmailIdExistOrNot(SharedEmailId);
		
		return emailIdExistanceFromHistory;
	}
	
	public int getLastGeneratedCodeId()
	{
		return accdao.getLastGeneratedCodeId();
	}
	
	public String getSenderName(int ind_id)
	{
		return accdao.getSenderName(ind_id);
	}
	
	public String getSaveNonKycCodeDetails(NonKycUserCodeDetailsModel nonUserModel)
	{
		return accdao.getSaveNonKycCodeDetails(nonUserModel);
	}
	
	public int orgApplyCodeCount(int code)
	{
		return accdao.orgApplyCodeCount(code);
	}
	
	public String getReciverName(int reciver_Ind_id)
	{
		return accdao.getReciverName(reciver_Ind_id);
	}
	
	public String getReciverEmailId(int reciver_Ind_id)
	{
		return accdao.getReciverEmailId(reciver_Ind_id);
	}
	
	public String getReciverMobileNo(int reciver_Ind_id)
	{
		return accdao.getReciverMobileNo(reciver_Ind_id);
	}
	
	public String getLastGeneratedCode()
	{
		return accdao.getLastGeneratedCode();
	}
	
	public String patternDetailsAsPerRecord(String searchResult,int ind_id)
	{
		String patternInArray[] = searchResult.split("/");
		
		String docName = "";
		
		String profileDetails = "";
		
		for(int i=0;i<patternInArray.length;i++)
		{
			if(patternInArray[i].contains("PROFILE"))
			{
				String profileArray[] = patternInArray[i].split(":");				
				String profileData[] = profileArray[1].split(",");
				for(int j=0;j<profileData.length;j++)
				{
					String profileData1[] = profileData[j].split("-");
					if(profileData1[0].equals("REGISTRATION"))
					{
						List<RegHistoryModel> regData = accdao.getRegistrationDetails(ind_id);
						
						if(profileData1[1].equals("Mobile No"))
						{
							if(regData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+regData.get(0).getMobile_no() + "PATTERNSEPERETOR";
							}
							
						}
						else if(profileData1[1].equals("Email Id"))
						{
							if(regData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+regData.get(0).getEmail_id() + "PATTERNSEPERETOR";
							}
							
						}
						else
						{
							if(regData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+regData.get(0).getGender() + "PATTERNSEPERETOR";
							}
						}							
					}
					else if(profileData1[0].equals("BASIC"))
					{
						List<BasicDetail> basicData = accdao.getBasicDetailDetails(ind_id);
						
						if(profileData1[1].equals("Alternate Email Id"))
						{
							if(basicData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+basicData.get(0).getAlternate_email() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else if(profileData1[1].equals("Date of Birth"))
						{
							if(basicData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+basicData.get(0).getDate_of_birth() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else if(profileData1[1].equals("Residence Contact no"))
						{
							if(basicData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+basicData.get(0).getTell_residence() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else if(profileData1[1].equals("Office Contact no"))
						{
							if(basicData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+basicData.get(0).getTell_office() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else if(profileData1[1].equals("Marital Status"))
						{
							if(basicData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+basicData.get(0).getMarital_status() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else if(profileData1[1].equals("Present Address"))
						{
							if(basicData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+basicData.get(0).getPresent_address().replaceAll("##", " ") + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else 
						{
							if(basicData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+basicData.get(0).getPermanent_address().replaceAll("##", " ") + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}	
					}
					else
					{
						List<FamilyDetail> familyData = accdao.getFamilyDetailDetails(ind_id);
						
						if(profileData1[1].equals("Father Name"))
						{
							if(familyData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+familyData.get(0).getFather_name() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else if(profileData1[1].equals("Mother Name"))
						{
							if(familyData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+familyData.get(0).getMother_name() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else if(profileData1[1].equals("Sibling Name"))
						{
							if(familyData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+familyData.get(0).getSibling_name() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}
						else 
						{
							if(familyData.size()>0)
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+familyData.get(0).getSpouse_name() + "PATTERNSEPERETOR";
							}
							else
							{
								profileDetails += profileData1[0]+"SEPERETOR"+profileData1[1]+"SEPERETOR"+ "Not Updated" + "PATTERNSEPERETOR";
							}
						}	
					}										
				}				
			}
			else
			{
				if(patternInArray[i].contains("ACADEMIC"))
				{
					String docname[] = patternInArray[i].split(":");
					String docTyp[] = docname[1].split(",");					
				}
				else if(patternInArray[i].contains("FINANCIAL"))
				{
					
				}
				else
				{
					
				}
			}
		}
		
		if(profileDetails.length()>0)
		{
			profileDetails = profileDetails.substring(0, profileDetails.length()-16);
			System.out.println("ProfileDetails-"+profileDetails);
		}
		
		if(docName.length()>0)
		{
			docName = docName.substring(0, docName.length()-1);
			System.out.println("Dcoument Details-"+docName);
		}
		
		return profileDetails +"@@@"+ docName;
	}
	
	
	public List<UtiliserProfileModel> getUtilizerDetails(String utiId)
	{
		return accdao.getUtilizerDetails(utiId);
	}
	
	public String deactiveLastAccess(AccessDetailsModel accmodel)
	{
		return accdao.deactiveLastAccess(accmodel);
	}
	
	
	public String getLatestDocumntNameOfThisType(String documentType,int IndId)
	{
		String docNames = "";
		docNames = accdao.getPannelStatus(documentType,IndId);			
		
		return docNames;
	}
	
	public String  getUtilizerFullName(String Kyc_id)
	{
		String utilizerFullName = "";
		List<UtiliserProfileModel> utiFullName = accdao.getUtilizerFullName(Kyc_id);
		utilizerFullName += utiFullName.get(0).getUti_first_name()+" "+utiFullName.get(0).getUti_middle_name()+" "+utiFullName.get(0).getUti_last_name();
		
		return utilizerFullName;
	}
	
	public String getIndNameFromKycId(String Kyc_id)
	{
		return accdao.getIndNameFromKycId(Kyc_id);
	}
	
	
	public String getDocumentAvailablityViaThePattern(String searchResult,int ind_id)
	{
		String documentAvailability = "";
        String patternInArray[] = searchResult.split("/");		
		
		for(int i=0;i<patternInArray.length;i++)
		{
			if(!patternInArray[i].contains("PROFILE"))
			{
				String patternArray[] = patternInArray[i].split(":");
				String documentArray[] = patternArray[1].split(",");
				for(int j=0;j<documentArray.length;j++)
				{
					String availability = accdao.getDocumentAvailablityViaThePattern(documentArray[j],patternArray[0],ind_id);
					documentAvailability += availability + ",";
				}
			}
		}
		
		if(documentAvailability.length()>0)
		{
			documentAvailability = documentAvailability.substring(0, documentAvailability.length()-1);
			System.out.println("document Status="+documentAvailability);
		}
		
		return documentAvailability;
	}
	
	
	
	
	
	
}
