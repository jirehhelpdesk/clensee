package com.kyc.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;





















































import com.kyc.dao.ProfileinfoDao;
import com.kyc.model.AccessDetailsModel;
import com.kyc.model.BasicDetail;
import com.kyc.model.CodesDetailsModel;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.IndSignupModel;
import com.kyc.model.IndividualPlanDetailModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.Matrial;
import com.kyc.model.ProfilePictureModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.ResetPasswordModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.Socialacc;
import com.kyc.model.Summarydetail;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPasswordRequestModel;
import com.kyc.util.SendEmailUtil;

@Service("profileinfoService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ProfileinfoServiceImpl implements ProfileinfoService{
   
	@Autowired
	private ProfileinfoDao ProfileinfoDao;
	
	public String addFamilyDetail(FamilyDetail FamilyDetail) {
	return	ProfileinfoDao.addFamilyDetail(FamilyDetail);
	}
	
	public String profileupdate(Profileupdatemodel Profileupdatemodel) {
		return	ProfileinfoDao.profileupdate(Profileupdatemodel);
		}	
	public String addMatrial(Matrial matrial) {
		return ProfileinfoDao.addMatrial(matrial);
	}
	
	
	public String addSocialacc(Socialacc socialacc) {
	return ProfileinfoDao.addSocialacc(socialacc);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public String addSummarydetail(Summarydetail summarydetail) {
	return	ProfileinfoDao.addSummarydetail(summarydetail);
	}


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public String addBasicDetail(BasicDetail BasicDetail) {
		return ProfileinfoDao.addBasicDetail(BasicDetail);
			
	}
	
	public String getkycid(int ind_kyc_id) {
		return ProfileinfoDao.getkycid(ind_kyc_id);
			
	} 

	
	
	public String getfirstname(String ind_kyc_id1) {
		return ProfileinfoDao.getfirstname(ind_kyc_id1);
			
	}
	
	public String visibilityBasicDetail(BasicDetail BasicDetail) {
		return ProfileinfoDao.visibilityBasicDetail(BasicDetail);
			
	}
	public FamilyDetail getfamilyvalue(int fam_details_id) {
		return ProfileinfoDao.getfamilyvalue(fam_details_id);
	}
	
	
	public Socialacc getsocialaccvalue(int social_det_id) {
		return ProfileinfoDao.socialaccvalue(social_det_id);
	}
	
	public Summarydetail getSummaryvalue(String ind_kyc_id) {
		return ProfileinfoDao.getSummaryvalue(ind_kyc_id);
	}
	
	public   Matrial getMatrimonyvalue(int matrimony_details_id){
			return ProfileinfoDao.getMatrimonyvalue(matrimony_details_id);
	}
	
	
	public   BasicDetail getBasicDetailvalue(int BasicDetailid){
		return ProfileinfoDao.getBasicDetailvalue(BasicDetailid);
    }
	@Override
	public Socialacc getsocialdetails(String ind_kyc_id) {
		return ProfileinfoDao.getsocialdetails(ind_kyc_id);
	}  
	
	
	public  IndSignupModel getregistrationdetails(String ind_kyc_id) {
		return ProfileinfoDao.getregistrationdetails(ind_kyc_id);
	}    
	

	public  String Getregid(Summarydetail summary) {
		return ProfileinfoDao.Getregid(summary);
	}
	
	public  void setregid(String ind_kyc_id) {
		 ProfileinfoDao.setregid(ind_kyc_id);
	}
	
	public String Getmatid(Matrial matrial){
		
		return ProfileinfoDao.Getmatid(matrial);
	}
	
	
public String Getbasicid(BasicDetail BasicDetail){
		
		return ProfileinfoDao.Getbasicid(BasicDetail);
	}

	@Override 
	public Summarydetail getSummaryvalue(int sum_det_id) {
		return ProfileinfoDao.getSummaryvalue(sum_det_id);
		
	}
	
	
	public String Getfamid(String ind_kyc_id) {
		return ProfileinfoDao.Getfamid(ind_kyc_id);
		
	}
	
	public String Getsocialaccid(String ind_kyc_id) {
		return ProfileinfoDao.Getsocialaccid(ind_kyc_id);
		
	}
	
	public String GetSocialaccid(String ind_kyc_id) {
		return ProfileinfoDao.GetSocialaccid(ind_kyc_id);
		
	}
	
	public String geteditfamilyvalue(FamilyDetail FamilyDetail) {
		return ProfileinfoDao.geteditfamilyvalue(FamilyDetail);
		
	} 
	public String editSocialacc(Socialacc socialacc) {
		return ProfileinfoDao.editSocialacc(socialacc);
		
	}   
	public String editBasicDetail(BasicDetail BasicDetail) {
		return ProfileinfoDao.editBasicDetail(BasicDetail);
		
	}
	public String editmatrial(Matrial matrial) {
		return ProfileinfoDao.editmatrial(matrial);
		
	}
	
	public String editsummary(Summarydetail summary) {
	return	ProfileinfoDao.editsummary(summary);
		
	}
	
	public String getdescription(String ind_kyc_id) {
		return ProfileinfoDao.getdescription(ind_kyc_id);
		
	}
	
	public List<Summarydetail> getsummarydetail(Summarydetail summary) {
		return ProfileinfoDao.getsummarydetail(summary);
		
	}
	
	@Override
	public List<IndSignupModel> searchprofile(IndSignupModel searchprofile) {
		// TODO Auto-generated method stub
		return ProfileinfoDao.searchprofile(searchprofile);
	}
	
	@Override
	public List<IndSignupModel> searchhomepage(IndSignupModel searchhomepage) {
		// TODO Auto-generated method stub
		return ProfileinfoDao.searchhomepage(searchhomepage);
	}
	
	@Override
	public List<IndSignupModel> searchprofilekycid(IndSignupModel searchprofile) {
		// TODO Auto-generated method stub
		return ProfileinfoDao.searchprofilekycid(searchprofile);
	}
	
	@Override
	public List<IndSignupModel> viewmore(IndSignupModel viewmore) {
		// TODO Auto-generated method stub
		return ProfileinfoDao.viewmore(viewmore);
	}
	@Override
	public String getprofilepic(IndSignupModel searchprofile) {
		// TODO Auto-generated method stub
		return ProfileinfoDao.getprofilepic(searchprofile);
	}
	
	public String getsearchedPresentAddress(IndSignupModel searchprofile)
	{
		return ProfileinfoDao.getsearchedPresentAddress(searchprofile);
	}
	
	@Override
	public List<Profileupdatemodel> profilenotification(Profileupdatemodel profilenotification) {
		// TODO Auto-generated method stub
		return ProfileinfoDao.profilenotification(profilenotification);
	}
	@Override
	public String profilevisitors(Profileupdatemodel profilenotification) {
		
		List<Profileupdatemodel> viewerList = ProfileinfoDao.profilevisitors(profilenotification);
		String viewerData = "";
		if(viewerList.size()>0)
		{
			for(int i=0;i<viewerList.size();i++)
			{
				String viewerKycId = viewerList.get(i).getViewer_kyc_id();
				int viewerIndId = ProfileinfoDao.findInd_id(viewerKycId);
				int year = ProfileinfoDao.getYear(viewerIndId);
				String viewerProfilePhoto = ProfileinfoDao.getLatestPicName(viewerIndId);
				String viewerName =  viewerList.get(i).getProfileupdatedetail();				
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				Date date = viewerList.get(i).getProfileupdatetime();        			
				String viewedDate = df.format(date);
				
				viewerData = viewerData + viewerIndId+":"+year + ":" + viewerProfilePhoto+":"+viewerName+":"+viewedDate+ ",";				
			}
			viewerData = viewerData.substring(0, viewerData.length()-1);
		}
		else
		{
			viewerData = "No Data";
		}
		return viewerData;
	}
	
	@Override
	public List<Profileupdatemodel> profileactivities(Profileupdatemodel profilenotification) {
		// TODO Auto-generated method stub
		return ProfileinfoDao.profileactivities(profilenotification);
	}
	@Override
	public String Getacademicdetail(DocumentDetailModel docdetails) {
		 // TODO Auto-generated method stub
		return ProfileinfoDao.Getacademicdetail(docdetails);
	}
	
	public List<DocumentDetailModel> Getemployeedetail(DocumentDetailModel docdetails) {
		 // TODO Auto-generated method stub
		
		System.out.println("The Final Result of Latest Documents"+ProfileinfoDao.Getemployeedetail(docdetails).toString());
		
		return ProfileinfoDao.Getemployeedetail(docdetails);

	}

	public  String searchhomepageforUti(String searchvalue)
	{
		List<UtiliserProfileModel> uti_pro = ProfileinfoDao.searchhomepageforUti(searchvalue);
		
		List<String> uti_prodata = new ArrayList<String>();
		
		for(int i=0;i<uti_pro.size();i++)
		{
		   uti_prodata.add(uti_pro.get(i).getKyc_uti_id());
		   uti_prodata.add(uti_pro.get(i).getUti_first_name());
		   
		   DateFormat df = new SimpleDateFormat("YYYY/mm/dd/ HH:mm:ss");		
		   Date today = uti_pro.get(i).getUti_cr_date();       
		   String reportDate = df.format(today);
		 		   
		   uti_prodata.add(reportDate);
			
		}		
		return uti_prodata.toString();
	}
	

	public int findInd_id(String ind_kyc_id)
	{
		return ProfileinfoDao.findInd_id(ind_kyc_id);
	}
	
	public String getBasicHistory(int ind_id)
	{
		List<BasicDetail> basicData = ProfileinfoDao.getBasicHistory(ind_id);
		List<String> basicList = new ArrayList<String>();
		String basicHistory = "";
		for(int i=0;i<basicData.size();i++)
		{
			
			basicList.add(basicData.get(i).getNationality());
			basicList.add(basicData.get(i).getDate_of_birth());
			basicList.add(basicData.get(i).getAlternate_email());
			basicList.add(basicData.get(i).getHobbies());
			
			
			if(!basicData.get(i).getPresent_address().equals(""))
			{
				if(basicData.get(i).getPresent_address().contains("##"))
				{
					basicList.add(basicData.get(i).getPresent_address().replaceAll("##","-"));
				}
			}
			
			if(basicData.get(i).getPresent_address().equals(""))
			{				
				basicList.add("NA");				
			}
			
			if(!basicData.get(i).getPermanent_address().equals(""))
			{
				if(basicData.get(i).getPermanent_address().contains("##"))
				{
					basicList.add(basicData.get(i).getPermanent_address().replaceAll("##","-"));
				}
			}
			
			if(basicData.get(i).getPermanent_address().equals(""))
			{
				basicList.add("NA");
			}
			
			basicList.add(basicData.get(i).getLanguages_known());
			
			basicList.add(basicData.get(i).getMarital_status());
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date today = basicData.get(i).getCr_date();        
			String reportDate = df.format(today);
			basicList.add(reportDate);
			
		}
		
		for(int b=0;b<basicList.size();b++)
		{
			basicHistory +=  basicList.get(b) + "$$$";
		}
		
		basicHistory = basicHistory.substring(0, basicHistory.length()-3);
				
		return basicHistory;
	}
	
	public String getFamilyHistory(int ind_id)
	{
		List<FamilyDetail> familyHistory = ProfileinfoDao.getFamilyHistory(ind_id);
		List<String> familyData = new ArrayList<String>();
				
		for(int i=0;i<familyHistory.size();i++)
		{
		   familyData.add(familyHistory.get(i).getFather_name());	
		   familyData.add(familyHistory.get(i).getFather_POI());
		   familyData.add(familyHistory.get(i).getMother_name());	
		   familyData.add(familyHistory.get(i).getMother_POI());
		   familyData.add(familyHistory.get(i).getSibling_name());		   
		   familyData.add(familyHistory.get(i).getSibling_POI());	
		   familyData.add(familyHistory.get(i).getSpouse_name());
		   familyData.add(familyHistory.get(i).getSpouse_POI());
		 
		   DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		   Date today = familyHistory.get(i).getCr_date();        
		   String reportDate = df.format(today);
		   familyData.add(reportDate);				   
		}	
		
		String familyDetails = "";
		
		for(int j=0;j<familyHistory.size();j++)
		{
			
			familyDetails +=  familyHistory.get(j).getFather_name() + "KYCSEP"
			                 +familyHistory.get(j).getFather_POI() + "KYCSEP"
			                 +familyHistory.get(j).getMother_name() + "KYCSEP"
			                 +familyHistory.get(j).getMother_POI() + "KYCSEP"			                
			                 +familyHistory.get(j).getSibling_name() + "KYCSEP"
			                 +familyHistory.get(j).getSibling_POI() + "KYCSEP"
			                 +familyHistory.get(j).getSpouse_name() + "KYCSEP"
			                 +familyHistory.get(j).getSpouse_POI() + "KYCSEP"
			                 +familyHistory.get(j).getHoroscopeInformation() + "KYCSEP"
			                 +familyHistory.get(j).getFoodpreferred() + "KYCSEP";
			                 			                 
		}
		
		familyDetails = familyDetails.substring(0, familyDetails.length()-6);
		
		return familyDetails;		
	}
	
	
	public String getsocialHistory(int ind_id)
	{
		List<Socialacc> socialdata = ProfileinfoDao.getsocialHistory(ind_id);
		
		List<String> socialHistory = new ArrayList<String>();
		
		String HistoryDetails = "";
		
		for(int i=0;i<socialdata.size();i++)
		{
			HistoryDetails += socialdata.get(i).getWebsite_link()+ "URLDIV" + socialdata.get(i).getLinkedln_link() + "URLDIV" + socialdata.get(i).getTwitter_link() + "URLDIV" ;
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		    Date today = socialdata.get(i).getCr_date();   
		    HistoryDetails += today + "URLDIV";
		}
		
		HistoryDetails = HistoryDetails.substring(0, HistoryDetails.length()-6);
		
		for(int i=0;i<socialdata.size();i++)
		{
			socialHistory.add(socialdata.get(i).getWebsite_link());
			socialHistory.add(socialdata.get(i).getLinkedln_link());
			socialHistory.add(socialdata.get(i).getTwitter_link());
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		    Date today = socialdata.get(i).getCr_date();        
			String reportDate = df.format(today);
			
			socialHistory.add(reportDate);
		}
		
		return HistoryDetails;
	}
	
	public String getIndNotify(String kyc_id)
	{
		List<String> indNotify = ProfileinfoDao.getIndNotify(kyc_id);
		
		List<Date> sharedIndDetails = ProfileinfoDao.getSharedIndDetails(kyc_id);
		
		String code_sender = "";
		
		if(indNotify.size()>0)
		{
			for(int i=0;i<indNotify.size();i++)
			{
				code_sender = code_sender + indNotify.get(0) + ",";
			}
		}
		
		String fullName = "";
		String GivenDate = "";
		
		if(code_sender.length()>0)
		{
			String codeSenderArray[] = code_sender.split(",");
			
			for(int j=0;j<codeSenderArray.length;j++)
			{
				String regDetails  = ProfileinfoDao.getCreatedBy(codeSenderArray[j]);
				fullName += regDetails + ",";
			}
			
			fullName = fullName.substring(0, fullName.length()-1);
			
			
			for(int d=0;d<sharedIndDetails.size();d++)
			{
				GivenDate += sharedIndDetails.get(d) + "date";
			}
						
			GivenDate = GivenDate.substring(0,GivenDate.length()-4);
		}
				
		return fullName+ "Plus" +GivenDate;
		
	}
	
	
	public String saveAboutUsInfo(Summarydetail sumDetail)
	{
		
		return ProfileinfoDao.saveAboutUsInfo(sumDetail);
	}
	
	public String getSum_det_id(String id)
	{
		return ProfileinfoDao.getSum_det_id(id);
	}
	
	public String getAboutUsInfo(Summarydetail sumDetail)
	{
		return ProfileinfoDao.getAboutUsInfo(sumDetail);
	}
	
	public String getPicName(int ind_id)
	{
		return ProfileinfoDao.getPicName(ind_id);
	}
	
	public String saveProfilePicInfo(ProfilePictureModel profileModel)
	{
		return ProfileinfoDao.saveProfilePicInfo(profileModel);
	}
	
	 public String getLatestPicName(int ind_id)
	{
		return ProfileinfoDao.getLatestPicName(ind_id);
	}
	 
	 public ProfilePictureModel getPrifilePicInfo(int ind_id)
	 {
		 return ProfileinfoDao.getPrifilePicInfo(ind_id);
	 }
	 
	 public int getPrifilePicId(String picName)
	 {
		 return ProfileinfoDao.getPrifilePicId(picName);
	 }
	 
	 public String GetacademicPostDate(DocumentDetailModel docdetails)
	 {
		 return ProfileinfoDao.GetacademicPostDate(docdetails);
	 }
	 
	 public String GetEmpPostDate(DocumentDetailModel docdetails)
	 {
		 return ProfileinfoDao.GetEmpPostDate(docdetails);
	 }
	 
	 public String getPermanentAddress(int id)
	 {
		 return ProfileinfoDao.getPermanentAddress(id);
	 }
		
	public String getPresentAddress(int id)
	{
		return ProfileinfoDao.getPresentAddress(id);
	}
	
    public String getRegVisisbility(int ind_id)
    {
    	return ProfileinfoDao.getRegVisisbility(ind_id);
    }
	
	public String getBasicVisisbility(int ind_id)
	{
		return ProfileinfoDao.getBasicVisisbility(ind_id);
	}
	
	public String getFamilyVisisbility(int ind_id)
	{
		return ProfileinfoDao.getFamilyVisisbility(ind_id);
	}
	
	public String getSocialVisisbility(int ind_id)
	{
		return ProfileinfoDao.getSocialVisisbility(ind_id);
	}
	
	public String updateRegVisibility(int ind_id,String val)
	{
		return ProfileinfoDao.updateRegVisibility(ind_id,val);
	}
	
	public String setBasicVisibility(int ind_id,String val)
	{
		return ProfileinfoDao.setBasicVisibility(ind_id,val);
	}
	
	public String setFamilyVisibility(int ind_id,String val)
	{
		return ProfileinfoDao.setFamilyVisibility(ind_id,val);
	}
	
	public String setSocialVisibility(int ind_id,String val)
	{
		return ProfileinfoDao.setSocialVisibility(ind_id,val);
	}
	
	public String getBasicDetail(int ind_id)
	{
		List<BasicDetail> basicData = ProfileinfoDao.getBasicDetail(ind_id);
		
		List<String> basiclist = new ArrayList<String>();
		
		if(basicData.size()>0)
		{
			for(int i=0;i<basicData.size();i++)
			{
				basiclist.add(basicData.get(i).getAlternate_email());	
				basiclist.add(basicData.get(i).getDate_of_birth());
				basiclist.add(basicData.get(i).getTell_office());
				basiclist.add(basicData.get(i).getTell_residence());
				basiclist.add(basicData.get(i).getNationality());
				basiclist.add(basicData.get(i).getMarital_status());
				basiclist.add(basicData.get(i).getLanguages_known());
				basiclist.add(basicData.get(i).getHobbies());
				basiclist.add(basicData.get(i).getPresent_address());
				basiclist.add(basicData.get(i).getPermanent_address());
			}
			return basiclist.toString();
		}
		else
		{
			return "NoData";
		}
		
		
	}
	
	public String getFamilyDetail(int ind_id)
	{
		List<FamilyDetail> familyData = ProfileinfoDao.getFamilyDetail(ind_id);
		List<String> familylist = new ArrayList<String>();
		
		if(familyData.size()>0)
		{
			for(int i=0;i<familyData.size();i++)
			{
				familylist.add(familyData.get(i).getFather_name());
				familylist.add(familyData.get(i).getMother_name());
				familylist.add(familyData.get(i).getSibling_name());
			}
			
			return familylist.toString();	
		}
		else
		{
			return "NoData";
		}
	}
	
	public String getsocialdetails(int ind_id)
	{
		List<Socialacc> socialData = ProfileinfoDao.getsocialdetails(ind_id);
		List<String> sociallist = new ArrayList<String>();
		
		if(socialData.size()>0)
		{
			for(int i=0;i<socialData.size();i++)
			{
				sociallist.add(socialData.get(i).getWebsite_link());
				sociallist.add(socialData.get(i).getLinkedln_link());
				sociallist.add(socialData.get(i).getTwitter_link());
			}
			return sociallist.toString();
		}
		else
		{
			return "NoData";
		}
	}
	
	public String getProfilePhoto(int ind_id)
	{
		
		return ProfileinfoDao.getProfilePhoto(ind_id);
	}
	
	public String updateProfilePic(String picName,int ind_id)
	{
		return ProfileinfoDao.updateProfilePic(picName,ind_id);
	}
	
	public String getIndividualDetails(int ind_id)
	{
		List<RegHistoryModel> regData = ProfileinfoDao.getIndividualDetails(ind_id);
		
		List<String> reglist = new ArrayList<String>();
		
		for(int i=0;i<regData.size();i++)
		{
			reglist.add(regData.get(i).getFirst_name());
			reglist.add(regData.get(i).getLast_name());			
			reglist.add(regData.get(i).getGender());			
			reglist.add(regData.get(i).getEmail_id());
			Long no = regData.get(i).getMobile_no();
			String phno = Long.toString(no);
			reglist.add(phno);			
		}
				
		return reglist.toString();
	}
	public int getYear(int ind_id)
	{
		return ProfileinfoDao.getYear(ind_id);
	}
	
	
	public String getSearchKycInd(String searchedValue)
	{
		List<IndSignupModel> indListData = ProfileinfoDao.getSearchKycInd(searchedValue);
		
		String indListToString = "";
				
		if(indListData.size()>0)
		{
			for(int i=0;i<indListData.size();i++)
			{
				Integer indId = indListData.get(i).getInd_id();		
				DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
			    Date date = indListData.get(i).getCr_date();        
				String year = df.format(date);
				
				String profilePic = ProfileinfoDao.getLatestPicName(indId);
				String indName = indListData.get(i).getFirstname() + " " +indListData.get(i).getMiddlename() + " " +indListData.get(i).getLastname();	
				String country = ProfileinfoDao.getCityName(indId);		
				String singleIndData = indId + ":" + year.split("/")[0] + ":" + profilePic+":"+indName+":"+country;
				indListToString = indListToString + singleIndData + ",";
			}
			indListToString = indListToString.substring(0, indListToString.length()-1);
			return indListToString;
		}
		else
		{
			return indListToString;
		}
		
	}
	
	public String getSearchedAboutUsInfo(int ind_id)
	{
		return ProfileinfoDao.getSearchedAboutUsInfo(ind_id);
	}
	
	public String getValidateEmailId(String emailId)
	{
		List<String> emailList = ProfileinfoDao.getValidateEmailId(emailId);
		String emailString = "";
		
		if(emailList.size()>0)
		{			
			return "Yes";
		}
		else
		{
			return "No Data";
		}
	}
	
	public String getValidateEmailIdForgotPassword(String emailId)
	{
		String status = ProfileinfoDao.individualIdThroughEmailId(emailId);
		
		return status;
	}
	
	public String getViewedStatus(String kycId,String ownKycId)
	{
		return ProfileinfoDao.getViewedStatus(kycId,ownKycId);
	}
	
	public String getIndividualIdVaiEmailId(String emailId)
	{
		return ProfileinfoDao.getIndividualIdVaiEmailId(emailId);
	}
	
	public List<KycNotificationModel> getNotificationDetails(int ind_id)
	{
		return ProfileinfoDao.getNotificationDetails(ind_id);
	}
	
	public String getMessagefromNotifyId(int notifyId)
	{
		return ProfileinfoDao.getMessagefromNotifyId(notifyId);
	}
	
	public int findUtiid(String uti_kycid)
	{
		return ProfileinfoDao.findUtiid(uti_kycid);
	}
	
	public String saveActivityDetails(UtilizerActivityModel actModel)
	{
		return ProfileinfoDao.saveActivityDetails(actModel);
	}
	
	public List<AccessDetailsModel> getuti2IndNotification(String kyc_id)
	{
		return ProfileinfoDao.getuti2IndNotification(kyc_id);
	}
	
	public String getValidateMobileNo(String mobileNo)
	{
		List<String> mobileNoList = ProfileinfoDao.getValidateMobileNo(mobileNo);		
		if(mobileNoList.size()>0)
		{			
			return "Yes";		
		}
		else
		{
			return "No";
		}
	}
	
	public String getUpdateEmailId(int ind_id,String EmailId)
	{
		return ProfileinfoDao.getUpdateEmailId(ind_id,EmailId);
	}
	
	public String getKycIdOfAccessGiven(String IndKycId)
	{
		List<String> receiverKycIdList =  ProfileinfoDao.getKycIdOfAccessGiven(IndKycId);
		System.out.println("Other kycid Ind_Id " + receiverKycIdList.toString());
		
		String OtherIndId = "";
				
		if(receiverKycIdList.size()>0)
		{
			for(int i=0;i<receiverKycIdList.size();i++)
			{
				String KycId = receiverKycIdList.get(i);
				OtherIndId += ProfileinfoDao.getIndividualIdViaKycId(KycId) + ",";
				System.out.println("Each Ind_Id " + OtherIndId);
			}
			OtherIndId = OtherIndId.substring(0,OtherIndId.length()-1);
			System.out.println("Total Ind_Id " + OtherIndId);
		}
		else
		{
			OtherIndId += "No Data";
		}
		
				
		return OtherIndId;
	}
	
	public String getYearFormIndId(String OtherIndId)
	{
		String indIndArray[] = OtherIndId.split(",");
		String OtherYear = "";
		for(int i=0;i<indIndArray.length;i++)
		{
			OtherYear += ProfileinfoDao.getYearFromIndividualId(indIndArray[i]) + ",";
		}
		OtherYear = OtherYear.substring(0,OtherYear.length()-1);
		
		return OtherYear;
	}
	
	public String saveResetPasswordModel(ResetPasswordModel resetPW)
	{
		return ProfileinfoDao.saveResetPasswordModel(resetPW);
	}
	
	public String getCurrentPlanName(int ownInd_id)
	{
		return ProfileinfoDao.getCurrentPlanName(ownInd_id);
	}
	
	public Date getRegisterDate(int ownInd_id)
	{
		return ProfileinfoDao.getRegisterDate(ownInd_id);
	}
	
	public String getEntryInPlanDetails(IndividualPlanDetailModel indPlanModel)
	{
		return ProfileinfoDao.getEntryInPlanDetails(indPlanModel);
	}
	
	public String getvalidateRegistrationEmailId(String emailId,int ind_id)
	{
		return ProfileinfoDao.getvalidateRegistrationEmailId(emailId,ind_id);
	}
	
	public String getvalidateRegistrationMobileNo(String mobileNo,int ind_id)
	{
		return ProfileinfoDao.getvalidateRegistrationMobileNo(mobileNo,ind_id);
	}
	
	public String setEmailStatusReport(EmailSentStatusModel emailReport)
	{
		return ProfileinfoDao.setEmailStatusReport(emailReport);
	}
	
	public String setSmsStatusReport(SmsSentStatusModel smsReport)
	{
		return ProfileinfoDao.setSmsStatusReport(smsReport);
	}
	
	public String migratePlanAfterOneYear(int ownInd_id)
	{
		return ProfileinfoDao.migratePlanAfterOneYear(ownInd_id);
	}
	
	public String saveUtiResetPasswordModel(UtilizerPasswordRequestModel resetPW)
	{
		return ProfileinfoDao.saveUtiResetPasswordModel(resetPW);
	}
	
	public  List<RegHistoryModel>  getIndividualDetailsviaIndId(int indId)
	{
		return ProfileinfoDao.getIndividualDetailsviaIndId(indId);
	}
	
	public String getUpdateRegTableWithLastRecord(RegHistoryModel indHistory,String fulleName)
	{
		return ProfileinfoDao.getUpdateRegTableWithLastRecord(indHistory,fulleName);
	}
	
	public String getUtilizerAccessForIndividual(String individualKycId,String utilizerKycId)
	{
		return ProfileinfoDao.getUtilizerAccessForIndividual(individualKycId,utilizerKycId);
	}
	
	public List<BasicDetail> getLatestBasicDetailsInfo(int basicId)
	{
		return ProfileinfoDao.getLatestBasicDetailsInfo(basicId);
	}
	
	public int getIndividualIdVaiVisibility(String resetVariable)
	{
		return ProfileinfoDao.getIndividualIdVaiVisibility(resetVariable);
	}
	
	public String updateVisibilityUrl(int ind_id,String resetVariable,String encryptedUrl)
	{
		return ProfileinfoDao.updateVisibilityUrl(ind_id,resetVariable,encryptedUrl);
	}
	
	public String getLatestProfilePitureName(int individualId)
	{
		return ProfileinfoDao.getLatestProfilePitureName(individualId);
	}
	
	public String saveClenseeContactDetails(ContactUsModel contactus)
	{
		return ProfileinfoDao.saveClenseeContactDetails(contactus);
	}
	
	public int getIndIdViaEmailId(String emailId)
	{
		return ProfileinfoDao.getIndIdViaEmailId(emailId);
	}
	
	

	
	public String sendMailForPlanChange(String reciverName,String reciverEmailId,String requestedPlanName)
	{
		
		ResourceBundle resource = ResourceBundle.getBundle("resources/planChangeMail");
				
		String headingImage = resource.getString("headingImage");
		String belowleftImage = resource.getString("belowleftImage");
		String belowrightImage = resource.getString("belowrightImage");
		String mailicon = resource.getString("mailicon");
		String contacticon = resource.getString("contacticon");

		String emailHeading = resource.getString("emailHeading");
		String contactNo = resource.getString("contactNo");
		String contactEmail = resource.getString("contactEmail");

		String plansubject = resource.getString("plansubject");
		String notifymessage1 = resource.getString("notifymessage1");
		String notifymessage2 = resource.getString("notifymessage2");

		String commonmessage = resource.getString("commonmessage");
		String aftermessageextraline = resource.getString("aftermessageextraline");
				
		String copyright = resource.getString("copyright");
		String termscondition = resource.getString("termscondition");
		String privacyPolicy = resource.getString("privacyPolicy");

		String facebookicon = resource.getString("facebookicon");
		String twittericon = resource.getString("twittericon");
		String googleicon = resource.getString("googleicon");
		String linkedicon = resource.getString("linkedicon");

		String facebookLink = resource.getString("facebookLink");
		String twitterLink = resource.getString("twitterLink");
		String googlePlus = resource.getString("googlePlus");
		String linkedIn = resource.getString("linkedIn");

		String clenseeLogo = resource.getString("clenseeLogo");
		String planwelcome = resource.getString("planwelcome");
		String clickhere = resource.getString("clickhere");
		String indactionUrl = resource.getString("indactionUrl");
		
		
				        StringBuilder text = new StringBuilder();
			        
				        
					    /* Sending Emails to Register user */
					       
				        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
				        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");					       
				        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
				       
				        /* Header */
				        
				        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
				        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
				        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
				        text.append("</tr></table></td></tr></table>");
				       
				        /* End of Header */
				        
				        
				        /* Header-2 */
				        
				        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
				        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
				        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
				        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
				        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
				        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
				        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
				        text.append("</p></td></tr></table></td></tr></table>");
				        
				        /* End of Header-2 */
				        
				        
				        /* BODY PART */
				        
				        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
				        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
				        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
				        text.append("<h3 style='color: #004879;'>Dear "+reciverName+",</h3>");
				        text.append("<p style='text-align: justify'>"+planwelcome+"</p>");
				        text.append("<p style='text-align: justify'>"+notifymessage1+" "+requestedPlanName+","+notifymessage2+"</p>");
				        text.append("<p style='text-align: justify'>"+commonmessage+"</p>");
				        			        					        
				        text.append("<br><p>"+aftermessageextraline+"<br><br>");					        	
				        text.append("<a href="+indactionUrl+" target='_blank' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
				        text.append("</p>");
				        
				        text.append("<p>");					        	
				        text.append("For any further clarification please drop a mail to <a href='#'><b>payments@clensee.com</b></a>");
				        text.append("</p>");
				        
				        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
				        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
				        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
				        text.append("</tr></table></td></tr>");
				        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
				        text.append("</table> ");
				        
				        
				        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
				        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
				        
				        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
				        
				        text.append("<tr><td>");
				        
				        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
				        text.append("</td></tr></table>");
				        
				        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
				        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
				        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
				        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
				        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
				        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
				        					        
				        text.append("</tr>");
				        text.append("</table></td></tr></table></td></tr></table>");
				        text.append("</body>");
				        text.append("</html>");
				        
				        /* END OF BODY PART */
				        
				        					        
				    /* End of Sending Emails to Register user */
				    
	       	        SendEmailUtil emailStatus = new SendEmailUtil();		     	        
	       	        String subject = plansubject+" "+requestedPlanName;
	       	        
	     			String resetStatus = "";
					try 
					{
						resetStatus = emailStatus.getEmailSent(reciverEmailId,subject,text.toString());
					} 
					catch (IOException e) 
					{							
						e.printStackTrace();
					}
					
	     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
	     	    	
	     	    	if(resetStatus.equals("success"))
	     	    	{	    		
	     	    		emailReport.setEmail_id(reciverEmailId);
	     	    		emailReport.setSubject(subject);
	     	    		emailReport.setMessage_body(text.toString());
	     	    		emailReport.setSent_date(new Date());
	     	    		emailReport.setSent_report("0");
	     	    	}
	     	    	else
	     	    	{
	     	    		emailReport.setEmail_id(reciverEmailId);
	     	    		emailReport.setSubject(subject);
	     	    		emailReport.setMessage_body(text.toString());
	     	    		emailReport.setSent_date(new Date());
	     	    		emailReport.setSent_report("1");	    			    		
	     	    	}
	     	    	
	     	    	String emailSatus =  ProfileinfoDao.setEmailStatusReport(emailReport);
	     	    	
	     	    	return resetStatus;
	     	    	
	}
	
	
	
	
	
	public String sendEmailForInvoice(int day,int month,int year,String uniqueInvoiceId,int randomNumber,String paymentAmount,String currentPlanName,String requestedPlanName,String reciverName,String reciverEmailId)
	{
		
		String resetStatus = "";
		SendEmailUtil emailStatus = new SendEmailUtil();
		
        ResourceBundle resource = ResourceBundle.getBundle("resources/planChangeMail");
		
		String clenseeLogo = resource.getString("clenseeLogo");
		
		ResourceBundle clenseeInvoiceDetails = ResourceBundle.getBundle("resources/clenseeInvoiceDetails");
		     
	    String invoicesubject=clenseeInvoiceDetails.getString("invoicesubject");
	    String invoiceleftName=clenseeInvoiceDetails.getString("invoiceleftName");
	    String invoicegenerated=clenseeInvoiceDetails.getString("invoicegenerated");
     
    	StringBuilder invoicetext = new StringBuilder();
    
    	invoicetext.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
    	invoicetext.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");					       
    	invoicetext.append("<body style='font: 14px/1.4 arial;'>");							        
    	
    	invoicetext.append("<div style='background-color: #eee;margin: 0 auto;width: 800px;'>");	
    	
 	    invoicetext.append("<div style='background: #222 none repeat scroll 0 0;color: white;letter-spacing: 20px;margin: 6px 0;padding: 8px 0;text-align: center;'>INVOICE</div>");
 	    	
 	    invoicetext.append("<div style='background-color:#ccc;'><div style='text-align: left;'>");
	    	invoicetext.append("<img src=" +clenseeLogo+ " alt='logo' />");
	    	invoicetext.append("</div>");
	    	invoicetext.append("<div style='float: right;font-size: 20px;font-weight: bold;margin-top: -40px;' >"+invoiceleftName+"</div>");								     	    							     	  
	    	invoicetext.append("</div>");
	    	
	    	invoicetext.append("<div style='overflow: hidden;margin-top: 42px;'>");
	    	
	    	invoicetext.append("<div style='float: left;font-size: 15px;' ><b>Name:</b>"+reciverName+"</div><br><br>");
	    	invoicetext.append("<div style='float: left;font-size: 15px;' ><b>Email Id:</b>"+reciverEmailId+"</div>");
	    	
	    	invoicetext.append("<table style='float: right;width: 300px;border-collapse: collapse;'>");
	    	invoicetext.append("<tr>");
	    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'>Invoice #</td>");
	    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'>"+uniqueInvoiceId+"</td>");
	    	invoicetext.append("</tr>");
	    	invoicetext.append("<tr>");
	    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;' >Date</td>");
	    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'>"+day+"/"+month+"/"+year+"</td>");
	    	invoicetext.append("</tr>");
	    	invoicetext.append("<tr>");
	    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;' >Amount Due</td>");
	    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'>Rs. "+paymentAmount+"</td>");
	    	invoicetext.append("</tr>");
	    	invoicetext.append("</table>");
	    	
	    	invoicetext.append("</div>");
	    	
	    	invoicetext.append("<table  style='border: 1px solid black;margin: 30px 0 0;width: 100%;border-collapse: collapse;'>");
	    	invoicetext.append("<tr>");
	    	invoicetext.append("<th style='border: 1px solid black;padding: 5px;'>Item</th>");
	    	invoicetext.append("<th style='border: 1px solid black;padding: 5px;'>Description</th>");
	    	invoicetext.append("<th style='border: 1px solid black;padding: 5px;'>Unit Cost</th>");
	    	invoicetext.append("</tr>");
	    	invoicetext.append("<tr>");					     	    	
	    	invoicetext.append("<td  style='border: 1px solid black;padding: 5px;'><div class='delete-wpr' style=''><div style=''>Plan Migration</div></div></td>");
	    	invoicetext.append("<td  style='border: 1px solid black;padding: 5px;'><div style=''>Plan migration from "+currentPlanName+" to "+requestedPlanName+"</div></td>");
	    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'><div class='cost' style=''>Rs. "+paymentAmount+"</div></td>");
	    	invoicetext.append("</tr>");
	    	invoicetext.append("<tr><td colspan='5' style='border: 1px solid black;padding: 5px;'></td></tr>");
	    	invoicetext.append("<tr>");
	    	invoicetext.append("<td  colspan='2' style='border: 1px solid black;padding: 5px;'><b>Taxes</b></td>");
	    	invoicetext.append("<td  style='border: 1px solid black;padding: 5px;'><div id='subtotal' style=''>Inclusive</div></td>");
	    	invoicetext.append("</tr>");
	    	invoicetext.append("<tr>");
	    	invoicetext.append("<td colspan='2' style='border: 1px solid black;padding: 5px;'><b>Total</b></td>");
	    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'><div id='total' style=''>Rs. "+paymentAmount+"</div></td>");
	    	invoicetext.append("</tr>");					     	    					     	    	
	    	invoicetext.append("</table>");
	    	
	    	invoicetext.append("<div  style='margin: 20px 0 0;text-align: center;'>");
	    	invoicetext.append("<div>Invoice generated by <b>"+invoicegenerated+"</b></div>");
	    	invoicetext.append("</div>");	
	    	invoicetext.append("</div>");	
	    	
	    	invoicetext.append("</body>");
	    	invoicetext.append("</html>");
	    					     	    	
	try 
	{
		resetStatus = emailStatus.getEmailSent(reciverEmailId,invoicesubject,invoicetext.toString());
	} 
	catch (IOException e) 
	{							
		e.printStackTrace();
	}
	
	
	EmailSentStatusModel emailReport = new EmailSentStatusModel();
	
 	if(resetStatus.equals("success"))
 	{	    		
 		emailReport.setEmail_id(reciverEmailId);
 		emailReport.setSubject(invoicesubject);
 		emailReport.setMessage_body(invoicetext.toString());
 		emailReport.setSent_date(new Date());
 		emailReport.setSent_report("0");
 	}
 	else
 	{
 		emailReport.setEmail_id(reciverEmailId);
 		emailReport.setSubject(invoicesubject);
 		emailReport.setMessage_body(invoicetext.toString());
 		emailReport.setSent_date(new Date());
 		emailReport.setSent_report("1");	    			    		
 	}
 	
 	ProfileinfoDao.setEmailStatusReport(emailReport);
		
 	return resetStatus;
 	
 	
}
	
	
	
	
	
	
	public String utilizerPlanChangeMail(String reciverName,String reciverEmailId,String requestedPlanName)
	{
		ResourceBundle resource = ResourceBundle.getBundle("resources/planChangeMail");
		     
	     String headingImage=resource.getString("headingImage");
   	 String belowleftImage=resource.getString("belowleftImage");
   	 String belowrightImage=resource.getString("belowrightImage");
   	 String mailicon=resource.getString("mailicon");
   	 String contacticon=resource.getString("contacticon");
   	 
   	 String emailHeading=resource.getString("emailHeading");
   	 String contactNo=resource.getString("contactNo");
   	 String contactEmail=resource.getString("contactEmail");
   	 
   	 String plansubject=resource.getString("plansubject");
   	 String notifymessage1=resource.getString("notifymessage1");
   	 String notifymessage2=resource.getString("notifymessage2");
		 
   	 String commonmessage=resource.getString("commonmessage");		
		 String aftermessageextraline = resource.getString("aftermessageextraline");
		 
   	 String copyright=resource.getString("copyright");
   	 String termscondition=resource.getString("termscondition");
   	 String privacyPolicy=resource.getString("privacyPolicy");
   	 
   	 String facebookicon=resource.getString("facebookicon");
   	 String twittericon=resource.getString("twittericon");
   	 String googleicon=resource.getString("googleicon");
   	 String linkedicon=resource.getString("linkedicon");
   	 
   	 String facebookLink=resource.getString("facebookLink");
   	 String twitterLink=resource.getString("twitterLink");
   	 String googlePlus=resource.getString("googlePlus");
   	 String linkedIn=resource.getString("linkedIn");
   	 
   	 String clenseeLogo = resource.getString("clenseeLogo");
		 String planwelcome = resource.getString("planwelcome");
		 String clickhere = resource.getString("clickhere");
		 String indactionUrl = resource.getString("indactionUrl");
		  
		 
		 
			
				        StringBuilder text = new StringBuilder();
			        
				        
					    /* Sending Emails to Register user */
					       
				        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
				        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");					       
				        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
				       
				        /* Header */
				        
				        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
				        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
				        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
				        text.append("</tr></table></td></tr></table>");
				       
				        /* End of Header */
				        
				        
				        /* Header-2 */
				        
				        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
				        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
				        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
				        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
				        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
				        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
				        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
				        text.append("</p></td></tr></table></td></tr></table>");
				        
				        /* End of Header-2 */
				       
				        
				        /* BODY PART */
				        
				        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
				        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
				        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
				        text.append("<h3 style='color: #004879;'>Dear "+reciverName+",</h3>");
				        text.append("<p style='text-align: justify'>"+planwelcome+"</p>");
				        text.append("<p style='text-align: justify'>"+notifymessage1+" "+requestedPlanName+","+notifymessage2+"</p>");
				        text.append("<p style='text-align: justify'>"+commonmessage+"</p>");
				        			        					        
				        text.append("<br><p>"+aftermessageextraline+"<br><br>");					        	
				        text.append("<a href="+indactionUrl+" target='_blank' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
				        text.append("</p>");
				        
				        text.append("<p>");					        	
				        text.append("For any further clarification please drop a mail to <a href='#'><b>payments@clensee.com</b></a>");
				        text.append("</p>");
				        
				        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
				        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
				        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
				        text.append("</tr></table></td></tr>");
				        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
				        text.append("</table> ");
				        									        
				        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
				        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
				        
				        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
				        
				        text.append("<tr><td>");
				        
				        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
				        text.append("</td></tr></table>");
				        
				        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
				        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
				        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
				        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
				        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
				        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
				        					        
				        text.append("</tr>");
				        text.append("</table></td></tr></table></td></tr></table>");
				        text.append("</body>");
				        text.append("</html>");
				        
				        /* END OF BODY PART */
				        
				       
				        
				    /* End of Sending Emails to Register user */
				    
	       	        SendEmailUtil emailStatus = new SendEmailUtil();		     	        
	       	        String subject = plansubject+" "+requestedPlanName;
	       	        
	     			String resetStatus = "";
					try 
					{
						resetStatus = emailStatus.getEmailSent(reciverEmailId,subject,text.toString());
					} 
					catch (IOException e) 
					{							
						e.printStackTrace();
					}
					
					
	     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
	     	    	
	     	    	if(resetStatus.equals("success"))
	     	    	{	    		
	     	    		emailReport.setEmail_id(reciverEmailId);
	     	    		emailReport.setSubject(subject);
	     	    		emailReport.setMessage_body(text.toString());
	     	    		emailReport.setSent_date(new Date());
	     	    		emailReport.setSent_report("0");
	     	    	}
	     	    	else
	     	    	{
	     	    		emailReport.setEmail_id(reciverEmailId);
	     	    		emailReport.setSubject(subject);
	     	    		emailReport.setMessage_body(text.toString());
	     	    		emailReport.setSent_date(new Date());
	     	    		emailReport.setSent_report("1");	    			    		
	     	    	}
	     	    	
	     	    	String emailstatus = ProfileinfoDao.setEmailStatusReport(emailReport);
	     	    	
	     	    	
	     	  return emailstatus;  	
	     	    	
	}
	
	
	
	public String utilizerInvoiceMail(int day,int month,int year,String uniqueInvoiceId,String paymentAmount,String currentPlanName,String reciverName,String reciverEmailId,String requestedPlanName)
	{
		
		String resetStatus = "";
		
		 ResourceBundle resource = ResourceBundle.getBundle("resources/planChangeMail");
    	 String clenseeLogo = resource.getString("clenseeLogo");
    	 
		ResourceBundle clenseeInvoiceDetails = ResourceBundle.getBundle("resources/clenseeInvoiceDetails");
		     
	     String invoicesubject=clenseeInvoiceDetails.getString("invoicesubject");
	     String invoiceleftName=clenseeInvoiceDetails.getString("invoiceleftName");
	     String invoicegenerated=clenseeInvoiceDetails.getString("invoicegenerated");
    
    	StringBuilder invoicetext = new StringBuilder();
       
    	invoicetext.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
    	invoicetext.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");					       
    	invoicetext.append("<body style='font: 14px/1.4 arial;'>");							        
    	
    	invoicetext.append("<div style='background-color: #eee;margin: 0 auto;width: 800px;'>");	
    	
    	invoicetext.append("<div style='background: #222 none repeat scroll 0 0;color: white;letter-spacing: 20px;margin: 6px 0;padding: 8px 0;text-align: center;'>INVOICE</div>");
    	
	    invoicetext.append("<div style='background-color:#ccc;'><div style='text-align: left;'>");
    	invoicetext.append("<img src=" +clenseeLogo+ " alt='logo' />");
    	invoicetext.append("</div>");
    	invoicetext.append("<div style='float: right;font-size: 20px;font-weight: bold;margin-top: -40px;' >"+invoiceleftName+"</div>");								     	    							     	  
    	invoicetext.append("</div>");
    	
    	invoicetext.append("<div style='overflow: hidden;margin-top: 42px;'>");
    	
    	invoicetext.append("<div style='float: left;font-size: 15px;' ><b>Name:</b>"+reciverName+"</div><br><br>");
    	invoicetext.append("<div style='float: left;font-size: 15px;' ><b>Email Id:</b>"+reciverEmailId+"</div>");
    	
    	invoicetext.append("<table style='float: right;width: 300px;border-collapse: collapse;'>");
    	invoicetext.append("<tr>");
    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'>Invoice #</td>");
    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'>"+uniqueInvoiceId+"</td>");
    	invoicetext.append("</tr>");
    	invoicetext.append("<tr>");
    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;' >Date</td>");
    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'>"+day+"/"+month+"/"+year+"</td>");
    	invoicetext.append("</tr>");
    	
    	invoicetext.append("</table>");
    	
    	invoicetext.append("</div>");
    									     	    	
    	invoicetext.append("<table  style='border: 1px solid black;margin: 30px 0 0;width: 100%;border-collapse: collapse;'>");
    	invoicetext.append("<tr>");
    	invoicetext.append("<th style='border: 1px solid black;padding: 5px;'>Item</th>");
    	invoicetext.append("<th style='border: 1px solid black;padding: 5px;'>Description</th>");
    	invoicetext.append("<th style='border: 1px solid black;padding: 5px;'>Unit Cost</th>");
    	invoicetext.append("</tr>");
    	invoicetext.append("<tr>");					     	    	
    	invoicetext.append("<td  style='border: 1px solid black;padding: 5px;'><div class='delete-wpr' style=''><div style=''>Plan Migration</div></div></td>");
    	invoicetext.append("<td  style='border: 1px solid black;padding: 5px;'><div style=''>Plan migration from "+currentPlanName+" to "+requestedPlanName+"</div></td>");
    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'><div class='cost' style=''>Rs. "+paymentAmount+"</div></td>");
    	invoicetext.append("</tr>");
    	invoicetext.append("<tr><td colspan='5' style='border: 1px solid black;padding: 5px;'></td></tr>");
    	invoicetext.append("<tr>");
    	invoicetext.append("<td  colspan='2' style='border: 1px solid black;padding: 5px;'><b>Taxes</b></td>");
    	invoicetext.append("<td  style='border: 1px solid black;padding: 5px;'><div id='subtotal' style=''>Inclusive</div></td>");
    	invoicetext.append("</tr>");
    	invoicetext.append("<tr>");
    	invoicetext.append("<td colspan='2' style='border: 1px solid black;padding: 5px;'><b>Total</b></td>");
    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'><div id='total' style=''>Rs. "+paymentAmount+"</div></td>");
    	invoicetext.append("</tr>");
    	invoicetext.append("<tr>");
    	invoicetext.append("<td colspan='2'  style='border: 1px solid black;padding: 5px;'><b>Amount Paid</b></td>");
    	invoicetext.append("<td style='border: 1px solid black;padding: 5px;'><div id='paid' style=''><b>Rs. "+paymentAmount+"</b></div></td>");
    	invoicetext.append("</tr>");					     	    	
    	invoicetext.append("</table>");
    	
    	invoicetext.append("<div  style='margin: 20px 0 0;text-align: center;'>");
    	invoicetext.append("<div>Invoice generated by <b>"+invoicegenerated+"</b></div>");
    	invoicetext.append("</div>");	
    	invoicetext.append("</div>");	
    	
    	invoicetext.append("</body>");
    	invoicetext.append("</html>");
    	
	try 
	{
		SendEmailUtil emailStatus = new SendEmailUtil();
		resetStatus = emailStatus.getEmailSent(reciverEmailId,invoicesubject,invoicetext.toString());
	} 
	catch (IOException e) 
	{							
		e.printStackTrace();
	}
	
	EmailSentStatusModel emailReport = new EmailSentStatusModel();
 	
	
	if(resetStatus.equals("success"))
	{	    		
		emailReport.setEmail_id(reciverEmailId);
		emailReport.setSubject(invoicesubject);
		emailReport.setMessage_body(invoicetext.toString());
		emailReport.setSent_date(new Date());
		emailReport.setSent_report("0");
	}
	else
	{
		emailReport.setEmail_id(reciverEmailId);
		emailReport.setSubject(invoicesubject);
		emailReport.setMessage_body(invoicetext.toString());
		emailReport.setSent_date(new Date());
		emailReport.setSent_report("1");	    			    		
	}
	
	String emailstatus = ProfileinfoDao.setEmailStatusReport(emailReport);		
		
		return emailstatus;
		
		
	}

	
	
	
	
}
