package com.kyc.service;


import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;










import com.kyc.bean.DocumentHierarchyBean;
import com.kyc.bean.IndSignupBean;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.DocumentHierarchyModel;
import com.kyc.model.DocumentModel;
import com.kyc.model.DomainHierarchyModel;
import com.kyc.model.DomainLevelModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.Employee;
import com.kyc.model.IndSignupModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.PatternDetailsModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.SscDocModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;

public interface AdminPanelService {
	
	
	public List<String> getAllUsersDetails();
	
	public String getUtiRefId(int utiUnqId);
	
	public String findtypeGroup(String type);
	
	public String find_acctypeGroup(String type);
	
	public String findSub_category(String type);
	
	public String savePattern(String pattern,String patternName);	
	
	public String getPatternNames();
	
	public String getPlan_Name();
	
	public String findPattern(String Pattern_Name);
	
	public List<PatternDetailsModel> patternforDelete();
	
	public String deletePattern(int p_id);
	
	public String searchUtiliser(String searchData,String searchType);
	
	public String upDatePattern(String pattern_name,String pattern);
		
	public String saveUtiliserDetails(UtiliserProfileModel utiProfile);
	
	public int findPatternId(String pattern_name);
	
	public List<UtiliserProfileModel> UtiliserProfileDetails(String utiid);
	
	public int findUtiId(String utiid);
	
	public String findKycid(int id);
	
	public String findPassword(int id);
	
	public String findDomain(int id);
	
	public String changestatus(IndSignupModel changestatus);
    
    public List<String> searchindividualplan(IndSignupModel searchprofile);

    public List<String> searchindividualname(IndSignupModel searchprofile);

    public List<String> searchindividualkycid(IndSignupModel searchprofile);
    
    public String getkycHierarchy();
    
    public String saveKycHierarchy(DocumentHierarchyModel kycHierarchy);
    
    public int getHierId(String kycHierarchy);
    
    public String getFinancialHierarchy();
    
    public String getAcademicHierarchy(String acaDocType);
    
    public String getAcademicDocType();
    
    public String saveDomain(DomainLevelModel domainModel);
    
    public String getDomainName();
    
    public String getDomainValue(String domainType);
    
    public String getSubDomainValue(String domainType);
    
    public String saveDomainHierarchy(String domainLavel,String domainType,String enterValue);
    
    public String getEntireHierarchy(String domainLevel,String domainValue);
    
    public String getDeleteHierarchy(String domainLevel,String domainValue);
    
    public String getDomainStatus(String domainLevel,String domainValue);
    
    public String getSubDomainForCreateUtiliser(String domainName);   
    
    public String getHeirarchyFromSubDomain(int domainId,String currentLevel);
    
    public String getSelectedNames(String selectedList);
    
    public int getUtiliserId(UtiliserProfileModel utiProfile);
    
    public String getfinancialSubType();
    
    public String getHierarchyComponents(String hierarchyType);
    
    public String getPlanNameAsPerPlanDomain(String planDomain);
    
    public String saveNotifyDetails(KycNotificationModel notifyModel);
    
    public List<IndSignupModel> getIndividualDetailsViaKycid(String searchedValue);
    
    public List<IndSignupModel> getIndividualDetailsViaPlan(String searchedValue);
    
    public List<IndSignupModel> getIndividualDetailsViaName(String searchedValue);
    
    public List<EmailSentStatusModel> getPendingEmail(); 
    
    public List<SmsSentStatusModel> getPendingSMS(); 
    
    public List<EmailSentStatusModel> getPendingEmailDetails(int email_uniqueId); 
	
    public List<SmsSentStatusModel> getPendingSMSDetails(int sms_uniqueId); 
    
    public String getUpdateEmailDetails(int email_uniqueId);
    
    public String getUpdateSmSDetails(int sms_uniqueId);
    
    public String getdtailsviakycId(String kycId);
    
    public String getPatternName(int patternId);
    
    public String getPassword(int utiUnqId);
    
    public String getPlanDomain(int utiUnqId);
    
    public String getUtilizerPlanName(int utiUnqId);
    
   	public String getUtilizerDomain(int utiUnqId);
   	
   	public Date getUtiRegDate(int utiUnqId);
   	
	public String getUtiProfilePic(int utiUnqId);
	
	public String saveUtilizerPlanDetails(UtilizerPlanDetailsModel utiPlanModel);
	
	public String saveUtilizerProfilePhoto(UtilizerProfilePhotoModel utiProfilePhoto);
	
	public String getPatternDetailAsperPatterName(String patternName);
	
	public String updatePattern(String existPatternName,String newpattern,String patternDetails);
	
	public int getLastUtilizerIndId();
	
	public String deleteAcademicHierarchy(String hierarchyName);  
	
	public String updateUtilizerDetails(UtiliserProfileModel utiProfile);
	
	public String updateUtiliserProfilePhoto(UtiliserProfileModel utiProfile);
	
	public String getClenseeYearList();
	
	public List<ContactUsModel> getAllContactUsDetails(String todaysDateFormat);
	
	public String getContactMssageByConId(int contactId);
	
	public List<ContactUsModel> getSearchContactUsDetails(String searchType,String searchValue);
}
