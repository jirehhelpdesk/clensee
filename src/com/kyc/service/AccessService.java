package com.kyc.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;

import com.kyc.model.AccessDetailsModel;
import com.kyc.model.CodesDetailsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.NonKycUserCodeDetailsModel;
import com.kyc.model.UtiliserProfileModel;

public interface AccessService {

	
	public String findUser(IndSignupModel codedetail);
	
	public String checkIndAccessStatus(String viewedkycid,String ownKycId);
	
	public List<String> findDocumentforKYC(HttpServletRequest request);
	
	public List<String> findDocumentforAcademic(HttpServletRequest request);
	
	public List<String> findDocumentforEmployement(HttpServletRequest request);	
	
	public List<String> findDocument(HttpServletRequest request);
	
	public List<String> findRecipentDetail(HttpServletRequest request);
	
	public List<String> findAsperType(IndSignupModel codedetail);
	
	public String findCrName(String kycid);
 	
	public String saveCodedetails(CodesDetailsModel codemodel);
	
	public List<IndSignupModel> viewUser(IndSignupModel individual);
	
	public List<CodesDetailsModel> findShareedCode(IndSignupModel individual);
	
	public String servicePatternforNonKycUsr(int non_kyc_code);
	
	public String servicePattern(int code);
	
	public int findid(String kycid);
	
	public int findyear(String docname,int ind_id);
	
	public String getDomainHir(String domtype);
	
	public String getDomain();
	
	public String saveAccessDetails(AccessDetailsModel accdetail);
	
	public String findUtiKycId(String ptname);
	
	public String findIndKycID(int acc_giver);
	
	public int getPatternId(String acc_reciver);
	
	public String getPatternLogic(int prtn_id);
	
	public int findInd_Id(String kycid);
	
	public String findDocView(String docname);
	
	public List<CodesDetailsModel> getAccessDetails(String ownId);
	
	public List<CodesDetailsModel> getIndAccessDetails(String ownId);
	
	public List<CodesDetailsModel> findHisInd(String kycid);
	
	public List<NonKycUserCodeDetailsModel> getCodeDetailsforNonKycUser(int ind_Id);
	
	public List<AccessDetailsModel> findHisUti(String kycid);
	
	public String revokeUtiliser(String kycid,Date date,String access_status);
	
	public String findName(String acc_sender);
	
	public String getUti_HisDoclist(String kycid,String Date,int ind_Id);
	
	public String getRecevierStatus(String receiver,String sender);

	public String findDocType(String docname,int ind_id); 
	
	public String getProfilePic(int kycInd_id);
	
	public String getFirstName(int kycInd_id);
	
	public String getUtiAccessDetails(String ownId);
	
	public String getPatternfromId(int patternId);
	
	public String getUtiliserAccessId(int utiliserId); 
	
	public String getValidateNonKycUserEmailId(String SharedEmailId);
	
	public int getLastGeneratedCodeId();
	
	public String getSenderName(int ind_id);
	
	public String getSaveNonKycCodeDetails(NonKycUserCodeDetailsModel nonUserModel);
	
	public String changeStatusOfUtiliser(String uti_unique_id,Date date,String access_status);
	
	public int orgApplyCodeCount(int code);
	
	public String getReciverName(int reciver_Ind_id);
	
	public String getReciverEmailId(int reciver_Ind_id);
	
	public String getReciverMobileNo(int reciver_Ind_id);
	
	public String getLastGeneratedCode();
	
	public String patternDetailsAsPerRecord(String searchResult,int ind_id);
	
	public List<UtiliserProfileModel> getUtilizerDetails(String utiId);
	
	public String deactiveLastAccess(AccessDetailsModel accmodel);
	
	public String getLatestDocumntNameOfThisType(String documentType,int IndId);
	
	public String  getUtilizerFullName(String Kyc_id);
	
	public String getIndNameFromKycId(String Kyc_id);
	
	public String getDocumentAvailablityViaThePattern(String searchResult,int ind_id);
}
