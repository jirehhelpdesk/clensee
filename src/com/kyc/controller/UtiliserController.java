package com.kyc.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.AuthenticationFailedException;
import javax.mail.IllegalWriteException;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.bean.BasicDetailBean;
import com.kyc.bean.DocumentDetailsBean;
import com.kyc.bean.DocumentsBean;
import com.kyc.bean.IndSignupBean;
import com.kyc.bean.ProfilePictureBean;
import com.kyc.bean.RegHistoryBean;
import com.kyc.bean.SocialaccBean;
import com.kyc.bean.SummarydetailBean;
import com.kyc.bean.UtiliserDetailsBean;
import com.kyc.bean.UtiliserProfileBean;
import com.kyc.bean.UtiliserUpdateBean;
import com.kyc.model.AccessDetailsModel;
import com.kyc.model.BasicDetail;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.DocumentModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.Matrial;
import com.kyc.model.ProfilePictureModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.ResetPasswordModel;
import com.kyc.model.Socialacc;
import com.kyc.model.Summarydetail;
import com.kyc.model.UtiliserDetailsModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtiliserUpdateModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPasswordRequestModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;
import com.kyc.service.AccessService;
import com.kyc.service.DocService;
import com.kyc.service.IndSignupService;
import com.kyc.service.PlanService;
import com.kyc.service.ProfileinfoService;
import com.kyc.service.UtiliserService;
import com.kyc.util.AccessControl;
import com.kyc.util.FileEncryptionDecryptionUtil;
import com.kyc.util.PasswordEncryptionDecryption;
import com.kyc.util.SendEmailUtil;
import com.kyc.util.SendSms;


@Controller 
public class UtiliserController {

	String utiid="";
	
	@Autowired
	private IndSignupService signupService;
		
	@Autowired
	private UtiliserService utiservice;
	
	@Autowired
	private PlanService PlanService;
	
	@Autowired
	private ProfileinfoService profileinfoService;
	
	@Autowired
	private AccessService accservice;

	@Autowired
	private DocService docservice;
	
	String kycId  = "";
	
	/*
	  @Autowired
      private JavaMailSender mailSender;
	*/
	
	static final Logger LOGGER = Logger.getLogger(UtiliserController.class);
    
	
	private static Logger log = Logger.getLogger(UtiliserController.class);
	FileAppender loggerfile;	 
		
	
	ResourceBundle resource = ResourceBundle.getBundle("resources/utilizerRequest");
	
	 String headingImage=resource.getString("headingImage");
	 String belowleftImage=resource.getString("belowleftImage");
	 String belowrightImage=resource.getString("belowrightImage");
	 String mailicon=resource.getString("mailicon");
	 String contacticon=resource.getString("contacticon");
	 
	 String emailHeading=resource.getString("emailHeading");
	 String contactNo=resource.getString("contactNo");
	 String contactEmail=resource.getString("contactEmail");
	 
	 String utilizerSubject=resource.getString("signupsubject");
	 String utilizerSignupmessage1 = resource.getString("signupmessage1");
	 String utilizerSignupmessage2 = resource.getString("signupmessage2");
	 String utilizerSignupmessage3 = resource.getString("signupmessage3");
 
	 String copyright = resource.getString("copyright");
	 String termscondition = resource.getString("termscondition");
	 String privacyPolicy = resource.getString("privacyPolicy");
	 
	 String facebookicon=resource.getString("facebookicon");
	 String twittericon=resource.getString("twittericon");
	 String googleicon=resource.getString("googleicon");
	 String linkedicon=resource.getString("linkedicon");
	 
	 String facebookLink=resource.getString("facebookLink");
	 String twitterLink=resource.getString("twitterLink");
	 String googlePlus=resource.getString("googlePlus");
	 String linkedIn=resource.getString("linkedIn");
	
	 String commonUrl=resource.getString("commonUrl");
	 		
	String smsmessage = resource.getString("smsmessage");
	String username = resource.getString("userName");
	String password = resource.getString("password");
	String senderId = resource.getString("senderId");
	
	 String clenseeLogo = resource.getString("clenseeLogo");
	 String clickhere = resource.getString("clickhere");
	 String actionUrl = resource.getString("actionUrl");
	 
	 
    SendSms smsUtilObject = new SendSms();
    
	
	@SuppressWarnings("static-access")
	@RequestMapping(value="/notifyToClenseeAdmin",method = RequestMethod.POST)
	public @ResponseBody String getReporttoAdmin(HttpServletRequest request) throws IOException
	{
		String reportStatus = "";
		
		String fullName = request.getParameter("fullname");
		String orgName = request.getParameter("orgname");
		String locName = request.getParameter("location");
		String emailId = request.getParameter("emailid");
		String mobileno = request.getParameter("mobileno");
	 
			 	
			    loggerfile = new FileAppender(new SimpleLayout(),"utilizerReportToKyc.log");
			    log.addAppender(loggerfile);
			    loggerfile.setLayout(new SimpleLayout());
			    
			    log.info(fullName+" had report to Clensee Admin at "+new Date());
			    log.info(fullName+" required Information are  "+emailId+", from "+orgName+"/"+locName+"for contact no "+mobileno);
			    
			    String url = commonUrl; // This is the URL which individual click to activate the account
		        String target = "_blank";
		        	
		      
		        StringBuilder text = new StringBuilder();
		        		        		        
		        /* Sending Emails to Register user */
			       
		        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
		        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
		       
		        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
		       
		        /* Header */
		        
		        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
		        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
		        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
		        text.append("</tr></table></td></tr></table>");
		       
		        /* End of Header */
		        
		        /* Header-2 */
		        
		        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
		        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
		        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
		        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
		        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
		        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
		        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
		        text.append("</p></td></tr></table></td></tr></table>");
		        
		        /* End of Header-2 */
		        
		        
		        /* BODY PART */
		        
		        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
		        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
		        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
		        text.append("<h3 style='color: #004879;'>Dear "+fullName+",</h3>");
		        text.append("<p>"+utilizerSignupmessage1+"<br>");
    	        text.append("<p>"+utilizerSignupmessage2+"<br>");
    	        text.append("<p>"+utilizerSignupmessage3+"<br>");
    	        text.append("</p>");
    	        		        
		        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
		        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
		        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>CLENSEE</td>");
		        text.append("</tr></table></td></tr>");
		        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
		        text.append("</table> ");
		        		        
		        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
		        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
		        
		        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
		        
		        text.append("<tr><td>");
		        
		        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
		        text.append("</td></tr></table>");
		       
		        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
		        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
		        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
		        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
		        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
		        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
		        
		        
		        text.append("</tr>");
		        text.append("</table></td></tr></table></td></tr></table>");
		        text.append("</body>");
		        text.append("</html>");
		       
		        /* END OF BODY PART */
		        
		        
		       /* End of Sending Emails to Register user */
		        		         
		       	            
		        SendEmailUtil obj = new SendEmailUtil();
		        
		        obj.getEmailSent(emailId, utilizerSubject, text.toString());
		        		       
		        reportStatus += "success"; 		       
		        return reportStatus;
	}
	
	@RequestMapping(value = "/utiAuthenticateLogin", method = RequestMethod.GET)
	public ModelAndView getValidateUtiliserbyGET(HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		
		session = request.getSession(false);
		
		if (session.getAttribute("uti_Ind_id") == null)
		{						 
			 request.setAttribute("status","Your session has expired please login to use the portal !");			
			 session.removeAttribute("uti_Ind_id");			  
			 return new ModelAndView("login_utiliser");
		}
		else if((Integer.toString((int)session.getAttribute("uti_Ind_id"))).equals(null))
		{
			
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("uti_Ind_id");
			 //session.invalidate();
			 
			 return new ModelAndView("login_utiliser");
		}
		else 
		{	
            session.setAttribute("uti_Ind_id",utiservice.getUtiIndId((String)session.getAttribute("KycId")));	
			
			int uti_Indid = (int) session.getAttribute("uti_Ind_id");
			model.put("viewBasicProfile", PrepareBasicDetails(utiservice.getUtiBasicDetails(kycId)));			
			
			model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
			
			session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
			System.out.println("My session id is " + session.getAttribute("USER_SESSION_ID"));			 
			
			String uti_kycid = (String)session.getAttribute("KycId");
			int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
			int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
			
			request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
			request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
			
			
			return new ModelAndView("uti_profile",model);
        }
	}
	
	 @SuppressWarnings("unused")
	 @RequestMapping(value ="/checksessionforUtilizer", method = RequestMethod.POST)
	 public @ResponseBody String checkSessioninIndividualSide(HttpServletRequest request,HttpSession session) {
				
		    session = request.getSession(false);
			
			if (session.getAttribute("uti_Ind_id") == null)
			{			
				System.out.println("So session Value when not available1="+(String)session.getAttribute("uti_Ind_id"));
				
				return "Not Exist";		
			}			
			else if(session == null)
			{
				return "Not Exist";			
			}
			else
			{				
				return "Exist";									
			}
	 }
	
	 
	@SuppressWarnings({ "unused", "resource" })
	@RequestMapping(value = "/utilizerProfilePhoto", method = RequestMethod.GET)
	@ResponseBody public byte[] previewprofileOther(HttpServletRequest request,HttpSession session) throws IOException {
			
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
		String utilizerProfilePhoto = fileResource.getString("utilizerProfilePhoto");	
		
		File ourPath = new File(utilizerProfilePhoto);		
				
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/");		
		fPath = fPath.append(request.getParameter("indId")+"/"+ request.getParameter("fileName"));				
		
		try 
		{						
				FileInputStream docdir = new FileInputStream(fPath.toString());												
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{	
					System.out.println("Resource not found !");
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} 
		catch (IOException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	

	
	 
	@SuppressWarnings({ "unused", "resource" })
	@RequestMapping(value = "/viewUtilizerProfilePhotoinPopup", method = RequestMethod.GET)
	@ResponseBody public byte[] viewUtilizrProfilePhoto(HttpServletRequest request,HttpSession session) throws IOException {
			
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
		String utilizerProfilePhoto = fileResource.getString("utilizerProfilePhoto");	
		
		File ourPath = new File(utilizerProfilePhoto);		
				
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/");		
		fPath = fPath.append((int)session.getAttribute("uti_Ind_id")+"/"+ request.getParameter("fileName"));				
		
		try 
		{						
				FileInputStream docdir = new FileInputStream(fPath.toString());												
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{	
					System.out.println("Resource not found !");
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} 
		catch (IOException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	@RequestMapping(value ="/saveProfilepictureofUtilizer",method = RequestMethod.POST)
	public @ResponseBody String saveProfilePicture(HttpSession session,HttpServletRequest request,
			@RequestParam CommonsMultipartFile[] Passportsize) throws IllegalStateException, IOException
	{
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
		String utilizerProfilePhoto = fileResource.getString("utilizerProfilePhoto");			
		File ourPath = new File(utilizerProfilePhoto);		
				
		int ind_kyc_id=   (int) session.getAttribute("uti_Ind_id");		
		
		String picName= "";		 
		
		File fld = new File(ourPath.getPath()+"/"+ind_kyc_id);
		fld.mkdirs();
		
		String loc = ourPath.getPath()+"/"+ind_kyc_id;
		
		StringBuilder finalstring=new StringBuilder();
			
		String documentName = "";
		float datasizeKB=(float)0.0; 
		
		if (Passportsize != null && Passportsize.length > 0)
        {			
            for (CommonsMultipartFile aFile : Passportsize)            
            {             
                if (!aFile.getOriginalFilename().equals(""))                 
                {                  	
                    aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
                   
                    File datasize = new File(loc+"/"+aFile.getOriginalFilename());
                    float bytes = datasize.length();
                    datasizeKB = datasizeKB + bytes/1024;      
                    
                    String extension = "";
                    
                    int i = aFile.getOriginalFilename().lastIndexOf('.');
                    if (i > 0)
                    {
                        extension = aFile.getOriginalFilename().substring(i+1);                             
                    }
                                                       
                   File store = new File(loc);
                   File[] storelist = store.listFiles();
                   
                   String flname= ind_kyc_id+"-Profile-Photo_1";
                   boolean check=true;
                   
                   for(int f=0;f<50;f++)
                    {                 	   
                	   if(check==true)
                	   {
                    	  for(int g=0;g<storelist.length;g++)
                    	   {                       		  
                    	      if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
                               	{                   		      
                    	    	    check=false;                    	    	    
                    	        }
                    	   }
                    	  if(check==true)
                       	       {                     		   
                       		      File floldname = new File(loc+"/"+aFile.getOriginalFilename());                   
                                  File flnewname = new File(loc+"/"+flname+"."+extension);
                                  floldname.renameTo(flnewname);                                 
                                  documentName = flnewname.getName();
                                  break;
                       	       }             	                         	                     	                     	                    	  
                	   }                	  
                	   if(check==false)
                	   {
                		   String[] ary=flname.split("_");
                		   
                		   for(int k=1;k<ary.length;k++)
                		   {  
         	                  int l=Integer.parseInt(ary[k])+1;
         	                  String s=Integer.toString(l);
         	                  flname="";
         	                  flname= ind_kyc_id+"-Profile-Photo_".concat(s);
         	                  check=true;
                		   }
                	    }                	                   	   
                    }                   
                }
            }
        }
		
		UtilizerProfilePhotoModel profilePhotoModel = new UtilizerProfilePhotoModel();
		profilePhotoModel.setUti_ind_id(ind_kyc_id);
		profilePhotoModel.setUti_profile_photo(documentName);
		profilePhotoModel.setCr_date(new Date());		
		
	    String status = utiservice.saveProfilePhotoFromUtilizer(profilePhotoModel);
	    
	    utiservice.updateProfPhotoDetails(documentName,ind_kyc_id);
	    
	    if(status.equals("success"))
   	    {	    	
	    	// Tracking Activity Details
	        
		     String uti_kycid = (String)session.getAttribute("KycId");
		     int uti_indid = utiservice.findUtiid(uti_kycid);
	         UtilizerActivityModel actModel = new UtilizerActivityModel();
			 actModel.setUti_ind_id(uti_indid);
			 actModel.setActivity_type("Profile Photo Change");
			 actModel.setActivity_details("Profile Photo changed on "+new Date());
			 actModel.setActivity_date(new Date());
			         
			 utiservice.saveActivityDetails(actModel);

             // End Of Tracking Activity Details			
   	    }	
	    
		return "";					
	}
	
	
	@RequestMapping(value = "/utiAuthenticateLogin", method = RequestMethod.POST)
	public ModelAndView getValidateUtiliser(HttpServletRequest request,HttpSession session)
	{
		String kycid = request.getParameter("kycid");
		String password = request.getParameter("password");
		utiid = request.getParameter("kycid");
		Map<String, Object> model = new HashMap<String, Object>();
		
		if(utiservice.utiliserAuthenticate(kycid,password).equals("success"))
		{
			if(kycid.contains("@"))
			{
				kycId = utiservice.getUtilizerKycId(kycid);
				session.setAttribute("KycId",kycId);
			}
			else
			{
				kycId = kycid;
				session.setAttribute("KycId",kycid);				
			}	
			
			session.setAttribute("uti_Ind_id",utiservice.getUtiIndId(kycId));	
			
			int uti_Indid = (int) session.getAttribute("uti_Ind_id");
			model.put("viewBasicProfile", PrepareBasicDetails(utiservice.getUtiBasicDetails(kycId)));			
			
			model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
						
			session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
			
			// Migrate Plan After one year 
			
			 String planname= PlanService.getUtilizerplanname(uti_Indid);			
			 
			 Date utiPlanActivatedDate = PlanService.getUtiActivatedPlanDate(uti_Indid);
			
			 if(!planname.equals("Basic"))
				{
				 
					Date register_Date = utiPlanActivatedDate;
					Date todays_Date = new Date();
					
					long diff = 0;
					   
				   Calendar c1 = Calendar.getInstance();		   
				   c1.setTime(register_Date);		 
				   Calendar c2 = Calendar.getInstance();		  
				   c2.setTime(todays_Date);		  
				   long ms1 = c1.getTimeInMillis();
				   long ms2 = c2.getTimeInMillis();		  
				   diff = ms2 - ms1;
				  
				   int diffInDays = (int) (diff / (24 * 60 * 60 * 1000));
				  
					  if(diffInDays>=364)
					  {
						 
						  PlanService.migrateUtilizerPlan("Basic", uti_Indid);
						  
						   UtilizerPlanDetailsModel utiPlanModel = new UtilizerPlanDetailsModel();
						   
						   
							utiPlanModel.setUti_ind_id(uti_Indid);				
							utiPlanModel.setUti_activate_plan("Basic");
							utiPlanModel.setCr_date(new Date());
							
							String planActiveStatus = utiservice.migrateUtilizerPlanDetails(utiPlanModel);
						  
						// Tracking Activity Details
					        
						     String uti_kycid = (String)session.getAttribute("KycId");
						     int uti_indid = utiservice.findUtiid(uti_kycid);
					         UtilizerActivityModel actModel = new UtilizerActivityModel();
							 actModel.setUti_ind_id(uti_indid);
							 actModel.setActivity_type("Plan Change");
							 actModel.setActivity_details("Your existing plan validity has over,So your Plan has migrated to Basic on "+new Date());
							 actModel.setActivity_date(new Date());
							         
							 utiservice.saveActivityDetails(actModel);

				         // End Of Tracking Activity Details
					  }
				}
				
	         // End of Migrate Plan if 1 Year complete 
			 			 
			
			 if(!request.getParameter("searchedIndividualName").equals(""))
				{
					int searchedInd_id = Integer.parseInt(request.getParameter("searchedIndividualName"));
					session.setAttribute("beforeSearchedIndId", searchedInd_id);	
					
					String uti_kycid = (String)session.getAttribute("KycId");
					int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
					int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
					
					request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
					request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
					
					
					
				    return new ModelAndView("redirect:/validateprofileviewingForutilizerBeforeLogin.html");
				}
			 else
			 {
				    String uti_kycid = (String)session.getAttribute("KycId");
					int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
					int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
					
					request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
					request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
					
				 return new ModelAndView("uti_profile",model);
			 }			 			
		}
		else
		{
			request.setAttribute("errormessage","Access is denied due to invalid credentials.");
			return new ModelAndView("login_utiliser");
		}		
	}
	
	
	@RequestMapping(value = "/checkemailIdforForgetPassword", method = RequestMethod.POST)
	public @ResponseBody String checkemailIdforForgetPassword(HttpServletRequest request,HttpSession session)
	{
		String status = "";
		String requestedEmailId = request.getParameter("emailId");
		
		status = utiservice.checkEmailIdAvailabilty(requestedEmailId);
		
		return status;
	}
	
	@RequestMapping(value="/utiresetPassword",method=RequestMethod.GET)
    public ModelAndView resetPasdword(HttpServletRequest request)
    {   	
   	  String reserVariable = request.getParameter("wdvrgtqsusb");    	     	  	     	   			 
   	  request.setAttribute("uniqueVariable", reserVariable);
   	 
   	  String checkStatus = utiservice.checkPasswordStatus(reserVariable);//signupService.getCheckStatus(reserVariable);
   	 
   	  request.setAttribute("resetStatus", checkStatus);
   	 
   	  return new ModelAndView("utiliser_reset_password");
    } 
	
	 @RequestMapping(value="/formatpasswordForUtilizer",method = RequestMethod.POST)
	 public @ResponseBody String formatpasswordForUtilizer(HttpServletRequest request) throws IOException, IllegalWriteException, AuthenticationFailedException, SendFailedException
		{
		   String resetStatus = "";
		   String emailId = request.getParameter("emailsignup");
		   
		   String resetVariable = utiservice.getRefIdViaEmailId(emailId);
		   
		   
		   String changedUrl = PasswordEncryptionDecryption.getEncryptPassword(resetVariable);
		   String encryptedUrl = changedUrl.replaceAll("[^\\p{Alpha}\\p{Digit}]+","");
		   		   
		   int uti_Ind_id = utiservice.getUtilizerIdVaiUniqueId(resetVariable);
		   utiservice.updateVisibilityUrl(uti_Ind_id,encryptedUrl);
		   
		   
		   ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
		   String serveros=loggerFileDirectory.getString("serveros");
			
			
		    ResourceBundle resource = ResourceBundle.getBundle("resources/resetPasswordSendingMail");
		    
		     String headingImage=resource.getString("headingImage");
	    	 String belowleftImage=resource.getString("belowleftImage");
	    	 String belowrightImage=resource.getString("belowrightImage");
	    	 String mailicon=resource.getString("mailicon");
	    	 String contacticon=resource.getString("contacticon");
	    	 
	    	 String emailHeading=resource.getString("emailHeading");
	    	 String contactNo=resource.getString("contactNo");
	    	 String contactEmail=resource.getString("contactEmail");
	    	 
	    	 String subject=resource.getString("signupsubject");
			 String messageBody=resource.getString("signupmessage");		
			 String urlLinktoActivate = resource.getString("aftermessageextraline");
			 
	    	 String copyright=resource.getString("copyright");
	    	 String termscondition=resource.getString("termscondition");
	    	 String privacyPolicy=resource.getString("privacyPolicy");
	    	 
	    	 String facebookicon=resource.getString("facebookicon");
	    	 String twittericon=resource.getString("twittericon");
	    	 String googleicon=resource.getString("googleicon");
	    	 String linkedicon=resource.getString("linkedicon");
	    	 
	    	 String facebookLink=resource.getString("facebookLink");
	    	 String twitterLink=resource.getString("twitterLink");
	    	 String googlePlus=resource.getString("googlePlus");
	    	 String linkedIn=resource.getString("linkedIn");
			
		     String commonUrl = resource.getString("commonUrl");
		    
	         String target = "_blank";
		   	 String clenseeLogo = resource.getString("clenseeLogo");
			 String clickhere = resource.getString("clickhere");
			 			
			 String signupwelcome = resource.getString("signupwelcome");
			
			 
		 
			String url = ""+commonUrl+"utiresetPassword.html?wdvrgtqsusb="+encryptedUrl+"";			
		    StringBuilder text = new StringBuilder();
		        	 
		        		        
			    /* Sending Emails to Register user */
			       
		        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
		        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
		       
		        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
		       
		        /* Header */
		        
		        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
		        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
		        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
		        text.append("</tr></table></td></tr></table>");
		       
		        /* End of Header */
		        
		        /* Header-2 */
		        
		        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
		        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
		        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
		        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
		        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
		        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
		        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
		        text.append("</p></td></tr></table></td></tr></table>");
		        
		        /* End of Header-2 */
		        
		        
		        /* BODY PART */
		        
		        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
		        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
		        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
		        text.append("<h3 style='color: #004879;'>Dear Clensee user,</h3>");
		        text.append("<p style='text-align: justify'>"+signupwelcome+"</p>");
		        text.append("<p style='text-align: justify'>"+messageBody+"</p><br><p>");
		        text.append(""+urlLinktoActivate+"<br></p><br>");
		        text.append("<a href="+url+" target="+target+" style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
		           		        
		        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
		        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
		        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>CLENSEE</td>");
		        text.append("</tr></table></td></tr>");
		        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
		        text.append("</table> ");
		        
		        
		        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
		        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
		        
		        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
		        
		        text.append("<tr><td>");
		        
		        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
		        text.append("</td></tr></table>");
		       
		        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
		        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
		        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
		        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
		        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
		        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
		        
		        
		        text.append("</tr>");
		        text.append("</table></td></tr></table></td></tr></table>");
		        text.append("</body>");
		        text.append("</html>");
		       
		       
		        /* END OF BODY PART */
		        
		        
		        /* End of Sending Emails to Register user */ 
		          
	        SendEmailUtil obj = new SendEmailUtil();
	        
			resetStatus = obj.getEmailSent(emailId,subject,text.toString());
			
			request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
	     
	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
	    	
	    	if(resetStatus.equals("success"))
	    	{		    		
	    		emailReport.setEmail_id(emailId);
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("0");
	    	}
	    	else
	    	{
	    		emailReport.setEmail_id(emailId);
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("1");	    			    		
	    	}
	    	
	    	profileinfoService.setEmailStatusReport(emailReport);
	    	
	    	// Store Reset password Info in Db
			
	    	UtilizerPasswordRequestModel resetPW = new UtilizerPasswordRequestModel();
		        
		     resetPW.setUti_reference_id(encryptedUrl);
		     resetPW.setUti_status("0");
		     resetPW.setRequested_date(new Date());
		       
		     profileinfoService.saveUtiResetPasswordModel(resetPW);
		     
		    // EOC Store Reset password Info in Db
		     
		   return resetStatus;
		}
	
	 @RequestMapping(value="/utiResetPassword",method=RequestMethod.POST)
     public @ResponseBody String indResetPassword(HttpServletRequest request,HttpSession session)
     {
    	 String uniqueVariable = request.getParameter("uniqueVariable");
    	 String password = request.getParameter("newPassword");
    	 String encryptedPassword = PasswordEncryptionDecryption.getEncryptPassword(password);
    	 
    	 UtiliserProfileModel utiModel = new UtiliserProfileModel();
    	 utiModel.setPassword(encryptedPassword);
    	 
    	 int uti_ind_id = utiservice.getindividualIndId(uniqueVariable);
    	 utiModel.setUti_ind_id(uti_ind_id);
    	 
    	 String passwordStatus = utiservice.getChangeUtiPassword(utiModel);
    	 
    	 // Tracking Activity Details
	        
    	 UtilizerActivityModel actModel = new UtilizerActivityModel();
		 actModel.setUti_ind_id(uti_ind_id);
		 actModel.setActivity_type("Password Change");
		 actModel.setActivity_details("You have changed your password on "+new Date());
		 actModel.setActivity_date(new Date());
		
		 utiservice.saveActivityDetails(actModel);

         // End Of Tracking Activity Details
								
	      String status = utiservice.getDeleteUtiResetPaswordInfo(uniqueVariable);
    	 
	      
    	 return status;
     }
	 
	@RequestMapping(value = "/utimyProfile", method = RequestMethod.GET)
	public ModelAndView getViewUtiliserProfile(HttpServletRequest request,HttpSession session)
	{
		    Map<String, Object> model = new HashMap<String, Object>();
		    
		    String kycId = (String)session.getAttribute("KycId");
			int uti_Indid = (int) session.getAttribute("uti_Ind_id");
			
			model.put("viewBasicProfile", PrepareBasicDetails(utiservice.getUtiBasicDetails(kycId)));			
			
			model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
			
			//model.put("utilizerAboutUsDate",prepareAboutUsBean(utiservice.getUtilizerDetail(uti_Indid)));
			
			 
			String uti_kycid = (String)session.getAttribute("KycId");
			int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
			int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
			
			request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
			request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
			
			
			return new ModelAndView("uti_profile",model);						
	}
	
	
	private UtiliserProfileBean prepareProfilePicBean(UtiliserProfileModel picModel)
	{
		UtiliserProfileBean picBean = new UtiliserProfileBean();
		
		picBean.setUti_ind_id(picModel.getUti_ind_id());
		picBean.setUti_profile_pic(picModel.getUti_profile_pic());
		picBean.setUti_cr_date(picModel.getUti_cr_date());
		picBean.setUti_first_name(picModel.getUti_first_name());
		picBean.setUti_office_Address(picModel.getUti_office_Address());
		picBean.setUti_head_office_Address(picModel.getUti_head_office_Address());
		picBean.setUti_dom_hierarchy(picModel.getUti_dom_hierarchy());
		
		return picBean;
	}
	
	private UtiliserDetailsBean prepareAboutUsBean(UtiliserDetailsModel detailModel)
	{
		UtiliserDetailsBean detailsBean = new UtiliserDetailsBean();
		
		detailsBean.setCr_date(detailModel.getCr_date());
		detailsBean.setAbout_utiliser(detailModel.getAbout_utiliser());
		detailsBean.setAbout_company(detailModel.getAbout_company());
		
		return detailsBean;
	}
	
	@RequestMapping(value="/saveutiAboutUs", method = RequestMethod.POST)
    public @ResponseBody String saveAboutUs(HttpSession session,HttpServletRequest request)
    {   
   	     String utiprofile_data = "";
   	   	     
   	     int uti_Indid = (int) session.getAttribute("uti_Ind_id"); 		
  		 String data = request.getParameter("aboutUs");
  		 		 
  		 utiprofile_data = utiservice.getUtiAboutUsInfo(uti_Indid);	
  		 
  		 UtiliserDetailsModel utiDetailsModel = new UtiliserDetailsModel();
  		  		
    	 if(utiprofile_data.equals("No Data"))
    	 {
    		 utiDetailsModel.setUti_ind_id(uti_Indid);
    		 utiDetailsModel.setAbout_utiliser(data);
    		 utiDetailsModel.setCr_date(new Date());   		 
    		 if("success".equals(utiservice.updateProfDetails(utiDetailsModel)))
    		 {    			 
    			 UtilizerActivityModel actModel = new UtilizerActivityModel();
    			 actModel.setUti_ind_id(uti_Indid);
    			 actModel.setActivity_type("About Us change");
    			 actModel.setActivity_details("About us had updated on "+new Date());
    			 actModel.setActivity_date(new Date());
    			
    			 utiservice.saveActivityDetails(actModel);
    			 
    			 return utiservice.getUtiAboutUsInfo(uti_Indid);	
    		 }
    		 else
    		 {
    			 return "Not Saved";
    		 }
    	 }
    	 else
    	 {
    		 utiDetailsModel.setDetails_id(utiservice.getUtiDetailsId(uti_Indid));
    		 utiDetailsModel.setUti_ind_id(uti_Indid);
    		 utiDetailsModel.setAbout_utiliser(data);
    		 utiDetailsModel.setCr_date(new Date());   		 
    		 if("success".equals(utiservice.updateProfDetails(utiDetailsModel)))
    		 {
    			 UtilizerActivityModel actModel = new UtilizerActivityModel();
    			 actModel.setUti_ind_id(uti_Indid);
    			 actModel.setActivity_type("About Us Change");
    			 actModel.setActivity_details("About us had updated on "+new Date());
    			 actModel.setActivity_date(new Date());
    			 
    			 utiservice.saveActivityDetails(actModel);
    			 
    			 return utiservice.getUtiAboutUsInfo(uti_Indid);	
    		 }
    		 else
    		 {
    			 return "Not Saved";
    		 }
    	 }  	    
    }
    
	
    @RequestMapping(value="/getutiAboutUsInfo", method = RequestMethod.POST)
    public @ResponseBody String getAboutUsInfo(HttpSession session,HttpServletRequest request)
    {
    	int uti_Indid = (int) session.getAttribute("uti_Ind_id"); 		
        String utiAboutUs = utiservice.getUtiAboutUsInfo(uti_Indid);
                    			
   	    return utiAboutUs;
    }
	
		
	@RequestMapping(value = "/viewUtiProfile",method = RequestMethod.POST)
	public ModelAndView getviewUtiProfile(HttpServletRequest request,HttpSession session)
	{
		String utikycid= (String)session.getAttribute("KycId");		
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("viewBasicProfile", PrepareBasicDetails(utiservice.getUtiBasicDetails(utikycid)));
		
		model.put("viewProfessional", PrepareProfessional(utiservice.getUtiProfDetails(utikycid)));
				
		return new ModelAndView("utiliser_view_profile",model);
	}
	
	
	@RequestMapping(value = "/editUtiProfile",method = RequestMethod.POST)
	public ModelAndView geteditUtiProfile(HttpServletRequest request,HttpSession session)
	{
		String utikycid= (String)session.getAttribute("KycId");		
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("editBasicProfile", PrepareBasicDetails(utiservice.getUtiBasicDetails(utikycid)));
		
		model.put("editProfessional", PrepareProfessional(utiservice.getUtiProfDetails(utikycid)));
		
		return new ModelAndView("utiliser_edit_profile",model);
	}
	
	
	
	 private List<UtiliserProfileBean> PrepareBasicDetails(List<UtiliserProfileModel> utidetails) {
     	
        	
     	List<UtiliserProfileBean> bean = null;
     	
		if (utidetails != null && !utidetails.isEmpty()) {
			bean = new ArrayList<UtiliserProfileBean>();
			UtiliserProfileBean beans = null;
        
			for (UtiliserProfileModel utidetail : utidetails) {
             beans = new UtiliserProfileBean();
			 	
             beans.setKyc_uti_id(utidetail.getKyc_uti_id());
             beans.setUti_first_name(utidetail.getUti_first_name());
             beans.setUti_middle_name(utidetail.getUti_middle_name());
             beans.setUti_last_name(utidetail.getUti_last_name());
             beans.setUti_email_id(utidetail.getUti_email_id());
             beans.setUti_office_emailId(utidetail.getUti_office_emailId());
             
             beans.setUti_office_first_no(utidetail.getUti_office_first_no());
             beans.setUti_office_second_no(utidetail.getUti_office_second_no());
             
             beans.setUti_cr_date(utidetail.getUti_cr_date());
             beans.setUti_office_Address(utidetail.getUti_office_Address());
             beans.setUti_profile_pic(utidetail.getUti_profile_pic());            
             beans.setUti_dom_hierarchy(utidetail.getUti_dom_hierarchy());
             beans.setUti_head_office_Address(utidetail.getUti_head_office_Address());
             
				bean.add(beans);
			}
		}
 		return bean;
 	} 
	
	
	 private List<UtiliserDetailsBean> PrepareProfessional(List<UtiliserDetailsModel> utidetails) {
	     	
     	
	     	List<UtiliserDetailsBean> bean = null;
	     	
			if (utidetails != null && !utidetails.isEmpty()) {
				bean = new ArrayList<UtiliserDetailsBean>();
				UtiliserDetailsBean beans = null;
	        
				for (UtiliserDetailsModel utidetail : utidetails) {
	             beans = new UtiliserDetailsBean();
				 	
	            
	             beans.setAbout_utiliser(utidetail.getAbout_utiliser());
	             beans.setAbout_company(utidetail.getAbout_company());
				
				 bean.add(beans);
				}
			}
	 		return bean;
	 	} 
	 
	 
	 @RequestMapping(value = "/utiNotification",method = RequestMethod.POST)
		public ModelAndView getNotification(HttpServletRequest request,HttpSession session)
		{
			String uti_kycid = utiid;
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("UtiNotification", PrepareNotificationDetails(utiservice.getUtiNotifications(uti_kycid)));			
			return new ModelAndView("uti_notification",model);
		}
	 
	   private List<UtiliserUpdateBean> PrepareNotificationDetails(List<UtiliserUpdateModel> utidetails) {
	     	
	     	
	     	List<UtiliserUpdateBean> bean = null;
	     	
			if (utidetails != null && !utidetails.isEmpty()) {
				bean = new ArrayList<UtiliserUpdateBean>();
				UtiliserUpdateBean beans = null;
	        
				for (UtiliserUpdateModel utidetail : utidetails) {
	             beans = new UtiliserUpdateBean();
				 	
	             beans.setThings_updated(utidetail.getThings_updated());
	             beans.setType_of_alert(utidetail.getType_of_alert());
	             beans.setUti_update_datetime(utidetail.getUti_update_datetime());
	             beans.setUti_update_details(utidetail.getUti_update_details());
				 
				 bean.add(beans);
				}
			}
	 		return bean;
	 	} 
	 
	@RequestMapping(value = "/awaitedDocs",method = RequestMethod.POST)
	public ModelAndView getAwaitedDocs(HttpServletRequest request,HttpSession session)
	{
		String uti_kycid = (String)session.getAttribute("KycId");
		
		List<AccessDetailsModel> awaitedData = utiservice.findAwaitedDocDetailsForUtilizer(uti_kycid);
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("awaitedData", awaitedData);
		
		
		int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
		int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
		
		request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
		request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");
		
		return new ModelAndView("utilizer_awaited_documents",model);
	}
	
	
	@RequestMapping(value ="/reciveddocuments",method = RequestMethod.GET)
	public ModelAndView reciveddocuments(HttpServletRequest request,HttpSession session)
	{
		session = request.getSession(false);
		
		if (session.getAttribute("uti_Ind_id") == null)
		{						 
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("uti_Ind_id");
			 //session.invalidate();
			 
			 return new ModelAndView("login_utiliser");
		}		
		else
		{
			String uti_kycid = (String)session.getAttribute("KycId");
			List<AccessDetailsModel> viewedData = utiservice.viewedDocDtailsForUtilizer(uti_kycid);
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("viewedData", viewedData);
			
			int uti_Indid = (int)session.getAttribute("uti_Ind_id");			
			model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
						
			int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
			int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
			
			request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
			request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");
			
		    return new ModelAndView("utilizer_recived_documents",model);
		}
	}
	
	
	@RequestMapping(value ="/searchUtilizerAccessDetails",method = RequestMethod.POST)
	public ModelAndView searchUtilizerAccessDetails(HttpServletRequest request,HttpSession session)
	{		
		String searchedIndex = request.getParameter("searchedData");
		String uti_kycid = (String)session.getAttribute("KycId");		
		
		//List<AccessDetailsModel> viewedData = utiservice.viewedDocDtailsForUtilizer(uti_kycid);
		
		List<AccessDetailsModel> viewedData = utiservice.viewedSearchedDocDetailsForUtilizer(uti_kycid,searchedIndex);
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("searchedviewedData", viewedData);
		
	    return new ModelAndView("searchUtilizerRecivedDocuments",model);		
	}
	
	@RequestMapping(value="/viewedDocs",method = RequestMethod.POST)
	public @ResponseBody
	String getViewedDocs(HttpServletRequest request,HttpSession session)
	{		
		String uti_kycid = (String)session.getAttribute("KycId");
		String lsdata = utiservice.findViewedDocDetails(uti_kycid);
				
		return lsdata.substring(1, lsdata.length()-1);
	}
	
	
	@RequestMapping(value="/acceptDocs" , method = RequestMethod.POST)
	public @ResponseBody String getAcceptDocs(HttpServletRequest request,HttpSession session) throws ParseException
	{		
		String giverKycId = request.getParameter("id");		
		Date date;
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = (Date)dateformat.parse(request.getParameter("date"));		
		String uti_status = utiservice.acceptDocs(giverKycId,date);		
		
		String uti_kycid = (String)session.getAttribute("KycId");
		 
		int indId = profileinfoService.findInd_id(giverKycId);
		List<RegHistoryModel> individualRegistrationDetails = profileinfoService.getIndividualDetailsviaIndId(indId);
		 
	    int uti_indid = utiservice.findUtiid(uti_kycid);
        UtilizerActivityModel actModel = new UtilizerActivityModel();
		actModel.setUti_ind_id(uti_indid);
		actModel.setActivity_type("activity Done in Document Section");
		actModel.setActivity_details("You have accepted "+individualRegistrationDetails.get(0).getFirst_name()+" "+individualRegistrationDetails.get(0).getMiddle_name()+" "+individualRegistrationDetails.get(0).getLast_name()+"'s documents on "+new Date());
		actModel.setActivity_date(new Date());
		 
		utiservice.saveActivityDetails(actModel);
		 
		return uti_status;		
	}
	
	//for further requirement
	
	String pass_kycid ="";
	String pass_date = "";
	

	@RequestMapping(value="/viewDetailsDocs" , method = RequestMethod.POST)
	public ModelAndView getAccesDetails(HttpServletRequest request,HttpSession session) throws ParseException
	{
		String kycid = request.getParameter("kycid");
		String dat1 = request.getParameter("date");		
		
		pass_kycid = kycid;
		pass_date =  dat1;
		
		session.setAttribute("pass_kycid", kycid);
		session.setAttribute("pass_date", dat1);
	
		String doc_pattern = utiservice.findAccessPattern(kycid.replaceAll(" ", ""),dat1);
			
		String profileAccessDetails = utiservice.getprofileAccessDetails(kycid.replaceAll(" ", ""),dat1);
		int indId = profileinfoService.findInd_id(kycid);
		 		 
		String profilePhoto = accservice.getProfilePic(indId);
		String year = profileinfoService.getYearFormIndId(Integer.toString(indId));
		 
		request.setAttribute("indIndId",indId);		
		session.setAttribute("individualIndId",indId);
		
		request.setAttribute("year",year);
		request.setAttribute("profilePhoto",profilePhoto);
		 
		if(!profileAccessDetails.equals("No Data"))
		{			
			 request.setAttribute("ProfileAccessData", profileAccessDetails);
			
			 String uti_kycid = (String)session.getAttribute("KycId");
			 			 			 
			 List<RegHistoryModel> individualRegistrationDetails = profileinfoService.getIndividualDetailsviaIndId(indId);
			 
		     int uti_indid = utiservice.findUtiid(uti_kycid);
	         UtilizerActivityModel actModel = new UtilizerActivityModel();
			 actModel.setUti_ind_id(uti_indid);			
			 actModel.setActivity_type("Viewing Document");
			 actModel.setActivity_details("You have viewed "+individualRegistrationDetails.get(0).getFirst_name()+" "+individualRegistrationDetails.get(0).getMiddle_name()+" "+individualRegistrationDetails.get(0).getLast_name()+" documents on "+new Date());
			 actModel.setActivity_date(new Date());
			         
			 utiservice.saveActivityDetails(actModel);
		}
		else
		{
			request.setAttribute("ProfileAccessData", "No Data");
		}
		
		request.setAttribute("accessPattern", doc_pattern);
		request.setAttribute("kycid", kycid);
				
		return new ModelAndView("viewedDocuments");		
	}
	
	
	@RequestMapping(value="/viewDocforUti" , method = RequestMethod.POST)
	public ModelAndView getviewDocforUti(HttpServletRequest request) 
	{
		String docname = request.getParameter("docname");
		String kycid = request.getParameter("kycid");
				
		String year = utiservice.findYear(kycid.replaceAll(" ", ""));
		int ind_id = utiservice.findid(kycid.replaceAll(" ", ""));
		String dir = utiservice.findDirName(docname);
		
		if(dir.equals("Employment_DOC"))
		{
			dir = "Employement_DOC";
		}		
		else if(dir.equals("ACADEMIC_DOC"))
		{
			dir = "ACADEMIC_DOC";
		}		
		else if(dir.equals("Financial_DOC"))
		{
			dir = "Financial_DOC";
		}		
		else
		{
			dir = "KYC_DOCUMENT";
		}
		
		request.setAttribute("year", year);
		request.setAttribute("ind_id",ind_id);
		request.setAttribute("dir", dir);
		request.setAttribute("docname", docname);
		
		request.setAttribute("documentComponentValue", utiservice.getDocumentComponentValue(docname,ind_id));
		
		String indvidualId = ind_id+"";
		
		//FileEncryptionDecryptionUtil.getFileDecryption(Ind_id, year, request);
		
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(year,indvidualId,dir,docname,request);
				
		// End Of Code File Decrypt
										
		return new ModelAndView("viewUtiliserDocs");
	}
	
	
	@RequestMapping(value="/downloadDocforUti" , method = RequestMethod.GET)
	public ModelAndView getdownloadDocforUti(HttpServletRequest request) 
	{
		String docname = request.getParameter("docname");
		String kycid = request.getParameter("kycid");
				
		String year = utiservice.findYear(kycid.replaceAll(" ", ""));
		int ind_id = utiservice.findid(kycid.replaceAll(" ", ""));
		String dir = utiservice.findDirName(docname);
		
		if(dir.equals("Employment_DOC"))
		{
			dir = "Employement_DOC";
		}		
		else if(dir.equals("ACADEMIC_DOC"))
		{
			dir = "ACADEMIC_DOC";
		}		
		else if(dir.equals("Financial_DOC"))
		{
			dir = "Financial_DOC";
		}		
		else
		{
			dir = "KYC_DOCUMENT";
		}
		
		request.setAttribute("year", year);
		request.setAttribute("ind_id",ind_id);
		request.setAttribute("dir", dir);
		request.setAttribute("docname", docname);
		
		String individualId = ind_id+"";
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(year,individualId,dir,docname,request);
				
		return new ModelAndView("downloadUtiliserDocs");
	}
	
	
	
	

	@RequestMapping(value="/viewDocFromUtilizer" , method = RequestMethod.POST)
	public ModelAndView getviewDocFromUtilizer(HttpServletRequest request,HttpSession session) 
	{
		String docname = request.getParameter("fileName");
		int ind_id = Integer.parseInt(request.getParameter("individualId"));
				
		String individualId = ind_id+"";
		int year = docservice.findYear(individualId);		
		String dir = utiservice.findDirName(docname);
		
		
		if(dir.equals("Employment_DOC"))
		{
			dir = "Employement_DOC";
		}		
		else if(dir.equals("ACADEMIC_DOC"))
		{
			dir = "ACADEMIC_DOC";
		}		
		else if(dir.equals("Financial_DOC"))
		{
			dir = "Financial_DOC";
		}		
		else
		{
			dir = "KYC_DOCUMENT";
		}
		
		request.setAttribute("year", year+"");
		request.setAttribute("ind_id",ind_id+"");
		request.setAttribute("dir", dir);
		request.setAttribute("docname", docname);
		
		
		request.setAttribute("documentComponentValue", utiservice.getDocumentComponentValue(docname,ind_id));
		
		// File Decrypt
		
		String crYear = year+"";
		
		
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(crYear,individualId,dir,docname,request);
		
		// End Of Code File Decrypt
						
				
		return new ModelAndView("viewUtiliserDocs");
	}
	
	
	
	@RequestMapping(value="/downloadDocfromUtilizer" , method = RequestMethod.GET)
	public ModelAndView getdownloadDocfromUtilizer(HttpServletRequest request,HttpSession session) 
	{
		String docname = request.getParameter("fileName");
		int ind_id = Integer.parseInt(request.getParameter("extp"));
		
		String individualId = ind_id+"";
		int year = docservice.findYear(individualId);
		String dir = utiservice.findDirName(docname);
		
		if(dir.equals("Employment_DOC"))
		{
			dir = "Employement_DOC";
		}		
		else if(dir.equals("ACADEMIC_DOC"))
		{
			dir = "ACADEMIC_DOC";
		}		
		else if(dir.equals("Financial_DOC"))
		{
			dir = "Financial_DOC";
		}		
		else
		{
			dir = "KYC_DOCUMENT";
		}
		
		request.setAttribute("year", year+"");
		request.setAttribute("ind_id",ind_id+"");
		request.setAttribute("dir", dir);
		request.setAttribute("docname", docname);
		
		String crYear = year+"";
				
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(crYear,individualId,dir,docname,request);
				
		return new ModelAndView("downloadUtiliserDocs");
	}
	
	
	 private List<DocumentDetailsBean> prepareDocumentDetailBean(List<DocumentDetailModel> docdetails) {
     	        	
     	List<DocumentDetailsBean> bean = null;
     	
		if (docdetails != null && !docdetails.isEmpty()) {
			
			bean = new ArrayList<DocumentDetailsBean>();
			DocumentDetailsBean beans = null;

			for (DocumentDetailModel docdetail : docdetails) {
             beans = new DocumentDetailsBean();
             
				beans.setDoc_id(docdetail.getDoc_id());
				beans.setDoc_name(docdetail.getDoc_name());
				beans.setCr_date(docdetail.getCr_date());
				beans.setDes(docdetail.getDes());
				beans.setDoc_des(docdetail.getDoc_des());
				beans.setDoc_type(docdetail.getDoc_type());
				bean.add(beans);
			}
		}
 		return bean;
 	} 
	
	 
	    @RequestMapping(value="/utilizerSettings" , method = RequestMethod.GET)
	    public ModelAndView utilizerSettings(HttpServletRequest request,HttpSession session)
	    {
	    	session = request.getSession(false);
			
			if (session.getAttribute("uti_Ind_id") == null)
			{						 
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("uti_Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("login_utiliser");
			}
			else if(session==null)
			{
				
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("uti_Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("login_utiliser");
			}
			else
			{
				Map<String, Object> model = new HashMap<String, Object>();
				int uti_Indid = (int) session.getAttribute("uti_Ind_id");			
				model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
				
				String uti_kycid = (String)session.getAttribute("KycId");
				int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
				int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
				
				request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
				request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
				
				
	            return new ModelAndView("utilizer_settings",model);
			}
	    }
	 
	    @RequestMapping(value="/utiSetting" , method = RequestMethod.POST)
	    public ModelAndView getChangeUtiSetting(HttpServletRequest request)
	    {
	        return new ModelAndView("uti_setting");
	    }
	    
	    	
	    
	    @RequestMapping(value="/changeUtiPassword" , method = RequestMethod.POST)
	    public @ResponseBody String getChangePassword(HttpServletRequest request,HttpSession session)
	    {	    	
	        String OldPassword = request.getParameter("OldPassword");
	        String Newpass = request.getParameter("newpassword");
	        String conpass = request.getParameter("conpassword");
	        String kycid = (String)session.getAttribute("KycId");
	   
	        String existPassword = utiservice.checkUtiPassword(OldPassword,Newpass,kycid);
	        
	        
           if (PasswordEncryptionDecryption.getValidatePassword(OldPassword, existPassword).equals("True")) {
	    		
	    		String chnagePassword = PasswordEncryptionDecryption.getEncryptPassword(Newpass);
	    		
	    		if("Password Changed".equals(utiservice.changePassword(OldPassword,chnagePassword,kycid)))
		        {	        
			         String uti_kycid = (String)session.getAttribute("KycId");
				     int uti_indid = utiservice.findUtiid(uti_kycid);
			         UtilizerActivityModel actModel = new UtilizerActivityModel();
					 actModel.setUti_ind_id(uti_indid);
					 actModel.setActivity_type("Password Change");
					 actModel.setActivity_details("You have changed your password on "+new Date());
					 actModel.setActivity_date(new Date());
					         
					 utiservice.saveActivityDetails(actModel);
					 	            
			         return "Your Password Successfully Changed";		            
		        } 
		        else
		        {
		        	 return "Some Problem arise Try Again";
		        }
	    	}	    	
	    	else
	    	{			
	    		return "NotMatched";
	    	}	     
	    }
	    
	    
	    @RequestMapping(value="/UtiHome" , method = RequestMethod.GET)
	    public ModelAndView getUtiHome(HttpServletRequest request, HttpSession session)
	    {
	        String kycid = kycId;
	        session.setAttribute("KycId", kycid);
	        Map<String,Object> model = new HashMap<String,Object>();
	        
            model.put("viewBasicProfile", PrepareBasicDetails(utiservice.getUtiBasicDetails(kycid)));
			
			model.put("viewProfessional", PrepareProfessional(utiservice.getUtiProfDetails(kycid)));
	        
	        return new ModelAndView("uti_profile", model);
	    }
	    
	    @RequestMapping(value="/doc_activity" , method = RequestMethod.POST)
	    public ModelAndView getIndDocActivity(HttpServletRequest request, HttpSession session)
	    {
	        String kycid = kycId;
	        Map<String,Object> model = new HashMap<String,Object>();
	        
	        model.put("utilizerActivity", prepareDocActivity(utiservice.getDocActivity(kycid)));
	        
	        return new ModelAndView("ind_Doc_Activity",model);
	        
	    }
	    
	    private List<DocumentsBean> prepareDocActivity(List<DocumentModel> docModels) {
	     	
        	
	     	List<DocumentsBean> bean = null;
	     	
			if (docModels != null && !docModels.isEmpty()) {
				bean = new ArrayList<DocumentsBean>();
				DocumentsBean beans = null;
	        
				
				for (int i=0;i<docModels.size();i++) {
				 				 	
	             beans = new DocumentsBean();				 		                                                
	             beans.setTypeofdoc(docModels.get(i).getTypeofdoc());
	             beans.setKycid(docModels.get(i).getKycid());
	             beans.setDate(docModels.get(i).getDate());
	             beans.setDocname(docModels.get(i).getDocname());
	           	             					
				 bean.add(beans);
				}
			}
	 		return bean;
	 	} 
	    
	    @RequestMapping(value ="/utisignOut", method = RequestMethod.GET)
		public ModelAndView getSignOutUtilizer(HttpServletRequest request,HttpSession session) {
			
           
	    	// Required file Config for entire Controller 
			
			 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
			 String kycMainDirectory = fileResource.getString("fileDirectory");					
			 File filePath = new File(kycMainDirectory);
			
		   // End of Required file Config for entire Controller 
				
			 
			if (session.getAttribute("uti_Ind_id") == null)
			{
				request.setAttribute("status","You have successfully loged out,Thank you for using the service !");				
				//request.getSession().invalidate();
				return new ModelAndView("login_utiliser");	
			}
			else
			{
				
				/* Decryption all document which is related to the utilizer */
				 
					String utiKycId = (String) session.getAttribute("KycId");
					int utiId = utiservice.getUtiIndId(utiKycId);		
					
					String getSenderKycId = utiservice.getListOfSenderKycId(utiKycId);
					
					if(getSenderKycId.length()>0)
					{
						String kycIdArray[] = getSenderKycId.split(",");
						for(int i=0;i<kycIdArray.length;i++)
						{
							String year = utiservice.findYear(kycIdArray[i]);
							int ind_id = utiservice.findid(kycIdArray[i]);
							String individualId = ind_id+"";

							File targetFile = new File((filePath.getPath()+"/"+year+"/"+individualId+"/Document"));						
							JIREncryptionUtils.encryptAllDocs(targetFile,"enc");								
						}
					}
					
					
					
				/* End of Decryption all document which is related to the utilizer */
				
					request.setAttribute("status","You have successfully loged out,Thank you for using the service !");				
					//request.getSession().invalidate();
					//session = request.getSession(false);
					session.removeAttribute("KycId");
					session.removeAttribute("uti_Ind_id");
					
				return new ModelAndView("login_utiliser");	
			}
	    			
		 }
	
	     @RequestMapping(value="/utilizerProfilePhoto",method=RequestMethod.POST)
	     public ModelAndView getProfilePictures(HttpServletRequest request,HttpSession session)
	     {	    	
	    	 int uti_ind_id = (int) session.getAttribute("uti_Ind_id"); 		  		 
	    	 String profilePhoto = utiservice.getProfilePhotoHistory(uti_ind_id);
	    	 String id = Integer.toString(uti_ind_id);
	    	
	    	 String historyDitails = id+"@@@"+profilePhoto;
	    	 request.setAttribute("profilePhotoHistory", historyDitails);
	    	   	 
	    	 return new ModelAndView("uti_profile_photo_history");    	   	 
	     }
	   
	     @RequestMapping(value="/updateProfilePhotoofUtilizer",method=RequestMethod.POST)
	     public @ResponseBody String setProfilePicture(HttpServletRequest request,HttpSession session)
	     {    	 
	    	 String picName = request.getParameter("pictureName");
	    	 int uti_ind_id = (int) session.getAttribute("uti_Ind_id"); 				 
	   		
	    	 String status = utiservice.updateProfilePic(picName,uti_ind_id);
	    	 if(status.equals("success"))
	    	 {
	    		
		         UtilizerActivityModel actModel = new UtilizerActivityModel();
				 actModel.setUti_ind_id(uti_ind_id);
				 actModel.setActivity_type("Profile Picture Change");
				 actModel.setActivity_details("You have changed your Profile picture on "+new Date());
				 actModel.setActivity_date(new Date());
				         
				 utiservice.saveActivityDetails(actModel);
	    	 }
	    	 
	    	 return status;
	     }
	     
	    
	    @RequestMapping(value ="/utiContactus", method = RequestMethod.GET)
		public ModelAndView contactusfromUtilizer(HttpServletRequest request,HttpSession session) {
			
            session = request.getSession(false);			
			if (session.getAttribute("uti_Ind_id") == null)
			{						 
				 request.setAttribute("status","Your session has expired please login to use the portal !");				
				 session.removeAttribute("uti_Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("login_utiliser");
			}		
			else
			{
				Map<String, Object> model = new HashMap<String, Object>();
				int uti_Indid = (int) session.getAttribute("uti_Ind_id");			
				model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
				
				
				String uti_kycid = (String)session.getAttribute("KycId");
				int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
				int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
				
				request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
				request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
				
				
			    return new ModelAndView("contactUsfromUtilizer",model);		
			}
		 }
	    
	    @SuppressWarnings("unused")
		@RequestMapping(value = "/utilizeralerts",method = RequestMethod.GET)
	    public ModelAndView utilizeralerts(HttpSession session,HttpServletRequest request)
	    {
           session = request.getSession(false);
			
			if (session.getAttribute("uti_Ind_id") == null)
			{						 
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("uti_Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("login_utiliser");
			}
			else if(session==null)
			{
				
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("uti_Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("login_utiliser");
			}
			else
			{
		    	String uti_kycid = (String)session.getAttribute("KycId");
		    	int uti_indid = utiservice.findUtiid(uti_kycid);
		    	
		    	Map<String,Object> model = new HashMap<String,Object>();
		    	
		    	model.put("utilizerActivity",utiservice.getUtilizerActivity(uti_indid));
		    	
		    	
				int uti_Indid = (int) session.getAttribute("uti_Ind_id");			
				model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
				
				
				int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
				int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
				
				request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
				request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");						
				
		    	return new ModelAndView("utilizer_alerts",model);
			}
	    }
	    
	    	    
	    @RequestMapping(value = "/utilzer_activity",method = RequestMethod.POST)
	    public ModelAndView getUtilizerActivityDetails(HttpSession session,HttpServletRequest request)
	    {
	    	String uti_kycid = (String)session.getAttribute("KycId");
	    	int uti_indid = utiservice.findUtiid(uti_kycid);
	    	
	    	Map<String,Object> model = new HashMap<String,Object>();
	    	
	    	model.put("utilizerActivity",utiservice.getUtilizerActivity(uti_indid));
	    	
	    	
	    	
			int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
			int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
			
			request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
			request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
			
			
	    	return new ModelAndView("utilizer_activity",model);
	    }
	    
	    
	    @RequestMapping(value = "/utilzer_notification",method = RequestMethod.POST)
	    public ModelAndView getUtilizerNotificationDetails(HttpSession session)
	    {
	    	String uti_kycid = (String)session.getAttribute("KycId");
	    	int uti_indid = utiservice.findUtiid(uti_kycid);
	    	Map<String, Object> model = new HashMap<String, Object>();
	    	
	    	model.put("ind2utiNotification",utiservice.getAccessNotificationDetails(uti_kycid));
	  	    	
			model.put("kycNotification",utiservice.getNotificationDetails(uti_indid));
	    	
	    	return new ModelAndView("utilizer_notification",model);
	    }
	    
	    
	    @RequestMapping(value = "/getUtiNotifyMessage", method = RequestMethod.POST)
		public @ResponseBody String getNotifyMessage(HttpServletRequest request) 
		{
			 int notifyId = Integer.parseInt(request.getParameter("notify_Id"));
			 String searchResult = utiservice.getMessagefromNotifyId(notifyId);
			 return searchResult;
		 }
		
	    
	    // Controller method for to access History Details of individual documents
	    
	    
	    
	    @SuppressWarnings("unchecked")
		@RequestMapping(value = "/kycHistorydetailsFromUtilizer", method = RequestMethod.POST)
	 	public ModelAndView GetKycViewMore(HttpServletRequest request,HttpSession session) {
	 		       	
		        
	    	    DocumentDetailModel docdetails = new DocumentDetailModel();	         	
	        	docdetails.setDoc_name((String)request.getParameter("id"));
	            docdetails.setDoc_id(Integer.parseInt((String)request.getParameter("idIndex")));
	        	
	    		Map<String, String> model = new HashMap<String, String>();
	    		
	    		model.put("viewdetails", docservice.getViewMore(docdetails));
	    		
	    	    request.setAttribute("docname", docservice.getDocName(docdetails));
	    	   
	    	    
	    	 String Indid = "" + (int) session.getAttribute("individualIndId");
	   		 
	   		 int year = docservice.findYear(Indid);
	   		 
	   		 request.setAttribute("year",year);
	   		 request.setAttribute("user",Indid);
	    	        
	 		return new ModelAndView("utilizerviewKycHistory",model);
	 	}
	    
	        
	    @RequestMapping(value ="/getDocDetailsHistoryRecord", method = RequestMethod.POST)
	    @ResponseBody public String getDocDetailsHistoryRecord(HttpServletRequest request,HttpSession session) {
		
	    	int doc_id = Integer.parseInt(request.getParameter("idIndex"));	     	
	    	Date generatedDate = docservice.getHistoryCreatedDatebyDocId(doc_id);
	    	
	    	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");	    	   	    	
	    	String reportDate = df.format(generatedDate);
	    	
	    	return reportDate;
	    }
	    
	    
	    @SuppressWarnings("unchecked")
		@RequestMapping(value = "/academicHistorydetailsFromUtilizer", method = RequestMethod.POST)
	 	public ModelAndView academicHistorydetailsFromUtilizer(HttpServletRequest request,HttpSession session) {
	 		
	    int doc_id = Integer.parseInt(request.getParameter("idIndex"));
     	
     	int getOwnedIndId = docservice.getIndIdviaDocId(doc_id);
     	int ownId = (Integer)session.getAttribute("tempIdasdgthfmUti");
     	     	
     		String docs_data = docservice.getAcademicDataById(doc_id);    		
     	    String docName = docservice.getAcademicDocNameById(doc_id);
     	     	     	    
     	    String Indid =  ""+ownId;	
     	    
    		int year = docservice.findYear(Indid);		 
    		
    		request.setAttribute("year",year);
     	    request.setAttribute("user", ownId);
     	    request.setAttribute("docsData",docs_data);
    	    request.setAttribute("docName", docName);
    	    
     		return new ModelAndView("utilizerviewAcademicHistory");   		
	 	}
	    
	        @SuppressWarnings("unchecked")
	  		@RequestMapping(value = "/employeeHistorydetailsFromUtilizer", method = RequestMethod.POST)
	  	 	public ModelAndView employeeHistorydetailsFromUtilizer(HttpServletRequest request,HttpSession session) {
	  	 		
	        	DocumentDetailModel docdetails = new DocumentDetailModel();
	         	
	         	docdetails.setDoc_name((String)request.getParameter("id"));
	         	
	            int doc_id = Integer.parseInt(request.getParameter("idIndex"));
	        	
	            int ownIndId = docservice.getIndIdviaDocId(doc_id);
	            int ownId = (Integer)session.getAttribute("tempIdasdgthfmUti");
	            	           
            	String employeeDetails = docservice.getEmpHistoryDetails(doc_id);            	
            	request.setAttribute("employeeDetails",employeeDetails);
             	
            	String Indid = "" +ownId;
    	   		 
    	   		int year = docservice.findYear(Indid);
    	   		 
    	   		request.setAttribute("year",year);
    	   		request.setAttribute("user",Indid); 
    	   		 
                return new ModelAndView("utilizerviewEmploymentHistory");	       		
	  	 	}
	    
	        @SuppressWarnings("unchecked")
	  		@RequestMapping(value = "/financialHistorydetailsFromUtilizer", method = RequestMethod.POST)
	  	 	public ModelAndView financialHistorydetailsFromUtilizer(HttpServletRequest request,HttpSession session) {
	  	 		
	        	
	        	String docName = request.getParameter("id");
	        	int i =(Integer)session.getAttribute("tempIdasdgthfmUti");
	        	String docsData = docservice.getFinancialDataviaDocName(i,docName);
	       	    
	        	request.setAttribute("docsData",docsData);
	        	request.setAttribute("indId",i);        	
	        	String financial= docservice.getFinancialComp();       	
	        	request.setAttribute("finHierarchy", financial); 
	        	
                String Indid = "" + i;
    	   		 
    	   		int year = docservice.findYear(Indid);
    	   		 
    	   		request.setAttribute("year",year);
    	   		request.setAttribute("user",Indid); 
    	   		 
	       		return new ModelAndView("utilizerviewFinancialHistory");
	  	 	}
	        
	        
	        @RequestMapping(value = "/viewDocumentsFromUtilizerInPopUp", method = RequestMethod.POST)
	    	public String viewDcuments(HttpServletRequest request, HttpSession session) {
	    		
	    		String docname = request.getParameter("docName");
	    		
	    		int ind_id = (Integer)session.getAttribute("individualIndId");
	    		
	    		int cr_year = accservice.findyear(docname,ind_id);
	    		
	            String docinfo = accservice.findDocView(docname);
	            
	            String docType = accservice.findDocType(docname,ind_id); 
	            
	            String str[] = docinfo.split(",");
	            String viewDocName = "";
	            
	            for(int i=0;i<str.length;i++)
	            {
	            	if(docname.equals(str[i]))
	            	{
	            		viewDocName = viewDocName+str[i];
	            	}
	            }
	           
	            String viewData =  ind_id+"/"+cr_year+"/"+viewDocName+"/"+docType;
	    		
	            request.setAttribute("viewData",viewData);
	            
	            return "view_applyCode_doc";
	    	}
	        
	        
	        @RequestMapping(value = "/downlaodDocsFromUtilizer", method = RequestMethod.GET)
	    	public ModelAndView DownloadCode(HttpServletRequest request,HttpSession session) {
	    		
	    		String docname = request.getParameter("fileName");
	    		
	    		int ind_id = (Integer)session.getAttribute("individualIndId");
	    		int year = accservice.findyear(docname, ind_id);
	    		String docType = accservice.findDocType(docname,ind_id); 
	    		
	    		request.setAttribute("docType", docType);
	    		request.setAttribute("docname", docname);
	    		request.setAttribute("user", ind_id);
	    		request.setAttribute("year", year);

	    		return new ModelAndView("downloadApplyedCode");
	    	}
	        
	        @RequestMapping(value="/viewUtilizrProfilePhoto",method=RequestMethod.POST)
	        public String previewProfilePhoto(HttpServletRequest request,HttpSession session)
	        {   
	       	 
	       	 String latestProfilePhoto = request.getParameter("fileName");
	       			 
	       	 request.setAttribute("latestProfilePhoto", latestProfilePhoto);
	       	
	       	 return "previewUtiProfilePhoto";
	        }
	        
	        
	        	        	        
	        /* Code of Utilizer view Individual documents */
	        
	        @SuppressWarnings({ "resource", "unused" })
			@RequestMapping(value = "/previewThumbNailDocsFromUtilizer", method = RequestMethod.GET)
	    	@ResponseBody public byte[] previewThumbNailDocs(HttpServletRequest request,HttpSession session) throws IOException {
	    		
	        	// Required file Config for entire Controller 
				
		       	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
		       	 String kycMainDirectory = fileResource.getString("fileDirectory");	
		       	 
		       	// End of Required file Config for entire Controller 
		       		
	       	    //int giverIndId = (int)session.getAttribute("tempIdasdgthfmUti");
		       	int giverIndId = Integer.parseInt(request.getParameter("extp"));
	       	    String giverUniqueId = giverIndId+"";
	       	    
	    		int year = docservice.findYear(giverUniqueId);
	    		
	    		ServletContext servletContext = request.getSession().getServletContext();
	    		
	    		File ourPath = new File(kycMainDirectory);
	    		
	    		String docCategory = request.getParameter("docCategory");
	    		
	    		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/");
	    		
	    		fPath = fPath.append(year + "/" + giverUniqueId);
	    		
	    		
	    		if(StringUtils.isNotBlank(docCategory))
	    		{
	    			if(docCategory.equals("KYC_DOCUMENT"))
	    			{				
	    				fPath = fPath.append("/ThumbNail_Documents/KYC_DOCUMENT/" + request.getParameter("fileName"));
	    			}
	    			else if(docCategory.equals("ACADEMIC_DOC"))
	    			{
	    				fPath = fPath.append("/ThumbNail_Documents/ACADEMIC_DOC/" + request.getParameter("fileName"));
	    			}
	    			else if(docCategory.equals("Financial_DOC"))
	    			{
	    				fPath = fPath.append("/ThumbNail_Documents/Financial_DOC/" + request.getParameter("fileName"));
	    			}
	    			else if(docCategory.equals("Employment_DOC"))
	    			{				
	    				fPath = fPath.append("/ThumbNail_Documents/Employement_DOC/" + request.getParameter("fileName"));
	    			}
	    			else
	    			{
	    				fPath = fPath.append("/ThumbNail_Documents/Profile_Photo/" + request.getParameter("fileName"));
	    			}
	    			
	    			

	    			/* Checks that files are exist or not  */
	    				File exeFile=new File(fPath.toString());
	    				if(!exeFile.exists())
	    				{
	    					fPath.delete(0, fPath.length());
	    					File targetFile = new File(request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY/image-not-found.png"));					
	    					fPath.append(targetFile.getPath());
	    				}	
	    			/* Checks that files are exist or not  */
	    			
	    		}
	    		
	    		
	    		InputStream in;
	    		try {
	    			
	    			if (AccessControl.isValidSession(request)) {
	    				
	    				FileInputStream docdir = new FileInputStream(fPath.toString());				
	    				if (docdir != null) 
	    				{										
	    					return IOUtils.toByteArray(docdir);					
	    				} 
	    				else 
	    				{						
	    					return null;
	    				}
	    			} 
	    			else {                                   
	    				
	    				in = servletContext.getResourceAsStream("/resources/images/Access_Denied.jpg");
	    				if (in != null) 
	    				{
	    					return IOUtils.toByteArray(in);
	    				} else
	    				{
	    					return null;
	    				}
	    			}
	    		} 
	    		catch (FileNotFoundException e) {
	    					
	    			return null; // todo: return safe photo instead
	    			
	    		} catch (IOException e) {
	    			System.out.println(e.getMessage());
	    			return null; // todo: return safe photo instead
	    		}

	    	}
	    	
	        
	        
	        
	        @RequestMapping(value="/historyDocforUti" , method = RequestMethod.POST)
			public ModelAndView gethistoryDocforUti(HttpServletRequest request,HttpSession session) 
			{
		    	String docname = "";	    	
		    	String kycid = "";

				docname = request.getParameter("docname");
				kycid = request.getParameter("kycid");
					
				session.setAttribute("Historydocname", docname);
				session.setAttribute("Historykycid", kycid);
				
				int ind_id = Integer.parseInt(request.getParameter("extp"));
				String doc_type = utiservice.findDirName(docname);
				
				if(doc_type.equals("Employment_DOC"))
				{
					//doc_type = "Employement_DOC";
					doc_type = "Employment_DOC";
				}		
				else if(doc_type.equals("ACADEMIC_DOC"))
				{
					doc_type = "ACADEMIC_DOC";
				}		
				else if(doc_type.equals("Financial_DOC"))
				{
					doc_type = "Financial_DOC";
				}		
				else
				{
					doc_type = "KYC_DOCUMENT";
				}
				
				Map<String, Object> model = new HashMap<String, Object>();
				
				if(doc_type.equals("Employement_DOC"))
				{
					
				}		
				else
				{
					String docHistory[] = docname.split("_");
					model.put("IndHistorydetails", prepareDocumentDetailBean(utiservice.getHistory(docHistory[0],ind_id)));					
				}
						
				request.setAttribute("kycid", (String)session.getAttribute("pass_kycid"));
				request.setAttribute("date", (String)session.getAttribute("pass_date"));
				
				session.setAttribute("urlValueToGoHistory", docname);
				
				return new ModelAndView("historyUtiliserDocs",model);	
			}
	        
	        
	        @RequestMapping(value = "/redirectviewKycDocumentsFrmUti", method = RequestMethod.GET)
	    	public String redirectviewKycDocumentsFrmUti(HttpServletRequest request,HttpSession session) 
	        {
	        	int accessGiverKcId = Integer.parseInt(request.getParameter("accgiver")); 
	        	
	        	session.setAttribute("tempIdasdgthfmUti", accessGiverKcId);
	        	
	        	return "redirect:/viewKycDocumentsFrmUti.html";
	        }
	        
	        
						        @RequestMapping(value = "/viewKycDocumentsFrmUti", method = RequestMethod.GET)
						    	public ModelAndView viewKycDocumentsFrmUti(HttpServletRequest request,HttpSession session) 
						        {	    			           	
								    
						        	/* These all are code for Shared Kyc Documents. */	
						        	
						        	session = request.getSession(false);
									
									if (session.getAttribute("uti_Ind_id") == null)
									{						 
										 request.setAttribute("status","Your session has expired please login to use the portal !");
										
										 session.removeAttribute("uti_Ind_id");
										 //session.invalidate();
										 
										 return new ModelAndView("login_utiliser");
									}				
									else
									{  	
								    			DocumentDetailModel docdetails = new DocumentDetailModel();
								    			docdetails.setKyc_ind_id((int)session.getAttribute("tempIdasdgthfmUti"));
								    			docdetails.setDoc_type("kyc");
								    			
								    			String indKycId = profileinfoService.getkycid((int)session.getAttribute("tempIdasdgthfmUti"));
								    			String uti_kycid = (String)session.getAttribute("KycId");
												int uti_Indid = (int) session.getAttribute("uti_Ind_id");	
												
												
												String accordname=docservice.getstatus(docdetails);		    									
												String accessPattern = utiservice.getAccessPatternViaKycIds(uti_kycid,indKycId,accordname);	    			
								    			accordname = accessPattern;	    			
								    			
								    			Map<String, Object> model = new HashMap<String, Object>();
										    	 				
								    			
								    			model.put("accordNames", accordname);			
								    			session.setAttribute("kyc_panel_status", accordname);	    				
								    			docdetails.setDoc_des(accordname);	    				
								    	 		String kycdatas = docservice.getKYCdataAsUtilizerDetails(docdetails);	    				
								    		
								    	 		
								    	 	// Set all documents to download in a zip 
								    	 		
								    	 		String allDocNames = "";
								    	 		
								    	 		String kycDocArray[] = kycdatas.split("/");
								    	 		for(int k=0;k<kycDocArray.length;k++)
								    	 		{												    	 			
								    	 			String eachPattrn = kycDocArray[k].substring(kycDocArray[k].lastIndexOf("$")+1, kycDocArray[k].length());									    	 			
								    	 			String eachPattrnArray[] = eachPattrn.split(",");
								    	 			
								    	 			if(eachPattrnArray[1].equals("Not Uploaded"))
								    	 			{
								    	 				allDocNames += eachPattrnArray[0] + ",";
								    	 			}
								    	 			else
								    	 			{
								    	 				allDocNames += eachPattrnArray[0] + "," +eachPattrnArray[1] + ",";
								    	 			}						    	 		
								    	 		}
								    	 		
								    	 		if(allDocNames.endsWith(","))
								    	 		{
								    	 			allDocNames = allDocNames.substring(0, allDocNames.length()-1);
								    	 		}
								    	 		
								    	 		
								    	 		 session.setAttribute("senderKycId", indKycId);
								    	 		 session.setAttribute("allDocumentNames", allDocNames);
								    	 		
								    	 	 // End of Set all documents to download in a zip 
								    	 		
								    	 		model.put("kycdatas", kycdatas);			    					    				
								    			String Indid = (int)session.getAttribute("tempIdasdgthfmUti")+"";	    				 
								    			int year = docservice.findYear(Indid);	    				 
								    		    
								    			request.setAttribute("year",year);
								    			request.setAttribute("user",Indid);
								    					    				
											   //Code for reselect Category
											   
								    			
								    			
											    String component = docservice.getKYCComp();	    					
												request.setAttribute("comp", component);	    					
												int indId = Integer.parseInt(Indid);
												String old_selComp = docservice.getOldComp(indId);	    					
												request.setAttribute("OldComp",old_selComp);	    					    					
												int ind_id = (int)session.getAttribute("tempIdasdgthfmUti");					    									
												String pannelWithId = docservice.getPannelWithId(ind_id,old_selComp);
												request.setAttribute("pannelWithId",pannelWithId);						
								    		
												
												
										 /* End Of These all are code for Shared Kyc Documents. */	
											
										
									
										/* In this page left side division content */		
												
											
											   	String kycId = profileinfoService.getkycid(ind_id);
											   	
											   	String created_year = signupService.findyear(kycId);	
											   	
												session.setAttribute("created_year", created_year);
												
												String visibility = signupService.getVisibility(ind_id);
												session.setAttribute("visibility", visibility);
												
												request.setAttribute("searched_Indid", ind_id);
												
												IndSignupBean signupBean = new IndSignupBean();
												signupBean.setKycid(kycId);
												
												BasicDetail BasicDetail=new BasicDetail();
												BasicDetail.setInd_kyc_id(Integer.toString(ind_id));
													    	
												model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(ind_id)));
												
												RegHistoryModel regModel = signupService.getRegistrationDetails(ind_id);
												
												model.put("regname", prepareBean(signupService.getname(ind_id)));
												
												request.setAttribute("utiFirstName", signupService.getUtilizerName(uti_Indid));
																		 
												if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(Integer.toString(ind_id)))) 
												{			
											     	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(Integer.toString(ind_id)));		
												    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
												}
															
												if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
												{
													int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
													model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));									
										    	}
											
												String ind_kyc_id = kycId;
												IndSignupModel individual = new IndSignupModel();
												
												individual.setKycid(ind_kyc_id);
												String i=Integer.toString(ind_id);
												
												Matrial matrial=new Matrial();
												matrial.setInd_kyc_id(i);
												
												BasicDetail.setInd_kyc_id(i);
											
												if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
												{			
													  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
													  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
												}
												
												if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
												{
													int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
													model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
											    }
														
												if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
												{	
													String picName = profileinfoService.getLatestPicName(ind_id);
													session.setAttribute("profilePicName", picName);
													int pic_id = profileinfoService.getPrifilePicId(picName);				
													model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
												}
													
												
												String searchedIndId = profileinfoService.getSearchedAboutUsInfo(ind_id);
												request.setAttribute("searchedIndId", searchedIndId);
												
												String indRegVisibility = profileinfoService.getRegVisisbility(ind_id);
												String indBasicVisibility = profileinfoService.getBasicVisisbility(ind_id);
												String indFamilyVisibility = profileinfoService.getFamilyVisisbility(ind_id);
												String indSocialVisibility = profileinfoService.getSocialVisisbility(ind_id);
												
												request.setAttribute("regVisibility", indRegVisibility);
												request.setAttribute("basicVisibility", indBasicVisibility);
												request.setAttribute("familyVisibility", indFamilyVisibility);
												request.setAttribute("socialVisibility", indSocialVisibility);
												
												String individualKycId = profileinfoService.getkycid(ind_id);
												String utilizerKycId = (String)session.getAttribute("KycId");
												
												String accessStatus = profileinfoService.getUtilizerAccessForIndividual(individualKycId,utilizerKycId);
												String profileAccessStatus = "";
												String otherAccess = "";
												if(accessStatus.length()>1)
												{
													if(accessStatus.startsWith("PROFILE"))
													{
														String profileAccessArray[] = accessStatus.split("/");
														String profileAccessDetails[] = profileAccessArray[0].split(":");
														profileAccessStatus += profileAccessDetails[1];
														
														
													}			
												}
												
												request.setAttribute("profileAccessDetails", profileAccessStatus);
												request.setAttribute("otherAccess", accessStatus);
												
												
												
									   
										/*  End of left side page division content  */	
												
												
												int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
												int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
												
												request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
												request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");			
												
												
						    		    return new ModelAndView("utilizerAccessKycDocs",model);	
									}
						    	}
						        
						        	    
						        
						     @RequestMapping(value = "/kychistoryfromUti", method = RequestMethod.POST)
						   	 public ModelAndView kychistoryfromUti(HttpServletRequest request,HttpSession session) {
						   			
						   			       								
						   						int id =  Integer.parseInt(request.getParameter("extp"));
						   						 
						   				    	DocumentDetailModel docdetails = new DocumentDetailModel();
						   				    	docdetails.setKyc_ind_id(id);
						   						
						   				    	String docName = request.getParameter("fileName");
						   				    	request.setAttribute("docName", docName);
						   				    	
						   						docdetails.setDoc_name((String)request.getParameter("fileName"));
						   						
						   						Map<String, Object> model = new HashMap<String, Object>();
						   						model.put("kyc_history", prepareDocumentDetailBeanForHistory(docservice.getHistory(docdetails,session)));
						   						
						   						int indId = id;				
						   						request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
						   				          
						   						session.setAttribute("tempIdasdgthfmUti", id);
						   						
						   						return new ModelAndView("utilizerviewKycHistory",model);
						   					
						   		}
						   	
	
	     //////////   End of Kyc Access Part from Utilizer
	     
						     @RequestMapping(value = "/redirectviewAcademicDocumentsFrmUti", method = RequestMethod.GET)
						    	public String redirectviewAcademicDocumentsFrmUti(HttpServletRequest request,HttpSession session) 
						        {
						    	    int accessGiverKcId = Integer.parseInt(request.getParameter("accgiver")); 
						        	
						        	session.setAttribute("tempIdasdgthfmUti", accessGiverKcId);
						        	return "redirect:/viewAcademicDocumentsFrmUti.html";
						        }
						        
						     
	     
						     
						        @RequestMapping(value = "/viewAcademicDocumentsFrmUti", method = RequestMethod.GET)
						    	public ModelAndView viewAcademicDocumentsFrmUti(HttpServletRequest request,HttpSession session) 
						        {	    			           	
								    		/* These all are code for Shared Kyc Documents. */	
						        	
						        	
						        	session = request.getSession(false);
									
									if (session.getAttribute("uti_Ind_id") == null)
									{						 
										 request.setAttribute("status","Your session has expired please login to use the portal !");
										
										 session.removeAttribute("uti_Ind_id");
										 //session.invalidate();
										 
										 return new ModelAndView("login_utiliser");
									}				
									else
									{  	
										
										session.setAttribute("historyBackUrl","redirectviewAcademicDocumentsFrmUti.html?accgiver="+session.getAttribute("tempIdasdgthfmUti"));  
										
								    			DocumentDetailModel docdetails = new DocumentDetailModel();
								    			docdetails.setKyc_ind_id((int)session.getAttribute("tempIdasdgthfmUti"));
								    			docdetails.setDoc_type("kyc");
								    			
								    			String indKycId = profileinfoService.getkycid((int)session.getAttribute("tempIdasdgthfmUti"));
								    			String uti_kycid = (String)session.getAttribute("KycId");
												int uti_Indid = (int) session.getAttribute("uti_Ind_id");	
												
												String accordname=docservice.getstatus(docdetails);											
												String getAcademicStatusAsPerPattern = utiservice.getAcademicStatusPattern(uti_kycid,indKycId,accordname);												
												String AcademicAccordian[] = getAcademicStatusAsPerPattern.split("ACADEMICSEPERATOR");
												
												String extraAddedDocuments = "";

								    			int ind_id = (int)session.getAttribute("tempIdasdgthfmUti");	
											
								    			String individualId = ind_id+"";
								    			
												for(int i=0;i<AcademicAccordian.length;i++)
												{
													if(AcademicAccordian[i].contains("_"))
													{
														extraAddedDocuments = AcademicAccordian[i];
													}
												}
												
												
												String documentNamesWithDetails = utiservice.getDocumentNamesWithDetails(individualId,AcademicAccordian[0],extraAddedDocuments);
												
												
												request.setAttribute("documentNamesWithDetails", documentNamesWithDetails);
												request.setAttribute("pannelStatus", AcademicAccordian[0]);
												request.setAttribute("extraAddedDocuments", extraAddedDocuments);
												
												Map<String, Object> model = new HashMap<String, Object>();
										    	
												
                                               // Set all documents to download in a zip 
								    	 		
								    	 		  String allDocNames = "";
								    	 		  session.setAttribute("senderKycId", indKycId);
								    	 		 
								    	 		  String docNameArray[] = documentNamesWithDetails.split("ENDSEP");
								    	 		  for(int dn=0;dn<docNameArray.length;dn++)
								    	 		  {
								    	 			  
								    	 			  if(docNameArray[dn].split("ACASEP").length==2)
								    	 			  {
								    	 				 allDocNames += docNameArray[dn].split("ACASEP")[1] + ",";
								    	 			  }
								    	 		  }
								    	 		  
								    	 		  if(extraAddedDocuments.length()>0)
								    	 		  {
								    	 			 String extraAddedDocArray[] = extraAddedDocuments.split(","); 
								    	 			 for(int ext=0;ext<extraAddedDocArray.length;ext++)
								    	 			 {
								    	 				 if(!allDocNames.contains(extraAddedDocArray[ext]))
								    	 				 {
								    	 					allDocNames += ","+extraAddedDocArray[ext];
								    	 				 }
								    	 			 }
								    	 			 
								    	 		  }
								    	 		  
								    	 		  if(extraAddedDocuments.endsWith(","))
								    	 		  {								    	 			  
								    	 			 allDocNames = allDocNames.substring(0, allDocNames.length()-1);
								    	 		  }
								    	 		 
								    	 		  session.setAttribute("senderKycId", indKycId);
								    	 		  session.setAttribute("allDocumentNames", allDocNames);
								    	 		
								    	 		  
								    	 	   // End of Set all documents to download in a zip 
																								
										 /* End Of These all are code for Shared Kyc Documents. */	
									
								    	 		  
								    	 		  
										/* In this page left side division content */	
								    	 		  
												
											   	String kycId = profileinfoService.getkycid(ind_id);
											   	
											   	String created_year = signupService.findyear(kycId);	
											   	
												session.setAttribute("created_year", created_year);
												
												String visibility = signupService.getVisibility(ind_id);
												session.setAttribute("visibility", visibility);
												
												request.setAttribute("searched_Indid", ind_id);
												
												IndSignupBean signupBean = new IndSignupBean();
												signupBean.setKycid(kycId);
												
												BasicDetail BasicDetail=new BasicDetail();
												BasicDetail.setInd_kyc_id(Integer.toString(ind_id));
													    	
												model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(ind_id)));
												
												RegHistoryModel regModel = signupService.getRegistrationDetails(ind_id);
												
												model.put("regname", prepareBean(signupService.getname(ind_id)));
												
												request.setAttribute("utiFirstName", signupService.getUtilizerName(uti_Indid));
																		 
												if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(Integer.toString(ind_id)))) 
												{			
											     	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(Integer.toString(ind_id)));		
												    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
												}
															
												if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
												{
													int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
													model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));									
										    	}
											
												String ind_kyc_id = kycId;
												IndSignupModel individual = new IndSignupModel();
												
												individual.setKycid(ind_kyc_id);
												String i=Integer.toString(ind_id);
												
												Matrial matrial=new Matrial();
												matrial.setInd_kyc_id(i);
												
												BasicDetail.setInd_kyc_id(i);
											
												if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
												{			
													  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
													  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
												}
												
												if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
												{
													int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
													model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
											    }
														
												if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
												{	
													String picName = profileinfoService.getLatestPicName(ind_id);
													session.setAttribute("profilePicName", picName);
													int pic_id = profileinfoService.getPrifilePicId(picName);				
													model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
												}
													
												
												String searchedIndId = profileinfoService.getSearchedAboutUsInfo(ind_id);
												request.setAttribute("searchedIndId", searchedIndId);
												
												String indRegVisibility = profileinfoService.getRegVisisbility(ind_id);
												String indBasicVisibility = profileinfoService.getBasicVisisbility(ind_id);
												String indFamilyVisibility = profileinfoService.getFamilyVisisbility(ind_id);
												String indSocialVisibility = profileinfoService.getSocialVisisbility(ind_id);
												
												request.setAttribute("regVisibility", indRegVisibility);
												request.setAttribute("basicVisibility", indBasicVisibility);
												request.setAttribute("familyVisibility", indFamilyVisibility);
												request.setAttribute("socialVisibility", indSocialVisibility);
												
												String individualKycId = profileinfoService.getkycid(ind_id);
												String utilizerKycId = (String)session.getAttribute("KycId");
												
												String accessStatus = profileinfoService.getUtilizerAccessForIndividual(individualKycId,utilizerKycId);
												String profileAccessStatus = "";
												String otherAccess = "";
												if(accessStatus.length()>1)
												{
													if(accessStatus.startsWith("PROFILE"))
													{
														String profileAccessArray[] = accessStatus.split("/");
														String profileAccessDetails[] = profileAccessArray[0].split(":");
														profileAccessStatus += profileAccessDetails[1];														
													}			
												}
												
												request.setAttribute("profileAccessDetails", profileAccessStatus);
												request.setAttribute("otherAccess", accessStatus);
											
									   
										/*  End of left side page division content  */	
												
												int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
												int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
												
												request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
												request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");			
												
												
						    		    return new ModelAndView("utilizerAccessAcademicDocs",model);	
									}
						    	}
						        
	     
						        @RequestMapping(value = "/redirectviewfindocumentsfrmUti", method = RequestMethod.GET)
						    	public String redirectviewfindocumentsfrmUti(HttpServletRequest request,HttpSession session) 
						        {
						        	int accessGiverKcId = Integer.parseInt(request.getParameter("accgiver")); 
						        	
						        	session.setAttribute("tempIdasdgthfmUti", accessGiverKcId);
						        	return "redirect:/viewfindocumentsfrmUti.html";
						        }
						        
						        @RequestMapping(value = "/viewfindocumentsfrmUti", method = RequestMethod.GET)
						    	public ModelAndView viewFinancialDocumentsFrmUti(HttpServletRequest request,HttpSession session) 
						        {	    			           	
								    		/* These all are code for Shared Financial Documents. */	
						        	
						        	
						        	session = request.getSession(false);
									
									if (session.getAttribute("uti_Ind_id") == null)
									{						 
										 request.setAttribute("status","Your session has expired please login to use the portal !");
										
										 session.removeAttribute("uti_Ind_id");
										 //session.invalidate();
										 
										 return new ModelAndView("login_utiliser");
									}				
									else
									{  	
										 session.setAttribute("historyBackUrl","redirectviewfindocumentsfrmUti.html?accgiver="+session.getAttribute("tempIdasdgthfmUti"));        
											
								    			DocumentDetailModel docdetails = new DocumentDetailModel();
								    			docdetails.setKyc_ind_id((int)session.getAttribute("tempIdasdgthfmUti"));
								    			docdetails.setDoc_type("FINANCIAL");
								    			
								    			String indKycId = profileinfoService.getkycid((int)session.getAttribute("tempIdasdgthfmUti"));
								    			String uti_kycid = (String)session.getAttribute("KycId");
												int uti_Indid = (int) session.getAttribute("uti_Ind_id");	
												
												int ind_id = (int)session.getAttribute("tempIdasdgthfmUti");	
												
												String accordname=docservice.getstatus(docdetails);;											
												
												String financialHierarchy = docservice.getFinancialTypeFromHierarchy();
												String financialHierarchyArray[] = financialHierarchy.split(",");
												
												String getFinancialStatusAsPerPattern = utiservice.getFinancialStatusPattern(uti_kycid,indKycId,accordname);												
												String financialAccordian[] = getFinancialStatusAsPerPattern.split("FINSEP");
																								
												String extraAddedDocuments = "";
												
												if(financialAccordian.length==2)
												{
													extraAddedDocuments += financialAccordian[1];
												}
												
												String finalAccordian = "";										
												for(int h=0;h<financialHierarchyArray.length;h++)
												{
													if(extraAddedDocuments.contains(financialHierarchyArray[h]))
													{
														if(!finalAccordian.contains(financialHierarchyArray[h]))
														{
															finalAccordian += financialHierarchyArray[h] + ",";
														}														
													}
												}
												
												if(finalAccordian.length()>0)
												{
													finalAccordian = finalAccordian.substring(0,finalAccordian.length()-1);
												}
												
												
								    			String financialDataWithDoc = utiservice.getFinancialDetailsBythePattern(ind_id,getFinancialStatusAsPerPattern);
											    
								    			
								    			request.setAttribute("financialAccordian", financialAccordian[0]+","+finalAccordian);
								    			request.setAttribute("financialDataWithDoc", financialDataWithDoc);
								    			request.setAttribute("extraAddedDocuments", extraAddedDocuments);
								    			
								    			request.setAttribute("typeOfFinancialFrmHierachy",financialHierarchy);
								    			
												Map<String, Object> model = new HashMap<String, Object>();
										    													
										// Set all documents to download in a zip 
								    	 		
												
								    	 		  String allDocNames = "";
								    	 		  session.setAttribute("senderKycId", indKycId);
								    	 		 
								    	 		  String docNameArray[] = financialDataWithDoc.split("ENDSEP");
								    	 		  
								    	 		  for(int dn=0;dn<docNameArray.length;dn++)
								    	 		  {		
								    	 			  if(!docNameArray[dn].equals(""))
								    	 			  {
								    	 				 String eachValue[] = docNameArray[dn].split("&&");
									    	 			 allDocNames += eachValue[4]+",";	
								    	 			  }								    	 			   							    	 			  
								    	 		  }
								    	 		  
								    	 		 
								    	 		  if(!extraAddedDocuments.equals("Not Added"))
								    	 		  {
								    	 			 allDocNames += ","+extraAddedDocuments;
								    	 		  }								    	 		 
								    	 		  
								    	 		  if(allDocNames.endsWith(","))
								    	 		  {
								    	 			 allDocNames = allDocNames.substring(0, allDocNames.length()-1);
								    	 		  }
								    	 		  
								    	 		  session.setAttribute("allDocumentNames", allDocNames);
								    	 		  
								    	 
								    	// End of Set all documents to download in a zip 
												
								    	 		  
								/* End Of These all are code for Shared Financial Documents. */	
											
										
									
										/* In this page left side division content */	
												
											   	String kycId = profileinfoService.getkycid(ind_id);
											   	
											   	String created_year = signupService.findyear(kycId);	
											   	
												session.setAttribute("created_year", created_year);
												
												String visibility = signupService.getVisibility(ind_id);
												session.setAttribute("visibility", visibility);
												
												request.setAttribute("searched_Indid", ind_id);
												
												IndSignupBean signupBean = new IndSignupBean();
												signupBean.setKycid(kycId);
												
												BasicDetail BasicDetail=new BasicDetail();
												BasicDetail.setInd_kyc_id(Integer.toString(ind_id));
													    	
												model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(ind_id)));
												
												RegHistoryModel regModel = signupService.getRegistrationDetails(ind_id);
												
												model.put("regname", prepareBean(signupService.getname(ind_id)));
												
												request.setAttribute("utiFirstName", signupService.getUtilizerName(uti_Indid));
																		 
												if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(Integer.toString(ind_id)))) 
												{			
											     	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(Integer.toString(ind_id)));		
												    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
												}
															
												if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
												{
													int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
													model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));									
										    	}
											
												String ind_kyc_id = kycId;
												IndSignupModel individual = new IndSignupModel();
												
												individual.setKycid(ind_kyc_id);
												String i=Integer.toString(ind_id);
												
												Matrial matrial=new Matrial();
												matrial.setInd_kyc_id(i);
												
												BasicDetail.setInd_kyc_id(i);
											
												if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
												{			
													  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
													  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
												}
												
												if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
												{
													int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
													model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
											    }
														
												if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
												{	
													String picName = profileinfoService.getLatestPicName(ind_id);
													session.setAttribute("profilePicName", picName);
													int pic_id = profileinfoService.getPrifilePicId(picName);				
													model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
												}
													
												
												String searchedIndId = profileinfoService.getSearchedAboutUsInfo(ind_id);
												request.setAttribute("searchedIndId", searchedIndId);
												
												String indRegVisibility = profileinfoService.getRegVisisbility(ind_id);
												String indBasicVisibility = profileinfoService.getBasicVisisbility(ind_id);
												String indFamilyVisibility = profileinfoService.getFamilyVisisbility(ind_id);
												String indSocialVisibility = profileinfoService.getSocialVisisbility(ind_id);
												
												request.setAttribute("regVisibility", indRegVisibility);
												request.setAttribute("basicVisibility", indBasicVisibility);
												request.setAttribute("familyVisibility", indFamilyVisibility);
												request.setAttribute("socialVisibility", indSocialVisibility);
												
												String individualKycId = profileinfoService.getkycid(ind_id);
												String utilizerKycId = (String)session.getAttribute("KycId");
												
												String accessStatus = profileinfoService.getUtilizerAccessForIndividual(individualKycId,utilizerKycId);
												String profileAccessStatus = "";
												String otherAccess = "";
												if(accessStatus.length()>1)
												{
													if(accessStatus.startsWith("PROFILE"))
													{
														String profileAccessArray[] = accessStatus.split("/");
														String profileAccessDetails[] = profileAccessArray[0].split(":");
														profileAccessStatus += profileAccessDetails[1];
														
														
													}			
												}
												
												request.setAttribute("profileAccessDetails", profileAccessStatus);
												request.setAttribute("otherAccess", accessStatus);
												
												
												
									   
										/*  End of left side page division content  */	
										
												
												int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
												int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
												
												request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
												request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");			
												
												
												
						    		    return new ModelAndView("utilizerAccessFinancialDocs",model);	
									}
						    	}
						        
						        
						        @RequestMapping(value = "/redirectviewEmpDocumentsFrmUti", method = RequestMethod.GET)
						    	public String redirectviewEmpDocumentsFrmUti(HttpServletRequest request,HttpSession session) 
						        {
						        	int accessGiverKcId = Integer.parseInt(request.getParameter("accgiver")); 
						        	
						        	session.setAttribute("tempIdasdgthfmUti", accessGiverKcId);
						        	return "redirect:/viewEmpDocumentsFrmUti.html";
						        }
						        
						        @RequestMapping(value = "/viewEmpDocumentsFrmUti", method = RequestMethod.GET)
						    	public ModelAndView viewEmpDocumentsFrmUti(HttpServletRequest request,HttpSession session) 
						        {	    			           	
								    		/* These all are code for Shared Kyc Documents. */	
						        	
						        	
						        	session = request.getSession(false);
									
									if (session.getAttribute("uti_Ind_id") == null)
									{						 
										 request.setAttribute("status","Your session has expired please login to use the portal !");
										 
										 session.removeAttribute("uti_Ind_id");
										 //session.invalidate();
										 
										 return new ModelAndView("login_utiliser");
									}				
									else
									{  	
										
										        session.setAttribute("historyBackUrl","redirectviewEmpDocumentsFrmUti.html?accgiver="+session.getAttribute("tempIdasdgthfmUti"));        
										
								    			DocumentDetailModel docdetails = new DocumentDetailModel();
								    			docdetails.setKyc_ind_id((int)session.getAttribute("tempIdasdgthfmUti"));
								    			docdetails.setDoc_type("Employment");
								    			
								    			String indKycId = profileinfoService.getkycid((int)session.getAttribute("tempIdasdgthfmUti"));
								    			String uti_kycid = (String)session.getAttribute("KycId");
												int uti_Indid = (int) session.getAttribute("uti_Ind_id");	
												
												int ind_id = (int)session.getAttribute("tempIdasdgthfmUti");	
																																					
												String getEmployeeStatusAsPerPattern = utiservice.getEmployeeStatusPattern(uti_kycid,indKycId);												
																								
												String employeeStatusArray[] = getEmployeeStatusAsPerPattern.split("EMPSEP");
												
												String extraAddedDocs = "";
												if(employeeStatusArray.length==2)
												{
													extraAddedDocs = employeeStatusArray[1];
												}
												
												String getEmployeeStatus = "";
												String getEmpDataandDetails = "";
												getEmployeeStatus = docservice.getstatus(docdetails);	
												
												if(!employeeStatusArray[0].equals("No Status"))
												{
													if(employeeStatusArray[0].contains("Employment"))
													{																										
														getEmpDataandDetails = utiservice.getEmployeeDetailsWithDocName(ind_id,getEmployeeStatus);		
														session.setAttribute("extraDocumentCheck","No");
													}
													else if(employeeStatusArray[0].contains("PaySlip"))
													{																										
														getEmpDataandDetails = utiservice.getEmployeeDetailsWithDocName(ind_id,getEmployeeStatus);	
														session.setAttribute("extraDocumentCheck","No");
													}
													else
													{				
														//This set code will exicute when no employment pattern is not exist but employment document added by an individual 
														
														String empStatusArray[] = getEmployeeStatus.split("/");
														String updateTheEmpPannelStatus = "";
														for(int i=0;i<empStatusArray.length;i++)
														{
															if(extraAddedDocs.contains(empStatusArray[i]))
															{
																updateTheEmpPannelStatus += empStatusArray[i] + "/";
															}
														}
														if(updateTheEmpPannelStatus.length()>0)
														{
															updateTheEmpPannelStatus = updateTheEmpPannelStatus.substring(0,updateTheEmpPannelStatus.length()-1);
														}
														getEmployeeStatus = updateTheEmpPannelStatus;
														getEmpDataandDetails = utiservice.getEmployeeDetailsWithDocName(ind_id,getEmployeeStatus);
														
														session.setAttribute("extraDocumentCheck","Yes");
													}
												}
											
												request.setAttribute("employeeData",getEmpDataandDetails);
												request.setAttribute("getEmployeeStatus",getEmployeeStatus);
												request.setAttribute("extraAddedDocs",extraAddedDocs);
												
												session.setAttribute("extraAddedDocsession",extraAddedDocs);
												
												Map<String, Object> model = new HashMap<String, Object>();
										    																									
												
												// Set all documents to download in a zip 
								    	 		
								    	 		  String allDocNames = "";
								    	 		  session.setAttribute("senderKycId", indKycId);
								    	 		 
								    	 		  String docNames = (String)request.getAttribute("employeeData");
								    	 		  
								    	 		  if(docNames!=null)
								    	 		  {
								    	 			 String docNameArray[] = docNames.split("ENDSEP");
									    	 		 for(int dn=0;dn<docNameArray.length;dn++)
									    	 		  {							   	 			  
									    	 			   String eachValue[] = docNameArray[dn].split("EMPSEP");
									    	 			   if(eachValue.length==2)
									    	 			   {
									    	 				   if(eachValue[1].contains("_"))
									    	 				   {
									    	 					  allDocNames += eachValue[1]+",";	
									    	 				   }
									    	 			   }								    	 			   							    	 			  
									    	 		  }
								    	 		  }
								    	 		 
								    	 		  if(!extraAddedDocs.equals("Not Added"))
								    	 		  {
								    	 			 allDocNames += ","+extraAddedDocs;
								    	 		  }							    	 		  
								    	 		  
								    	 		  if(allDocNames.endsWith(","))
								    	 		  {
								    	 			 allDocNames = allDocNames.substring(0, allDocNames.length()-1);
								    	 		  }
								    	 		  
								    	 		  if(allDocNames.startsWith(","))
								    	 		  {
								    	 			 allDocNames = allDocNames.substring(1, allDocNames.length());
								    	 		  }
								    	 		  
								    	 		  String extraDocumentCheck = (String)session.getAttribute("extraDocumentCheck");
								    	 		  
									    	 		 if(extraDocumentCheck.equals("Yes"))
									    	 		 {
									    	 			allDocNames = extraAddedDocs;
									    	 		 }
									    	 		 else
									    	 		 {
									    	 			 if(allDocNames.length()>0)
										    	 		  {
										    	 			  String allDocNamesArray[] = allDocNames.split(",");
										    	 			  String uniqueDocName = "";
										    	 			  for(int a=0;a<allDocNamesArray.length;a++)
										    	 			  {
										    	 				 if(!uniqueDocName.contains(allDocNamesArray[a]))
										    	 				 {
										    	 					uniqueDocName += allDocNamesArray[a] + ",";
										    	 				 }
										    	 			  }
										    	 			  if(uniqueDocName.length()>0)
										    	 			  {
										    	 				 uniqueDocName = uniqueDocName.substring(0, uniqueDocName.length()-1);
										    	 			  }
										    	 			  
										    	 			 allDocNames = uniqueDocName;
										    	 		  }
									    	 		 }
								    	 		
								    	 		  session.setAttribute("allDocumentNames", allDocNames);
								    	 		 
								    	 	   // End of Set all documents to download in a zip 
											  
										      /* End Of These all are code for Shared Kyc Documents. */	
																		
										      /* In this page left side division content */	
												 
											   	String kycId = profileinfoService.getkycid(ind_id);
											   	
											   	String created_year = signupService.findyear(kycId);	
											   	
												session.setAttribute("created_year", created_year);
												
												String visibility = signupService.getVisibility(ind_id);
												session.setAttribute("visibility", visibility);
												
												request.setAttribute("searched_Indid", ind_id);
												
												IndSignupBean signupBean = new IndSignupBean();
												signupBean.setKycid(kycId);
												
												BasicDetail BasicDetail=new BasicDetail();
												BasicDetail.setInd_kyc_id(Integer.toString(ind_id));
													    	
												model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(ind_id)));
												
												RegHistoryModel regModel = signupService.getRegistrationDetails(ind_id);
												
												model.put("regname", prepareBean(signupService.getname(ind_id)));
												
												request.setAttribute("utiFirstName", signupService.getUtilizerName(uti_Indid));
																		 
												if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(Integer.toString(ind_id)))) 
												{			
											     	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(Integer.toString(ind_id)));		
												    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
												}
															
												if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
												{
													int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
													model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));									
										    	}
											
												String ind_kyc_id = kycId;
												IndSignupModel individual = new IndSignupModel();
												
												individual.setKycid(ind_kyc_id);
												String i=Integer.toString(ind_id);
												
												Matrial matrial=new Matrial();
												matrial.setInd_kyc_id(i);
												
												BasicDetail.setInd_kyc_id(i);
											
												if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
												{			
													  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
													  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
												}
												
												if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
												{
													int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
													model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
											    }
														
												if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
												{	
													String picName = profileinfoService.getLatestPicName(ind_id);
													session.setAttribute("profilePicName", picName);
													int pic_id = profileinfoService.getPrifilePicId(picName);				
													model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
												}
													
												
												String searchedIndId = profileinfoService.getSearchedAboutUsInfo(ind_id);
												request.setAttribute("searchedIndId", searchedIndId);
												
												String indRegVisibility = profileinfoService.getRegVisisbility(ind_id);
												String indBasicVisibility = profileinfoService.getBasicVisisbility(ind_id);
												String indFamilyVisibility = profileinfoService.getFamilyVisisbility(ind_id);
												String indSocialVisibility = profileinfoService.getSocialVisisbility(ind_id);
												
												request.setAttribute("regVisibility", indRegVisibility);
												request.setAttribute("basicVisibility", indBasicVisibility);
												request.setAttribute("familyVisibility", indFamilyVisibility);
												request.setAttribute("socialVisibility", indSocialVisibility);
												
												String individualKycId = profileinfoService.getkycid(ind_id);
												String utilizerKycId = (String)session.getAttribute("KycId");
												
												String accessStatus = profileinfoService.getUtilizerAccessForIndividual(individualKycId,utilizerKycId);
												String profileAccessStatus = "";
												String otherAccess = "";
												if(accessStatus.length()>1)
												{
													if(accessStatus.startsWith("PROFILE"))
													{
														String profileAccessArray[] = accessStatus.split("/");
														String profileAccessDetails[] = profileAccessArray[0].split(":");
														profileAccessStatus += profileAccessDetails[1];
														
														
													}			
												}
												
												request.setAttribute("profileAccessDetails", profileAccessStatus);
												request.setAttribute("otherAccess", accessStatus);
												
											
										/*  End of left side page division content  */	
												
												int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
												int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
												
												request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
												request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");			
												
												
						    		    return new ModelAndView("utilizerAccessEmpDocs",model);	
									}
						    	}
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	        
				        /* Code  Related to individidual   */
				        
				        
								        private SocialaccBean prepareSocialaccBean(Socialacc socialacc) {
								    		
								        	SocialaccBean bean = new SocialaccBean();
							
								    		bean.setSocialwebsitelink(socialacc.getWebsite_link());
								    		bean.setSocialwebsitename(socialacc.getWebsite_name());
								    		bean.setSociallinkedlnlink(socialacc.getLinkedln_link());
								    		bean.setSociallinkedlnname(socialacc.getLinkedln_name());
								    		bean.setSocialtwitterlink(socialacc.getTwitter_link());
								    		bean.setSocialtwittername(socialacc.getTwitter_name());
								    		
								    		bean.setInd_kyc_id(socialacc.getInd_kyc_id());
								    		return bean;
								    	}
								    	private BasicDetailBean prepareBasicBean(BasicDetail BasicDetail)
								    	{
								    		BasicDetailBean bean=new BasicDetailBean();
								    		bean.setHobbies(BasicDetail.getHobbies());
								    		bean.setDOB(BasicDetail.getDate_of_birth());
								    		bean.setEmailalternative(BasicDetail.getAlternate_email());
								    		bean.setLanguages(BasicDetail.getLanguages_known());
								    		bean.setMatrialstatus(BasicDetail.getMarital_status());
								    		bean.setNationality(BasicDetail.getNationality());
								    		bean.setPassportsize(BasicDetail.getProfile_pic());
								    		bean.setPermanent_address(BasicDetail.getPermanent_address());
								    		bean.setPresent_address(BasicDetail.getPresent_address());
								    		bean.setTeloff(BasicDetail.getTell_office());
								    		bean.setTelres(BasicDetail.getTell_residence());
								    		return bean;
								    	}
								         private IndSignupBean prepareBean(IndSignupModel IndSignup) {
								    		
								    		IndSignupBean bean=new IndSignupBean(); 
								    		bean.setFirstname(IndSignup.getFirstname());
								    		bean.setLastname(IndSignup.getLastname());
								    		bean.setMiddlename(IndSignup.getMiddlename());
								    		bean.setMobileno(IndSignup.getMobileno());
								    		bean.setEmailid(IndSignup.getEmailid());
								    		bean.setCr_date(IndSignup.getCr_date());
								    		return bean;
								    		
								    	}
								         
								    	
								         private RegHistoryBean prepareRegHistory(RegHistoryModel regModel)
								         {
								        	 RegHistoryBean bean = new RegHistoryBean();
								        	 bean.setFirst_name(regModel.getFirst_name());
								        	 bean.setMiddle_name(regModel.getMiddle_name());
								        	 bean.setLast_name(regModel.getLast_name());
								        	 bean.setGender(regModel.getGender());
								        	 bean.setEmail_id(regModel.getEmail_id());
								        	 bean.setMobile_no(regModel.getMobile_no());
								        	 bean.setCr_date(regModel.getCr_date());
								        	 
								        	 return bean;
								         }								    
								        
								        private ProfilePictureBean prepareProfilePicBean(ProfilePictureModel picModel)
								    	{
								    		ProfilePictureBean picBean = new ProfilePictureBean();
								    		
								    		picBean.setKyc_ind_id(picModel.getKyc_ind_id());
								    		picBean.setProfile_picture_name(picModel.getProfile_picture_name());
								    		picBean.setCr_date(picModel.getCr_date());	
								    		
								    		return picBean;
								    	}
				        
								        private List<DocumentDetailsBean> prepareDocumentDetailBeanForHistory(List<DocumentDetailModel> docdetails) {
								         	
								        	
								         	List<DocumentDetailsBean> bean = null;
								         	
								    		if (docdetails != null && !docdetails.isEmpty()) {
								    			bean = new ArrayList<DocumentDetailsBean>();
								    			DocumentDetailsBean beans = null;

								    			for (DocumentDetailModel docdetail : docdetails) {
								                 beans = new DocumentDetailsBean();
								    				beans.setDoc_id(docdetail.getDoc_id());
								    				beans.setDoc_name(docdetail.getDoc_name());
								    				beans.setCr_date(docdetail.getCr_date());
								    				beans.setDes(docdetail.getDes());
								    				beans.setDoc_des(docdetail.getDoc_des());
								    				beans.setDocs_data(docdetail.getDocs_data());
								    				bean.add(beans);
								    			}
								    		}
								     		return bean;
								     	} 
								    	 
				        
				        /* End of code related to individual  */
	        
	        
								        
								        
	        /* End of code utilizer View Individual documents */
	        
								        
        @RequestMapping(value = "/downlaodalldocumentsinzipForUtilizer", method = RequestMethod.POST)
    	public void  downlaodalldocumentsinzip(HttpServletRequest request,HttpServletResponse response,HttpSession session)throws ServletException, IOException 
    	{	
        	
        	
    		String zipFileName = request.getParameter("codeName");
    					
    		// Required file Config for entire Controller 

    		 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
    		 String kycMainDirectory = fileResource.getString("fileDirectory");		
    		 File filePath = new File(kycMainDirectory);
    		
    		// End of Required file Config for entire Controller 
    					 
    		response.setContentType("Content-type: text/zip");
    		response.setHeader("Content-Disposition","attachment; filename="+zipFileName+".zip");
    		List<File> files = new ArrayList<File>();
    				
    		//String kycid = (String)session.getAttribute("senderKycId");		
    		int ind_id = Integer.parseInt(request.getParameter("extp"));
    		int year = accservice.findyear(" ", ind_id);
    		String docType = "";
    		
    		
    		String documents = request.getParameter("exttemp");
    		
    		System.out.println("Document Names="+documents);
    		
    		String documentNameArray[] = documents.split(",");
    		
    		String individualId = ind_id+"";
    		String registerYear = year+"";
    		
    		
    		for(int i=0;i<documentNameArray.length;i++)
    		{
    			
    			docType = accservice.findDocType(documentNameArray[i],ind_id);
    			
    			if(docType.equals("KYC_DOCUMENT"))
    			{
    				docType = "KYC_DOCUMENT";
    				
    				FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,documentNameArray[i],request);
    				
    				files.add(new File(filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"KYC_DOCUMENT"+"/"+documentNameArray[i]));
    			}
    			if(docType.equals("ACADEMIC_DOC"))
    			{
    				docType = "ACADEMIC_DOC";
    				
    				FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,documentNameArray[i],request);
    				
    				files.add(new File(filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"ACADEMIC_DOC"+"/"+documentNameArray[i]));
    			}
    			if(docType.equals("Financial_DOC"))
    			{
    				docType = "Financial_DOC";
    				
    				FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,documentNameArray[i],request);
    				
    				files.add(new File(filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"Financial_DOC"+"/"+documentNameArray[i]));
    			}
    			if(docType.equals("Employment_DOC"))
    			{
    				docType = "Employement_DOC";
    				
    				FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,documentNameArray[i],request);
    				
    				files.add(new File(filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"Employement_DOC/"+documentNameArray[i]));
    			}	
    		}
    		
    									
    		ServletOutputStream out = response.getOutputStream();
    		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));

    		for (File file : files) {
   			
    			zos.putNextEntry(new ZipEntry(file.getName()));

    			// Get the file
    			
    			FileInputStream fis = null;
    			try 
    			{				
    				fis = new FileInputStream(file);
    			}
    			catch (FileNotFoundException fnfe)
    			{
    				zos.write(("ERRORld not find file " + file.getName()).getBytes());
    				zos.closeEntry();
    				
    				continue;
    			}

    			BufferedInputStream fif = new BufferedInputStream(fis);
    			int data = 0;
    			while ((data = fif.read()) != -1)
    			{
    				zos.write(data);
    			}
    			
    			fif.close();

    			zos.closeEntry();
    		
    		}

    		zos.close();
    			
    	}	
		
        
        @RequestMapping(value="/getIndProfilePicturesHistory",method=RequestMethod.POST)
        public ModelAndView getIndProfilePicturesHistory(HttpServletRequest request,HttpSession session)
        {       	
	         int ind_id = Integer.parseInt(request.getParameter("indvidualId")); 		 
	       	 String profilePhoto = profileinfoService.getProfilePhoto(ind_id);
	       	 String id = Integer.toString(ind_id);
	       	 
	       	 String historyDitails = id+"@@@"+profilePhoto;
	       	 request.setAttribute("profilePhotoHistory", historyDitails);
	       	 request.setAttribute("regYear", profileinfoService.getYear(ind_id));  	 
	       	 
	       	 return new ModelAndView("utiViewIndProfilePhotoHistory");    	   	 
        }
        
}
