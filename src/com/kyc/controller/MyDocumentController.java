package com.kyc.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.spec.IvParameterSpec;
import javax.imageio.ImageIO;
import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.captcha.Captcha;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.*;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.bean.BasicDetailBean;
import com.kyc.bean.DocumentDetailsBean;
import com.kyc.bean.DocumentHierarchyBean;
import com.kyc.bean.EmployeeBean;
import com.kyc.bean.FamilyDetailBean;
import com.kyc.bean.IndSignupBean;
import com.kyc.bean.ProfilePictureBean;
import com.kyc.bean.RegHistoryBean;
import com.kyc.bean.SocialaccBean;
import com.kyc.bean.SummarydetailBean;
import com.kyc.model.AcademicFileUploadModel;
import com.kyc.model.BasicDetail;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.IndSignupModel;
import com.kyc.model.IndividualPlanDetailModel;
import com.kyc.model.Matrial;
import com.kyc.model.ProfilePictureModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.SalarySlipModel;
import com.kyc.model.Socialacc;
import com.kyc.model.Summarydetail;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.VisibilityModel;
import com.kyc.service.AccessService;
import com.kyc.service.DocService;
import com.kyc.service.IndSignupService;
import com.kyc.service.ProfileinfoService;
import com.kyc.service.UtiliserService;
import com.kyc.util.AccessControl;
import com.kyc.util.CaptureIPandMACaddressUtil;
import com.kyc.util.FileEncryptionDecryptionUtil;
import com.kyc.util.MakeBackUpForDocuments;
import com.kyc.util.PasswordEncryptionDecryption;
import com.kyc.util.SendEmailUtil;
import com.kyc.util.SendSms;


@Controller
public class MyDocumentController {

	
	@Autowired
	private IndSignupService signupService;
	
	@Autowired
	private DocService docservice;
	
	@Autowired
	private ProfileinfoService profileinfoService;
	
	@Autowired
	private AccessService accservice;

	@Autowired
	private UtiliserService utiservice;
	
	
	/*@Autowired
    private JavaMailSender mailSender;*/
		
	
	// Required Config for entire Controller 
	
	static final Logger LOGGER = Logger.getLogger(MyDocumentController.class);
    
	
	Cookie individualCookie = new Cookie("individualCookie", "");
	
	
	private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MyDocumentController.class);
	FileAppender loggerfile;	 
	
	ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
	String logDirectory=loggerFileDirectory.getString("loggerDirectoryDirectory");
	
	
	ResourceBundle smsresource = ResourceBundle.getBundle("resources/smsParameters");
	String userName=smsresource.getString("userName");
	String password = smsresource.getString("password");
	String senderId = smsresource.getString("senderId");
	
	String smscommonUrl = smsresource.getString("commonUrl");
	String smsfromDetails = smsresource.getString("fromDetails");
		   
    SendSms obj = new SendSms();
    	
	public String individualCategory = "Individual";
	
	String Ind_id="";
	
	String IND_KYCID = "";
	 
	CaptureIPandMACaddressUtil  addressObject = new CaptureIPandMACaddressUtil();
	
		     
	// End of Required Config for entire Controller 
	
		
	// Required file Config for entire Controller 
			
	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 String kycMainDirectory = fileResource.getString("fileDirectory");	
	 String kycSubDirectory = fileResource.getString("fileSubDirectory");	
	 File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller 
		
	
	//Controller Method are Starts from here
			
	@RequestMapping(value = "/validateKYCid", method = RequestMethod.POST)
	public @ResponseBody String validateKycId(HttpServletRequest request) {
	
		String kycId = request.getParameter("kycId");	
		String status = docservice.getKycRegistrationStatus(kycId); 
		
		return status;		
	}
		
	@RequestMapping(value = "/individual1", method = RequestMethod.GET)
	public ModelAndView addEmployee(HttpServletRequest request) {
				
		return new ModelAndView("indProfile");
	}
	
	
	@RequestMapping(value = "/getintotheHomePage", method = RequestMethod.GET)
	public String getintotheHomePage(HttpServletRequest request) {
		
		request.setAttribute("kycdocumentList", docservice.getKYCComp());

		String returnType = "";
		HttpSession session = request.getSession(false);
		
		if (session.getAttribute("uti_Ind_id") != null)
		{						
			returnType =  "redirect:/utimyProfile.html";
		}
		else if(session.getAttribute("Ind_id")!=null)
		{
			returnType =  "redirect:/profiletohome.html";	
		}
		else if(session.getAttribute("adminId")!=null)
		{	
			returnType =  "redirect:/authenticateAdminLogin.html";						
		}
		else
		{
			returnType =  "redirect:/clenseeHome.html";	
		}
		return returnType;	
				
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String showHome(HttpSession session,HttpServletRequest request) {
				
			return "login_corporate";				
	}
	
	
	@RequestMapping(value = "/general-error", method = RequestMethod.GET)
	public String generalError(HttpSession session,HttpServletRequest request) 
	{			
		session = request.getSession(false);			
		return "kycErrorPage";
	}
	
	
	@RequestMapping(value ="/outduesession", method = RequestMethod.GET)
	 public ModelAndView getOutSessionExpired(HttpServletRequest request,HttpSession session,HttpServletResponse response) throws IOException {
		
		 request.setAttribute("kycdocumentList", docservice.getKYCComp());
		 request.setAttribute("status","Your session has expired please login to use the portal !");
		 			 
		       String indId = individualCookie.getValue();
		  
					 if(!indId.isEmpty())
					 {
						 String kycId = signupService.getKYCIDFromCookiesValue(indId);
						 String registeredYear = signupService.findyear(kycId);
						 
						 String OtherIndId = profileinfoService.getKycIdOfAccessGiven(kycId);          // Get All Other Ind Id For Decrypt the Variable.
							
							File ourPath = new File(kycMainDirectory);
							
							if(!OtherIndId.equals("No Data"))
							{
								String otherRegYear = profileinfoService.getYearFormIndId(OtherIndId);
								
								OtherIndId += "," + indId;
								otherRegYear += "," + registeredYear;
								
								String OtherIndIdArray[] = OtherIndId.split(",");
								String otherRegYearArray[] = otherRegYear.split(",");
								
								for(int id=0;id<OtherIndIdArray.length;id++)
								{
									File targetFile = new File((ourPath.getPath()+"/"+otherRegYearArray[id]+"/"+OtherIndIdArray[id]+"/Document"));
									JIREncryptionUtils.encryptAllDocs(targetFile,"enc");				
								}									
							}
							else
							{
								File targetFile = new File((ourPath.getPath()+"/"+registeredYear+"/"+indId+"/Document"));
								JIREncryptionUtils.encryptAllDocs(targetFile,"enc");				
							}				
							 	
							    // End of  File encryption 
							 							
							    String DATE_FORMAT_NOW = "dd-MM-yyyy";
							    Date date = new Date();
								SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
								String stringDate = sdf.format(date );
								stringDate += "-logout_Due_To_Session_TimeOut";
								   
								loggerfile = new FileAppender(new SimpleLayout(),""+logDirectory+"/"+stringDate+".log");
							    log.addAppender(loggerfile);
							    loggerfile.setLayout(new SimpleLayout());
							    
							    log.info(kycId+"- All Files got encrypted due to session time out.");
							    
							    log.info("-"+date+"--"+addressObject.getIPddress(request)+"--"+kycId+" has LogOut.");
							    
							    log.info("-------------------------");
					 }
					 else
					 {
						 //System.out.println("Individual Id="+indId+",KYC ID=null,Registerd Year=null");
					 }
					 
					 session.removeAttribute("Ind_id");
					 //session.invalidate();
		 
		          return new ModelAndView("individualPage");			 
	    }
	
	 @SuppressWarnings("unused")
	 @RequestMapping(value ="/checksessionforIndividual", method = RequestMethod.POST)
	 public @ResponseBody String checkSessioninIndividualSide(HttpServletRequest request,HttpSession session) {
				
		 session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{							
				return "Not Exist";		
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				return "Not Exist";		
			}
			else if(session == null)
			{
				return "Not Exist";			
			}
			else
			{				
				return "Exist";									
			}
	 }
	
	 
	@RequestMapping(value = "/loginkyc", method = RequestMethod.GET)
	public String loginPage(HttpServletRequest request,HttpSession session) {
		
				
		request.setAttribute("kycdocumentList", docservice.getKYCComp());
		return "login_individual";	
		
	}
	
	
	@RequestMapping(value = "/individual", method = RequestMethod.GET)
	public String showIndividual(HttpServletRequest request) {
		
		request.setAttribute("kycdocumentList", docservice.getKYCComp());

		HttpSession session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null || !request.isRequestedSessionIdValid())
		{
			System.out.println("So session Value when not available1="+(String)session.getAttribute("Ind_id"));
			
			return "login_individual";
		}
		else if(((String)session.getAttribute("Ind_id")).equals(""))
		{
			return "login_individual";
		}
		else
		{	
			System.out.println("So finaly what is the session value="+(String)session.getAttribute("Ind_id"));
			
			return "redirect:/profiletohome.html";				
		}			
	}
	
	
	@RequestMapping(value = "/clenseeHome", method = RequestMethod.GET)
	public ModelAndView showCorporate() {
		
		return new ModelAndView("login_corporate");
	}
	
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "/utiliser", method = RequestMethod.GET)
	public String showUtiliser(HttpServletRequest request,HttpSession session) {
		
        session = request.getSession(false);		
		if (session.getAttribute("uti_Ind_id") != null)
		{						 
			 return "redirect:/utimyProfile.html";
		}		
		else
		{
			return "login_utiliser";	
		}					
	}
		
	 @RequestMapping(value = "/profiletohome", method = RequestMethod.GET)
	 public ModelAndView showSignUp1(HttpServletRequest request,HttpSession session) {
	 		
		   session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("individualPage");	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return  new ModelAndView("individualPage");	
			}
			else
			{	
			      
						Summarydetail summary=new Summarydetail();
						summary.setInd_kyc_id((String)session.getAttribute("Ind_id"));
						String i = (String)session.getAttribute("Ind_id");
						BasicDetail BasicDetail=new BasicDetail();
						BasicDetail.setInd_kyc_id((String)session.getAttribute("Ind_id"));
						DocumentDetailModel docdetails = new DocumentDetailModel();
				    	docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
						 
				    	int individual_id = Integer.parseInt(i);
				    	String visibility = signupService.getVisibility(individual_id);
						session.setAttribute("visibility", visibility);
									
						Map<String, Object> model = new HashMap<String, Object>();
						model.put("regname", prepareBean(signupService.getname(Integer.parseInt((String)session.getAttribute("Ind_id")))));
						model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(Integer.parseInt((String)session.getAttribute("Ind_id")))));
						model.put("indName", prepareBean(signupService.getname(Integer.parseInt((String)session.getAttribute("Ind_id")))));
									
						if (!profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
						{								   			
						   model.put("documentdetail",profileinfoService.Getacademicdetail(docdetails));
						   model.put("postOn", profileinfoService.GetacademicPostDate(docdetails));	
					  	}
						
						if (!profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
						{					
						  model.put("employeedetail", prepareDocumentDetail(profileinfoService.Getemployeedetail(docdetails)));
						  model.put("empdate", profileinfoService.GetEmpPostDate(docdetails));			
					  	}
						
						if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
						{						
							int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
							model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));			  	
						}
									 			
						if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
							{
								int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));
								
								model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
					    	}
						
						if(!"failure".equals(profileinfoService.Getregid(summary))) 
						{								
							int su= Integer.parseInt(profileinfoService.Getregid(summary));				
							model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));				
						}
											
						if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
						{			
							int fam=Integer.parseInt(profileinfoService.Getfamid(i));			
						    model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
						}
						
						int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
						if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
						{	
							String picName = profileinfoService.getLatestPicName(ind_id);
							session.setAttribute("profilePicName", picName);
							int pic_id = profileinfoService.getPrifilePicId(picName);
							
							model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
						}
						
						return new ModelAndView("individualProfile",model);
			}
			
	    }

		@RequestMapping(value = "/signin", method = RequestMethod.GET)
		public ModelAndView showSignIn() {
			return new ModelAndView("signIn");
		}
	
		@RequestMapping(value = "/signup", method = RequestMethod.POST)
		public ModelAndView showSignUp() {
			return new ModelAndView("signUp");
		}
	
		@RequestMapping(value = "/search_financial", method = RequestMethod.GET)
		public ModelAndView showsearch_financial() {
			
			return new ModelAndView("individual_financial_document");
		}
		
		@RequestMapping(value = "/kyc_viewmore", method = RequestMethod.GET)
		public ModelAndView detailskycviewmore() {
			
			return new ModelAndView("kyc_viewmore");
		}
				
		@RequestMapping(value = "/financial_doc_not_found", method = RequestMethod.GET)
		public ModelAndView finNotFound() {
			return new ModelAndView("financial_doc_not_found");
		}
				
		//For Authentication Login
		
		@RequestMapping(value ="/signOut", method = RequestMethod.GET)
		 public ModelAndView getSignOut(HttpServletRequest request,HttpSession session,HttpServletResponse response) throws IOException {
			 
						 
			 // Start File encryption 
			session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{	
				request.setAttribute("kycdocumentList", docservice.getKYCComp());
				request.setAttribute("status","Thank you for using Clensee, You have successfully logged out.");
				
				return new ModelAndView("individualPage");	
			}
			else
			{
				int individualInd_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
				
				String IndKycId = profileinfoService.getkycid(individualInd_id);							
				String OtherIndId = profileinfoService.getKycIdOfAccessGiven(IndKycId);          // Get All Other Ind Id For Decrypt the Variable.
				
				File ourPath = new File(kycMainDirectory);
				
				if(!OtherIndId.equals("No Data"))
				{
					String otherRegYear = profileinfoService.getYearFormIndId(OtherIndId);
					
					OtherIndId += "," + (String)session.getAttribute("Ind_id");
					otherRegYear += "," + (String)session.getAttribute("created_year");
					
					String OtherIndIdArray[] = OtherIndId.split(",");
					String otherRegYearArray[] = otherRegYear.split(",");
					
					for(int id=0;id<OtherIndIdArray.length;id++)
					{
						File targetFile = new File((ourPath.getPath()+"/"+otherRegYearArray[id]+"/"+OtherIndIdArray[id]+"/Document"));
						JIREncryptionUtils.encryptAllDocs(targetFile,"enc");				
					}	
					
				}
				else
				{
					File targetFile = new File((ourPath.getPath()+"/"+session.getAttribute("created_year")+"/"+session.getAttribute("Ind_id")+"/Document"));
					JIREncryptionUtils.encryptAllDocs(targetFile,"enc");				
				}				
				 	
				// End of  File encryption 
				 
		    
			 // Logger File  for current user's IpAddress and Mac Address with Name Date and Time
			    
				ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
				String IndividualDirectory=loggerFileDirectory.getString("loggerDirectoryIndividual");

				    String DATE_FORMAT_NOW = "dd-MM-yyyy";
				    Date date = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
					String stringDate = sdf.format(date );
					stringDate += "-Clensee-Logout";
					   
					loggerfile = new FileAppender(new SimpleLayout(),""+IndividualDirectory+"/"+stringDate+".log",false);
				    log.addAppender(loggerfile);
				    loggerfile.setLayout(new SimpleLayout());
				    
				    log.info("-"+date+"--"+addressObject.getIPddress(request)+"--"+profileinfoService.getkycid(individualInd_id)+" has LogOut.");
				   
				    log.info("-------------------------");
				    				    				    
				    session.removeAttribute("Ind_id");					
				    session.removeAttribute("Kyc_id"); 		
				    
				    request.setAttribute("kycdocumentList", docservice.getKYCComp());
					request.setAttribute("status","Thank you for using Clensee, You have successfully logged out.");
					
				    return new ModelAndView("individualPage");	
			 }
			
		 }
		
		public ModelAndView getSigOutUnderSessionExpired(HttpServletRequest request,HttpSession session) {
			 			 		 
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 request.getSession().invalidate();
			 						 
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has Expired so please relogin !");
					 
			 return new ModelAndView("individualPage");	
		 }
	
	String created_year = "";
	
	@RequestMapping(value = "/authenticateLogin", method = RequestMethod.GET)
	public String copyPasteActivity(HttpServletRequest request,HttpSession session) throws IOException, Exception 
	{		
		request.setAttribute("kycdocumentList", docservice.getKYCComp());
		if (session.getAttribute("Ind_id") == null || session.getAttribute("Ind_id").equals(""))
		{
			return "login_individual";
		}
		else
		{
			return "redirect:/profiletohome.html";
		}	
	}
	
	
	@RequestMapping(value="/accademic_details_help",method=RequestMethod.GET)
    public String accademicDetailsDelp(HttpServletRequest request,HttpSession session)
    {
   	     int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
		 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
		  
   	     return "accademic_details_help";
    }
	
	@RequestMapping(value="/employee_details_help",method=RequestMethod.GET)
    public String employeeDetailsHelp(HttpServletRequest request,HttpSession session)
    {
   	     int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
		 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
		 
   	     return "employee_details_help";
    }
	
	@RequestMapping(value="/financial_details_help",method=RequestMethod.GET)
    public String financialDetailsHelp(HttpServletRequest request,HttpSession session)
    {
   	    int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
		 
   	    return "financial_details_help";
    }
	
	@RequestMapping(value="/kyc_document_help",method=RequestMethod.GET)
    public String kycdocumenthelp(HttpServletRequest request,HttpSession session)
    {
   	 	 int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
		 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
		 
		 return "kyc_document_help";
    }
    
	
    
	@RequestMapping(value = "/authenticateLogin", method = RequestMethod.POST)
	public ModelAndView getAuthenticateLogin(HttpServletRequest request,HttpSession session,
			@ModelAttribute("command") IndSignupBean signupBean,SummarydetailBean SummarydetailBean,
			BindingResult result) throws IOException, Exception 
	 {				
	 
		
		File ourPath = new File(kycMainDirectory);
		
		String kycLoginId ="";
		IndSignupModel individual = new IndSignupModel();
				
		individual.setKycid(signupBean.getKycid());	
		
		individual.setPassword(signupBean.getPassword());
   		             		
		String kycId = individual.getKycid();
		 
		String authenticateStatus = signupService.checkIndividual(individual);
						
					if (!"failure".equals(authenticateStatus))    //checks the User KYCID/EmailId and Password
					{	
						
						if(signupBean.getKycid().contains("@") || signupBean.getKycid().contains(".com"))
						{
							String kid = signupBean.getKycid();
							kycLoginId += docservice.getKycIdViaEmailId(kid);	
							
							individual.setKycid(kycLoginId);			
						}
						else
						{
							individual.setKycid(signupBean.getKycid());
						}
												
							String Ind_id=signupService.getInd_id(individual);			
							int individual_id = Integer.parseInt(Ind_id);
							session.setAttribute("Ind_id", Ind_id);		
																					
							individualCookie.setValue(Ind_id);
							
							if(!signupBean.getKycid().contains("@") && !signupBean.getKycid().contains(".com"))	
							{	
								kycLoginId = signupBean.getKycid();
							    session.setAttribute("Kyc_id", signupBean.getKycid());
							    System.out.println("kycId via KYC Id "+signupBean.getKycid());
							}
							else
							{								
							    kycLoginId = docservice.getKycIdViaEmailId(signupBean.getKycid());	
							    System.out.println("kycId via Email Id "+kycLoginId);
							    session.setAttribute("Kyc_id", kycLoginId);
							}	
													    						    						    
							created_year = signupService.findyear(kycLoginId);			
							String registeredYear = signupService.findyear(kycLoginId);		
							session.setAttribute("created_year", registeredYear);
							
							String visibility = signupService.getVisibility(individual_id);
							session.setAttribute("visibility", visibility);
							
							Summarydetail summary=new Summarydetail();
							summary.setInd_kyc_id((String)session.getAttribute("Ind_id"));
							BasicDetail BasicDetail=new BasicDetail();
							BasicDetail.setInd_kyc_id((String)session.getAttribute("Ind_id"));
							DocumentDetailModel docdetails = new DocumentDetailModel();
					    	docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
					    	
					    	Map<String, Object> model = new HashMap<String, Object>();
					    		    				
							model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(Integer.parseInt((String)session.getAttribute("Ind_id")))));
							
							model.put("regname", prepareBean(signupService.getname(Integer.parseInt((String)session.getAttribute("Ind_id")))));
							
							model.put("indName", prepareBean(signupService.getname(Integer.parseInt((String)session.getAttribute("Ind_id")))));
							
							if (!profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
							{				
						       model.put("documentdetail",profileinfoService.Getacademicdetail(docdetails));
						       model.put("postOn", profileinfoService.GetacademicPostDate(docdetails));
						  	}
													
							if (!profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
							{			
							   model.put("employeedetail", prepareDocumentDetail(profileinfoService.Getemployeedetail(docdetails)));        //   This Information For Employee Details
							   model.put("empdate", profileinfoService.GetEmpPostDate(docdetails));											//    This information last update date	   
						  	}
			
							if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid((String)session.getAttribute("Ind_id")))) 
							{			
								int fam=Integer.parseInt(profileinfoService.Getsocialaccid((String)session.getAttribute("Ind_id")));
								
								model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));							  	
							}
										
							if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
								{
									int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));									
									model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));														
								
									/*List<BasicDetail> basicDetails = profileinfoService.getLatestBasicDetailsInfo(BasicDetailid);
									*/									
						    	}
							
							if(!"failure".equals(profileinfoService.Getregid(summary))) 
							{						
								int su= Integer.parseInt(profileinfoService.Getregid(summary));
								
								model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));
														
							}
							
							String ind_kyc_id=(String)session.getAttribute("Kyc_id");
						
							individual.setKycid(ind_kyc_id);
							String i=signupService.getInd_id(individual);
							
							summary.setInd_kyc_id(i);
							
							Matrial matrial=new Matrial();
							matrial.setInd_kyc_id(i);
							
							BasicDetail.setInd_kyc_id(i);
						
							if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
							{			
							    int fam=Integer.parseInt(profileinfoService.Getfamid(i));			
						        model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
							}
							
							if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
							{			
							  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
							  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));
						  	
							}
							
							if(!"failure".equals(profileinfoService.Getregid(summary))) 
							{
								int su= Integer.parseInt(profileinfoService.Getregid(summary));
								model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));				
							}
			
							if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
							{
								int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
								model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
						    }
							
							int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
							if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
							{	
								String picName = profileinfoService.getLatestPicName(ind_id);
								session.setAttribute("profilePicName", picName);
								int pic_id = profileinfoService.getPrifilePicId(picName);				
								model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
							}
						
							
							
							// Logger File  for current user's IpAddress and Mac Address with Name Date and Time
							
							ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
							String individualDirectory=loggerFileDirectory.getString("loggerDirectoryIndividual");

							
							String DATE_FORMAT_NOW = "dd-MM-yyyy";
							Date date = new Date();
							SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
							String stringDate = sdf.format(date );
							stringDate += "-Clensee-Login";
							   
							loggerfile = new FileAppender(new SimpleLayout(),""+individualDirectory+"/"+stringDate+".log",false);
						    log.addAppender(loggerfile);						    
						    loggerfile.setLayout(new SimpleLayout());						   
						    log.info("-"+date+"--"+addressObject.getIPddress(request)+"--"+signupBean.getKycid()+" has Login.");						   
						    log.info("-------------------------");
						   
						   
						    // Logger Config who has Log in	
						 
						    
						    
						    
						    // Migrate plan if 1 year complete with existence plan
									
						    		int ownInd_id = Integer.parseInt(i);
									String currentPlanName = profileinfoService.getCurrentPlanName(ownInd_id);
									if(!currentPlanName.equals("Basic"))
									{
										Date register_Date = profileinfoService.getRegisterDate(ownInd_id);
										Date todays_Date = new Date();
										
										long diff = 0;
										   
									   Calendar c1 = Calendar.getInstance();		   
									   c1.setTime(register_Date);		 
									   Calendar c2 = Calendar.getInstance();		  
									   c2.setTime(todays_Date);		  
									   long ms1 = c1.getTimeInMillis();
									   long ms2 = c2.getTimeInMillis();		  
									   diff = ms2 - ms1;
									  
									   int diffInDays = (int) (diff / (24 * 60 * 60 * 1000));
									  
										  if(diffInDays>=364)
										  {
											  profileinfoService.migratePlanAfterOneYear(ownInd_id);
											  
											   // Tracking Activity Details
										        
										        String Kyc_id= (String)session.getAttribute("Kyc_id");
												Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
												Profileupdatemodel.setProfileupdatedetail("Your validity is over so your plan migrated to Basic !");
												Profileupdatemodel.setProfileupdatetime(new Date());
												Profileupdatemodel.setType_of_alert("Plan Updated");
												Profileupdatemodel.setMore_about_it("Event done in Plan");
											    Profileupdatemodel.setKyc_id(Kyc_id);
												profileinfoService.profileupdate(Profileupdatemodel);

									          // End Of Tracking Activity Details
										  }
									}
									
						    // End of Migrate Plan if 1 Year complete 
						    
							
						    						   						    						   						    
							// File Decryption when Login 
							
							session.setAttribute("fileStorageCondition", "true");
							session.setAttribute("generateCodeCondition", "true");
							session.setAttribute("applyCodeCondition", "true");
							session.setAttribute("allowAccessCondition", "true");
							session.setAttribute("revokeAccessCondition", "true");

							session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
							
							int individualInd_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
							
							String IndKycId = profileinfoService.getkycid(individualInd_id);							
							
								/*File targetFile = new File((ourPath.getPath()+"/"+session.getAttribute("created_year")+"/"+session.getAttribute("Ind_id")+"/Document"));						
								System.out.println("curr file "+targetFile.getAbsolutePath());							
								JIREncryptionUtils.encryptAllDocs(targetFile,"dec");
								*/																									
							// End File Decryption when Login 
							
                            
							     /* If you searched any profile before login it will take to that searche profile file */	
							
								if(!request.getParameter("searchedIndividualName").equals(""))
								{
									int searchedInd_id = Integer.parseInt(request.getParameter("searchedIndividualName"));
									session.setAttribute("beforeSearchedIndId", searchedInd_id);	
									
								    return new ModelAndView("redirect:/validateProfileViewingBeforeLogin.html");
								}									
								else
								{
									if((String)session.getAttribute("Ind_id")==null)
									{
										request.setAttribute("kycdocumentList", docservice.getKYCComp());
										request.setAttribute("errormessage","Your Session expired,please login to the application !");
																						    
										return new ModelAndView("individualPage");	
									}
									else
									{
										return new ModelAndView("individualProfile",model);
									}								
								}																																											
					} 		
					else
					{
						request.setAttribute("kycdocumentList", docservice.getKYCComp());
						
						request.setAttribute("errormessage",signupService.statusOfIndividualForLogin(individual));
						
						return new ModelAndView("individualPage");		
					}
	     }
	 
		
	@RequestMapping(value="/viewSearchedProfile",method=RequestMethod.GET)
    public ModelAndView viewSearchedProfile(HttpServletRequest request,HttpSession session)
    {	
						
		int ind_id = (int)session.getAttribute("asdgth");
		
		//int ind_id = Integer.parseInt(request.getParameter("asdgth"));
		
	   	session.setAttribute("sarchedIndId", ind_id);
				  	
	   	String kycId = profileinfoService.getkycid(ind_id);
   	    
	   	created_year = signupService.findyear(kycId);	
	   	
		session.setAttribute("created_year", created_year);
		
		String visibility = signupService.getVisibility(ind_id);
		session.setAttribute("visibility", visibility);
		
		request.setAttribute("searched_Indid", ind_id);
		
		IndSignupBean signupBean = new IndSignupBean();
		signupBean.setKycid(kycId);
		IND_KYCID = signupBean.getKycid();
		
		Summarydetail summary=new Summarydetail();
		summary.setInd_kyc_id(Integer.toString(ind_id));
		BasicDetail BasicDetail=new BasicDetail();
		BasicDetail.setInd_kyc_id(Integer.toString(ind_id));
		DocumentDetailModel docdetails = new DocumentDetailModel();
    	docdetails.setKyc_ind_id(ind_id);
    	
    	Map<String, Object> model = new HashMap<String, Object>();
    	    	    	
		model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(ind_id)));
		
		model.put("regname", prepareBean(signupService.getname(ind_id)));
		
		model.put("indName", prepareBean(signupService.getname(Integer.parseInt((String)session.getAttribute("Ind_id")))));
				
		if (!profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
		{				
	        model.put("documentdetail",profileinfoService.Getacademicdetail(docdetails));
	        model.put("postOn", profileinfoService.GetacademicPostDate(docdetails));
	  	}								
		if (!profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
		{			
		    model.put("employeedetail", prepareDocumentDetail(profileinfoService.Getemployeedetail(docdetails)));
		    model.put("empdate", profileinfoService.GetEmpPostDate(docdetails));				   
	  	}

		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(Integer.toString(ind_id)))) 
		{			
	     	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(Integer.toString(ind_id)));		
		    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
		}
					
		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));									
    	}
		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{						
			int su= Integer.parseInt(profileinfoService.Getregid(summary));			
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));			
		}
		
		String ind_kyc_id = kycId;
		IndSignupModel individual = new IndSignupModel();
		
		individual.setKycid(ind_kyc_id);
		String i=Integer.toString(ind_id);
		summary.setInd_kyc_id(i);
		
		Matrial matrial=new Matrial();
		matrial.setInd_kyc_id(i);
		
		BasicDetail.setInd_kyc_id(i);
	
		if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
		{			
		    int fam=Integer.parseInt(profileinfoService.Getfamid(i));			
	        model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
		}
		
		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
		{			
			  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
			  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
		}
		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{
			int su= Integer.parseInt(profileinfoService.Getregid(summary));
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));				
		}

		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
	    }
				
		if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
		{	
			String picName = profileinfoService.getLatestPicName(ind_id);
			session.setAttribute("profilePicName", picName);
			int pic_id = profileinfoService.getPrifilePicId(picName);				
			model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
		}
		
		String ownKycId=(String)session.getAttribute("Kyc_id");
		
		String firstname=profileinfoService.getfirstname(ownKycId);
		
		if(!kycId.equals(ownKycId))
		{
			String dbStatus = profileinfoService.getViewedStatus(kycId,ownKycId);
			if(!dbStatus.equals("No Data"))
			{
				int idprofileupdate = Integer.parseInt(dbStatus);
				
			    Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			    Profileupdatemodel.setIdprofileupdate(idprofileupdate);
				Profileupdatemodel.setProfileupdatedetail(firstname);
				Profileupdatemodel.setType_of_alert("visitor update");
				Profileupdatemodel.setKyc_id(kycId);
				Profileupdatemodel.setViewer_kyc_id(ownKycId);
				Profileupdatemodel.setMore_about_it(firstname+" has viewed your profile");
				Profileupdatemodel.setProfileupdatetime(new Date());
				profileinfoService.profileupdate(Profileupdatemodel); 
				
			}
			else
			{
				Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
				Profileupdatemodel.setProfileupdatedetail(firstname);
				Profileupdatemodel.setType_of_alert("visitor update");
				Profileupdatemodel.setKyc_id(kycId);
				Profileupdatemodel.setViewer_kyc_id(ownKycId);
				Profileupdatemodel.setMore_about_it(firstname+" has viewed your profile");
				Profileupdatemodel.setProfileupdatetime(new Date());
				profileinfoService.profileupdate(Profileupdatemodel); 
			}
		}
				
		String searchedIndId = profileinfoService.getSearchedAboutUsInfo(ind_id);
		request.setAttribute("searchedIndId", searchedIndId);
		
		String indRegVisibility = profileinfoService.getRegVisisbility(ind_id);
		String indBasicVisibility = profileinfoService.getBasicVisisbility(ind_id);
		String indFamilyVisibility = profileinfoService.getFamilyVisisbility(ind_id);
		String indSocialVisibility = profileinfoService.getSocialVisisbility(ind_id);
		
		request.setAttribute("regVisibility", indRegVisibility);
		request.setAttribute("basicVisibility", indBasicVisibility);
		request.setAttribute("familyVisibility", indFamilyVisibility);
		request.setAttribute("socialVisibility", indSocialVisibility);
				
		session.removeAttribute("beforeSearchedIndId");
		
   	    return new ModelAndView("searchProfileHome",model);
    }
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewprofileOther", method = RequestMethod.GET)
	@ResponseBody public byte[] previewprofileOther(HttpServletRequest request,HttpSession session) throws IOException {
							
		File ourPath = new File(kycMainDirectory);		
		String docCategory = request.getParameter("docCategory");		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/");		
		fPath = fPath.append(request.getParameter("year") + "/" + request.getParameter("indId"));				
		if(StringUtils.isNotBlank(docCategory))
		{			
			if(docCategory.equals("Profile_Photo"))
			{				
				fPath = fPath.append("/Profile_Photo/" + request.getParameter("fileName"));
			}
			
		}		
		try {						
				FileInputStream docdir = new FileInputStream(fPath.toString());												
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{	
					System.out.println("Resource not found !");
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewThumbprofileOther", method = RequestMethod.GET)
	@ResponseBody public byte[] previewThumbprofileOther(HttpServletRequest request,HttpSession session) throws IOException {
							
		File ourPath = new File(kycMainDirectory);		
		String docCategory = request.getParameter("docCategory");		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/");		
		fPath = fPath.append(request.getParameter("year") + "/" + request.getParameter("indId"));				
		if(StringUtils.isNotBlank(docCategory))
		{			
			if(docCategory.equals("Profile_Photo"))
			{				
				fPath = fPath.append("/ThumbNail_Documents/Profile_Photo/" + request.getParameter("fileName"));			
				
				/* Checks that files are exist or not  */
				
					File exeFile=new File(fPath.toString());
					if(!exeFile.exists())
					{
						fPath.delete(0, fPath.length());
						File targetFile = new File(request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY/image-not-found.png"));					
						fPath.append(targetFile.getPath());
					}	
					
				/* Checks that files are exist or not  */
					
			}			
		}		
		try {						
				FileInputStream docdir = new FileInputStream(fPath.toString());												
				if (docdir != null) 
				{												
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{	
					System.out.println("Resource not found !");					
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
						
				System.out.println(e.getMessage());
				return null; // todo: return safe photo instead
		
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	@RequestMapping(value="/ProfileViewingfromUtilizer",method=RequestMethod.GET)
    public ModelAndView viewSearchedProfilefromUtilizer(HttpServletRequest request,HttpSession session)
    {									
		int ind_id =(int)session.getAttribute("asdgthfmUti");
		
		int uti_Indid = (int) session.getAttribute("uti_Ind_id");	   				  	
	   	String kycId = profileinfoService.getkycid(ind_id);
   	    
	   	created_year = signupService.findyear(kycId);	
	   	
		session.setAttribute("created_year", created_year);
		
		String visibility = signupService.getVisibility(ind_id);
		session.setAttribute("visibility", visibility);
		
		request.setAttribute("searched_Indid", ind_id);
		
		IndSignupBean signupBean = new IndSignupBean();
		signupBean.setKycid(kycId);
		IND_KYCID = signupBean.getKycid();
		
		Summarydetail summary=new Summarydetail();
		summary.setInd_kyc_id(Integer.toString(ind_id));
		BasicDetail BasicDetail=new BasicDetail();
		BasicDetail.setInd_kyc_id(Integer.toString(ind_id));
		DocumentDetailModel docdetails = new DocumentDetailModel();
    	docdetails.setKyc_ind_id(ind_id);
    	
    	Map<String, Object> model = new HashMap<String, Object>();
    	    	    	
		model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(ind_id)));
		
		RegHistoryModel regModel = signupService.getRegistrationDetails(ind_id);
		
		model.put("regname", prepareBean(signupService.getname(ind_id)));
		
		request.setAttribute("utiFirstName", signupService.getUtilizerName(uti_Indid));
		
	   
		if (!profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
		{				
	        model.put("documentdetail",profileinfoService.Getacademicdetail(docdetails));
	        model.put("postOn", profileinfoService.GetacademicPostDate(docdetails));
	  	}								
		if (!profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
		{			
		    model.put("employeedetail", prepareDocumentDetail(profileinfoService.Getemployeedetail(docdetails)));
		    model.put("empdate", profileinfoService.GetEmpPostDate(docdetails));				   
	  	}

		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(Integer.toString(ind_id)))) 
		{			
	     	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(Integer.toString(ind_id)));		
		    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
		}
					
		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));									
    	}
		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{						
			int su= Integer.parseInt(profileinfoService.Getregid(summary));			
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));			
		}
		
		String ind_kyc_id = kycId;
		IndSignupModel individual = new IndSignupModel();
		
		individual.setKycid(ind_kyc_id);
		String i=Integer.toString(ind_id);
		summary.setInd_kyc_id(i);
		
		Matrial matrial=new Matrial();
		matrial.setInd_kyc_id(i);
		
		BasicDetail.setInd_kyc_id(i);
	
		if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
		{			
		    int fam=Integer.parseInt(profileinfoService.Getfamid(i));			
	        model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
		}
		
		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
		{			
			  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
			  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
		}
		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{
			int su= Integer.parseInt(profileinfoService.Getregid(summary));
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));				
		}

		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
	    }
				
		if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
		{	
			String picName = profileinfoService.getLatestPicName(ind_id);
			session.setAttribute("profilePicName", picName);
			int pic_id = profileinfoService.getPrifilePicId(picName);				
			model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
		}
			
		 // Tracking Activity Details
					        
		     String uti_kycid = (String)session.getAttribute("KycId");
		     int uti_indid = utiservice.findUtiid(uti_kycid);
	         UtilizerActivityModel actModel = new UtilizerActivityModel();
			 actModel.setUti_ind_id(uti_indid);
			 actModel.setActivity_type("Profile Viewing");
			 actModel.setActivity_details("Your have viewed "+regModel.getFirst_name()+" "+regModel.getMiddle_name()+" "+regModel.getLast_name()+" profile on "+new Date());
			 actModel.setActivity_date(new Date());
			         
			 utiservice.saveActivityDetails(actModel);

		 // End Of Tracking Activity Details
				
		String searchedIndId = profileinfoService.getSearchedAboutUsInfo(ind_id);
		request.setAttribute("searchedIndId", searchedIndId);
		
		String indRegVisibility = profileinfoService.getRegVisisbility(ind_id);
		String indBasicVisibility = profileinfoService.getBasicVisisbility(ind_id);
		String indFamilyVisibility = profileinfoService.getFamilyVisisbility(ind_id);
		String indSocialVisibility = profileinfoService.getSocialVisisbility(ind_id);
		
		request.setAttribute("regVisibility", indRegVisibility);
		request.setAttribute("basicVisibility", indBasicVisibility);
		request.setAttribute("familyVisibility", indFamilyVisibility);
		request.setAttribute("socialVisibility", indSocialVisibility);
		
		String individualKycId = profileinfoService.getkycid(ind_id);
		String utilizerKycId = (String)session.getAttribute("KycId");
		
		String accessStatus = profileinfoService.getUtilizerAccessForIndividual(individualKycId,utilizerKycId);
		String profileAccessStatus = "";
		String otherAccess = "";
		if(accessStatus.length()>1)
		{
			if(accessStatus.startsWith("PROFILE"))
			{
				String profileAccessArray[] = accessStatus.split("/");
				String profileAccessDetails[] = profileAccessArray[0].split(":");
				profileAccessStatus += profileAccessDetails[1];
				
				
			}			
		}
		
		request.setAttribute("profileAccessDetails", profileAccessStatus);
		request.setAttribute("otherAccess", accessStatus);
		
		if(accessStatus.length()>0)
		{
			request.setAttribute("sharedCond","accesable");
		}
		else
		{
			request.setAttribute("sharedCond","notaccesable");
		}
		
			
		int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
		int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
		
		request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
		request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
		
		
   	    return new ModelAndView("individualProfileFromUtilizer",model);
    }
	
	
	
	
	
	/*  Individual Profile view from Utilizer     */
	
	
	
	
	
	
	@RequestMapping(value="/viewIndAccessDetailsfromUti",method=RequestMethod.GET)
    public ModelAndView viewSearchedProfilefromUtilizerFromRecDocuments(HttpServletRequest request,HttpSession session)
    {									
		int ind_id =(int)session.getAttribute("tempIdasdgthfmUti");
		
		
		int uti_Indid = (int)session.getAttribute("uti_Ind_id");	   				  	
	   	String kycId = profileinfoService.getkycid(ind_id);
	   	
	   	String utilizerName = utiservice.getUtilizerFullName(uti_Indid);
	   			
	   	created_year = signupService.findyear(kycId);	
	   	
		session.setAttribute("created_year", created_year);
		
		String visibility = signupService.getVisibility(ind_id);
		session.setAttribute("visibility", visibility);
		
		request.setAttribute("searched_Indid", ind_id);
		
		IndSignupBean signupBean = new IndSignupBean();
		signupBean.setKycid(kycId);
		IND_KYCID = signupBean.getKycid();
		
		String indiKycId = signupBean.getKycid();
		
		Summarydetail summary=new Summarydetail();
		summary.setInd_kyc_id(Integer.toString(ind_id));
		BasicDetail BasicDetail=new BasicDetail();
		BasicDetail.setInd_kyc_id(Integer.toString(ind_id));
		DocumentDetailModel docdetails = new DocumentDetailModel();
    	docdetails.setKyc_ind_id(ind_id);
    	
    	Map<String, Object> model = new HashMap<String, Object>();
    	    	    	
		model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(ind_id)));
		
		RegHistoryModel regModel = signupService.getRegistrationDetails(ind_id);
		
		model.put("regname", prepareBean(signupService.getname(ind_id)));
		
		request.setAttribute("utiFirstName", signupService.getUtilizerName(uti_Indid));
			   
		if (!profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
		{				
	        model.put("documentdetail",profileinfoService.Getacademicdetail(docdetails));
	        model.put("postOn", profileinfoService.GetacademicPostDate(docdetails));
	  	}								
		if (!profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
		{			
		    model.put("employeedetail", prepareDocumentDetail(profileinfoService.Getemployeedetail(docdetails)));
		    model.put("empdate", profileinfoService.GetEmpPostDate(docdetails));				   
	  	}
		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(Integer.toString(ind_id)))) 
		{			
	     	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(Integer.toString(ind_id)));		
		    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
		}					
		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));									
    	}		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{						
			int su= Integer.parseInt(profileinfoService.Getregid(summary));			
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));			
		}
		
		String ind_kyc_id = kycId;
		IndSignupModel individual = new IndSignupModel();
		
		individual.setKycid(ind_kyc_id);
		String i=Integer.toString(ind_id);
		summary.setInd_kyc_id(i);
		
		Matrial matrial=new Matrial();
		matrial.setInd_kyc_id(i);
		
		BasicDetail.setInd_kyc_id(i);
	
		if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
		{			
		    int fam=Integer.parseInt(profileinfoService.Getfamid(i));			
	        model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
		}
		
		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
		{			
			  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
			  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
		}
		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{
			int su= Integer.parseInt(profileinfoService.Getregid(summary));
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));				
		}

		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
	    }
				
		if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
		{	
			String picName = profileinfoService.getLatestPicName(ind_id);
			session.setAttribute("profilePicName", picName);
			int pic_id = profileinfoService.getPrifilePicId(picName);				
			model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
		}
			
		 // Tracking Activity Details
					        
		     String uti_kycid = (String)session.getAttribute("KycId");
		     int uti_indid = utiservice.findUtiid(uti_kycid);
	        
		     UtilizerActivityModel actModel = new UtilizerActivityModel();
			 actModel.setUti_ind_id(uti_indid);
			 actModel.setActivity_type("Profile Viewing");
			 actModel.setActivity_details("Your have viewed "+regModel.getFirst_name()+" "+regModel.getMiddle_name()+" "+regModel.getLast_name()+" profile on "+new Date());
			 actModel.setActivity_date(new Date());
			         
			 String utiViewStatus = utiservice.saveActivityDetails(actModel);
			 
			 if(utiViewStatus.equals("success"))
			 {
				 // Send Email to individual that utilizer accepted your access details
				 
				 ResourceBundle resource = ResourceBundle.getBundle("resources/notifyIndividualFromUtilizer");
	     		    
			     String headingImage=resource.getString("headingImage");
		    	 String belowleftImage=resource.getString("belowleftImage");
		    	 String belowrightImage=resource.getString("belowrightImage");
		    	 String mailicon=resource.getString("mailicon");
		    	 String contacticon=resource.getString("contacticon");
		    	 
		    	 String emailHeading=resource.getString("emailHeading");
		    	 String contactNo=resource.getString("contactNo");
		    	 String contactEmail=resource.getString("contactEmail");
		    	 
		    	 String sharingsubject=resource.getString("sharingsubject");
		    	 String notifymessage=resource.getString("notifymessage");
				 String sharingmessage=resource.getString("sharingmessage");		
				 String aftermessageextraline = resource.getString("aftermessageextraline");
				 
		    	 String copyright=resource.getString("copyright");
		    	 String termscondition=resource.getString("termscondition");
		    	 String privacyPolicy=resource.getString("privacyPolicy");
		    	 
		    	 String facebookicon=resource.getString("facebookicon");
		    	 String twittericon=resource.getString("twittericon");
		    	 String googleicon=resource.getString("googleicon");
		    	 String linkedicon=resource.getString("linkedicon");
		    	 
		    	 String facebookLink=resource.getString("facebookLink");
		    	 String twitterLink=resource.getString("twitterLink");
		    	 String googlePlus=resource.getString("googlePlus");
		    	 String linkedIn=resource.getString("linkedIn");
		    	 
		    	 String clenseeLogo = resource.getString("clenseeLogo");
				 String sharingwelcome = resource.getString("sharingwelcome");
				 String clickhere = resource.getString("clickhere");
				 String actionUrl = resource.getString("actionUrl");
				 
		    	 
								String reciverKycId = indiKycId;
								int reciver_Ind_id = accservice.findid(reciverKycId);
								String reciverName = accservice.getReciverName(reciver_Ind_id);
								String reciverEmailId = accservice.getReciverEmailId(reciver_Ind_id);
								
						        StringBuilder text = new StringBuilder();
						        
					        
						        
							    /* Sending Emails to Register user */
							       
						        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
						        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
						       
						        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
						       
						        /* Header */
						        
						        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
						        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
						        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
						        text.append("</tr></table></td></tr></table>");
						       
						        /* End of Header */
						        
						        /* Header-2 */
						        
						        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
						        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
						        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
						        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
						        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
						        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
						        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
						        text.append("</p></td></tr></table></td></tr></table>");
						        
						        /* End of Header-2 */
						        
						        
						        /* BODY PART */
						        
						        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
						        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
						        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
						        text.append("<h3 style='color: #004879;'>Dear "+reciverName+",</h3>");
						        text.append("<p style='text-align: justify'>"+sharingwelcome+"</p>");
						        text.append("<p style='text-align: justify'>"+utilizerName+" "+notifymessage+"</p>");
						        text.append("<p style='text-align: justify'>"+sharingmessage+"</p>");
						       
						        text.append("<br><p>"+aftermessageextraline+"<br><br>");					        	
						        text.append("<a href="+actionUrl+" target='_blank' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
						        text.append("</p>");
						        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
						        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
						        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
						        text.append("</tr></table></td></tr>");
						        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
						        text.append("</table> ");
						        
						        
						        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
						        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
						        
						        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
						        
						        text.append("<tr><td>");
						        
						        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
						        text.append("</td></tr></table>");
						       
						        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
						        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
						        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
						        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
						        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
						        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
						        					        
						        text.append("</tr>");
						        text.append("</table></td></tr></table></td></tr></table>");
						        text.append("</body>");
						        text.append("</html>");
						       
						        /* END OF BODY PART */
						        
						        
						      /* End of Sending Emails to Register user */
						    
			       	         SendEmailUtil emailStatus = new SendEmailUtil();
			     	        
			       	        String subject = sharingsubject+" "+utilizerName;
			     			String resetStatus = "";
							try {
								resetStatus = emailStatus.getEmailSent(reciverEmailId,subject,text.toString());
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
			     	     
			     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
			     	    	
			     	    	if(resetStatus.equals("success"))
			     	    	{	    		
			     	    		emailReport.setEmail_id(reciverEmailId);
			     	    		emailReport.setSubject(subject);
			     	    		emailReport.setMessage_body(text.toString());
			     	    		emailReport.setSent_date(new Date());
			     	    		emailReport.setSent_report("0");
			     	    	}
			     	    	else
			     	    	{
			     	    		emailReport.setEmail_id(reciverEmailId);
			     	    		emailReport.setSubject(subject);
			     	    		emailReport.setMessage_body(text.toString());
			     	    		emailReport.setSent_date(new Date());
			     	    		emailReport.setSent_report("1");	    			    		
			     	    	}
			     	    	
			     	    	profileinfoService.setEmailStatusReport(emailReport);
			     	
				 // End of sending mail to individual that utilizer accepted your access details
			 }

		 // End Of Tracking Activity Details
				
		String searchedIndId = profileinfoService.getSearchedAboutUsInfo(ind_id);
		request.setAttribute("searchedIndId", searchedIndId);
		
		String indRegVisibility = profileinfoService.getRegVisisbility(ind_id);
		String indBasicVisibility = profileinfoService.getBasicVisisbility(ind_id);
		String indFamilyVisibility = profileinfoService.getFamilyVisisbility(ind_id);
		String indSocialVisibility = profileinfoService.getSocialVisisbility(ind_id);
		
		request.setAttribute("regVisibility", indRegVisibility);
		request.setAttribute("basicVisibility", indBasicVisibility);
		request.setAttribute("familyVisibility", indFamilyVisibility);
		request.setAttribute("socialVisibility", indSocialVisibility);
		
		String individualKycId = profileinfoService.getkycid(ind_id);
		
		String utilizerKycId = (String)session.getAttribute("KycId");
		
		String accessStatus = profileinfoService.getUtilizerAccessForIndividual(individualKycId,utilizerKycId);
		
		String profileAccessStatus = "";
		String otherAccess = "";
		
		if(accessStatus.length()>1)
		{
			if(accessStatus.startsWith("PROFILE"))
			{
				String profileAccessArray[] = accessStatus.split("/");
				String profileAccessDetails[] = profileAccessArray[0].split(":");
				profileAccessStatus += profileAccessDetails[1];				
			}			
		}
		
		
		request.setAttribute("profileAccessDetails", profileAccessStatus);
		request.setAttribute("otherAccess", accessStatus);
		
		if(accessStatus.length()>0)
		{
			request.setAttribute("sharedCond","accesable");
		}
		else
		{
			request.setAttribute("sharedCond","notaccesable");
		}
		
				
		int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
		int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
		
		request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
		request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
		
		
   	    return new ModelAndView("viewIndFromUtiOnReceivedDocuments",model);
    }
	
	
	
	@RequestMapping(value="/reloadIndAccessDetailsfromUti",method=RequestMethod.GET)
    public ModelAndView reloadIndAccessDetailsfromUti(HttpServletRequest request,HttpSession session)
    {									
		int ind_id =(int)session.getAttribute("tempIdasdgthfmUti");
		
		
		int uti_Indid = (int)session.getAttribute("uti_Ind_id");	   				  	
	   	String kycId = profileinfoService.getkycid(ind_id);
	   	
	   	created_year = signupService.findyear(kycId);	
	   	
		session.setAttribute("created_year", created_year);
		
		String visibility = signupService.getVisibility(ind_id);
		session.setAttribute("visibility", visibility);
		
		request.setAttribute("searched_Indid", ind_id);
		
		IndSignupBean signupBean = new IndSignupBean();
		signupBean.setKycid(kycId);
		IND_KYCID = signupBean.getKycid();
		
		Summarydetail summary=new Summarydetail();
		summary.setInd_kyc_id(Integer.toString(ind_id));
		BasicDetail BasicDetail=new BasicDetail();
		BasicDetail.setInd_kyc_id(Integer.toString(ind_id));
		DocumentDetailModel docdetails = new DocumentDetailModel();
    	docdetails.setKyc_ind_id(ind_id);
    	
    	Map<String, Object> model = new HashMap<String, Object>();
    	    	    	
		model.put("regHisname",prepareRegHistory(signupService.getRegistrationDetails(ind_id)));
		
		RegHistoryModel regModel = signupService.getRegistrationDetails(ind_id);
		
		model.put("regname", prepareBean(signupService.getname(ind_id)));
		
		request.setAttribute("utiFirstName", signupService.getUtilizerName(uti_Indid));
			   
		if (!profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
		{				
	        model.put("documentdetail",profileinfoService.Getacademicdetail(docdetails));
	        model.put("postOn", profileinfoService.GetacademicPostDate(docdetails));
	  	}								
		if (!profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
		{			
		    model.put("employeedetail", prepareDocumentDetail(profileinfoService.Getemployeedetail(docdetails)));
		    model.put("empdate", profileinfoService.GetEmpPostDate(docdetails));				   
	  	}
		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(Integer.toString(ind_id)))) 
		{			
	     	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(Integer.toString(ind_id)));		
		    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
		}					
		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));									
    	}		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{						
			int su= Integer.parseInt(profileinfoService.Getregid(summary));			
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));			
		}
		
		String ind_kyc_id = kycId;
		IndSignupModel individual = new IndSignupModel();
		
		individual.setKycid(ind_kyc_id);
		String i=Integer.toString(ind_id);
		summary.setInd_kyc_id(i);
		
		Matrial matrial=new Matrial();
		matrial.setInd_kyc_id(i);
		
		BasicDetail.setInd_kyc_id(i);
	
		if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
		{			
		    int fam=Integer.parseInt(profileinfoService.Getfamid(i));			
	        model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
		}
		
		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
		{			
			  int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));			
			  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  	
		}
		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{
			int su= Integer.parseInt(profileinfoService.Getregid(summary));
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));				
		}

		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));					
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));					
	    }
				
		if(!"no image".equals(profileinfoService.getLatestPicName(ind_id)))
		{	
			String picName = profileinfoService.getLatestPicName(ind_id);
			session.setAttribute("profilePicName", picName);
			int pic_id = profileinfoService.getPrifilePicId(picName);				
			model.put("profilePicInfo", prepareProfilePicBean(profileinfoService.getPrifilePicInfo(pic_id)));				
		}
			
		
		String searchedIndId = profileinfoService.getSearchedAboutUsInfo(ind_id);
		request.setAttribute("searchedIndId", searchedIndId);
		
		String indRegVisibility = profileinfoService.getRegVisisbility(ind_id);
		String indBasicVisibility = profileinfoService.getBasicVisisbility(ind_id);
		String indFamilyVisibility = profileinfoService.getFamilyVisisbility(ind_id);
		String indSocialVisibility = profileinfoService.getSocialVisisbility(ind_id);
		
		request.setAttribute("regVisibility", indRegVisibility);
		request.setAttribute("basicVisibility", indBasicVisibility);
		request.setAttribute("familyVisibility", indFamilyVisibility);
		request.setAttribute("socialVisibility", indSocialVisibility);
		
		String individualKycId = profileinfoService.getkycid(ind_id);
		String utilizerKycId = (String)session.getAttribute("KycId");
		
		String accessStatus = profileinfoService.getUtilizerAccessForIndividual(individualKycId,utilizerKycId);
		
		String profileAccessStatus = "";
		String otherAccess = "";
		
		if(accessStatus.length()>1)
		{
			if(accessStatus.startsWith("PROFILE"))
			{
				String profileAccessArray[] = accessStatus.split("/");
				String profileAccessDetails[] = profileAccessArray[0].split(":");
				profileAccessStatus += profileAccessDetails[1];				
			}			
		}
		
		
		request.setAttribute("profileAccessDetails", profileAccessStatus);
		request.setAttribute("otherAccess", accessStatus);
		
		if(accessStatus.length()>0)
		{
			request.setAttribute("sharedCond","accesable");
		}
		else
		{
			request.setAttribute("sharedCond","notaccesable");
		}
		
		
		String uti_kycid = (String)session.getAttribute("KycId");
		int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
		int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
		
		request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
		request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
		
   	    return new ModelAndView("viewIndFromUtiOnReceivedDocuments",model);
    }
	
	
	
	/*  End of individual Profile view from utilizer  */
	
	
	
	
	private ProfilePictureBean prepareProfilePicBean(ProfilePictureModel picModel)
	{
		ProfilePictureBean picBean = new ProfilePictureBean();
		
		picBean.setKyc_ind_id(picModel.getKyc_ind_id());
		picBean.setProfile_picture_name(picModel.getProfile_picture_name());
		picBean.setCr_date(picModel.getCr_date());	
		
		return picBean;
	}
	
	private FamilyDetailBean prepareEmployeeBean1(FamilyDetail familyDetail) {
		FamilyDetailBean bean = new FamilyDetailBean();
		bean.setFathername(familyDetail.getFather_name());
		bean.setInd_kyc_id(familyDetail.getInd_kyc_id());
		bean.setFatherPOI(familyDetail.getFather_POI());
		bean.setBrothername(familyDetail.getSibling_name());
		bean.setBrotherPOI(familyDetail.getSibling_POI());
		bean.setMothername(familyDetail.getMother_name());
		bean.setMotherPOI(familyDetail.getMother_POI());
		bean.setSpousename(familyDetail.getSpouse_name());
		bean.setSpousePOI(familyDetail.getSpouse_POI());
		bean.setMaritalstatus(familyDetail.getMaritalstatus());
		bean.setSpousegender(familyDetail.getSpousegender());
		bean.setHoroscopeInformation(familyDetail.getHoroscopeInformation());
		bean.setFoodpreferred(familyDetail.getFoodpreferred());
						
		bean.setFid(familyDetail.getFam_details_id());
		return bean;
	}
	
	
		private List<DocumentDetailsBean> prepareDocumentDetail(List<DocumentDetailModel> docdetails) {
     	
     	//DocumentDetailsBean  bean = new DocumentDetailsBean();
     	
     	List<DocumentDetailsBean> bean = null;
     	
		if (docdetails != null && !docdetails.isEmpty()) {
			bean = new ArrayList<DocumentDetailsBean>();
			DocumentDetailsBean beans = null;

			for (DocumentDetailModel docdetail : docdetails) {
             beans = new DocumentDetailsBean();
				
				beans.setDoc_name(docdetail.getDoc_name());
				beans.setDocs_data(docdetail.getDocs_data());
				
				bean.add(beans);
			}
		}
 		return bean;
 	} 
	 

   private List<DocumentDetailsBean> prepareDocumentDetail1(List<DocumentDetailModel> documentdetail) {
    	
    	List<DocumentDetailsBean> bean = null;
    	
	if (documentdetail != null && !documentdetail.isEmpty()) {
		
		bean = new ArrayList<DocumentDetailsBean>();
		DocumentDetailsBean beans = null;
		DocumentDetailModel indmodel=new DocumentDetailModel();
		
		List<DocumentDetailModel> docvalue1=documentdetail;
		
		Iterator it=docvalue1.iterator();
		
		while(it.hasNext()) 
		{
			beans = new DocumentDetailsBean();
			indmodel.setDoc_type((String) it.next());
			beans.setDoc_type(indmodel.getDoc_type());			
			bean.add(beans);
		}
		bean.add(beans);
		
	}
		return bean;

	} 
	

	private SocialaccBean prepareSocialaccBean(Socialacc socialacc) {
		SocialaccBean bean = new SocialaccBean();

		bean.setSocialwebsitelink(socialacc.getWebsite_link());
		bean.setSocialwebsitename(socialacc.getWebsite_name());
		bean.setSociallinkedlnlink(socialacc.getLinkedln_link());
		bean.setSociallinkedlnname(socialacc.getLinkedln_name());
		bean.setSocialtwitterlink(socialacc.getTwitter_link());
		bean.setSocialtwittername(socialacc.getTwitter_name());
		
		bean.setInd_kyc_id(socialacc.getInd_kyc_id());
		return bean;
	}
	private BasicDetailBean prepareBasicBean(BasicDetail BasicDetail)
	{
		BasicDetailBean bean=new BasicDetailBean();
		bean.setHobbies(BasicDetail.getHobbies());
		bean.setDOB(BasicDetail.getDate_of_birth());
		bean.setEmailalternative(BasicDetail.getAlternate_email());
		bean.setLanguages(BasicDetail.getLanguages_known());
		bean.setMatrialstatus(BasicDetail.getMarital_status());
		bean.setNationality(BasicDetail.getNationality());
		bean.setPassportsize(BasicDetail.getProfile_pic());
		bean.setPermanent_address(BasicDetail.getPermanent_address());
		bean.setPresent_address(BasicDetail.getPresent_address());
		bean.setTeloff(BasicDetail.getTell_office());
		bean.setTelres(BasicDetail.getTell_residence());
		return bean;
	}
     private IndSignupBean prepareBean(IndSignupModel IndSignup) {
		
		IndSignupBean bean=new IndSignupBean(); 
		bean.setFirstname(IndSignup.getFirstname());
		bean.setLastname(IndSignup.getLastname());
		bean.setMiddlename(IndSignup.getMiddlename());
		bean.setMobileno(IndSignup.getMobileno());
		bean.setEmailid(IndSignup.getEmailid());
		bean.setCr_date(IndSignup.getCr_date());
		return bean;
		
	}
     
	
     private RegHistoryBean prepareRegHistory(RegHistoryModel regModel)
     {
    	 RegHistoryBean bean = new RegHistoryBean();
    	 bean.setFirst_name(regModel.getFirst_name());
    	 bean.setMiddle_name(regModel.getMiddle_name());
    	 bean.setLast_name(regModel.getLast_name());
    	 bean.setGender(regModel.getGender());
    	 bean.setEmail_id(regModel.getEmail_id());
    	 bean.setMobile_no(regModel.getMobile_no());
    	 bean.setCr_date(regModel.getCr_date());
    	 
    	 return bean;
     }
          
    private SummarydetailBean prepareSummaryBean(Summarydetail summarydetail) {
	SummarydetailBean bean = new SummarydetailBean();
	
	bean.setLandpagedescription(summarydetail.getLandpagedescription());
	bean.setCr_date(summarydetail.getCr_date());
	return bean;

}

	//saving the New Individual User signUP details
   	
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/saveIndSignUp", method = RequestMethod.POST)
	public @ResponseBody String saveIndSignUpDetails(HttpServletRequest request,HttpSession session) throws Exception {

		String resetStatus = "";
		
			IndSignupBean signupBean = new IndSignupBean();
			signupBean.setFirstname(request.getParameter("firstname"));
			signupBean.setMiddlename(request.getParameter("middlename"));
			signupBean.setLastname(request.getParameter("lastname"));
			signupBean.setGender(request.getParameter("gender"));
			signupBean.setMobileno(request.getParameter("mobileno"));
			signupBean.setEmailid(request.getParameter("emailid"));
		
			String idType = request.getParameter("typeofId");
			String idValue = request.getParameter("kycid").toUpperCase();
		
			String fulleName = "";
			
			if(!request.getParameter("firstname").equals(""))
			{
				fulleName += request.getParameter("firstname") + " ";
			}
			
			if(!request.getParameter("middlename").equals(""))
			{
				fulleName += request.getParameter("middlename") + " ";
			}
			
			if(!request.getParameter("lastname").equals(""))
			{
				fulleName += request.getParameter("lastname");
			}
			
			signupBean.setIndividual_full_name(fulleName); 
			
			signupBean.setKycid(request.getParameter("kycid").toUpperCase());
			
			// Start code for Encrypt Password
			
			  String indPassword = request.getParameter("password");
			
			  signupBean.setPassword(PasswordEncryptionDecryption.getEncryptPassword(indPassword));	
			
			  
			// EOC for Encrypt Password 
									
			signupBean.setStatus("0");
			signupBean.setPlan_available("Basic");
			int lastId = signupService.getLastIndId();
			
			int randomNo1 = lastId++;
			long randomNo2 = randomNo1 * 11;
			randomNo2 = randomNo2 / 2;
			int randomNo4 = randomNo1 / 2;
			String randomNo3 = randomNo2 + "234dse"+lastId+"ytnkdn";
			
			signupBean.setVisibility(randomNo4+randomNo3+"abj1t8gjf"+randomNo2+"gk85zrkv"+randomNo1);
			
			signupBean.setCr_date(new Date());
		
			IndSignupModel signupModel = prepareSignupModel(signupBean);

			
			// Validate Captche Code given by Individual 
			
			String answer = request.getParameter("answer");
													
			if(docservice.getKycRegistrationStatus(idValue).equals("Yes"))
			{
				return "Document id already exists.";
			}
			else
			{
				if(profileinfoService.getValidateMobileNo(request.getParameter("mobileno")).equals("Yes"))
				{
					return "Mobile number already exists.";
				}
				else
				{
					if(profileinfoService.getValidateEmailId(request.getParameter("emailid")).equals("Yes"))
					{
						return "Email id already exists.";
					}
					else						
					{
						Captcha captcha = (Captcha)session.getAttribute(Captcha.NAME);
						request.setCharacterEncoding("UTF-8");						
						if (captcha.isCorrect(answer)) 
						{
							if ("success".equals(signupService.addIndividual(signupModel))) 			
							{
								
											IndSignupModel individual = new IndSignupModel();
											individual.setKycid(signupBean.getKycid());			
											signupService.getInd_id(individual);
																													
											int ind_id = Integer.parseInt(signupService.getInd_id(individual));
								            RegHistoryModel regmodel = new RegHistoryModel();
											
											regmodel.setInd_id(ind_id);			
											regmodel.setFirst_name(signupBean.getFirstname());
											regmodel.setMiddle_name(signupBean.getMiddlename());
											regmodel.setLast_name(signupBean.getLastname());
											regmodel.setGender(signupBean.getGender());
											regmodel.setMobile_no(Long.parseLong(signupBean.getMobileno()));
											regmodel.setEmail_id(signupBean.getEmailid());
											regmodel.setCr_date(new Date());
											
											signupService.saveRegHistory(regmodel);
											
											VisibilityModel  visibility = new VisibilityModel();
											
											visibility.setKyc_ind_id(ind_id);
											visibility.setRegistration_visibility("11");
											visibility.setBasic_visibility("1111111111");
											visibility.setFamily_visibility("111111");
											visibility.setSocial_account_visibility("111");
											
											signupService.saveVisibilitySetting(visibility);
												
											DocumentDetailModel docdetailmodel = new DocumentDetailModel();
											
											
											docdetailmodel.setDoc_size(0);
											docdetailmodel.setDoc_type("KYC_DOCUMENT");
											docdetailmodel.setKyc_ind_id(ind_id);
											docdetailmodel.setDocs_data(idType+","+idType+","+idValue+","+"Captured in Registration"+","+new Date());
											docdetailmodel.setCr_date(new Date());
											docdetailmodel.setPanel_status(idType);      
											docdetailmodel.setDoc_des("Intial Registration");
									        			
											session.setAttribute("id", docdetailmodel.getDoc_id());
											
											String status ="";
											
											if ("success".equals(docservice.insertKYCDocDetailsInRegn(docdetailmodel)))
												
												{											        
																	String Kyc_id= (String) session.getAttribute("Kyc_id");
																	Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
																	Profileupdatemodel.setProfileupdatedetail("uploaded the kycdoc");
																	Profileupdatemodel.setProfileupdatetime(new Date());
																	Profileupdatemodel.setType_of_alert("doc update");
																	Profileupdatemodel.setMore_about_it(request.getParameter("path")+" uploaded");
																    Profileupdatemodel.setKyc_id(Kyc_id);
																	profileinfoService.profileupdate(Profileupdatemodel);
																	
																	IndividualPlanDetailModel indPlanModel = new IndividualPlanDetailModel();
																	indPlanModel.setInd_id(ind_id);
																	indPlanModel.setPlan_name("Basic");
																	indPlanModel.setCr_date(new Date());
																	
																	profileinfoService.getEntryInPlanDetails(indPlanModel);
																	
																	ResourceBundle resource = ResourceBundle.getBundle("resources/signUpSendingMail");
																	
																	
																	String headingImage=resource.getString("headingImage");
															    	 String belowleftImage=resource.getString("belowleftImage");
															    	 String belowrightImage=resource.getString("belowrightImage");
															    	 String mailicon=resource.getString("mailicon");
															    	 String contacticon=resource.getString("contacticon");
															    	 
															    	 String emailHeading=resource.getString("emailHeading");
															    	 String contactNo=resource.getString("contactNo");
															    	 String contactEmail=resource.getString("contactEmail");
															    	 
															    	 String subject=resource.getString("signupsubject");
																	 String messageBody=resource.getString("signupmessage");		
																	 String urlLinktoActivate = resource.getString("aftermessageextraline");
																	 
															    	 String copyright=resource.getString("copyright");
															    	 String termscondition=resource.getString("termscondition");
															    	 String privacyPolicy=resource.getString("privacyPolicy");
															    	 
															    	 String facebookicon=resource.getString("facebookicon");
															    	 String twittericon=resource.getString("twittericon");
															    	 String googleicon=resource.getString("googleicon");
															    	 String linkedicon=resource.getString("linkedicon");
															    	 
															    	 String facebookLink=resource.getString("facebookLink");
															    	 String twitterLink=resource.getString("twitterLink");
															    	 String googlePlus=resource.getString("googlePlus");
															    	 String linkedIn=resource.getString("linkedIn");
															    	 
															    	 String clenseeLogo = resource.getString("clenseeLogo");
																	 String signupwelcome = resource.getString("signupwelcome");
																	 String clickhere = resource.getString("clickhere");
																	 
																    String commonUrl = resource.getString("commonUrl");
																   
																    String url = ""+commonUrl+"activation_account.html?asfgkfyhr="+signupBean.getVisibility()+""; // This is the URL which individual click to activate the account
															        String target = "_blank";
															        
															        StringBuilder text = new StringBuilder();
																						        									        
															        /* Sending Emails to Register user */
															       
															        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
															        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
															       
															        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
															       
															        /* Header */
															        
															        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
															        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
															        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
															        text.append("</tr></table></td></tr></table>");
															       
															        /* End of Header */
															        
															        /* Header-2 */
															        
															        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
															        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
															        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
															        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
															        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
															        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
															        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
															        text.append("</p></td></tr></table></td></tr></table>");
															        
															        /* End of Header-2 */
															        
															        
															        /* BODY PART */
															        
															        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
															        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
															        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
															        text.append("<h3 style='color: #004879;'>Dear "+signupBean.getFirstname()+" "+signupBean.getMiddlename()+ " "+signupBean.getLastname()+",</h3>");
															        text.append("<p style='text-align: justify'>"+signupwelcome+"</p>");
															        text.append("<p style='text-align: justify'>"+messageBody+"</p><p>");
															        text.append("<br>"+urlLinktoActivate+"<br><br>");
												        	        text.append("<a href="+url+" target="+target+" style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
												        	        text.append("</p>");
															        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
															        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");															        
															        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
															        text.append("</tr></table></td></tr>");
															        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
															        text.append("</table> ");
															        
															        															        
															        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
															        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
															        
															        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
															        
															        text.append("<tr><td>");
															        
															        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
															        text.append("</td></tr></table>");
															        					
															        
															        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
															        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
															        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
															        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
															        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
															        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
															        
															        
															        text.append("</tr>");
															        text.append("</table></td></tr></table></td></tr></table>");
															        text.append("</body>");
															        text.append("</html>");
															       
															        /* END OF BODY PART */
															        
															        
															        /* End of Sending Emails to Register user */
																																		        	        												        	       												       	            
												       	        SendEmailUtil obj = new SendEmailUtil();
												     	        
												     			resetStatus = obj.getEmailSent(signupBean.getEmailid(),subject,text.toString());
																request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
												     	     
												     	    	System.out.println("status="+resetStatus);
												     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
												     	    	
												     	    	if(resetStatus.equals("success"))
												     	    	{	    		
												     	    		emailReport.setEmail_id(signupBean.getEmailid());
												     	    		emailReport.setSubject(subject);
												     	    		emailReport.setMessage_body(text.toString());
												     	    		emailReport.setSent_date(new Date());
												     	    		emailReport.setSent_report("0");
												     	    	}
												     	    	else
												     	    	{
												     	    		emailReport.setEmail_id(signupBean.getEmailid());
												     	    		emailReport.setSubject(subject);
												     	    		emailReport.setMessage_body(text.toString());
												     	    		emailReport.setSent_date(new Date());
												     	    		emailReport.setSent_report("1");	    			    		
												     	    	}
												     	    	
												     	    	profileinfoService.setEmailStatusReport(emailReport);
												     	    	
												     	    	request.setAttribute("kycdocumentList", docservice.getKYCComp());
																 
																return "success";     // If mail id sent properly then it will go to login page to login		
													
												}
											else
											{
												return "Failed Due to Some Server Problem Please Try Again Later !";      // if something problem in Registration or mail sending problem it will hit this page.
											}
										   
										} 
										else
										{
											return "Failed Due to Some Server Problem Please Try Again Later !";      // if something problem in Registration or mail sending problem it will hit this page.
										}							
						}
						else
						{
							return "Please enter proper captcha code.";
						}
					}
				}
			}							        	
	}
	
	@RequestMapping(value = "/activation_account", method = RequestMethod.GET)
	public String getactivateAccount(HttpServletRequest request) {
		
		String kycId = request.getParameter("asfgkfyhr");		
		String checkStatus = signupService.checkStatus(kycId);
		
		if(checkStatus.equals("0"))
		{
			String status = signupService.getActiveStatus(kycId);		
			if(status.equals("successfully activated"))
			{
				request.setAttribute("status", "Your account is verified and activated successfully.");
				request.setAttribute("kycdocumentList", docservice.getKYCComp());
				
				String codeStatus = signupService.getUpdateAppliedCodeStatus(kycId);
						
				return "individualPage";
			}
			else if(status.equals("failed to activate"))
			{
				return "errorLogin";
			}
			else if(status.equals("Expired"))
			{
				request.setAttribute("status", "Your activation link has expired.");
				request.setAttribute("kycdocumentList", docservice.getKYCComp());
				
				return "individualPage";
			}
			else
			{
				request.setAttribute("status", "Your account is blocked by clensee admin.");
				request.setAttribute("kycdocumentList", docservice.getKYCComp());
				
				
				return "individualPage";
			}
		}		
		else
		{
			String activationLinkStatus = signupService.getActiveValidityStatus(kycId);	
			if(activationLinkStatus.equals("failed to activate"))
			{
				request.setAttribute("status", "Your account is already verified and activated Successfully.");
				request.setAttribute("kycdocumentList", docservice.getKYCComp());
				
				return "individualPage";
			}
			else if(activationLinkStatus.equals("Expired"))
			{
				request.setAttribute("status", "Your Activation Link has expired for further process contact to KYC support team !");
				request.setAttribute("kycdocumentList", docservice.getKYCComp());
				
				return "individualPage";
			}
			else
			{
				request.setAttribute("status", "Your Account is Blocked by KYC Admin ,for further process contact to support team !");
				request.setAttribute("kycdocumentList", docservice.getKYCComp());
				
				return "individualPage";
			}			
		}
				
	}
	
	private IndSignupModel prepareSignupModel(IndSignupBean indBean) {
		
		IndSignupModel signup = new IndSignupModel();
		signup.setFirstname(indBean.getFirstname());
		signup.setMiddlename(indBean.getMiddlename());
		signup.setLastname(indBean.getLastname());
		signup.setGender(indBean.getGender());
		signup.setEmailid(indBean.getEmailid());
		signup.setMobileno(indBean.getMobileno());
		signup.setPassword(indBean.getPassword());
		signup.setCr_date(indBean.getCr_date());
		signup.setKycid(indBean.getKycid());
		signup.setVisibility(indBean.getVisibility());
		//indBean.setIndid(null);
		signup.setStatus("0");
		signup.setAdmin_control("1");
		signup.setPlan_available("Basic");
		signup.setIndividual_full_name(indBean.getIndividual_full_name());
		
		return signup;
	}

	// Checking Experiments

	@RequestMapping(value = "/help", method = RequestMethod.GET)
	public ModelAndView getHelp(
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		
		return new ModelAndView("helpPage", model);
	}
	
	@RequestMapping(value = "/history_details", method = RequestMethod.GET)
	public ModelAndView gethistory(
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		
		return new ModelAndView("history_details", model);
	}
	
	@RequestMapping(value = "/emp_history_details", method = RequestMethod.GET)
	public ModelAndView getEmphistory(
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		
		return new ModelAndView("emp_history_details", model);
	}
	
	@RequestMapping(value = "/ViewMore", method = RequestMethod.GET)
	public ModelAndView getViewMore(
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		
		return new ModelAndView("ViewMore", model);
	}
	
			
	@RequestMapping(value = "/center_body5", method = RequestMethod.GET)
	public ModelAndView getbody5(
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		
		return new ModelAndView("center_body5", model);
	}
		
	@RequestMapping(value = "/center_body", method = RequestMethod.GET)
	public ModelAndView getbody(
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		
		return new ModelAndView("center_body", model);
	}

	
		
	@RequestMapping(value = "/viewKycDoc", method = RequestMethod.GET)
	public  ModelAndView getview(HttpServletRequest request,HttpSession session) {
		
		
		String individualInd_id = (String)session.getAttribute("Ind_id");								
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn((String)session.getAttribute("created_year"),individualInd_id,"KYC_DOCUMENT",request.getParameter("fileName"),request);
				 
		return new ModelAndView("view");
	}
	
	@RequestMapping(value = "/viewfinancial", method = RequestMethod.GET)
	public ModelAndView getfinancialview(HttpServletRequest request,HttpSession session) {
		 
		
					String docName = request.getParameter("fileName");
					
					request.setAttribute("docName", docName);
					
					int Individual_Id =Integer.parseInt((String)session.getAttribute("Ind_id"));
					
					request.setAttribute("Individual_Id", Individual_Id);
					
					String individualInd_id = (String)session.getAttribute("Ind_id");								
					FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn((String)session.getAttribute("created_year"),individualInd_id,"Financial_DOC",request.getParameter("fileName"),request);					
					
					return new ModelAndView("viewfinancial");
			
	}
	
	
	@RequestMapping(value = "/downloadfinancial", method = RequestMethod.GET)
	public ModelAndView getfinancialdownload(HttpServletRequest request,HttpSession session) {
		
       String docName = request.getParameter("fileName");
		
		request.setAttribute("docName", docName);
		
        int Individual_Id =Integer.parseInt((String)session.getAttribute("Ind_id"));
		
		request.setAttribute("Individual_Id", Individual_Id);
		
		String individualInd_id = (String)session.getAttribute("Ind_id");								
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn((String)session.getAttribute("created_year"),individualInd_id,"Financial_DOC",request.getParameter("fileName"),request);					
		
		
		return new ModelAndView("downloadfinancial");
	}
	
	@RequestMapping(value = "/viewAcademic", method = RequestMethod.GET)
	public ModelAndView getviewAcademic(HttpServletRequest request,HttpSession session) {
		
		 
		String individualInd_id = (String)session.getAttribute("Ind_id");								
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn((String)session.getAttribute("created_year"),individualInd_id,"ACADEMIC_DOC",request.getParameter("fileName"),request);

	    return new ModelAndView("viewAcademic");
			
	}
			
	@RequestMapping(value = "/downloadAcademic", method = RequestMethod.GET)
	public ModelAndView getdownloadAcademic(HttpServletRequest request,HttpSession session) {
		
		String individualInd_id = (String)session.getAttribute("Ind_id");								
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn((String)session.getAttribute("created_year"),individualInd_id,"ACADEMIC_DOC",request.getParameter("fileName"),request);
		
		 return new ModelAndView("downloadAcademic");
	}
	
	@RequestMapping(value = "/uploading", method = RequestMethod.GET)
	public ModelAndView getchange() {
		
		return new ModelAndView("uploading");
	}
	
	@RequestMapping(value = "/uploadAadhar", method = RequestMethod.GET)
	public ModelAndView getChangeAadhar() {
		
		return new ModelAndView("uploadAadhar");
	}
	
	@RequestMapping(value = "/viewAadhar", method = RequestMethod.GET)
	public ModelAndView getviewAadhar() {
		 return new ModelAndView("viewAadhar");
	}
	
		
	@RequestMapping(value = "/choose_category_ID", method = RequestMethod.POST)
	public ModelAndView ProceedChooseIDs(HttpServletRequest request,HttpSession session) {
		
		
		String allAccordian ="";
		
		int id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		
		DocumentDetailModel docdetails = new DocumentDetailModel();
		docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
		docdetails.setDoc_type("kyc");
		String click[] = request.getParameterValues("click");
	    
		String accordname="";	
		
		List list =  Arrays.asList(click); 
		
		Iterator<String> itr=list.iterator();
		
		while(itr.hasNext())
		{			
			accordname = accordname.concat(itr.next()+",");
		}
				
		allAccordian = accordname.substring(0, accordname.length() - 1);
				
		String oldPannel = docservice.getOldComp(Integer.parseInt((String)session.getAttribute("Ind_id")));
		
		if(oldPannel.length()>0)
		{
			allAccordian = oldPannel+","+accordname;
			allAccordian = allAccordian.substring(0,allAccordian.length()-1);
		}
	
		DocumentDetailModel docmodel = new DocumentDetailModel();
		
		String docsData = docservice.getKYCLatestDocsData(id);
		
		String docsDataArray[] = docsData.split(",");
		
		docmodel.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
		docmodel.setCr_by(docservice.getIndName(id));
		docmodel.setDoc_name(docservice.getKYCLatestDocName(id));
		docmodel.setDoc_type("KYC_DOCUMENT");
		docmodel.setDocs_data(docsData);
		docmodel.setCr_date(new Date());
		
		docmodel.setDoc_des(docsDataArray[2]);
		
		docmodel.setPanel_status(allAccordian.replaceAll(",,",","));
		
		if("success".equals(docservice.setKYCPannelDocDetails(docmodel)));		
		{
			System.out.println("Saved");
		}
		
		session.setAttribute("kyc_panel_status",allAccordian);
		
		Map<String, String> model = new HashMap<String, String>();
		
		model.put("accordNames", allAccordian);
		
		docdetails.setDoc_des(allAccordian);
		String kycdatas = docservice.getKYCdatas(docdetails);
						
		model.put("kycdatas", kycdatas);		
		
				
		//Code for Image is there or not
		String Indid = (String) session.getAttribute("Ind_id"); 
		int year = docservice.findYear(Indid);		 
		request.setAttribute("year",year);
		request.setAttribute("user",Indid);
		//End of Code for image is there or not
		
		
		//Code for reselect Category
	    String component = docservice.getKYCComp();
		request.setAttribute("comp", component);
		int indId = Integer.parseInt(Indid);
		String old_selComp = docservice.getOldComp(indId);
		request.setAttribute("OldComp",old_selComp);
	    //End of code for Reselect Category
		
		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		
		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
				
	    // On File Scheduler
		String individualInd_id = (String)session.getAttribute("Ind_id");								
		FileEncryptionDecryptionUtil.getOwnFileDecryption(individualInd_id,(String)session.getAttribute("created_year"),request);
		//End Of File Scheduler
		
		String pannelWithId = docservice.getPannelWithId(ind_id,old_selComp);
							
		request.setAttribute("pannelWithId",pannelWithId);	
		
		
	    return new ModelAndView("individualDocuments",model);
	    
	}
	
	 private DocumentDetailModel prepareDocModel(DocumentDetailsBean docBean) {
			
		    DocumentDetailModel docmodel = new DocumentDetailModel();
				    
		    docmodel.setKyc_ind_id(docBean.getKyc_ind_id());
		    docmodel.setCr_by(docBean.getCr_by());
		    docmodel.setCr_date(docBean.getCr_date());
		    docmodel.setDoc_type(docBean.getDoc_type());
		    docmodel.setPanel_status(docBean.getPanel_status());
		    
			return docmodel;
			
		}
	
	
	 @RequestMapping(value = "/kychistory", method = RequestMethod.POST)
	 public ModelAndView getKYChistoryDetails(HttpServletRequest request,HttpSession session) {
			
			       								
						int id = Integer.parseInt((String) session.getAttribute("Ind_id"));
						 
				    	DocumentDetailModel docdetails = new DocumentDetailModel();
				    	docdetails.setKyc_ind_id(id);
						
				    	String docName = request.getParameter("fileName");
				    	request.setAttribute("docName", docName);
				    	
						docdetails.setDoc_name((String)request.getParameter("fileName"));
						
						Map<String, Object> model = new HashMap<String, Object>();
						model.put("kyc_history", prepareDocumentDetailBean(docservice.getHistory(docdetails,session)));
						
						int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
						request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
				
						return new ModelAndView("ind_kyc_history",model);
					
		}
	
	 
	
	 @SuppressWarnings("unchecked")
	@RequestMapping(value = "/kycviewmore", method = RequestMethod.POST)
 	public ModelAndView GetKycViewMore(HttpServletRequest request,HttpSession session) {
 		       	
	DocumentDetailModel docdetails = new DocumentDetailModel();
        	
        	docdetails.setDoc_name((String)request.getParameter("id"));
        	
        	ArrayList<Integer> doclist = (ArrayList<Integer>)session.getAttribute("docidlist");
        	
        	
        	String typeUrl=request.getParameter("typeUrl");
        	
        	if(typeUrl.equals("dtlsIterator")) {

            	docdetails.setDoc_id(doclist.get(Integer.parseInt((String)request.getParameter("idIndex"))));	
        	} else {

            	docdetails.setDoc_id(Integer.parseInt((String)request.getParameter("idIndex")));
        	}
        	
    		Map<String, String> model = new HashMap<String, String>();
    		
    		model.put("viewdetails", docservice.getViewMore(docdetails));
    		
    	    request.setAttribute("docname", docservice.getDocName(docdetails));
    	    
    	   // session.setAttribute("user", Integer.parseInt((String)session.getAttribute("Ind_id")));
    	    
    	 String Indid = (String) session.getAttribute("Ind_id");
   		 
   		 int year = docservice.findYear(Indid);
   		 
   		 request.setAttribute("year",year);
   		 request.setAttribute("user",Indid);
    	        
 		return new ModelAndView("kyc_viewmore",model);
 	}
	
	 @RequestMapping(value="/financialDetails", method = RequestMethod.POST)
     public ModelAndView getFinancialDetail(HttpServletRequest request,HttpSession session)
     {   
     	String docName = request.getParameter("docName");
     	int i =Integer.parseInt((String) session.getAttribute("Ind_id"));
     	
     	String docsData = docservice.getFinancialDataviaDocName(i,docName);
     	
     	request.setAttribute("docsData",docsData);
     	request.setAttribute("indId",i);
     	
     	String financial= docservice.getFinancialComp();
     	
     	request.setAttribute("finHierarchy", financial);
     	
     	return new ModelAndView("historydetailfinancial");
     }
	 
	@RequestMapping(value = "/historydetailfinancial", method = RequestMethod.GET)
	public ModelAndView getFinDetails(
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
		
		Map<String, Object> model = new HashMap<String, Object>();
		Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
		Profileupdatemodel.setProfileupdatedetail("viewed the doc");
		Profileupdatemodel.setProfileupdatetime(new Date());
		Profileupdatemodel.setType_of_alert("doc update");
		//Profileupdatemodel.setKyc_id(kycid);
		profileinfoService.profileupdate(Profileupdatemodel); 
		return new ModelAndView("historydetailfinancial", model);
	}
	
	@RequestMapping(value = "/myDocumentsMain", method = RequestMethod.GET)
	public ModelAndView getMyDocumentsMain(HttpServletRequest request,HttpSession session) {
		
       session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null)
		{			
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return new ModelAndView("individualPage");	
		}
		else if(((String)session.getAttribute("Ind_id")).equals(null))
		{
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return new ModelAndView("individualPage");	
		}
		else
		{	
			Map<String, Object> model = new HashMap<String, Object>();
			
			DocumentDetailModel docdetails = new DocumentDetailModel();
			docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
			docdetails.setDoc_type("kyc");
			
			if(!docservice.getstatus(docdetails).equals("Not There"))
			{
				String accordname=docservice.getstatus(docdetails);		
				
				Map<String, String> model1 = new HashMap<String, String>();			
				model1.put("accordNames", accordname);			
				session.setAttribute("kyc_panel_status", accordname);
				
				docdetails.setDoc_des(accordname);
				String kycdatas = docservice.getKYCdatas(docdetails);
				
				//kycdatas = kycdatas.substring(0, kycdatas.length()-1);
				
				model1.put("kycdatas", kycdatas);		
				
				String Indid = (String) session.getAttribute("Ind_id");
				 
				int year = docservice.findYear(Indid);
				 
				 request.setAttribute("year",year);
				 request.setAttribute("user",Indid);
				
				
				   //Code for reselect Category
				    
				    String component = docservice.getKYCComp();
					
					request.setAttribute("comp", component);
					
					int indId = Integer.parseInt(Indid);
					String old_selComp = docservice.getOldComp(indId);
					
					request.setAttribute("OldComp",old_selComp);
				
					//End of code for Reselect Category
					
					
					/*// On File Scheduler 
					
					String individualInd_id = (String)session.getAttribute("Ind_id");								
					FileEncryptionDecryptionUtil.getOwnFileDecryption(individualInd_id,(String)session.getAttribute("created_year"),request);
					
					// End Of File Scheduler
                    */					
					
					
					int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));				
					request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));						
									
					String pannelWithId = docservice.getPannelWithId(ind_id,old_selComp);
					request.setAttribute("pannelWithId",pannelWithId);						
					
				    return new ModelAndView("individualDocuments",model1);				
			}			
			else
			{
				String component = docservice.getKYCComp();		
				request.setAttribute("comp", component);		
											
				return new ModelAndView("choose_IDs", model); 
			}
		}						
	}
	
	
	@RequestMapping(value = "/choose_KYC_Documents", method = RequestMethod.GET)
	public ModelAndView ChooseKYCDocument(HttpServletRequest request,HttpSession session) {
		
				
		Map<String, Object> model = new HashMap<String, Object>();
		
		DocumentDetailModel docdetails = new DocumentDetailModel();
		docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
		docdetails.setDoc_type("kyc");
		
		if(!docservice.getstatus(docdetails).equals("Not There"))
		{
			String accordname=docservice.getstatus(docdetails);		
			
			Map<String, String> model1 = new HashMap<String, String>();			
			model1.put("accordNames", accordname);			
			session.setAttribute("kyc_panel_status", accordname);
			
			docdetails.setDoc_des(accordname);
			String kycdatas = docservice.getKYCdatas(docdetails);
			
			//kycdatas = kycdatas.substring(0, kycdatas.length()-1);
			
			model1.put("kycdatas", kycdatas);		
			
			String Indid = (String) session.getAttribute("Ind_id");
			 
			int year = docservice.findYear(Indid);
			 
			 request.setAttribute("year",year);
			 request.setAttribute("user",Indid);
			
			//Code for reselect Category
			    String component = docservice.getKYCComp();
				
				request.setAttribute("comp", component);
				
				int indId = Integer.parseInt(Indid);
				String old_selComp = docservice.getOldComp(indId);
				
				request.setAttribute("OldComp",old_selComp);
			 //End of code for Reselect Category
			
                int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
				
				request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
				
				//On File Scheduler
				String individualInd_id = (String)session.getAttribute("Ind_id");								
				FileEncryptionDecryptionUtil.getOwnFileDecryption(individualInd_id,(String)session.getAttribute("created_year"),request);
				//End Of File Scheduler
			
				
				String pannelWithId = docservice.getPannelWithId(ind_id,old_selComp);
									
				request.setAttribute("pannelWithId",pannelWithId);	
				
			return new ModelAndView("individualDocuments",model1);
		}
		
		else
		{
			String component = docservice.getKYCComp();		
			request.setAttribute("comp", component);		
			
			return new ModelAndView("choose_IDs", model); 
		}
	}
	
	
	@RequestMapping(value="/choose_IDs", method=RequestMethod.POST)
	public ModelAndView reselectCategory(HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
        String component = docservice.getKYCComp();
		
		request.setAttribute("comp", component);
		
		int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));
		String old_selComp = docservice.getOldComp(indId);
		
		request.setAttribute("OldComp",old_selComp);
		
		return new ModelAndView("choose_IDs", model);
	}
	
	
	
	/* Preview Of Thumbnail */
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewThumbNailDocs", method = RequestMethod.GET)
	@ResponseBody public byte[] previewThumbNailDocs(HttpServletRequest request,HttpSession session) throws IOException {
		
		int year = docservice.findYear((String)session.getAttribute("Ind_id"));
		
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(kycMainDirectory);
		
		String docCategory = request.getParameter("docCategory");
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/");
		
		fPath = fPath.append(year + "/" + (String)session.getAttribute("Ind_id"));
		
		
		if(StringUtils.isNotBlank(docCategory))
		{
			if(docCategory.equals("KYC_DOCUMENT"))
			{				
				fPath = fPath.append("/ThumbNail_Documents/KYC_DOCUMENT/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("ACADEMIC_DOC"))
			{
				fPath = fPath.append("/ThumbNail_Documents/ACADEMIC_DOC/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("Financial_DOC"))
			{
				fPath = fPath.append("/ThumbNail_Documents/Financial_DOC/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("Employment_DOC"))
			{				
				fPath = fPath.append("/ThumbNail_Documents/Employement_DOC/" + request.getParameter("fileName"));
			}
			else
			{
				fPath = fPath.append("/ThumbNail_Documents/Profile_Photo/" + request.getParameter("fileName"));
			}
			
			

			/* Checks that files are exist or not  */
				File exeFile=new File(fPath.toString());
				if(!exeFile.exists())
				{
					fPath.delete(0, fPath.length());
					File targetFile = new File(request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY/image-not-found.png"));					
					fPath.append(targetFile.getPath());
				}	
			/* Checks that files are exist or not  */
			
		}
		
		InputStream in;
		try {
			
			if (AccessControl.isValidSession(request)) {
				
				FileInputStream docdir = new FileInputStream(fPath.toString());				
				if (docdir != null) 
				{										
					return IOUtils.toByteArray(docdir);		
				} 
				else 
				{						
					return null;
				}
			} 
			else {                                   
				
				in = servletContext.getResourceAsStream("/resources/images/Access_Denied.jpg");
				if (in != null) 
				{
					return IOUtils.toByteArray(in);
				} else
				{
					return null;
				}
			}
			
		} 
		catch (FileNotFoundException e) {
					
			return null; // todo: return safe photo instead
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	
	
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewDocs", method = RequestMethod.GET)
	@ResponseBody public byte[] previewDocs(HttpServletRequest request,HttpSession session) throws IOException {
		
		int year = docservice.findYear((String)session.getAttribute("Ind_id"));
		
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(kycMainDirectory);
		
		String docCategory = request.getParameter("docCategory");
				
		//StringBuilder fPath = new StringBuilder("/KYC_DOC_REPOSITORY/");
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/");
		
		fPath = fPath.append(year + "/" + (String)session.getAttribute("Ind_id"));
		
		
		if(StringUtils.isNotBlank(docCategory))
		{
			if(docCategory.equals("KYC_DOCUMENT"))
			{
				fPath = fPath.append("/Document/KYC_DOCUMENT/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("ACADEMIC_DOC"))
			{
				fPath = fPath.append("/Document/ACADEMIC_DOC/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("Financial_DOC"))
			{
				fPath = fPath.append("/Document/Financial_DOC/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("Employment_DOC"))
			{				
				fPath = fPath.append("/Document/Employement_DOC/" + request.getParameter("fileName"));
			}
			else
			{
				fPath = fPath.append("/Profile_Photo/" + request.getParameter("fileName"));
			}
			

			/* Checks that files are exist or not  */
				File exeFile=new File(fPath.toString());
				if(!exeFile.exists())
				{
					fPath.delete(0, fPath.length());
					File targetFile = new File(request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY/image-not-found.png"));					
					fPath.append(targetFile.getPath());
				}	
			/* Checks that files are exist or not  */
				
		}
		
		InputStream in;
		try {
			
			if (AccessControl.isValidSession(request)) {
				
					
				FileInputStream docdir = new FileInputStream(fPath.toString());
				
				//in = servletContext.getResourceAsStream(fPath.toString());
												
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{	
					System.out.println("Resource not found !");
					return null;
				}
			} 
			else {
				
				in = servletContext.getResourceAsStream("/resources/images/Access_Denied.jpg");
				if (in != null) 
				{
					return IOUtils.toByteArray(in);
				} else
				{
					return null;
				}
			}
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
		
	private Object getServletContext() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@RequestMapping(value ="/checkKYCIdExistOrNot", method = RequestMethod.POST)
	@ResponseBody public String checkKYCIdExistOrNot(HttpSession session,HttpServletRequest request)throws Exception 
	{
		String status = "";
		String KycId = request.getParameter("kycId");
		int ownIndId =Integer.parseInt((String)session.getAttribute("Ind_id"));
		status = docservice.getcheckKYCIdExistOrNot(ownIndId,KycId);
		return status;
	}
	
	
	@RequestMapping(value ="/kyc_documents", method = RequestMethod.POST)
	@ResponseBody public String getChangePan(HttpSession session,HttpServletRequest request,@RequestParam CommonsMultipartFile[] fileUpload,@RequestParam CommonsMultipartFile[] fileUploadback)throws Exception 
	{	
		String documentName = "";
		String documentNameback = "";
		
		String checkKYCIdExistOrNot = "";
		String KycId = request.getParameter("name").toUpperCase();
		int ownIndId =Integer.parseInt((String)session.getAttribute("Ind_id"));
		checkKYCIdExistOrNot = docservice.getcheckKYCIdExistOrNot(ownIndId,KycId);
		
		File ourPath = new File(kycMainDirectory);
		
		if(checkKYCIdExistOrNot=="Yes")
		{	        		
			return "Yes";
		}
	    else
		{	    	
	    	String user = (String)session.getAttribute("Ind_id");		           
			float datasizeKB=(float)0.0;         
			File fld = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT");
	    	fld.mkdirs();
	    	
	    	
			   String loc=ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT";
			
								if (fileUpload != null && fileUpload.length > 0)
						        {			
						            for (CommonsMultipartFile aFile : fileUpload)            
						            {             
						                if (!aFile.getOriginalFilename().equals(""))                 
						                {   
						                	
						                    aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
						                                       
						                    File datasize = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT"+"/"+aFile.getOriginalFilename());
						                    float bytes = datasize.length();
						                    datasizeKB = datasizeKB + bytes/1024;                   
						                    String extension = "";
						                    
						                    int i = aFile.getOriginalFilename().lastIndexOf('.');
						                    if (i > 0) {
						                        extension = aFile.getOriginalFilename().substring(i+1);
						                               }
						                                                       
						                   File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT");
						                   File[] storelist = store.listFiles();
						                   
						                   String flname=""+request.getParameter("path")+" front side"+"_1";
						                   boolean check=true;
						                   
						                   for(int f=0;f<50;f++)
						                    {                 	   
						                	   if(check==true)
						                	   {
						                    	  for(int g=0;g<storelist.length;g++)
						                    	   {    
						                    		  
						                    	      if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
						                               	{                   		      
						                    	    	    check=false;                    	    	    
						                    	        }
						                    	   }
						                    	  if(check==true)
						                       	       {                     		   
						                       		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT"+"/"+aFile.getOriginalFilename());                   
						                                  File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT"+"/"+flname+"."+extension);
						                                  floldname.renameTo(flnewname);                                 
						                                  documentName = flnewname.getName();
						                                  
							                                 // For backup the file uploading file
							                      	    	    
							                      	    		ResourceBundle addResource = ResourceBundle.getBundle("resources/kycFileDirectory");		
							                      	    		String fileBackUpDirectory = addResource.getString("fileBackUpDirectory");		
							                      	    		
							                      	    		File backUpFolder = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT");							                      	    		
							                                    
							                                    if(!backUpFolder.exists())
							                                    {
							                                    	backUpFolder.mkdirs();
							                                    }                        
							                                    File destinationFile = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT/"+flnewname.getName());
							                                    MakeBackUpForDocuments copyFileExample = new MakeBackUpForDocuments();
							                                    copyFileExample.copyFile(flnewname, destinationFile);
							                                    
							                      	    	// For backup the file uploading file	
							                      	    		
						                      	    								                                 
						                                  /* ThumbNail For KYC Documents */
						                                  
							                                if(!extension.equals("pdf"))
							                                {
							                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"KYC_DOCUMENT");
							                                	  thumbNailDirectory.mkdirs();
							                                	  BufferedImage img = ImageIO.read(flnewname); 
								                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
								                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
								                                  ImageIO.write(thumbImg,extension,os);
								                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"KYC_DOCUMENT"+"/"+flname+"."+extension);  
								                                  ImageIO.write(thumbImg, extension, f2);						                               
							                                }
							                                 					                               
						                                  /* End of Thumbnail Creation */
						                                  
						                                  break;
						                       	       }             	                         	                     	                     	                    	  
						                	   }                	  
						                	   if(check==false)
						                	   {
						                		   String[] ary=flname.split("_");
						                		   
						                		   for(int k=1;k<ary.length;k++)
						                		   {  
						         	                  int l=Integer.parseInt(ary[k])+1;
						         	                  String s=Integer.toString(l);
						         	                  flname="";
						         	                  flname=""+request.getParameter("path")+" front side"+"_".concat(s);
						         	                  check=true;
						                		   }
						                	    }                	                   	   
						                    }                   
						                }
						            }
						        }
								
								
								
								
								/*  File Two  */
								
								if (fileUploadback != null && fileUploadback.length > 0)
						        {			
						            for (CommonsMultipartFile aFile : fileUploadback)            
						            {             
						                if (!aFile.getOriginalFilename().equals(""))                 
						                {   
						                	
						                    aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
						                                       
						                    File datasize = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT"+"/"+aFile.getOriginalFilename());
						                    float bytes = datasize.length();
						                    datasizeKB = datasizeKB + bytes/1024;                   
						                    String extension = "";
						                    
						                    int i = aFile.getOriginalFilename().lastIndexOf('.');
						                    if (i > 0) {
						                        extension = aFile.getOriginalFilename().substring(i+1);
						                               }
						                                                       
						                   File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT");
						                   File[] storelist = store.listFiles();
						                   
						                   String flname=""+request.getParameter("path")+" back side"+"_1";
						                   boolean check=true;
						                   
						                   for(int f=0;f<50;f++)
						                    {                 	   
						                	   if(check==true)
						                	   {
						                    	  for(int g=0;g<storelist.length;g++)
						                    	   {    
						                    		  System.out.println("Check File name and given file Name"+flname.concat(".").concat(extension)+"=="+storelist[g].getName());
						                    	      if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
						                               	{                   		      
						                    	    	    check=false;                    	    	    
						                    	        }
						                    	   }
						                    	  if(check==true)
						                       	       {                     		   
						                       		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT"+"/"+aFile.getOriginalFilename());                   
						                                  File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT"+"/"+flname+"."+extension);
						                                  floldname.renameTo(flnewname);                                 
						                                  documentNameback = flnewname.getName();
						                                  
						                                  
						                                  // For backup the file uploading file
						                      	    	    
						                      	    		ResourceBundle addResource = ResourceBundle.getBundle("resources/kycFileDirectory");		
						                      	    		String fileBackUpDirectory = addResource.getString("fileBackUpDirectory");		
						                      	    		
						                      	    		File backUpFolder = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT");							                      	    		
						                                    
						                                    if(!backUpFolder.exists())
						                                    {
						                                    	backUpFolder.mkdirs();
						                                    }                        
						                                    File destinationFile = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"KYC_DOCUMENT/"+flnewname.getName());
						                                    MakeBackUpForDocuments copyFileExample = new MakeBackUpForDocuments();
						                                    copyFileExample.copyFile(flnewname, destinationFile);
						                                    
						                      	    	 // For backup the file uploading file	
						                                    
						                                    
						                                  /* ThumbNail For KYC Documents */
						                                  
							                                if(!extension.equals("pdf"))
							                                {
							                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"KYC_DOCUMENT");
							                                	  thumbNailDirectory.mkdirs();
							                                	  BufferedImage img = ImageIO.read(flnewname); 
								                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
								                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
								                                  ImageIO.write(thumbImg,extension,os);
								                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"KYC_DOCUMENT"+"/"+flname+"."+extension);  
								                                  ImageIO.write(thumbImg,extension, f2);						                               
							                                }
							                                 
							                                  
						                                  /* End of Thumbnail Creation */
						                                  			
						                                  break;
						                       	       }             	                         	                     	                     	                    	  
						                	   }                	  
						                	   if(check==false)
						                	   {
						                		   String[] ary=flname.split("_");
						                		   
						                		   for(int k=1;k<ary.length;k++)
						                		   {  
						         	                  int l=Integer.parseInt(ary[k])+1;
						         	                  String s=Integer.toString(l);
						         	                  flname="";
						         	                  flname=""+request.getParameter("path")+" back side"+"_".concat(s);
						         	                  check=true;
						                		   }
						                	    }                	                   	   
						                    }                   
						                }
						                else
						                {

											String documentNameStatus = docservice.getLatestKycBackSideDoc(user,request.getParameter("path"));								
											
											if(documentNameback.contains("_"))
											{
												documentNameback = documentNameStatus;
											}
											else
											{
												documentNameback = "Not Uploaded";
											}
											
						                }
						            }
						           
						        }
								
								
						        int i =Integer.parseInt((String)session.getAttribute("Ind_id"));
						        
						        DocumentDetailsBean docdetailsbean = new DocumentDetailsBean();
						          
						        docdetailsbean.setDoc_name(documentName+","+documentNameback);
						        docdetailsbean.setDoc_size(datasizeKB);
						        docdetailsbean.setDoc_type("KYC_DOCUMENT");
						        docdetailsbean.setKyc_ind_id(i);
						        docdetailsbean.setDocs_data(documentName+","+documentNameback+","+request.getParameter("name").toUpperCase()+","+request.getParameter("des")+","+new Date());
						        docdetailsbean.setCr_date(new Date());
						       // docdetailsbean.setPanel_status((String)session.getAttribute("kyc_panel_status"));
						        docdetailsbean.setPanel_status(docservice.getLatestKYCPannel(i));
						        docdetailsbean.setDoc_des(request.getParameter("des"));
						        
								DocumentDetailModel docdetailmodel = prepareDocDetailModel(docdetailsbean);
								session.setAttribute("id", docdetailmodel.getDoc_id());
								
								String status ="";
								
								if ("success".equals(docservice.uploadKYCDocumentDetail(docdetailmodel))) {
							        
									String Kyc_id= (String) session.getAttribute("Kyc_id");
									Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
									Profileupdatemodel.setProfileupdatedetail(request.getParameter("path")+" has been updated under KYC documents.");
									Profileupdatemodel.setProfileupdatetime(new Date());
									Profileupdatemodel.setType_of_alert("doc update");
									Profileupdatemodel.setMore_about_it(request.getParameter("path")+" uploaded");
								    Profileupdatemodel.setKyc_id(Kyc_id);
									profileinfoService.profileupdate(Profileupdatemodel);
									
									return "redirect:/choose_KYC_Documents.html";								
								}					        
								else
								{
									return "failure";
								}	
		}
		
	
	}   /* End of Uploading KYC Documents */
	
			
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ModelAndView getchangedone(HttpServletRequest request,HttpSession session) {
		
         String Indid = (String) session.getAttribute("Ind_id");
		 
		 int year = docservice.findYear(Indid);
		 
		 request.setAttribute("year",year);
		 request.setAttribute("user",Indid);
				 		 							
		 FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn((String)session.getAttribute("created_year"),Indid,"KYC_DOCUMENT",request.getParameter("fileName"),request);
		 				 
		 return new ModelAndView("download");
	}
		
	@RequestMapping(value = "/downloadAadhar", method = RequestMethod.GET)
	public ModelAndView getAadhar() {
		
		return new ModelAndView("downloadAadhar");
	}
	
	
	@RequestMapping(value = "/center_body21", method = RequestMethod.GET)
	public ModelAndView getAcademinBody(HttpServletRequest request,HttpSession session ,DocumentDetailModel docdetailmodel) {
		
        String accordname = (String)session.getAttribute("accordname");
	    
        String Indid = (String) session.getAttribute("Ind_id");	 
		int year = docservice.findYear(Indid);		 
		request.setAttribute("year",year);
		request.setAttribute("user",Indid);		
		//Code for reselect option		
		int kycInd_id = Integer.parseInt((String) session.getAttribute("Ind_id"));
		String slectedComponents = docservice.getAcademicStatus(kycInd_id);		
		List<String> acaComponents = docservice.getAcademicDocComp();	
		request.setAttribute("oldSelect", slectedComponents);
		request.setAttribute("comp", acaComponents);		           // this block is setting indid and year in a session and as per the selected panel and old panel setting in request object for show the non-selected in a slider of academicdetails.jsp page 
		//End of code for reselect option 
		
		
		DocumentHierarchyBean docbean = new DocumentHierarchyBean();		
		docbean.setDoc_hir_id(Integer.parseInt((String)session.getAttribute("Ind_id")));		
		docbean.setDoc_hierarchy(accordname);
		
		String docsStringToken=docservice.getDocsVeiwOrNew(docbean); 
		docsStringToken = docsStringToken.substring(0, docsStringToken.length() - 1);		
			
		session.setAttribute("accordname", accordname);		
		session.setAttribute("acdemic_panel_status", accordname);		
		      		
		Map<String, String> model = new HashMap<String, String>();
		model.put("accordNames", accordname);	
		
		model.put("accDataAndHierarchy", docsStringToken);                  // this block will retrieve the data as per the chosen Panel
										
		return new ModelAndView("academicDetails",model);
				
	}
	
	
	@RequestMapping(value = "/center_body2", method = RequestMethod.GET)
	public ModelAndView getbody2(@ModelAttribute("command") EmployeeBean employeeBean,BindingResult result) {
		
		   Map<String, Object> model = new HashMap<String, Object>();
		
		return new ModelAndView("academicDetails", model);
	}
		
	@RequestMapping(value = "/add/choosen/academic/category", method = RequestMethod.POST)
	public ModelAndView ProceedChooseBody(HttpServletRequest request,HttpSession session) {
		
	    String allAccorName = "";
		
		String click[] = request.getParameterValues("click");	    
		String accordname="";
		
		List list =  Arrays.asList(click); 
		Iterator<String> itr=list.iterator();
		
		while(itr.hasNext())
		{			
			accordname = accordname.concat(itr.next()+",");			
		}
				
		accordname = accordname.substring(0, accordname.length() - 1);	
		
		System.out.println("Selected Accord Names="+accordname);
		
		int id = Integer.parseInt((String) session.getAttribute("Ind_id"));
		String lsd = docservice.getAcademicStatus(id);
		
		if(lsd.length()>0)
		{
			accordname = lsd+","+accordname;
		}
		
		DocumentDetailsBean docBean =  new DocumentDetailsBean();
		
		docBean.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
		docBean.setCr_by(docservice.getIndName(id));		
		docBean.setCr_date(new Date());				
		docBean.setDoc_type("ACADEMIC_DOC");
		docBean.setPanel_status(accordname);		
		DocumentDetailModel docmodel = prepareDocModel(docBean);		
		docservice.setPannelDocDetails(docmodel);		               // Up to that Save the selected Academic type from choose category in document_details table   
					
		String Indid = (String) session.getAttribute("Ind_id");		 
		int year = docservice.findYear(Indid);		 
		request.setAttribute("year",year);
		request.setAttribute("user",Indid);		
		//Code for reselect option		
		int kycInd_id = Integer.parseInt(Indid);
		String slectedComponents = docservice.getAcademicStatus(kycInd_id);		
		List<String> acaComponents = docservice.getAcademicDocComp();	
		request.setAttribute("oldSelect", slectedComponents);
		request.setAttribute("comp", acaComponents);		           // this block is setting indid and year in a session and as per the selected panel and old panel setting in request object for show the non-selected in a slider of academicdetails.jsp page 
		//End of code for reselect option 
		
		
		DocumentHierarchyBean docbean = new DocumentHierarchyBean();		
		docbean.setDoc_hir_id(Integer.parseInt((String)session.getAttribute("Ind_id")));		
		docbean.setDoc_hierarchy(accordname);
		
		String docsStringToken=docservice.getDocsVeiwOrNew(docbean); 
		docsStringToken = docsStringToken.substring(0, docsStringToken.length() - 1);		
			
		session.setAttribute("accordname", accordname);		
		session.setAttribute("acdemic_panel_status", accordname);		
		      		
		Map<String, String> model = new HashMap<String, String>();
		model.put("accordNames", accordname);	
		
		model.put("accDataAndHierarchy", docsStringToken);                  // this block will retrieve the data as per the chosen Panel
		
		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));		
		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
			
		return new ModelAndView("individualAcademicDetails",model);
	}
	
	

	@RequestMapping(value ="/getacademicdetails", method = RequestMethod.GET)
	public ModelAndView getAcademicDetails(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		    session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("individualPage");	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("individualPage");	
			}
			else
			{	
						DocumentDetailModel docdetails = new DocumentDetailModel();
						
						docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
						docdetails.setDoc_type("ACADEMIC");
						
						String accordname = docservice.getstatus(docdetails);
						
						// Logger File  for current user's IpAddress and Mac Address with Name Date and Time
						
						ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
						String individualDirectory=loggerFileDirectory.getString("loggerDirectoryIndividual");

						
						String DATE_FORMAT_NOW = "dd-MM-yyyy";
						Date date = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
						String stringDate = sdf.format(date );
						stringDate += "-academicLogger";
						   
                        loggerfile = new FileAppender(new SimpleLayout(),""+individualDirectory+"/"+stringDate+".log");
						
					    log.addAppender(loggerfile);
					    loggerfile.setLayout(new SimpleLayout());
					    
					    log.info("reaches Fine the Panel Exising Status="+accordname);
					  
					    // Logger Config who has Log in	
					 
					    
					    int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
						request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
						Map<String, String> model = new HashMap<String, String>();
						
						model.put("accordNames", accordname);	
						
						List<String> acaComponents = docservice.getAcademicDocComp();	
						request.setAttribute("comp", acaComponents);	
						String Indid = Integer.toString(ind_id);		 
						int year = docservice.findYear(Indid);		 
						request.setAttribute("year",year);
						request.setAttribute("user",Indid);		
						int kycInd_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
						String slectedComponents = docservice.getAcademicStatus(kycInd_id);				
						request.setAttribute("oldSelect", slectedComponents);
					    
						
						if(!accordname.equals("Not There"))
						{								
							try
							{				
								//Code for reselect option		
								
								// this block is setting indid and year in a session and as per the selected panel and old panel setting in request object for show the non-selected in a slider of academicdetails.jsp page 
								//End of code for reselect option 
											
								DocumentHierarchyBean docbean = new DocumentHierarchyBean();		
								docbean.setDoc_hir_id(Integer.parseInt((String)session.getAttribute("Ind_id")));		
								docbean.setDoc_hierarchy(accordname);
								
								String docsStringToken=docservice.getDocsVeiwOrNew(docbean); 
								docsStringToken = docsStringToken.substring(0, docsStringToken.length() - 1);		
									
								session.setAttribute("accordname", accordname);		
								session.setAttribute("acdemic_panel_status", accordname);		
								      													
								model.put("accDataAndHierarchy", docsStringToken);                  // this block will retrieve the data as per the chosen Panel
				
								
								
								log.info("-"+date+"--"+accordname+"-Exist");
								
							}
							catch (Exception e)
							{
								e.getStackTrace();
								log.info("-"+date+"--Academic Details Already Exist & Theses are="+accordname);
								log.error("-"+date+"--"+e.toString());
							}
										  
							return new ModelAndView("individualAcademicDetails",model);
						}
						
						else
						{								
							return new ModelAndView("individualAcademicDetails",model);			
						}
						
			}
		
	}
	
	
	
	@RequestMapping(value = "/choose_categoryonly", method = RequestMethod.POST)
	public ModelAndView ChooseBody(HttpServletRequest request,HttpSession session) throws IOException {
						
		DocumentDetailModel docdetails = new DocumentDetailModel();
		docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
		docdetails.setDoc_type("ACADEMIC");
		
		String accordname = docservice.getstatus(docdetails);
		
		if(!accordname.equals("Not There"))
		{	
		
				String Indid = (String)session.getAttribute("Ind_id");		 
				int year = docservice.findYear(Indid);		 
				request.setAttribute("year",year);
				request.setAttribute("user",Indid);		
				//Code for reselect option		
				int kycInd_id = Integer.parseInt(Indid);
				String slectedComponents = docservice.getAcademicStatus(kycInd_id);		
				List<String> acaComponents = docservice.getAcademicDocComp();	
				request.setAttribute("oldSelect", slectedComponents);
				request.setAttribute("comp", acaComponents);		           // this block is setting indid and year in a session and as per the selected panel and old panel setting in request object for show the non-selected in a slider of academicdetails.jsp page 
				//End of code for reselect option 
				
				
				DocumentHierarchyBean docbean = new DocumentHierarchyBean();		
				docbean.setDoc_hir_id(Integer.parseInt((String)session.getAttribute("Ind_id")));		
				docbean.setDoc_hierarchy(accordname);
				
				String docsStringToken=docservice.getDocsVeiwOrNew(docbean); 
				docsStringToken = docsStringToken.substring(0, docsStringToken.length() - 1);		
					
				session.setAttribute("accordname", accordname);		
				session.setAttribute("acdemic_panel_status", accordname);		
				      		
				Map<String, String> model = new HashMap<String, String>();
				model.put("accordNames", accordname);	
				
				model.put("accDataAndHierarchy", docsStringToken);                  // this block will retrieve the data as per the chosen Panel

				int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
				request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
							
			
				  
		   
			return new ModelAndView("individualAcademicDetails",model);
		}
		
		else
		{
		
			Map<String, Object> model = new HashMap<String, Object>();			
			List<String> ls = docservice.getAcademicDocComp();
			request.setAttribute("comp", ls);	
			
			int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
			request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
		
			return new ModelAndView("individual_choose_academic_category", model);
		}
		
	}
	
	@RequestMapping(value = "/reselect" , method = RequestMethod.POST)
	public ModelAndView reselect(HttpServletRequest request,HttpSession session)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		
		int id = Integer.parseInt((String) session.getAttribute("Ind_id"));
		String lsd = docservice.getAcademicStatus(id);
		
		List<String> ls = docservice.getAcademicDocComp();
		
		request.setAttribute("oldSelect", lsd);
		request.setAttribute("comp", ls);
		
		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
				
		return new ModelAndView("individual_choose_academic_category", model);
	}
	
	   
	@RequestMapping(value = "/academic_viewmore", method = RequestMethod.GET)
 	public ModelAndView GetViewMore(HttpServletRequest request,HttpSession session) {
 	    
		session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null)
		{			
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return new ModelAndView("individualPage");	
		}
		else if(((String)session.getAttribute("Ind_id")).equals(null))
		{
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return new ModelAndView("individualPage");	
		}
		else
		{					
		     	int doc_id = Integer.parseInt(request.getParameter("idIndex"));
		     	
		     	session.setAttribute("hitoryDate", docservice.getHitoryDateOfThisId(doc_id));
		     	
		     	int getOwnedIndId = docservice.getIndIdviaDocId(doc_id);
		     	int ownId = Integer.parseInt((String)session.getAttribute("Ind_id"));
		     	
		     	if(getOwnedIndId==ownId)
		     	{
		     		String docs_data = docservice.getAcademicDataById(doc_id);    		
		     	    String docName = docservice.getAcademicDocNameById(doc_id);
		     	     	     	    
		     	    String Indid = (String)session.getAttribute("Ind_id");		 
		    		int year = docservice.findYear(Indid);		 
		    		
		    		request.setAttribute("year",year);
		     	    request.setAttribute("user", Integer.parseInt((String) session.getAttribute("Ind_id")));
		     	    request.setAttribute("docsData",docs_data);
		    	    request.setAttribute("docName", docName);
		    	    
		
		    	    int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
		    	    request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
		   	    
		    	    request.setAttribute("TypeOfDoc", request.getParameter("docType"));
		    	    
		     		return new ModelAndView("individual_academic_viewmore");
		     	}
		     	else
		     	{
		     		return new ModelAndView("kycErrorPage");
		     	}
		     	
		}
  	
 	}
      
	
	
	@RequestMapping(value = "/academicHistorydetail", method = RequestMethod.GET)
	public ModelAndView GetHistory(HttpServletRequest request,HttpSession session) {
		
		
						session = request.getSession(false);
						
						if (session.getAttribute("Ind_id") == null)
						{			
							 request.setAttribute("kycdocumentList", docservice.getKYCComp());
							 request.setAttribute("status","Your session has expired please login to use the portal !");
							
							 session.removeAttribute("Ind_id");
							 //session.invalidate();
							 
							 return new ModelAndView("individualPage");	
						}
						else if(((String)session.getAttribute("Ind_id")).equals(null))
						{
							 request.setAttribute("kycdocumentList", docservice.getKYCComp());
							 request.setAttribute("status","Your session has expired please login to use the portal !");
							
							 session.removeAttribute("Ind_id");
							 //session.invalidate();
							 
							 return new ModelAndView("individualPage");	
						}
						else
						{							
						    	DocumentDetailModel docdetails = new DocumentDetailModel();
						    	docdetails.setKyc_ind_id(Integer.parseInt((String) session.getAttribute("Ind_id")));
								    	
						    	request.setAttribute("doctype", request.getParameter("id"));
						    	
								docdetails.setDoc_name((String)request.getParameter("id"));		
								Map<String, Object> model = new HashMap<String, Object>();		
								model.put("Historydetails", prepareDocumentDetailBean(docservice.getHistory(docdetails,session)));	    
								int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
								request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
								
								return new ModelAndView("ind_academic_historyDetail",model);	
						}
						
			}
		   
			@RequestMapping(value="/deleteAcademicfileRecord", method=RequestMethod.POST)
			public @ResponseBody String deleteAcademicFileRecord(HttpServletRequest request,HttpSession session) throws Exception
			{
				String status = "";
				String user =(String) session.getAttribute("Ind_id");      
		        int ownIndId =Integer.parseInt((String)user);
		        String fileList = docservice.getAcademicFaultFileName(ownIndId);
		        
		        session.setAttribute("garbageFileName",fileList);
		        
		        String register_year = (String)session.getAttribute("created_year");
		        if(!fileList.equals(""))
		        {
		        	String fileListArry[] = fileList.split(",");
		        	String fileStatusFormDB = docservice.deleteAcademicFaultFileDetails(ownIndId);
		        	
		        	for(int i=0;i<fileListArry.length;i++)
		        	{        		
		        		File file = new File(request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY")+"/"+register_year+"/"+user+"/Document/"+"ACADEMIC_DOC"+"/"+fileListArry[i]);
		        		System.out.println("File Location ="+file.getAbsolutePath());
		        		boolean blnDeleted = file.delete();     
		        		System.out.println("File status ="+blnDeleted);
		        	} 
		        	
		        	status = "Done";
		        }
		        
		     // Logger File  for current user's IpAddress and Mac Address with Name Date and Time
				
				ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
				String individualDirectory=loggerFileDirectory.getString("loggerDirectoryIndividual");

				
				String DATE_FORMAT_NOW = "dd-MM-yyyy";
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
				String stringDate = sdf.format(date );
				stringDate += "-academicLogger";
				   
                loggerfile = new FileAppender(new SimpleLayout(),""+individualDirectory+"/"+stringDate+".log");
				
			    log.addAppender(loggerfile);
			    loggerfile.setLayout(new SimpleLayout());
			    
			    log.info("If unconditional upload document and didn't save the records will delete.="+status);
			  
			    // Logger Config who has Log in	
			 
			    
				return status;
	}
	
	
	@RequestMapping(value="/uploadAcademicDocuments", method=RequestMethod.POST)
	public @ResponseBody String uploadAcademicDocuments(HttpServletRequest request,HttpSession session,@RequestParam CommonsMultipartFile[] uploadcert) throws Exception
	{
			
		String user =(String) session.getAttribute("Ind_id");
		int kyc_ind_id = Integer.parseInt(user);
		float datasizeKB=(float) 0.0;	
		String documentName1= "";		
		
		File ourPath = new File(kycMainDirectory);
		
		String register_year = (String)session.getAttribute("created_year");
		
		File fld = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"ACADEMIC_DOC");
    	if(!fld.exists())
    	{
		fld.mkdirs();
    	}
		String loc=ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"ACADEMIC_DOC";
        												
						if (uploadcert.length > 0)
				        {			
				            for (CommonsMultipartFile aFile : uploadcert)            
				            {    
				                if (!aFile.getOriginalFilename().equals(""))                 
				                {                   	 
				                    aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
				                    
				                    File datasize = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"ACADEMIC_DOC"+"/"+aFile.getOriginalFilename());
				                    float bytes = datasize.length();
				                    datasizeKB = datasizeKB + bytes/1024; 
				                    String extension = "";
				                    
				                    int i = aFile.getOriginalFilename().lastIndexOf('.');
				                    if (i > 0) {
				                        extension = aFile.getOriginalFilename().substring(i+1);
				                               }
				                                                       
				                   File store = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"ACADEMIC_DOC");
				                   File[] storelist = store.listFiles();
				                   
				                   String flname=""+request.getParameter("standardname")+"-"+request.getParameter("labelName")+"_1";
				                   boolean check=true;
				                   
				                   for(int f=0;f<150;f++)
				                    { 
				                	   
				                	   if(check==true)
				                	   {
				                    	  for(int g=0;g<storelist.length;g++)
				                    	   {
				                    		 
				                    	     if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
				                               	{                   		      
				                    	    	    check=false;                    	    	    
				                    	        }
				                    	    }
				                    	  if(check==true)
				                       	       {                    		  
				                       		      File floldname = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"ACADEMIC_DOC"+"/"+aFile.getOriginalFilename());                   
				                                  File flnewname = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"ACADEMIC_DOC"+"/"+flname+"."+extension);
				                                  floldname.renameTo(flnewname);
				                                  documentName1 = flnewname.getName();
				                                  
				                                  
				                                  // For backup the file uploading file
				                      	    	    
				                      	    		ResourceBundle addResource = ResourceBundle.getBundle("resources/kycFileDirectory");		
				                      	    		String fileBackUpDirectory = addResource.getString("fileBackUpDirectory");		
				                      	    		
				                      	    		File backUpFolder = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"ACADEMIC_DOC");							                      	    		
				                                    
				                                    if(!backUpFolder.exists())
				                                    {
				                                    	backUpFolder.mkdirs();
				                                    }                        
				                                    File destinationFile = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"ACADEMIC_DOC/"+flnewname.getName());
				                                    MakeBackUpForDocuments copyFileExample = new MakeBackUpForDocuments();
				                                    copyFileExample.copyFile(flnewname, destinationFile);
				                                    
				                      	    	 // For backup the file uploading file	
				                                    
				                                    
				                                  /* ThumbNail For KYC Documents */
				                                  
					                                if(!extension.equals("pdf"))
					                                {
					                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"ACADEMIC_DOC");
					                                	  thumbNailDirectory.mkdirs();
					                                	  BufferedImage img = ImageIO.read(flnewname); 
						                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
						                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
						                                  ImageIO.write(thumbImg,extension,os);
						                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"ACADEMIC_DOC"+"/"+flname+"."+extension);  
						                                  ImageIO.write(thumbImg, extension, f2);						                               
					                                }
					                                 
					                                  
				                                  /* End of Thumbnail Creation */

				                                  
				                                  break;
				                       	       }             	     
				                    	                     	                     	                    	  
				                	   }
				                	  
				                	   if(check==false)
				                	   {
				                		   String[] ary=flname.split("_");
				                		   
				                		   for(int k=1;k<ary.length;k++)
				                		   {  
				         	                  int l=Integer.parseInt(ary[k])+1;
				         	                  String s=Integer.toString(l);
				         	                  flname="";
				         	                  flname=""+request.getParameter("standardname")+"-"+request.getParameter("labelName")+"_".concat(s);
				         	                  check=true;
				                		   }
				                	    }                	                   	   
				                    }                    
				                }
				            }
				        }
						
						AcademicFileUploadModel academicFileDetails = new AcademicFileUploadModel();
						academicFileDetails.setKyc_ind_id(kyc_ind_id);
						academicFileDetails.setFile_label_name(request.getParameter("labelName"));
						academicFileDetails.setFile_description(request.getParameter("description"));
						academicFileDetails.setFile_name(documentName1);
						academicFileDetails.setFile_size(datasizeKB);
						
						String fileUploadStatus = docservice.academicFileUpload(academicFileDetails);
								
						return fileUploadStatus;						
	}
	
	
	@RequestMapping(value="/Academic", method=RequestMethod.POST)
	public @ResponseBody String savesscDetails(HttpServletRequest request,HttpSession session) throws Exception {
				
		String user =(String)session.getAttribute("Ind_id");
								             
        int i =Integer.parseInt((String)session.getAttribute("Ind_id"));
        
        int kyc_ind_id = i;
        DocumentDetailsBean docdetailsbean = new DocumentDetailsBean();
         
        String oldDocumentList = docservice.getoldDocumentList(i,request.getParameter("standardname"));
        String oldDocumentListArray[] =  oldDocumentList.split(",");
        String newDocumentList = docservice.getAcademicUploadedDocName(kyc_ind_id);      
        String newDocumentListArray[] = newDocumentList.split(",");
        
        String latestdocumentName = "";
        
        if(!oldDocumentList.equals("No Data")  &&  !newDocumentList.equals("No Data"))      // checking previous document names list and current updated document names 
        {
        	for(int old=0;old<oldDocumentListArray.length;old++)
        	{
        		boolean condition = true;
        		String oldDocName[] = oldDocumentListArray[old].split("_");
   			
        		for(int ndoc=0;ndoc<newDocumentListArray.length;ndoc++)
        		{        			
        			String newDocName[] = newDocumentListArray[ndoc].split("_");
        			
        			 if(oldDocName[0].equals(newDocName[0]))
        			 {
        				   
        				 latestdocumentName += newDocumentListArray[ndoc] + ",";
        				 condition = false;        				
        			 }
        		}
        		
        		if(condition)
        		{
        			 latestdocumentName += oldDocumentListArray[old] + ",";        			   
        		}
        		
        	}    
        	
        	latestdocumentName = latestdocumentName.substring(0, latestdocumentName.length()-1);
        	
        	for(int ndoc=0;ndoc<newDocumentListArray.length;ndoc++)
    		{
        		if(!latestdocumentName.contains(newDocumentListArray[ndoc]))
        		{
        			latestdocumentName += ","+ newDocumentListArray[ndoc];
        		}
    		}        	
        }
        else if(!oldDocumentList.equals("No Data")  && newDocumentList.equals("No Data"))
        {
        	latestdocumentName = oldDocumentList;
        } 
        else if(oldDocumentList.equals("No Data")  && !newDocumentList.equals("No Data"))
        {
        	latestdocumentName += newDocumentList;
        }
        else 
        {
        	latestdocumentName += newDocumentList;
        }

        
        docdetailsbean.setDoc_name(latestdocumentName);        //Setting latest document List to the current Data
        
        docdetailsbean.setDoc_size(docservice.getAcademicUploadedDocSize());        
        docdetailsbean.setDoc_type("ACADEMIC_DOC");
        docdetailsbean.setKyc_ind_id(i);
        
        docdetailsbean.setDes("");
        docdetailsbean.setDoc_des(docservice.getAcademicUploadedDocDes());
        docdetailsbean.setPanel_status((String)session.getAttribute("acdemic_panel_status"));
        
        StringBuilder docsdatas = new StringBuilder();
 
        docsdatas.append(request.getParameter("standardname")+"-");
        		
        DocumentDetailModel detail = new DocumentDetailModel();
        detail.setDoc_type(request.getParameter("standardname"));
                      
        String strary[] = docservice.getHierarchy(detail).split(",");
        
        for(int j=0;j<strary.length;j++)
        {        	
        	if(!strary[j].split(":")[1].equals("fb"))
        	{
        		if((request.getParameter(strary[j].split(":")[0])).equals(""))
        		{       			
        			docsdatas.append(strary[j].split(":")[0]+":"+"Not filled"+",");
        		}
        		else
        		{       			
        			docsdatas.append(strary[j].split(":")[0]+":"+request.getParameter(strary[j].split(":")[0])+",");
        		}
        	}
        	      	
        }
        		       
        docdetailsbean.setDocs_data(docsdatas.toString().substring(0,docsdatas.length()-1));
        		
		DocumentDetailModel docdetailmodel = prepareDocDetailModel(docdetailsbean);
		session.setAttribute("id", docdetailmodel.getDoc_id());
		
		if ("success".equals(docservice.uploadDocumentDetail(docdetailmodel))) {
			
			String Kyc_id= (String) session.getAttribute("Kyc_id");
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail(request.getParameter("standardname")+" has been updated under Academic documents.");
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("doc update");
			Profileupdatemodel.setKyc_id(Kyc_id);
			Profileupdatemodel.setMore_about_it(request.getParameter("standardname")+"uploaded");
			profileinfoService.profileupdate(Profileupdatemodel); 
			
			docservice.getDeleteAllFromUploadTableDetails(kyc_ind_id);
			
			return "success";
			
		} else {

			return "failure";
		}       
	}
	
		
        private DocumentDetailModel prepareDocDetailModel(DocumentDetailsBean docdetailsbean) {
		
	    DocumentDetailModel docdetailmodel = new DocumentDetailModel();
			    
	    docdetailmodel.setDoc_name(docdetailsbean.getDoc_name());
	    docdetailmodel.setDoc_size(docdetailsbean.getDoc_size());
	    docdetailmodel.setDoc_type(docdetailsbean.getDoc_type());	    
	    docdetailmodel.setKyc_ind_id(docdetailsbean.getKyc_ind_id());	
	    docdetailmodel.setDes(docdetailsbean.getDes());
	    docdetailmodel.setDocs_data(docdetailsbean.getDocs_data());	 
	    docdetailmodel.setDoc_des(docdetailsbean.getDoc_des());
	    docdetailmodel.setPanel_status(docdetailsbean.getPanel_status());
	    docdetailmodel.setCr_date(new Date());
		return docdetailmodel;
	}
      
        
        @RequestMapping(value="/search_financialDoc", method = RequestMethod.GET)
        public ModelAndView getsearchFinancial(HttpServletRequest request,DocumentDetailModel docdetailmodel,HttpSession session) {
    		
               	
        	session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("individualPage");	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("individualPage");	
			}
			else
			{												
			        	int id =Integer.parseInt((String) session.getAttribute("Ind_id"));
			    		String financial= docservice.getFinancialComp();
			   		    
			    		String financialDoc = docservice.getFinancialDoc(id);
			    		
			    		Map<String, String> model = new HashMap<String, String>();
			    		
			    		model.put("financial", financial);
			    		model.put("docName", financialDoc);
			    		
			    		int year = Calendar.getInstance().get(Calendar.YEAR);
			    		
			    		request.setAttribute("year", year);
			    		
			    		String financialData = docservice.getFinancialData(id);
			    		
			    		request.setAttribute("financialData", financialData);
			    		
			    		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
			    		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
			    		
			    		
			    		return new ModelAndView("individual_financial_document",model);
			  }
			
    	}

        @RequestMapping(value="/financialDocument", method=RequestMethod.POST)
    	public String saveFinancialDetails(HttpServletRequest request,HttpSession session,
    			@RequestParam CommonsMultipartFile[] upl) throws Exception {
    		       	       	
    		String user =(String)session.getAttribute("Ind_id");
    		float docsizeKB = (float) 0.0;
    		String docName = "";
    		
    		String financial= docservice.getFinancialComp();
    		String finComp[] = financial.split(",");
    		String finName = "";
    		
    		finName = finName+request.getParameter("fromYear")+"-"+request.getParameter("toYear")+"-";
    		
    		String docsHalfData = "";
    		String docType = request.getParameter("docType").split(":")[0];
    		
    		docsHalfData = docsHalfData+docType+"&&"+request.getParameter("docMode");
    	    finName = finName+docType+"-"+request.getParameter("docMode");
    	  	
    	    ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
    		String kycMainDirectory = fileResource.getString("fileDirectory");	
    		 
    	    File ourPath = new File(kycMainDirectory);
    	    
			    		File fld = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC");
			        	if(!fld.exists())
			        	{
			    		fld.mkdirs();
			        	}
			    		String loc = ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC";
			            
			    		File fld1 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC");
			        	if(!fld1.exists())
			        	{
			    		fld1.mkdirs();
			        	}
			    			     		 
			    		if (upl != null && upl.length > 0)
			            {
			                for (CommonsMultipartFile aFile : upl)            
			                {             
			                    if (!aFile.getOriginalFilename().equals(""))                 
			                    {            
			                        aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));   
			                        
			                        File datasize = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC"+"/"+aFile.getOriginalFilename());
			                        float bytes = datasize.length();
			                        docsizeKB = docsizeKB + bytes/1024; 
			                        String extension = "";                        
			                        int i = aFile.getOriginalFilename().lastIndexOf('.');
			                        if (i > 0) {
			                             extension = aFile.getOriginalFilename().substring(i+1);
			                                   }    
			                                                                       		
			                       File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC");
			                       File[] storelist = store.listFiles();
			                       
			                       String flname=""+finName+"-FINANCIAL-DOCUMENT_1";
			                       boolean check=true;
			                       
			                       for(int f=0;f<50;f++)
			                        { 
			                    	   
			                    	   if(check==true)
			                    	   {
			                        	  for(int g=0;g<storelist.length;g++)
			                        	   {                       		 
			                        	     if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
			                                   	{                   		      
			                        	    	    check=false;                    	    	    
			                        	        }  
			                        	   }
			                        	  
			                        	  if(check==true)
			                           	       {                         		   
			                           		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC"+"/"+aFile.getOriginalFilename());                   
			                                      File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC"+"/"+flname+"."+extension);
			                                      floldname.renameTo(flnewname);                                     
			                                      docName=flnewname.getName();
			                                      
			                                      // For backup the file uploading file
				                      	    	    
				                      	    		ResourceBundle addResource = ResourceBundle.getBundle("resources/kycFileDirectory");		
				                      	    		String fileBackUpDirectory = addResource.getString("fileBackUpDirectory");		
				                      	    		
				                      	    		File backUpFolder = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC");							                      	    		
				                                    
				                                    if(!backUpFolder.exists())
				                                    {
				                                    	backUpFolder.mkdirs();
				                                    }                        
				                                    File destinationFile = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Financial_DOC/"+flnewname.getName());
				                                    MakeBackUpForDocuments copyFileExample = new MakeBackUpForDocuments();
				                                    copyFileExample.copyFile(flnewname, destinationFile);
				                                    
				                      	    	// For backup the file uploading file	
			                                    
			                                      /* ThumbNail For KYC Documents */
				                                  
					                                if(!extension.equals("pdf"))
					                                {
					                                	if(!extension.equals("doc"))
						                                {
					                                		if(!extension.equals("docx"))
							                                {
					                                			if(!extension.equals("xlsx"))
								                                {
					                                				if(!extension.equals("xls"))
									                                {
										                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Financial_DOC");
										                                	  thumbNailDirectory.mkdirs();
										                                	  BufferedImage img = ImageIO.read(flnewname); 
											                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
											                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
											                                  ImageIO.write(thumbImg,extension,os);
											                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Financial_DOC"+"/"+flname+"."+extension);  
											                                  ImageIO.write(thumbImg, extension, f2);	
									                                }
								                                }
							                                }
						                                }
					                                }
					                                 
					                                  
				                                  /* End of Thumbnail Creation */
			                                      
			                                      break;          
			                           	       }             	                              	                     	                     	                    	  
			                    	   }
			                    	  
			                    	   if(check==false)
			                    	   {
			                    		   String[] ary=flname.split("_");
			                    		   
			                    		   for(int k=1;k<ary.length;k++)
			                    		   {  
			             	                  int l=Integer.parseInt(ary[k])+1;
			             	                  String s=Integer.toString(l);
			             	                  flname="";
			             	                  flname=""+finName+"-FINANCIAL-DOCUMENT_".concat(s);
			             	                  check=true;
			                    		   }
			                    	   }                    	                       	   
			                        }                                                                    
			                    }
			                }
			            }  
			    		 
			    		int i =Integer.parseInt(user);
			    		    		
			            DocumentDetailsBean docdetailsbean = new DocumentDetailsBean();
			            
			            
			            docdetailsbean.setDoc_name(docName);
			           
			            docdetailsbean.setDoc_size(docsizeKB);
			            docdetailsbean.setDoc_type("Financial_DOC");
			            docdetailsbean.setKyc_ind_id(i);
			            docdetailsbean.setDes(request.getParameter("des"));
			                                                           
			            StringBuilder docsdatas = new StringBuilder();
			          
			            	docsdatas.append(request.getParameter("fromYear")+"-"+request.getParameter("toYear")+"&&");
			                docsdatas.append(docsHalfData);
			                               
			                Date dateNow = new Date();
			                SimpleDateFormat dateformatJava = new SimpleDateFormat("dd-MM-yyyy");
			                String attacheddate = dateformatJava.format(dateNow);
			               
			                docsdatas.append("&&"+attacheddate);
			                docsdatas.append("&&"+docName);
			                		
			                docdetailsbean.setDocs_data(docsdatas.toString());
			            
			                // Logger File  for current user's IpAddress and Mac Address with Name Date and Time
							
							ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
							String individualDirectory=loggerFileDirectory.getString("loggerDirectoryIndividual");

							
							String DATE_FORMAT_NOW = "dd-MM-yyyy";
							Date date = new Date();
							SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
							String stringDate = sdf.format(date );
							stringDate += "-FinancialLogger";
							   
			                loggerfile = new FileAppender(new SimpleLayout(),""+individualDirectory+"/"+stringDate+".log",false);
							
						    log.addAppender(loggerfile);
						    loggerfile.setLayout(new SimpleLayout());
						    
						    log.info("Financial details is ready to save.");
						  
						    // Logger Config who has Log in	
						 
						    
			            
			    		DocumentDetailModel docdetailmodel = prepareDocDetailModel(docdetailsbean);
			    		
			    		if ("success".equals(docservice.uploadotherDocumentDetail(docdetailmodel))) {
			                
			    			
			    			String Kyc_id= (String) session.getAttribute("Kyc_id");
			    			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			    			Profileupdatemodel.setProfileupdatedetail(finName+" has been updated under financial documents.");
			    			Profileupdatemodel.setProfileupdatetime(new Date());
			    			Profileupdatemodel.setType_of_alert("doc update");
			    			Profileupdatemodel.setKyc_id(Kyc_id);
			    			profileinfoService.profileupdate(Profileupdatemodel); 
			    			
			    			return "redirect:/search_financialDoc.html";
			    			
			    		} else {
			
			    			log.error("Financial details is not saved check on Clensee-All-Debug-Report.");
			    			
			    			return "failure";
			    		}   
			    		   				
    	}
        
        
        
        @RequestMapping(value="/viewfinancialdetails", method = RequestMethod.GET)
        public String getFinancialDetails(HttpServletRequest request,HttpSession session)
        {  
        	session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return "individualPage";	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return  "individualPage";	
			}
			else
			{					
		        	String docName = request.getParameter("docName");
		        	int i =Integer.parseInt((String) session.getAttribute("Ind_id"));
		        	
		        	String docsData = docservice.getFinancialDataviaDocName(i,docName);
		        	
		        	request.setAttribute("docsData",docsData);
		        	request.setAttribute("indId",i);
		        	
		        	String financial= docservice.getFinancialComp();
		        	
		        	request.setAttribute("finHierarchy", financial);
		        	
		        	int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
		        	request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
		
		        	
		        	return "individual_view_financialdocuments";
			}
        }
        
        @RequestMapping(value="/financialhistorydetails", method = RequestMethod.GET)
        public ModelAndView getFinancialHistory(HttpServletRequest request,HttpSession session)
        {  
            session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("individualPage");	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return  new ModelAndView("individualPage");	
			}
			else
			{	
	        	//String docName[] = request.getParameter("docName").split("_");
	        	String docName[] = request.getParameter("docName").split("_");
	        	int i =Integer.parseInt((String)session.getAttribute("Ind_id"));
	        	       	
	        	Map<String, Object> model = new HashMap<String, Object>();
	    		model.put("financialhistory", prepareDocumentDetailBean(docservice.getFinancialHistory(i,docName[0])));
	    		    		
	    		int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
	    		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
	
	    		request.setAttribute("yeardiff",request.getParameter("yeardiff"));
	    		request.setAttribute("docName",request.getParameter("docName"));
	    		
	    		session.setAttribute("yeardiff",request.getParameter("yeardiff"));
	    		
	        	return new ModelAndView("individual_financial_historydetails",model);
			}
        }
            
        @RequestMapping(value="/financialHistoryView" ,method = RequestMethod.POST)
        public ModelAndView financialHistoryViews(HttpServletRequest request,HttpSession session)
        {
             session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("individualPage");	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return  new ModelAndView("individualPage");	
			}
			else
			{	
		        	String docName = request.getParameter("docName");
		        	int i =Integer.parseInt((String) session.getAttribute("Ind_id"));        	
		        	String docsData = docservice.getFinancialDataviaDocName(i,docName);
		       	    
		        	request.setAttribute("docsData",docsData);
		        	request.setAttribute("indId",i);        	
		        	String financial= docservice.getFinancialComp();       	
		        	request.setAttribute("finHierarchy", financial); 
		        	
		        	return new ModelAndView("financialHistoryView");
			}
        }
        
        
        @RequestMapping(value="/financialHistoryView" ,method = RequestMethod.GET)
        public ModelAndView financialHistoryView(HttpServletRequest request,HttpSession session)
        {
        	session = request.getSession(false);
 			
 			if (session.getAttribute("Ind_id") == null)
 			{			
 				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
 				 request.setAttribute("status","Your session has expired please login to use the portal !");
 				
 				 session.removeAttribute("Ind_id");
 				 //session.invalidate();
 				 
 				 return new ModelAndView("individualPage");	
 			}
 			else if(((String)session.getAttribute("Ind_id")).equals(null))
 			{
 				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
 				 request.setAttribute("status","Your session has expired please login to use the portal !");
 				
 				 session.removeAttribute("Ind_id");
 				 //session.invalidate();
 				 
 				 return  new ModelAndView("individualPage");	
 			}
 			else
 			{			 			
		        	String docName = request.getParameter("docName");
		        	int i =Integer.parseInt((String) session.getAttribute("Ind_id"));        	
		        	String docsData = docservice.getFinancialDataviaDocName(i,docName);
		       	    
		        	request.setAttribute("docsData",docsData);
		        	request.setAttribute("indId",i);        	
		        	String financial= docservice.getFinancialComp();       	
		        	request.setAttribute("finHierarchy", financial); 
		        	
		        	int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
		        	request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
		       	
		        	return new ModelAndView("individual_financial_viewdetails");
 			}
        }
        
        
        @RequestMapping(value="/backfinancial" ,method = RequestMethod.POST)
        public ModelAndView backHistoryfinancial(HttpServletRequest request,HttpSession session,DocumentDetailModel docdetailmodel) throws IOException
        {           	
        	String docName[] = request.getParameter("docName").split("_");
        	int i =Integer.parseInt((String) session.getAttribute("Ind_id"));
        	
        	Map<String, Object> model = new HashMap<String, Object>();
    		model.put("financialhistory", prepareDocumentDetailBean(docservice.getFinancialHistory(i,docName[0])));   		        	
        	
    		
    		return new ModelAndView("financial_history",model);   
        	
        }
        
        
        @RequestMapping(value="/center_body31", method = RequestMethod.GET)
        public ModelAndView getFinancialBody(HttpSession session ,DocumentDetailModel docdetailmodel) {
    		
    		String financial= docservice.getFinancialComp();    		
    		docdetailmodel.setKyc_ind_id(Integer.parseInt((String) session.getAttribute("Ind_id")));    		
            String financialview = docservice.getDocsDataFinancial(docdetailmodel);
    	    
            if("value Not Found".equals(financialview))
            {
            	session.setAttribute("financial", financial);          
            }            
            else
            {
            	session.setAttribute("financialview", financialview);
            	docdetailmodel.setDoc_type("financial");
            	String findocname = docservice.getfinDocName(docdetailmodel);            	
            	session.setAttribute("findocname", findocname);            	
            }
            
    		return new ModelAndView("redirect:/center_body3.html");    		
    	}
                             
        @RequestMapping(value = "/fin_historydetail", method = RequestMethod.POST)
    	public ModelAndView GetFinancialView(HttpServletRequest request,HttpSession session) {
    		       	
        	DocumentDetailModel docdetails = new DocumentDetailModel();
        	
        	docdetails.setDoc_name((String)request.getParameter("id"));
        	
        	ArrayList<Integer> doclist = (ArrayList<Integer>)session.getAttribute("docidlist");
        	        	
        	String typeUrl=request.getParameter("typeUrl");
        	
        	if(typeUrl.equals("dtlsIterator")) {

            	docdetails.setDoc_id(doclist.get(Integer.parseInt((String)request.getParameter("idIndex"))));	
        	} else {

            	docdetails.setDoc_id(Integer.parseInt((String)request.getParameter("idIndex")));
        	}
        	
    		Map<String, String> model = new HashMap<String, String>();
    		
    		model.put("viewdetails", docservice.getViewMore(docdetails));
    		
    	    session.setAttribute("docname", docservice.getDocName(docdetails));
    	    
    	    session.setAttribute("user", Integer.parseInt((String)session.getAttribute("Ind_id")));
    	       	      	    
    		return new ModelAndView("historydetailfinancial",model);
    	} 
        
                        
        @RequestMapping(value = "/center_body41", method = RequestMethod.GET)
        public ModelAndView getEmployementBody(HttpSession session ,DocumentDetailModel docdetailmodel) {
    		
    		//String employement= docservice.getEmployementComp();
    		
    		docdetailmodel.setKyc_ind_id(Integer.parseInt((String) session.getAttribute("Ind_id")));
           
    		String employementviewornew = docservice.getDocsDataEmployement(docdetailmodel);
    	        		
    		session.setAttribute("employementviewornew", employementviewornew);
                      
    		return new ModelAndView("redirect:/center_body4.html");
    		
    	}
    	        
                                                         
        @RequestMapping(value = "/center_body51", method = RequestMethod.POST)
        public ModelAndView getBusinessBody(HttpSession session ,DocumentDetailModel docdetailmodel) {
    		
    		String business= docservice.getBusinessComp();
    		
    		docdetailmodel.setKyc_ind_id(Integer.parseInt((String) session.getAttribute("Ind_id")));
            String businessview = docservice.getDocsDataBusiness(docdetailmodel);
    	    
            if("value Not Found".equals(businessview))
            {
            	session.setAttribute("business", business);
            }
            else
            {
            	session.setAttribute("businessview", businessview);
            	System.out.println("ok");
            }
            
    		return new ModelAndView("redirect:/center_body5.html");
    		
    	}
        
                    		       
        @RequestMapping(value = "/history1", method = RequestMethod.GET)
    	public ModelAndView GetDocHistory(HttpServletRequest request,HttpSession session) {
    		
        	DocumentDetailModel docdetails = new DocumentDetailModel();
        	docdetails.setKyc_ind_id(Integer.parseInt((String) session.getAttribute("Ind_id")));
    		    		
    		docdetails.setDoc_name((String)request.getParameter("id"));
    		
    		Map<String, Object> model = new HashMap<String, Object>();
    		model.put("Historydetails", prepareDocumentDetailBean(docservice.getHistory(docdetails,session)));
    	
    	
    		return new ModelAndView("history_DOC_detail",model);
    	}
        
        private List<DocumentDetailsBean> prepareDocumentDetailBean(List<DocumentDetailModel> docdetails) {
        	
           	
        	List<DocumentDetailsBean> bean = null;
        	
		if (docdetails != null && !docdetails.isEmpty()) {
			bean = new ArrayList<DocumentDetailsBean>();
			DocumentDetailsBean beans = null;

			for (DocumentDetailModel docdetail : docdetails) {
                beans = new DocumentDetailsBean();
				beans.setDoc_id(docdetail.getDoc_id());
				beans.setDoc_name(docdetail.getDoc_name());
				beans.setCr_date(docdetail.getCr_date());
				beans.setDes(docdetail.getDes());
				beans.setDoc_des(docdetail.getDoc_des());
				beans.setDocs_data(docdetail.getDocs_data());
				bean.add(beans);
			}
		}
    		return bean;
    	} 
                     
       
         
        @RequestMapping(value = "/employeeDetails", method = RequestMethod.GET)
    	public ModelAndView getEmployeeDetail(HttpServletRequest request,HttpSession session) {
    		
        	 session = request.getSession(false);
 			
 			if (session.getAttribute("Ind_id") == null)
 			{			
 				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
 				 request.setAttribute("status","Your session has expired please login to use the portal !");
 				
 				 session.removeAttribute("Ind_id");
 				 //session.invalidate();
 				 
 				 return new ModelAndView("individualPage");	
 			}
 			else if(((String)session.getAttribute("Ind_id")).equals(null))
 			{
 				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
 				 request.setAttribute("status","Your session has expired please login to use the portal !");
 				
 				 session.removeAttribute("Ind_id");
 				 //session.invalidate();
 				 
 				 return  new ModelAndView("individualPage");	
 			}
 			else
 			{			 				
		 			
		        	int i =Integer.parseInt((String)session.getAttribute("Ind_id"));
		        	
		        	String panel = docservice.getEmpPanelStatus(i);
		        	System.out.println("All the Panels Names are="+panel);
		        	request.setAttribute("panel",panel);
		        	
		        	String employeeDetails = docservice.getEmpDetails(panel,i);
		        	request.setAttribute("employeeDetails",employeeDetails);
		        	 
		        	int year = Calendar.getInstance().get(Calendar.YEAR);
		    		request.setAttribute("year", year);
		    		
		    		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		    		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
		    			

		    		return new ModelAndView("individual_employee_details");
		    		
 			}
    	} 
        
        
        public String availabilityStatus(String fromDate,String givenDate,String toDate) throws ParseException
        {
        	String status = "";
        	
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");

    	    String oeStartDateStr = fromDate;
    	    String oeEndDateStr = toDate;
    	   
    	    Date startDate = sdf.parse(oeStartDateStr);
    	    Date endDate = sdf.parse(oeEndDateStr);        
    	    Date d = sdf.parse(givenDate);    	    
    	    String currDt = sdf.format(d);

    	    
    	    if((d.after(startDate) && (d.before(endDate))) || (currDt.equals(sdf.format(startDate)) ||currDt.equals(sdf.format(endDate))))
    	    {
    	        System.out.println("Date is between "+oeStartDateStr+" to "+givenDate+" to  "+oeEndDateStr+"...");
    	        status = "Available";
    	    }
    	    else
    	    {
    	        System.out.println("Date is not between "+oeStartDateStr+" to "+givenDate+" to  "+oeEndDateStr+"...");
    	        status = "Not Available";
    	    }
    	    
    	    return status;
        }
        
        public String getLatestJobDuration()
        {
        	String status = "";
        	String previousData = "";    
        	
        	int individualId =Integer.parseInt((String)Ind_id);       	
        	String panel1 = docservice.getEmpPanelStatus(individualId);     
        	
        	if(!panel1.equals(""))
        	{
        		String latestDocsDataofEachCompany = docservice.getEmployeeDetailsEachCompnay(panel1,individualId);
            	String eachRecord[] = latestDocsDataofEachCompany.split("##");
            	
            	for(int i=0;i<eachRecord.length;i++)
            	{
            		String seperatedRecord[] = eachRecord[i].split("&&");           		
            	}
        	}       	
        	return status;
        }
        
        
        
        @RequestMapping(value = "/validateJobduration", method = RequestMethod.POST)
    	public @ResponseBody String getvalidateJobduration(HttpServletRequest request,HttpSession session) throws ParseException
        {
        	String status = "";
        	String previousData = "";    
        	
        	String companyName = request.getParameter("cmyName");
        	
        	int individualId =Integer.parseInt((String)session.getAttribute("Ind_id"));       	
        	String panel1 = docservice.getEmpPanelStatus(individualId);    
        	
        	String employeeDetails = docservice.getEmpDetails(panel1,individualId);  //  All the Employer docs_data with doc_names will capture        	
        	
        	String employeeDetailsExceptThisCompany = docservice.getEmpDetailsExceptThisCompany(panel1,individualId,companyName);  //  All the Employer docs_data with doc_names will capture        	
        	
        	String durationArray1[] = employeeDetailsExceptThisCompany.split("#");   
        	
        	//  All the Employer docs_data with doc_names will separated By '#'
        	
        	for(int c=0;c<durationArray1.length;c++)
        	{
        		String str1[] = durationArray1[c].split("@@@");                     // Each docs_data and doc_names separated By @@@
        		
        		String str2[] = str1[0].split("&&");					            // 1st index contain docs_data separated By &&
        		
        		if(!str2[3].equals("No Data"))
        		{
        			previousData +=  str2[3]+"-"+str2[4] + "#";                     // Index 3rd and 4th contains job Duration of All Employer Information
        		}     		       		
        	}
        	
        	if(!previousData.equals(""))
        	{
        		previousData = previousData.substring(0, previousData.length()-1);       		
        		String DateArray[] = previousData.split("#");
        		       		       		
        		for(int d=0;d<DateArray.length;d++)
        		{
        			String eachDateArray[] = DateArray[d].split("-");
        			String dateString = null;
    				SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yy");       
    				
        			if(eachDateArray[1].equals("Presently Working"))
        			{      				 				   
        				try
        				{
        					dateString = sdfr.format(new Date());
        					
        				}
        				catch (Exception ex )
        				{
        					System.out.println(ex);
        				}
        				        				    				
        				status = availabilityStatus(eachDateArray[0],request.getParameter("checkDate"),dateString);
        				if(status.equals("Available"))
        				{
        					status = "Available";
        					break;
        				}
        			}
        			
        			else
        			{
        				String checkDate = request.getParameter("checkDate");
        				
        				if(checkDate.equals("Presently Working"))
        				{
        					try
            				{
        						dateString = sdfr.format(new Date());
            					
            				}
            				catch (Exception ex )
            				{
            					System.out.println(ex);
            				}
            				
            				status = availabilityStatus(eachDateArray[0],dateString,eachDateArray[1]);
            				if(status.equals("Available"))
            				{
            					status = "Available";
            					break;
            				}
        				}
        				else
        				{
        					status = availabilityStatus(eachDateArray[0],checkDate,eachDateArray[1]);
            				if(status.equals("Available"))
            				{
            					status = "Available";
            					break;
            				}
        				}
        				
        			}
        			
        		}
        	}
        	else
        	{
        		status = "Not Available";
        	}
        	
        	
        	return status;
        }
        
        
        
        @RequestMapping(value = "/validateJobdurationForToDate", method = RequestMethod.POST)
    	public @ResponseBody String getvalidateJobdurationForToDate(HttpServletRequest request,HttpSession session) throws ParseException
        {
        	String status = "";
        	String previousData = "";    
        	
        	String fromDate = request.getParameter("fromDate");
        	String toDate = request.getParameter("toDate");
        	String companyName = request.getParameter("companyName"); 
        	
        	int individualId =Integer.parseInt((String)session.getAttribute("Ind_id"));       	
        	String panel1 = docservice.getEmpPanelStatus(individualId);       	        	
        	String employeeDetails = docservice.getEmpDetails(panel1,individualId);  //  All the Employer docs_data with doc_names will capture        	
        	
        	String employeeDetailsExceptThisCompany = docservice.getEmpDetailsExceptThisCompany(panel1,individualId,companyName);  //  All the Employer docs_data with doc_names will capture        	        	
        	
        	String durationArray1[] = employeeDetailsExceptThisCompany.split("#");                   //  All the Employer docs_data with doc_names will separated By '#'
        	
        	for(int c=0;c<durationArray1.length;c++)
        	{
        		String str1[] = durationArray1[c].split("@@@");                     // Each docs_data and doc_names separated By @@@
        		
        		String str2[] = str1[0].split("&&");					            // 1st index contain docs_data separated By &&
        		
        		if(!str2[3].equals("No Data"))
        		{
        			previousData +=  str2[3]+"-"+str2[4] + "#";                     // Index 3rd and 4th contains job Duration of All Employer Information
        		}     		       		
        	}
        	
        	
        	String fromDateArray[] = fromDate.split("/");
        	
        	int getToYear = 0;
        	int getToMonth = 0;
        	
        	String toDateString = null;
        	if(toDate.equals("Presently Working"))
        	{
        		
        		SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yy");        				   
				try
				{
					toDateString = sdfr.format(new Date());
					System.out.println("As per the Date"+toDateString);
				}
				catch (Exception ex )
				{
					System.out.println(ex);
				}
				
				String toDateArray[] = toDateString.split("/");
				
				getToMonth = Integer.parseInt(toDateArray[1]);
				getToYear = Integer.parseInt(toDateArray[2]);
				
        	}       	
        	else
        	{
        		String toDateArray[] = toDate.split("/");
        		getToMonth = Integer.parseInt(toDateArray[1]);
        		getToYear = Integer.parseInt(toDateArray[2]);
        	}
        	
        	int fromDay = Integer.parseInt(fromDateArray[0]);
        	int fromMonth = Integer.parseInt(fromDateArray[1]);
        	int fromYear = Integer.parseInt(fromDateArray[2]);
        	
        	int toYear = getToYear;
        	
        	      	
        	if(!previousData.equals(""))
        	{
        		previousData = previousData.substring(0, previousData.length()-1);       		
        		String DateArray[] = previousData.split("#");
        		       		       		
        		for(int d=0;d<DateArray.length;d++)
        		{
        			String eachDateArray[] = DateArray[d].split("-");
        			
        			if(eachDateArray[1].equals("Presently Working"))
        			{
        				String dateString = null;
        				SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yy");        				   
        				try
        				{
        					dateString = sdfr.format(new Date());
        					System.out.println("As per the Date"+dateString);
        				}
        				catch (Exception ex )
        				{
        					System.out.println(ex);
        				}
        				
        				if(toDate.equals("Presently Working"))
        	        	{
        					status = availabilityStatus(eachDateArray[0],toDateString,dateString);
        	        	}
        				else
        				{
        					status = availabilityStatus(eachDateArray[0],toDate,dateString);
        				}
        				
        				
        				if(status.equals("Available"))
        				{
        					status = "Available";
        					break;
        				}
        			}       			
        			else
        			{        				
        				if(toDate.equals("Presently Working"))
        	        	{
        					status = availabilityStatus(eachDateArray[0],toDateString,eachDateArray[1]);
        	        	}
        				else
        				{
        					status = availabilityStatus(eachDateArray[0],toDate,eachDateArray[1]);
        				}
        				
        				if(status.equals("Available"))
        				{
        					status = "Available";       					
        					break;
        				}
        			}        			
        		}        		
        		
        		
        		if(!status.equals("Available"))
        		{
        			for(int d=0;d<DateArray.length;d++)
            		{
                          String eachDateArray[] = DateArray[d].split("-");
            			
            			if(eachDateArray[1].equals("Presently Working"))
            			{
            				String dateString = null;
            				SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yy");        				   
            				try
            				{
            					dateString = sdfr.format(new Date());     					
            				}
            				catch (Exception ex)
            				{
            					System.out.println(ex);
            				}
            				
            				for(int y=fromYear;y<=toYear;y++)
            				{
            					if(y==toYear)
            					{
            						for(int m=fromMonth;m<=12;m++)
                    				{
                						if(m<getToMonth)
                						{
                							String givenDate = fromDay+"/"+m+"/"+y;
                    						
                							System.out.println("Given date1="+givenDate);
                							
                    						status = availabilityStatus(eachDateArray[0],givenDate,dateString);
                            				
                            				if(status.equals("Available"))
                            				{
                            					status = "Available";
                            					break;
                            				}
                						}           							                       				
                    				}
            					}
            					
            					if(y!=toYear && y<toYear)
            					{
            						if((toYear-fromYear)==1)
            						{
            							for(int m=fromMonth;m<=12;m++)
                        				{
                    						String givenDate = fromDay+"/"+m+"/"+y;
                    						
                    						System.out.println("Given date2="+givenDate);
                    						
                    						status = availabilityStatus(eachDateArray[0],givenDate,dateString);
                            				
                            				if(status.equals("Available"))
                            				{
                            					status = "Available";
                            					break;
                            				}
                        				}
            						}
            						else
            						{
            							for(int m=fromMonth;m<=12;m++)
                        				{
                    						String givenDate = fromDay+"/"+m+"/"+y;
                    						
                    						System.out.println("Given date2="+givenDate);
                    						
                    						status = availabilityStatus(eachDateArray[0],givenDate,dateString);
                            				
                            				if(status.equals("Available"))
                            				{
                            					status = "Available";
                            					break;
                            				}
                        				}
            						}
            						
            					} 
            					
            					if(status.equals("Available"))
                				{
                					status = "Available";
                					break;
                				}
            					
            				}
            				           				
            			}       			
            			else
            			{
            				for(int y=fromYear;y<=toYear;y++)
            				{
            					if(y==toYear)
            					{
            						for(int m=fromMonth;m<=12;m++)
                    				{            							
            							if(m<getToMonth)
            							{
            								String givenDate = fromDay+"/"+m+"/"+y;
            								
            								System.out.println("Given date3="+givenDate);
            								
                    						status = availabilityStatus(eachDateArray[0],givenDate,eachDateArray[1]);
                            				
                            				if(status.equals("Available"))
                            				{
                            					status = "Available";
                            					break;
                            				}
            							}
            							
                    				}
            					}
            					
            					if(y!=toYear && y<toYear)
            					{
            						if((toYear-fromYear)==1)
            						{
            							for(int m=fromMonth;m<=12;m++)
                        				{
                    						String givenDate = fromDay+"/"+m+"/"+y;
                    						
                    						System.out.println("Given date4="+givenDate);
                    						
                    						status = availabilityStatus(eachDateArray[0],givenDate,eachDateArray[1]);
                            				
                            				if(status.equals("Available"))
                            				{
                            					status = "Available";
                            					break;
                            				}
                        				}
            						}
            						else
            						{
            							for(int m=fromMonth;m<=12;m++)
                        				{
                    						String givenDate = fromDay+"/"+m+"/"+y;
                    						
                    						System.out.println("Given date4="+givenDate);
                    						
                    						status = availabilityStatus(eachDateArray[0],givenDate,eachDateArray[1]);
                            				
                            				if(status.equals("Available"))
                            				{
                            					status = "Available";
                            					break;
                            				}
                        				}
            					    }
            						
            					} 
            					if(status.equals("Available"))
                				{
                					status = "Available";
                					break;
                				}
            				}
            			}        
            		}
        		}
        		              		
        	}
        	else
        	{
        		status = "Not Available";
        	}
        	
        	
        	return status;
        }
        
        
        @RequestMapping(value = "/getPrevious/employment/duration", method = RequestMethod.POST)
    	public @ResponseBody String getPreviousEmploymentDuration(HttpServletRequest request,HttpSession session)
        {
    		String previousData = "";    
    		int individualId =Integer.parseInt((String)session.getAttribute("Ind_id"));       	
        	String panel1 = docservice.getEmpPanelStatus(individualId);       	        	
        	String employeeDetails = docservice.getEmpDetails(panel1,individualId);  //  All the Employer docs_data with doc_names will capture        	
        	
        	System.out.println("Employee's Captures data="+employeeDetails);
        	
        	String durationArray1[] = employeeDetails.split("#");                   //  All the Employer docs_data with doc_names will separated By '#'
        	
        	for(int c=0;c<durationArray1.length;c++)
        	{
        		String str1[] = durationArray1[c].split("@@@");                     // Each docs_data and doc_names separated By @@@
        		
        		String str2[] = str1[0].split("&&");					            // 1st index contain docs_data separated By &&
        		
        		if(!str2[3].equals("No Data"))
        		{
        			previousData +=  str2[3]+"-"+str2[4] + "#";                     // Index 3rd and 4th contains job Duration of All Employer Information
        		}     		       		
        	}
        	
        	if(!previousData.equals(""))
        	{
        		previousData = previousData.substring(0, previousData.length()-1);
        	}
        	
		    return previousData;		  
    	}
        
        
        @RequestMapping(value = "/checkorgnameexistornot", method = RequestMethod.POST)
    	public @ResponseBody String checkorgnameexistornot(HttpServletRequest request,HttpSession session) {
    	
        	String statusResult = "";
        	int ind_id =Integer.parseInt((String)session.getAttribute("Ind_id"));
        	String orgName = request.getParameter("orgName");
        	
        	statusResult += docservice.checkOrgNameExistOrNot(ind_id,orgName);
        	
        	return statusResult;
        	
        }
        
        @RequestMapping(value = "/checkLastEmployeeDetails", method = RequestMethod.POST)
    	public @ResponseBody String getcheckLastEmployeeDetails(HttpServletRequest request,HttpSession session) {
    	
        	String statusResult = "";
        	int ind_id =Integer.parseInt((String)session.getAttribute("Ind_id"));
        	
        	statusResult += docservice.employerStatus(ind_id);
        	
        	return statusResult;
        	
        }
       
       
        @RequestMapping(value = "/createEmployerName", method = RequestMethod.POST)
    	public ModelAndView getCreateEmployerName(HttpServletRequest request,HttpSession session) {
    		
        	String orgName = request.getParameter("cmyName");
        	
            int i =Integer.parseInt((String)session.getAttribute("Ind_id"));
            
            DocumentDetailsBean docdetailsbean = new DocumentDetailsBean();
           
            docdetailsbean.setDoc_name("No Document,No Document,No Document,No Document,No Document");           
            docdetailsbean.setDoc_type("Employment_DOC");
            docdetailsbean.setDocs_data(orgName+"&&No Data&&No Data&&No Data&&No Data");
            docdetailsbean.setKyc_ind_id(i);
            docdetailsbean.setCr_date(new Date());            
            
            String panel = docservice.getEmpPanelStatusAsofNow(i);
            
            if(!panel.equals(""))
            {
            	docdetailsbean.setPanel_status(orgName+"/"+panel);
            }
            else
            {
            	docdetailsbean.setPanel_status(orgName);
            }
                                                        
    		DocumentDetailModel docdetailmodel = prepareDocDetailModel(docdetailsbean);
    		
    		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
    		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
    		
    		if ("success".equals(docservice.uploadotherDocumentDetail(docdetailmodel))) {

    			
    			int i1 =Integer.parseInt((String)session.getAttribute("Ind_id"));
            	
            	String panel1 = docservice.getEmpPanelStatus(i1);
            	request.setAttribute("panel",panel1);
            	
            	String employeeDetails = docservice.getEmpDetails(panel1,i1);
            	
            	request.setAttribute("employeeDetails",employeeDetails);
    			
            	int year = Calendar.getInstance().get(Calendar.YEAR);
        		request.setAttribute("year", year);
            	
        		/* Tracking the Activity Details */
        		
        		String Kyc_id= (String) session.getAttribute("Kyc_id");
    			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
    			Profileupdatemodel.setProfileupdatedetail("Employer "+orgName+" has been added to your employer list.");
    			Profileupdatemodel.setProfileupdatetime(new Date());
    			Profileupdatemodel.setType_of_alert("doc update");
    			Profileupdatemodel.setKyc_id(Kyc_id);
    			Profileupdatemodel.setMore_about_it(request.getParameter("standardname")+" added.");
    			profileinfoService.profileupdate(Profileupdatemodel); 
    			
    			
    			/* End  of  Tracking the Activity Details */
    			
    			return new ModelAndView("individual_employee_details");
    			
    		}
    		else 
    		{    	
    			int year = Calendar.getInstance().get(Calendar.YEAR);
        		request.setAttribute("year", year);
        		
    			return new ModelAndView("individual_employee_details");
    		}   
    		   		
    	}
        
        @RequestMapping(value="/checkEmpEndDate",method=RequestMethod.POST)
        public @ResponseBody String checkEmpEndDate(HttpServletRequest request,HttpSession session)
        {
        	String status = "";
        	
        	int Individual_Id =Integer.parseInt((String)session.getAttribute("Ind_id"));       	
        	status = docservice.getEmpDataofAllCompany(Individual_Id);  
        	
        	return status;
        }
        
        @RequestMapping(value = "/editEmployeeDetail", method = RequestMethod.POST)
    	public ModelAndView getEditEmployeeDetail(HttpServletRequest request,HttpSession session,@RequestParam CommonsMultipartFile[] offletter,
    			@RequestParam CommonsMultipartFile[] hikeletter,@RequestParam CommonsMultipartFile[] expletter,
    			@RequestParam CommonsMultipartFile[] relletter,@RequestParam CommonsMultipartFile[] formletter    			
    		    ) throws Exception {
    		 
            
        	String orgName = request.getParameter("cmyName");
        	String jobDesignation = request.getParameter("jobDesignation");
        	String jobProfile = request.getParameter("jobPro");        	
        	String jobDurFrom = request.getParameter("jobfrom");
        	String jobDurTo = request.getParameter("jobto");
        	
        	/*String flName = orgName.split(" ")[0]+" Offer Letter_";       	
        	String flNamehike = orgName.split(" ")[0]+" Hike Letter_";
        	String flNameexp = orgName.split(" ")[0]+" Experience Letter_";
        	String flNamerel = orgName.split(" ")[0]+" Reliving Letter_";
        	String flNameform = orgName.split(" ")[0]+" Form 16_";*/
        	        	
        	String flName = orgName+" Offer Letter_";       	
        	String flNamehike = orgName+" Hike Letter_";
        	String flNameexp = orgName+" Experience Letter_";
        	String flNamerel = orgName+" Reliving Letter_";
        	String flNameform = orgName+" Form 16_";
        	
        	String user = (String)session.getAttribute("Ind_id");
        	String empDocName = "";
        	float datasizeKB1 = (float) 0.0;
    		    		
    		int Individual_Id =Integer.parseInt((String)session.getAttribute("Ind_id"));
    		 
    		File ourPath = new File(kycMainDirectory);
    		    		
    		String register_year = (String)session.getAttribute("created_year");
    		
    		
    		if (offletter != null && offletter.length > 0)
            {    			
    			File fld = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC");
            	if(!fld.exists())
            	{
        		fld.mkdirs();
            	}
        		String loc = ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC";
                
                for (CommonsMultipartFile aFile : offletter)            
                {             
                    if (!aFile.getOriginalFilename().equals(""))                 
                    {     
                    	String extension = "";
                        
                        int i = aFile.getOriginalFilename().lastIndexOf('.');
                        if (i > 0) {
                            extension = aFile.getOriginalFilename().substring(i+1);
                                   }                                        
                        aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
                        File datasize = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());
                        float bytes = datasize.length();
                        datasizeKB1 = datasizeKB1 + bytes/1024;   
                        
                        
                        File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC");
	                    File[] storelist = store.listFiles();
	                       
                        
                        
                           String flname = flName+"1";
	                       boolean check=true;
	                       
	                       for(int f=0;f<50;f++)
	                        { 
	                    	   
	                    	   if(check==true)
	                    	   {
	                        	  for(int g=0;g<storelist.length;g++)
	                        	   {                       		 
	                        	     if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
	                                   	{                   		      
	                        	    	    check=false;                    	    	    
	                        	        }  
	                        	   }
	                        	  
	                        	  if(check==true)
	                           	       {                         		   
	                           		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());                   
	                                      File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+flname+"."+extension);
	                                      floldname.renameTo(flnewname);                                     
	                                      empDocName=empDocName + "," +flnewname.getName();
	                                      	
		                                    // For backup the file uploading file
			                      	    	    
			                      	    		ResourceBundle addResource = ResourceBundle.getBundle("resources/kycFileDirectory");		
			                      	    		String fileBackUpDirectory = addResource.getString("fileBackUpDirectory");		
			                      	    		
			                      	    		File backUpFolder = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC");							                      	    		
			                                    
			                                    if(!backUpFolder.exists())
			                                    {
			                                    	backUpFolder.mkdirs();
			                                    }                        
			                                    File destinationFile = new File(fileBackUpDirectory+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC/"+flnewname.getName());
			                                    MakeBackUpForDocuments copyFileExample = new MakeBackUpForDocuments();
			                                    copyFileExample.copyFile(flnewname, destinationFile);
			                                    
			                      	    	// For backup the file uploading file	
		                                    
			                                      /* ThumbNail For Offer Latter Documents */
				                                  
					                                if(!extension.equals("pdf"))
					                                {
					                                	if(!extension.equals("doc"))
						                                {
					                                		if(!extension.equals("docx"))
							                                {
					                                			if(!extension.equals("xlsx"))
								                                {
					                                				if(!extension.equals("xls"))
									                                {
										                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC");
										                                	  thumbNailDirectory.mkdirs();
										                                	  BufferedImage img = ImageIO.read(flnewname); 
											                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
											                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
											                                  ImageIO.write(thumbImg,extension,os);
											                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC"+"/"+flname+"."+extension);  
											                                  ImageIO.write(thumbImg, extension, f2);	
									                                }
								                                }
							                                }
						                                }
					                                }
					                                 
					                                  
				                                  /* End of Thumbnail Creation */
			                                
	                                      break;          
	                           	       }             	                              	                     	                     	                    	  
	                    	   }
	                    	  
	                    	   if(check==false)
	                    	   {
	                    		   String[] ary=flname.split("_");
	                    		   
	                    		   for(int k=1;k<ary.length;k++)
	                    		   {  
	             	                  int l=Integer.parseInt(ary[k])+1;
	             	                  String s=Integer.toString(l);
	             	                  flname="";
	             	                  flname=""+flName+"".concat(s);
	             	                  check=true;
	                    		   }
	                    	   }                
  
	                    }
	                }
	            }
            }	
    		
    		for (CommonsMultipartFile aFile : offletter)            
            {             
                if (aFile.getOriginalFilename().equals(""))                 
                {
                	 String offletName = docservice.getEmpDocNames(Individual_Id,orgName);
                	 String offAry[] = offletName.split(",");
                	 
                	 String offletterName = offAry[0];
                	 
                	 if(!offletterName.equals("No Document"))
                	 {
                		 empDocName = empDocName + "," + offletterName;
                	 }
                	 else
                	 {
    			         empDocName = empDocName + "," + "No Document";
                	 }
    		    }
            }
    		
    		
    		
    		if (hikeletter != null && hikeletter.length > 0)
            {    			
    			File fld = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC");
            	if(!fld.exists())
            	{
        		fld.mkdirs();
            	}
        		String loc=ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC";
                
                for (CommonsMultipartFile aFile : hikeletter)            
                {             
                    if (!aFile.getOriginalFilename().equals(""))                 
                    {     
                    	String extension = "";
                        
                        int i = aFile.getOriginalFilename().lastIndexOf('.');
                        if (i > 0) {
                            extension = aFile.getOriginalFilename().substring(i+1);
                                   }                                        
                        aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
                        File datasize = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());
                        float bytes = datasize.length();
                        datasizeKB1 = datasizeKB1 + bytes/1024;   
                        
                        
                        File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC");
	                    File[] storelist = store.listFiles();
	                       
                        
                        
                           String flname = flNamehike+"1";
	                       boolean check=true;
	                       
	                       for(int f=0;f<50;f++)
	                        { 
	                    	   
	                    	   if(check==true)
	                    	   {
	                        	  for(int g=0;g<storelist.length;g++)
	                        	   {                       		 
	                        	     if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
	                                   	{                   		      
	                        	    	    check=false;                    	    	    
	                        	        }  
	                        	   }
	                        	  
	                        	  if(check==true)
	                           	       {                         		   
	                           		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());                   
	                                      File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+flname+"."+extension);
	                                      floldname.renameTo(flnewname);                                     
	                                      empDocName=empDocName + "," +flnewname.getName();
	                                      	           
			                                      /* ThumbNail For Hike  Documents */
				                                  
					                                if(!extension.equals("pdf"))
					                                {
					                                	if(!extension.equals("doc"))
						                                {
					                                		if(!extension.equals("docx"))
							                                {
					                                			if(!extension.equals("xlsx"))
								                                {
					                                				if(!extension.equals("xls"))
									                                {
										                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC");
										                                	  thumbNailDirectory.mkdirs();
										                                	  BufferedImage img = ImageIO.read(flnewname); 
											                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
											                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
											                                  ImageIO.write(thumbImg,extension,os);
											                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC"+"/"+flname+"."+extension);  
											                                  ImageIO.write(thumbImg, extension, f2);	
									                                }
								                                }
							                                }
						                                }
					                                }
					                                 
					                                  
				                                  /* End of Thumbnail Creation */
					                                
	                                      break;          
	                           	       }             	                              	                     	                     	                    	  
	                    	   }
	                    	  
	                    	   if(check==false)
	                    	   {
	                    		   String[] ary=flname.split("_");
	                    		   
	                    		   for(int k=1;k<ary.length;k++)
	                    		   {  
	             	                  int l=Integer.parseInt(ary[k])+1;
	             	                  String s=Integer.toString(l);
	             	                  flname="";
	             	                  flname=""+flName+"".concat(s);
	             	                  check=true;
	                    		   }
	                    	   }                
	                    }
                        
                       
                    }
                }
            } 
    		
    		for (CommonsMultipartFile aFile : hikeletter)            
            {             
                if (aFile.getOriginalFilename().equals(""))                 
                {
                	 String hikeletName = docservice.getEmpDocNames(Individual_Id,orgName);                	 
                	 String hikeAry[] = hikeletName.split(",");               	 
                	 if(!hikeAry[1].equals("No Document"))
                	 {
                		 empDocName = empDocName + "," + hikeAry[1];
                	 }
                	 else
                	 {
    			         empDocName = empDocName + "," + "No Document";
                	 }
    		    }
            }
    		
    		
    		if (expletter != null && expletter.length > 0)
            {    			
    			File fld = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC");
            	if(!fld.exists())
            	{
        		fld.mkdirs();
            	}
            	
        		String loc = ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC";
                
                for (CommonsMultipartFile aFile : expletter)            
                {             
                    if (!aFile.getOriginalFilename().equals(""))                 
                    {     
                    	String extension = "";
                        
                        int i = aFile.getOriginalFilename().lastIndexOf('.');
                        if (i > 0) {
                            extension = aFile.getOriginalFilename().substring(i+1);
                                   }                                        
                        aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
                        File datasize = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());
                        float bytes = datasize.length();
                        datasizeKB1 = datasizeKB1 + bytes/1024;   
                        
                        
                        
                        

                        File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC");
	                    File[] storelist = store.listFiles();
	                       
                        
                        
                           String flname = flNameexp+"1";
	                       boolean check=true;
	                       
	                       for(int f=0;f<50;f++)
	                        { 
	                    	   
	                    	   if(check==true)
	                    	   {
	                        	  for(int g=0;g<storelist.length;g++)
	                        	   {                       		 
	                        	     if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
	                                   	{                   		      
	                        	    	    check=false;                    	    	    
	                        	        }  
	                        	   }
	                        	  
	                        	  if(check==true)
	                           	       {                         		   
	                           		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());                   
	                                      File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+flname+"."+extension);
	                                      floldname.renameTo(flnewname);                                     
	                                      empDocName=empDocName + "," +flnewname.getName();
	                                      	              
	                                      /* ThumbNail For Experience Documents */
		                                  
			                                if(!extension.equals("pdf"))
			                                {
			                                	if(!extension.equals("doc"))
				                                {
			                                		if(!extension.equals("docx"))
					                                {
			                                			if(!extension.equals("xlsx"))
						                                {
			                                				if(!extension.equals("xls"))
							                                {
								                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC");
								                                	  thumbNailDirectory.mkdirs();
								                                	  BufferedImage img = ImageIO.read(flnewname); 
									                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
									                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
									                                  ImageIO.write(thumbImg,extension,os);
									                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC"+"/"+flname+"."+extension);  
									                                  ImageIO.write(thumbImg, extension, f2);	
							                                }
						                                }
					                                }
				                                }
			                                }
			                                 
			                                  
		                                  /* End of Thumbnail Creation */
	                                      break;          
	                           	       }             	                              	                     	                     	                    	  
	                    	   }
	                    	  
	                    	   if(check==false)
	                    	   {
	                    		   String[] ary=flname.split("_");
	                    		   
	                    		   for(int k=1;k<ary.length;k++)
	                    		   {  
	             	                  int l=Integer.parseInt(ary[k])+1;
	             	                  String s=Integer.toString(l);
	             	                  flname="";
	             	                  flname=""+flName+"".concat(s);
	             	                  check=true;
	                    		   }
	                    	   }                
	                    }
                        
                       
                    }
                }
            }
    		
    		for (CommonsMultipartFile aFile : expletter)            
            {             
                if (aFile.getOriginalFilename().equals(""))                 
                {
                	String expletName = docservice.getEmpDocNames(Individual_Id,orgName);
               	    String expAry[] = expletName.split(",");
               	 
               	    if(!expAry[2].equals("No Document"))
               	      {
    			          empDocName = empDocName + "," + expAry[2];
               	      }
               	    
               	    else
               	      {
               	    	 empDocName = empDocName + "," + "No Document";
               	      }
    		    }
            }
    		
    		
    		if (relletter != null && relletter.length > 0)
            {    			
    			File fld = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC");
            	if(!fld.exists())
            	{
        		fld.mkdirs();
            	}
        		String loc = ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC";
                
                for (CommonsMultipartFile aFile : relletter)            
                {             
                    if (!aFile.getOriginalFilename().equals(""))                 
                    {     
                    	String extension = "";
                        
                        int i = aFile.getOriginalFilename().lastIndexOf('.');
                        if (i > 0)
                        {
                            extension = aFile.getOriginalFilename().substring(i+1);
                        }                                        
                        aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
                        File datasize = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());
                        float bytes = datasize.length();
                        datasizeKB1 = datasizeKB1 + bytes/1024;   
                        

                        File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC");
	                    File[] storelist = store.listFiles();
	                       
                        
                        
                           String flname = flNamerel+"1";
	                       boolean check=true;
	                       
	                       for(int f=0;f<50;f++)
	                        { 
	                    	   
	                    	   if(check==true)
	                    	   {
	                        	  for(int g=0;g<storelist.length;g++)
	                        	   {                       		 
	                        	     if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
	                                   	{                   		      
	                        	    	    check=false;                    	    	    
	                        	        }  
	                        	   }
	                        	  
	                        	  if(check==true)
	                           	       {                         		   
	                           		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());                   
	                                      File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+flname+"."+extension);
	                                      floldname.renameTo(flnewname);                                     
	                                      empDocName=empDocName + "," +flnewname.getName();
	                                      	             
	                                      /* ThumbNail For Reliving Documents */
		                                  
			                                if(!extension.equals("pdf"))
			                                {
			                                	if(!extension.equals("doc"))
				                                {
			                                		if(!extension.equals("docx"))
					                                {
			                                			if(!extension.equals("xlsx"))
						                                {
			                                				if(!extension.equals("xls"))
							                                {
								                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC");
								                                	  thumbNailDirectory.mkdirs();
								                                	  BufferedImage img = ImageIO.read(flnewname); 
									                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
									                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
									                                  ImageIO.write(thumbImg,extension,os);
									                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC"+"/"+flname+"."+extension);  
									                                  ImageIO.write(thumbImg, extension, f2);	
							                                }
						                                }
					                                }
				                                }
			                                }
			                                 
			                                  
		                                  /* End of Thumbnail Creation */
			                                
	                                      break;          
	                           	       }             	                              	                     	                     	                    	  
	                    	   }
	                    	  
	                    	   if(check==false)
	                    	   {
	                    		   String[] ary=flname.split("_");
	                    		   
	                    		   for(int k=1;k<ary.length;k++)
	                    		   {  
	             	                  int l=Integer.parseInt(ary[k])+1;
	             	                  String s=Integer.toString(l);
	             	                  flname="";
	             	                  flname=""+flName+"".concat(s);
	             	                  check=true;
	                    		   }
	                    	   }                
	                    }
	                       
                      
                    }
                }
            }  
    		
    		for (CommonsMultipartFile aFile : relletter)            
            {             
                if (aFile.getOriginalFilename().equals(""))                 
                {
                	String relletName = docservice.getEmpDocNames(Individual_Id,orgName);
               	    String relAry[] = relletName.split(",");
               	 
               	    if(!relAry[3].equals("No Document"))
               	      {
    			          empDocName = empDocName + "," + relAry[3];
               	      }
               	    
               	    else
               	      {
               	    	 empDocName = empDocName + "," + "No Document";
               	      }
    		    }
            }
    		
    		if (formletter != null && formletter.length > 0)
            {    			
    			File fld = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC");
            	if(!fld.exists())
            	{
        		fld.mkdirs();
            	}
        		String loc=ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC";
                
                for (CommonsMultipartFile aFile : formletter)            
                {             
                    if (!aFile.getOriginalFilename().equals(""))                 
                    {     
                    	String extension = "";
                        
                        int i = aFile.getOriginalFilename().lastIndexOf('.');
                        if (i > 0) {
                            extension = aFile.getOriginalFilename().substring(i+1);
                                   }                                        
                        aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
                        File datasize = new File(ourPath.getPath()+"/"+register_year+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());
                        float bytes = datasize.length();
                        datasizeKB1 = datasizeKB1 + bytes/1024;   
                        

                        File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC");
	                    File[] storelist = store.listFiles();
	                       
                        
                        
                           String flname = flNameform+"1";
	                       boolean check=true;
	                       
	                       for(int f=0;f<50;f++)
	                        { 
	                    	   
	                    	   if(check==true)
	                    	   {
	                        	  for(int g=0;g<storelist.length;g++)
	                        	   {                       		 
	                        	     if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
	                                   	{                   		      
	                        	    	    check=false;                    	    	    
	                        	        }  
	                        	   }
	                        	  
	                        	  if(check==true)
	                           	       {                         		   
	                           		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());                   
	                                      File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+flname+"."+extension);
	                                      floldname.renameTo(flnewname);                                     
	                                      empDocName=empDocName + "," +flnewname.getName();
	                                      	                          
	                                      /* ThumbNail For Form-16 Documents */
		                                  
			                                if(!extension.equals("pdf"))
			                                {
			                                	if(!extension.equals("doc"))
				                                {
			                                		if(!extension.equals("docx"))
					                                {
			                                			if(!extension.equals("xlsx"))
						                                {
			                                				if(!extension.equals("xls"))
							                                {
								                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC");
								                                	  thumbNailDirectory.mkdirs();
								                                	  BufferedImage img = ImageIO.read(flnewname); 
									                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
									                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
									                                  ImageIO.write(thumbImg,extension,os);
									                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC"+"/"+flname+"."+extension);  
									                                  ImageIO.write(thumbImg, extension, f2);	
							                                }
						                                }
					                                }
				                                }
			                                }
			                                 
			                                  
		                                  /* End of Thumbnail Creation */
	                                      break;          
	                           	       }             	                              	                     	                     	                    	  
	                    	   }
	                    	  
	                    	   if(check==false)
	                    	   {
	                    		   String[] ary=flname.split("_");
	                    		   
	                    		   for(int k=1;k<ary.length;k++)
	                    		   {  
	             	                  int l=Integer.parseInt(ary[k])+1;
	             	                  String s=Integer.toString(l);
	             	                  flname="";
	             	                  flname=""+flName+"".concat(s);
	             	                  check=true;
	                    		   }
	                    	   }                
	                    }
                       
                    }
                }
            }  
    		
    		for (CommonsMultipartFile aFile : formletter)            
            {             
                if (aFile.getOriginalFilename().equals(""))                 
                {
                	String formletName = docservice.getEmpDocNames(Individual_Id,orgName);
               	    String formAry[] = formletName.split(",");
               	 
               	    if(!formAry[4].equals("No Document"))
               	      {
    			          empDocName = empDocName + "," + formAry[4];
               	      }
               	    
               	    else
               	      {
               	    	 empDocName = empDocName + "," + "No Document";
               	      }
    		    }
            }
    		   
    		
            int i =Integer.parseInt((String)session.getAttribute("Ind_id"));
            String empDocumentName = "";
           
            DocumentDetailsBean docdetailsbean = new DocumentDetailsBean();
                        
            SalarySlipModel salSlipModel  =  new SalarySlipModel();
            
            salSlipModel.setCompany_name(request.getParameter("cmyName"));
            salSlipModel.setInd_id(i);
            
            String payslipsName = docservice.getSalarySlipsNameAsPerUpload(salSlipModel);
            //String payslipsName = docservice.getPaySlipsName(salSlipModel);
            
            if(!payslipsName.equals("No Data"))
            {
            	 docdetailsbean.setDoc_name(empDocName.substring(1,empDocName.length())+","+payslipsName);
            }
            else
            {
                 docdetailsbean.setDoc_name(empDocName.substring(1,empDocName.length()));
            }
            
            docdetailsbean.setDoc_size(datasizeKB1);
            docdetailsbean.setDoc_type("Employment_DOC");
            docdetailsbean.setKyc_ind_id(i);
            docdetailsbean.setCr_date(new Date());
            
            docdetailsbean.setDes("Employement Details Changed.");
            
            docdetailsbean.setDoc_des("Changed Employement Status.");
            
            String panel = docservice.getEmpPanelStatusAsofNow(i);
            
            if(!panel.equals(""))
            {
            	docdetailsbean.setPanel_status(panel);
            }
                                  
            docdetailsbean.setDocs_data(orgName+"&&"+jobDesignation+"&&"+jobProfile+"&&"+jobDurFrom+"&&"+jobDurTo);
                                    
    		DocumentDetailModel docdetailmodel = prepareDocDetailModel(docdetailsbean);
    		
    		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
    		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
    		
    		if ("success".equals(docservice.uploadEmpDocumentDetail(docdetailmodel))) 
    		{
    			String Kyc_id= (String)session.getAttribute("Kyc_id");
    			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
    			Profileupdatemodel.setProfileupdatedetail(orgName+" details has been updated under Employment documents.");
    			Profileupdatemodel.setProfileupdatetime(new Date());
    			Profileupdatemodel.setType_of_alert("doc update");
    			Profileupdatemodel.setKyc_id(Kyc_id);
    			profileinfoService.profileupdate(Profileupdatemodel); 
    			    	    			   			
    			int i1 =Integer.parseInt((String) session.getAttribute("Ind_id"));
            	
            	String panel1 = docservice.getEmpPanelStatus(i1);
            	request.setAttribute("panel",panel1);
            	
            	String employeeDetails = docservice.getEmpDetails(panel1,i1);
            	request.setAttribute("employeeDetails",employeeDetails);
    			
            	String paySlipsMonthYear = docservice.getPaySlipsMonthYear(salSlipModel);
            	
            	request.setAttribute("paySlipsMonthYear", paySlipsMonthYear);
            	
            	int year = Calendar.getInstance().get(Calendar.YEAR);
        		request.setAttribute("year", year);
        		
        		request.setAttribute("paySlipMonths", docservice.getMonthSalarySlip(i1,orgName));
        		
    			
        		return new ModelAndView("individual_employee_details");
    			
    		} 
    		
    		else {
    			
    			int year = Calendar.getInstance().get(Calendar.YEAR);
        		request.setAttribute("year", year);
        		
    			return new ModelAndView("individual_employee_details");
    		}   
    		
    		
    	}
        
        @RequestMapping(value = "/viewEmployeeDocument", method = RequestMethod.GET)
    	public ModelAndView getviewEmp(HttpServletRequest request,HttpSession session)
        {
        
        		        	
	        	int Individual_Id =Integer.parseInt((String)session.getAttribute("Ind_id"));
	        	String year = (String)session.getAttribute("created_year");
	        	
	        	String fileName = request.getParameter("fileName");
	        	
	        	request.setAttribute("fileName", fileName);
	        	request.setAttribute("User", Individual_Id);
	        	request.setAttribute("Year", year);
	        	
	        	String individualInd_id = (String)session.getAttribute("Ind_id");	
	        	FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn((String)session.getAttribute("created_year"),individualInd_id,"Employement_DOC",request.getParameter("fileName"),request);

	    	    return new ModelAndView("viewEmp");
	    
    	}
    	
        
    	@RequestMapping(value = "/downloadEmployeeDocument", method = RequestMethod.GET)
    	public ModelAndView getdownloadEmp(HttpServletRequest request,HttpSession session)
    	{
    		int Individual_Id =Integer.parseInt((String)session.getAttribute("Ind_id"));
        	String year = (String)session.getAttribute("created_year");
        	
        	String fileName = request.getParameter("fileName");
        	
        	request.setAttribute("fileName", fileName);
        	request.setAttribute("User", Individual_Id);
        	request.setAttribute("Year", year);
        	
        	String individualInd_id = (String)session.getAttribute("Ind_id");	
        	FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn((String)session.getAttribute("created_year"),individualInd_id,"Employement_DOC",request.getParameter("fileName"),request);

        	return new ModelAndView("downloadEmp");
    	}
       
    	
    	 @RequestMapping(value="/employement_history" ,method=RequestMethod.GET)
         public ModelAndView getEmpHistory(HttpSession session,HttpServletRequest request)
         {
         	
	    		 session = request.getSession(false);
	 			
	 			if (session.getAttribute("Ind_id") == null)
	 			{			
	 				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
	 				 request.setAttribute("status","Your session has expired please login to use the portal !");
	 				
	 				 session.removeAttribute("Ind_id");
	 				 //session.invalidate();
	 				 
	 				 return new ModelAndView("individualPage");	
	 			}
	 			else if(((String)session.getAttribute("Ind_id")).equals(null))
	 			{
	 				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
	 				 request.setAttribute("status","Your session has expired please login to use the portal !");
	 				
	 				 session.removeAttribute("Ind_id");
	 				 //session.invalidate();
	 				 
	 				 return  new ModelAndView("individualPage");	
	 			}
	 			else
	 			{	
			         	DocumentDetailModel docdetails = new DocumentDetailModel();
			         	
			         	docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
			     		    		
			     		docdetails.setDocs_data(request.getParameter("cmyName"));
			     		
			     		Map<String, Object> model = new HashMap<String, Object>();
			     		
			     		model.put("Historydetails", prepareDocumentDetailBean(docservice.getEmpHistory(docdetails)));
			     	    
			     		int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
			     		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
			     		
			     		request.setAttribute("cmyName",request.getParameter("cmyName"));
			     		
			     		return new ModelAndView("individual_employee_historydetails",model);
	 			}
         	
         }
    
    	@RequestMapping(value = "/emp_viewmore", method = RequestMethod.GET)
     	public ModelAndView empViewDetails(HttpServletRequest request,HttpSession session) {
     		
    		 session = request.getSession(false);
 			
 			if (session.getAttribute("Ind_id") == null)
 			{			
 				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
 				 request.setAttribute("status","Your session has expired please login to use the portal !");
 				
 				 session.removeAttribute("Ind_id");
 				 //session.invalidate();
 				 
 				 return new ModelAndView("individualPage");	
 			}
 			else if(((String)session.getAttribute("Ind_id")).equals(null))
 			{
 				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
 				 request.setAttribute("status","Your session has expired please login to use the portal !");
 				
 				 session.removeAttribute("Ind_id");
 				 //session.invalidate();
 				 
 				 return  new ModelAndView("individualPage");	
 			}
 			else
 			{	
		         	DocumentDetailModel docdetails = new DocumentDetailModel();
		         	
		         	docdetails.setDoc_name((String)request.getParameter("id"));
		         	
		            int doc_id = Integer.parseInt(request.getParameter("docId"));
		        	
		            Date historyDate = docservice.getHitoryDateOfThisId(doc_id);
		            
		            int ownIndId = docservice.getIndIdviaDocId(doc_id);
		            int ownIndSessionId = Integer.parseInt((String)session.getAttribute("Ind_id"));
		            
		            if(ownIndSessionId==ownIndId)
		            {
		            	String employeeDetails = docservice.getEmpHistoryDetails(doc_id);
		            	
		            	request.setAttribute("employeeDetails",employeeDetails);
		             	        	         	        	
		
		            	int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
		            	request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
		
		            	request.setAttribute("historyDate",historyDate);
		        		
		         		return new ModelAndView("individual_employee_viewdetails");
		            }
		            else
		            {
		            	return new ModelAndView("kycErrorPage");
		            }
            
 			}
     	}
    	
    	@RequestMapping(value = "/saveSalarySlip", method = RequestMethod.POST)
     	public @ResponseBody String saveSalarySlip(HttpServletRequest request,HttpSession session,@RequestParam CommonsMultipartFile[] salarySlip) throws IllegalStateException, IOException {
     		
    		String monthYear = request.getParameter("month_year");
    		String flName = "PaySlip-"+request.getParameter("cmyName").split(" ")[0]+" "+monthYear+"_";
        	String user = (String)session.getAttribute("Ind_id");
        	String salarySlipName = "";
        	float datasizeKB1 = (float) 0.0;
    		
        	File ourPath = new File(kycMainDirectory);
        	
    		int Individual_Id = Integer.parseInt((String)session.getAttribute("Ind_id"));
    		    		
    		if (salarySlip != null && salarySlip.length > 0)
            {    			
    			File fld = new File(ourPath.getPath()+"/"+created_year+"/"+user+"/Document/"+"Employement_DOC");
            	if(!fld.exists())
            	{
        		fld.mkdirs();
            	}
        		String loc=ourPath.getPath()+"/"+created_year+"/"+user+"/Document/"+"Employement_DOC";
                
                for (CommonsMultipartFile aFile : salarySlip)            
                {             
                    if (!aFile.getOriginalFilename().equals(""))                 
                    {     
                    	String extension = "";
                        
                        int i = aFile.getOriginalFilename().lastIndexOf('.');
                        if (i > 0) {
                            extension = aFile.getOriginalFilename().substring(i+1);
                                   }                                        
                        aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
                        File datasize = new File(ourPath.getPath()+"/"+created_year+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());
                        float bytes = datasize.length();
                        datasizeKB1 = datasizeKB1 + bytes/1024;   
                        
                        
                        File store = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC");
	                    File[] storelist = store.listFiles();
	                       
                        
                        
                           String flname = flName+"1";
	                       boolean check=true;
	                       
	                       for(int f=0;f<50;f++)
	                        { 
	                    	   
	                    	   if(check==true)
	                    	   {
	                        	  for(int g=0;g<storelist.length;g++)
	                        	   {                       		 
	                        	     if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
	                                   	{                   		      
	                        	    	    check=false;                    	    	    
	                        	        }  
	                        	   }
	                        	  
	                        	  if(check==true)
	                           	       {                         		   
	                           		      File floldname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+aFile.getOriginalFilename());                   
	                                      File flnewname = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/Document/"+"Employement_DOC"+"/"+flname+"."+extension);
	                                      floldname.renameTo(flnewname);                                     
	                                      salarySlipName=flnewname.getName();
	                                      
					                                      /* ThumbNail For Syalary Documents */
						                                  
							                                if(!extension.equals("pdf"))
							                                {
							                                	if(!extension.equals("doc"))
								                                {
							                                		if(!extension.equals("docx"))
									                                {
							                                			if(!extension.equals("xlsx"))
										                                {
							                                				if(!extension.equals("xls"))
											                                {
												                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC");
												                                	  thumbNailDirectory.mkdirs();
												                                	  BufferedImage img = ImageIO.read(flnewname); 
													                                  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,230,150, Scalr.OP_ANTIALIAS); 
													                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
													                                  ImageIO.write(thumbImg,extension,os);
													                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+user+"/ThumbNail_Documents/"+"Employement_DOC"+"/"+flname+"."+extension);  
													                                  ImageIO.write(thumbImg, extension, f2);	
											                                }
										                                }
									                                }
								                                }
							                                }
							                                 
							                                  
						                                  /* End of Thumbnail Creation */
							                                
	                                      break;          
	                           	       }             	                              	                     	                     	                    	  
	                    	   }
	                    	  
	                    	   if(check==false)
	                    	   {
	                    		   String[] ary=flname.split("_");
	                    		   
	                    		   for(int k=1;k<ary.length;k++)
	                    		   {  
	             	                  int l=Integer.parseInt(ary[k])+1;
	             	                  String s=Integer.toString(l);
	             	                  flname="";
	             	                  flname=""+flName+"".concat(s);
	             	                  check=true;
	                    		   }
	                    	   }                
	                    }
                                      
                    }
                }
            }  
    		
    		SalarySlipModel salSlipModel  =  new SalarySlipModel();
    		
    		salSlipModel.setInd_id(Integer.parseInt(user));
    		salSlipModel.setCompany_name(request.getParameter("cmyName"));
    		salSlipModel.setMonth_year(request.getParameter("month_year"));
    		salSlipModel.setSlip_name(salarySlipName);
    		salSlipModel.setDoc_size(datasizeKB1);
    		salSlipModel.setCr_date(new Date());
    		    		
    		String status = docservice.savePaySlips(salSlipModel);
    		
    		
    		String Kyc_id= (String)session.getAttribute("Kyc_id");
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail(request.getParameter("month_year")+" "+request.getParameter("cmyName")+" salary slip has been added under Employment documents.");
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("doc update");
			Profileupdatemodel.setKyc_id(Kyc_id);
			profileinfoService.profileupdate(Profileupdatemodel); 
    		
    		String payslipsName = docservice.getPaySlipsName(salSlipModel);
    		    		
    		String paySlipsMonthYear = docservice.getPaySlipsMonthYear(salSlipModel);
    		
    		String cr_date = docservice.getCreatedDateOfPaySlips(payslipsName,salSlipModel);
    		
    		String currentSalSlipName = salarySlipName;
    		
    		String resultStatus = paySlipsMonthYear+"$$$"+payslipsName+"$$$"+cr_date;
   		
     		return resultStatus;
     	}
    	
    	
    	
}//End Of Controller
