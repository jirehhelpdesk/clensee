package com.kyc.controller;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.bean.FamilyDetailBean;
import com.kyc.bean.FeaturesDetailsBean;
import com.kyc.bean.PlanDetailsBean;
import com.kyc.bean.Profileupdatebean;
import com.kyc.bean.UtiliserProfileBean;
import com.kyc.dao.ProfileinfoDao;
import com.kyc.model.ClenseeInvoiceModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.FeaturesDetailsModel;
import com.kyc.model.PaymentDetailsModel;
import com.kyc.model.PlanDetailsModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.service.AccessService;
import com.kyc.service.DocService;
import com.kyc.service.IndSignupService;
import com.kyc.service.PlanService;
import com.kyc.service.ProfileinfoService;
import com.kyc.service.UtiliserService;
import com.kyc.util.CaptureIPandMACaddressUtil;
import com.kyc.util.SendEmailUtil;

@Controller
public class PlansController {
	
	@Autowired
	private ProfileinfoService profileinfoService;
	
	@Autowired
	private DocService docservice;
	
	@Autowired
	private UtiliserService utiservice;
		
	@Autowired
	private PlanService PlanService;

	@Autowired
	private IndSignupService signupService;
	
	@Autowired
	private AccessService accservice;
	
	/*     Admin Features related controller Methods      */
	
	public static String individualCategory = "Individual";
	public static String utilizerCategory = "Utilizer";
	public static String corporateCategory = "Corporate";
	
	
	static final Logger LOGGER = Logger.getLogger(PlansController.class);
    
	
	@RequestMapping(value = "/savefeaturedetail", method = RequestMethod.POST)
	public ModelAndView savefeaturedetail(HttpSession session,@ModelAttribute("command")FeaturesDetailsBean FeaturesDetailsBean, BindingResult result) {
	 		
		 FeaturesDetailsModel FeaturesDetailsModel = new FeaturesDetailsModel();		
		 FeaturesDetailsModel.setFeature_category(FeaturesDetailsBean.getFeature_category());
		 FeaturesDetailsModel.setFeature_name(FeaturesDetailsBean.getFeature_name());
		 FeaturesDetailsModel.setFeature_value(FeaturesDetailsBean.getFeature_value());
		 FeaturesDetailsModel.setFeature_price(FeaturesDetailsBean.getFeature_price());
		 FeaturesDetailsModel.setCr_by("Admin");
		 FeaturesDetailsModel.setCr_date(new Date());
		 
		if("successsavefeaturesdetail".equals(PlanService.addFeaturesDetail(FeaturesDetailsModel)))
		{
			 
		}
		 
		return new ModelAndView("createfeature");
	}
	
	@RequestMapping(value = "/viewfeature", method = RequestMethod.POST)
	public ModelAndView viewAdminPlanfeatures() {
		
	     return new ModelAndView("viewfeature");
	}
	
	
	@RequestMapping(value = "/selectedviewfeature", method = RequestMethod.POST)
	public @ResponseBody String selectedviewfeature1(HttpSession session,HttpServletRequest request) {
				
		String featureCategory = request.getParameter("featureCategory");	
		String featureName = request.getParameter("featureName");
		
		String fetureDetails = PlanService.getFeatureDetailsByCategory(featureCategory,featureName);
		
		return fetureDetails;	
		
	}
	
	@RequestMapping(value = "/organisefeature", method = RequestMethod.POST)
	public ModelAndView organisefeature1(HttpServletRequest request,
			@ModelAttribute("command") FeaturesDetailsModel FeaturesDetailsModel, 
				BindingResult result) {
	
		String organiseIndividualfeature = "Individual";
		String organiseUtilizerfeature = "Utilizer";
		String organiseCorporatefeature = "Corporate";
		
		return new ModelAndView("organisefeature");
	}

	
	@RequestMapping(value = "/organisefeaturefromAdmin", method = RequestMethod.POST)
	public ModelAndView organiseFeature(HttpServletRequest request) {
			
		String featureCategory = request.getParameter("featureCategory");		
		String featureName = request.getParameter("featureName");
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		if(featureCategory.equals("Individual"))
		{
			model.put("organiseIndividualfeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategoryAndName(featureCategory,featureName)));				
		}
		else if(featureCategory.equals("Utilizer"))
		{
			model.put("organiseUtilizerfeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategoryAndName(featureCategory,featureName)));			
		}
		else
		{
			model.put("organiseCorporatefeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategoryAndName(featureCategory,featureName)));			
		}
		
		return new ModelAndView("organisefeaturefromAdmin",model);
	}

	
	   private List<FeaturesDetailsBean> prepareOrganisefeature(List<FeaturesDetailsModel> getallfeatures) {
		 		 	
		 	List<FeaturesDetailsBean> bean = null;
		 	
			if (getallfeatures!= null && !getallfeatures.isEmpty()) {
				
				bean = new ArrayList<FeaturesDetailsBean>();
				FeaturesDetailsBean beans = null;
	
				for (FeaturesDetailsModel featuredetail : getallfeatures) {
		            
					beans = new FeaturesDetailsBean();
					beans.setFeature_name(featuredetail.getFeature_name());
					beans.setFeature_value(featuredetail.getFeature_value());
					beans.setFeature_price(featuredetail.getFeature_price());
					beans.setFeature_id(featuredetail.getFeature_id());
					beans.setFeature_category(featuredetail.getFeature_category());
					
					bean.add(beans);
				}
			}
			
			return bean;
		}
	   
	
	   
	   @RequestMapping(value = "/editfeaturedetail", method = RequestMethod.POST)
	   public @ResponseBody String editfeaturedetail1(HttpSession session,HttpServletRequest request,
			   @ModelAttribute("command") FeaturesDetailsBean  FeaturesDetailsBean,BindingResult result)
	   
	   {
	       		   
			FeaturesDetailsModel FeaturesDetailsModel=new FeaturesDetailsModel();
						 
			FeaturesDetailsModel.setFeature_name(FeaturesDetailsBean.getFeature_name());
			FeaturesDetailsModel.setFeature_value(FeaturesDetailsBean.getFeature_value());
			FeaturesDetailsModel.setFeature_price(FeaturesDetailsBean.getFeature_price());
			FeaturesDetailsModel.setFeature_category(FeaturesDetailsBean.getFeature_category());
			FeaturesDetailsModel.setFeature_id(Integer.parseInt(request.getParameter("feature_id")));
			FeaturesDetailsModel.setCr_by("Admin");
			FeaturesDetailsModel.setCr_date(new Date());
			
			if("success".equals(PlanService.addFeaturesDetail(FeaturesDetailsModel)))
			{
				return "Feature information successfully Updated";	      
			}
			else
			{
				return "Problem occured due to some reason,Try Again !!";	      
			}
						
		}
	
	   
	   @RequestMapping(value = "/deleteFeature", method = RequestMethod.POST)
		public @ResponseBody String deleteplanhasfeature1(HttpSession session,HttpServletRequest request,
				@ModelAttribute("command") FeaturesDetailsModel FeaturesDetailsModel,
					BindingResult result) {
						
			FeaturesDetailsModel.setFeature_id(Integer.parseInt(request.getParameter("feature_id")));
			
			if("success".equals(PlanService.deleteplanhasfeature(FeaturesDetailsModel)))
			{
				return "Feature deleted successfully !";
			}
			else
			{
				return "Problem occured due to some reason,Try Again !!";	 
			}
			
		}
		
	   
	 /*   End of Admin Features related controller Methods      */
	   
	   
	 
	 /*   Admin Plan related controller Methods      */
	   
	   @RequestMapping(value = "/createplan", method = RequestMethod.GET)
		public ModelAndView createplan1(HttpServletRequest request,HttpSession session,@ModelAttribute("command")FeaturesDetailsModel FeaturesDetailsModel, 
					BindingResult result) {
		   
		   if(((String)session.getAttribute("adminId"))!=null)
	        {
			    String organiseIndividualfeature = "Individual";
				String organiseUtilizerfeature = "Utilizer";
				String organiseCorporatefeature = "Corporate";
				
				Map<String, Object> model = new HashMap<String, Object>();
				
				model.put("Individualfeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategory(organiseIndividualfeature)));	
				
				model.put("Utilizerfeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategory(organiseUtilizerfeature)));
				
				model.put("Corporatefeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategory(organiseCorporatefeature)));
				
				return new ModelAndView("admin_myPlan",model);				
	        }
	        else
	        {
	        	request.setAttribute("status","failure");
				return new ModelAndView("adminLoginPage");		
	        }	
		   		    
		}
	   		   
	   @RequestMapping(value = "/checkPlanExistence", method = RequestMethod.POST)
		public @ResponseBody String checkPlanExistence(HttpSession session,
				@ModelAttribute("command") PlanDetailsBean  PlanDetailsBean, 
					BindingResult result) 
	   {
			
		    String planName = PlanDetailsBean.getPlan_name();
		    String planType = PlanDetailsBean.getPlan_type();
		    String planFeatures = PlanDetailsBean.getFeature_ids();
		    String planDomain = PlanDetailsBean.getPlan_domain();
		    
		    String planStatus = "";
		    		
		    if(planType.equals("Individual"))
		    {
		    	planStatus = PlanService.getCheckPlanExistOrNot4Ind(planName,planType);
		    }
		    else if(planType.equals("Utilizer"))
		    {
		    	planStatus = PlanService.getCheckPlanExistOrNot4Uti(planName,planDomain,planType);
		    }
		    else
		    {
		    	planStatus = PlanService.getCheckPlanExistOrNot4Cor(planName,planType);
		    }
			 			
			if(planStatus.equals("Yes"))
			{
				return "Yes";		      
			}
			else
			{
				return "No";	     
			}
						
		}
	   
	   	   
		@RequestMapping(value = "/savecreateplan", method = RequestMethod.POST)
		public @ResponseBody String savecreatefeature1(HttpSession session,
				@ModelAttribute("command") PlanDetailsBean  PlanDetailsBean, 
					BindingResult result) {
			
			FeaturesDetailsModel FeaturesDetailsModel=new FeaturesDetailsModel();
			 
			PlanDetailsModel  PlanDetailsModel = new  PlanDetailsModel();
			
			PlanDetailsModel.setFeature_ids( PlanDetailsBean.getFeature_ids());			
			PlanDetailsModel.setPlan_name( PlanDetailsBean.getPlan_name());
			PlanDetailsModel.setPlan_type( PlanDetailsBean.getPlan_type());
			
			if(PlanDetailsBean.getPlan_type().equals("Utilizer"))
			{
				PlanDetailsModel.setPlan_domain(PlanDetailsBean.getPlan_domain());
			}
			else
			{
				PlanDetailsModel.setPlan_domain("Default");
			}
			
			PlanDetailsModel.setPlan_price( PlanDetailsBean.getPlan_price());
			PlanDetailsModel.setCr_by("Admin");
			PlanDetailsModel.setCr_date(new Date());
			
			Map<String, Object> model = new HashMap<String, Object>();
			 
			model.put("organisefeature", prepareOrganisefeature(PlanService.getallfeatures(FeaturesDetailsModel)));		
					
			if("success".equals(PlanService.addPlanDetail(PlanDetailsModel)))
			{
				return "New Plan "+PlanDetailsBean.getPlan_name()+" Successfully Saved";		      
			}	
			else
			{
				return "Problem occured due to some reason,Try Again !!";	     
			}
						
		}
		
		
		@RequestMapping(value = "/viewplan", method = RequestMethod.POST)
		public ModelAndView viewplan1() {
								  
			return new ModelAndView("viewplan");		
			
		}
		
		@RequestMapping(value = "/get/plan/name/bycategory", method = RequestMethod.POST)
		public @ResponseBody String getPlanNameByCategory(HttpServletRequest request) {
			
			String searchResult = "";
			
			String planCategory = request.getParameter("planCategory");
			searchResult += PlanService.getPlanNamebyCategory(planCategory);
			
			return searchResult;
		}
		
				
		@RequestMapping(value = "/getplanviaDomainforutilizer", method = RequestMethod.POST)
		public @ResponseBody String getplanviaDomainforutilizer(HttpServletRequest request) {
			
			String searchResult = "";
			
			String planDomain = request.getParameter("planDomain");
			searchResult += PlanService.getPlanNameforUtilizer(planDomain);
			
			return searchResult;
		}
		
		@RequestMapping(value = "/get/plan/name/bycategory4Uti", method = RequestMethod.POST)
		public @ResponseBody String getPlanNameByCategoryForUti(HttpServletRequest request) {
			
			String searchResult = "";
			
			String planCategory= request.getParameter("planCategory");
			String planDomain = request.getParameter("planDomain");
			
			searchResult += PlanService.getPlanNameForUti(planCategory,planDomain);
						
			return searchResult;
		}
		
		
			
		@RequestMapping(value = "/selectedviewplan", method = RequestMethod.POST)
		public @ResponseBody String selectedviewplan1(HttpSession session,HttpServletRequest request) 		
		{
			FeaturesDetailsModel FeaturesDetailsModel=new FeaturesDetailsModel();
			FeaturesDetailsBean FeaturesDetailsBean=new FeaturesDetailsBean();	
			
			String plan_name = request.getParameter("plan_name");
			String plan_domain = request.getParameter("plandomain");
			String plan_category = request.getParameter("planCategory");
			
			float planPrice = (float)0.0;
			
			FeaturesDetailsModel.setFeature_name(plan_name);
									
			planPrice += PlanService.getPlanPricAsPerPlanCategory(plan_category,plan_domain,plan_name);
			
			List<FeaturesDetailsModel> planDetails = PlanService.getAllPlanDetailsBytheDetails(plan_category,plan_domain,plan_name);
			
			return prepareselectedviewplan(planDetails).toString()+"#"+planPrice;			
		}
		
	   private List<String> prepareselectedviewplan(List<FeaturesDetailsModel> getallfeatures) {
		 			 	
		   List<String> selectedvalue = new ArrayList<String>();
		   List<FeaturesDetailsBean> bean = null;
		 	
			if(getallfeatures!= null && !getallfeatures.isEmpty()) {
				
				bean = new ArrayList<FeaturesDetailsBean>();
				FeaturesDetailsBean beans = null;

				for (FeaturesDetailsModel featuredetail : getallfeatures) {
					
		            beans = new FeaturesDetailsBean();
		            
					beans.setFeature_name(featuredetail.getFeature_name());
					selectedvalue.add(featuredetail.getFeature_name());
					
					beans.setFeature_value(featuredetail.getFeature_value());
					selectedvalue.add(featuredetail.getFeature_value());
					
					beans.setFeature_price(featuredetail.getFeature_price());
					selectedvalue.add(featuredetail.getFeature_price().toString());
										
					bean.add(beans);
				}
			}
				return selectedvalue;
		} 
				
		@RequestMapping(value = "/organiseplan", method = RequestMethod.POST)
		public ModelAndView organiseplan1(HttpSession session,HttpServletRequest request,
				@ModelAttribute("command") PlanDetailsModel PlanDetailsModel,
					BindingResult result)		
		{								
			
			String organiseIndividualfeature = "Individual";
			String organiseUtilizerfeature = "Utilizer";
			String organiseCorporatefeature = "Corporate";
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("Individualfeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategory(organiseIndividualfeature)));	
			
			model.put("Utilizerfeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategory(organiseUtilizerfeature)));
			
			model.put("Corporatefeature", prepareOrganisefeature(PlanService.getallfeaturesbyCategory(organiseCorporatefeature)));
			
			return new ModelAndView("organiseplan",model);					
		}
		
		
		@RequestMapping(value = "/editplandetail", method = RequestMethod.POST)
		public @ResponseBody String editplandetail1(HttpSession session,HttpServletRequest request,
				@ModelAttribute("command") PlanDetailsBean  PlanDetailsBean,
					BindingResult result) {
			
			String searchResult = "";
			String planName = request.getParameter("plan_name");
			String planType = request.getParameter("plan_type");
			String planDomain = request.getParameter("planDomain");
			
			//String planFeatureId = 	PlanService.getPlanFeatureIdDetails(planName,planType);		
			String planFatureIdsForUtilizer = PlanService.getFeaturesForUtilizer(planName,planType,planDomain);
					
		   // String featureDetailsByCategory = PlanService.getAllFeatureDetailsByPlanName(planType);	
		    String featureDetailsByCategory = PlanService.getAllfeatureDetailsByPlanDetails(planName,planType,planDomain);
		    
			//float planPrice = PlanService.getPlanPricAsPrCategory(planName);
			
			float planPrice = PlanService.getPlanPricAsPerPlanCategory(planType,planDomain,planName);
			
			searchResult +=  featureDetailsByCategory + "#" + planFatureIdsForUtilizer + "#" + planPrice;
			
			return searchResult;
		}
		
		@RequestMapping(value = "/reOrganizePlan", method = RequestMethod.POST)
		public @ResponseBody String reOrganizePlan(HttpSession session,HttpServletRequest request)
		{
			String plan_type = request.getParameter("plan_type");
			String plan_name = request.getParameter("plan_Name");
			int plan_id = 0;
			String featureValue = request.getParameter("fatures_id");
			float plan_price = Float.parseFloat(request.getParameter("plan_price"));
			
            PlanDetailsModel  PlanDetailsModel = new  PlanDetailsModel();
            
           
			PlanDetailsModel.setFeature_ids(featureValue);			
			PlanDetailsModel.setPlan_name(plan_name);
			
			if(plan_type.equals("Individual"))
			{
				plan_id = PlanService.getPlanIdByNameAndType(plan_name,plan_type,"Default");
				
				PlanDetailsModel.setPlan_domain("Default");
			}
			else if(plan_type.equals("Utilizer"))
			{
				plan_id = PlanService.getPlanIdByNameAndType(plan_name,plan_type,request.getParameter("planDomain"));
				PlanDetailsModel.setPlan_domain(request.getParameter("planDomain"));
			}
			else
			{
				PlanDetailsModel.setPlan_domain("Default");
			}
			
			 PlanDetailsModel.setPlan_id(plan_id);
			PlanDetailsModel.setPlan_type(plan_type);
			PlanDetailsModel.setPlan_price(plan_price);
			PlanDetailsModel.setCr_by("Admin");
			PlanDetailsModel.setCr_date(new Date());
							
			if("success".equals(PlanService.addPlanDetail(PlanDetailsModel)))
			{
				return "success";
			}
			else
			{
				return "failure";
			}
			
		}
								
		@RequestMapping(value = "/deletePlan", method = RequestMethod.POST)
		public @ResponseBody String deleteplanhasfeatureorganise1(HttpSession session,HttpServletRequest request)
		{						
			String planType = request.getParameter("planType");
			String planName = request.getParameter("planName");
			
			String status = PlanService.deletePlanDetails(planType,planName);
			return status;		
		}
		   
	  							   
	   /* End of  Admin Plan related controller Methods */
	
		
		
		
		
		
		
	/*  Start of Individual Plan Controller Methods  */	
	
		
		@RequestMapping(value = "/individualviewplan", method = RequestMethod.GET)
		public ModelAndView individualviewplanMain(HttpSession session,HttpServletRequest request)
		{		
		        session = request.getSession(false);
				
				if (session.getAttribute("Ind_id") == null)
				{			
					 request.setAttribute("kycdocumentList", docservice.getKYCComp());
					 request.setAttribute("status","Your session has expired please login to use the portal !");
					
					 session.removeAttribute("Ind_id");
					 //session.invalidate();
					 
					 return new ModelAndView("individualPage");	
				}
				else if(((String)session.getAttribute("Ind_id")).equals(null))
				{
					 request.setAttribute("kycdocumentList", docservice.getKYCComp());
					 request.setAttribute("status","Your session has expired please login to use the portal !");
					
					 session.removeAttribute("Ind_id");
					 //session.invalidate();
					 
					 return  new ModelAndView("individualPage");	
				}
				else
				{														
					     int ind_id=Integer.parseInt((String)session.getAttribute("Ind_id"));		     
					     PlanDetailsModel PlanDetailsModel=new PlanDetailsModel();		     
					     PlanDetailsModel.setPlan_id(ind_id);						
					     FeaturesDetailsModel FeaturesDetailsModels = new FeaturesDetailsModel();
						 FeaturesDetailsModels.setFeature_id(ind_id);			 
						 Map<String, Object> model = new HashMap<String, Object>();
						 model.put("viewplan", prepareviewplan(PlanService.getcurrentplan(FeaturesDetailsModels)));					
						 String planname= PlanService.getplanname(FeaturesDetailsModels);			
						 request.setAttribute("planname",planname);
						 
						 int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
						 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
						 
						 Date register_Date = profileinfoService.getRegisterDate(ind_id);
						 
						 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");      			
						 String startDate = df.format(register_Date);
			
						 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						 Calendar c = Calendar.getInstance();
						 c.setTime(register_Date); // Now use register_Date .
						 c.add(Calendar.DATE, 364); // Adding 364 days
						 String endDate = sdf.format(c.getTime());
						 
						 request.setAttribute("startDate", startDate);
						 request.setAttribute("endDate", endDate);
						 
						 return new ModelAndView("individualPlan",model);
				}
		}
		
		@RequestMapping(value = "/individualviewplan", method = RequestMethod.POST)
		public ModelAndView individualviewplan1(HttpSession session,HttpServletRequest request)
		{	
			 session = request.getSession(false);
				
				if (session.getAttribute("Ind_id") == null)
				{			
					 request.setAttribute("kycdocumentList", docservice.getKYCComp());
					 request.setAttribute("status","Your session has expired please login to use the portal !");
					
					 session.removeAttribute("Ind_id");
					 //session.invalidate();
					 
					 return new ModelAndView("individualPage");	
				}
				else if(((String)session.getAttribute("Ind_id")).equals(null))
				{
					 request.setAttribute("kycdocumentList", docservice.getKYCComp());
					 request.setAttribute("status","Your session has expired please login to use the portal !");
					
					 session.removeAttribute("Ind_id");
					 //session.invalidate();
					 
					 return  new ModelAndView("individualPage");	
				}
				else
				{	
				
				     int ind_id=Integer.parseInt((String)session.getAttribute("Ind_id"));		     
				     PlanDetailsModel PlanDetailsModel=new PlanDetailsModel();		     
				     PlanDetailsModel.setPlan_id(ind_id);						
				     FeaturesDetailsModel FeaturesDetailsModels = new FeaturesDetailsModel();
					 FeaturesDetailsModels.setFeature_id(ind_id);			 
					 Map<String, Object> model = new HashMap<String, Object>();
					 model.put("viewplan", prepareviewplan(PlanService.getcurrentplan(FeaturesDetailsModels)));					
					 String planname= PlanService.getplanname(FeaturesDetailsModels);			
					 request.setAttribute("planname",planname);
					 
		             Date register_Date = profileinfoService.getRegisterDate(ind_id);
					 
					 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");      			
					 String startDate = df.format(register_Date);
		
					 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					 Calendar c = Calendar.getInstance();
					 c.setTime(register_Date); // Now use register_Date .
					 c.add(Calendar.DATE, 364); // Adding 364 days
					 String endDate = sdf.format(c.getTime());
					 
					 request.setAttribute("startDate", startDate);
					 request.setAttribute("endDate", endDate);
					 
					 return new ModelAndView("individualviewplan",model);
				}	 
		}
		
		
		
	   private List<FeaturesDetailsBean> prepareviewplan(List<FeaturesDetailsModel> getallfeatures) {
		 	
		 	List<FeaturesDetailsBean> bean = null;
		 	
			if (getallfeatures!= null && !getallfeatures.isEmpty()) {
				bean = new ArrayList<FeaturesDetailsBean>();
				FeaturesDetailsBean beans = null;

				for (FeaturesDetailsModel featuredetail : getallfeatures) {
		         beans = new FeaturesDetailsBean();
					beans.setFeature_name(featuredetail.getFeature_name());
					beans.setFeature_value(featuredetail.getFeature_value());
					beans.setFeature_price(featuredetail.getFeature_price());
					beans.setFeature_id(featuredetail.getFeature_id());
					beans.setFeature_category(featuredetail.getFeature_category());
					
					bean.add(beans);
				}
			}
				return bean;
		} 
					   
	   
		
	   @RequestMapping(value = "/individualgetmigrateplan", method = RequestMethod.GET)
		public String getMigratePlan(HttpSession session,HttpServletRequest request) 
	    {
		     
				   session = request.getSession(false);
					
					if (session.getAttribute("Ind_id") == null)
					{			
						 request.setAttribute("kycdocumentList", docservice.getKYCComp());
						 request.setAttribute("status","Your session has expired please login to use the portal !");
						
						 session.removeAttribute("Ind_id");
						 //session.invalidate();
						 
						 return "individualPage";	
					}
					else if(((String)session.getAttribute("Ind_id")).equals(null))
					{
						 request.setAttribute("kycdocumentList", docservice.getKYCComp());
						 request.setAttribute("status","Your session has expired please login to use the portal !");
						
						 session.removeAttribute("Ind_id");
						 //session.invalidate();
						 
						 return  "individualPage";	
					}
					else
					{	
						
					     String featureCategory = "Individual";
						
					     String featurecategory=PlanService.getfeaturecategory(featureCategory);
						 
						 String basicFeatures_Id = PlanService.getPlanFeatureId(featureCategory,"Basic");
						 String silverFeatures_Id = PlanService.getPlanFeatureId(featureCategory,"Silver");
						 String goldFeatures_Id = PlanService.getPlanFeatureId(featureCategory,"Gold");
						 String platinumFeatures_Id = PlanService.getPlanFeatureId(featureCategory,"Platinum");
						 	
						 String basicDetails = PlanService.getPlanDetails(basicFeatures_Id,featurecategory);
						 String silverDetails = PlanService.getPlanDetails(silverFeatures_Id,featurecategory);
						 String goldDetails = PlanService.getPlanDetails(goldFeatures_Id,featurecategory);
						 String platinumDetails = PlanService.getPlanDetails(platinumFeatures_Id,featurecategory);
						 				 			 					 
						 request.setAttribute("featurecategory", featurecategory);
						 request.setAttribute("basicDetails", basicDetails);
						 request.setAttribute("silverDetails", silverDetails);
						 request.setAttribute("goldDetails", goldDetails);			 
						 request.setAttribute("platinumDetails", platinumDetails);
						 
						 float basicprice = PlanService.getPlanPrice(featureCategory,"Basic");
						 float silverprice =PlanService.getPlanPrice(featureCategory,"Silver");
						 float goldprice = PlanService.getPlanPrice(featureCategory,"Gold");
						 float platinumprice = PlanService.getPlanPrice(featureCategory,"Platinum");
						 
						 int basicPriceValue = (int)basicprice;
						 int silverPriceValue = (int)silverprice;
						 int goldPriceValue = (int)goldprice;
						 int platinumPriceValue = (int)platinumprice;
								 
						 request.setAttribute("basicprice", basicPriceValue);
						 request.setAttribute("silverprice", silverPriceValue);
						 request.setAttribute("goldprice", goldPriceValue);			 
						 request.setAttribute("platinumprice", platinumPriceValue);
						
						 int ind_id=Integer.parseInt((String)session.getAttribute("Ind_id"));	
						 String currentPlanName = profileinfoService.getCurrentPlanName(ind_id);
						 request.setAttribute("currentPlanName", currentPlanName);
						 
						 int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
						 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
						 
					     return "individual_migratePlan";
					     
					}
		}
	   
	   
	   /* Individual requesting to change the plan */
	   
	   
	   /* Check oUt page for migrate the plan */
	  
	 
	   CaptureIPandMACaddressUtil  addressObject = new CaptureIPandMACaddressUtil();
		
	   
	   @RequestMapping(value = "/indRequestplanchange", method = RequestMethod.POST)
	   public ModelAndView checkout1(HttpSession session,HttpServletRequest request) throws SocketException, UnknownHostException {
		   
		   
		   int ind_id=Integer.parseInt((String) session.getAttribute("Ind_id"));
		   		   
		   String currentPlanName = profileinfoService.getCurrentPlanName(ind_id);
		   
		   String requestedPlanName = request.getParameter("planName");
		   float planPrice = PlanService.getPlanPrice("Individual",requestedPlanName); 
		   int planPriceValue = (int) planPrice;
		   String LatestPicName = profileinfoService.getLatestPicName(ind_id);
		   List<RegHistoryModel> regDetails = profileinfoService.getIndividualDetailsviaIndId(ind_id);
		   String fullName = regDetails.get(0).getFirst_name()+" "+regDetails.get(0).getMiddle_name()+" "+regDetails.get(0).getLast_name();
		   String emailId = regDetails.get(0).getEmail_id();
		   long mobileno = regDetails.get(0).getMobile_no();
		   
		   request.setAttribute("requestedPlanName", requestedPlanName);
		   request.setAttribute("planPrice", planPriceValue);
		   request.setAttribute("LatestPicName", LatestPicName);
		   request.setAttribute("fullName", fullName);
		   request.setAttribute("emailId", emailId);
		   request.setAttribute("mobileno", mobileno);
		   		  
		   session.setAttribute("requestedPlan", requestedPlanName);
		   
		   
		   FeaturesDetailsModel FeaturesDetailsModels = new FeaturesDetailsModel();
		   FeaturesDetailsModels.setFeature_id(ind_id);			 
		   Map<String, Object> model = new HashMap<String, Object>();
		   model.put("viewplan", prepareviewplan(PlanService.getcurrentplan(FeaturesDetailsModels)));					
			 
		   	   
		   //Save the records before payment 
		  
		   if(session.getAttribute("paymentId")==null)
		   {
			   PaymentDetailsModel payu = new PaymentDetailsModel();
			   
			   payu.setPayment_from("Individual");
			   payu.setPayment_done_by(fullName);
			   payu.setPayment_amount(planPriceValue+"");
			   payu.setPayment_date(new Date());
			   payu.setPayment_status("Pending");
			   payu.setPayment_info_emailId(emailId);
			   payu.setPayment_info_mobileno(mobileno+"");		   		   
			   payu.setPayment_info_ipaddress(addressObject.getIPddress(request));
		  	   	   
			   String planStaus = PlanService.savePaymentRecords(payu);
			   session.setAttribute("paymentId", PlanService.getUniquePaymentId(payu));		
			   
			   System.out.println("PayU hits the success url.");
		   }
		   
				
		  //End of Saving the records before payment 
		   
		   return new ModelAndView("clenseeCheckOut",model);
	   }
	  
	   
	   @RequestMapping(value ="/checkoutSuccess", method = RequestMethod.POST)
	   public String checkoutSuccessPost(HttpSession session,HttpServletRequest request) {
		
		   	
		   try
		   {			  
		       System.out.println("PayU hits the success url.");
		   
			   String paymentAmount = request.getParameter("amount");
			   String paymentStatus = request.getParameter("status");
			   String paymentTranId = request.getParameter("txnid");
			   	
			   System.out.println("PayU Status.==txnid=="+paymentTranId);
			   System.out.println("PayU Status.==paymentStatus=="+paymentStatus);
			   System.out.println("PayU Status.==paymentAmount=="+paymentAmount);
			   System.out.println("PayU Status.==paymentId=="+session.getAttribute("paymentId"));
			   
			   int paymentId = (int) session.getAttribute("paymentId");
			   
			   PlanService.updatePaymentStatus(paymentId,paymentAmount,paymentStatus,paymentTranId);
		   
			   session.removeAttribute("paymentId");
			  			   
			      // Do Clensee operation as per the success result
			   			   
			   		int ind_id=Integer.parseInt((String) session.getAttribute("Ind_id"));
			   		String requestedPlanName = (String)session.getAttribute("requestedPlan");
				 				
				  // Tracking Activity Details
			
				     String currentPlanName = profileinfoService.getCurrentPlanName(ind_id);
				     
				     String activity = "";
				     if(currentPlanName.equals("Platinum"))
				     {
				    	 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
				     }
				     else if(currentPlanName.equals("Gold"))
				     {
				    	 if(requestedPlanName.equals("Platinum"))
				    	 {
				    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
				    	 }
				    	 else
				    	 {
				    		 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
				    	 }			    	 
				     }
				     else if(currentPlanName.equals("Silver"))
				     {
				    	 if(requestedPlanName.equals("Platinum"))
				    	 {
				    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
				    	 }
				    	 else if(requestedPlanName.equals("Gold"))
				    	 {
				    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
				    	 }
				    	 else
				    	 {
				    		 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
				    	 }	
				     }
				     else
				     {
				    	 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
				     }
					   
				     
                    if(paymentStatus.equals("success"))
                    {
                    	String migrateStatus = PlanService.changeplan(ind_id,requestedPlanName);
						
						
				        String Kyc_id= (String)session.getAttribute("Kyc_id");
						Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 				
						Profileupdatemodel.setProfileupdatedetail(activity);				
						Profileupdatemodel.setProfileupdatetime(new Date());
						Profileupdatemodel.setType_of_alert("Plan Updated");
						Profileupdatemodel.setMore_about_it("Event done in Plan");
					    Profileupdatemodel.setKyc_id(Kyc_id);
						profileinfoService.profileupdate(Profileupdatemodel);
				
                    }
						
						
		         // End Of Tracking Activity Details
			   
						
					
					// Mail to user as a plan changed Status
						
						RegHistoryModel individualDetails = signupService.getLatestRegData(ind_id);
						 
						String reciverName = individualDetails.getFirst_name()+" "+individualDetails.getMiddle_name()+" "+individualDetails.getLast_name();
						String reciverEmailId = individualDetails.getEmail_id();
						
						String planChangeMail = profileinfoService.sendMailForPlanChange(reciverName,reciverEmailId,requestedPlanName);
						
						// End of mailing to user as a plan changed Status
						
					     	    	
							// Sending Invoice to user from Clensee side
							     	
					     	    	
					     	    //Save Invoice Details 
					     	    	
					     	       Date date = new Date(); // your date
					     	       Calendar cal = Calendar.getInstance();
					     	       cal.setTime(date);
					     	       int year = cal.get(Calendar.YEAR);
					     	       int month = cal.get(Calendar.MONTH);
					     	       int day = cal.get(Calendar.DAY_OF_MONTH);
					     	       int currentTime = cal.get(Calendar.HOUR);
					     	      			     	       					     	              
					     	       int lastInvoiceNumber = PlanService.getLastInvoiceNumber();
					     	       int randomNumber = lastInvoiceNumber+ind_id;
					     	      
					     	       String uniqueInvoiceId = "CLE"+year+month+day+currentTime+randomNumber;
					     	       
					     	       ClenseeInvoiceModel invoiceData = new ClenseeInvoiceModel();
					     	       invoiceData.setInvoice_id(uniqueInvoiceId);
					     	       invoiceData.setClensee_unique_id(ind_id);
					     	       invoiceData.setInvoice_category("Individual");
					     	       invoiceData.setInvoice_done_for(reciverName);
					     	       invoiceData.setInvoice_generated_date(date);
					     	    	invoiceData.setInvoice_payment_amount(paymentAmount);
					     	    	invoiceData.setInvoice_description("Plan migration from "+currentPlanName+" to "+requestedPlanName);
					     	    						     	    	
					     	    	String invoiceSavingStatus = PlanService.saveInvoiceDetails(invoiceData);
					     	    						     	   
					     	   // End of saving Invoice details 	
					     	    	
				     	    if(invoiceSavingStatus.equals("success"))
				     	    {				  
				     	    	   String invoiceMail = profileinfoService.sendEmailForInvoice(day,month,year,uniqueInvoiceId,randomNumber,paymentAmount,currentPlanName,requestedPlanName,reciverName,reciverEmailId);
				     	    	   
				     	    	   request.setAttribute("contactNotifyMessage","You have successfully submited the request.");				     	    	  
					        }
							     	    	
				// End of sending invoice to user from clensee side.
					
			   // End of Clensee opration as per the success result
				     	    
				     	   return "individualPaymentSuccess";
			 
		   }
		   catch(Exception e)
		   {
			   System.out.println("Exception due to="+e.getMessage());
			   
			   return "individualPaymentSuccess";
		   }
		 
		   
	   }
	  
	   @RequestMapping(value = "/checkoutFailure", method = RequestMethod.POST)
	   public String checkoutFailure(HttpSession session,HttpServletRequest request) {
		
		   String paymentAmount = request.getParameter("amount");
		   String paymentStatus = request.getParameter("status");
		   String paymentTranId = request.getParameter("txnid");
		   
		   int paymentId = (int) session.getAttribute("paymentId");
		   
		   PlanService.updatePaymentStatus(paymentId,paymentAmount,paymentStatus,paymentTranId);
	   
	       return "individualPaymentFailure";
	   }
	   
	   
	   
	   /* End of checkout for migrating the plan */
	   
	   
	    @RequestMapping(value = "/changeplan", method = RequestMethod.POST)
		public @ResponseBody String changeplan1(HttpSession session,HttpServletRequest request) {
			 
	    	
			 int ind_id=Integer.parseInt((String) session.getAttribute("Ind_id"));
			 String requestedPlanName = request.getParameter("planName");
			 
			 String migrateStatus = PlanService.changeplan(ind_id,requestedPlanName);
			
			  // Tracking Activity Details
			  
			     String currentPlanName = profileinfoService.getCurrentPlanName(ind_id);
			     String activity = "";
			     if(currentPlanName.equals("Platinum"))
			     {
			    	 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
			     }
			     else if(currentPlanName.equals("Gold"))
			     {
			    	 if(requestedPlanName.equals("Platinum"))
			    	 {
			    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
			    	 }
			    	 else
			    	 {
			    		 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
			    	 }			    	 
			     }
			     else if(currentPlanName.equals("Silver"))
			     {
			    	 if(requestedPlanName.equals("Platinum"))
			    	 {
			    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
			    	 }
			    	 else if(requestedPlanName.equals("Gold"))
			    	 {
			    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
			    	 }
			    	 else
			    	 {
			    		 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
			    	 }	
			     }
			     else
			     {
			    	 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
			     }
				  
			        String Kyc_id= (String)session.getAttribute("Kyc_id");
					Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 				
					Profileupdatemodel.setProfileupdatedetail(activity);				
					Profileupdatemodel.setProfileupdatetime(new Date());
					Profileupdatemodel.setType_of_alert("Plan Updated");
					Profileupdatemodel.setMore_about_it("Event done in Plan");
				    Profileupdatemodel.setKyc_id(Kyc_id);
					profileinfoService.profileupdate(Profileupdatemodel);
					
					
	         // End Of Tracking Activity Details
								
		     return migrateStatus;		     
		}
		
	   
	    /* End of requesting to change Plan */
	  
	        @RequestMapping(value = "/successResultOfchangeplan", method = RequestMethod.POST)
	 		public String successResultOfchangeplan(HttpSession session,HttpServletRequest request) {
	 			 	 		   
	 			 int ind_id=Integer.parseInt((String) session.getAttribute("Ind_id"));
	 			 String requestedPlanName = (String) session.getAttribute("PlanName");
	 			 
	 			 String migrateStatus = PlanService.changeplan(ind_id,requestedPlanName);
	 				 			 
	 			 // Tracking Activity Details
	 		        
	 		        String Kyc_id= (String)session.getAttribute("Kyc_id");
	 				Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
	 				Profileupdatemodel.setProfileupdatedetail("You have Upgrade your plan to "+requestedPlanName);
	 				Profileupdatemodel.setProfileupdatetime(new Date());
	 				Profileupdatemodel.setType_of_alert("Plan Updated");
	 				Profileupdatemodel.setMore_about_it("Event done in Plan");
	 			    Profileupdatemodel.setKyc_id(Kyc_id);
	 				profileinfoService.profileupdate(Profileupdatemodel);

	 	         // End Of Tracking Activity Details
	 			
	 			return "redirect:/individualviewplan.html";	 					 		  
	 		}
	   
	   
	   	 
	   @RequestMapping(value = "/usageDetails", method = RequestMethod.POST)
	   public ModelAndView getUsageDetails(HttpSession session,HttpServletRequest request) {
					    	
		   int ind_id=Integer.parseInt((String) session.getAttribute("Ind_id"));
		     		     			
		   String kyc_id = PlanService.getKycId(ind_id);
		  
		   int noOfGeneratedCode = PlanService.getCountGeneratedCode(kyc_id);
		   int noOfdApplyCode = PlanService.getCountApplyCode(kyc_id);
		   int noOfAllowAccess = PlanService.getCountAllowAccess(kyc_id);
		   int noOfRevokeAccess = PlanService.getCountRevokeAccess(kyc_id);
		   float totalFileSize = PlanService.getTotalFileStorage(ind_id);
		   
		   double totalSizeinMB = totalFileSize/1024.0;
		   			   
		   String currentPlanName = PlanService.getCurrentPlanName(ind_id);
		   
		  		   
		    
		    String planName = PlanService.getCurrentPlanName(ind_id);
			String featuresIdList =  PlanService.getPlanFeatureIdDetails(planName, individualCategory);
			String featuresListArray[] = featuresIdList.split(",");
			
			//Calculate file Storage As per the Plan
			
			
			float fileStorageAsPerPlan=(float)0.0;		
			for(int i=0;i<featuresListArray.length;i++)
			{
				int featureId = Integer.parseInt(featuresListArray[i]);
				String featureNames = PlanService.getFeatureNameById(featureId);
				if(featureNames.equalsIgnoreCase("Files Storage"))
				{
					String restrictStatus = PlanService.getFeatureValue(featureId);
					if(restrictStatus.contains("MB"))
					{
						String clean = restrictStatus.replaceAll("\\D+","");
					    fileStorageAsPerPlan = fileStorageAsPerPlan + Float.parseFloat(clean);				   
					}
					else
					{
						String clean = restrictStatus.replaceAll("\\D+","");
					    fileStorageAsPerPlan = fileStorageAsPerPlan + Float.parseFloat(clean);			
					    fileStorageAsPerPlan = fileStorageAsPerPlan * 1024;
					}
				}
			}	
		   
			//Calculate Generate Code As per the Plan
						
			int noOfGeneratedCodeAsPerPlan = (int)0;							
			for(int i=0;i<featuresListArray.length;i++)
			{
				int featureId = Integer.parseInt(featuresListArray[i]);
				String featureNames = PlanService.getFeatureNameById(featureId);
				if(featureNames.equalsIgnoreCase("Individual Sharing"))
				{
					String restrictStatus = PlanService.getFeatureValue(featureId);
					String clean = restrictStatus.replaceAll("\\D+","");
					noOfGeneratedCodeAsPerPlan = noOfGeneratedCodeAsPerPlan + Integer.parseInt(clean);
				}
			 }
		   
			//Calculate Apply Code As per the Plan
			
			int noOfApplyCodeAsPerPlan = (int)0;
			for(int i=0;i<featuresListArray.length;i++)
			{
				int featureId = Integer.parseInt(featuresListArray[i]);
				String featureNames = PlanService.getFeatureNameById(featureId);
				if(featureNames.equalsIgnoreCase("Documents Received"))
				{
					String restrictStatus = PlanService.getFeatureValue(featureId);
					String clean = restrictStatus.replaceAll("\\D+","");
					noOfApplyCodeAsPerPlan = noOfApplyCodeAsPerPlan + Integer.parseInt(clean);
				}
			}
			
			//Calculate Allow Access As per the Plan
			
			int noOfAllowAccessAsPerPlan = (int)0;
			for(int i=0;i<featuresListArray.length;i++)
			{
				int featureId = Integer.parseInt(featuresListArray[i]);
				String featureNames = PlanService.getFeatureNameById(featureId);
				if(featureNames.equalsIgnoreCase("Utilizer Sharing / Removing"))
				{
					String restrictStatus = PlanService.getFeatureValue(featureId);
					String clean = restrictStatus.replaceAll("\\D+","");
					noOfAllowAccessAsPerPlan = noOfAllowAccessAsPerPlan + Integer.parseInt(clean);
				}
			}
			
			
			//Calculate profile viewing as per Except Basic
						
			if(!planName.equalsIgnoreCase("Basic"))
			{
				String restrictStatus = "";			
				for(int i=0;i<featuresListArray.length;i++)
				{
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("Profile Viewing"))
					{
						restrictStatus += PlanService.getFeatureValue(featureId);
					}
				}	
				String planStatus = "";
				
				 String clean = restrictStatus.replaceAll("\\D+","");
				 int noOfFullProfileAsPerPlan = Integer.parseInt(clean);
				 session.setAttribute("noOfFullProfileAsPerPlan", noOfFullProfileAsPerPlan);
				 String individual_kyc_id = PlanService.getKycId(ind_id);
				 int noOfFullProfileAsOfNow = PlanService.noOfFullProfileAsOfNow(individual_kyc_id);
				 session.setAttribute("noOfFullProfileAsOfNow",noOfFullProfileAsOfNow);
				 
				 planStatus += "Full Profile";	
				 
				 request.setAttribute("noOfFullProfileAsPerPlan", noOfFullProfileAsPerPlan);
				 request.setAttribute("noOfFullProfileAsOfNow", noOfFullProfileAsOfNow);
				 
				 request.setAttribute("plan", "other");
			}
			else
			{
				request.setAttribute("plan", "Basic");
			}
			
			//End of calculating ProfileViewing
			
			
			//Calculate SMS as per Plan
			
			String currentPlan = PlanService.getCurrentPlanName(ind_id);
			String  restrictStatus = "";			
			for(int i=0;i<featuresListArray.length;i++)
			{
				int featureId = Integer.parseInt(featuresListArray[i]);
				String featureNames = PlanService.getFeatureNameById(featureId);
				if(featureNames.equalsIgnoreCase("SMS"))
				{
					restrictStatus += PlanService.getFeatureValue(featureId);
				}
			}
			
			
			 String clean = restrictStatus.replaceAll("\\D+","");
			 
			 int noOfSMSAsPerPlan = 0;
				
			 int noOfSMSAsOfNow = PlanService.noOfSMSAsOfNow(ind_id);
			 
				 if(!clean.equals(""))
				 {
					 noOfSMSAsPerPlan = Integer.parseInt(clean);
				 }
			
			//End of calculating SMS criteria
			
			
			
			   request.setAttribute("noOfGeneratedCode", noOfGeneratedCode);
			   request.setAttribute("noOfdApplyCode", noOfdApplyCode);
			   request.setAttribute("noOfAllowAccess", noOfAllowAccess);
			   request.setAttribute("noOfRevokeAccess", noOfRevokeAccess);
			   request.setAttribute("totalSizeinMB", totalSizeinMB);
			   request.setAttribute("currentPlanName", currentPlanName);
			   
			   request.setAttribute("noOfGeneratedCodeAsperPlan", noOfGeneratedCodeAsPerPlan);
			   request.setAttribute("noOfdApplyCodeAsperPlan", noOfApplyCodeAsPerPlan);
			   request.setAttribute("noOfAllowAccessAsperPlan", noOfAllowAccessAsPerPlan);
			   request.setAttribute("noOfRevokeAccessAsperPlan", noOfAllowAccessAsPerPlan);
			   request.setAttribute("totalSizeinMBAsperPlan", fileStorageAsPerPlan);
			   
			   request.setAttribute("noOfSMSAsOfNow", noOfSMSAsOfNow);
			   request.setAttribute("noOfSMSAsPerPlan", noOfSMSAsPerPlan);
			   
			   request.setAttribute("currentPlanName", currentPlanName);
		   
		   return new ModelAndView("usagedetails");
		}

		
	/*  End  of Individual Plan Controller Methods  */	
		

	   
	   
	   
	   
	   
	/* Start of Utilizer Plan Controller Methods */
	   
	   
	   @RequestMapping(value ="/utilizerPlan", method = RequestMethod.GET)
	   public ModelAndView utilizerPlan(HttpServletRequest request,HttpSession session) {
			
				   session = request.getSession(false);
					
					if (session.getAttribute("uti_Ind_id") == null)
					{						 
						 request.setAttribute("status","Your session has expired please login to use the portal !");
						
						 session.removeAttribute("uti_Ind_id");
						 //session.invalidate();
						 
						 return new ModelAndView("login_utiliser");
					}					
					else
					{	
			
					     int uti_Ind_id= (int)session.getAttribute("uti_Ind_id");
					     		    
					     FeaturesDetailsModel FeaturesDetailsModels = new FeaturesDetailsModel();
						 FeaturesDetailsModels.setFeature_id(uti_Ind_id);
						 
						 Map<String, Object> model = new HashMap<String, Object>();
						 model.put("viewplan", prepareviewplan(PlanService.getUtilizerCurrentplan(FeaturesDetailsModels)));		
						 
						 String planname= PlanService.getUtilizerplanname(uti_Ind_id);			
						 
						 request.setAttribute("planname",planname);
						 	
						 Date utiPlanActivatedDate = PlanService.getUtiActivatedPlanDate(uti_Ind_id);
						 
						 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");      			
						 String startDate = df.format(utiPlanActivatedDate);

						 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						 Calendar c = Calendar.getInstance();
						 c.setTime(utiPlanActivatedDate); // Now use register_Date .
						 c.add(Calendar.DATE, 364); // Adding 364 days
						 String endDate = sdf.format(c.getTime());
						 
						 request.setAttribute("startDate", startDate);
						 request.setAttribute("endDate", endDate);
						 
						   
						int uti_Indid = (int) session.getAttribute("uti_Ind_id");			
						model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
							
						String uti_kycid = (String)session.getAttribute("KycId");
						int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
						int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
						
						request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
						request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
						
					     return new ModelAndView("utilizer_plan_details",model);		
					
					}
		}
	  
	   private UtiliserProfileBean prepareProfilePicBean(UtiliserProfileModel picModel)
		{
			UtiliserProfileBean picBean = new UtiliserProfileBean();
			
			picBean.setUti_ind_id(picModel.getUti_ind_id());
			picBean.setUti_profile_pic(picModel.getUti_profile_pic());
			picBean.setUti_cr_date(picModel.getUti_cr_date());
			picBean.setUti_first_name(picModel.getUti_first_name());
			picBean.setUti_office_Address(picModel.getUti_office_Address());
			picBean.setUti_head_office_Address(picModel.getUti_head_office_Address());
			picBean.setUti_dom_hierarchy(picModel.getUti_dom_hierarchy());
			
			return picBean;
		}
	   
	   
	   @RequestMapping(value ="/utilizermyplan", method = RequestMethod.POST)
	   public ModelAndView getUtilizerViewMyPlan(HttpServletRequest request,HttpSession session) {
				    	
		     int uti_Ind_id= (int)session.getAttribute("uti_Ind_id");
		     		    
		     FeaturesDetailsModel FeaturesDetailsModels = new FeaturesDetailsModel();
			 FeaturesDetailsModels.setFeature_id(uti_Ind_id);
			 
			 Map<String, Object> model = new HashMap<String, Object>();
			 model.put("viewplan", prepareviewplan(PlanService.getUtilizerCurrentplan(FeaturesDetailsModels)));		
			 
			 String planname= PlanService.getUtilizerplanname(uti_Ind_id);			
			 
			 request.setAttribute("planname",planname);
			 	    				
			 Date utiPlanActivatedDate = PlanService.getUtiActivatedPlanDate(uti_Ind_id);
			 
			 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");      			
			 String startDate = df.format(utiPlanActivatedDate);

			 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			 Calendar c = Calendar.getInstance();
			 c.setTime(utiPlanActivatedDate); // Now use register_Date .
			 c.add(Calendar.DATE, 364); // Adding 364 days
			 String endDate = sdf.format(c.getTime());
			 
			 request.setAttribute("startDate", startDate);
			 request.setAttribute("endDate", endDate);
			 
			 
			 
		     return new ModelAndView("utiMyPlan",model);			
		}
	    
	    
	    
	    @RequestMapping(value = "/Uti_Migrateplan", method = RequestMethod.GET)
	    public ModelAndView utiMigrateplan(HttpSession session,HttpServletRequest request)
	    {
	    	 session = request.getSession(false);
				
				if (session.getAttribute("uti_Ind_id") == null)
				{						 
					 request.setAttribute("status","Your session has expired please login to use the portal !");					
					 session.removeAttribute("uti_Ind_id");
					 //session.invalidate();					 
					 return new ModelAndView("login_utiliser");
				}					
				else
				{		     
					     int uti_Ind_id= (int)session.getAttribute("uti_Ind_id");
					   
					     String featureCategory = "Utilizer";
					     String planDomain = PlanService.getUtilizerPlanDomain(uti_Ind_id);
						 
					     String featurecategory=PlanService.getfeaturecategory(featureCategory);
						 
						 String basicFeatures_Id = PlanService.getUtilizerPlanFeatureId(featureCategory,"Basic",planDomain);
						 String silverFeatures_Id = PlanService.getUtilizerPlanFeatureId(featureCategory,"Silver",planDomain);
						 String goldFeatures_Id = PlanService.getUtilizerPlanFeatureId(featureCategory,"Gold",planDomain);
						 String platinumFeatures_Id = PlanService.getUtilizerPlanFeatureId(featureCategory,"Platinum",planDomain);
						 						  					 
						 String basicDetails = PlanService.getPlanDetails(basicFeatures_Id,featurecategory);
						 String silverDetails = PlanService.getPlanDetails(silverFeatures_Id,featurecategory);
						 String goldDetails = PlanService.getPlanDetails(goldFeatures_Id,featurecategory);
						 String platinumDetails = PlanService.getPlanDetails(platinumFeatures_Id,featurecategory);
						 
						 
						 request.setAttribute("featurecategory", featurecategory);
						 request.setAttribute("basicDetails", basicDetails);
						 request.setAttribute("silverDetails", silverDetails);
						 request.setAttribute("goldDetails", goldDetails);			 
						 request.setAttribute("platinumDetails", platinumDetails);
						 
						 float basicprice = PlanService.getPlanPriceforUtilizer(featureCategory,planDomain,"Basic");
						 float silverprice =PlanService.getPlanPriceforUtilizer(featureCategory,planDomain,"Silver");
						 float goldprice = PlanService.getPlanPriceforUtilizer(featureCategory,planDomain,"Gold");
						 float platinumprice = PlanService.getPlanPriceforUtilizer(featureCategory,planDomain,"Platinum");
						 
						 int basicPriceValue = (int) basicprice;
						 int silverPriceValue = (int) silverprice;
						 int goldPriceValue = (int) goldprice;
						 int platinumPriceValue = (int) platinumprice;
						 
						 request.setAttribute("basicprice", basicPriceValue);
						 request.setAttribute("silverprice", silverPriceValue);
						 request.setAttribute("goldprice", goldPriceValue);			 
						 request.setAttribute("platinumprice", platinumPriceValue);
						 
						 String planname= PlanService.getUtilizerplanname(uti_Ind_id);			
						 
						 request.setAttribute("planname",planname);
						 
						 Map<String, Object> model = new HashMap<String, Object>();  
						
						 int uti_Indid = (int) session.getAttribute("uti_Ind_id");								
						 model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
							
						 String uti_kycid = (String)session.getAttribute("KycId");
						 int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
						 int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
							
						 request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
						 request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
													 
						 // Unused feature will remove from the plan list.						 
						 
						 String allfeaturesId = PlanService.getFeatureIdByFeatureNames("Utilizer",featurecategory);
						 String allFeatureIdRecords = basicFeatures_Id + "," + silverFeatures_Id + "," + goldFeatures_Id + "," + platinumFeatures_Id;
						 String extrafeatureNames = "";
						 if(allfeaturesId.length()>0)
						 {
							 String allfeaturesIdArray[] =  allfeaturesId.split(",");
							 for(int k=0;k<allfeaturesIdArray.length;k++)
							 {
								 if(!allFeatureIdRecords.contains(allfeaturesIdArray[k]))
								 {
									 int featureId = Integer.parseInt(allfeaturesIdArray[k]);
									 extrafeatureNames += PlanService.getFeatureNameById(featureId) + ",";
								 }
							 }
						 }
							 
						 if(extrafeatureNames.length()>0)
						 {
							 extrafeatureNames = extrafeatureNames.substring(0, extrafeatureNames.length()-1);
						 }
						 
						 request.setAttribute("extrafeatureNames", extrafeatureNames);
						 
			         // End of unused feature will remove from the plan list
						 
				         return new ModelAndView("utilizer_migratePlan",model);	
				}	     
	    }
	    
	    
	    
	    
	    // Utilizer plan Change by payment API
	    
	    
	    
	    @RequestMapping(value = "/utiRequestplanchange", method = RequestMethod.POST)
		public ModelAndView utiRequestplanchange(HttpSession session,HttpServletRequest request) throws SocketException, UnknownHostException {
			   
			   
			   int uti_Ind_id= (int) session.getAttribute("uti_Ind_id");
			   	
			   String uti_kycid = (String)session.getAttribute("KycId");
			   
			   String currentPlanName = PlanService.getUtilizerplanname(uti_Ind_id);	
			   String planDomain = PlanService.getUtilizerPlanDomain(uti_Ind_id);
			   
			   String requestedPlanName = request.getParameter("planName");
			   float planPrice = PlanService.getPlanPriceforUtilizer("Utilizer",planDomain,requestedPlanName);			  
			   int priceValue = (int) planPrice;
			   
			    List<UtiliserProfileModel> utilizerdetails = accservice.getUtilizerDetails(uti_kycid);
				
				String fullName = utilizerdetails.get(0).getUti_first_name()+" "+utilizerdetails.get(0).getUti_middle_name()+" "+utilizerdetails.get(0).getUti_last_name();
				String emailId = utilizerdetails.get(0).getUti_email_id();
				
			    String mobileno = utilizerdetails.get(0).getUti_office_first_no();
			   
				   request.setAttribute("requestedPlanName", requestedPlanName);
				   request.setAttribute("planPrice", priceValue);			  
				   request.setAttribute("fullName", fullName);
				   request.setAttribute("emailId", emailId);
				   request.setAttribute("mobileno", mobileno);
				   		  
			    session.setAttribute("requestedPlan", requestedPlanName);
			   
			    FeaturesDetailsModel FeaturesDetailsModels = new FeaturesDetailsModel();
				 FeaturesDetailsModels.setFeature_id(uti_Ind_id);
				 
				 Map<String, Object> model = new HashMap<String, Object>();
				 model.put("viewplan", prepareviewplan(PlanService.getUtilizerCurrentplan(FeaturesDetailsModels)));		
				 
				 
				   //Save the records before payment 
				  
						   if(session.getAttribute("paymentId")==null)
						   {
							   PaymentDetailsModel payu = new PaymentDetailsModel();
							   
							   payu.setPayment_from("Utilizer");
							   payu.setPayment_done_by(fullName);
							   payu.setPayment_amount(priceValue+"");
							   payu.setPayment_date(new Date());
							   payu.setPayment_status("Pending");
							   payu.setPayment_info_emailId(emailId);
							   payu.setPayment_info_mobileno(mobileno+"");		   		   
							   payu.setPayment_info_ipaddress(addressObject.getIPddress(request));
						  	   	   
							   String planStaus = PlanService.savePaymentRecords(payu);
							   session.setAttribute("paymentId", PlanService.getUniquePaymentId(payu));			  
						   }
						   					
				  //End of Saving the records before payment 
				   
			   return new ModelAndView("utilizerclenseeCheckOut",model);
		   }
	    
	    
	    
	    @RequestMapping(value = "/uticheckoutSuccess", method = RequestMethod.POST)
		public String utiCheckoutSuccess(HttpSession session,HttpServletRequest request) {
			
	    	
	    	
	    	try
	    	{
				   String paymentAmount = request.getParameter("amount");
				   String paymentStatus = request.getParameter("status");
				   String paymentTranId = request.getParameter("txnid");
				   int paymentId = (int) session.getAttribute("paymentId");
				   
				   PlanService.updatePaymentStatus(paymentId,paymentAmount,paymentStatus,paymentTranId);
			   
				   session.removeAttribute("paymentId");
				  				   		 
		            //  Do Clensee operation as per the success result
				   				   
				    int uti_Ind_id= (int)session.getAttribute("uti_Ind_id");
				   
				    String requestedPlanName = (String)session.getAttribute("requestedPlan");
					
				       
					// Track Activity 
						
						String currentPlanName = PlanService.getUtilizerplanname(uti_Ind_id);
						   
						     String activity = "";
						     if(currentPlanName.equals("Platinum"))
						     {
						    	 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
						     }
						     else if(currentPlanName.equals("Gold"))
						     {
						    	 if(requestedPlanName.equals("Platinum"))
						    	 {
						    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
						    	 }
						    	 else
						    	 {
						    		 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
						    	 }			    	 
						     }
						     else if(currentPlanName.equals("Silver"))
						     {
						    	 if(requestedPlanName.equals("Platinum"))
						    	 {
						    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
						    	 }
						    	 else if(requestedPlanName.equals("Gold"))
						    	 {
						    		 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
						    	 }
						    	 else
						    	 {
						    		 activity = "Your plan has been successfully downgraded to "+requestedPlanName;
						    	 }	
						     }
						     else
						     {
						    	 activity = "Your plan has been successfully upgraded to "+requestedPlanName;
						     }
							   						     
								String uti_kycid = (String)session.getAttribute("KycId");
							    int uti_indid = PlanService.findUtiid(uti_kycid);
						        
							    UtilizerActivityModel actModel = new UtilizerActivityModel();
								 
							    actModel.setUti_ind_id(uti_indid);
								actModel.setActivity_details(activity);
								actModel.setActivity_date(new Date());
								 
								utiservice.saveActivityDetails(actModel);
								
								
							if(paymentStatus.equals("success"))	
							{
								String migrateStatus = PlanService.migrateUtilizerPlan(requestedPlanName,uti_Ind_id);
							     
								UtilizerPlanDetailsModel utiPlanModel = new UtilizerPlanDetailsModel();
								utiPlanModel.setUti_ind_id(uti_Ind_id);				
								utiPlanModel.setUti_activate_plan(requestedPlanName);
								utiPlanModel.setCr_date(new Date());
									
								String planActiveStatus = utiservice.migrateUtilizerPlanDetails(utiPlanModel);									
							}
															
						// End of Track Activity  
					    
						
								
						// Mail to user as a plan changed Status
							
							 List<UtiliserProfileModel> utilizerdetails = accservice.getUtilizerDetails(uti_kycid);
							
							 String reciverName = utilizerdetails.get(0).getUti_first_name()+" "+utilizerdetails.get(0).getUti_middle_name()+" "+utilizerdetails.get(0).getUti_last_name();
							 String reciverEmailId = utilizerdetails.get(0).getUti_email_id();
								
							 String planChangeMail = profileinfoService.utilizerPlanChangeMail(reciverName,reciverEmailId,requestedPlanName);
							
							 request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
				     	     
							 
							  // End of mailing to user as a plan changed Status
							
						     	    	
										// Sending Invoice to user from Clensee side
										     	
								     	    	
								     	    //Save Invoice Details 
								     	    	
								     	       Date date = new Date(); // your date
								     	       Calendar cal = Calendar.getInstance();
								     	       cal.setTime(date);
								     	       int year = cal.get(Calendar.YEAR);
								     	       int month = cal.get(Calendar.MONTH);
								     	       int day = cal.get(Calendar.DAY_OF_MONTH);
								     	       int currentTime = cal.get(Calendar.HOUR);
	     	       					     	       
								     	       int lastInvoiceNumber = PlanService.getLastInvoiceNumber();
								     	       int randomNumber = lastInvoiceNumber+uti_Ind_id;
								     	      
								     	       String uniqueInvoiceId = "CLE"+year+month+day+currentTime+randomNumber;
								     	       
								     	       
								     	    	ClenseeInvoiceModel invoiceData = new ClenseeInvoiceModel();
								     	    	invoiceData.setInvoice_id(uniqueInvoiceId);
								     	    	invoiceData.setClensee_unique_id(uti_Ind_id);
								     	    	invoiceData.setInvoice_category("Utilizer");
								     	    	invoiceData.setInvoice_done_for(reciverName);
								     	    	invoiceData.setInvoice_generated_date(date);
								     	    	invoiceData.setInvoice_payment_amount(paymentAmount);
								     	    	invoiceData.setInvoice_description("Plan migration from "+currentPlanName+" to "+requestedPlanName);
								     	    	
								     	    	
								     	    	String invoiceSavingStatus = PlanService.saveInvoiceDetails(invoiceData);
								     	    	
								     	   
								     	   // End of saving Invoice details 	
								     	    	
							     	    if(invoiceSavingStatus.equals("success"))
							     	    {
							     	    	
											     String sendInvoiceEmailtoUtilizer = profileinfoService.utilizerInvoiceMail(day,month,year,uniqueInvoiceId,paymentAmount,currentPlanName,reciverName,reciverEmailId,requestedPlanName);
											     
											     request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
									     	     					     	
								         }	
										     	    	
									    // End of sending invoice to user from clensee side.
								
				   // End of Clensee opration as per the success result
				   
			      
			      return "utilizerCheckOutSuccess";
					 
			   }
			   catch(Exception e)
			   {
				   System.out.println("Exception due to="+e.getMessage());
				   
				   return "utilizerCheckOutSuccess";
			   }
		 
		   
		   }
		   
		   
		   @RequestMapping(value = "/uticheckoutFailure", method = RequestMethod.POST)
		   public String utiCheckoutFailure(HttpSession session,HttpServletRequest request) {
			
				   String paymentAmount = request.getParameter("amount");
				   String paymentStatus = request.getParameter("status");
				   String paymentTranId = request.getParameter("txnid");
				   int paymentId = (int) session.getAttribute("paymentId");
				   
				   PlanService.updatePaymentStatus(paymentId,paymentAmount,paymentStatus,paymentTranId);
			   
			      return "utilizerCheckoutFailure";
		   }
		   
	    
	    
	    
	    
	    
	    // End of Utilizer plan Change by payment API 
	    
	   
	    @RequestMapping(value = "/changeUti_plan", method = RequestMethod.POST)
		public @ResponseBody String getChnageUtiPlan(HttpSession session,HttpServletRequest request) {
			
	    	 int uti_Ind_id = (int)session.getAttribute("uti_Ind_id");	   
		     String requestedPlanName = request.getParameter("planName");
			 
		     String migrateStatus = PlanService.migrateUtilizerPlan(requestedPlanName,uti_Ind_id);
		     
		        UtilizerPlanDetailsModel utiPlanModel = new UtilizerPlanDetailsModel();
				utiPlanModel.setUti_ind_id(uti_Ind_id);				
				utiPlanModel.setUti_activate_plan(requestedPlanName);
				utiPlanModel.setCr_date(new Date());
				
				String planActiveStatus = utiservice.migrateUtilizerPlanDetails(utiPlanModel);
				
		     String uti_kycid = (String)session.getAttribute("KycId");
		     int uti_indid = PlanService.findUtiid(uti_kycid);
	         
		     UtilizerActivityModel actModel = new UtilizerActivityModel();
			 
		     actModel.setUti_ind_id(uti_indid);
			 actModel.setActivity_details("You had migrated your plan to "+requestedPlanName+" on "+new Date());
			 actModel.setActivity_date(new Date());
			 
			 utiservice.saveActivityDetails(actModel);
			 
		     return migrateStatus;
		}
	    
	    
	    
	    @RequestMapping(value="/utilizerusageDetails" , method = RequestMethod.GET)
	    public ModelAndView getUtilizerUsageDetails(HttpServletRequest request,HttpSession session)
	    {
	    
            session = request.getSession(false);
			
			if (session.getAttribute("uti_Ind_id") == null)
			{						 
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("uti_Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("login_utiliser");
			}			
			else
			{
				String uti_kycid = (String)session.getAttribute("KycId");
			    int uti_indid = utiservice.findUtiid(uti_kycid);
		    	
			    
			    int uti_Indid = (int) session.getAttribute("uti_Ind_id");	   		
			    String planName = PlanService.getUtilizerplanname(uti_Indid);			
			    String domainName = PlanService.getUtilizerPlanDomain(uti_Indid);
			    
				String restrictStatus = "";
				
				String featuresIdList =  PlanService.getPlanFeatureIdDetailsForUtilizer(planName,domainName,utilizerCategory);
				String featuresListArray[] = featuresIdList.split(",");
				
				request.setAttribute("currentPlan", planName);
				
			    // usage details of Viewed documents 
				
			    String viewStatus = "";		   
			    int viewedDocumentinthisMonth = utiservice.viewedDocumentinthisMonth(uti_kycid);
			    
			    for(int i=0;i<featuresListArray.length;i++)
				{					
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("Doc  Accessory of Employees and customers"))
					{
						viewStatus += PlanService.getFeatureValue(featureId);
					}
				}		
			    
			    String docclean = viewStatus.replaceAll("\\D+","");
				int noOfViewDocAsPerPlan = Integer.parseInt(docclean);
				
				request.setAttribute("noOfViewDocAsPerPlan", noOfViewDocAsPerPlan);			
			    request.setAttribute("viewedDocumentinthisMonth",viewedDocumentinthisMonth);
			    
				
				// Profile viewing Usage Details
				
				for(int i=0;i<featuresListArray.length;i++)
				{					
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("Profile Viewing"))
					{
						restrictStatus += PlanService.getFeatureValue(featureId);
					}
				}		
							
				if(!planName.equalsIgnoreCase("Basic")) 
				{			
					 String profileClean = restrictStatus.replaceAll("\\D+","");
					 int noOfFullProfileAsPerPlan = Integer.parseInt(profileClean);
					 request.setAttribute("noOfFullProfileAsPerPlanForUtilizer", noOfFullProfileAsPerPlan);
					 
					 int noOfFullProfileAsOfNow = PlanService.noOfFullProfileAsOfNowOfUtilizer(uti_Indid);
					 request.setAttribute("noOfFullProfileAsOfNowForUtilizer",noOfFullProfileAsOfNow);				 				 				 
				}
			    
				// SMS  Usage Details
				
				String smsStatus = "";
				for(int i=0;i<featuresListArray.length;i++)
				{					
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("SMS"))
					{
						smsStatus += PlanService.getFeatureValue(featureId);
					}
				}	
			    
				String smsclean = smsStatus.replaceAll("\\D+","");
				int noOfSMSAsPerPlan = Integer.parseInt(smsclean);
				request.setAttribute("noOfSMSAsPerPlan", noOfSMSAsPerPlan);
				 
				int noOfSMSAsOfNow = PlanService.noOfSMSAsOfNowAsOfNowOfUtilizer(uti_Indid);
				request.setAttribute("noOfSMSAsOfNow",noOfSMSAsOfNow);			
				
				Map<String, Object> model = new HashMap<String, Object>();			
				model.put("utilizerProfilePic",prepareProfilePicBean(utiservice.getLatestProfilePic(uti_Indid)));
									
				int noOfAwaitedDocuments = utiservice.getNoOfAwaitedRequets(uti_kycid);
				int noOfRecivedDocuments = utiservice.getNoOfRecivedDocuments(uti_kycid);
				
				request.setAttribute("noOfAwaitedDocuments", noOfAwaitedDocuments+"");
				request.setAttribute("noOfRecivedDocuments", noOfRecivedDocuments+"");		
				
		        return new ModelAndView("utilizer_usage_details",model);
			}	    		    	
	    }
	   
	    
	    
	
	/* End of Utilizer Plan Controller Methods */
	
	   
	   
	   
	   
	@RequestMapping(value = "/planhasfeature", method = RequestMethod.POST)
	public @ResponseBody String planhasfeature1(HttpSession session,HttpServletRequest request,
			@ModelAttribute("command") PlanDetailsModel PlanDetailsModel,
				BindingResult result) {
		
		
		PlanDetailsModel.setFeature_ids(request.getParameter("feature_id"));
		PlanService.getplanhasfeature(PlanDetailsModel);
		return (PlanService.getplanhasfeature(PlanDetailsModel)).toString();
			
	}
	
	@RequestMapping(value = "/planhasfeatureorganise", method = RequestMethod.POST)
	public @ResponseBody String planhasfeatureorganise1(HttpSession session,HttpServletRequest request,@ModelAttribute("command") PlanDetailsModel PlanDetailsModel,
				BindingResult result) {
		
		
		PlanDetailsModel.setFeature_ids(request.getParameter("plan_id"));
		getplanhasfeatureorganise(PlanService.getplanhasfeatureorganise(PlanDetailsModel));
		return (getplanhasfeatureorganise(PlanService.getplanhasfeatureorganise(PlanDetailsModel))).toString();
		
	
	}
	
	  private List<String> getplanhasfeatureorganise(List<FeaturesDetailsModel> list) {
		
		   List<String> selectedvalue = new ArrayList<String>();
		 	List<FeaturesDetailsBean> bean = null;
		 	
			if (list!= null && !list.isEmpty()) {
				bean = new ArrayList<FeaturesDetailsBean>();
				FeaturesDetailsBean beans = null;

				for (FeaturesDetailsModel PlanDetailsModel : list) {
		            beans = new FeaturesDetailsBean();
		            
					beans.setFeature_name(PlanDetailsModel.getFeature_name());
					selectedvalue.add(PlanDetailsModel.getFeature_name());			
					bean.add(beans);
				}
			}
				return selectedvalue;
		} 
					
	  
	  
	@RequestMapping(value = "/createfeature", method = RequestMethod.GET)
	public ModelAndView createfeature1(HttpServletRequest request,HttpSession session) {
		 
		if(((String)session.getAttribute("adminId"))!=null)
        {
			return new ModelAndView("admin_myFeature");
        }
        else
        {
        	request.setAttribute("status","failure");
			return new ModelAndView("adminLoginPage");		
        }	
		
	}
	
	
	
	
   
    
   
    
}
      