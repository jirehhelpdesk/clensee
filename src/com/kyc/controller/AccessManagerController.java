package com.kyc.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.AuthenticationFailedException;
import javax.mail.IllegalWriteException;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.bean.AccessDetailsBean;
import com.kyc.bean.CodesDetailsBean;
import com.kyc.bean.DocumentDetailsBean;
import com.kyc.bean.IndSignupBean;
import com.kyc.dao.AccessDao;
import com.kyc.model.AccessDetailsModel;
import com.kyc.model.CodesDetailsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.NonKycUserCodeDetailsModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.service.AccessService;
import com.kyc.service.DocService;
import com.kyc.service.IndSignupService;
import com.kyc.service.PlanService;
import com.kyc.service.ProfileinfoService;
import com.kyc.service.UtiliserService;
import com.kyc.util.AccessControl;
import com.kyc.util.FileEncryptionDecryptionUtil;
import com.kyc.util.SendEmailUtil;
import com.kyc.util.SendSms;

@Controller
public class AccessManagerController {

	@Autowired
	private ProfileinfoService profileinfoService;
	
	@Autowired
	private AccessService accservice;
	
	@Autowired
	private DocService docservice;
		
	@Autowired
	private IndSignupService signupService;
	
	@Autowired
	private PlanService PlanService;
	
	// Config for SMS Service.
	
	static final Logger LOGGER = Logger.getLogger(AccessManagerController.class);
    
	
	ResourceBundle smsresource = ResourceBundle.getBundle("resources/smsParameters");
	
	String userName = smsresource.getString("userName");
	String password = smsresource.getString("password");
	String senderId = smsresource.getString("senderId");
	String smsMessage1 = smsresource.getString("notifynonKycUsersms");
	String smsMessage2 = smsresource.getString("notifyKycUsersms");
	String smscommonUrl = smsresource.getString("commonUrl");
	String smsfromDetails = smsresource.getString("fromDetails");
		   
    SendSms obj = new SendSms();
			
	// End of Config for SMS Service
		
    private static Logger log = Logger.getLogger(AccessManagerController.class);
	FileAppender loggerfile;	 
	FileHandler fh;
	
	ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
	String logDirectory=loggerFileDirectory.getString("loggerDirectoryDirectory");
    
	String kycMainDirectory = loggerFileDirectory.getString("fileDirectory");	
	File filePath = new File(kycMainDirectory);
    	
    // Preview Doc with in a session
       
    
    @SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewDocs_apply_code", method = RequestMethod.GET)
	@ResponseBody public byte[] previewDocs(HttpServletRequest request) throws IOException {
		
    	File filePath = new File(kycMainDirectory);
    	 
    	String year = request.getParameter("year");
    	String Ind_id = request.getParameter("ind_id");
		ServletContext servletContext = request.getSession().getServletContext();
		
		String docCategory = request.getParameter("docCategory");
		
		StringBuilder fPath = new StringBuilder(filePath.getPath()+"/");
		
		fPath = fPath.append(year + "/" + Ind_id );
		
		if(StringUtils.isNotBlank(docCategory))
		{
			if(docCategory.equals("KYC_DOCUMENT"))
			{
				fPath = fPath.append("/Document/KYC_DOCUMENT/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("ACADEMIC_DOC"))
			{
				fPath = fPath.append("/Document/ACADEMIC_DOC/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("Financial_DOC"))
			{
				fPath = fPath.append("/Document/Financial_DOC/" + request.getParameter("fileName"));
			}
			else if(docCategory.equals("Employment_DOC"))
			{				
				fPath = fPath.append("/Document/Employement_DOC/" + request.getParameter("fileName"));
			}
			else
			{
				fPath = fPath.append("/Profile_Photo/" + request.getParameter("fileName"));
			}			
		}
		
		InputStream in;
		
		try {
			
			if (AccessControl.isValidSession(request)) {
				
				FileInputStream docdir = new FileInputStream(fPath.toString());
				
				
				if (docdir != null) {
					
					return IOUtils.toByteArray(docdir);
					
				} else {
					return null;
				}

			} else {
				
				in = servletContext.getResourceAsStream("/resources/images/Access_Denied.jpg");
				if (in != null) {
					
					return IOUtils.toByteArray(in);
					
				} else {
					return null;
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} 
	}
    
    
    // End of Code Preview Doc with in a session
    
			
	@RequestMapping(value = "/recipientDetails", method = RequestMethod.GET)
	public ModelAndView generateRecDetails() {
		
		
		return new ModelAndView("recipientDetails");
		
	}
	

	@RequestMapping(value ="/generateCode", method = RequestMethod.GET)
	public String genetratecode(HttpSession session,HttpServletRequest request) {
		
		    session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 ////session.invalidate();
				 
				 return "individualPage";	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 ////session.invalidate();
				 
				 return  "individualPage";	
			}
			else
			{	
				int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
				request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
				 
		        return "individualSharingManager";		    
			}
	}

	@RequestMapping(value = "/generateCode", method = RequestMethod.POST)
	public ModelAndView getGenCode() {
		
		return new ModelAndView("generate_code");
	}


	
	@RequestMapping(value ="/recipientdetailsforSharing", method = RequestMethod.GET)
	public String getRecpientDetailsIndirect(HttpServletRequest request,
			HttpSession session) throws IOException {

		String pattern = request.getParameter("newPatternName");
		
		request.setAttribute("pattern",pattern);	
		
		String DATE_FORMAT_NOW = "dd-MM-yyyy";
	    Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String stringDate = sdf.format(date );
		stringDate += "-recpientCodedetails";
		   
		loggerfile = new FileAppender(new SimpleLayout(),""+logDirectory+"/"+stringDate+".log");
		log.addAppender(loggerfile);
		loggerfile.setLayout(new SimpleLayout());
		  
		log.info("codegenrated details="+pattern);
		log.info("code generated at="+new Date());
		
		int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
		request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
				
		return "individualrecipeintDetails";
	}
	
			
	@RequestMapping(value = "/applyCode", method = RequestMethod.GET)
	public ModelAndView genetrateApplycode(HttpServletRequest request,
			HttpSession session) {

			session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				 
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return new ModelAndView("individualPage");	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //session.invalidate();
				 
				 return  new ModelAndView("individualPage");	
			}
			else
			{								
					IndSignupModel individual = new IndSignupModel();
					individual.setKycid((String)session.getAttribute("Ind_id"));
			
					Map<String, Object> model = new HashMap<String, Object>();
					model.put("applyCode",prepareApplyCodemodel(accservice.findShareedCode(individual)));
					
					int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
					request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
					
					//int individualInd_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
												
																						
					return new ModelAndView("individual_applycode", model);
			}
	}
	
	private List<CodesDetailsBean> prepareApplyCodemodel(
			List<CodesDetailsModel> applyList) {

		List<CodesDetailsBean> bean = null;

		if (applyList != null && !applyList.isEmpty()) {
			
			bean = new ArrayList<CodesDetailsBean>();
			CodesDetailsBean beans = null;
			for (CodesDetailsModel apply : applyList) {
				
				beans = new CodesDetailsBean();
				
				beans.setCode_id(apply.getCode_id());
				beans.setCode_generated(apply.getCode_generated());
				beans.setCr_date(apply.getCr_date());
				beans.setCode_sender(apply.getCode_sender());
				beans.setCr_by(apply.getCr_by());
				beans.setCode_description(apply.getCode_description());
				beans.setCode_generated(apply.getCode_generated());
				bean.add(beans);
				
			}
		}
		
		return bean;
		
	}
	 
	@RequestMapping(value = "/sendingPattern", method = RequestMethod.POST)
	public @ResponseBody String sendingPattern(HttpServletRequest request,
			HttpSession session) {

		int id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		
		String senderKycId = accservice.findIndKycID(id);
		String reciverkycid = request.getParameter("kycid");
		reciverkycid = reciverkycid.replaceAll(" ","");
		int kycInd_id = accservice.findid(reciverkycid);
		
		int year = accservice.findyear(reciverkycid, kycInd_id);
		String firstName = accservice.getFirstName(kycInd_id);
		String profilePic = accservice.getProfilePic(kycInd_id);
		
		
		String shareData = senderKycId+"$"+reciverkycid+"$"+year+"$"+firstName+"$"+profilePic;
		
		
		return shareData;
	}

	
	@RequestMapping(value = "/search1_profile", method = RequestMethod.POST)
	public @ResponseBody
	String searchRecipient(HttpServletRequest request) {
		IndSignupModel codedetail = new IndSignupModel();

		codedetail.setMobileno(request.getParameter("searchby"));
		codedetail.setPassword(request.getParameter("doctype"));
		List<String> lsdata = new ArrayList<String>();
		lsdata = accservice.findAsperType(codedetail);
		
		String strresponse = lsdata.toString().substring(1,
				lsdata.toString().length() - 1);
		return strresponse;
	}

	
	@SuppressWarnings("unused")
	private List<IndSignupBean> prepareCodeDetailsModel(
			List<IndSignupModel> codedetails) {

		List<IndSignupBean> bean = null;

		if (codedetails != null && !codedetails.isEmpty()) {
			bean = new ArrayList<IndSignupBean>();
			IndSignupBean beans = null;

			for (IndSignupModel codedetail : codedetails) {
				beans = new IndSignupBean();

				beans.setEmailid(codedetail.getEmailid());
				beans.setFirstname(codedetail.getFirstname());
				beans.setLastname(codedetail.getLastname());
				beans.setKycid(codedetail.getKycid());

				bean.add(beans);
			}
		}
		return bean;
	}
	
	
	@RequestMapping(value = "/generateRecipient", method = RequestMethod.POST)
	public ModelAndView displaysearchRecipient(HttpServletRequest request) {
		
		IndSignupModel codedetail = new IndSignupModel();

		codedetail.setMobileno(request.getParameter("searchby"));
		codedetail.setPassword(request.getParameter("name"));
				
		String ind_Search = accservice.findUser(codedetail);
					
		request.setAttribute("ind_Search",ind_Search);
		
		return new ModelAndView("search_ind_for_sharing_code");
	}

		
	@RequestMapping(value = "/latestDocumentType", method = RequestMethod.POST)
	public @ResponseBody
	String hello(@RequestParam(value = "doctype") String doctype,HttpServletRequest request, HttpSession session) {
		
		
		request.setAttribute("doctype", request.getParameter("doctype"));		
		request.setAttribute("kyc_ind_id", session.getAttribute("Ind_id"));
		//List<String> ls = new ArrayList<String>();
		
		String documentType = request.getParameter("doctype");
		String individualId = (String) session.getAttribute("Ind_id");
		int indId = Integer.parseInt(individualId);
		String latestDocumentName = accservice.getLatestDocumntNameOfThisType(documentType,indId);
			
		if(latestDocumentName.length()>0)
		{
		    return latestDocumentName;
		}
		else
		{
			return "No Document you have uploaded";
		}
	}
		
	@RequestMapping(value = "/validateNonClenseeuserEmailId", method = RequestMethod.POST)
	  public @ResponseBody String validateNonKycUserEmailId(HttpServletRequest request) {
		
		String validateStatus = "";
	    String SharedEmailId = request.getParameter("SharedEmailId");
	    
	    validateStatus = accservice.getValidateNonKycUserEmailId(SharedEmailId);
		
		return validateStatus;
	}
	
		
	
	@RequestMapping(value = "/sendNonKycUser", method = RequestMethod.POST)
	public @ResponseBody String doSendEmail(HttpServletRequest request,HttpSession session) throws IOException, IllegalWriteException, AuthenticationFailedException, SendFailedException {
	      
		
		/* Logger format */
		
		
			String DATE_FORMAT_NOW = "dd-MM-yyyy";
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
			String stringDate = sdf.format(date );
			stringDate += "-shareDoctoNonClenseeUser";
			
			ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
			String logDirectory=loggerFileDirectory.getString("loggerDirectoryDirectory");
			loggerfile = new FileAppender(new SimpleLayout(),""+logDirectory+"/"+stringDate+".log");
			
			log.addAppender(loggerfile);
			loggerfile.setLayout(new SimpleLayout());
		 
			
	     /* End of Logger format */
		 
			
			
			
		    int id = Integer.parseInt((String)session.getAttribute("Ind_id"));
			String senderName = accservice.getSenderName(id);
			
            String recipientAddress = request.getParameter("nonKycUser");	        
	        String subject = request.getParameter("shareSubject");
	        String name = request.getParameter("nonKycName");
	        String description = request.getParameter("shareComment");
	        	        
	        String reciver_Mobile_no = request.getParameter("nonKycPhno");
	        
			NonKycUserCodeDetailsModel  nonUserModel = new NonKycUserCodeDetailsModel();
			
			nonUserModel.setSender_ind_id(id);
			nonUserModel.setSender_name(senderName);
			nonUserModel.setReciver_email_id(recipientAddress);
			nonUserModel.setReciver_name(name);
			nonUserModel.setDescription(description);
			
			String codeDetails = request.getParameter("sahredCode");
			codeDetails = codeDetails.substring(0,codeDetails.length()-1);
			
			String codeArray[] = codeDetails.split(",");
			String generateFormatedCode = "";
			
			String kycCode = "KYC:";
			String academicCode = "ACADEMIC:";
			String financialCode = "Financial:";
			String empCode = "Employment:";
			
			for(int c=0;c<codeArray.length;c++)
			{
			     String codeArr1[] = codeArray[c].split("--");
			     if(codeArr1[0].equals("KYC"))
			     {
			    	 String selectedKycDoc = codeArr1[1];
			    	 selectedKycDoc = selectedKycDoc.replaceAll("#", ",");
			    	 
			    	 //kycCode += codeArr1[1] + ",";
			    	 
			    	 kycCode += selectedKycDoc.split(" -")[1] + ",";
			     }
			     if(codeArr1[0].equals("ACADEMIC"))
			     {
			    	 academicCode += codeArr1[1] + ",";
			     }
			     if(codeArr1[0].equals("Financial"))
			     {
			    	 financialCode += codeArr1[1] + ",";
			     }
			     if(codeArr1[0].equals("Employment"))
			     {
			    	 empCode += codeArr1[1] + ",";
			     }		     
			}
			
			kycCode = kycCode.substring(0, kycCode.length()-1);
			academicCode = academicCode.substring(0, academicCode.length()-1);
			financialCode = financialCode.substring(0, financialCode.length()-1);
			empCode = empCode.substring(0, empCode.length()-1);
			
			generateFormatedCode = kycCode + "/" + academicCode + "/" + financialCode + "/" + empCode;
			
			nonUserModel.setCode_details(generateFormatedCode);
			nonUserModel.setSend_date(new Date());
			
			String resetStatus = "";
			
			if("success".equals(accservice.getSaveNonKycCodeDetails(nonUserModel)))
			{
						
				        String sharingMessage = request.getParameter("shareComment");	  
				        
				        ResourceBundle resource = ResourceBundle.getBundle("resources/SharingDocToIndividual");
			     		

				         String headingImage=resource.getString("headingImage");
				    	 String belowleftImage=resource.getString("belowleftImage");
				    	 String belowrightImage=resource.getString("belowrightImage");
				    	 String mailicon=resource.getString("mailicon");
				    	 String contacticon=resource.getString("contacticon");
				    	 
				    	 String emailHeading=resource.getString("emailHeading");
				    	 String contactNo=resource.getString("contactNo");
				    	 String contactEmail=resource.getString("contactEmail");
				    	 
				    	 String sharingsubject=resource.getString("sharingsubject") +senderName;
						 String sharingmessage=resource.getString("sharingmessage");		
						 String aftermessageextraline = resource.getString("aftermessageextraline");
						 
				    	 String copyright=resource.getString("copyright");
				    	 String termscondition=resource.getString("termscondition");
				    	 String privacyPolicy=resource.getString("privacyPolicy");
				    	 
				    	 String facebookicon=resource.getString("facebookicon");
				    	 String twittericon=resource.getString("twittericon");
				    	 String googleicon=resource.getString("googleicon");
				    	 String linkedicon=resource.getString("linkedicon");
				    	 
				    	 String facebookLink=resource.getString("facebookLink");
				    	 String twitterLink=resource.getString("twitterLink");
				    	 String googlePlus=resource.getString("googlePlus");
				    	 String linkedIn=resource.getString("linkedIn");
				    	 
				    	 String clenseeLogo = resource.getString("clenseeLogo");
						 String sharingwelcome = resource.getString("sharingwelcome");
						 String clickhere = resource.getString("clickhere");
						 String actionUrl = resource.getString("actionUrl");
						 
				         StringBuilder text = new StringBuilder();
				        
				         
				         /* Sending Emails to Register user */
					       
					        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
					        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
					       
					        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
					       
					        /* Header */
					        
					        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
					        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
					        text.append("</tr></table></td></tr></table>");
					       
					        /* End of Header */
					        
					        /* Header-2 */
					        
					        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
					        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
					        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
					        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
					        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
					        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
					        text.append("</p></td></tr></table></td></tr></table>");
					        
					        /* End of Header-2 */
					        
					        
					        /* BODY PART */
					        
					        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
					        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
					        text.append("<h3 style='color: #004879;'>Dear "+name+",</h3>");
					        text.append("<p style='text-align: justify'>"+sharingwelcome+"</p>");
					        text.append("<p style='text-align: justify'>"+sharingmessage+"</p>");
					        
					       /* if(!sharingMessage.isEmpty())
					        {
					        	text.append("<br><p style='text-align: justify'>"+sharingMessage+"</p>");
					        }*/
					        
					        text.append("<br><p>"+aftermessageextraline+"<br><br>");
					        text.append("<a href="+actionUrl+" target='_target' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a></p>");				        
					        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
					        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
					        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
					        text.append("</tr></table></td></tr>");
					        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
					        text.append("</table> ");
					        
					        
					        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
					        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
					        
					        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
					        
					        text.append("<tr><td>");
					        
					        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
					        text.append("</td></tr></table>");
					        
					        
					        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
					        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
					        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
					        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
					        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
					        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
					        
					        
					        text.append("</tr>");
					        text.append("</table></td></tr></table></td></tr></table>");
					        text.append("</body>");
					        text.append("</html>");
					       
					        /* END OF BODY PART */
					        
					        
					       /* End of Sending Emails to Register user */
					    				         
				    
	       	         SendEmailUtil emailObject = new SendEmailUtil();
	     	        
	     			resetStatus = emailObject.getEmailSent(recipientAddress,sharingsubject,text.toString());
	     			
					request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
	     	     
	     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
	     	    	
	     	    	if(resetStatus.equals("success"))
	     	    	{	    		
	     	    		emailReport.setEmail_id(recipientAddress);
	     	    		emailReport.setSubject(subject);
	     	    		emailReport.setMessage_body(text.toString());
	     	    		emailReport.setSent_date(new Date());
	     	    		emailReport.setSent_report("0");
	     	    	}
	     	    	else
	     	    	{
	     	    		emailReport.setEmail_id(recipientAddress);
	     	    		emailReport.setSubject(subject);
	     	    		emailReport.setMessage_body(text.toString());
	     	    		emailReport.setSent_date(new Date());
	     	    		emailReport.setSent_report("1");	    			    		
	     	    	}
	     	    	
	     	    	profileinfoService.setEmailStatusReport(emailReport);
				     
					     
					     if(!reciver_Mobile_no.equals(""))
					        {					    	 
					    	   String smsCriteria = checkSMSStatus(request,session);
					    	  
					    	   if(smsCriteria.equals("Allow"))
					    	   {
					    		   String mobileNo = ""+reciver_Mobile_no;
							        String asPerclientMessag = "Dear "+name+",  "+senderName+"         "+smsMessage1;
							        
							        String smsStatus = obj.SMSSender(userName,password,asPerclientMessag,senderId,mobileNo,"0");   
							        
							        SmsSentStatusModel smsModel = new SmsSentStatusModel();
							        
							        if(smsStatus.equals("success"))
							        {
							        	smsModel.setUnique_id(id);
							        	smsModel.setSms_belongs_to("Individual");
							        	smsModel.setMobile_no(reciver_Mobile_no);
							        	smsModel.setMessage_body(asPerclientMessag);
							        	smsModel.setSent_date(new Date());
							        	smsModel.setSent_report("0");
							        }
							        else
							        {
							        	smsModel.setUnique_id(id);
							        	smsModel.setSms_belongs_to("Individual");
							        	smsModel.setMobile_no(reciver_Mobile_no);
							        	smsModel.setMessage_body(asPerclientMessag);
							        	smsModel.setSent_date(new Date());
							        	smsModel.setSent_report("1");
							        }
							       
							        profileinfoService.setSmsStatusReport(smsModel);
					    	   }					        	
					        }
					        
					        // Tracking Activity Details
					        
						        String Kyc_id= (String)session.getAttribute("Kyc_id");
								Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
								Profileupdatemodel.setProfileupdatedetail("you had shared documents to "+name+" a non-register Clensee user");
								Profileupdatemodel.setProfileupdatetime(new Date());
								Profileupdatemodel.setType_of_alert("Access manager Updated");
								Profileupdatemodel.setMore_about_it("Event done in generate code of Access Manager");
							    Profileupdatemodel.setKyc_id(Kyc_id);
								profileinfoService.profileupdate(Profileupdatemodel);
		        
			                 // End Of Tracking Activity Details
			
								
			    return "success";     
			}	        
			else
			{
				return "failure";
			}
	        	        	        
	    }
	
	
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/shareDoctoClenseeUser", method = RequestMethod.POST)
	public @ResponseBody String saveCodeDetails(HttpServletRequest request,HttpSession session) throws IOException, IllegalWriteException, AuthenticationFailedException, SendFailedException {

		CodesDetailsBean codesbean = new CodesDetailsBean();

		int id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		
		String CodeGenerate = "@kycc2o3de1000";
		
        int lastGeneratedCodeId = accservice.getLastGeneratedCodeId();
		
        if(lastGeneratedCodeId>0)
        {
        	String lastCode = accservice.getLastGeneratedCode();
        	
        	String coseAry[] = lastCode.split("e");
        	
        	int nextIndex = Integer.parseInt(coseAry[1]);
        	nextIndex = nextIndex + 1;
        	String nextGeneratedCode = coseAry[0] + "e" + nextIndex;
        	
        	codesbean.setCode_generated(nextGeneratedCode);
        }
        else
        {
        	codesbean.setCode_generated(CodeGenerate);
        }
				
		codesbean.setCode_description(request.getParameter("des"));
		codesbean.setCode_sender(request.getParameter("sender"));
		codesbean.setCode_receiver(request.getParameter("reciver"));
		codesbean.setCr_date(new Date());
		String kid = request.getParameter("sender");
		String name = accservice.findCrName(kid);
		codesbean.setCr_by(name);
		  		
		String pattern = request.getParameter("codePattern");
		String codePattern = pattern.substring(0, pattern.length());
		
		String codeArray[] = codePattern.split(",");
		String generateFormatedCode = "";
		
		String kycCode = "KYC:";
		String academicCode = "ACADEMIC:";
		String financialCode = "Financial:";
		String empCode = "Employment:";
		
		for(int c=0;c<codeArray.length;c++)
		{
		     String codeArr1[] = codeArray[c].split("--");
		     if(codeArr1[0].equals("KYC"))
		     {
		    	 String selectedKycDoc = codeArr1[1];
		    	 selectedKycDoc = selectedKycDoc.replaceAll("#", ",");
		    	 
		    	 kycCode += selectedKycDoc.split(" -")[1] + ",";
		     }
		     if(codeArr1[0].equals("ACADEMIC"))
		     {
		    	 academicCode += codeArr1[1] + ",";
		     }
		     if(codeArr1[0].equals("Financial"))
		     {
		    	 financialCode += codeArr1[1] + ",";
		     }
		     if(codeArr1[0].equals("Employment"))
		     {
		    	 empCode += codeArr1[1] + ",";
		     }		     
		}
		
		kycCode = kycCode.substring(0, kycCode.length()-1);
		academicCode = academicCode.substring(0, academicCode.length()-1);
		financialCode = financialCode.substring(0, financialCode.length()-1);
		empCode = empCode.substring(0, empCode.length()-1);
		
		generateFormatedCode = kycCode + "/" + academicCode + "/" + financialCode + "/" + empCode;
		codesbean.setCode_pattern(generateFormatedCode);

		CodesDetailsModel codemodel = prepareCodeModel(codesbean);

		String resetStatus = "";
		
		if ("success".equals(accservice.saveCodedetails(codemodel))) {
										
             ResourceBundle resource = ResourceBundle.getBundle("resources/SharingDocToIndividual");
		     		    
		     String headingImage=resource.getString("headingImage");
	    	 String belowleftImage=resource.getString("belowleftImage");
	    	 String belowrightImage=resource.getString("belowrightImage");
	    	 String mailicon=resource.getString("mailicon");
	    	 String contacticon=resource.getString("contacticon");
	    	 
	    	 String emailHeading=resource.getString("emailHeading");
	    	 String contactNo=resource.getString("contactNo");
	    	 String contactEmail=resource.getString("contactEmail");
	    	 
	    	 String sharingsubject=resource.getString("sharingsubject");
			 String sharingmessage=resource.getString("sharingmessage");		
			 String aftermessageextraline = resource.getString("aftermessageextraline");
			 
	    	 String copyright=resource.getString("copyright");
	    	 String termscondition=resource.getString("termscondition");
	    	 String privacyPolicy=resource.getString("privacyPolicy");
	    	 
	    	 String facebookicon=resource.getString("facebookicon");
	    	 String twittericon=resource.getString("twittericon");
	    	 String googleicon=resource.getString("googleicon");
	    	 String linkedicon=resource.getString("linkedicon");
	    	 
	    	 String facebookLink=resource.getString("facebookLink");
	    	 String twitterLink=resource.getString("twitterLink");
	    	 String googlePlus=resource.getString("googlePlus");
	    	 String linkedIn=resource.getString("linkedIn");
	    	 
	    	 String clenseeLogo = resource.getString("clenseeLogo");
			 String sharingwelcome = resource.getString("sharingwelcome");
			 String clickhere = resource.getString("clickhere");
			 String actionUrl = resource.getString("actionUrl");
			 
	    	 
							String reciverKycId = request.getParameter("reciver");
							int reciver_Ind_id = accservice.findid(reciverKycId);
							String reciverName = accservice.getReciverName(reciver_Ind_id);
							String reciverEmailId = accservice.getReciverEmailId(reciver_Ind_id);
							String reciver_Mobile_no = accservice.getReciverMobileNo(reciver_Ind_id);
							
							
							String giverMessage = request.getParameter("des");	        	        
					        
					        StringBuilder text = new StringBuilder();
					        
				        
					        
						    /* Sending Emails to Register user */
						       
					        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
					        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
					       
					        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
					       
					        /* Header */
					        
					        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
					        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
					        text.append("</tr></table></td></tr></table>");
					       
					        /* End of Header */
					        
					        /* Header-2 */
					        
					        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
					        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
					        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
					        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
					        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
					        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
					        text.append("</p></td></tr></table></td></tr></table>");
					        
					        /* End of Header-2 */
					        
					        
					        /* BODY PART */
					        
					        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
					        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
					        text.append("<h3 style='color: #004879;'>Dear "+reciverName+",</h3>");
					        text.append("<p style='text-align: justify'>"+sharingwelcome+"</p>");
					        text.append("<p style='text-align: justify'>"+sharingmessage+"</p>");
					        
					        /*if(!giverMessage.isEmpty())
					        {
					        	text.append("<br><p style='text-align: justify'>"+giverMessage+"</p>");
					        }
					        */
					        
					        text.append("<br><p>"+aftermessageextraline+"<br><br>");					        	
					        text.append("<a href="+actionUrl+" target='_blank' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
					        text.append("</p>");
					        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
					        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
					        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
					        text.append("</tr></table></td></tr>");
					        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
					        text.append("</table> ");
					        
					        
					        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
					        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
					        
					        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
					        
					        text.append("<tr><td>");
					        
					        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
					        text.append("</td></tr></table>");
					       
					        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
					        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
					        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
					        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
					        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
					        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
					        					        
					        text.append("</tr>");
					        text.append("</table></td></tr></table></td></tr></table>");
					        text.append("</body>");
					        text.append("</html>");
					       
					        /* END OF BODY PART */
					        
					        
					      /* End of Sending Emails to Register user */
					    
		       	         SendEmailUtil emailStatus = new SendEmailUtil();
		     	        
		       	        String subject = sharingsubject+" "+name;
		     			resetStatus = emailStatus.getEmailSent(reciverEmailId,subject,text.toString());
						request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
		     	     
		     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
		     	    	
		     	    	if(resetStatus.equals("success"))
		     	    	{	    		
		     	    		emailReport.setEmail_id(reciverEmailId);
		     	    		emailReport.setSubject(subject);
		     	    		emailReport.setMessage_body(text.toString());
		     	    		emailReport.setSent_date(new Date());
		     	    		emailReport.setSent_report("0");
		     	    	}
		     	    	else
		     	    	{
		     	    		emailReport.setEmail_id(reciverEmailId);
		     	    		emailReport.setSubject(subject);
		     	    		emailReport.setMessage_body(text.toString());
		     	    		emailReport.setSent_date(new Date());
		     	    		emailReport.setSent_report("1");	    			    		
		     	    	}
		     	    	
		     	    	profileinfoService.setEmailStatusReport(emailReport);
		     	    	
						
					     if(!reciver_Mobile_no.equals(""))
					        {
					    	   String smsCriteria = checkSMSStatus(request,session);
					    	  
					    	   if(smsCriteria.equals("Allow"))
					    	   {
					    	 
						        	String mobileNo = ""+reciver_Mobile_no;
							        String asPerclientMessag = "Dear "+reciverName+",  "+name+"         "+smsMessage2;
							        
							        String smsStatus = obj.SMSSender(userName,password,asPerclientMessag,senderId,mobileNo,"0");   
							        
	                                SmsSentStatusModel smsModel = new SmsSentStatusModel();
							        
							        if(smsStatus.equals("success"))
							        {
							        	smsModel.setUnique_id(id);
							        	smsModel.setSms_belongs_to("Individual");
							        	smsModel.setMobile_no(reciver_Mobile_no);
							        	smsModel.setMessage_body(asPerclientMessag);
							        	smsModel.setSent_date(new Date());
							        	smsModel.setSent_report("0");
							        }
							        else
							        {
							        	smsModel.setUnique_id(id);
							        	smsModel.setSms_belongs_to("Individual");
							        	smsModel.setMobile_no(reciver_Mobile_no);
							        	smsModel.setMessage_body(asPerclientMessag);
							        	smsModel.setSent_date(new Date());
							        	smsModel.setSent_report("1");
							        }
							        
							        profileinfoService.setSmsStatusReport(smsModel);
						        
					    	   }
					        }
						       
						        // Tracking Activity Details
						        
										        String Kyc_id= (String)session.getAttribute("Kyc_id");
												Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
												Profileupdatemodel.setProfileupdatedetail("you had shared documents to "+reciverName);
												Profileupdatemodel.setProfileupdatetime(new Date());
												Profileupdatemodel.setType_of_alert("Access manager Updated");
												Profileupdatemodel.setMore_about_it("Event done in generate code of Access Manager");
											    Profileupdatemodel.setKyc_id(Kyc_id);
												profileinfoService.profileupdate(Profileupdatemodel);
						        
								// End Of Tracking Activity Details
							
			return "success";			
		} 
		else
		{
			return "failure";
		}
	}
	
	private CodesDetailsModel prepareCodeModel(CodesDetailsBean codesbean) {

		CodesDetailsModel codemodel = new CodesDetailsModel();

		codemodel.setCode_generated(codesbean.getCode_generated());
		codemodel.setCode_description(codesbean.getCode_description());
		codemodel.setCode_sender(codesbean.getCode_sender());
		codemodel.setCode_receiver(codesbean.getCode_receiver());		
		
		accservice.getRecevierStatus(codesbean.getCode_receiver(),codesbean.getCode_sender());
		
		codemodel.setCr_by(codesbean.getCr_by());
		codemodel.setCr_date(codesbean.getCr_date());
		codemodel.setCode_pattern(codesbean.getCode_pattern());
		codemodel.setCode_status("1");
		codemodel.setApply_code_count("0");
		
		return codemodel;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/viewProfileInGC", method = RequestMethod.POST)
	public @ResponseBody
	String viewprofile(HttpServletRequest request, HttpSession session) {
		
		List<String> userDetail = new ArrayList<String>();
		
		String kycid = request.getParameter("kycid");
        
		IndSignupModel individual = new IndSignupModel();
		individual.setKycid(kycid);
        
		List<IndSignupModel> viewlist = accservice.viewUser(individual);
		for (int i=0;i<viewlist.size();i++) {
			userDetail.add(viewlist.get(i).getKycid());
			userDetail.add(viewlist.get(i).getFirstname());
			userDetail.add(viewlist.get(i).getLastname());
			userDetail.add(viewlist.get(i).getEmailid());
			userDetail.add(viewlist.get(i).getMobileno());			
		}
		
		return userDetail.toString().substring(1, userDetail.toString().length()-1);
	}
	
	@RequestMapping(value = "/showCode", method = RequestMethod.POST)
	public ModelAndView generateApplyedDocs(HttpServletRequest request,HttpSession session) {
		
		session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null)
		{			
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 
			 return new ModelAndView("individualPage");	
		}
		else if(((String)session.getAttribute("Ind_id")).equals(null))
		{
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 
			 return  new ModelAndView("individualPage");	
		}
		else
		{	
				int code = Integer.parseInt(request.getParameter("id"));
				String kycid = request.getParameter("kycid");
				String viewedkycid = request.getParameter("kycid");
				String documents = accservice.servicePattern(code);
						        		
				request.setAttribute("documents", documents);
				request.setAttribute("foldrid", kycid);
				request.setAttribute("codeName",request.getParameter("codename"));
				
				try 
				{
					  
					    String DATE_FORMAT_NOW = "dd-MM-yyyy";
					    Date date = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
						String stringDate = sdf.format(date );
						stringDate += "-showDocFromApplyedCode";
						   
						loggerfile = new FileAppender(new SimpleLayout(),""+logDirectory+"/"+stringDate+".log");
					
						log.addAppender(loggerfile);
						loggerfile.setLayout(new SimpleLayout());
												
						log.info("-"+date+"Applyed Code info="+kycid+"Code Name="+request.getParameter("codename"));
					    
						
						
						int individualInd_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
						
						String otherIndName = accservice.getReciverName(individualInd_id);
						
						String ownKycId = accservice.findIndKycID(individualInd_id);
						
						String accessStatus = accservice.checkIndAccessStatus(viewedkycid,ownKycId);
						
						if(!accessStatus.equals("No Data"))
						{
							if(accessStatus.equals("0"))
							{

								// Send Email to individual that utilizer accepted your access details
								 
								 int apply_code_count = accservice.orgApplyCodeCount(code);
								
								 ResourceBundle resource = ResourceBundle.getBundle("resources/notifyIndividualFromIndividual");
					     		    
							     String headingImage=resource.getString("headingImage");
						    	 String belowleftImage=resource.getString("belowleftImage");
						    	 String belowrightImage=resource.getString("belowrightImage");
						    	 String mailicon=resource.getString("mailicon");
						    	 String contacticon=resource.getString("contacticon");
						    	 
						    	 String emailHeading=resource.getString("emailHeading");
						    	 String contactNo=resource.getString("contactNo");
						    	 String contactEmail=resource.getString("contactEmail");
						    	 
						    	 String sharingsubject=resource.getString("sharingsubject");
						    	 String notifymessage=resource.getString("notifymessage");
								 String sharingmessage=resource.getString("sharingmessage");		
								 String aftermessageextraline = resource.getString("aftermessageextraline");
								 
						    	 String copyright=resource.getString("copyright");
						    	 String termscondition=resource.getString("termscondition");
						    	 String privacyPolicy=resource.getString("privacyPolicy");
						    	 
						    	 String facebookicon=resource.getString("facebookicon");
						    	 String twittericon=resource.getString("twittericon");
						    	 String googleicon=resource.getString("googleicon");
						    	 String linkedicon=resource.getString("linkedicon");
						    	 
						    	 String facebookLink=resource.getString("facebookLink");
						    	 String twitterLink=resource.getString("twitterLink");
						    	 String googlePlus=resource.getString("googlePlus");
						    	 String linkedIn=resource.getString("linkedIn");
						    	 
						    	 String clenseeLogo = resource.getString("clenseeLogo");
								 String sharingwelcome = resource.getString("sharingwelcome");
								 String clickhere = resource.getString("clickhere");
								 String actionUrl = resource.getString("actionUrl");
								 						    	 
												String reciverKycId = viewedkycid;
												int reciver_Ind_id = accservice.findid(reciverKycId);
												String reciverName = accservice.getReciverName(reciver_Ind_id);
												String reciverEmailId = accservice.getReciverEmailId(reciver_Ind_id);
												
										        StringBuilder text = new StringBuilder();
										        
									        
										        
											    /* Sending Emails to Register user */
											       
										        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
										        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
										       
										        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
										       
										        /* Header */
										        
										        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
										        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
										        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
										        text.append("</tr></table></td></tr></table>");
										       
										        /* End of Header */
										        
										        
										        /* Header-2 */
										        
										        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
										        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
										        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
										        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
										        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
										        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
										        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
										        text.append("</p></td></tr></table></td></tr></table>");
										        
										        /* End of Header-2 */
										        
										        
										        /* BODY PART */
										        
										        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
										        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
										        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
										        text.append("<h3 style='color: #004879;'>Dear "+reciverName+",</h3>");
										        text.append("<p style='text-align: justify'>"+sharingwelcome+"</p>");
										        text.append("<p style='text-align: justify'>"+otherIndName+" "+notifymessage+"</p>");
										        text.append("<p style='text-align: justify'>"+sharingmessage+"</p>");
										       
										        text.append("<br><p>"+aftermessageextraline+"<br><br>");					        	
										        text.append("<a href="+actionUrl+" target='_blank' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
										        text.append("</p>");
										        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
										        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
										        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
										        text.append("</tr></table></td></tr>");
										        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
										        text.append("</table> ");
										        
										        
										        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
										        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
										        
										        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
										        
										        text.append("<tr><td>");
										        
										        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
										        text.append("</td></tr></table>");
										        
										        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
										        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
										        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
										        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
										        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
										        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
										        					        
										        text.append("</tr>");
										        text.append("</table></td></tr></table></td></tr></table>");
										        text.append("</body>");
										        text.append("</html>");
										        
										        /* END OF BODY PART */
										        
										        
										        
										      /* End of Sending Emails to Register user */
										    
							       	         SendEmailUtil emailStatus = new SendEmailUtil();
							     	        
							       	        String subject = sharingsubject+" "+otherIndName;
							     			String resetStatus = "";
											try {
												resetStatus = emailStatus.getEmailSent(reciverEmailId,subject,text.toString());
											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
							     	     
							     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
							     	    	
							     	    	if(resetStatus.equals("success"))
							     	    	{	    		
							     	    		emailReport.setEmail_id(reciverEmailId);
							     	    		emailReport.setSubject(subject);
							     	    		emailReport.setMessage_body(text.toString());
							     	    		emailReport.setSent_date(new Date());
							     	    		emailReport.setSent_report("0");
							     	    	}
							     	    	else
							     	    	{
							     	    		emailReport.setEmail_id(reciverEmailId);
							     	    		emailReport.setSubject(subject);
							     	    		emailReport.setMessage_body(text.toString());
							     	    		emailReport.setSent_date(new Date());
							     	    		emailReport.setSent_report("1");	    			    		
							     	    	}
							     	    	
							     	    	profileinfoService.setEmailStatusReport(emailReport);
							     	
								 // End of sending mail to individual that utilizer accepted your access details
								
								
							}
						}
						
						
				
						
					     /* Decrypt sender Documents */						
						
						        
								String IndKycId = profileinfoService.getkycid(individualInd_id);							
								String OtherIndId = profileinfoService.getKycIdOfAccessGiven(IndKycId);          // Get All Other Ind Id For Decrypt the Variable.
								
								File ourPath = new File(kycMainDirectory);
								
								if(!OtherIndId.equals("No Data"))
								{							
									String otherRegYear = profileinfoService.getYearFormIndId(OtherIndId);
									
									String OtherIndIdArray[] = OtherIndId.split(",");
									String otherRegYearArray[] = otherRegYear.split(",");
									
									/*for(int id=0;id<OtherIndIdArray.length;id++)
									{
										File targetFile = new File((ourPath.getPath()+"/"+otherRegYearArray[id]+"/"+OtherIndIdArray[id]+"/Document"));						
														
										JIREncryptionUtils.encryptAllDocs(targetFile,"dec");
									}	*/															
								}				
						
						/* End of Decrypt sender Documents */	
								
				} 
				catch (IOException e)
				{					
					Date date = new Date();					
					e.printStackTrace();					
					log.error("-"+date+"Applyed Code info="+kycid+"Code Name="+request.getParameter("codename"));				    					
				}
			    				
				return new ModelAndView("showCode");					
		 }
	}
	
	
	@RequestMapping(value = "/downlaodCodes", method = RequestMethod.GET)
	public ModelAndView DownloadCode(HttpServletRequest request) {
		
		String docname = request.getParameter("name");
		String kycid = request.getParameter("kycid");
		
		int ind_id = accservice.findid(kycid);
		int year = accservice.findyear(docname, ind_id);
		String docType = accservice.findDocType(docname,ind_id); 
		
		if(docType.equals("Employment_DOC"))
		{
        	docType = "Employement_DOC";
		}		
		else if(docType.equals("ACADEMIC_DOC"))
		{
			docType = "ACADEMIC_DOC";
		}		
		else if(docType.equals("Financial_DOC"))
		{
			docType = "Financial_DOC";
		}		
		else
		{
			docType = "KYC_DOCUMENT";
		}
		
		request.setAttribute("docType", docType);
		request.setAttribute("docname", docname);
		request.setAttribute("user", ind_id);
		request.setAttribute("year", year);

		String individualId = ind_id+"";
		String registerYear = year+"";
		
		//FileEncryptionDecryptionUtil.getFileDecryption(Ind_id, year, request);
		
		FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,docname,request);
				
		// End Of Code File Decrypt
					
				
		return new ModelAndView("downloadApplyedCode");
	}
	
	
	@RequestMapping(value = "/downlaodalldocumentsinzip", method = RequestMethod.POST)
	public void  downlaodalldocumentsinzip(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException 
	{		
		String zipFileName = request.getParameter("codeName");
					
		// Required file Config for entire Controller 

		 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
		 String kycMainDirectory = fileResource.getString("fileDirectory");		
		 File filePath = new File(kycMainDirectory);
		
		// End of Required file Config for entire Controller 
					 
		response.setContentType("Content-type: text/zip");
		response.setHeader("Content-Disposition","attachment; filename="+zipFileName+".zip");
		List<File> files = new ArrayList<File>();
				
		String kycid = request.getParameter("kycId");		
		int ind_id = accservice.findid(kycid);
		int year = accservice.findyear(" ", ind_id);
		String docType = "";
		
		String documents = request.getParameter("docnames");
		documents = documents.substring(0, documents.length()-1);

		String documentNameArray[] = documents.split("&");
		
		String individualId = ind_id+"";
		String registerYear = year+"";
		
		for(int i=0;i<documentNameArray.length;i++)
		{
			docType = accservice.findDocType(documentNameArray[i],ind_id);
												
			if(docType.equals("KYC_DOCUMENT"))
			{
				docType = "KYC_DOCUMENT";
				
				FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,documentNameArray[i],request);
				
				files.add(new File(filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"KYC_DOCUMENT"+"/"+documentNameArray[i]));
			}
			if(docType.equals("ACADEMIC_DOC"))
			{
				docType = "ACADEMIC_DOC";
				
				FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,documentNameArray[i],request);
				
				files.add(new File(filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"ACADEMIC_DOC"+"/"+documentNameArray[i]));
			}
			if(docType.equals("Financial_DOC"))
			{
				docType = "Financial_DOC";
				
				FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,documentNameArray[i],request);
				
				files.add(new File(filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"Financial_DOC"+"/"+documentNameArray[i]));
			}
			if(docType.equals("Employment_DOC"))
			{
				docType = "Employement_DOC";
				
				FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,individualId,docType,documentNameArray[i],request);
				
				files.add(new File(filePath.getPath()+"/"+year+"/"+ind_id+"/Document/"+"Employement_DOC/"+documentNameArray[i]));
			}			
		}
		
									
		ServletOutputStream out = response.getOutputStream();
		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));

		for (File file : files) {

			zos.putNextEntry(new ZipEntry(file.getName()));

			// Get the file
			FileInputStream fis = null;
			try 
			{				
				fis = new FileInputStream(file);
			}
			catch (FileNotFoundException fnfe)
			{
				zos.write(("ERROR not find file " + file.getName()).getBytes());
				zos.closeEntry();
				System.out.println("Couldfind file "+ file.getAbsolutePath());
				continue;
			}

			BufferedInputStream fif = new BufferedInputStream(fis);
			int data = 0;
			while ((data = fif.read()) != -1)
			{
				zos.write(data);
			}
			
			fif.close();

			zos.closeEntry();
			
			System.out.println("Finishedng file " + file.getName());
		}

		zos.close();
			
	}	
	
	@RequestMapping(value = "/viewDocuments", method = RequestMethod.POST)
	public String viewDcuments(HttpServletRequest request, HttpSession session) 
	{
				
					String docname = request.getParameter("docName");
					String kycid = request.getParameter("kycid");
					
					int ind_id = accservice.findInd_Id(kycid);		
					int cr_year = accservice.findyear(docname,ind_id);			        
			        String docType = accservice.findDocType(docname,ind_id);       
			        
			        if(docType.equals("Employment_DOC"))
					{
			        	docType = "Employement_DOC";
					}		
					else if(docType.equals("ACADEMIC_DOC"))
					{
						docType = "ACADEMIC_DOC";
					}		
					else if(docType.equals("Financial_DOC"))
					{
						docType = "Financial_DOC";
					}		
					else
					{
						docType = "KYC_DOCUMENT";
					}
					
			        
			        String viewData =  ind_id+"/"+cr_year+"/"+docname+"/"+docType;
					
			        request.setAttribute("viewData",viewData);
			        
			        String indvidualId = ind_id+"";
					String registerYear = cr_year+"";
					
					//FileEncryptionDecryptionUtil.getFileDecryption(Ind_id, year, request);
					
					FileEncryptionDecryptionUtil.getDecryptSpecificFileOFOwn(registerYear,indvidualId,docType,docname,request);
							
					// End Of Code File Decrypt
			      
			 return "view_applyCode_doc";			
	}
	
		
	@RequestMapping(value = "/allowAccess", method = RequestMethod.POST)
	public ModelAndView getAllowAccess(HttpServletRequest request) 
	{		
		String domain_Type = accservice.getDomain();
		request.setAttribute("domainType", domain_Type);
		
		return new ModelAndView("generate_AllowAccess");
	}
		
	@RequestMapping(value = "/sharingtoUti", method = RequestMethod.GET)
	public ModelAndView sharingtoUti(HttpServletRequest request,HttpSession session) 
	{			
		session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null)
		{			
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			 
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return new ModelAndView("individualPage");	
		}
		else if(((String)session.getAttribute("Ind_id")).equals(null))
		{
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return  new ModelAndView("individualPage");	
		}
		else
		{			
				int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));				
				request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
								
				String domain_Type = accservice.getDomain();
				request.setAttribute("domainType", domain_Type);
				
				return new ModelAndView("individualUtilizerSharing");
		}
		
	}
	
	
	@RequestMapping(value="/findUtiliser",method = RequestMethod.POST)
	public @ResponseBody String getfindUtiliser(HttpServletRequest request)
	{		
	    String ptname=request.getParameter("domainPattern");	    
	    String utiliserDetails = accservice.findUtiKycId(ptname);
	      		    	
	    return utiliserDetails;	   
	}
	
	@RequestMapping(value="/searchUtilizerFromInd",method = RequestMethod.POST)
	public String searchUtilizerFromInd(HttpServletRequest request)
	{		
	    String ptname=request.getParameter("domainPattern");	    
	    String utiliserDetails = accservice.findUtiKycId(ptname);
	     
	    request.setAttribute("domainPattern", ptname);
	    request.setAttribute("utiliserDetails", utiliserDetails);
	    
	    return "searchUtilizerfromIndividual";	   
	}
		
	
	@RequestMapping(value="/getPatternfromId",method = RequestMethod.POST)
	public ModelAndView getPatternfromId(HttpServletRequest request,HttpSession session)
	{	
		int patternId = Integer.parseInt(request.getParameter("patternId"));
		String searchResult = accservice.getPatternfromId(patternId);
		
		String utiAddress = request.getParameter("utiAddress");
		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
				
		String getDetailsofPatterns = accservice.patternDetailsAsPerRecord(searchResult,ind_id);	
		
		request.setAttribute("utiName",request.getParameter("name"));
		request.setAttribute("utiAddress", utiAddress);
		request.setAttribute("domainPattern", request.getParameter("domainPattern"));
		request.setAttribute("patternDetails", searchResult);
		request.setAttribute("profileDetails", getDetailsofPatterns);
		request.setAttribute("utiliserId", request.getParameter("utiliserId"));	
				
		String documentAvailablityViaThePattern = accservice.getDocumentAvailablityViaThePattern(searchResult,ind_id);
		request.setAttribute("availabilityStatus", documentAvailablityViaThePattern);	
		
		return new ModelAndView("allowAccessForUtilizer");
	}
	
	@RequestMapping(value="/allowAccessPopUp",method = RequestMethod.GET)
	public  String allowAccessPopUp(HttpServletRequest request,HttpSession session)
	{		
		return "allowAccessForUtilizer";
	}
	
	@SuppressWarnings("static-access")
	@RequestMapping(value="/shareToUtilizer",method = RequestMethod.POST)
	public @ResponseBody
	String getAccessToUti(HttpServletRequest request,HttpSession session) throws IOException
	{
		String pattern_logic = request.getParameter("pattern");
		int utiliserId = Integer.parseInt(request.getParameter("utilisrId"));
		String allowComment = request.getParameter("allowComment");
		String attachedDocuments = request.getParameter("newPattern");
		
		String utiId =  accservice.getUtiliserAccessId(utiliserId); 
		
		int acc_giver = Integer.parseInt((String)session.getAttribute("Ind_id"));
		String acc_sender = accservice.findIndKycID(acc_giver);
						
		String name = accservice.findName(acc_sender);
		
		AccessDetailsBean accbean = new AccessDetailsBean();
				
		accbean.setAccess_giver(acc_sender);
		accbean.setGiver_name(name);
		accbean.setAccess_taker(utiId);		
		if(allowComment.equals(""))
		{
			accbean.setAccess_description("Sharing reason not mentioned.");
		}
		else
		{
			accbean.setAccess_description(allowComment);
		}
		
		
		if(!attachedDocuments.equals(""))
		{
			String codeArray[] = attachedDocuments.split("/");
			String generateFormatedCode = "";
			
			String kycCode = "KYC:";
			String academicCode = "ACADEMIC:";
			String financialCode = "Financial:";
			String empCode = "Employment:";
			
			for(int c=0;c<codeArray.length;c++)
			{
			     String codeArr1[] = codeArray[c].split("--");
			     if(codeArr1[0].equals("KYC"))
			     {
			    	 kycCode += codeArr1[1] + ",";
			     }
			     if(codeArr1[0].equals("ACADEMIC"))
			     {
			    	 academicCode += codeArr1[1] + ",";
			     }
			     if(codeArr1[0].equals("Financial"))
			     {
			    	 financialCode += codeArr1[1] + ",";
			     }
			     if(codeArr1[0].equals("Employment"))
			     {
			    	 empCode += codeArr1[1] + ",";
			     }		     
			}
			
			kycCode = kycCode.substring(0, kycCode.length()-1);
			academicCode = academicCode.substring(0, academicCode.length()-1);
			financialCode = financialCode.substring(0, financialCode.length()-1);		
			empCode = empCode.substring(0, empCode.length()-1);
			
			generateFormatedCode = kycCode + "/" + academicCode + "/" + financialCode + "/" + empCode;
			
			accbean.setAccess_pattern(pattern_logic+"#"+generateFormatedCode);
		}
		else
		{
			accbean.setAccess_pattern(pattern_logic);
		}
		
		
		AccessDetailsModel accmodel = prepareAccessModel(accbean);
		
		String checkLastAccess = accservice.deactiveLastAccess(accmodel);
		
		String strCheck = accservice.saveAccessDetails(accmodel);
		
        if ("ResultSuccess".equals(strCheck)) 
        {       
       
        	List<UtiliserProfileModel> utilizerData = accservice.getUtilizerDetails(utiId);
			
        // Tracking Activity Details
	        
	        String Kyc_id= (String)session.getAttribute("Kyc_id");
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("You had successfully shared documents to an Utilizer ("+utilizerData.get(0).getUti_first_name()+" "+utilizerData.get(0).getUti_middle_name()+" "+utilizerData.get(0).getUti_last_name()+")");
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("Access manager Updated");
			Profileupdatemodel.setMore_about_it("Event done in allow access of Access Manager");
		    Profileupdatemodel.setKyc_id(Kyc_id);
			profileinfoService.profileupdate(Profileupdatemodel);

         // End Of Tracking Activity Details
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/SharingDocToUtilizer");
			
		
			 String headingImage=resource.getString("headingImage");
	    	 String belowleftImage=resource.getString("belowleftImage");
	    	 String belowrightImage=resource.getString("belowrightImage");
	    	 String mailicon=resource.getString("mailicon");
	    	 String contacticon=resource.getString("contacticon");
	    	 
	    	 String emailHeading=resource.getString("emailHeading");
	    	 String contactNo=resource.getString("contactNo");
	    	 String contactEmail=resource.getString("contactEmail");
	    	 
	    	 String mailSubject = resource.getString("utilizersignupsubject");
			 String mailmessage = resource.getString("utilizersignupmessage");
			 String mailmessage2 = resource.getString("utilizeraftermessageextraline1");
			 String mailmessage3 = resource.getString("utilizeraftermessageextraline2");
			 	
	    	 String copyright=resource.getString("copyright");
	    	 String termscondition=resource.getString("termscondition");
	    	 String privacyPolicy=resource.getString("privacyPolicy");
	    	 
	    	 String facebookicon=resource.getString("facebookicon");
	    	 String twittericon=resource.getString("twittericon");
	    	 String googleicon=resource.getString("googleicon");
	    	 String linkedicon=resource.getString("linkedicon");
	    	 
	    	 String facebookLink=resource.getString("facebookLink");
	    	 String twitterLink=resource.getString("twitterLink");
	    	 String googlePlus=resource.getString("googlePlus");
	    	 String linkedIn=resource.getString("linkedIn");
	    	 
	    	 String clenseeLogo = resource.getString("clenseeLogo");			
			 String clickhere = resource.getString("clickhere");
			 String actionUrl = resource.getString("actionUrl");
			 	    	 
			 String smsMessage = resource.getString("smsMessage");
					
			// Allow access Email and SMs Operation 
			
			String url = "https://www.clensee.com/individual.html";
		        
		    StringBuilder text = new StringBuilder();
		    		    
		    /* Sending Emails to Register user */
		       
	        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
	        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
	       
	        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
	       
	        /* Header */
	        
	        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
	        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
	        text.append("</tr></table></td></tr></table>");
	       
	        /* End of Header */
	        
	        /* Header-2 */
	        
	        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
	        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
	        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
	        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
	        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
	        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
	        text.append("</p></td></tr></table></td></tr></table>");
	        
	        /* End of Header-2 */
	        
	        
	        /* BODY PART */
	        
	        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
	        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
	        text.append("<h3 style='color: #004879;'>Dear "+utilizerData.get(0).getUti_first_name()+",</h3>");
	        text.append("<p style='text-align: justify'>"+mailmessage+"</p>");
	        text.append("<p>"+mailmessage2+"<br>");	        
 	       // text.append("<p>"+allowComment+"<br>");
 	        
	        /*if(!allowComment.isEmpty())
	        {
	        	text.append("<br><p style='text-align: justify'>"+allowComment+"</p>");
	        }*/
	        	        
	        text.append("<br><p>"+mailmessage3+"<br><br>");					        	
	        text.append("<a href="+actionUrl+" target='_blank' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
	        text.append("</p>");
	        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
	        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
	        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
	        text.append("</tr></table></td></tr>");
	        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
	        text.append("</table> ");
	        
	        
	        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
	        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
	        
	        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
	        
	        text.append("<tr><td>");
	        
	        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
	        text.append("</td></tr></table>");
	       
	        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
	        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
	        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
	        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
	        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
	        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
	        
	        
	        text.append("</tr>");
	        text.append("</table></td></tr></table></td></tr></table>");
	        text.append("</body>");
	        text.append("</html>");
	       
	        /* END OF BODY PART */
	        
	        
	      /* End of Sending Emails to Register user */
	    
		 
	        SendEmailUtil emailStatus = new SendEmailUtil();
	        
	        String subject = mailSubject+" "+name;
			String resetStatus = emailStatus.getEmailSent(utilizerData.get(0).getUti_email_id(),subject,text.toString());
			request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
	     
	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
	    	
	    	if(resetStatus.equals("success"))
	    	{	    		
	    		emailReport.setEmail_id(utilizerData.get(0).getUti_email_id());
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("0");
	    	}
	    	else
	    	{
	    		emailReport.setEmail_id(utilizerData.get(0).getUti_email_id());
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("1");	    			    		
	    	}
	    	
	    	profileinfoService.setEmailStatusReport(emailReport);
	    				
		    if(!utilizerData.get(0).getUti_office_first_no().equals(""))
		        {
		    	   String smsCriteria = checkSMSStatusFromUtilizer(utiliserId);
		    	  
		    	   if(smsCriteria.equals("Allow"))
		    	   {		    	 
			        	String mobileNo = ""+utilizerData.get(0).getUti_office_first_no();
				        String asPerclientMessag = "Dear "+utilizerData.get(0).getUti_first_name()+", "+name+" "+smsMessage;
				        
				        String smsStatus = obj.SMSSender(userName,password,asPerclientMessag,senderId,mobileNo,"0");   
				        
                        SmsSentStatusModel smsModel = new SmsSentStatusModel();
				        
				        if(smsStatus.equals("success"))
				        {
				        	smsModel.setUnique_id(utiliserId);
				        	smsModel.setSms_belongs_to("Utilizer");
				        	smsModel.setMobile_no(utilizerData.get(0).getUti_office_first_no());
				        	smsModel.setMessage_body(asPerclientMessag);
				        	smsModel.setSent_date(new Date());
				        	smsModel.setSent_report("0");
				        }
				        else
				        {
				        	smsModel.setUnique_id(utiliserId);
				        	smsModel.setSms_belongs_to("Utilizer");
				        	smsModel.setMobile_no(utilizerData.get(0).getUti_office_first_no());
				        	smsModel.setMessage_body(asPerclientMessag);
				        	smsModel.setSent_date(new Date());
				        	smsModel.setSent_report("1");
				        }
				        
				        profileinfoService.setSmsStatusReport(smsModel);			        
		    	   }
		        }
			       
			
			//  End of Allow access Email and SMs Operation 
		
			return "You have successfully granted access to Utilizer on your selected profile and documents.";
		}
        else
        {       	
			return "Some problem arise in provide accessing need to try Again,Click left side Allow Access option";
		}
		
	}
	
	private AccessDetailsModel prepareAccessModel(AccessDetailsBean accbean)
	{
		
		AccessDetailsModel accmodel = new AccessDetailsModel();
		
		accmodel.setAccess_pattern(accbean.getAccess_pattern());
		accmodel.setAccess_giver(accbean.getAccess_giver());
		accmodel.setGiver_name(accbean.getGiver_name());
		accmodel.setAccess_taker(accbean.getAccess_taker());
		accmodel.setAccess_given_date(new Date());
		accmodel.setAccess_taken_date(new Date());
		accmodel.setAccess_description(accbean.getAccess_description());
		accmodel.setStatus("1");
		accmodel.setUti_status("0");
		accmodel.setRepeate_access_status("No");
		
		return accmodel;
		
	}
	
	
	@RequestMapping(value="/revokeAccess",method = RequestMethod.POST)
	public ModelAndView getRevokeAccess(HttpServletRequest request,HttpSession session)
	{		       		
		return new ModelAndView("generateRevokeAccess");
	}
	
	
	@RequestMapping(value="/revokeAccessFromUtiliser",method = RequestMethod.POST)
	public @ResponseBody String revokeAccessFromUtiliser(HttpServletRequest request,HttpSession session)
	{
		int Ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		String ownId = accservice.findIndKycID(Ind_id);
		
		String utiAccessDetails = accservice.getUtiAccessDetails(ownId);
				
		return utiAccessDetails;
	}
		
	@RequestMapping(value="/changeutiliseraccess",method = RequestMethod.POST)
	public @ResponseBody String changeUtiliserAccess(HttpServletRequest request,HttpSession session) throws ParseException
	{
		String statusResult = "";
		
		String uti_unique_id = request.getParameter("kycid");
		String access_status= request.getParameter("status");
		Date date;
		SimpleDateFormat dateformat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = (Date)dateformat1.parse(request.getParameter("date"));
		
		statusResult = accservice.changeStatusOfUtiliser(uti_unique_id,date,access_status);
		
		int individualInd_id = Integer.parseInt((String)session.getAttribute("Ind_id"));		
		String otherIndName = accservice.getReciverName(individualInd_id);
		
		if(statusResult.equals("Revoked Your Access"))
		{
			 
			 // Send Email to individual that utilizer accepted your access details
			 
			 ResourceBundle resource = ResourceBundle.getBundle("resources/revokeNotifyUtilizerFromIndividual");
   		     
		     String headingImage=resource.getString("headingImage");
	    	 String belowleftImage=resource.getString("belowleftImage");
	    	 String belowrightImage=resource.getString("belowrightImage");
	    	 String mailicon=resource.getString("mailicon");
	    	 String contacticon=resource.getString("contacticon");
	    	 
	    	 String emailHeading=resource.getString("emailHeading");
	    	 String contactNo=resource.getString("contactNo");
	    	 String contactEmail=resource.getString("contactEmail");
	    	 
	    	 String sharingsubject=resource.getString("sharingsubject");
	    	 String notifymessage=resource.getString("notifymessage");
			 String sharingmessage=resource.getString("sharingmessage");		
			 String aftermessageextraline = resource.getString("aftermessageextraline");
			 
	    	 String copyright=resource.getString("copyright");
	    	 String termscondition=resource.getString("termscondition");
	    	 String privacyPolicy=resource.getString("privacyPolicy");
	    	 
	    	 String facebookicon=resource.getString("facebookicon");
	    	 String twittericon=resource.getString("twittericon");
	    	 String googleicon=resource.getString("googleicon");
	    	 String linkedicon=resource.getString("linkedicon");
	    	 
	    	 String facebookLink=resource.getString("facebookLink");
	    	 String twitterLink=resource.getString("twitterLink");
	    	 String googlePlus=resource.getString("googlePlus");
	    	 String linkedIn=resource.getString("linkedIn");
	    	 
	    	 String clenseeLogo = resource.getString("clenseeLogo");
			 String sharingwelcome = resource.getString("sharingwelcome");
			 String clickhere = resource.getString("clickhere");
			 String actionUrl = resource.getString("actionUrl");
			 						    	 
			 
							List<UtiliserProfileModel> utilizerdetails = accservice.getUtilizerDetails(uti_unique_id);
							
							String reciverName = utilizerdetails.get(0).getUti_first_name()+" "+utilizerdetails.get(0).getUti_middle_name()+" "+utilizerdetails.get(0).getUti_last_name();
							String reciverEmailId = utilizerdetails.get(0).getUti_email_id();
							
					        StringBuilder text = new StringBuilder();
				        
					        
						    /* Sending Emails to Register user */
						       
					        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
					        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");					       
					        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
					       
					        /* Header */
					        
					        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
					        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
					        text.append("</tr></table></td></tr></table>");
					       
					        /* End of Header */
					        
					        
					        /* Header-2 */
					        
					        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
					        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
					        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
					        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
					        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
					        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
					        text.append("</p></td></tr></table></td></tr></table>");
					        
					        /* End of Header-2 */
					       
					        
					        /* BODY PART */
					        
					        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
					        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
					        text.append("<h3 style='color: #004879;'>Dear "+reciverName+",</h3>");
					        text.append("<p style='text-align: justify'>"+sharingwelcome+"</p>");
					        text.append("<p style='text-align: justify'>"+otherIndName+" "+notifymessage+"</p>");
					        text.append("<p style='text-align: justify'>"+sharingmessage+"</p>");
					        			        					        
					        text.append("<br><p>"+aftermessageextraline+"<br><br>");					        	
					        text.append("<a href="+actionUrl+" target='_blank' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
					        text.append("</p>");
					        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
					        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
					        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
					        text.append("</tr></table></td></tr>");
					        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
					        text.append("</table> ");
					        
					        
					        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
					        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
					        
					        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
					        
					        text.append("<tr><td>");
					        
					        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
					        text.append("</td></tr></table>");
					        
					        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
					        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
					        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
					        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
					        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
					        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
					        					        
					        text.append("</tr>");
					        text.append("</table></td></tr></table></td></tr></table>");
					        text.append("</body>");
					        text.append("</html>");
					        
					        /* END OF BODY PART */
					        
					        					        
					    /* End of Sending Emails to Register user */
					    
		       	        SendEmailUtil emailStatus = new SendEmailUtil();		     	        
		       	        String subject = sharingsubject+" "+otherIndName;
		       	        
		     			String resetStatus = "";
						try 
						{
							resetStatus = emailStatus.getEmailSent(reciverEmailId,subject,text.toString());
						} 
						catch (IOException e) 
						{							
							e.printStackTrace();
						}
						
						request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
		     	     
		     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
		     	    	
		     	    	if(resetStatus.equals("success"))
		     	    	{	    		
		     	    		emailReport.setEmail_id(reciverEmailId);
		     	    		emailReport.setSubject(subject);
		     	    		emailReport.setMessage_body(text.toString());
		     	    		emailReport.setSent_date(new Date());
		     	    		emailReport.setSent_report("0");
		     	    	}
		     	    	else
		     	    	{
		     	    		emailReport.setEmail_id(reciverEmailId);
		     	    		emailReport.setSubject(subject);
		     	    		emailReport.setMessage_body(text.toString());
		     	    		emailReport.setSent_date(new Date());
		     	    		emailReport.setSent_report("1");	    			    		
		     	    	}
		     	    	
		     	    	profileinfoService.setEmailStatusReport(emailReport);
		     	
		     	    	
			// End of sending mail to individual that utilizer accepted your access details
			
		     	    	
            // Tracking Activity Details
	        
	        String Kyc_id= (String)uti_unique_id;
	        String utilizerFullName = accservice.getUtilizerFullName(Kyc_id);
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("You had revoked utilizer access to "+utilizerFullName);
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("Revoke Access From Utilizer");
			Profileupdatemodel.setMore_about_it("Event done in Revoke Access-"+Kyc_id);
		    Profileupdatemodel.setKyc_id((String)session.getAttribute("Kyc_id"));
			profileinfoService.profileupdate(Profileupdatemodel);

	        // End Of Tracking Activity Details
		}
		
		
		return statusResult;
	}
	
	@RequestMapping(value="/revokeAccessOfIndividual",method = RequestMethod.POST)
	public @ResponseBody 
	String getUtiAccessDetails(HttpServletRequest request,HttpSession session)
	{
		int Ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		
		String ownId = accservice.findIndKycID(Ind_id);  //getting the kycid because in the code access table we are mention giver and sender as per the kycid
		
		List<CodesDetailsModel> lsdata  = accservice.getAccessDetails(ownId);
		
		List<String> indCodesDetails = new ArrayList<String>();
		
		for(int i=0;i<lsdata.size();i++)
		{			
			
			indCodesDetails.add(lsdata.get(i).getCode_receiver());		
			
			SimpleDateFormat dateformatJava = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String cr_date = dateformatJava.format(lsdata.get(i).getCr_date());
			indCodesDetails.add(cr_date);
			indCodesDetails.add(lsdata.get(i).getCode_description());
			indCodesDetails.add(lsdata.get(i).getCode_status());
			
			indCodesDetails.add(accservice.findName(lsdata.get(i).getCode_receiver()));
										
		}
		
		return indCodesDetails.toString().substring(1, indCodesDetails.toString().length()-1);
	}
	
	
	@RequestMapping(value="/InactiveAccess",method = RequestMethod.POST)
	public @ResponseBody String revokeUtiliser(HttpServletRequest request,HttpSession session) throws ParseException
	{	
		
		String kycid = request.getParameter("kycid");
		String access_status= request.getParameter("status");
		Date date;
		SimpleDateFormat dateformat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = (Date)dateformat1.parse(request.getParameter("date"));
				
		String status = accservice.revokeUtiliser(kycid,date,access_status);
		
		int individualInd_id = Integer.parseInt((String)session.getAttribute("Ind_id"));		
		String otherIndName = accservice.getReciverName(individualInd_id);

		
		if(status.equals("Successfully revoked access"))
		{
			// Send Email to individual that utilizer accepted your access details
			 			 
			 ResourceBundle resource = ResourceBundle.getBundle("resources/revokeNotifyIndividualFromIndividual");
    		    
		     String headingImage=resource.getString("headingImage");
	    	 String belowleftImage=resource.getString("belowleftImage");
	    	 String belowrightImage=resource.getString("belowrightImage");
	    	 String mailicon=resource.getString("mailicon");
	    	 String contacticon=resource.getString("contacticon");
	    	 
	    	 String emailHeading=resource.getString("emailHeading");
	    	 String contactNo=resource.getString("contactNo");
	    	 String contactEmail=resource.getString("contactEmail");
	    	 
	    	 String sharingsubject=resource.getString("sharingsubject");
	    	 String notifymessage=resource.getString("notifymessage");
			 String sharingmessage=resource.getString("sharingmessage");		
			 String aftermessageextraline = resource.getString("aftermessageextraline");
			 
	    	 String copyright=resource.getString("copyright");
	    	 String termscondition=resource.getString("termscondition");
	    	 String privacyPolicy=resource.getString("privacyPolicy");
	    	 
	    	 String facebookicon=resource.getString("facebookicon");
	    	 String twittericon=resource.getString("twittericon");
	    	 String googleicon=resource.getString("googleicon");
	    	 String linkedicon=resource.getString("linkedicon");
	    	 
	    	 String facebookLink=resource.getString("facebookLink");
	    	 String twitterLink=resource.getString("twitterLink");
	    	 String googlePlus=resource.getString("googlePlus");
	    	 String linkedIn=resource.getString("linkedIn");
	    	 
	    	 String clenseeLogo = resource.getString("clenseeLogo");
			 String sharingwelcome = resource.getString("sharingwelcome");
			 String clickhere = resource.getString("clickhere");
			 String actionUrl = resource.getString("actionUrl");
			 						    	 
							String reciverKycId = kycid;
							
							int reciver_Ind_id = accservice.findid(reciverKycId);
							String reciverName = accservice.getReciverName(reciver_Ind_id);
							String reciverEmailId = accservice.getReciverEmailId(reciver_Ind_id);
							
					        StringBuilder text = new StringBuilder();
					        
				        
					        
						    /* Sending Emails to Register user */
						       
					        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
					        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
					       
					        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
					       
					        /* Header */
					        
					        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
					        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
					        text.append("</tr></table></td></tr></table>");
					       
					        /* End of Header */
					        
					        
					        /* Header-2 */
					        
					        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
					        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
					        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
					        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
					        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
					        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
					        text.append("</p></td></tr></table></td></tr></table>");
					        
					        /* End of Header-2 */
					        
					        
					        /* BODY PART */
					        
					        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
					        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
					        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
					        text.append("<h3 style='color: #004879;'>Dear "+reciverName+",</h3>");
					        text.append("<p style='text-align: justify'>"+sharingwelcome+"</p>");
					        text.append("<p style='text-align: justify'>"+otherIndName+" "+notifymessage+"</p>");
					        text.append("<p style='text-align: justify'>"+sharingmessage+"</p>");
					       
					        text.append("<br><p>"+aftermessageextraline+"<br><br>");					        	
					        text.append("<a href="+actionUrl+" target='_blank' style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
					        text.append("</p>");
					        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
					        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
					        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
					        text.append("</tr></table></td></tr>");
					        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
					        text.append("</table> ");
					        
					        
					        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
					        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
					        
					        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
					        
					        text.append("<tr><td>");
					        
					        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
					        text.append("</td></tr></table>");
					        
					        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
					        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
					        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
					        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
					        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
					        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
					        					        
					        text.append("</tr>");
					        text.append("</table></td></tr></table></td></tr></table>");
					        text.append("</body>");
					        text.append("</html>");
					        
					        /* END OF BODY PART */
					        
					        
					        
					      /* End of Sending Emails to Register user */
					    
		       	         SendEmailUtil emailStatus = new SendEmailUtil();
		     	        
		       	        String subject = sharingsubject+" "+otherIndName;
		     			String resetStatus = "";
						try {
							resetStatus = emailStatus.getEmailSent(reciverEmailId,subject,text.toString());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
		     	     
		     	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
		     	    	
		     	    	if(resetStatus.equals("success"))
		     	    	{	    		
		     	    		emailReport.setEmail_id(reciverEmailId);
		     	    		emailReport.setSubject(subject);
		     	    		emailReport.setMessage_body(text.toString());
		     	    		emailReport.setSent_date(new Date());
		     	    		emailReport.setSent_report("0");
		     	    	}
		     	    	else
		     	    	{
		     	    		emailReport.setEmail_id(reciverEmailId);
		     	    		emailReport.setSubject(subject);
		     	    		emailReport.setMessage_body(text.toString());
		     	    		emailReport.setSent_date(new Date());
		     	    		emailReport.setSent_report("1");	    			    		
		     	    	}
		     	    	
		     	    	profileinfoService.setEmailStatusReport(emailReport);
		     	
			 // End of sending mail to individual that utilizer accepted your access details
			
			
		}
		
        String Kyc_id= (String)kycid;	        
        String indFullName = accservice.getIndNameFromKycId(Kyc_id);
		Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
		Profileupdatemodel.setProfileupdatedetail("You had revoked individual access to "+indFullName);
		Profileupdatemodel.setProfileupdatetime(new Date());
		Profileupdatemodel.setType_of_alert("Revoke Access From Individual ");
		Profileupdatemodel.setMore_about_it("Event done in Revoke Access-"+Kyc_id);
	    Profileupdatemodel.setKyc_id((String)session.getAttribute("Kyc_id"));
		profileinfoService.profileupdate(Profileupdatemodel);

	     
		return status;
	}
	
	@RequestMapping(value="/Active_Access",method = RequestMethod.POST)
	public @ResponseBody String activeUtiliser(HttpServletRequest request) throws ParseException
	{
				
		String kycid = request.getParameter("kycid");
		String access_status= request.getParameter("status");
		Date date;
		SimpleDateFormat dateformat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = (Date)dateformat1.parse(request.getParameter("date"));
				
		String status = accservice.revokeUtiliser(kycid,date,access_status);
				
		return status;
	}
	
	@RequestMapping(value="/accessHistory",method = RequestMethod.POST)
	public ModelAndView getAccessHistory(HttpServletRequest request,HttpSession session)
	{			
		return new ModelAndView("generateAccessHistory");
	}
	
	
	@RequestMapping(value="/accIndHistory",method = RequestMethod.POST)
	public @ResponseBody String getIndHistory(HttpSession session)
	{
		int ind_Id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		String kycid = accservice.findIndKycID(ind_Id);
		
		List<CodesDetailsModel> lsdata = accservice.findHisInd(kycid);
		List<String> indHis = new ArrayList<String>();
		
		for(int i=0;i<lsdata.size();i++)
		{			
			int c_id = lsdata.get(i).getCode_id();
			String code_id = Integer.toString(c_id); 
			indHis.add(code_id);
			indHis.add(accservice.findName(lsdata.get(i).getCode_receiver()));
			indHis.add(lsdata.get(i).getCode_description());
			SimpleDateFormat dateformatJava = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String cr_date = dateformatJava.format(lsdata.get(i).getCr_date());
            indHis.add(cr_date);
								
		}
		
		List<NonKycUserCodeDetailsModel> codeOfNonKycUser = accservice.getCodeDetailsforNonKycUser(ind_Id);
		
		for(int j=0;j<codeOfNonKycUser.size();j++)
		{
			indHis.add(codeOfNonKycUser.get(j).getNon_kyc_user_code_id()+"NonKyc");
			indHis.add(codeOfNonKycUser.get(j).getReciver_name());
			indHis.add(codeOfNonKycUser.get(j).getDescription());
			SimpleDateFormat dateformatJava = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String cr_date = dateformatJava.format(codeOfNonKycUser.get(j).getSend_date());
            indHis.add(cr_date);
			
		}
		
		
		return indHis.toString().substring(1,indHis.toString().length()-1);
	}
	
	
	@RequestMapping(value="/accUtiHistory",method = RequestMethod.POST)
	public @ResponseBody String getUtiHistory(HttpSession session)
	{
		int ind_Id = Integer.parseInt((String)session.getAttribute("Ind_id"));
		String kycid = accservice.findIndKycID(ind_Id);
		
		List<AccessDetailsModel> lsdata = accservice.findHisUti(kycid);
		List<String> utiHis = new ArrayList<String>();
		
		for(int i=0;i<lsdata.size();i++)
		{
			utiHis.add(lsdata.get(i).getAccess_taker());
			utiHis.add(lsdata.get(i).getAccess_description());
			SimpleDateFormat dateformatJava = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String cr_date = dateformatJava.format(lsdata.get(i).getAccess_given_date());
			utiHis.add(cr_date);
		}
		return utiHis.toString().substring(1,utiHis.toString().length()-1);
	}
	
	
	String pass_kycid ="";
	String pass_date = "";
	
	@RequestMapping(value="/viewIndDocList" , method = RequestMethod.POST)
	public ModelAndView getIndDocListDetails(HttpServletRequest request) throws ParseException
	{
		String kycid = request.getParameter("kycid");
		String dat1 = request.getParameter("date");
		
		pass_kycid = kycid;
		pass_date =  dat1;
		
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = dateformat.parse(dat1);
		dateformat.applyPattern("yyyy-MM-dd HH:mm:ss");
		String newDateString = dateformat.format(date);
		
		return new ModelAndView("ind_History_view_DocList");
		
	}
	
	@RequestMapping(value="/viewUtiDocList" , method = RequestMethod.POST)
	public ModelAndView getUtiDocListDetails(HttpServletRequest request,HttpSession session) throws ParseException
	{
		
		int ind_Id = Integer.parseInt((String)session.getAttribute("Ind_id")); 
		
		String kycid = request.getParameter("kycid");
		String dat1 = request.getParameter("date");
		
		pass_kycid = kycid;
		pass_date =  dat1;
		
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = dateformat.parse(dat1);
		dateformat.applyPattern("yyyy-MM-dd HH:mm:ss");
		String newDateString = dateformat.format(date);
						
		String doc_pattern = accservice.getUti_HisDoclist(kycid.replaceAll(" ", ""),newDateString,ind_Id);
		
		
		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id")); 
		String sender_kycid = accservice.findIndKycID(ind_id);
		
		
		request.setAttribute("Ind_id",sender_kycid);
		
		request.setAttribute("accessPattern", doc_pattern);
	    request.setAttribute("kycid", kycid);
						
		return new ModelAndView("uti_History_view_DocList");		
	}
		
	@RequestMapping(value = "/viewIndividualHistoryDocumentlist", method = RequestMethod.POST)
	public ModelAndView getIndDocList(HttpServletRequest request,HttpSession session) {
		
		String generateCode = request.getParameter("id").replaceAll(" ","");
		int code = 0;
		String documents = "";
		
		if(generateCode.contains("NonKyc"))
		{
			generateCode = generateCode.replaceAll("NonKyc", "");
			int non_kyc_code = Integer.parseInt(generateCode);
			documents = accservice.servicePatternforNonKycUsr(non_kyc_code);
		}
		else
		{
			code = Integer.parseInt(request.getParameter("id").replaceAll(" ",""));	
			documents = accservice.servicePattern(code);
		}
	
		String kycid = request.getParameter("kycid");
		kycid = kycid.replaceAll(" ","");
		
	   
        		
		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id")); 
		String sender_kycid = accservice.findIndKycID(ind_id);
				
		request.setAttribute("Ind_id",sender_kycid);
		request.setAttribute("documents", documents);
		request.setAttribute("foldrid", kycid);
				
		return new ModelAndView("ind_History_view_DocList");
		
	}
		
	@RequestMapping(value = "/view/own/Documents", method = RequestMethod.POST)
	public String viewOwnDocuments(HttpServletRequest request, HttpSession session) {
		
		String docname = request.getParameter("docName");
		String kycid = request.getParameter("kycid");
		
		int ind_id = accservice.findInd_Id(kycid);
		
		int cr_year = accservice.findyear(docname,ind_id);
		
        String docinfo = accservice.findDocView(docname);
        
        String docType = accservice.findDocType(docname,ind_id); 
        
        if(docType.equals("Employment_DOC"))
		{
        	docType = "Employement_DOC";
		}		
		else if(docType.equals("ACADEMIC_DOC"))
		{
			docType = "ACADEMIC_DOC";
		}		
		else if(docType.equals("Financial_DOC"))
		{
			docType = "Financial_DOC";
		}		
		else
		{
			docType = "KYC_DOCUMENT";
		}
        
        String str[] = docinfo.split(",");
        String viewDocName = "";
        
        for(int i=0;i<str.length;i++)
        {
        	if(docname.equals(str[i]))
        	{
        		viewDocName = viewDocName+str[i];
        	}
        }
       
        String viewData =  ind_id+"/"+cr_year+"/"+viewDocName+"/"+docType;
		
        request.setAttribute("viewData",viewData);
        
        return "view_applyCode_doc";
	}
		
	public String checkSMSStatus(HttpServletRequest request,HttpSession session)
	{
		int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id")); 
		String currentPlan = PlanService.getCurrentPlanName(ind_id);
		String  restrictStatus = "";
		String featuresIdList =  PlanService.getPlanFeatureIdDetails(currentPlan, "Individual");
		String featuresListArray[] = featuresIdList.split(",");
		for(int i=0;i<featuresListArray.length;i++)
		{
			int featureId = Integer.parseInt(featuresListArray[i]);
			String featureNames = PlanService.getFeatureNameById(featureId);
			if(featureNames.equalsIgnoreCase("SMS"))
			{
				restrictStatus += PlanService.getFeatureValue(featureId);
			}
		}
		
		 int noOfSMSAsPerPlan = 0;
		
		 int noOfSMSAsOfNow = PlanService.noOfSMSAsOfNow(ind_id);
		 
		 String clean = restrictStatus.replaceAll("\\D+","");
		 
		 if(!clean.equals(""))
		 {
			 noOfSMSAsPerPlan = Integer.parseInt(clean);
		 }
		 
		 
		 if(noOfSMSAsPerPlan>noOfSMSAsOfNow)
		 {
			 return "Allow";
		 }
		 else
		 {
			 return "Not Allow";
		 }
		
	}
	
	public String checkSMSStatusFromUtilizer(int utiliserId)
	{		
		String currentPlanAndDomain = PlanService.getCurrentPlanNameOfUtilizer(utiliserId);
		String  restrictStatus = "";
		String planName = currentPlanAndDomain.split(",")[0];
		String planDomain = currentPlanAndDomain.split(",")[1];
		String featuresIdList =  PlanService.getFeaturesForUtilizer(planName,"Utilizer",planDomain);
		
		String featuresListArray[] = featuresIdList.split(",");
		for(int i=0;i<featuresListArray.length;i++)
		{
			int featureId = Integer.parseInt(featuresListArray[i]);
			String featureNames = PlanService.getFeatureNameById(featureId);
			if(featureNames.equalsIgnoreCase("SMS"))
			{
				restrictStatus += PlanService.getFeatureValue(featureId);
			}
		}
		
		 int noOfSMSAsPerPlan = 0;
		
		 int noOfSMSAsOfNow = PlanService.noOfSMSAsOfNowAsOfNowOfUtilizer(utiliserId);
		 
		 String clean = restrictStatus.replaceAll("\\D+","");
		 
		 if(!clean.equals(""))
		 {
			 noOfSMSAsPerPlan = Integer.parseInt(clean);
		 }		 
		 if(noOfSMSAsPerPlan>noOfSMSAsOfNow)
		 {
			 return "Allow";
		 }
		 else
		 {
			 return "Not Allow";
		 }
		 
	}
}
