package com.kyc.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.service.DocService;
import com.kyc.service.IndSignupService;



@Controller
public class StoreController {

	@Autowired
	private DocService docservice;
	
	
	@Autowired
	private IndSignupService signupService;
	
	
	static final Logger LOGGER = Logger.getLogger(StoreController.class);
    
	
	
	@RequestMapping(value = "/individualstore", method = RequestMethod.GET)
	public String getIndividualStore(HttpServletRequest request,HttpSession session) {
		
		   session = request.getSession(false);
			
			if (session.getAttribute("Ind_id") == null)
			{			
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //////session.invalidate();
				 
				 return "individualPage";	
			}
			else if(((String)session.getAttribute("Ind_id")).equals(null))
			{
				 request.setAttribute("kycdocumentList", docservice.getKYCComp());
				 request.setAttribute("status","Your session has expired please login to use the portal !");
				
				 session.removeAttribute("Ind_id");
				 //////session.invalidate();
				 
				 return  "individualPage";	
			}
			else
			{	
			
				int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
				request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
				
				return "individualStore";
			}
	}
	
}
