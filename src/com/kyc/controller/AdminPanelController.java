package com.kyc.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.AuthenticationFailedException;
import javax.mail.IllegalWriteException;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.bean.FeaturesDetailsBean;
import com.kyc.bean.KycNotificationBean;
import com.kyc.bean.PatternDetailsBean;
import com.kyc.bean.UtiliserProfileBean;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentHierarchyModel;
import com.kyc.model.DomainHierarchyModel;
import com.kyc.model.DomainLevelModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.PatternDetailsModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;
import com.kyc.service.AccessService;
import com.kyc.service.AdminPanelService;
import com.kyc.service.ProfileinfoService;
import com.kyc.util.ClenseeAdvertise;
import com.kyc.util.FileEncryptionDecryptionUtil;
import com.kyc.util.PasswordEncryptionDecryption;
import com.kyc.util.RecordScedulerTime;
import com.kyc.util.SendEmailUtil;
import com.kyc.util.SendSms;

@Controller
public class AdminPanelController {
	
	@Autowired
	private AdminPanelService apService;
	
	@Autowired
	private AccessService accservice;
	
	@Autowired
	private ProfileinfoService profileinfoService;
	
	
	static final Logger LOGGER = Logger.getLogger(AdminPanelController.class);
    
	
	
	// Required file Config for entire Controller 
	
	 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
	 String kycMainDirectory = fileResource.getString("fileDirectory");	
	 String kycSubDirectory = fileResource.getString("fileSubDirectory");	
	 File filePath = new File(kycMainDirectory);
	
	// End of Required file Config for entire Controller 
		
	 
		
	// Config for SMS Service.
	
		ResourceBundle smsresource = ResourceBundle.getBundle("resources/smsParameters");
		String userName=smsresource.getString("userName");
		String password = smsresource.getString("password");
		String senderId = smsresource.getString("senderId");
		String smsMessage1 = smsresource.getString("notifynonKycUsersms");
		String smsMessage2 = smsresource.getString("notifyKycUsersms");
		String smscommonUrl = smsresource.getString("commonUrl");
		String smsfromDetails = smsresource.getString("fromDetails");
			   
	    SendSms obj = new SendSms();
		
			
		// End of Config for SMS Service
	    
	    

	    
    @RequestMapping(value="/adminLogin", method = RequestMethod.GET)	
	public ModelAndView adminLogin(HttpServletRequest request,HttpSession session)
	{					
    	return new ModelAndView("adminLoginPage");		
	}

    @SuppressWarnings("unused")
	@RequestMapping(value="/authenticateAdminLogin", method = RequestMethod.GET)	
	public ModelAndView authenticateAdminbyGet(HttpServletRequest request,HttpSession session)
	{	
    	if (session.getAttribute("adminId") == null)
		{						
			session.removeAttribute("adminId");
	    	
	    	request.setAttribute("status","failure");
			return new ModelAndView("adminLoginPage");		
		}
		else if(((String)session.getAttribute("adminId")).equals(null))
		{
			session.removeAttribute("adminId");
	    	
	    	request.setAttribute("status","failure");
			return new ModelAndView("adminLoginPage");		
		}
		else if(session == null)
		{
			session.removeAttribute("adminId");
	    	
	    	request.setAttribute("status","failure");
			return new ModelAndView("adminLoginPage");		
		}
		else 
		{
			session.setAttribute("adminId", "adfgthkyc");
			return new ModelAndView("adminHome");		
		}
	}
    
    @RequestMapping(value="/authenticateAdminLogin", method = RequestMethod.POST)	
	public ModelAndView authenticateAdmin(HttpServletRequest request,HttpSession session)
	{			
		String adminUserId = request.getParameter("userId");
		String adminPassword =  request.getParameter("password");
		
		ResourceBundle smsresource = ResourceBundle.getBundle("resources/adminAuthenticate");
		String userName=smsresource.getString("adminusername");
		String password = smsresource.getString("adminpassword");
		
		if(adminUserId.equals(userName) && adminPassword.equals(password))
		{
			session.setAttribute("adminId", "adfgthkyc");
			return new ModelAndView("adminHome");		
		}   	
		else
		{
			request.setAttribute("status","failure");
			return new ModelAndView("adminLoginPage");		
		}  	
	}
    

    @RequestMapping(value="/redirecttoLogin", method = RequestMethod.GET)	
	public ModelAndView redirecttoLogin(HttpServletRequest request,HttpSession session)
	{			
    	session.removeAttribute("adminId");
    	//session.invalidate();   	
    	request.setAttribute("status", "success");	   
    	return new ModelAndView("adminLoginPage");		
	}
    
    @SuppressWarnings("unused")
	@RequestMapping(value="/sessioncheck", method = RequestMethod.POST)	
   	public @ResponseBody String sessionCheckforAdmin(HttpServletRequest request,HttpSession session)
   	{	
    	if (session.getAttribute("adminId") == null)
		{						
			session.removeAttribute("adminId");
	    	//session.invalidate();
	    	
	    	request.setAttribute("status", "success");	    	 
			return "Not Exist";		
		}
		else if(((String)session.getAttribute("adminId")).equals(null))
		{
			session.removeAttribute("adminId");
	    	//session.invalidate();
	    	
	    	request.setAttribute("status", "success");
			return "Not Exist";		
		}
		else if(session == null)
		{
			session.removeAttribute("adminId");
	    	//session.invalidate();
	    	
	    	request.setAttribute("status", "success");
			return "Not Exist";			
		}
		else
		{				
     	     return "Exist";					
		}		  			
   	}
   
    @RequestMapping(value="/logout_admin", method = RequestMethod.GET)	
   	public ModelAndView logout_admin(HttpServletRequest request,HttpSession session)
   	{	
    	if (session.getAttribute("adminId") == null)
		{
    		return new ModelAndView("adminLoginPage");		
		}
    	else
    	{
    		 session.removeAttribute("adminId");       	 
        	 request.setAttribute("status", "success");
        	 
       	     return new ModelAndView("adminLoginPage");		
    	}
    	  			
   	}
    
	@RequestMapping(value="/sendAjax", method = RequestMethod.POST)	
	public @ResponseBody String add(HttpServletRequest request, HttpServletResponse response)
	{					
		return apService.getAllUsersDetails().toString();						
	}
	
	@RequestMapping(value="/createAdminPattern.html",method = RequestMethod.GET)
	public ModelAndView getCreatePattern(HttpServletRequest request,HttpSession session)
	{
		if(((String)session.getAttribute("adminId"))!=null)
        {
			return new ModelAndView("admin_myPattern");
        }
        else
        {
        	request.setAttribute("status","failure");
			return new ModelAndView("adminLoginPage");		
        }	
		
	}
	
	@RequestMapping(value="/editAdminPattern.html",method = RequestMethod.POST)
	public ModelAndView getEditPattern(HttpServletRequest request)
	{
		String pattern_name = apService.getPatternNames();		
		request.setAttribute("patt_name",pattern_name);		
		return new ModelAndView("admin_edit_pattern");
	}
	
	@RequestMapping(value="/deleteAdminPattern.html",method = RequestMethod.POST)
	public ModelAndView getDeletePattern()
	{
	    
	    Map<String, Object> model = new HashMap<String, Object>();
		model.put("PatternDetails", preparePatternDetailBean(apService.patternforDelete()));
	    
		return new ModelAndView("admin_delete_pattern",model);
	}
	
	 private List<PatternDetailsBean> preparePatternDetailBean(List<PatternDetailsModel> docdetails) {
     	
        	
     	List<PatternDetailsBean> bean = null;
     	
		if (docdetails != null && !docdetails.isEmpty()) {
			bean = new ArrayList<PatternDetailsBean>();
			PatternDetailsBean beans = null;

			for (PatternDetailsModel docdetail : docdetails) {
                
				beans = new PatternDetailsBean();
             
                beans.setPattern_id(docdetail.getPattern_id());
				beans.setPattern_name(docdetail.getPattern_name());
				beans.setCr_by(docdetail.getCr_by());
				beans.setCr_date(docdetail.getCr_date());
				
				bean.add(beans);
				
			}
		}
 		return bean;
 	}
	@RequestMapping(value="/code_grp",method = RequestMethod.POST)
	public @ResponseBody String  getKyc_grp(HttpServletRequest request)
	{
		
		String type = request.getParameter("doctype");
		
		String docs = apService.findtypeGroup(type);
		
		System.out.println(docs.toString());
		
		return docs;		
		
	}
	
	@RequestMapping(value="/subValue",method = RequestMethod.POST)
	public @ResponseBody String  getSub_cat(HttpServletRequest request)
	{		
		String type = request.getParameter("sub_type");		
		String docs = apService.findSub_category(type);				
		return docs;				
	}
		
	@RequestMapping(value="/patterninfo",method = RequestMethod.POST)
	public @ResponseBody String  patternDetails(HttpServletRequest request)
	{
		String pattern = request.getParameter("newPattern");		
		String patternName = request.getParameter("p_Name");
		
		//String patternStatus = apService.getPatternExistorNot
		
		if("success".equals(apService.savePattern(pattern,patternName)))
        {
        	return "success";
        }		
        else
        {
        	return "failed";
        }		
		
	}
	
	
	@RequestMapping(value="/getPatternDetailsviaName",method = RequestMethod.POST)
	public @ResponseBody String  getPatternDetailsviaName(HttpServletRequest request)
	{
		String patternName = request.getParameter("patternName");		
		
		String patternDetails = apService.getPatternDetailAsperPatterName(patternName);
		
		return patternDetails;
	}
	
	
	@RequestMapping(value="/updatePatternDetails",method = RequestMethod.POST)
	public @ResponseBody String  updatePatternDetails(HttpServletRequest request)
	{
			
		String existPatternName = request.getParameter("existPatternName");
		String newpattern = request.getParameter("patternName");	
		String patternDetails = request.getParameter("patternValue");	
		
		if("success".equals(apService.updatePattern(existPatternName,newpattern,patternDetails)))
        {
        	return "success";
        }		
        else
        {
        	return "failed";
        }		
		
	}
	
	@RequestMapping(value="/edit_grp",method = RequestMethod.POST)
	public @ResponseBody String  get_editgrp(HttpServletRequest request)
	{		
		String type = request.getParameter("doctype");
		String patternCategory = "";
		
		if(type.equals("REGISTRATION"))
		{
			patternCategory += "Email Id,Gender,Mobile No";
		}
		else if(type.equals("BASIC"))
		{
			patternCategory += "Alternate Email Id,Date of Birth,Residence Contact no,Office Contact no,Marital Status,Present Address,Permanent Address";
		}
		else if(type.equals("FAMILY"))
		{
			patternCategory += "Father Name,Mother Name,Sibling Name,Spouse Name";
		}
		else
		{
			patternCategory += apService.findtypeGroup(type);		
		}
		
		
		return patternCategory;				
	}
			
	@RequestMapping(value="/findPattern",method=RequestMethod.POST)
	public @ResponseBody String getPattern(HttpServletRequest request)
	{
		String Pattern_Name = request.getParameter("p_Name");
		
		String str = apService.findPattern(Pattern_Name);
		
		String str1[]= str.split("/,"); 
		String docType = "";
		
		for(int i=0;i<str1.length;i++)
		{
			String str2[] = str1[i].split(":");
			docType = docType + str2[0] + ",";
			
		}	
			
		return docType.substring(0,docType.length()-1);
	}
	
	@RequestMapping(value="/upDatePattern",method=RequestMethod.POST)
	public @ResponseBody String getUpdatePattern(HttpServletRequest request)
	{		
		String pattern = request.getParameter("pattern");
		String pattern_name = request.getParameter("pattern_name");
		
		String update_pattern = apService.upDatePattern(pattern_name,pattern);
		
		return update_pattern;
	}
	
	@RequestMapping(value="/deletePattern",method=RequestMethod.POST)
	public @ResponseBody String getDeletePattern(HttpServletRequest request)
	{
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		String delete = apService.deletePattern(p_id);
		
		return delete;
	}
	
	  
	
	@RequestMapping(value="/viewUtiliser",method=RequestMethod.POST)
	public ModelAndView getViewUtiliser()
	{		
		return new ModelAndView("viewUtiliser");
	}
	 
    @RequestMapping(value="/createUtiliser",method=RequestMethod.GET)
	public ModelAndView getCreateUtiliser(HttpServletRequest request,HttpSession session)
	{
    	if(((String)session.getAttribute("adminId"))!=null)
        {
    		String pattern_name = apService.getPatternNames();
    		request.setAttribute("patt_name",pattern_name);
        	
    		String domain_Type = apService.getDomainName();
    		request.setAttribute("domainType", domain_Type);
    		
    		String plan_name = apService.getPlan_Name();		
    		request.setAttribute("plan_name", plan_name);
    		
    		return new ModelAndView("admin_myUtilizer");
        }
        else
        {
        	request.setAttribute("status","failure");
			return new ModelAndView("adminLoginPage");		
        }	
   	    	
	}
	
    @RequestMapping(value="/subdomaincreateutiliser",method=RequestMethod.POST)
	public @ResponseBody String subDomainCreateUtiliser(HttpServletRequest request)
	{
    	
        String searchResult = "";
        String domainName = request.getParameter("domainValue");
        searchResult = apService.getSubDomainForCreateUtiliser(domainName);    
	    
        return searchResult;
	}
    
    @RequestMapping(value="/heirarchysubdomain",method=RequestMethod.POST)
   	public @ResponseBody String subheirarchyCreateUtiliser(HttpServletRequest request)
   	{
           String searchResult = "";
           int  domainId = Integer.parseInt(request.getParameter("domainValue"));
           String currentLevel = request.getParameter("currentLevel");
           searchResult = apService.getHeirarchyFromSubDomain(domainId,currentLevel);    
   	       
           return searchResult;
   	}
    
    @RequestMapping(value="/getselectiondomainName",method=RequestMethod.POST)
   	public @ResponseBody String getselectiondomainName(HttpServletRequest request)
   	{
    	String searchResult = "";
    	String selectedList = request.getParameter("selectedList");
    	
    	searchResult = apService.getSelectedNames(selectedList);
    	
    	return searchResult;
   	}
    
    
    
	@RequestMapping(value="/organiseUtiliser",method=RequestMethod.POST)
	public ModelAndView getOrganiseUtiliser()
	{		
		return new ModelAndView("organiseUtiliser");
	}
	
	
	
	@RequestMapping(value="/Profile_searchUtiliser",method=RequestMethod.POST)
	public @ResponseBody String getSearchResult(HttpServletRequest request)
	{
		
		String searchData = request.getParameter("searchdata");
		String searchType = request.getParameter("type");
				
		String searchResult = apService.searchUtiliser(searchData,searchType);
		
		return searchResult;
	}
	
	
	@RequestMapping(value="/get/planname/as/per/plandomain",method=RequestMethod.POST)
	public @ResponseBody String getPlanNameAsPerPlanDomain(HttpServletRequest request)
	{
		String planDomain = request.getParameter("plandomain");
		String searchResult = apService.getPlanNameAsPerPlanDomain(planDomain);
		
		return searchResult;
	}
	
	
	@RequestMapping(value="/saveUtiliser",method=RequestMethod.POST)
	public @ResponseBody String getSaveCreateUtiliser(HttpServletRequest request,@RequestParam CommonsMultipartFile[] userPhoto) throws IllegalStateException, IOException, IllegalWriteException, AuthenticationFailedException, SendFailedException
	{
		
		int p_id  = apService.findPatternId(request.getParameter("pattern_name"));
        
		String uti_KYC_id = "UTI";
		
		String password = "";
		
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String phno = request.getParameter("ofcphno1");
 		
		uti_KYC_id = uti_KYC_id + firstName.charAt(firstName.length()-1) +lastName.charAt(2) + phno.charAt(3);	
		
		int lastUtiId = apService.getLastUtilizerIndId();
		int passwordAlgo = p_id*2+lastUtiId;
		String passwordAlgo2 = ""+firstName.charAt(1);
		int passwordAlgo3 = passwordAlgo*3;
		
		//password = password + passwordAlgo + passwordAlgo2 + firstName + passwordAlgo3 + firstName.charAt(2) + "jdfgk" +request.getParameter("plan_name").charAt(3);		
		
		password = password + passwordAlgo + passwordAlgo2 + firstName + firstName.charAt(2);		
					
		UtiliserProfileModel utiProfile = new UtiliserProfileModel();
		
		utiProfile.setUti_first_name(request.getParameter("firstName"));
		utiProfile.setUti_middle_name(request.getParameter("middleName"));
		utiProfile.setUti_last_name(request.getParameter("lastName"));
		utiProfile.setUti_reg_kyc_id(request.getParameter("utiregkycId"));
		utiProfile.setUti_email_id(request.getParameter("emailid"));
		utiProfile.setUti_office_emailId(request.getParameter("officeEmailId"));
		utiProfile.setUti_office_first_no(request.getParameter("ofcphno1"));
		utiProfile.setUti_office_second_no(request.getParameter("ofcphno2"));
		utiProfile.setUti_plan_domain(request.getParameter("plan_domain"));
		utiProfile.setUtiliser_plan(request.getParameter("plan_name"));
		utiProfile.setUti_dom_hierarchy(request.getParameter("domain"));
		utiProfile.setUti_gender(request.getParameter("gender"));
		utiProfile.setUti_office_Address(request.getParameter("ofcaddress"));
		utiProfile.setUti_head_office_Address(request.getParameter("headofcaddress"));
		utiProfile.setPattern_id(p_id);
		utiProfile.setKyc_uti_id(request.getParameter("utiregkycId"));
				
		utiProfile.setPassword(PasswordEncryptionDecryption.getEncryptPassword(password));
		
		String refStr1 = request.getParameter("ofcphno1");
		String refStr2 = request.getParameter("ofcphno2");
		int refStr3 = p_id;
		String refStr4 = refStr1.substring(refStr1.length()-3, refStr1.length());
		String refStr5 = refStr2.substring(refStr2.length()-3, refStr2.length());
		long refStr6 = Integer.parseInt(refStr4)*2;
		long refStr7 = Integer.parseInt(refStr5)*2;
		int refStr8 = (int)(refStr7-refStr3);
		
		String finalRefStr = refStr6+"q67l"+"03"+refStr8+"95"+refStr7+"pw";
				
		utiProfile.setUti_cr_date(new Date());
		utiProfile.setUti_reference_id(finalRefStr);
		
		if("success".equals(apService.saveUtiliserDetails(utiProfile)))
		{	
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
			String utilizerProfilePhoto = fileResource.getString("utilizerProfilePhoto");			
			File ourPath = new File(utilizerProfilePhoto);		
			
			int uti_id = apService.getUtiliserId(utiProfile);
			
			UtilizerPlanDetailsModel utiPlanModel = new UtilizerPlanDetailsModel();
			utiPlanModel.setUti_ind_id(uti_id);
			utiPlanModel.setUti_plan_domain(request.getParameter("plan_domain"));
			utiPlanModel.setUti_activate_plan(request.getParameter("plan_name"));
			utiPlanModel.setCr_date(new Date());
			
			String planActiveStatus = apService.saveUtilizerPlanDetails(utiPlanModel);
						
			File fld = new File(ourPath.getPath()+"/"+uti_id);
	    	fld.mkdirs();
			String loc=ourPath.getPath()+"/"+uti_id;
			
			String fileName = "";
			if (userPhoto != null && userPhoto.length > 0)
	        {			
	            for (CommonsMultipartFile aFile : userPhoto)            
	            {             
	                if (!aFile.getOriginalFilename().equals(""))                 
	                {   	                	
	                    aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));	                                       	                                                            
	                }
	               
	                utiProfile.setUti_ind_id(uti_id);
	                utiProfile.setUti_profile_pic(aFile.getOriginalFilename());
	                fileName = aFile.getOriginalFilename();
	                apService.updateUtiliserProfilePhoto(utiProfile); 
	            }
	        }
			
			UtilizerProfilePhotoModel utiProfilePhoto = new UtilizerProfilePhotoModel();
			utiProfilePhoto.setUti_ind_id(uti_id);
			utiProfilePhoto.setUti_profile_photo(fileName);
			utiProfilePhoto.setCr_date(new Date());
			
			String profilePhotoStatus = apService.saveUtilizerProfilePhoto(utiProfilePhoto);
			
			
			ResourceBundle resource = ResourceBundle.getBundle("resources/utilizerSignUpMailFromAdmin");
			
			
			 String headingImage=resource.getString("headingImage");
	    	 String belowleftImage=resource.getString("belowleftImage");
	    	 String belowrightImage=resource.getString("belowrightImage");
	    	 String mailicon=resource.getString("mailicon");
	    	 String contacticon=resource.getString("contacticon");
	    	 
	    	 String emailHeading=resource.getString("emailHeading");
	    	 String contactNo=resource.getString("contactNo");
	    	 String contactEmail=resource.getString("contactEmail");
	    	 
	    	 String subject=resource.getString("utilizersignupsubject");
			 String messageBody=resource.getString("utilizersignupmessage");		
			 String message1 = resource.getString("utilizeraftermessageextraline1");
			 String message2 = resource.getString("utilizeraftermessageextraline2");
			 
	    	 String copyright=resource.getString("copyright");
	    	 String termscondition=resource.getString("termscondition");
	    	 String privacyPolicy=resource.getString("privacyPolicy");
	    	 
	    	 String facebookicon=resource.getString("facebookicon");
	    	 String twittericon=resource.getString("twittericon");
	    	 String googleicon=resource.getString("googleicon");
	    	 String linkedicon=resource.getString("linkedicon");
	    	 
	    	 String facebookLink=resource.getString("facebookLink");
	    	 String twitterLink=resource.getString("twitterLink");
	    	 String googlePlus=resource.getString("googlePlus");
	    	 String linkedIn=resource.getString("linkedIn");
	    	 
	    	 String clenseeLogo = resource.getString("clenseeLogo");
	    	 String clickhere = resource.getString("clickhere");
	    	 String actionUrl = resource.getString("actionUrl");
	    	 
	    	 
	        StringBuilder text = new StringBuilder();
        
	        String  resetStatus = "";
	        
	        
	        /* Sending Emails to Register user */
		       
	        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
	        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
	       
	        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
	       
	        /* Header */
	        
	        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
	        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
	        text.append("</tr></table></td></tr></table>");
	       
	        /* End of Header */
	        
	        /* Header-2 */
	        
	        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
	        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
	        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
	        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
	        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
	        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
	        text.append("</p></td></tr></table></td></tr></table>");
	        
	        /* End of Header-2 */
	        
	        
	        /* BODY PART */
	        
	        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
	        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
	        text.append("<h3 style='color: #004879;'>Dear "+request.getParameter("firstName")+",</h3>");
	        text.append("<p style='text-align: justify'>"+messageBody+"</p><p>");
	        text.append(""+message1+"<br></p>");	 
	        text.append("Usename - KYCID/Email Id,Password - "+password+""+"<br></p><br>");	 
	        text.append("This is an auto generated Password change after Login.<br></p><br>");	 
	        text.append(""+message2+"<br><br></p>");	
	        text.append("</p>");
	       
	        
	        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
	        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
	        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>CLENSEE</td>");
	        text.append("</tr></table></td></tr>");
	        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
	        text.append("</table> ");
	        
	        
	        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
	        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
	        
	        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
	        
	        text.append("<tr><td>");
	        
	        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
	        text.append("</td></tr></table>");
	       
	        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
	        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
	        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
	        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
	        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
	        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
	        
	        
	        text.append("</tr>");
	        text.append("</table></td></tr></table></td></tr></table>");
	        text.append("</body>");
	        text.append("</html>");
	       
	        /* END OF BODY PART */
	        
	        
	        /* End of Sending Emails to Register user */
	        	 	            
	        SendEmailUtil obj = new SendEmailUtil();
	        
			resetStatus = obj.getEmailSent(request.getParameter("emailid"),subject,text.toString());
			request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
	     
	    	System.out.println("status="+resetStatus);
	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
	    	
	    	if(resetStatus.equals("success"))
	    	{	    		
	    		emailReport.setEmail_id(request.getParameter("emailid"));
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("0");
	    	}
	    	else
	    	{
	    		emailReport.setEmail_id(request.getParameter("emailid"));
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("1");	    			    		
	    	}
	    	
	    	profileinfoService.setEmailStatusReport(emailReport);
	    	
			return "Utilizer Details Saved";
		}
		else
		{
			return "Some Problem Arise Try Again";
		}
		
		
	}
	
	
	
	@RequestMapping(value="/UpdateUtiliserDetails",method=RequestMethod.POST)
	public ModelAndView getupdateUtiliser(HttpServletRequest request)
	{			
		String utiid = request.getParameter("uniquId");
		
		String pattern_name = apService.getPatternNames();
		request.setAttribute("patt_name",pattern_name);
    	
		String domain_Type = apService.getDomainName();
		request.setAttribute("domainType", domain_Type);
		
		String plan_name = apService.getPlan_Name();		
		request.setAttribute("plan_name", plan_name);
					
		List<UtiliserProfileModel> utiDetails = apService.UtiliserProfileDetails(utiid);
		
		request.setAttribute("uti_ind_id",utiDetails.get(0).getUti_ind_id());
		
		request.setAttribute("kycId",utiDetails.get(0).getKyc_uti_id());
		request.setAttribute("firstName",utiDetails.get(0).getUti_first_name());
		request.setAttribute("middleName",utiDetails.get(0).getUti_middle_name());
		request.setAttribute("lastName",utiDetails.get(0).getUti_last_name());
		request.setAttribute("utiEmailId",utiDetails.get(0).getUti_email_id());
		request.setAttribute("utiOfcEmailId",utiDetails.get(0).getUti_office_emailId());
		request.setAttribute("utiOfcNo1",utiDetails.get(0).getUti_office_first_no());
		request.setAttribute("utiOfcNo2",utiDetails.get(0).getUti_office_second_no());
		request.setAttribute("gender",utiDetails.get(0).getUti_gender());
		request.setAttribute("ofcAddress",utiDetails.get(0).getUti_office_Address());
		request.setAttribute("headOfcAddress",utiDetails.get(0).getUti_head_office_Address());
		request.setAttribute("plandomain",utiDetails.get(0).getUti_plan_domain());
		request.setAttribute("planname",utiDetails.get(0).getUtiliser_plan());
		request.setAttribute("patternId",utiDetails.get(0).getPattern_id());
		request.setAttribute("domain",utiDetails.get(0).getUti_dom_hierarchy());
		
		int patternId = utiDetails.get(0).getPattern_id();
		String patternName = apService.getPatternName(patternId);
		request.setAttribute("patternName",patternName);
		
		return new ModelAndView("updateUtiliserProfile");
	}
	

	    @RequestMapping(value="/updateUtiliser",method=RequestMethod.POST)
		public @ResponseBody String getUpdateUtiliser(HttpServletRequest request)
		{
	    	int p_id  = apService.findPatternId(request.getParameter("pattern_name"));
	    	
	    	int utiUnqId = Integer.parseInt(request.getParameter("utiUnqId"));
	    	
	    	String password = apService.getPassword(utiUnqId);
	    	
	    	String plan_domain = "";
	    	String plan_name = "";
	    	String domain = "";

	    	
	    	UtiliserProfileModel utiProfile = new UtiliserProfileModel();
			
	    	utiProfile.setUti_ind_id(utiUnqId);
			utiProfile.setUti_first_name(request.getParameter("firstName"));
			utiProfile.setUti_middle_name(request.getParameter("middleName"));
			utiProfile.setUti_last_name(request.getParameter("lastName"));
			utiProfile.setUti_reg_kyc_id(request.getParameter("utiregkycId"));
			utiProfile.setUti_email_id(request.getParameter("emailid"));
			utiProfile.setUti_office_emailId(request.getParameter("officeEmailId"));
			utiProfile.setUti_office_first_no(request.getParameter("ofcphno1"));
			utiProfile.setUti_office_second_no(request.getParameter("ofcphno2"));
			
			if(request.getParameter("plan_domain").equals("Select a Plan Domain"))
			{
	    		plan_domain += apService.getPlanDomain(utiUnqId);
	    		utiProfile.setUti_plan_domain(plan_domain);
			}
	    	
			if(!request.getParameter("plan_domain").equals("Select a Plan Domain"))
			{
				utiProfile.setUti_plan_domain(request.getParameter("plan_domain"));
			}
						
	    	if(request.getParameter("plan_name")==null)
			{
	    		plan_name += apService.getUtilizerPlanName(utiUnqId);
	    		utiProfile.setUtiliser_plan(plan_name);
			}
	    	
	    	if(request.getParameter("plan_name")!=null)
			{
	    		utiProfile.setUtiliser_plan(request.getParameter("plan_name"));
			}
	    	
	    	if(request.getParameter("domain").equals(""))
			{
	    		domain += apService.getUtilizerDomain(utiUnqId);
	    		utiProfile.setUti_dom_hierarchy(domain);
			}
	    	
	    	if(!request.getParameter("domain").equals(""))
			{
	    		utiProfile.setUti_dom_hierarchy(request.getParameter("domain"));
			}
	    				
			utiProfile.setUti_gender(request.getParameter("gender"));
			utiProfile.setUti_office_Address(request.getParameter("ofcaddress"));
			utiProfile.setUti_head_office_Address(request.getParameter("headofcaddress"));
			utiProfile.setPattern_id(p_id);
			utiProfile.setKyc_uti_id(request.getParameter("utiregkycId"));
			utiProfile.setPassword(password);
			utiProfile.setUti_cr_date(apService.getUtiRegDate(utiUnqId));
			utiProfile.setUti_profile_pic(apService.getUtiProfilePic(utiUnqId));
			utiProfile.setUti_reference_id(apService.getUtiRefId(utiUnqId));
			
			if("success".equals(apService.updateUtilizerDetails(utiProfile)))
			{
				return "success";			
			}
			else
			{
				return "failure";			
			}
			
		}
	 
	 	    
	    @RequestMapping(value="/viewindividual.html",method = RequestMethod.GET)
		public ModelAndView viewindividual(HttpSession session,HttpServletRequest request)
		{
	        if(((String)session.getAttribute("adminId"))!=null)
	        {
	        	return new ModelAndView("admin_myIndividual");
	        }
	        else
	        {
	        	request.setAttribute("status","failure");
				return new ModelAndView("adminLoginPage");		
	        }	        	              
		}
		
		@RequestMapping(value="/organiseindividual",method = RequestMethod.POST)
		public ModelAndView organiseindividual()
		{
			return new ModelAndView("organiseindividual");
		}
		
		@RequestMapping(value="/changestatus",method = RequestMethod.POST)
		public @ResponseBody String changestatus(HttpServletRequest request)
		{
			
			int indid = Integer.parseInt(request.getParameter("indid"));
			String statusvalue = request.getParameter("status");
			
			IndSignupModel changestatus=new IndSignupModel();
			
			if(statusvalue.contentEquals("1"))
			{
				changestatus.setStatus("0");
				changestatus.setIndid(indid);
			}
			
			if(statusvalue.contentEquals("0"))
			{
				changestatus.setStatus("1");
				changestatus.setIndid(indid);
			}
			
		    String resultStatus = apService.changestatus(changestatus); 
		    
		    return resultStatus;
		}
		
		
		@RequestMapping(value="/viewindividualsection", method = RequestMethod.POST)
		public @ResponseBody String add1(HttpServletRequest request, HttpServletResponse response){
			
			IndSignupModel searchprofile=new IndSignupModel();						
			String selectedvalue=request.getParameter("selectedvalue");			
			String kycid=request.getParameter("kycid");
					
				if(selectedvalue.equals("kycid"))
				{					 
					searchprofile.setKycid(kycid);
					return apService.searchindividualkycid(searchprofile).toString();							 
				}
				
				else if(selectedvalue.equals("plan"))
				{					 
				    searchprofile.setKycid(kycid);
					return apService.searchindividualplan(searchprofile).toString();			
				}
				
				else if(selectedvalue.equals("name"))
				{						
				    searchprofile.setKycid(kycid);
					return apService.searchindividualname(searchprofile).toString();			
				}
				
				else 
				{										
					searchprofile.setFirstname(kycid);
					return "kcorrect";
				}			
			}
		
		@RequestMapping(value="/orgindividualsection", method = RequestMethod.POST)
		public ModelAndView orgindividualsection(HttpServletRequest request, HttpServletResponse response){
			
						
			String selectedvalue = request.getParameter("selectedvalue");			
			String searchedValue = request.getParameter("kycid");
			
			List<IndSignupModel> indDetails = new ArrayList<IndSignupModel>();			
			System.out.println("Search Details are="+selectedvalue+" "+searchedValue);
			
				if(selectedvalue.equals("kycid"))
				{					 
					 indDetails = apService.getIndividualDetailsViaKycid(searchedValue);							 
				}
				
				if(selectedvalue.equals("plan"))
				{					 
					 indDetails = apService.getIndividualDetailsViaPlan(searchedValue);				
				}
				
				if(selectedvalue.equals("name"))
				{						
					 indDetails = apService.getIndividualDetailsViaName(searchedValue);			
				}				
				
				Map<String, Object> model = new HashMap<String, Object>();
				model.put("indDetails",indDetails);
								
				return new ModelAndView("adminControlIndividual",model);
			}
		
		
		@RequestMapping(value="/createKycHierarchy",method = RequestMethod.GET)
		public ModelAndView createKycHierarchy(HttpServletRequest request,HttpSession session)
		{						
			if(((String)session.getAttribute("adminId"))!=null)
	        {
				String kycHierarchy = apService.getkycHierarchy();				
				request.setAttribute("kycHierarchy",kycHierarchy);
				
				return new ModelAndView("admin_DocumentHierachy");
	        }
	        else
	        {
	        	request.setAttribute("status","failure");
				return new ModelAndView("adminLoginPage");		
	        }				
		}
		
		
		@RequestMapping(value="/addkycHierarchy",method = RequestMethod.POST)
		public ModelAndView createkycHierarchy(HttpServletRequest request)
		{									
			String kycType = request.getParameter("docType");			
			String kycHierarchy = apService.getkycHierarchy();	
			
			DocumentHierarchyModel hierModel = new DocumentHierarchyModel();
			
			hierModel.setDoc_hir_name("KYC_DOCUMENT");
			hierModel.setDoc_type("KYC");
			hierModel.setCr_date(new Date());
			
			if(!kycHierarchy.equals("No Data"))
			{
				kycHierarchy = kycHierarchy.substring(0, kycHierarchy.length()-1);
				
				int hier_id = apService.getHierId("KYC");
				hierModel.setDoc_hir_id(hier_id);
				
				kycHierarchy += ","+kycType;				
				hierModel.setSubdoc_type(kycHierarchy);		
				hierModel.setDoc_hierarchy(kycHierarchy);
			}
			else
			{				
				hierModel.setSubdoc_type(kycType);		
				hierModel.setDoc_hierarchy(kycType);
			}
			
			String status = apService.saveKycHierarchy(hierModel);			
			String currentKycHierarchy = apService.getkycHierarchy();				
			request.setAttribute("kycHierarchy",currentKycHierarchy);
			
			return new ModelAndView("redirect:/createKycHierarchy.html");
		}
		
		
		@RequestMapping(value="/removeKycHierarchy",method = RequestMethod.POST)
		public String removeKycHierarchy(HttpServletRequest request)
		{
			String click[] = request.getParameterValues("click");
			String kycHierName="";			
			
			List list =  Arrays.asList(click); 
			Iterator<String> itr=list.iterator();
			
			while(itr.hasNext())
			{			
				kycHierName = kycHierName.concat(itr.next()+",");			
			}
			
			if(kycHierName.length()>0)
			{
				kycHierName= kycHierName.substring(0, kycHierName.length()-1);				
			}
												
			String kycHierarchy = apService.getkycHierarchy();
			
			if(kycHierarchy.length()>0)
			{
				kycHierarchy = kycHierarchy.substring(0, kycHierarchy.length()-1);		
			}
							
			int hier_id = apService.getHierId("KYC");
			
			String newkycHierarchy = "";
			
            DocumentHierarchyModel hierModel = new DocumentHierarchyModel();			
			hierModel.setDoc_hir_name("KYC_DOCUMENT");
			hierModel.setDoc_type("KYC");
			hierModel.setCr_date(new Date());						
			hierModel.setDoc_hir_id(hier_id);
			
			for(int i=0;i<kycHierName.split(",").length;i++)
			{
				for(int j=0;j<kycHierarchy.split(",").length;j++)
				{
					if(kycHierName.split(",")[i].equals(kycHierarchy.split(",")[j]))
					{
						kycHierarchy = kycHierarchy.replaceAll(kycHierName.split(",")[i], "");
					}
				}
			}
			
			if(kycHierarchy.startsWith(","))
			{
				kycHierarchy = kycHierarchy.substring(1, kycHierarchy.length());
			}
			
			if(kycHierarchy.contains(",,"))
			{
				kycHierarchy = kycHierarchy.replaceAll(",,", "");
			}
			if(kycHierarchy.endsWith(","))
			{
				kycHierarchy = kycHierarchy.substring(0, kycHierarchy.length()-1);
			}
			//newkycHierarchy = newkycHierarchy.substring(0, newkycHierarchy.length()-1);
			
			hierModel.setSubdoc_type(kycHierarchy);		
			hierModel.setDoc_hierarchy(kycHierarchy);
			
            String status = apService.saveKycHierarchy(hierModel);
			
			String currentKycHierarchy = apService.getkycHierarchy();	
			
			request.setAttribute("kycHierarchy",currentKycHierarchy);
						
			return "redirect:/createKycHierarchy.html";
		}
		
		
		@RequestMapping(value="/createFinancialHierarchy",method = RequestMethod.GET)
		public ModelAndView createFinancialHierarchy(HttpServletRequest request)
		{		
			String currentHierarchy = apService.getFinancialHierarchy();
			
			request.setAttribute("currentHierarchy", currentHierarchy);
			
			return new ModelAndView("createFinancialHierarchy");
		}
		
		@RequestMapping(value="/saveFinancialHierarchy",method = RequestMethod.POST)
		public ModelAndView saveFinancialHierarchy(HttpServletRequest request)
		{			 
			String financialType = request.getParameter("docType");
			String docMode = request.getParameter("docMode");
			String finHierarchy = financialType+":"+docMode;
			DocumentHierarchyModel hierModel = new DocumentHierarchyModel();			
			 
			 hierModel.setDoc_hir_name("FINANCIAL_DOC");
			 hierModel.setDoc_type("FINANCIAL");
			 hierModel.setCr_date(new Date());						
			 			 					 			 	     			 			 
			String currentHierarchy = apService.getFinancialHierarchy();
		    String currentSubType = apService.getfinancialSubType();
			
		    if(currentHierarchy.equals("No Data"))
			{
				hierModel.setDoc_hierarchy(finHierarchy);
				hierModel.setSubdoc_type(financialType);
			}
			else
			{
				int hierId = apService.getHierId("FINANCIAL");
				hierModel.setDoc_hir_id(hierId);
				hierModel.setSubdoc_type(currentSubType+","+financialType);
				hierModel.setDoc_hierarchy(currentHierarchy+","+finHierarchy);	
			}
			
		    String status = apService.saveKycHierarchy(hierModel);
			 
		    String latestHierarchy = apService.getFinancialHierarchy();
		    
		    request.setAttribute("currentHierarchy", latestHierarchy);
		    
			return new ModelAndView("createFinancialHierarchy");
		}
					
		@RequestMapping(value="/delteFinancialHierarchy",method = RequestMethod.POST)
		public ModelAndView delteFinancialHierarchy(HttpServletRequest request)
		{
            
			String deleteData = request.getParameter("docType");
			String updatedHier = "";	
			String updatedSubHier = "";	
			
			String currentHierarchy = apService.getFinancialHierarchy();	
			String currentHierarchyArray[] = currentHierarchy.split(",");
			
			for(int i=0;i<currentHierarchyArray.length;i++)
			{
				if(!deleteData.split(":")[0].equals(currentHierarchyArray[i].split(":")[0]))
				{
				  	updatedHier += currentHierarchyArray[i] + ",";
				  	updatedSubHier += currentHierarchyArray[i].split(":")[0] + ",";
				}	
				
			}
											 
		  updatedHier = updatedHier.substring(0, updatedHier.length()-1);
		  updatedSubHier = updatedSubHier.substring(0, updatedSubHier.length()-1);
		  
		  DocumentHierarchyModel hierModel = new DocumentHierarchyModel();			
			 
			hierModel.setDoc_hir_name("FINANCIAL_DOC");
			hierModel.setDoc_type("FINANCIAL");
			hierModel.setCr_date(new Date());													 						
			int hierId = apService.getHierId("FINANCIAL");
			hierModel.setDoc_hir_id(hierId);			
			hierModel.setDoc_hierarchy(updatedHier); 
			hierModel.setSubdoc_type(updatedSubHier); 
						
			String status = apService.saveKycHierarchy(hierModel); 
									
			String latestHierarchy = apService.getFinancialHierarchy();		    
		    request.setAttribute("currentHierarchy", latestHierarchy); 
		    
			return new ModelAndView("createFinancialHierarchy");
		} 
		
		
		@RequestMapping(value="/updateFinancialHierarchy",method = RequestMethod.POST)
		public ModelAndView updateFinancialHierarchy(HttpServletRequest request)
		{
			String updateData = request.getParameter("updateData");
			String updatedHier = "";			
			
			String currentHierarchy = apService.getFinancialHierarchy();	
			String currentHierarchyArray[] = currentHierarchy.split(",");
			
			for(int i=0;i<currentHierarchyArray.length;i++)
			{
				if(!updateData.split(":")[0].equals(currentHierarchyArray[i].split(":")[0]))
				{
				  	updatedHier += currentHierarchyArray[i] + ",";
				}	
				else
				{
				  	updatedHier += updateData + ",";
				}
			}
									
		  String currentSubType = apService.getfinancialSubType();
		  updatedHier = updatedHier.substring(0, updatedHier.length()-1);
		  DocumentHierarchyModel hierModel = new DocumentHierarchyModel();			
			 
			hierModel.setDoc_hir_name("FINANCIAL_DOC");
			hierModel.setDoc_type("FINANCIAL");
			hierModel.setCr_date(new Date());													 						
			int hierId = apService.getHierId("FINANCIAL");
			hierModel.setDoc_hir_id(hierId);			
			hierModel.setDoc_hierarchy(updatedHier);
			hierModel.setSubdoc_type(currentSubType);
						
			String status = apService.saveKycHierarchy(hierModel);
									
			String latestHierarchy = apService.getFinancialHierarchy();		    
		    request.setAttribute("currentHierarchy", latestHierarchy);
		    
			return new ModelAndView("createFinancialHierarchy");
		}
		
					
		@RequestMapping(value="/createAcademicHierarchy",method = RequestMethod.GET)
		public ModelAndView createAcademicHierarchy(HttpServletRequest request)
		{
            String docType = apService.getAcademicDocType();
            
            request.setAttribute("docType", docType);
                       
			return new ModelAndView("createAcademicHierarchy");
		}
		
		@RequestMapping(value="/academic/newHierachy",method = RequestMethod.POST)
		public ModelAndView academicNewHierachy(HttpServletRequest request)
		{
			String hierarchyName = request.getParameter("hierarchyName");
			
			DocumentHierarchyModel hierModel = new DocumentHierarchyModel();			
			 
			hierModel.setDoc_hir_name("ACADEMIC_"+hierarchyName+"_DOC");
			hierModel.setDoc_type(hierarchyName);
			hierModel.setCr_date(new Date());									 
			hierModel.setSubdoc_type(hierarchyName);	
			
			String status = apService.saveKycHierarchy(hierModel);
			
            String hierarchyType = apService.getAcademicDocType();                      
            request.setAttribute("docType", hierarchyType);
                       
			return new ModelAndView("createAcademicHierarchy");
		}
		
		@RequestMapping(value="/gethierarchy/components",method = RequestMethod.POST)
		public @ResponseBody String getHierarchyComponents(HttpServletRequest request)
		{
			String hierarchyType = request.getParameter("hierarchyType");
			
			String searchResult = apService.getHierarchyComponents(hierarchyType);
						
			return searchResult;
		}
		
		
		@RequestMapping(value="/saveAcademicHierarchy",method = RequestMethod.POST)
		public @ResponseBody String saveAcademicHierarchy(HttpServletRequest request)
		{
			String hierarchyType = request.getParameter("heirarchyType");
			String component = request.getParameter("component");
			         
			DocumentHierarchyModel hierModel = new DocumentHierarchyModel();			
			 
			int hierId = apService.getHierId(hierarchyType);
			 hierModel.setDoc_hir_id(hierId);
			 hierModel.setDoc_hir_name("ACADEMIC_"+hierarchyType+"_DOC");
			 hierModel.setDoc_type(hierarchyType);
			 hierModel.setCr_date(new Date());									 
			 hierModel.setSubdoc_type(hierarchyType);					 
			 	     			 			 
			String currentHierarchy = apService.getHierarchyComponents(hierarchyType);
		
			if(currentHierarchy.equals("No Data"))
			{
				hierModel.setDoc_hierarchy(component);	
			}
			else
			{					
				System.out.println("Current Component is "+component);
				hierModel.setDoc_hierarchy(currentHierarchy+","+component);	
			}
			
		    String status = apService.saveKycHierarchy(hierModel);
			
		    String latestHierarchy = apService.getHierarchyComponents(hierarchyType);
			
			return latestHierarchy;
		}
		
		@RequestMapping(value="/grabAcademicHierByDocType",method = RequestMethod.POST)
		public @ResponseBody String grabAcademicHierByDocType(HttpServletRequest request)
		{
			String docType = request.getParameter("docType");
			
			String docHierarchy = apService.getAcademicHierarchy(docType);
			
			System.out.println("so the result="+docHierarchy);
			
			return docHierarchy;
		}
		
		@RequestMapping(value="/removeAcademicLabel",method = RequestMethod.POST)
		public @ResponseBody String removeAcademicLabel(HttpServletRequest request)
		{
          String component = request.getParameter("component"); 
          String doc = request.getParameter("docType");
          
          String currentHier = apService.getAcademicHierarchy(doc);                 
          String check[] = currentHier.split(",");
          
          String updatedHierarchy = "";
          
           for(int j=0;j<check.length;j++)
           {
	           String componentMatching[] = check[j].split(":");
        	   if(!componentMatching[0].equals(component) )
        	   {
        		   updatedHierarchy += check[j] + ",";
        	   }        	   
           }
            
           if(updatedHierarchy.length()>0)
           {
        	   updatedHierarchy = updatedHierarchy.substring(0, updatedHierarchy.length()-1);
           }          
           
		     DocumentHierarchyModel hierModel = new DocumentHierarchyModel();			
			 
			 hierModel.setDoc_hir_name("ACADEMIC_"+doc+"_DOC");
			 hierModel.setDoc_type(doc);
			 hierModel.setCr_date(new Date());									 
			 hierModel.setSubdoc_type(doc);					 			 
			 int hierId = apService.getHierId(doc);
			 
			 hierModel.setDoc_hir_id(hierId);			 
			 
			 hierModel.setDoc_hierarchy(updatedHierarchy);	
			 
			 String status = apService.saveKycHierarchy(hierModel);				
			 String currentHierarchy = 	apService.getAcademicHierarchy(doc);  
			 
			return currentHierarchy;
		}
		
		
		
		@RequestMapping(value="/deleteAcademicHierarchy",method = RequestMethod.POST)
		public @ResponseBody String deleteAcademicHierarchy(HttpServletRequest request)
		{
            String hierarchyName = request.getParameter("hierarchyName"); 
            String currentHier = apService.deleteAcademicHierarchy(hierarchyName);  
            
			return currentHier;
		}
		
	
		@RequestMapping(value="/updateAcademicComponent",method = RequestMethod.POST)
		public @ResponseBody String updateAcademicComponent(HttpServletRequest request)
		{						
          String component = request.getParameter("componentName"); 
          String doc = request.getParameter("docType");
          String coponenetProperty = request.getParameter("componentProperty");
          
          String currentHier = apService.getAcademicHierarchy(doc);                 
          String check[] = currentHier.split(",");
          
          String updatedHierarchy = "";
          
           for(int j=0;j<check.length;j++)
           {
	           String componentMatching[] = check[j].split(":");
        	   if(componentMatching[0].equals(component) )
        	   {
        		   updatedHierarchy += coponenetProperty + ",";
        	   }
        	   else
        	   {
        		   updatedHierarchy += check[j] + ",";
        	   }
           }
                     
          updatedHierarchy = updatedHierarchy.substring(0, updatedHierarchy.length()-1);
           
           
		  DocumentHierarchyModel hierModel = new DocumentHierarchyModel();			
			 
			 hierModel.setDoc_hir_name("ACADEMIC_"+doc+"_DOC");
			 hierModel.setDoc_type(doc);
			 hierModel.setCr_date(new Date());									 
			 hierModel.setSubdoc_type(doc);					 			 
			 int hierId = apService.getHierId(doc);
			 
			 hierModel.setDoc_hir_id(hierId);			 
			 
			 hierModel.setDoc_hierarchy(updatedHierarchy);	
			 
			 String status = apService.saveKycHierarchy(hierModel);				
			 String currentHierarchy = 	apService.getAcademicHierarchy(doc);  
			 
			return currentHierarchy;
		}
		
		
		
		
	    @RequestMapping(value="/createDomain",method=RequestMethod.GET)
		public ModelAndView getCreateDomain(HttpServletRequest request,HttpSession session)
		{	
	    	
	    	if(((String)session.getAttribute("adminId"))!=null)
	        {
	    		request.setAttribute("domainName", apService.getDomainName());		    	
				return new ModelAndView("admin_DomainHierarchy");
	        }
	        else
	        {
	        	request.setAttribute("status","failure");
				return new ModelAndView("adminLoginPage");		
	        }	
	    		    	
		}
	    	
	    @RequestMapping(value="/deleteDomain",method=RequestMethod.POST)
		public ModelAndView getDeleteDomain(HttpServletRequest request)
		{	
	    	request.setAttribute("domainName", apService.getDomainName());
			return new ModelAndView("deleteDomain");
		}
	    
		@RequestMapping(value="/createDomainName",method = RequestMethod.POST)
		public ModelAndView createDomainName(HttpServletRequest request)
		{
			String domainName = request.getParameter("domainName");				
			DomainLevelModel domainModel = new DomainLevelModel();
			domainModel.setDomain_name(domainName);
			apService.saveDomain(domainModel);
			
			request.setAttribute("domainName", apService.getDomainName());
			
			return new ModelAndView("admin_myUtilizer");
		}
		
		@RequestMapping(value="/getDomainValue",method = RequestMethod.POST)
		public @ResponseBody String getchildName(HttpServletRequest request)
		{			
			String domainType = request.getParameter("domainType");
			String domainValueasPerType = apService.getDomainValue(domainType);
			
			
			return domainValueasPerType;			
		}
		
		@RequestMapping(value="/getSubDomainValue",method = RequestMethod.POST)
		public @ResponseBody String getSubDomainValue(HttpServletRequest request)
		{			
			String domainType = request.getParameter("domainType");
			String domainValueasPerType = apService.getSubDomainValue(domainType);
						
			return domainValueasPerType;			
		}
				
		@RequestMapping(value="/saveDomainHierarchy",method = RequestMethod.POST)
		public @ResponseBody String  saveChildName(HttpServletRequest request)
		{	
			String domainLavel = request.getParameter("domainLavel");
			String domainType = request.getParameter("domainType");
			String enterValue = request.getParameter("enterValue");						
			
			String domainStatus = apService.saveDomainHierarchy(domainLavel,domainType,enterValue);			
			request.setAttribute("domainName", apService.getDomainName());
			
			//String entireDomainTree = apService.getEntireHierarchy(domainLavel,enterValue);
			
			return "";
			
		}
		
		@RequestMapping(value="/displayDomainHierarchy",method = RequestMethod.POST)
		public @ResponseBody String displayDomainHierarchy(HttpServletRequest request)
		{
			String entireDomainTree = "";
			
			String domainLevel = request.getParameter("domainLavel");
			String domainValue = request.getParameter("domainType");
			
			entireDomainTree = apService.getEntireHierarchy(domainLevel,domainValue);
			
			
			return entireDomainTree;
		}
		
		@RequestMapping(value="/deleteDomainHierarchy",method = RequestMethod.POST)
		public @ResponseBody String deleteDomainHierarchy(HttpServletRequest request)
		{
			String domainStatus = "";
			
			String domainLevel = request.getParameter("domainLavel");
			String domainValue = request.getParameter("domainType");
			
			domainStatus = apService.getDomainStatus(domainLevel,domainValue);
			
			if(domainStatus.equals("NoChild"))
			{
				domainStatus = apService.getDeleteHierarchy(domainLevel,domainValue);
				return "success";
			}
			else
			{
				return domainStatus;
			}
			
		}
		
		
		
		//   Start of Controller function for admin Notification
		
		@RequestMapping(value="/adminNotification",method = RequestMethod.GET)
		public ModelAndView indNotify(HttpServletRequest request,HttpSession session)
		{	
			if(((String)session.getAttribute("adminId"))!=null)
	        {
				return new ModelAndView("admin_Notification");
	        }
	        else
	        {
	        	request.setAttribute("status","failure");
				return new ModelAndView("adminLoginPage");		
	        }				
		}
		
		@RequestMapping(value="/utilizerNotify",method = RequestMethod.POST)
		public ModelAndView utiNotify(HttpServletRequest request)
		{			
			return new ModelAndView("admin_uti_notification");
		}
		
		@RequestMapping(value="/systemNotify",method = RequestMethod.POST)
		public ModelAndView sysNotify(HttpServletRequest request)
		{			
			return new ModelAndView("admin_sys_notification");
		}
		
		@RequestMapping(value="/notifyToIndividual",method = RequestMethod.POST)
		public @ResponseBody String notifyToIndividual(HttpServletRequest request,@ModelAttribute("command")KycNotificationBean notifyBean, BindingResult result)
		{
			
			KycNotificationModel notifyModel = new KycNotificationModel();
			
			notifyModel.setNotification_type(notifyBean.getNotification_type());
			notifyModel.setNotification_to(notifyBean.getNotification_to());
			notifyModel.setNotification_subject(notifyBean.getNotification_subject());
			notifyModel.setNotification_message(notifyBean.getNotification_message());
			notifyModel.setNotification_date(new Date());
			notifyModel.setNotification_status("active");
			
			String notifyStatus = apService.saveNotifyDetails(notifyModel);
			
			return notifyStatus;
		}
		
		@RequestMapping(value="/notifyToUtilizer",method = RequestMethod.POST)
		public @ResponseBody String notifyToUtilizer(HttpServletRequest request,@ModelAttribute("command")KycNotificationBean notifyBean, BindingResult result)
		{
			
			KycNotificationModel notifyModel = new KycNotificationModel();
			
			notifyModel.setNotification_type(notifyBean.getNotification_type());
			notifyModel.setNotification_to(notifyBean.getNotification_to());
			notifyModel.setNotification_subject(notifyBean.getNotification_subject());
			notifyModel.setNotification_message(notifyBean.getNotification_message());
			notifyModel.setNotification_date(new Date());
			notifyModel.setNotification_status("active");
			
			String notifyStatus = apService.saveNotifyDetails(notifyModel);
			
			return notifyStatus;
		}
		
		@RequestMapping(value="/notifyToAll",method = RequestMethod.POST)
		public @ResponseBody String notifyToAll(HttpServletRequest request,@ModelAttribute("command")KycNotificationBean notifyBean, BindingResult result)
		{
			
			KycNotificationModel notifyModel = new KycNotificationModel();
			
			notifyModel.setNotification_type(notifyBean.getNotification_type());
			notifyModel.setNotification_to(notifyBean.getNotification_to());
			notifyModel.setNotification_subject(notifyBean.getNotification_subject());
			notifyModel.setNotification_message(notifyBean.getNotification_message());
			notifyModel.setNotification_date(new Date());
			notifyModel.setNotification_status("active");
			
			String notifyStatus = apService.saveNotifyDetails(notifyModel);
			
			return notifyStatus;
		}
		
		
		//   End of Controller function for admin Notification
		
		
		@RequestMapping(value="/getemailreport",method = RequestMethod.POST)
		public ModelAndView getEmailReport(HttpServletRequest request)
		{	
			List<EmailSentStatusModel> emailReport = apService.getPendingEmail(); 
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("emailDetails", emailReport);
			
			return new ModelAndView("email_sent_report",model);
		}
		
		@RequestMapping(value="/getsmsreport",method = RequestMethod.POST)
		public ModelAndView getSMSreport(HttpServletRequest request)
		{	
			List<SmsSentStatusModel> smsReport = apService.getPendingSMS(); 
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("smsDetails", smsReport);
			
			return new ModelAndView("sms_sent_report",model);
		}
		
		@RequestMapping(value="/getresendemail",method = RequestMethod.POST)
		public ModelAndView getResendeMail(HttpServletRequest request) throws IOException, MessagingException
		{	
			String resetStatus = "";
			int email_uniqueId = Integer.parseInt(request.getParameter("mailUniId"));
			
			List<EmailSentStatusModel> emailDetails = apService.getPendingEmailDetails(email_uniqueId); 
			
			 SendEmailUtil obj = new SendEmailUtil();
  	        
  			resetStatus = obj.getEmailSent(emailDetails.get(0).getEmail_id(),emailDetails.get(0).getSubject(),emailDetails.get(0).getMessage_body());
			request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
			
  			if(resetStatus.equals("success"))
  			{
  				apService.getUpdateEmailDetails(email_uniqueId);
  			}
  						
			List<EmailSentStatusModel> emailReport = apService.getPendingEmail(); 
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("emailDetails", emailReport);
			
			return new ModelAndView("email_sent_report",model);
		}
		
		@SuppressWarnings("static-access")
		@RequestMapping(value="/getresendesms",method = RequestMethod.POST)
		public ModelAndView getResendeSMS(HttpServletRequest request) throws IOException
		{	
			
			int sms_uniqueId = Integer.parseInt(request.getParameter("smsId"));
			
			List<SmsSentStatusModel> smsDetails = apService.getPendingSMSDetails(sms_uniqueId); 
					        
	        String smsStatus = obj.SMSSender(userName,password,smsDetails.get(0).getMessage_body(),senderId,smsDetails.get(0).getMobile_no(),"0");   
	        System.out.println(smsStatus);
	       
	        if(smsStatus.equals("success"))
	        {
	        	apService.getUpdateSmSDetails(sms_uniqueId);
	        }
	        else
	        {
	        	
	        }
			
			List<SmsSentStatusModel> smsReport = apService.getPendingSMS(); 
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("smsDetails", smsReport);
			
			return new ModelAndView("sms_sent_report",model);
		}
		
		
		
		/* Admin Utilizer Module in a new Model */
		
				
		@RequestMapping(value="/admincreateadmin",method = RequestMethod.GET)
		public String admincreateadmin(HttpServletRequest request) throws IllegalWriteException
		{	
			System.out.println("It hits the admin");
			
			String pattern_name = apService.getPatternNames();
			request.setAttribute("patt_name",pattern_name);
	    	
			String domain_Type = apService.getDomainName();
			request.setAttribute("domainType", domain_Type);
			
			String plan_name = apService.getPlan_Name();		
			request.setAttribute("plan_name", plan_name);
			
			return "admin_create_utilizer";
		}
		
		
		@RequestMapping(value="/getdtailsviakycId",method = RequestMethod.POST)
		public @ResponseBody String getdtailsviakycId(HttpServletRequest request) throws IllegalWriteException
		{	
			System.out.println("It hits the admin");
			
			String kycId = request.getParameter("kycId");
			
			String regDetails = apService.getdtailsviakycId(kycId);
			
			return regDetails;
		}
		
		@RequestMapping(value="/orgAdertisement",method = RequestMethod.POST)
		public  String orgAdertisement(HttpServletRequest request) throws IllegalWriteException
		{	
						
			return "adminAddvertise";
		}
		
		@RequestMapping(value="/setIndAdertisement",method = RequestMethod.POST)
		public String setIndAdertisement(HttpServletRequest request) throws IllegalWriteException
		{				
			String addInfo = request.getParameter("addInfo");		
			String status = "";			
			
			ClenseeAdvertise adObject = new ClenseeAdvertise();
			status = adObject.advertiseConfig(addInfo);
			
			return status;
		}
		
		
		
		@RequestMapping(value="/setUtiAdertisement",method = RequestMethod.POST)
		public String setUtiAdertisement(HttpServletRequest request) throws IllegalWriteException
		{	
			String addInfo = request.getParameter("addInfo");
			
			String status = "";
			
			try {
				
				Properties properties = new Properties();				
				properties.setProperty("utilizerAll", "Antarctica");
				
				File file = new File("src/resources/kycaddvertisment.properties");
				FileOutputStream fileOut = new FileOutputStream(file);
				properties.store(fileOut, "Favorite Things");
				fileOut.close();
				status = "success";
			}
			catch (FileNotFoundException e)
			{
				status = "failure";
				e.printStackTrace();
			} 
			catch (IOException e)
			{
				status = "failure";
				e.printStackTrace();
			}
			
			
			return status;
		}
		
		
				
		@RequestMapping(value="/admin_settings",method = RequestMethod.GET)
		public ModelAndView getAdminSettings(HttpServletRequest request,HttpSession session) throws ParseException
		{	
			if(((String)session.getAttribute("adminId"))!=null)
	        {
				//RecordScedulerTime recSchTime = new RecordScedulerTime();				
				
				//System.out.println("Start time="+recSchTime.getSchedulerStartTime());
				//System.out.println("Start time="+recSchTime.getSchedulerEndTime());
				
				return new ModelAndView("admin_settings");
	        }
	        else
	        {
	        	request.setAttribute("status","failure");
				return new ModelAndView("adminLoginPage");		
	        }				
		}
		
		
		
		@RequestMapping(value="/managefileScheduler",method = RequestMethod.POST)
		public ModelAndView managefileScheduler(HttpServletRequest request,HttpSession session)
		{				
			String yearList = apService.getClenseeYearList();

			request.setAttribute("yearList", yearList);
			
			return new ModelAndView("adminManageScheduler");	       			
		}
		
		
		@RequestMapping(value="/getProfileDetailsForEncrypt",method = RequestMethod.POST)
		public ModelAndView getProfileDetailsForEncrypt(HttpServletRequest request,HttpSession session)
		{				
			String year = request.getParameter("registerYear");
			String searchType = request.getParameter("searchType");
			
			List<RegHistoryModel> indDetails = new ArrayList<RegHistoryModel>();
			int individualId = 0;
			String ProfilePhoto = "";
			
			if(searchType.equals("Indid"))
			{
				individualId = Integer.parseInt(request.getParameter("searchValue"));			
				ProfilePhoto = profileinfoService.getLatestProfilePitureName(individualId);
				indDetails = profileinfoService.getIndividualDetailsviaIndId(individualId);				
			}
			else if(searchType.equals("Kycid"))
			{				
				individualId = profileinfoService.findInd_id(request.getParameter("searchValue"));			
				ProfilePhoto = profileinfoService.getLatestProfilePitureName(individualId);
				indDetails = profileinfoService.getIndividualDetailsviaIndId(individualId);				
			}
			else 
			{
				individualId = profileinfoService.getIndIdViaEmailId(request.getParameter("searchValue"));			
				ProfilePhoto = profileinfoService.getLatestProfilePitureName(individualId);
				indDetails = profileinfoService.getIndividualDetailsviaIndId(individualId);				
			}
			
			
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("indDetails", indDetails);
		    
			request.setAttribute("yearValue", year);
			request.setAttribute("individualId", individualId);
			request.setAttribute("ProfilePhoto", ProfilePhoto);
			
			if(indDetails.size()>0)
			{
				request.setAttribute("individualExistence", "Yes");
				request.setAttribute("indFullName", indDetails.get(0).getFirst_name()+" "+indDetails.get(0).getMiddle_name()+" "+indDetails.get(0).getLast_name());
				request.setAttribute("Gender", indDetails.get(0).getGender());
				request.setAttribute("emailId", indDetails.get(0).getEmail_id());
				request.setAttribute("mobileno", indDetails.get(0).getMobile_no()+"");
			}
			else
			{
				request.setAttribute("individualExistence", "No");
			}
			
			
			return new ModelAndView("individualDetailsForAdmin");	       			
		}
		
		@SuppressWarnings("static-access")
		@RequestMapping(value="/encryptThisUserFiles",method = RequestMethod.POST)
		@ResponseBody public String startEncryptingFromAdmin(HttpServletRequest request,HttpSession session)
		{				
			String year = request.getParameter("registerYear");		
			String directoryValue = request.getParameter("directoryValue");			
			String searchType = request.getParameter("searchType");
			
			int individualId = 0;
			
			if(searchType.equals("Indid"))
			{
				individualId = Integer.parseInt(request.getParameter("individualId"));							
			}
			else if(searchType.equals("Kycid"))
			{				
				individualId = profileinfoService.findInd_id(request.getParameter("individualId"));								
			}
			else 
			{
				individualId = Integer.parseInt(profileinfoService.getIndividualIdVaiEmailId(request.getParameter("individualId")));								
			}
			
			FileEncryptionDecryptionUtil fileEncUtil = new FileEncryptionDecryptionUtil();
			
			fileEncUtil.getOtherFileEncryptionFromAdmin(year,individualId,directoryValue);
			System.out.println("It hits");
			return "Done";
		}
		
		
		@SuppressWarnings("static-access")
		@RequestMapping(value="/EncryptAllDocuments",method = RequestMethod.POST)
		@ResponseBody public String EncryptAllDocuments(HttpServletRequest request,HttpSession session)
		{							
			FileEncryptionDecryptionUtil fileEncUtil = new FileEncryptionDecryptionUtil();
			fileEncUtil.EncryptAllDocumentsForRequestOfAdmin();
			return "Done";
		}
		
				
		@RequestMapping(value="/contactUsDetailsSettings",method = RequestMethod.POST)
		public ModelAndView contactUsDetailsSettings(HttpServletRequest request,HttpSession session)
		{		
			Date todaysDate = new Date();					
			String todaysDateFormat = new SimpleDateFormat("yyyy-MM-dd").format(todaysDate);
			List<ContactUsModel> contactUsDtails = apService.getAllContactUsDetails(todaysDateFormat);
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("contactUsDtails", contactUsDtails);
			  
			return new ModelAndView("adminContactUsSettings",model);	       			
		}
		
		
		@RequestMapping(value="/getcontactMessageByConId",method = RequestMethod.POST)
		@ResponseBody public String getcontactMessageByConId(HttpServletRequest request,HttpSession session)
		{			
			String contactMessage = "";
			
			int contactId = Integer.parseInt(request.getParameter("contactId"));
			contactMessage = apService.getContactMssageByConId(contactId);
			
			return contactMessage;	       			
		}
		
		
		@RequestMapping(value="/searchContactDetails",method = RequestMethod.POST)
		public ModelAndView searchContactDetails(HttpServletRequest request,HttpSession session)
		{		
			String searchType = request.getParameter("searchType");			
			String searchValue = request.getParameter("searchValue");
			
			List<ContactUsModel> searchContactUsDtails = apService.getSearchContactUsDetails(searchType,searchValue);
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("contactUsDtails", searchContactUsDtails);
			  
			return new ModelAndView("adminContactUsSettings",model);	       			
		}
		/* End of Admin Utilizer Module in a new Model */
}

