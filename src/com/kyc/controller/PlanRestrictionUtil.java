package com.kyc.controller;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.model.ResetPasswordModel;
import com.kyc.service.PlanService;







import com.kyc.service.ProfileinfoService;
import com.kyc.service.UtiliserService;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
public  class PlanRestrictionUtil {

	@Autowired
	private ProfileinfoService profileinfoService;
	
	@Autowired
	private PlanService PlanService;
	
	public static String individualCategory = "Individual";
	public static String utilizerCategory = "Utilizer";
	public static String corporateCategory = "Corporate";
		
	
	static final Logger LOGGER = Logger.getLogger(PlanRestrictionUtil.class);
    
	
	@RequestMapping(value="/validateProfileViewingBeforeLogin",method=RequestMethod.GET)
	public String validateProfileViewingBeforeLogin(HttpServletRequest request,HttpSession session)
	{
		int viewerIndId = 0;
				
		 viewerIndId += (int)session.getAttribute("beforeSearchedIndId");	
										
				session.setAttribute("asdgth", viewerIndId);
				
				int individualId = Integer.parseInt((String)session.getAttribute("Ind_id"));
				
				String restrictStatus = "";
				String planStatus = "";
				String individual_kyc_id = PlanService.getKycId(individualId);
				String planName = PlanService.getCurrentPlanName(individualId);
				String featuresIdList =  PlanService.getPlanFeatureIdDetails(planName, individualCategory);
				String featuresListArray[] = featuresIdList.split(",");
					
				for(int i=0;i<featuresListArray.length;i++)
				{
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("Profile Viewing"))
					{
						restrictStatus += PlanService.getFeatureValue(featureId);
					}
				}		
											
				if(!planName.equalsIgnoreCase("Basic"))
				{			
					 String clean = restrictStatus.replaceAll("\\D+","");
					 int noOfFullProfileAsPerPlan = Integer.parseInt(clean);
					 session.setAttribute("noOfFullProfileAsPerPlan", noOfFullProfileAsPerPlan);
					 int noOfFullProfileAsOfNow = PlanService.noOfFullProfileAsOfNow(individual_kyc_id);
					 session.setAttribute("noOfFullProfileAsOfNow",noOfFullProfileAsOfNow);
					 
					 planStatus += "Full Profile";	
					
					 if(noOfFullProfileAsPerPlan>noOfFullProfileAsOfNow)
					 {
						  session.setAttribute("planName", "Other");
						  return "redirect:/viewSearchedProfile.html";
					 }
					 else 
					 {	
						  session.setAttribute("planName", "Basic");
					      return "redirect:/viewSearchedProfile.html";						    
					 }					 
				}
				else
				{
					session.setAttribute("planName", "Basic");
					return "redirect:/viewSearchedProfile.html";
				}																				
	     }
	
	

	@RequestMapping(value="/validateProfileViewing",method=RequestMethod.GET)
	public String validateProfileViewing(HttpServletRequest request,HttpSession session)
	{
		int viewerIndId = 0;
		
		
		if (session.getAttribute("Ind_id") == null)
		{				
			return "redirect:individual.html";	
		}
		else
		{
			String searchedIndId = request.getParameter("asdgth");
			
			 viewerIndId += Integer.parseInt(request.getParameter("asdgth"));	
											
					session.setAttribute("asdgth", viewerIndId);
					
					int individualId = Integer.parseInt((String)session.getAttribute("Ind_id"));
					String restrictStatus = "";
					String planStatus = "";
					String individual_kyc_id = PlanService.getKycId(individualId);
					String planName = PlanService.getCurrentPlanName(individualId);
					String featuresIdList =  PlanService.getPlanFeatureIdDetails(planName, individualCategory);
					String featuresListArray[] = featuresIdList.split(",");
						
					for(int i=0;i<featuresListArray.length;i++)
					{
						int featureId = Integer.parseInt(featuresListArray[i]);
						String featureNames = PlanService.getFeatureNameById(featureId);
						if(featureNames.equalsIgnoreCase("Profile Viewing"))
						{
							restrictStatus += PlanService.getFeatureValue(featureId);
						}
					}		
												
					if(!planName.equalsIgnoreCase("Basic"))
					{			
						 String clean = restrictStatus.replaceAll("\\D+","");
						 int noOfFullProfileAsPerPlan = Integer.parseInt(clean);
						 session.setAttribute("noOfFullProfileAsPerPlan", noOfFullProfileAsPerPlan);
						 int noOfFullProfileAsOfNow = PlanService.noOfFullProfileAsOfNow(individual_kyc_id);
						 session.setAttribute("noOfFullProfileAsOfNow",noOfFullProfileAsOfNow);
						 
						 planStatus += "Full Profile";	
						 
						 if(noOfFullProfileAsPerPlan>noOfFullProfileAsOfNow)
						 {
							 session.setAttribute("planName", "Other");
							 return "redirect:/viewSearchedProfile.html";
						 }
						 else 
						 {	
							  session.setAttribute("planName", "Basic");
							  return "redirect:/viewSearchedProfile.html";
							 
						 }					 
					}
					else
					{
						session.setAttribute("planName", "Basic");
						return "redirect:/viewSearchedProfile.html";
					}
		}
		
																				
	     }
	
	

    @RequestMapping(value = "/migratenowplan", method = RequestMethod.GET)
	public ModelAndView Migrateplan1(HttpSession session,HttpServletRequest request) 
    {
	     
	     String featureCategory = "Individual";
		 String featurecategory=PlanService.getfeaturecategory(featureCategory);
		 
		 String basicFeatures_Id = PlanService.getPlanFeatureId(featureCategory,"Basic");
		 String silverFeatures_Id = PlanService.getPlanFeatureId(featureCategory,"Silver");
		 String goldFeatures_Id = PlanService.getPlanFeatureId(featureCategory,"Gold");
		 String platinumFeatures_Id = PlanService.getPlanFeatureId(featureCategory,"Platinum");
		 	
		 String basicDetails = PlanService.getPlanDetails(basicFeatures_Id,featurecategory);
		 String silverDetails = PlanService.getPlanDetails(silverFeatures_Id,featurecategory);
		 String goldDetails = PlanService.getPlanDetails(goldFeatures_Id,featurecategory);
		 String platinumDetails = PlanService.getPlanDetails(platinumFeatures_Id,featurecategory);
		 				 			 					 
		 request.setAttribute("featurecategory", featurecategory);
		 request.setAttribute("basicDetails", basicDetails);
		 request.setAttribute("silverDetails", silverDetails);
		 request.setAttribute("goldDetails", goldDetails);			 
		 request.setAttribute("platinumDetails", platinumDetails);
		 
		 float basicprice = PlanService.getPlanPrice(featureCategory,"Basic");
		 float silverprice =PlanService.getPlanPrice(featureCategory,"Silver");
		 float goldprice = PlanService.getPlanPrice(featureCategory,"Gold");
		 float platinumprice = PlanService.getPlanPrice(featureCategory,"Platinum");
		 
		 request.setAttribute("basicprice", basicprice);
		 request.setAttribute("silverprice", silverprice);
		 request.setAttribute("goldprice", goldprice);			 
		 request.setAttribute("platinumprice", platinumprice);
		 
		
	     request.setAttribute("planStatus","As per Your this month no.of profile viewing has completed,to view Profile migrate your Plan or wait for next month !");
			 
	     return new ModelAndView("individual_migratePlan");
	}
 
    

    @RequestMapping(value="/validateFileStorage", method=RequestMethod.POST)
	public @ResponseBody String validateFileStorage(HttpServletRequest request,HttpSession session)        
    {
    	        	
        /* code for checking  File Storage */
    	float givenfileSize = (float)0.0;
    	
    		givenfileSize =  Float.parseFloat(request.getParameter("givenfileSize"));
        	givenfileSize = (float)(givenfileSize/1024.0);
    	
    	          	
		int ind_id=Integer.parseInt((String)session.getAttribute("Ind_id"));			
		float totalFileStorageAsOfNow = PlanService.getTotalFileStorage(ind_id);
		totalFileStorageAsOfNow = (float) (totalFileStorageAsOfNow/1024.0);
		
		totalFileStorageAsOfNow = totalFileStorageAsOfNow + givenfileSize;
		
		float fileStorageAsPerPlan=(float)0.0;		
		String planName = PlanService.getCurrentPlanName(ind_id);
		String featuresIdList =  PlanService.getPlanFeatureIdDetails(planName, individualCategory);
		String featuresListArray[] = featuresIdList.split(",");
			
		for(int i=0;i<featuresListArray.length;i++)
		{
			int featureId = Integer.parseInt(featuresListArray[i]);
			String featureNames = PlanService.getFeatureNameById(featureId);
			if(featureNames.equalsIgnoreCase("Files Storage"))
			{
				String restrictStatus = PlanService.getFeatureValue(featureId);
				if(restrictStatus.contains("MB"))
				{
					String clean = restrictStatus.replaceAll("\\D+","");
				    fileStorageAsPerPlan = fileStorageAsPerPlan + Float.parseFloat(clean);				   
				}
				else
				{
					String clean = restrictStatus.replaceAll("\\D+","");
				    fileStorageAsPerPlan = fileStorageAsPerPlan + Float.parseFloat(clean);			
				    fileStorageAsPerPlan = fileStorageAsPerPlan * 1024;
				}
			}
		}		
		
		if(fileStorageAsPerPlan>totalFileStorageAsOfNow)
		{
			return "withinStorage";
		}
		else
		{
			return "beyondStorage";
		}
				
		/* End of Code for FileStorage */
    }
    	
	@RequestMapping(value = "/validategenerateCode", method = RequestMethod.POST)
	public @ResponseBody String validategenerateCode(HttpServletRequest request,HttpSession session) {

		int individualId = Integer.parseInt((String)session.getAttribute("Ind_id"));
		
		String kyc_id = PlanService.getKycId(individualId);
		int noOfGeneratedCodeAsOfNow = PlanService.getCountGeneratedCode(kyc_id);
		int noOfGeneratedCodeAsPerPlan = (int)0;
		
		String planName = PlanService.getCurrentPlanName(individualId);		
		String featuresIdList =  PlanService.getPlanFeatureIdDetails(planName, "Individual");
		String featuresListArray[] = featuresIdList.split(",");
			
		for(int i=0;i<featuresListArray.length;i++)
		{
			int featureId = Integer.parseInt(featuresListArray[i]);
			String featureNames = PlanService.getFeatureNameById(featureId);
			if(featureNames.equalsIgnoreCase("Individual Sharing"))
			{
				String restrictStatus = PlanService.getFeatureValue(featureId);
				String clean = restrictStatus.replaceAll("\\D+","");
				noOfGeneratedCodeAsPerPlan = noOfGeneratedCodeAsPerPlan + Integer.parseInt(clean);
			}
		 }
				
		if(noOfGeneratedCodeAsPerPlan>noOfGeneratedCodeAsOfNow)
		{
			return "withinLimit";
		}
		else
		{
			return "beyoundLimit";
		}
		
	}
		
	@RequestMapping(value = "/validateapplyCode", method = RequestMethod.POST)
	public @ResponseBody String validateapplyCode(HttpServletRequest request,HttpSession session) {

		int individualId = Integer.parseInt((String)session.getAttribute("Ind_id"));
		String kyc_id = PlanService.getKycId(individualId);
		int noOfApplyCodeAsPerPlan = (int)0;
		String planName = PlanService.getCurrentPlanName(individualId);		
		String featuresIdList =  PlanService.getPlanFeatureIdDetails(planName, "Individual");
		String featuresListArray[] = featuresIdList.split(",");
				
		for(int i=0;i<featuresListArray.length;i++)
		{
			int featureId = Integer.parseInt(featuresListArray[i]);
			String featureNames = PlanService.getFeatureNameById(featureId);
			if(featureNames.equalsIgnoreCase("Documents Received"))
			{
				String restrictStatus = PlanService.getFeatureValue(featureId);
				String clean = restrictStatus.replaceAll("\\D+","");
				noOfApplyCodeAsPerPlan = noOfApplyCodeAsPerPlan + Integer.parseInt(clean);
			}
		}
		
		int noOfApplyCodeAsOfNow = PlanService.getCountApplyCode(kyc_id);
		
		return ""+noOfApplyCodeAsPerPlan;
		
	}
	
	
	@RequestMapping(value = "/validateAllowAccess", method = RequestMethod.POST)
	public @ResponseBody String validateAllowAccess(HttpServletRequest request,HttpSession session) {

		int individualId = Integer.parseInt((String)session.getAttribute("Ind_id"));
		String kyc_id = PlanService.getKycId(individualId);
		int noOfAllowAccessAsOfNow = PlanService.getCountAllowAccess(kyc_id);
		int noOfApplyCodeAsPerPlan = (int)0;
		
		String planName = PlanService.getCurrentPlanName(individualId);		
		String featuresIdList =  PlanService.getPlanFeatureIdDetails(planName, "Individual");
		String featuresListArray[] = featuresIdList.split(",");
		
		for(int i=0;i<featuresListArray.length;i++)
		{
			int featureId = Integer.parseInt(featuresListArray[i]);
			String featureNames = PlanService.getFeatureNameById(featureId);
			if(featureNames.equalsIgnoreCase("Utilizer Sharing / Removing"))
			{
				String restrictStatus = PlanService.getFeatureValue(featureId);
				String clean = restrictStatus.replaceAll("\\D+","");
				noOfApplyCodeAsPerPlan = noOfApplyCodeAsPerPlan + Integer.parseInt(clean);
			}
		}
		
		if(noOfApplyCodeAsPerPlan>noOfAllowAccessAsOfNow)
		{
			return "withinLimit";
		}
		else
		{
			return "beyoundLimit";
		}
		
	}
		
	@RequestMapping(value = "/validateRevokeAccess", method = RequestMethod.POST)
	public @ResponseBody String validateRevokeAccess(HttpServletRequest request,HttpSession session) {

		int individualId = Integer.parseInt((String)session.getAttribute("Ind_id"));
		String kyc_id = PlanService.getKycId(individualId);
		int noOfAllowAccessAsOfNow = PlanService.getCountAllowAccess(kyc_id);
		int noOfApplyCodeAsPerPlan = (int)0;
		
		String planName = PlanService.getCurrentPlanName(individualId);		
		String featuresIdList =  PlanService.getPlanFeatureIdDetails(planName, "Individual");
		String featuresListArray[] = featuresIdList.split(",");
		
		for(int i=0;i<featuresListArray.length;i++)
		{
			int featureId = Integer.parseInt(featuresListArray[i]);
			String featureNames = PlanService.getFeatureNameById(featureId);
			if(featureNames.equalsIgnoreCase("Utilizer Sharing / Removing"))
			{
				String restrictStatus = PlanService.getFeatureValue(featureId);				
				String clean = restrictStatus.replaceAll("\\D+","");
				noOfApplyCodeAsPerPlan = noOfApplyCodeAsPerPlan + Integer.parseInt(clean);				
			}
		}
		
		return ""+noOfApplyCodeAsPerPlan;
	}
	
	
	
	// Start code for Utilizer Plan restriction
	
	@RequestMapping(value = "/validateprofileviewingForutilizerBeforeLogin", method = RequestMethod.GET)
	public String validateprofileviewingForutilizerBeforeLogin(HttpServletRequest request,HttpSession session)
	{			
		
		
		
		
				int viewerIndId = 0;
				
				viewerIndId += (int)session.getAttribute("beforeSearchedIndId");	
				session.setAttribute("asdgthfmUti", viewerIndId);
						
			    int uti_Indid = (int) session.getAttribute("uti_Ind_id");	   		
			    String planName= PlanService.getUtilizerplanname(uti_Indid);			
			    String domainName = PlanService.getUtilizerPlanDomain(uti_Indid);
			    
				String restrictStatus = "";
				String planStatus = "";				
				
				String featuresIdList =  PlanService.getPlanFeatureIdDetailsForUtilizer(planName,domainName,utilizerCategory);
				String featuresListArray[] = featuresIdList.split(",");
					
				for(int i=0;i<featuresListArray.length;i++)
				{					
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("Profile Viewing"))
					{
						restrictStatus += PlanService.getFeatureValue(featureId);
					}
				}		
				
				
				if(!planName.equalsIgnoreCase("Basic")) 
				{			
					 String clean = restrictStatus.replaceAll("\\D+","");
					 int noOfFullProfileAsPerPlan = Integer.parseInt(clean);
					 session.setAttribute("noOfFullProfileAsPerPlanForUtilizer", noOfFullProfileAsPerPlan);
					 
					 int noOfFullProfileAsOfNow = PlanService.noOfFullProfileAsOfNowOfUtilizer(uti_Indid);
					 session.setAttribute("noOfFullProfileAsOfNowForUtilizer",noOfFullProfileAsOfNow);
					 
					 planStatus += "Full Profile";	
					 
					 if(noOfFullProfileAsPerPlan>noOfFullProfileAsOfNow)
					 {
						 session.setAttribute("planName", "Other");
						 return "redirect:/ProfileViewingfromUtilizer.html";
					 }
					 else 
					 {		
						 session.setAttribute("planName", "Basic");
					     return "redirect:/ProfileViewingfromUtilizer.html";
					 }					 
				}
				else
				{
					session.setAttribute("planName", "Basic");
					return "redirect:/ProfileViewingfromUtilizer.html";
				}																				
	     }
	
	
	
	@RequestMapping(value = "/validateprofileviewingForutilizer", method = RequestMethod.GET)
	public String validateProfileViewingForUtilizer(HttpServletRequest request,HttpSession session)
	{			
		if (session.getAttribute("uti_Ind_id") == null)
		{
			request.setAttribute("status","Session got expired please login to access !");				
			
			return "redirect:/utiliser.html";	
		}
		else
		{
		        int viewerIndId = Integer.parseInt(request.getParameter("asdgth"));	
				session.setAttribute("asdgthfmUti", viewerIndId);
						
			    int uti_Indid = (int) session.getAttribute("uti_Ind_id");	   		
			    String planName= PlanService.getUtilizerplanname(uti_Indid);			
			    String domainName = PlanService.getUtilizerPlanDomain(uti_Indid);
			    
				String restrictStatus = "";
				String planStatus = "";				
				
				String featuresIdList =  PlanService.getPlanFeatureIdDetailsForUtilizer(planName,domainName,utilizerCategory);
				String featuresListArray[] = featuresIdList.split(",");
					
				for(int i=0;i<featuresListArray.length;i++)
				{					
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("Profile Viewing"))
					{
						restrictStatus += PlanService.getFeatureValue(featureId);
					}
				}		
											
				if(!planName.equalsIgnoreCase("Basic")) 
				{			
					 String clean = restrictStatus.replaceAll("\\D+","");
					 int noOfFullProfileAsPerPlan = Integer.parseInt(clean);
					 session.setAttribute("noOfFullProfileAsPerPlanForUtilizer", noOfFullProfileAsPerPlan);
					 
					 int noOfFullProfileAsOfNow = PlanService.noOfFullProfileAsOfNowOfUtilizer(uti_Indid);
					 session.setAttribute("noOfFullProfileAsOfNowForUtilizer",noOfFullProfileAsOfNow);
					 
					 planStatus += "Full Profile";	
					
					 if(noOfFullProfileAsPerPlan>noOfFullProfileAsOfNow)
					 {
						 session.setAttribute("planName", "Other");
						 return "redirect:/ProfileViewingfromUtilizer.html";
					 }
					 else 
					 {		
						 session.setAttribute("planName", "Basic");
					     return "redirect:/ProfileViewingfromUtilizer.html";
					 }					 
				}
				else
				{
					session.setAttribute("planName", "Basic");
					return "redirect:/ProfileViewingfromUtilizer.html";
				}	
				
		}
	  }
	
	
	@RequestMapping(value = "/validateDocViewingViaAllowAccessPerMonth", method = RequestMethod.POST)
	public @ResponseBody String DocViewingViaAllowAccessPerMonth(HttpServletRequest request,HttpSession session) {

		int noOfAccessDocByUtilizerAsPerPlan = (int)0;		
		
		int uti_Ind_id = (int)session.getAttribute("uti_Ind_id");	    
		String utiUniqueId = PlanService.getUtilizerUniqueId(uti_Ind_id);
		
		int noOfAccessDocByUtilizerAsOfNow = PlanService.getCountAllowAccessByUtilizer(utiUniqueId);
		
	    String planName = PlanService.getUtilizerplanname(uti_Ind_id);
	    String planDomain = PlanService.getUtilizerPlanDomain(uti_Ind_id);
		
	    String featuresIdsAsPerPlan = PlanService.getUtilizerFeatureIdsAsPerPlan(utilizerCategory,planDomain,planName);
	    String featuresListArray[] = featuresIdsAsPerPlan.split(",");
	    
	    for(int i=0;i<featuresListArray.length;i++)
		{
	    	int featureId = Integer.parseInt(featuresListArray[i]);
			String featureNames = PlanService.getFeatureNameById(featureId);
			if(featureNames.equalsIgnoreCase("Doc  Accessory of Employees and customers"))
			{
				String restrictStatus = PlanService.getFeatureValue(featureId);
				if(restrictStatus.contains("Yes"))
				{
					String clean = restrictStatus.replaceAll("\\D+","");
					noOfAccessDocByUtilizerAsPerPlan = noOfAccessDocByUtilizerAsPerPlan + Integer.parseInt(clean);		
				}
				else
				{
					noOfAccessDocByUtilizerAsPerPlan = noOfAccessDocByUtilizerAsPerPlan + 0;
				}
			}
		}
	    
	    if(noOfAccessDocByUtilizerAsPerPlan>=noOfAccessDocByUtilizerAsOfNow)
	    {
	    	return "withinLimit";
	    }
	    else
	    {
	    	return "beyoundLimit";
	    }
	}
	
	
	
	
	/* view Individual Profile and details from utilizer .*/
	
	
	@RequestMapping(value = "/validateProfileViewingForUtilizerFromRecDocuments", method = RequestMethod.GET)
	public String validateProfileViewingForUtilizerFromRecDocuments(HttpServletRequest request,HttpSession session)
	{			   
		if (session.getAttribute("uti_Ind_id") == null)
		{
			request.setAttribute("status","Session got expired please login to access !");				
			
			return "redirect:/utiliser.html";	
		}
		else
		{      
	        	String accessGiverKcId = request.getParameter("accessGiver"); 
	        	int viewerIndId = profileinfoService.findInd_id(accessGiverKcId.replaceAll(" ",""));		
	        	
	        	session.setAttribute("reloadUrl","reloadIndProfileFromUti.html?accessGiver="+accessGiverKcId);
	    		
				session.setAttribute("tempIdasdgthfmUti",viewerIndId);
						
			    int uti_Indid = (int) session.getAttribute("uti_Ind_id");	   		
			    String planName= PlanService.getUtilizerplanname(uti_Indid);			
			    String domainName = PlanService.getUtilizerPlanDomain(uti_Indid);
			    
				String restrictStatus = "";
				String planStatus = "";				
				
				String featuresIdList =  PlanService.getPlanFeatureIdDetailsForUtilizer(planName,domainName,utilizerCategory);
				String featuresListArray[] = featuresIdList.split(",");
					
				for(int i=0;i<featuresListArray.length;i++)
				{					
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("Profile Viewing"))
					{
						restrictStatus += PlanService.getFeatureValue(featureId);
					}
				}		
											
				if(!planName.equalsIgnoreCase("Basic")) 
				{			
					 String clean = restrictStatus.replaceAll("\\D+","");
					 int noOfFullProfileAsPerPlan = Integer.parseInt(clean);
					 session.setAttribute("noOfFullProfileAsPerPlanForUtilizer", noOfFullProfileAsPerPlan);
					 
					 int noOfFullProfileAsOfNow = PlanService.noOfFullProfileAsOfNowOfUtilizer(uti_Indid);
					 session.setAttribute("noOfFullProfileAsOfNowForUtilizer",noOfFullProfileAsOfNow);
					 
					 planStatus += "Full Profile";	
					 
					 if(noOfFullProfileAsPerPlan>noOfFullProfileAsOfNow)
					 {
						 session.setAttribute("planName", "Other");
						 return "redirect:/viewIndAccessDetailsfromUti.html";
					 }
					 else 
					 {		
						 session.setAttribute("planName", "Basic");
					     return "redirect:/viewIndAccessDetailsfromUti.html";
					 }					 
				}
				else
				{
					session.setAttribute("planName", "Basic");
					return "redirect:/viewIndAccessDetailsfromUti.html";
				}																				
	     }
	
	}
	
	
	
	
	@RequestMapping(value = "/reloadIndProfileFromUti", method = RequestMethod.GET)
	public String reloadIndProfileFromUti(HttpServletRequest request,HttpSession session)
	{			   
		if (session.getAttribute("uti_Ind_id") == null)
		{
			request.setAttribute("status","Session got expired please login to access !");				
			
			return "redirect:/utiliser.html";	
		}
		else
		{    
	        	String accessGiverKcId = request.getParameter("accessGiver"); 
	        	int viewerIndId = profileinfoService.findInd_id(accessGiverKcId.replaceAll(" ",""));		
	        	
	        	session.setAttribute("reloadUrl","reloadIndProfileFromUti.html?accessGiver="+accessGiverKcId);
	    		
				session.setAttribute("tempIdasdgthfmUti", viewerIndId);
						
			    int uti_Indid = (int) session.getAttribute("uti_Ind_id");	   		
			    String planName= PlanService.getUtilizerplanname(uti_Indid);			
			    String domainName = PlanService.getUtilizerPlanDomain(uti_Indid);
			    
				String restrictStatus = "";
				String planStatus = "";				
				
				String featuresIdList =  PlanService.getPlanFeatureIdDetailsForUtilizer(planName,domainName,utilizerCategory);
				String featuresListArray[] = featuresIdList.split(",");
					
				for(int i=0;i<featuresListArray.length;i++)
				{					
					int featureId = Integer.parseInt(featuresListArray[i]);
					String featureNames = PlanService.getFeatureNameById(featureId);
					if(featureNames.equalsIgnoreCase("Profile Viewing"))
					{
						restrictStatus += PlanService.getFeatureValue(featureId);
					}
				}		
											
				if(!planName.equalsIgnoreCase("Basic")) 
				{			
					 String clean = restrictStatus.replaceAll("\\D+","");
					 int noOfFullProfileAsPerPlan = Integer.parseInt(clean);
					 session.setAttribute("noOfFullProfileAsPerPlanForUtilizer", noOfFullProfileAsPerPlan);
					 
					 int noOfFullProfileAsOfNow = PlanService.noOfFullProfileAsOfNowOfUtilizer(uti_Indid);
					 session.setAttribute("noOfFullProfileAsOfNowForUtilizer",noOfFullProfileAsOfNow);
					 
					 planStatus += "Full Profile";	
					 
					 if(noOfFullProfileAsPerPlan>noOfFullProfileAsOfNow)
					 {
						 session.setAttribute("planName", "Other");
						 return "redirect:/reloadIndAccessDetailsfromUti.html";
					 }
					 else 
					 {		
						 session.setAttribute("planName", "Basic");
					     return "redirect:/reloadIndAccessDetailsfromUti.html";
					 }					 
				}
				else
				{
					session.setAttribute("planName", "Basic");
					return "redirect:/reloadIndAccessDetailsfromUti.html";
				}																				
	     }
	}
	/*  End of individual profile view from Utilizer */
	
	
	
	
	
	
	
	
	
}
 