package com.kyc.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.log4j.FileAppender;
import org.apache.log4j.SimpleLayout;

import com.kyc.util.CryptoException;
import com.kyc.util.JIRCryptoUtils;
public class JIREncryptionUtils {
	
	
	
			    
	public static void encryptAllDocs(File fl,String mode) 
	{
		
		try{
			
			// Logger File  for current user's IpAddress and Mac Address with Name Date and Time
		    /*org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(JIREncryptionUtils.class);
			
			FileAppender loggerfile;	 
			ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
			String logDirectory=loggerFileDirectory.getString("loggerDirectoryFILE");
			
		    String DATE_FORMAT_NOW = "dd-MM-yyyy";
		    Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
			String stringDate = sdf.format(date);
			stringDate += "-Clensee-File-ENR-DCR";
			   
			loggerfile = new FileAppender(new SimpleLayout(),""+logDirectory+"/"+stringDate+".log",false);
		    log.addAppender(loggerfile);
		    loggerfile.setLayout(new SimpleLayout());
*/		    
			if (fl.isFile()) {
				
				try {
					if ("enc".equals(mode)) 
					{								
						JIRCryptoUtils.encrypt("KYC1KEY100000001", fl, fl);												
					} 
					else if ("dec".equals(mode)) 
					{
						JIRCryptoUtils.decrypt("KYC1KEY100000001", fl, fl);												
					}
				  }
					catch (CryptoException ex) 
					{					
						ex.printStackTrace();
						//log.error(fl.getName()+" This has some problem-- "+ex.getMessage());
					}
			} 
			else if (fl.isDirectory()) 
			{				
				if (fl.isDirectory()) 
				{
					for (File fl2 : fl.listFiles())
					{
						File checkFileDirectory = new File(fl + "\\" + fl2.getName());
						String directory = checkFileDirectory.getAbsolutePath();
						
						if(directory.contains("Profile_Photo"))
						{
							
						}
						else if(directory.contains("ThumbNail_Documents"))
						{
							
						}
						else if(directory.contains("Utiliser_Profile_Photo"))
						{
							
						}
						else
						{
							encryptAllDocs(new File(fl + "\\" + fl2.getName()), mode);
						}
						
						checkFileDirectory = null;
						System.gc();
					}
				}
			}
			else
			{
				
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage()); 		    
		}

		
		
		
	}
}
