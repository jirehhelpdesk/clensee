package com.kyc.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.mail.AuthenticationFailedException;
import javax.mail.IllegalWriteException;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.captcha.Captcha;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.kyc.bean.BasicDetailBean;
import com.kyc.bean.ContactUsBean;
import com.kyc.bean.DocumentDetailsBean;
import com.kyc.bean.EmployeeBean;
import com.kyc.bean.FamilyDetailBean;
import com.kyc.bean.FeaturesDetailsBean;
import com.kyc.bean.IndSignupBean;
import com.kyc.bean.KycNotificationBean;
import com.kyc.bean.MaritalBean;
import com.kyc.bean.Profileupdatebean;
import com.kyc.bean.SocialaccBean;
import com.kyc.bean.SummarydetailBean;
import com.kyc.model.BasicDetail;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.IndSignupModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.Matrial;
import com.kyc.model.ProfilePictureModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.ResetPasswordModel;
import com.kyc.model.Socialacc;
import com.kyc.model.Summarydetail;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.service.DocService;
import com.kyc.service.IndSignupService;
import com.kyc.service.ProfileinfoService;
import com.kyc.util.PasswordEncryptionDecryption;
import com.kyc.util.SendEmailUtil;

@Controller
public class ProfileinfoController {
	
	private static final IndSignupModel familyvalues = null;
	
	@Autowired
	private ProfileinfoService profileinfoService;
	
	@Autowired
	private IndSignupService signupService;

	@Autowired
	private DocService docservice;
	

	String Ind_id="";
	String IND_KYCID = "";
	
	
	static final Logger LOGGER = Logger.getLogger(ProfileinfoController.class);
    	
	// Required file Config for entire Controller 
	
		 ResourceBundle fileResource = ResourceBundle.getBundle("resources/kycFileDirectory");
		 String kycMainDirectory = fileResource.getString("fileDirectory");	
		 String kycSubDirectory = fileResource.getString("fileSubDirectory");	
		 File filePath = new File(kycMainDirectory);
		
		// End of Required file Config for entire Controller 
		 
	
	private List<String> prepareIndSignupModel(List<IndSignupModel> searchvalue) {
	  	//DocumentDetailsBean  bean = new DocumentDetailsBean();
		
	 	List<IndSignupBean> bean = null;
	 		 	
	 	List<String> search = new ArrayList<String>();
	 	
		if (searchvalue != null && !searchvalue.isEmpty()) {
			bean = new ArrayList<IndSignupBean>();
			IndSignupBean beans = null;

			for (IndSignupModel  docdetail : searchvalue) {
	            beans = new IndSignupBean();
	            
	            IndSignupModel searchpic=new IndSignupModel();
				
	            beans.setIndid(docdetail.getIndid());
				search.add(beans.getIndid().toString());
				
				beans.setFirstname(docdetail.getFirstname()); 
				search.add(beans.getFirstname().toString());
								
				beans.setMiddlename(docdetail.getMiddlename());
				search.add(beans.getMiddlename().toString());
				
				beans.setLastname(docdetail.getLastname());
				search.add(beans.getLastname().toString());
				
				beans.setCr_date(docdetail.getCr_date());
				
				if(beans.getCr_date()!=null) {
				   
					search.add(beans.getCr_date().toString());
				}
				if(beans.getCr_date()==null) {
					
					/*search.add(beans.getCr_date().toString());*/
					   
					search.add("");
				}
				
				searchpic.setIndid(beans.getIndid());
			
				if(!"noprofilepic".equals(profileinfoService.getprofilepic(searchpic)))
				{
					String profilepic=profileinfoService.getprofilepic(searchpic);				
					beans.setEmailid(profilepic);
					search.add(beans.getEmailid().toString());
				}
				
				if("noprofilepic".equals(profileinfoService.getprofilepic(searchpic)))
				{
					search.add("no image");					
				}
									
				search.add(profileinfoService.getsearchedPresentAddress(searchpic));
				
				bean.add(beans);
												
			}
		  }
		return search;
		}  

	
	
	@RequestMapping(value ="/getKYCAboutUs",method = RequestMethod.POST)
	public String getKYCAboutUs(HttpSession session,HttpServletRequest request) 
	{
		return "aboutUs";
	}
	
	
	@RequestMapping(value ="/progressBar",method = RequestMethod.GET)
	public String progressBar() 
	{
		return "progressBar";
	}
	
	@RequestMapping(value ="/clenseeTermConditions",method = RequestMethod.GET)
	public String getKYCTermConditions(HttpSession session,HttpServletRequest request) 
	{
		return "termsAndconditions";
	}
	
	@RequestMapping(value ="/clenseePrivatePolicy",method = RequestMethod.GET)
	public String getKYCPrivatePolicy(HttpSession session,HttpServletRequest request) 
	{
		return "privacyPolicy";
	}	
	
	
	
	
	@RequestMapping(value ="/individualshare",method = RequestMethod.GET)
	public String individualShare(HttpSession session,HttpServletRequest request) 
	{
		return "individualSharingInfo";
	}
	
	@RequestMapping(value ="/utilizershare",method = RequestMethod.GET)
	public String utilizerShare(HttpSession session,HttpServletRequest request) 
	{
		return "utilizerSharingInfo";
	}
	
	@RequestMapping(value ="/kycService",method = RequestMethod.GET)
	public String kycService(HttpSession session,HttpServletRequest request) 
	{
		return "kycServicesInfo";
	}
	
	@RequestMapping(value ="/easyaccess",method = RequestMethod.GET)
	public String easyAccess(HttpSession session,HttpServletRequest request) 
	{
		return "easyAccessInfo";
	}
	
	
	
	
	
	@RequestMapping(value ="/validateCaptchaCode",method = RequestMethod.POST)
	public @ResponseBody String checkCapthcaCode(HttpSession session,HttpServletRequest request) throws IllegalStateException, IOException
	{
		String status = "";
		Captcha captcha = (Captcha)session.getAttribute(Captcha.NAME);
		request.setCharacterEncoding("UTF-8");
		String answer = request.getParameter("answer");
		if (captcha.isCorrect(answer)) 
		{			
			status = "Correct";
		}
		else
		{
			status = "Incorrect";
		}
		return status;
	}
		
	// to save basic detail
	
	@RequestMapping(value ="/saveProfilepicture",method = RequestMethod.POST)
	public @ResponseBody String saveProfilePicture(HttpSession session,HttpServletRequest request,
			@RequestParam CommonsMultipartFile[] Passportsize) throws IllegalStateException, IOException
	{
		
		String ind_kyc_id=(String)session.getAttribute("Ind_id");
		int ind_id = Integer.parseInt(ind_kyc_id);
		int year = docservice.findYear(ind_kyc_id);
		String picName= "";
		 
		File ourPath = new File(kycMainDirectory);
		
		File fld = new File(ourPath.getPath()+"/"+year+"/"+ind_kyc_id+"/Profile_Photo");
		fld.mkdirs();
		
		String loc = ourPath.getPath()+"/"+year+"/"+ind_kyc_id+"/Profile_Photo";
		
		StringBuilder finalstring=new StringBuilder();
			
		String documentName = "";
		float datasizeKB=(float)0.0; 
		
		if (Passportsize != null && Passportsize.length > 0)
        {			
            for (CommonsMultipartFile aFile : Passportsize)            
            {             
                if (!aFile.getOriginalFilename().equals(""))                 
                {                  	
                    aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
                   
                    File datasize = new File(loc+"/"+aFile.getOriginalFilename());
                    float bytes = datasize.length();
                    datasizeKB = datasizeKB + bytes/1024;      
                    
                    String extension = "";
                    
                    int i = aFile.getOriginalFilename().lastIndexOf('.');
                    if (i > 0) {
                                extension = aFile.getOriginalFilename().substring(i+1);
                               }
                                                       
                   File store = new File(loc);
                   File[] storelist = store.listFiles();
                   
                   String flname= ind_id+"-Profile-Photo_1";
                   boolean check=true;
                   
                   for(int f=0;f<50;f++)
                    {                 	   
                	   if(check==true)
                	   {
                    	  for(int g=0;g<storelist.length;g++)
                    	   {    
                    		  System.out.println("Check File name and given file Name"+flname.concat(".").concat(extension)+"=="+storelist[g].getName());
                    	      if((flname.concat(".").concat(extension)).equals(storelist[g].getName()))
                               	{                   		      
                    	    	    check=false;                    	    	    
                    	        }
                    	   }
                    	  if(check==true)
                       	       {                     		   
                       		      File floldname = new File(loc+"/"+aFile.getOriginalFilename());                   
                                  File flnewname = new File(loc+"/"+flname+"."+extension);
                                  floldname.renameTo(flnewname);                                 
                                  documentName = flnewname.getName();
                                  
                                  
		                                  /* ThumbNail For KYC Documents */
		                                  
			                                if(!extension.equals("pdf"))
			                                {			                                	
			                                	  File thumbNailDirectory = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+ind_id+"/ThumbNail_Documents/"+"Profile_Photo");
			                                	  thumbNailDirectory.mkdirs();
			                                	  BufferedImage img = ImageIO.read(flnewname); 
			                                	  int width          = img.getWidth();
			                                	  int height         = img.getHeight();
			                                	  if(width>220 && height>230)
			                                	  {
			                                		  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,220,230, Scalr.OP_ANTIALIAS); 
					                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
					                                  ImageIO.write(thumbImg,extension,os);
					                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+ind_id+"/ThumbNail_Documents/"+"Profile_Photo"+"/"+flname+"."+extension);  
					                                  ImageIO.write(thumbImg, extension, f2);				                        		                                		 
			                                	  }
			                                	  else
			                                	  {
			                                		  BufferedImage thumbImg = Scalr.resize(img, Method.QUALITY,Mode.AUTOMATIC,width,height, Scalr.OP_ANTIALIAS); 
					                                  ByteArrayOutputStream os = new ByteArrayOutputStream();
					                                  ImageIO.write(thumbImg,extension,os);
					                                  File f2 = new File(ourPath.getPath()+"/"+(String)session.getAttribute("created_year")+"/"+ind_id+"/ThumbNail_Documents/"+"Profile_Photo"+"/"+flname+"."+extension);  
					                                  ImageIO.write(thumbImg, extension, f2);	
			                                	  }
				                                  							                             
			                                }
			                                 
			                                  
		                                /* End of Thumbnail Creation */
                                  break;
                       	       }             	                         	                     	                     	                    	  
                	   }                	  
                	   if(check==false)
                	   {
                		   String[] ary=flname.split("_");
                		   
                		   for(int k=1;k<ary.length;k++)
                		   {  
         	                  int l=Integer.parseInt(ary[k])+1;
         	                  String s=Integer.toString(l);
         	                  flname="";
         	                  flname= ind_id+"-Profile-Photo_".concat(s);
         	                  check=true;
                		   }
                	    }                	                   	   
                    }                   
                }
            }
        }
		
		ProfilePictureModel profileModel = new ProfilePictureModel();
		profileModel.setKyc_ind_id(ind_id);
		profileModel.setProfile_picture_name(documentName);
		profileModel.setCr_date(new Date());		
	    String status = profileinfoService.saveProfilePicInfo(profileModel);
	    
	    if(status.equals("success"))
   	    {
	    	
   	        String profile_Kyc_id= profileinfoService.getkycid(ind_id);
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("Profile picture has been updated.");
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("Profile Picture updated");
			Profileupdatemodel.setMore_about_it("Profile Picture uploaded");
		    Profileupdatemodel.setKyc_id(profile_Kyc_id);		    
			profileinfoService.profileupdate(Profileupdatemodel);
			
   	    }	
	    
	    String profile_picName = profileinfoService.getLatestPicName(ind_id);	    	  
		return ind_kyc_id+","+profile_picName;					
	}
	
	
	
	@RequestMapping(value = "/savebasicdetail", method = RequestMethod.POST)
	public @ResponseBody String saveBasicDetails11(HttpSession session,HttpServletRequest request,
			@RequestParam CommonsMultipartFile[] Passportsize) throws Exception  {
				
		 String ind_kyc_id=(String)session.getAttribute("Ind_id");
		 
		    IndSignupModel individual = new IndSignupModel();
			individual.setKycid(ind_kyc_id);
			
			 int year = docservice.findYear(ind_kyc_id);
			File fld = new File(request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY")+"/"+year+"/"+ind_kyc_id);
			fld.mkdirs();
			String loc=request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY")+"/"+year+"/"+ind_kyc_id;
			StringBuilder finalstring=new StringBuilder();
						
			if (Passportsize != null )
			{
			    for (CommonsMultipartFile aFile : Passportsize)			     
			    {			          			      		         
			        if (!aFile.getOriginalFilename().equals("")) 			        
			        {			        	
			        	session.setAttribute("profilepic", aFile.getOriginalFilename());
			            aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));			           
			        }
			    }
			}
			
			String i =(String)session.getAttribute("Ind_id");
			BasicDetailBean DetailBean=new BasicDetailBean();
			
			DetailBean.setDOB(request.getParameter("DOB"));
			DetailBean.setEmailalternative(request.getParameter("Emailalternative"));
			DetailBean.setHobbies(request.getParameter("Hobbies"));
		
			String str=request.getParameter("Languages");
			DetailBean.setInd_kyc_id(i);  
			DetailBean.setLanguages(request.getParameter("Languages"));
						
			DetailBean.setMatrialstatus(request.getParameter("Matrialstatus"));
			DetailBean.setNationality(request.getParameter("Nationality"));
			DetailBean.setPassportsize((String)session.getAttribute("profilepic"));
			DetailBean.setCr_date(new Date());
				
			String photo=(String)session.getAttribute("profilepic");
						
			            //finalstring.append(location);
			 finalstring.append(year);
			 finalstring.append(",");
			 finalstring.append(photo);
			 finalstring.append(",");
			 finalstring.append(ind_kyc_id);
			            
			DetailBean.setPermanent_address(request.getParameter("Permanent_address"));
			DetailBean.setPresent_address(request.getParameter("Present_address"));
			DetailBean.setTeloff(request.getParameter("Teloff"));
			DetailBean.setTelres(request.getParameter("Telres"));
			DetailBean.setDOB(request.getParameter("DOB"));
			DetailBean.setEmailalternative(request.getParameter("Emailalternative"));
			
		BasicDetail BasicDetail = preparebasicModel(DetailBean);
		profileinfoService.addBasicDetail(BasicDetail);
		
		if("successsavebasic".equals(profileinfoService.addBasicDetail(BasicDetail)))
		{
			String ind_kyc_id1=(String)session.getAttribute("Kyc_id");
			
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("Basic Details has updated");
			Profileupdatemodel.setType_of_alert("profile update");
			Profileupdatemodel.setKyc_id(ind_kyc_id1);
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setMore_about_it("Saved Basic Detail");
			profileinfoService.profileupdate(Profileupdatemodel); 
		    
			return finalstring.toString();
		}
		
		else
		{			
			return "failure";
		}
	}
	
	
	
	
	
	
	// to save basic detail

		
	private BasicDetail preparebasicModel(BasicDetailBean BasicDetailBean) {
		
		BasicDetail BasicDetail=new BasicDetail();
		BasicDetail.setDate_of_birth(BasicDetailBean.getDOB());
		BasicDetail.setAlternate_email(BasicDetailBean.getEmailalternative());
		BasicDetail.setMarital_status(BasicDetailBean.getMatrialstatus());
		BasicDetail.setNationality(BasicDetailBean.getNationality());
		BasicDetail.setProfile_pic(BasicDetailBean.getPassportsize());
		BasicDetail.setTell_office(BasicDetailBean.getTeloff());
		BasicDetail.setTell_residence(BasicDetailBean.getTelres());
	
		BasicDetail.setPermanent_address(BasicDetailBean.getPermanent_address());
		BasicDetail.setPresent_address(BasicDetailBean.getPresent_address());
		BasicDetail.setLanguages_known(BasicDetailBean.getLanguages());
		BasicDetail.setAlternate_email(BasicDetailBean.getEmailalternative());
		BasicDetail.setHobbies(BasicDetailBean.getHobbies());
		BasicDetail.setInd_kyc_id(BasicDetailBean.getInd_kyc_id());
		BasicDetail.setCr_date(new Date());
		BasicDetail.setVisibility(BasicDetailBean.getVisibility());
	return BasicDetail;
	
	}
	
	
	// Edit basic detail
	@RequestMapping(value = "/editbasicdetail", method = RequestMethod.POST)
	public @ResponseBody String saveBasicDetail(HttpSession session,HttpServletRequest request) throws Exception  {
		 
		 String ind_kyc_id=(String)session.getAttribute("Ind_id");
		 int year = docservice.findYear(ind_kyc_id);
		 		
		 StringBuilder str=new StringBuilder();				
		 IndSignupModel individual = new IndSignupModel();
		 individual.setKycid(ind_kyc_id);
			
			
			
			
			String i =(String)session.getAttribute("Ind_id");
			BasicDetailBean DetailBean=new BasicDetailBean();
			
			DetailBean.setDOB(request.getParameter("DOB"));
			DetailBean.setEmailalternative(request.getParameter("Emailalternative"));			
			DetailBean.setHobbies(request.getParameter("Hobbies"));
			
			DetailBean.setInd_kyc_id(i);  			
			DetailBean.setLanguages(request.getParameter("Languages"));
						
			DetailBean.setMatrialstatus(request.getParameter("Matrialstatus"));
			DetailBean.setNationality("Indian");
		    DetailBean.setVisibility((String)session.getAttribute("visibility"));
			String profilepicName = "";
			int id = Integer.parseInt(i);
			profilepicName = profileinfoService.getLatestPicName(id);
			
			if(!profilepicName.equals("no image"))
			{
			     DetailBean.setPassportsize(profilepicName);
			}
			else
			{				
				DetailBean.setPassportsize("");				
			}
			
			String permanentAddr = "";
			String presentAddr = "";
			
			
			
			if(request.getParameter("Permanent_address").equals(""))
			{
				permanentAddr = permanentAddr + profileinfoService.getPermanentAddress(id);		
				if(!permanentAddr.equals("nothing"))
				{
					DetailBean.setPermanent_address(permanentAddr);
				}
				else
				{
					DetailBean.setPermanent_address("");
				}
				
				
			}
						
			if(!request.getParameter("Permanent_address").equals(""))
			{
				
				DetailBean.setPermanent_address(request.getParameter("Permanent_address"));
				
			}
			
						
			if(request.getParameter("Present_address").equals(""))
			{
				presentAddr = presentAddr + profileinfoService.getPresentAddress(id);	
				
				if(!presentAddr.equals("nothing"))
				{
					DetailBean.setPresent_address(presentAddr);
				}
				else
				{
					DetailBean.setPresent_address("");
				}
			}
			
			if(!request.getParameter("Present_address").equals(""))
			{
				
				DetailBean.setPresent_address(request.getParameter("Present_address"));
				
			}
								
			
			DetailBean.setTeloff(request.getParameter("Teloff"));
			DetailBean.setTelres(request.getParameter("Telres"));
			String editbasicdetail=request.getParameter("profileupdatebasic");
			
		BasicDetail BasicDetail = preparebasicModel(DetailBean);
		
		str.append(year);
		str.append(",");
		str.append((String)session.getAttribute("profilepic"));
		str.append(",");
		str.append(ind_kyc_id);
				
		if("successeditbasic".equals(profileinfoService.editBasicDetail(BasicDetail)))
		{			
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("Basic Details has updated");
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("profile update");
			Profileupdatemodel.setKyc_id(ind_kyc_id);
			Profileupdatemodel.setMore_about_it(editbasicdetail);
			profileinfoService.profileupdate(Profileupdatemodel); 
			
			
			
			return str.toString();
		}
		
		
		return "failure";
	}		
	
	
	
	
	@RequestMapping(value = "/editbasicdetailfinal", method = RequestMethod.POST)
	public @ResponseBody String saveBasicDetail1(HttpSession session,HttpServletRequest request,
			@RequestParam CommonsMultipartFile[] Passportsize) throws Exception  {
		 
		 String ind_kyc_id=(String)session.getAttribute("Ind_id");
		 
		 StringBuilder str=new StringBuilder();
							
			IndSignupModel individual = new IndSignupModel();
			individual.setKycid(ind_kyc_id);
			
			
			File fld = new File(request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY")+"/"+session.getAttribute("created_year")+"/"+ind_kyc_id);
			fld.mkdirs();
			String loc=request.getServletContext().getRealPath("/KYC_DOC_REPOSITORY")+"/"+session.getAttribute("created_year")+"/"+ind_kyc_id;
			if (Passportsize != null && Passportsize.length > 0)
			{
			    for (CommonsMultipartFile aFile : Passportsize)
			     
			    {			      
			        if (!aFile.getOriginalFilename().equals("")) 
			        
			        {
			        	 session.setAttribute("profilepic", aFile.getOriginalFilename());
			        	
			            aFile.transferTo(new File(loc + "/" + aFile.getOriginalFilename()));
			           
			        }
			    }
			}
			  String i =(String)session.getAttribute("Ind_id");
			BasicDetailBean DetailBean=new BasicDetailBean();
			
			DetailBean.setDOB(request.getParameter("DOB"));
			DetailBean.setEmailalternative(request.getParameter("Emailalternative"));
			DetailBean.setHobbies(request.getParameter("Hobbies"));			
			DetailBean.setInd_kyc_id(i);  			
			DetailBean.setLanguages(request.getParameter("Languages"));			
			DetailBean.setMatrialstatus(request.getParameter("Matrialstatus"));
			DetailBean.setNationality(request.getParameter("Nationality"));
			DetailBean.setPassportsize((String)session.getAttribute("profilepic"));
			DetailBean.setPermanent_address(request.getParameter("Permanent_address"));				
			DetailBean.setPresent_address(request.getParameter("Present_address"));
			DetailBean.setTeloff(request.getParameter("Teloff"));
			DetailBean.setTelres(request.getParameter("Telres"));
			String editbasicdetail=request.getParameter("profileupdatebasic");
			
			int year = docservice.findYear(ind_kyc_id);
			
			
		BasicDetail BasicDetail = preparebasicModel(DetailBean);
		if("successeditbasic".equals(profileinfoService.editBasicDetail(BasicDetail)))
		{			
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("Basic Details has updated");
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("profile update");
			Profileupdatemodel.setKyc_id(ind_kyc_id);
			Profileupdatemodel.setMore_about_it(editbasicdetail);
			profileinfoService.profileupdate(Profileupdatemodel);  
			str.append(year);
			str.append((String)session.getAttribute("profilepic"));
			str.append(ind_kyc_id);
			
			return str.toString();
		}
		
		else{
			return "saving failed";
		}
		
	}		
	
	

	@RequestMapping(value = "/myhistory", method = RequestMethod.POST)
	public ModelAndView savehistory(HttpSession session,@ModelAttribute("command") FamilyDetailBean FamilyDetailBean, 
			BindingResult result) {
		
		String kyc_id=(String)session.getAttribute("Kyc_id");
		
		
		return new ModelAndView("myhistory");
	}
	
	
	@RequestMapping(value = "/reghistory", method = RequestMethod.POST)
	public @ResponseBody
	String reghistory(HttpSession session) {

		String kyc_id = (String) session.getAttribute("Kyc_id");
		int ind_id = profileinfoService.findInd_id(kyc_id);

		String historyData = signupService.reghistory(ind_id);

		return historyData;

	}
	
	
	@RequestMapping(value = "/reghistoryfromUtilizer", method = RequestMethod.POST)
	public @ResponseBody
	String reghistoryfromUtilizer(HttpSession session,HttpServletRequest request) {

		
		int ind_id = Integer.parseInt(request.getParameter("searchedIndId"));

		String historyData = signupService.reghistory(ind_id);

		return historyData;

	}
	
		
	@RequestMapping(value = "/savefamilydetail", method = RequestMethod.POST)
	public ModelAndView saveFamilyDetails(HttpSession session,@ModelAttribute("command")FamilyDetailBean FamilyDetailBean, 
				BindingResult result) {
	 
     String ind_kyc_id=(String)session.getAttribute("Ind_id");
	 System.out.println("savefamilydetail contkycid"+ind_kyc_id);
		
			FamilyDetail FamilyDetail = new FamilyDetail();
			FamilyDetail.setInd_kyc_id(ind_kyc_id);
			FamilyDetail.setFather_name(FamilyDetailBean.getFathername());
			FamilyDetail.setFather_POI(FamilyDetailBean.getFatherPOI());
			FamilyDetail.setMother_name(FamilyDetailBean.getMothername());
			FamilyDetail.setSpouse_name(FamilyDetailBean.getSpousename());
			FamilyDetail.setSibling_POI(FamilyDetailBean.getBrotherPOI());
			FamilyDetail.setMother_POI(FamilyDetailBean.getMotherPOI());
			FamilyDetail.setSibling_name(FamilyDetailBean.getBrothername());
			FamilyDetail.setSpouse_POI(FamilyDetailBean.getSpousePOI());
			FamilyDetail.setMaritalstatus(FamilyDetailBean.getMaritalstatus());
			FamilyDetail.setSpousegender(FamilyDetailBean.getSpousegender());	
			FamilyDetail.setHoroscopeInformation(FamilyDetailBean.getHoroscopeInformation());
			FamilyDetail.setFoodpreferred(FamilyDetailBean.getFoodpreferred());	
			 
		
			if("successsavefamily".equals(profileinfoService.addFamilyDetail(FamilyDetail)))
			{
				 String ind_kyc_id1=(String)session.getAttribute("Kyc_id");
				 System.out.println("transaction savefamilydetail ");
				Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
				Profileupdatemodel.setProfileupdatedetail("saved family detail");
				Profileupdatemodel.setProfileupdatetime(new Date());
				Profileupdatemodel.setType_of_alert("profile update");
				Profileupdatemodel.setKyc_id(ind_kyc_id1);
				Profileupdatemodel.setMore_about_it("saved Family Detail");
				profileinfoService.profileupdate(Profileupdatemodel);              
			}
		return new ModelAndView("redirect:/help1.html");
	}
	
	@RequestMapping(value = "/savefamilydetailview", method = RequestMethod.POST)
	public ModelAndView saveFamilyDetailsview(HttpSession session,@ModelAttribute("command")FamilyDetailBean FamilyDetailBean, 
				BindingResult result) {
	 
     String ind_kyc_id=(String)session.getAttribute("Ind_id");
	 System.out.println("savefamilydetail contkycidvvvvvvvvvvvvvvvvvvv"+ind_kyc_id);
		
			FamilyDetail FamilyDetail = new FamilyDetail();
		
			FamilyDetail.setFather_name(FamilyDetailBean.getFathername());
			FamilyDetail.setFather_POI(FamilyDetailBean.getFatherPOI());
			FamilyDetail.setMother_name(FamilyDetailBean.getMothername());
			FamilyDetail.setSpouse_name(FamilyDetailBean.getSpousename());
			FamilyDetail.setSibling_POI(FamilyDetailBean.getBrotherPOI());
			FamilyDetail.setMother_POI(FamilyDetailBean.getMotherPOI());
			FamilyDetail.setSibling_name(FamilyDetailBean.getBrothername());
			FamilyDetail.setSpouse_POI(FamilyDetailBean.getSpousePOI());
			FamilyDetail.setInd_kyc_id(ind_kyc_id); 
		
			if("successsavefamily".equals(profileinfoService.addFamilyDetail(FamilyDetail)))
			{
				 String ind_kyc_id1=(String)session.getAttribute("Kyc_id");
				 System.out.println("transaction savefamilydetail ");
				Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
				Profileupdatemodel.setProfileupdatedetail("Family details has been updated");
				Profileupdatemodel.setProfileupdatetime(new Date());
				Profileupdatemodel.setType_of_alert("profile update");
				Profileupdatemodel.setKyc_id(ind_kyc_id1);
				Profileupdatemodel.setMore_about_it("saved Family Detail");
				profileinfoService.profileupdate(Profileupdatemodel);              
			}
			
		return new ModelAndView("redirect:/helpviewprofile.html");
	}
	
		
// edit family detail		
		@RequestMapping(value = "/editfamilydetail", method = RequestMethod.POST)
		public @ResponseBody String editFamilyDetails(HttpServletRequest request,HttpSession session,@ModelAttribute("command")FamilyDetailBean FamilyDetailBean, 
					BindingResult result) {
		 
			
         String ind_kyc_id=(String)session.getAttribute("Kyc_id");
         String ind_id=(String)session.getAttribute("Ind_id");
		 
         int id = Integer.parseInt(ind_id);
         
         IndSignupModel individual = new IndSignupModel();
		 individual.setKycid(ind_kyc_id);
						            
         String str=request.getParameter("profileupdatereg");

			FamilyDetail FamilyDetail = new FamilyDetail();
					
			FamilyDetail.setFather_name(FamilyDetailBean.getFathername());
			FamilyDetail.setFather_POI(FamilyDetailBean.getFatherPOI());
			FamilyDetail.setMother_name(FamilyDetailBean.getMothername());
			FamilyDetail.setSpouse_name(FamilyDetailBean.getSpousename());
			FamilyDetail.setSibling_POI(FamilyDetailBean.getBrotherPOI());
			FamilyDetail.setMother_POI(FamilyDetailBean.getMotherPOI());
			FamilyDetail.setSibling_name(FamilyDetailBean.getBrothername());
			FamilyDetail.setSpouse_POI(FamilyDetailBean.getSpousePOI());
			FamilyDetail.setMaritalstatus(FamilyDetailBean.getMaritalstatus());
			FamilyDetail.setHoroscopeInformation(FamilyDetailBean.getHoroscopeInformation());
			FamilyDetail.setFoodpreferred(FamilyDetailBean.getFoodpreferred());
			FamilyDetail.setSpousegender(FamilyDetailBean.getSpousegender());
			FamilyDetail.setInd_kyc_id(ind_id); 
			FamilyDetail.setCr_by(ind_kyc_id);
			FamilyDetail.setCr_date(new Date());			
			
			if("successeditfamily".equals(profileinfoService.geteditfamilyvalue(FamilyDetail)))
			{
				
				Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
				Profileupdatemodel.setProfileupdatedetail("Family details has been updated");
				Profileupdatemodel.setProfileupdatetime(new Date());
				Profileupdatemodel.setType_of_alert("profile update");
				Profileupdatemodel.setMore_about_it(str);
				Profileupdatemodel.setKyc_id(ind_kyc_id);
				profileinfoService.profileupdate(Profileupdatemodel);     
											
				return "family detail saved successfully";
			}
			else
			{
				return "family detail fail to save";
				
			}
				
			
		}
		
		@RequestMapping(value ="/getvalidateRegistrationEmailId", method = RequestMethod.POST)
		public @ResponseBody String getvalidateRegistrationEmailId(HttpServletRequest request,HttpSession session) 
		{
			
			String emailId = request.getParameter("emailId");
			int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
			
			String emailIdStatus = profileinfoService.getvalidateRegistrationEmailId(emailId,ind_id);
			
			return emailIdStatus;
		}
		
		@RequestMapping(value ="/getvalidateRegistrationMobileNo", method = RequestMethod.POST)
		public @ResponseBody String getvalidateRegistrationMobileNo(HttpServletRequest request,HttpSession session) 
		{
			String mobileNo = request.getParameter("mobileNo");
			int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
			
			String mobileNoStatus = profileinfoService.getvalidateRegistrationMobileNo(mobileNo,ind_id);
			
			return mobileNoStatus;
		}
			
		
		@RequestMapping(value ="/getvalidateBasicEmailId", method = RequestMethod.POST)
		public @ResponseBody String getvalidateBasicEmailId(HttpServletRequest request,HttpSession session) 
		{
			String emailIdStatus = "";
			String emailId = request.getParameter("emailId");
			int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
			
			System.out.println("Given Email Id-"+emailId);
			
			if(emailId.matches(".*[a-zA-Z]+.*"))
			{
				emailIdStatus = profileinfoService.getvalidateRegistrationEmailId(emailId,ind_id);
			}
			else
			{
				emailIdStatus = "Not Exist";				
			}
											
			return emailIdStatus;
		}
		
		@RequestMapping(value ="/getvalidateBasicMobileNo", method = RequestMethod.POST)
		public @ResponseBody String getvalidateBasicMobileNo(HttpServletRequest request,HttpSession session) 
		{
			String mobileNoStatus = "";
			String mobileNo = request.getParameter("mobileNo");
			int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
			
			System.out.println("Given ph No-"+mobileNo);
			
			if(mobileNo.matches("[0-9]+"))
			{
				mobileNoStatus = profileinfoService.getvalidateRegistrationMobileNo(mobileNo,ind_id);
			}
			else
			{
				mobileNoStatus = "Not Exist";
			}
						
			return mobileNoStatus;
		}
							
			@RequestMapping(value ="/editregdetail", method = RequestMethod.POST)
			public @ResponseBody String editregacc(HttpServletRequest request,HttpSession session,@ModelAttribute("command")FamilyDetailBean FamilyDetailBean  ,BindingResult result) {
				
				  String ind_kyc_id=(String)session.getAttribute("Kyc_id");
				  int ind_id = profileinfoService.findInd_id(ind_kyc_id);
				  
				   RegHistoryModel indHistory = new RegHistoryModel();
					 				 
					 indHistory.setInd_id(ind_id);
					 indHistory.setFirst_name(request.getParameter("firstname"));
					 indHistory.setMiddle_name(request.getParameter("middlename"));
					 indHistory.setLast_name(request.getParameter("lastname"));
					 indHistory.setGender(request.getParameter("gender"));
					 indHistory.setMobile_no(Long.parseLong(request.getParameter("mobileno")));
					 indHistory.setEmail_id(request.getParameter("emailid"));
					 indHistory.setGender(request.getParameter("gender"));
					 indHistory.setCr_date(new Date());
				
					 //RegHistoryModel previousRegData =  signupService.getLatestRegData(ind_id);	 
					 
				if("success".equals(signupService.editIndividual(indHistory)))
				{
					String EmailId = request.getParameter("emailid");
					profileinfoService.getUpdateEmailId(ind_id,EmailId);
					
					String fulleName = "";
					
					if(!request.getParameter("firstname").equals(""))
					{
						fulleName += request.getParameter("firstname") + " ";
					}
					
					if(!request.getParameter("middlename").equals(""))
					{
						fulleName += request.getParameter("middlename") + " ";
					}
					
					if(!request.getParameter("lastname").equals(""))
					{
						fulleName += request.getParameter("lastname");
					}
										
					profileinfoService.getUpdateRegTableWithLastRecord(indHistory,fulleName);
				}
					
	            return "saved successfully";
			}
		
		
		
				private List<IndSignupBean> Preparesignupbean(List<IndSignupModel> searchvalue) {
				  	//DocumentDetailsBean  bean = new DocumentDetailsBean();
				 	List<IndSignupBean> bean = null;
				 	
					if (searchvalue != null && !searchvalue.isEmpty()) {
						bean = new ArrayList<IndSignupBean>();
						IndSignupBean beans = null;

						for (IndSignupModel  docdetail : searchvalue) {
				            beans = new IndSignupBean();
				           
							beans.setIndid(docdetail.getIndid());
							beans.setCr_date(docdetail.getCr_date());
							beans.setPassword(docdetail.getPassword());
							beans.setPlan_available(docdetail.getPlan_available());
							beans.setStatus(docdetail.getStatus());
							beans.setVisibility(docdetail.getVisibility()); 
							bean.add(beans);
						}
					  }
					
					return  bean;
					}  
		

				
	
				//edit socialdetail
		@RequestMapping(value ="/editsocialdetail", method = RequestMethod.POST)
		public @ResponseBody String editsocialacc(HttpServletRequest request,HttpSession session,@ModelAttribute("command") SocialaccBean SocialaccBean,BindingResult result) {
			
			
		     String ind_kyc_id=(String)session.getAttribute("Kyc_id");
					
				IndSignupModel individual = new IndSignupModel();
				individual.setKycid(ind_kyc_id);
				
				
				int ind_id = profileinfoService.findInd_id(ind_kyc_id);
				
				String sum_det_id=signupService.getInd_id(individual);
						
				Socialacc socialacc=new Socialacc();
				  
		        String str=request.getParameter("profileupdatereg");
                
		        String fcbookLink = "";
		        String twtrLink = "";
		        String linkedLink = "";
		        
		        fcbookLink = SocialaccBean.getSocialwebsitelink();
		        twtrLink = SocialaccBean.getSocialtwitterlink();
		        linkedLink = SocialaccBean.getSociallinkedlnlink();
		        
		        if(!fcbookLink.isEmpty())
		        {
		        	if(!fcbookLink.equals(""))
		        	{
		        		if(!fcbookLink.contains("http"))
		        		{
		        			fcbookLink = "https://"+fcbookLink;
		        		}
		        	}
		        }
		        
		        if(!twtrLink.isEmpty())
		        {
		        	if(!twtrLink.equals(""))
		        	{
		        		if(!twtrLink.contains("http"))
		        		{
		        			twtrLink = "https://"+twtrLink;
		        		}
		        	}
		        }
		        
		        
		        if(!linkedLink.isEmpty())
		        {
		        	if(!linkedLink.equals(""))
		        	{
		        		if(!linkedLink.contains("http"))
		        		{
		        			linkedLink = "https://"+linkedLink;
		        		}
		        	}
		        }
		        
				socialacc.setWebsite_link(fcbookLink);
				socialacc.setWebsite_name("facebook");
				socialacc.setTwitter_link(twtrLink);
				socialacc.setTwitter_name("twitter");
				socialacc.setLinkedln_link(linkedLink);
				socialacc.setLinkedln_name("linkedIn");
				socialacc.setInd_kyc_id(sum_det_id);
				socialacc.setCr_date(new Date());
				//socialacc.setCr_by(cr_by)
				 
			if("successeditsocialacc".equals(profileinfoService.editSocialacc(socialacc)))
			{					
				Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
				Profileupdatemodel.setProfileupdatedetail("Social account details has been updated.");
				Profileupdatemodel.setProfileupdatetime(new Date());
				Profileupdatemodel.setType_of_alert("profile update");
				Profileupdatemodel.setKyc_id(ind_kyc_id);
				Profileupdatemodel.setMore_about_it(str);
				profileinfoService.profileupdate(Profileupdatemodel);    				
			}
				
			return "saved successfully";
		}
		
		
		//edit marital details
		
		@RequestMapping(value ="/editmatrialdetail", method = RequestMethod.POST)
		public ModelAndView editmatrial(HttpServletRequest request,HttpSession session,@ModelAttribute("command") MaritalBean matrialBean,BindingResult result) {
			
			 String ind_kyc_id=(String)session.getAttribute("Kyc_id");
			 System.out.println("editfamilydetail contkycid"+ind_kyc_id);
				
						
				IndSignupModel individual = new IndSignupModel();
				individual.setKycid(ind_kyc_id);
				
				String i=signupService.getInd_id(individual);
								
		        String str=request.getParameter("profileupdatereg");

				Matrial matrial = new Matrial();
				matrial.setMarital_status(matrialBean.getMartialstatus());
				matrial.setSpouse_name(matrialBean.getSpousename());
				matrial.setSpouse_gender(matrialBean.getSpousegender());
				matrial.setSpouse_POI(matrialBean.getSpousePOI());
				matrial.setInd_kyc_id(i);
				matrial.setHoroscope_info(matrialBean.getHoroscope_info());
				matrial.setFood_preferred(matrialBean.getFood_preferred());
			   						
			if("successeditmatrimony".equals(profileinfoService.editmatrial(matrial)))
			{
				
				Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
				Profileupdatemodel.setProfileupdatedetail("edit matrimony detail");
				Profileupdatemodel.setProfileupdatetime(new Date());
				Profileupdatemodel.setType_of_alert("profile update");
				Profileupdatemodel.setKyc_id(ind_kyc_id);
				Profileupdatemodel.setMore_about_it(str);
				profileinfoService.profileupdate(Profileupdatemodel);              
			}
				
			return new ModelAndView("redirect:/help1.html");
		}
		
		
		// tell about himself, landing page
		@RequestMapping(value ="/landpagesummarydetail", method = RequestMethod.POST)
		public ModelAndView editsummary(HttpSession session,@ModelAttribute("command") SummarydetailBean SummarydetailBean,BindingResult result) {
			
			 String ind_kyc_id=(String)session.getAttribute("Ind_id");
			 System.out.println("landpagesummarydetailcontkycid"+ind_kyc_id);
				
						
				IndSignupModel individual = new IndSignupModel();
				individual.setKycid(ind_kyc_id);
				
				
				Summarydetail summarydetail = new Summarydetail();
				summarydetail.setInd_kyc_id(ind_kyc_id);
				
				summarydetail.setLandpagedescription(SummarydetailBean.getLandpagedescription());
				profileinfoService.editsummary(summarydetail);
				
     			return new ModelAndView("redirect:/landsum.html");
		}
		
		
		@RequestMapping(value = "/landsum")
		public ModelAndView landsummary(HttpSession session,HttpServletRequest request,@ModelAttribute("command") IndSignupBean signupBean,BindingResult result) {
			
			 String ind_kyc_id=(String)session.getAttribute("Kyc_id");
			 String ind_id=(String)session.getAttribute("Ind_id");
			 System.out.println("savebasicdetail contkycid"+ind_kyc_id);
				IndSignupModel individual = new IndSignupModel();
				individual.setKycid(ind_kyc_id);
				System.out.println("success");
				String i=signupService.getInd_id(individual);
				System.out.println("reg_id"+i); 
				
				IndSignupModel individualname = new IndSignupModel();
				int name= Integer.parseInt(i);
				System.out.println("summary id"+name); 
				DocumentDetailModel docdetails = new DocumentDetailModel();
		    	docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
				Summarydetail summary=new Summarydetail();
				summary.setInd_kyc_id(i);
							
				individual.setKycid(signupBean.getKycid());
				individual.setPassword(signupBean.getPassword());
				BasicDetail BasicDetail=new BasicDetail();
				BasicDetail.setInd_kyc_id(i);
				
				
				Map<String, Object> model = new HashMap<String, Object>();
			

				if ("failuresocialacc".equals(profileinfoService.Getsocialaccid(ind_id))) 
				{
					
				} 
				
				if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(ind_id))) 
				{			
				int fam=Integer.parseInt(profileinfoService.Getsocialaccid(ind_id));
				System.out.println("success socialacc"+fam); 

				  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));
			  	
				}
				
				
				if("failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
						
					{System.out.println("failurebasic");
						
					} 
				if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
					{
						int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));
						System.out.println("BasicDetailid"+BasicDetailid); 
						model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));
						System.out.println("BasicDetailid"+BasicDetailid); 
						
			    	}
				if(!"failure".equals(profileinfoService.Getregid(summary))) 
				{
					
					System.out.println("!failure");
					int su= Integer.parseInt(profileinfoService.Getregid(summary));
					System.out.println("summary id"+su); 
					
					model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));
					System.out.println("summary id"+su); 
					
				    
				 }
				if (profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
				{
					System.out.println("not success academic"); 
				} 
				
				if (!profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
				{			
			
				   System.out.println("success academic"); 
				
				   model.put("documentdetail",profileinfoService.Getacademicdetail(docdetails));
				
			
			  	}
				
				if (profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
				{
					System.out.println("not success employee"); 
				} 
				
				if (!profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
				{			
			
				System.out.println("success employee"); 
				
				model.put("employeedetail", prepareDocumentDetail(profileinfoService.Getemployeedetail(docdetails)));
						
			  	}
				
				summary.setInd_kyc_id(i);
				
				Matrial matrial=new Matrial();
				matrial.setInd_kyc_id(i);
				
				BasicDetail.setInd_kyc_id(i);
				
					
			    if ("failurefamily".equals(profileinfoService.Getfamid(i))) 
				{
					
				} 
				
				if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
				{			
				int fam=Integer.parseInt(profileinfoService.Getfamid(i));
				System.out.println("family success"+fam); 
			    model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
				}
				if ("failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
				{
					
				} 
				
				if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
				{			
				int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));
				System.out.println("success socialacc"+fam); 

				  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));
			  	
				}
				
				if("failure".equals(profileinfoService.Getregid(summary))) 
					
				{
					
				} 
				if(!"failure".equals(profileinfoService.Getregid(summary))) 
				{
					int su= Integer.parseInt(profileinfoService.Getregid(summary));
					System.out.println("summary id"+su); 
					model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));
					System.out.println("summary id"+su); 
					
				}
				
			   
				
				 if("failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
						
					{
						
					} 
					if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
					{
						int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));
						System.out.println("BasicDetailid"+BasicDetailid); 
						model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));
						System.out.println("BasicDetailid"+BasicDetailid); 
						
			    	}
				model.put("regname", prepareBean(signupService.getname(Integer.parseInt(i))));
				return new ModelAndView("individualProfile",model);
				
				
				
		}	
				
		
		
		//edit summary detail
		@RequestMapping(value ="/editsummarydetail", method = RequestMethod.POST)
		public ModelAndView editsummary1(HttpSession session,@ModelAttribute("command") SummarydetailBean SummarydetailBean,BindingResult result) {
			
			 String ind_kyc_id=(String)session.getAttribute("Kyc_id");
			 System.out.println("editfamilydetail contkycid"+ind_kyc_id);
				
						
				IndSignupModel individual = new IndSignupModel();
				individual.setKycid(ind_kyc_id);
				
				
				String ind_id=signupService.getInd_id(individual);
				System.out.println("reg_id"+ind_id); 
				String description=profileinfoService.getdescription(ind_id);
				
				Summarydetail summarydetail = new Summarydetail();
				summarydetail.setSum_det_id(SummarydetailBean.getIdsummarydetailBean());
				
				summarydetail.setInd_kyc_id(ind_id);
				summarydetail.setLandpagedescription(description);
				profileinfoService.editsummary(summarydetail);
			return new ModelAndView("redirect:/help1.html");
		}
		
		
		
		
		
		
		private FamilyDetailBean prepareEmployeeBean1(FamilyDetail familyDetail) {
			FamilyDetailBean bean = new FamilyDetailBean();
			bean.setFathername(familyDetail.getFather_name());
			bean.setInd_kyc_id(familyDetail.getInd_kyc_id());
			bean.setFatherPOI(familyDetail.getFather_POI());
			bean.setBrothername(familyDetail.getSibling_name());
			bean.setBrotherPOI(familyDetail.getSibling_POI());
			bean.setMothername(familyDetail.getMother_name());
			bean.setMotherPOI(familyDetail.getMother_POI());
			bean.setSpousename(familyDetail.getSpouse_name());
			bean.setSpousePOI(familyDetail.getSpouse_POI());
			bean.setMaritalstatus(familyDetail.getMaritalstatus());
			bean.setFoodpreferred(familyDetail.getFoodpreferred());
			bean.setHoroscopeInformation(familyDetail.getHoroscopeInformation()); 
			bean.setSpousegender(familyDetail.getSpousegender());				
							
			bean.setFid(familyDetail.getFam_details_id());
			return bean;
		}
			
		
		//autocomplete in edit profile
		@RequestMapping(value = "/help1", method = RequestMethod.GET)
		public ModelAndView saveEmployee(HttpSession session,@ModelAttribute("command") FamilyDetailBean FamilyDetailBean, 
				BindingResult result) {
			

				String ind_kyc_id=(String)session.getAttribute("Kyc_id");
				System.out.println("employee contkycid"+ind_kyc_id);
				
				IndSignupModel individual = new IndSignupModel();
				individual.setKycid(ind_kyc_id);
				String i=signupService.getInd_id(individual);
				System.out.println("reg_id"+i); 
				
				Summarydetail summary=new Summarydetail();
				summary.setInd_kyc_id(i);
				
				Matrial matrial=new Matrial();
				matrial.setInd_kyc_id(i);
				BasicDetail BasicDetail=new BasicDetail();
				BasicDetail.setInd_kyc_id(i);
				
				DocumentDetailModel docdetails = new DocumentDetailModel();
		    	docdetails.setKyc_ind_id(Integer.parseInt((String)session.getAttribute("Ind_id")));
				Map<String, Object> model = new HashMap<String, Object>();
						
				if ("failurefamily".equals(profileinfoService.Getfamid(i))) 
				{
					
				} 
				
				if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
				{			
				int fam=Integer.parseInt(profileinfoService.Getfamid(i));
				System.out.println("family success"+fam); 
			    model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
				}
						
				

				if ("failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
				{
					
				} 
				
				if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
				{			
				int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));
				System.out.println("success socialacc"+fam); 
			
		    	  model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));
			  
					
				}
				
				if("failure".equals(profileinfoService.Getregid(summary))) 
					
				{
					
				} 
				if(!"failure".equals(profileinfoService.Getregid(summary))) 
				{
					int su= Integer.parseInt(profileinfoService.Getregid(summary));
					System.out.println("summary id"+su); 
					model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));
					System.out.println("summary id"+su); 
					
		    	}
				
		        if("failurematrimony".equals(profileinfoService.Getmatid(matrial))) 
					
				{
					
				} 
				if(!"failurematrimony".equals(profileinfoService.Getmatid(matrial))) 
				{
					int matrialid= Integer.parseInt(profileinfoService.Getmatid(matrial));
					System.out.println("matrial id"+matrial); 
					model.put("matrialdetails", prepareMatrimonyBean(profileinfoService.getMatrimonyvalue(matrialid)));
					System.out.println("matrial id"+matrial); 
					
		    	}

				 if("failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
						
					{
						
					} 
					if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
					{
						int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));
						System.out.println("BasicDetailid"+BasicDetailid); 
						model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));
						System.out.println("BasicDetailid"+BasicDetailid); 
						
			    	}

					if (profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
					{
						System.out.println("not success academic"); 
					} 
					
					if (!profileinfoService.Getacademicdetail(docdetails).isEmpty()) 
					{			
				
					   System.out.println("success academic"); 
					
					   model.put("documentdetail",profileinfoService.Getacademicdetail(docdetails));
					
				
				  	}
					
					if (profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
					{
						System.out.println("not success employee"); 
					} 
					
					if (!profileinfoService.Getemployeedetail(docdetails).isEmpty()) 
					{			
				
					System.out.println("success employee"); 
					
					model.put("employeedetail", prepareDocumentDetail(profileinfoService.Getemployeedetail(docdetails)));
							
				  	}
					IndSignupModel individualname = new IndSignupModel();
					int name= Integer.parseInt(i);
					System.out.println("summary id"+name); 
					
					
					model.put("regname", prepareBean(signupService.getname(name)));
				
				return new ModelAndView("individualProfile",model);
				
		}
			
		
			private IndSignupModel prepareSignupModel(IndSignupBean indBean) {
				IndSignupModel signup = new IndSignupModel();
				signup.setFirstname(indBean.getFirstname());
				signup.setMiddlename(indBean.getMiddlename());
				signup.setLastname(indBean.getLastname());
				signup.setEmailid(indBean.getEmailid());
				signup.setMobileno(indBean.getMobileno());
				signup.setCr_date(new Date());
				signup.setPassword("123");
			//signup.setIndid(indBean.getIndid());
				return signup;
			}
			
			private IndSignupBean prepareBean(IndSignupModel IndSignup) {
				
				IndSignupBean bean=new IndSignupBean(); 
				bean.setFirstname(IndSignup.getFirstname());
				bean.setEmailid(IndSignup.getEmailid());
				bean.setKycid(IndSignup.getKycid());
				bean.setLastname(IndSignup.getLastname());
				bean.setMiddlename(IndSignup.getMiddlename());
				bean.setMobileno(IndSignup.getMobileno());
				bean.setPassword(IndSignup.getPassword());
				return bean;
				
			}
			private SocialaccBean prepareSocialaccBean(Socialacc socialacc) {
				SocialaccBean bean = new SocialaccBean();

				bean.setSocialwebsitelink(socialacc.getWebsite_link());
				bean.setSocialtwitterlink(socialacc.getTwitter_link());
				bean.setSociallinkedlnlink(socialacc.getLinkedln_link());
		
				bean.setInd_kyc_id(socialacc.getInd_kyc_id());
				return bean;
			}
			private SummarydetailBean prepareSummaryBean(Summarydetail summarydetail) {
				SummarydetailBean bean = new SummarydetailBean();
				
				bean.setLandpagedescription(summarydetail.getLandpagedescription());
				return bean;

			}
			
			
			private BasicDetailBean prepareBasicBean(BasicDetail BasicDetail)
			{
				BasicDetailBean bean=new BasicDetailBean();
				bean.setHobbies(BasicDetail.getHobbies());
				bean.setDOB(BasicDetail.getDate_of_birth());
				bean.setEmailalternative(BasicDetail.getAlternate_email());
				bean.setLanguages(BasicDetail.getLanguages_known());
				bean.setMatrialstatus(BasicDetail.getMarital_status());
				bean.setNationality(BasicDetail.getNationality());
				bean.setPassportsize(BasicDetail.getProfile_pic());
				bean.setPermanent_address(BasicDetail.getPermanent_address());
				bean.setPresent_address(BasicDetail.getPresent_address());
				bean.setTeloff(BasicDetail.getTell_office());
				bean.setTelres(BasicDetail.getTell_residence());
				return bean;
			}
			
			
			private MaritalBean prepareMatrimonyBean(Matrial matrial) {
				MaritalBean bean = new MaritalBean();
				bean.setInd_kyc_id(matrial.getInd_kyc_id());
				System.out.println("matrial value"+matrial.getInd_kyc_id());
				bean.setMartialstatus(matrial.getMarital_status());
				bean.setSpousegender(matrial.getSpouse_gender());
				bean.setSpousename(matrial.getSpouse_name());
				bean.setSpousePOI(matrial.getSpouse_POI());
				bean.setFood_preferred(matrial.getFood_preferred());
				bean.setHoroscope_info(matrial.getHoroscope_info());
				bean.setIdmatrialdetail(matrial.getMatrimony_details_id());
				return bean;
			}
			private FamilyDetail prepareModel(FamilyDetailBean FamilyDetailBean){
						
			FamilyDetail FamilyDetail = new FamilyDetail();
			FamilyDetail.setFam_details_id(FamilyDetailBean.getFid());
			FamilyDetail.setFather_name(FamilyDetailBean.getFathername());
			FamilyDetail.setFather_POI(FamilyDetailBean.getFatherPOI());
			FamilyDetail.setMother_name(FamilyDetailBean.getMothername());
			FamilyDetail.setSpouse_name(FamilyDetailBean.getSpousename());
			FamilyDetail.setSibling_POI(FamilyDetailBean.getBrotherPOI());
			FamilyDetail.setMother_POI(FamilyDetailBean.getMotherPOI());
			FamilyDetail.setSibling_name(FamilyDetailBean.getBrothername());
			FamilyDetail.setSpouse_POI(FamilyDetailBean.getSpousePOI());
			return FamilyDetail;
		 }
			
		
	//save maritaldetail
			@RequestMapping(value ="/matrialdetail", method = RequestMethod.POST)
			public ModelAndView saveMatrial(HttpSession session,
					@ModelAttribute("command") MaritalBean matrialBean,
					BindingResult result) {
				
					
		         String ind_kyc_id=(String)session.getAttribute("Ind_id");
				 System.out.println("save matrialdetail contkycid"+ind_kyc_id);
					
							
					IndSignupModel individual = new IndSignupModel();
					individual.setKycid(ind_kyc_id);
					Matrial matrial = new Matrial();
					matrial.setMarital_status(matrialBean.getMartialstatus());
					matrial.setSpouse_name(matrialBean.getSpousename());
					matrial.setSpouse_gender(matrialBean.getSpousegender());
					matrial.setSpouse_POI(matrialBean.getSpousePOI());
					matrial.setInd_kyc_id(ind_kyc_id);
					matrial.setHoroscope_info(matrialBean.getHoroscope_info());
					matrial.setFood_preferred(matrialBean.getFood_preferred());
					
					

					if("successsavematrimony".equals(profileinfoService.addMatrial(matrial)))
					{
						 String ind_kyc_id1=(String)session.getAttribute("Kyc_id");
						 System.out.println("transaction editfamilydetail ");
						Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
						Profileupdatemodel.setProfileupdatedetail("save matrimony detail");
						Profileupdatemodel.setProfileupdatetime(new Date());
						Profileupdatemodel.setType_of_alert("profile update");
						Profileupdatemodel.setKyc_id(ind_kyc_id1);
						profileinfoService.profileupdate(Profileupdatemodel);              
					}
					
					return new ModelAndView("redirect:/help1.html");
			}
			
		
			@RequestMapping(value ="/matrialdetailview", method = RequestMethod.POST)
			public ModelAndView saveMatrialview(HttpSession session,
					@ModelAttribute("command") MaritalBean matrialBean,
					BindingResult result) {
				
					
		         String ind_kyc_id=(String)session.getAttribute("Ind_id");
				 System.out.println("save matrialdetail contkycid"+ind_kyc_id);
					
							
					IndSignupModel individual = new IndSignupModel();
					individual.setKycid(ind_kyc_id);
					Matrial matrial = new Matrial();
					matrial.setMarital_status(matrialBean.getMartialstatus());
					matrial.setSpouse_name(matrialBean.getSpousename());
					matrial.setSpouse_gender(matrialBean.getSpousegender());
					matrial.setSpouse_POI(matrialBean.getSpousePOI());
					matrial.setInd_kyc_id(ind_kyc_id);
					matrial.setHoroscope_info(matrialBean.getHoroscope_info());
					matrial.setFood_preferred(matrialBean.getFood_preferred());
					
					

					if("successsavematrimony".equals(profileinfoService.addMatrial(matrial)))
					{
						 String ind_kyc_id1=(String)session.getAttribute("Kyc_id");
						 System.out.println("transaction editfamilydetail ");
						Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
						Profileupdatemodel.setProfileupdatedetail("save matrimony detail");
						Profileupdatemodel.setProfileupdatetime(new Date());
						Profileupdatemodel.setType_of_alert("profile update");
						Profileupdatemodel.setKyc_id(ind_kyc_id1);
						profileinfoService.profileupdate(Profileupdatemodel);              
					}
					
					return new ModelAndView("redirect:/helpviewprofile.html");
			}
			
		
		
		
		@RequestMapping(value = "/help2", method = RequestMethod.GET)
		public ModelAndView saveEmployee1(@ModelAttribute("command") MaritalBean MatrialBean, 
				BindingResult result) {
				return new ModelAndView("left_menu0");
		}

		private Matrial prepareModel(MaritalBean matrialBean) {
		
			Matrial matrial = new Matrial();
			matrial.setMarital_status(matrialBean.getMartialstatus());
			matrial.setSpouse_name(matrialBean.getSpousename());
			matrial.setSpouse_gender(matrialBean.getSpousegender());
			matrial.setSpouse_POI(matrialBean.getSpousePOI());
			matrial.setInd_kyc_id(matrialBean.getInd_kyc_id());
			matrial.setHoroscope_info(matrialBean.getHoroscope_info());
			matrial.setFood_preferred(matrialBean.getFood_preferred());
			return matrial;
		}
	
//save social detail
		@RequestMapping(value ="/socialdetail", method = RequestMethod.POST)
		public @ResponseBody String saveMatrial(HttpSession session,@ModelAttribute("command") SocialaccBean SocialaccBean,BindingResult result) {
			
			
		    String ind_kyc_id=(String)session.getAttribute("Ind_id");
			 System.out.println("socialdetail contkycid"+ind_kyc_id);
				
				Socialacc socialacc=new Socialacc();
				System.out.println("get website link"+SocialaccBean.getSocialwebsitelink()); 
				System.out.println("get website name"+SocialaccBean.getSocialwebsitename()); 
				socialacc.setWebsite_link(SocialaccBean.getSocialwebsitelink());
				socialacc.setLinkedln_link(SocialaccBean.getSociallinkedlnlink());
				socialacc.setTwitter_link(SocialaccBean.getSocialtwitterlink());
				socialacc.setWebsite_name(SocialaccBean.getSocialwebsitename());
				socialacc.setInd_kyc_id(ind_kyc_id);
						
				if("successsavesocialacc".equals(profileinfoService.addSocialacc(socialacc)))
				{
					 String ind_kyc_id1=(String)session.getAttribute("Kyc_id");
					 System.out.println("transaction socialaccdetail ");
					Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
					Profileupdatemodel.setProfileupdatedetail("save Socialaccount detail");
					Profileupdatemodel.setProfileupdatetime(new Date());
					Profileupdatemodel.setType_of_alert("profile update");
					Profileupdatemodel.setKyc_id(ind_kyc_id1);
					profileinfoService.profileupdate(Profileupdatemodel); 
					return "saved successfully";
				}
				else
				{
					return "not saved";
				}
		}

		
		
		@RequestMapping(value ="/socialdetailview", method = RequestMethod.POST)
		public ModelAndView saveMatrialview(HttpSession session,@ModelAttribute("command") SocialaccBean SocialaccBean,BindingResult result) {
			
			
		    String ind_kyc_id=(String)session.getAttribute("Ind_id");
			 System.out.println("socialdetail contkycid"+ind_kyc_id);
				
				Socialacc socialacc=new Socialacc();				
				socialacc.setWebsite_link(SocialaccBean.getSocialwebsitelink());
				socialacc.setWebsite_name(SocialaccBean.getSocialwebsitename());
				socialacc.setInd_kyc_id(ind_kyc_id);
						
				if("successsavesocialacc".equals(profileinfoService.addSocialacc(socialacc)))
				{
					String ind_kyc_id1=(String)session.getAttribute("Kyc_id");					 
					Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
					Profileupdatemodel.setProfileupdatedetail("save Socialaccount detail");
					Profileupdatemodel.setProfileupdatetime(new Date());
					Profileupdatemodel.setType_of_alert("profile update");
					Profileupdatemodel.setKyc_id(ind_kyc_id1);
					profileinfoService.profileupdate(Profileupdatemodel);              
				}
			
			return new ModelAndView("redirect:/helpviewprofile.html");
		}
		
		
// to populate values in view profile
@RequestMapping(value = "/help3", method = RequestMethod.GET)
public ModelAndView saveEmployee2(HttpSession session,@ModelAttribute("command") MaritalBean MatrialBean, 
		BindingResult result) {
	
	String ind_kyc_id=(String)session.getAttribute("Kyc_id");
	
	IndSignupModel individual = new IndSignupModel();
	individual.setKycid(ind_kyc_id);
	String i=signupService.getInd_id(individual);
	
	Summarydetail summary=new Summarydetail();
	summary.setInd_kyc_id(i);
	
	Matrial matrial=new Matrial();
	matrial.setInd_kyc_id(i);
	BasicDetail BasicDetail=new BasicDetail();
	BasicDetail.setInd_kyc_id(i);
	Map<String, Object> model = new HashMap<String, Object>();
		
if ("failurefamily".equals(profileinfoService.Getfamid(i))) 
	{
		
	} 
	
	if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
	{			
	int fam=Integer.parseInt(profileinfoService.Getfamid(i));	
    model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
	}
			
	

	if ("failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
	{
		
	} 
	
	if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
	{			
	    int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));
        model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));  		
	}
	
	if("failure".equals(profileinfoService.Getregid(summary))) 		
	{
		
	} 
	if(!"failure".equals(profileinfoService.Getregid(summary))) 
	{
		int su= Integer.parseInt(profileinfoService.Getregid(summary));
		model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));		
	}
	
    if("failurematrimony".equals(profileinfoService.Getmatid(matrial))) 
		
	{
		
	} 
	if(!"failurematrimony".equals(profileinfoService.Getmatid(matrial))) 
	{
		int matrialid= Integer.parseInt(profileinfoService.Getmatid(matrial));
		System.out.println("matrial id"+matrial); 
		model.put("matrialdetails", prepareMatrimonyBean(profileinfoService.getMatrimonyvalue(matrialid)));
		System.out.println("matrial id"+matrial); 
		
	}
	
	 if("failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
			
		{
			
		} 
		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));
			System.out.println("BasicDetailid"+BasicDetailid); 
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));
			System.out.println("BasicDetailid"+BasicDetailid); 
			
    	}
		IndSignupModel individualname = new IndSignupModel();
		int name= Integer.parseInt(i);
		System.out.println("summary id"+name); 
		
		
		model.put("regname", prepareBean(signupService.getname(name)));
     	return new ModelAndView("individualProfile", model);
}

		

private Socialacc prepareModel(SocialaccBean SocialaccBean) {
	Socialacc socialacc = new Socialacc();
	socialacc.setSocial_det_id(SocialaccBean.getIdsocialaccountdetail());
	socialacc.setWebsite_link(SocialaccBean.getSocialwebsitelink());
	socialacc.setWebsite_name(SocialaccBean.getSocialwebsitename());
		return socialacc;
}	


@RequestMapping(value ="/landpagesummarydetail1", method = RequestMethod.POST)
public ModelAndView savelandpage(HttpSession session,
		@ModelAttribute("command") SummarydetailBean SummarydetailBean,
		BindingResult result) {

        String ind_kyc_id=(String)session.getAttribute("Ind_id");
	    System.out.println("landpagesummarydetail1 add  contkycid"+SummarydetailBean.getLandpagedescription());
	    
	    System.out.println("ind_kyc_id"+ind_kyc_id);
	 	Summarydetail summarydetail = new Summarydetail();
		summarydetail.setInd_kyc_id(ind_kyc_id);
		summarydetail.setLandpagedescription(SummarydetailBean.getLandpagedescription());
	
	profileinfoService.addSummarydetail(summarydetail);
	return new ModelAndView("redirect:/landsum.html");
}

//save summary detail
@RequestMapping(value ="/summarydetail", method = RequestMethod.POST)
public ModelAndView saveMatrial(HttpSession session,
		@ModelAttribute("command") SummarydetailBean SummarydetailBean,
		BindingResult result) {
	
    String ind_kyc_id=(String)session.getAttribute("Kyc_id");
	 System.out.println("savebasicdetail contkycid"+ind_kyc_id);
				
		IndSignupModel individual = new IndSignupModel();
		individual.setKycid(ind_kyc_id);
		String i=signupService.getInd_id(individual);
		System.out.println("reg_id"+i); 
		String description=profileinfoService.getdescription(ind_kyc_id);
		Summarydetail summarydetail = new Summarydetail();
		summarydetail.setSum_det_id(SummarydetailBean.getIdsummarydetailBean());
		
		summarydetail.setInd_kyc_id(i);
		summarydetail.setLandpagedescription(description);
	
	profileinfoService.addSummarydetail(summarydetail);
	return new ModelAndView("redirect:/help4.html");
}


	@RequestMapping(value = "/mynotificationsDetails", method = RequestMethod.GET)
	public ModelAndView getNotificationDetails(HttpSession session,HttpServletRequest request)
	{
		session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null)
		{			
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return new ModelAndView("individualPage");	
		}
		else if(((String)session.getAttribute("Ind_id")).equals(null))
		{
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return  new ModelAndView("individualPage");	
		}
		else
		{	
			String kyc_id = (String)session.getAttribute("Kyc_id");		
			
			String indNotify = profileinfoService.getIndNotify(kyc_id);		
	        request.setAttribute("IndNotify",indNotify);
	        
	        int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
	        Map<String, Object> model = new HashMap<String, Object>();
	        
	        model.put("uti2IndNotification",profileinfoService.getuti2IndNotification(kyc_id));
	        
			model.put("kycNotification", prepareKYCNotificationBean(profileinfoService.getNotificationDetails(ind_id)));
	        
			int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));	
			
			request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
					
			return new ModelAndView("individual_notification",model);
		}
	}

	 private List<KycNotificationBean> prepareKYCNotificationBean(List<KycNotificationModel> notifyModels) {
     	
        	
     	List<KycNotificationBean> bean = null;
     	
		if (notifyModels != null && !notifyModels.isEmpty()) {
			bean = new ArrayList<KycNotificationBean>();
			KycNotificationBean beans = null;

			for (KycNotificationModel notifyModel : notifyModels) {
             
				beans = new KycNotificationBean();
             
				beans.setNotification_id(notifyModel.getNotification_id());
				beans.setNotification_subject(notifyModel.getNotification_subject());
				beans.setNotification_date(notifyModel.getNotification_date());
				beans.setNotification_message(notifyModel.getNotification_message());
								
				bean.add(beans);
			}
		}
 		return bean;
 	} 
	
	
	 @RequestMapping(value = "/getNotifyMessage", method = RequestMethod.POST)
	 public @ResponseBody String getNotifyMessage(HttpServletRequest request) 
	 {
		 int notifyId = Integer.parseInt(request.getParameter("notify_Id"));
		 String searchResult = profileinfoService.getMessagefromNotifyId(notifyId);
		 return searchResult;
	 }
	
	@RequestMapping(value = "/myvisitors", method = RequestMethod.POST)
	public ModelAndView getMyVisitors(HttpServletRequest request,HttpSession session) 
	{		
		Profileupdatemodel profilenotification = new Profileupdatemodel();
		String ind_kyc_id = (String)session.getAttribute("Kyc_id");
		profilenotification.setKyc_id(ind_kyc_id);
				
		request.setAttribute("visiterProfileDetails",profileinfoService.profilevisitors(profilenotification));
			
		return new ModelAndView("myvisitors");				
	}

	@RequestMapping(value = "/myvisitors", method = RequestMethod.GET)
	public String getMyVisitorsMain(HttpServletRequest request,HttpSession session) 
	{
		session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null)
		{			
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return "individualPage";	
		}
		else if(((String)session.getAttribute("Ind_id")).equals(null))
		{
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return  "individualPage";	
		}
		else
		{	
			Profileupdatemodel profilenotification = new Profileupdatemodel();
			String ind_kyc_id = (String) session.getAttribute("Kyc_id");
			profilenotification.setKyc_id(ind_kyc_id);
					
			request.setAttribute("visiterProfileDetails",profileinfoService.profilevisitors(profilenotification));
			
			int ind_id = Integer.parseInt((String)session.getAttribute("Ind_id"));
			
			request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
					
			return "individualAlert";		
		}
	}
	
	@RequestMapping(value = "/myactivities", method = RequestMethod.POST)
	public ModelAndView getbody11111(HttpSession session,
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
		
		Profileupdatemodel profilenotification=new Profileupdatemodel();
	    String ind_kyc_id=(String)session.getAttribute("Kyc_id");
	    profilenotification.setKyc_id(ind_kyc_id);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("profilenotification", prepareProfileupdateModel(profileinfoService.profileactivities(profilenotification)));
		
	 	return new ModelAndView("myactivities",model);
	}

	private List<Profileupdatebean> prepareProfileupdateModel(List<Profileupdatemodel> profilenotification) {
	 	
	 	List<Profileupdatebean> bean = null;
	 	
		if (profilenotification!= null && !profilenotification.isEmpty()) {
			bean = new ArrayList<Profileupdatebean>();
			Profileupdatebean beans = null;
	
			for (Profileupdatemodel docdetail : profilenotification) {
	         beans = new Profileupdatebean();
				beans.setProfileupdatedetail(docdetail.getProfileupdatedetail());
				beans.setProfileupdatetime(docdetail.getProfileupdatetime());
				beans.setType_of_alert(docdetail.getType_of_alert());
				beans.setKyc_id(docdetail.getKyc_id());
				beans.setViewer_kyc_id(docdetail.getViewer_kyc_id());
				beans.setMore_about_it(docdetail.getMore_about_it());			
				bean.add(beans);
			}
		}
			return bean;
	} 










    @RequestMapping(value = "/setting", method = RequestMethod.GET)
	public String getSettingMain(HttpSession session,HttpServletRequest request) {
		
    	session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null)
		{			
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return "individualPage";	
		}
		else if(((String)session.getAttribute("Ind_id")).equals(null))
		{
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return "individualPage";	
		}
		else
		{	
			String ind_kyc_id=(String)session.getAttribute("Kyc_id");
			int ind_id = profileinfoService.findInd_id(ind_kyc_id);
			
			int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));
			
			request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
					
		 	return "individualSetting";
		}
	}

	@RequestMapping(value = "/setting", method = RequestMethod.POST)
	public ModelAndView getSetting(HttpSession session,HttpServletRequest request) {
		
		String ind_kyc_id=(String)session.getAttribute("Kyc_id");
		int ind_id = profileinfoService.findInd_id(ind_kyc_id);
		
	 	return new ModelAndView("setting");
	}

	
	@RequestMapping(value = "/visibility/setting", method = RequestMethod.POST)
	public ModelAndView getVisibilitySetting(HttpSession session,HttpServletRequest request) {
		
		String ind_kyc_id=(String)session.getAttribute("Kyc_id");
		int ind_id = profileinfoService.findInd_id(ind_kyc_id);
		
		String reg_Visibility = profileinfoService.getRegVisisbility(ind_id);
		String basic_visibility = profileinfoService.getBasicVisisbility(ind_id);
		String family_visibility = profileinfoService.getFamilyVisisbility(ind_id);
		String social_visibility = profileinfoService.getSocialVisisbility(ind_id);
		
		request.setAttribute("reg_Visibility", reg_Visibility);
		request.setAttribute("basic_visibility", basic_visibility);
		request.setAttribute("family_visibility", family_visibility);
		request.setAttribute("social_visibility", social_visibility);
		
		
	 	return new ModelAndView("visibilitySetting");
	}

	@RequestMapping(value = "/indVisibilitySetting", method = RequestMethod.GET)
	public String indVisibilitySetting(HttpSession session,HttpServletRequest request) {

		session = request.getSession(false);
		
		if (session.getAttribute("Ind_id") == null)
		{			
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return "individualPage";	
		}
		else if(((String)session.getAttribute("Ind_id")).equals(null))
		{
			 request.setAttribute("kycdocumentList", docservice.getKYCComp());
			 request.setAttribute("status","Your session has expired please login to use the portal !");
			
			 session.removeAttribute("Ind_id");
			 //session.invalidate();
			 
			 return  "individualPage";	
		}
		else
		{	
						
			String ind_kyc_id=(String)session.getAttribute("Kyc_id");
			int ind_id = profileinfoService.findInd_id(ind_kyc_id);
			
			String reg_Visibility = profileinfoService.getRegVisisbility(ind_id);
			String basic_visibility = profileinfoService.getBasicVisisbility(ind_id);
			String family_visibility = profileinfoService.getFamilyVisisbility(ind_id);
			String social_visibility = profileinfoService.getSocialVisisbility(ind_id);
			
			request.setAttribute("reg_Visibility", reg_Visibility);
			request.setAttribute("basic_visibility", basic_visibility);
			request.setAttribute("family_visibility", family_visibility);
			request.setAttribute("social_visibility", social_visibility);
			
			request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(ind_id));
			
			return "individualVisibilitySetting";
		}

	}
	

	@RequestMapping(value = "/myprofilecenter", method = RequestMethod.POST)
	public ModelAndView getbody(HttpSession session,
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
	
		String ind_kyc_id=(String)session.getAttribute("Kyc_id");
		
		IndSignupModel individual = new IndSignupModel();
		individual.setKycid(ind_kyc_id);
		String i=signupService.getInd_id(individual);
		
		Summarydetail summary=new Summarydetail();
		summary.setInd_kyc_id(i);
		
		Matrial matrial=new Matrial();
		matrial.setInd_kyc_id(i);
		BasicDetail BasicDetail=new BasicDetail();
		BasicDetail.setInd_kyc_id(i);
		Map<String, Object> model = new HashMap<String, Object>();
				
		if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
		{			
			int fam=Integer.parseInt(profileinfoService.Getfamid(i));		
		    model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
		}					
		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
		{			
		int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));		
		model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));	  			
		}
				
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{
			int su= Integer.parseInt(profileinfoService.Getregid(summary));			
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));		
		}
			  
		if(!"failurematrimony".equals(profileinfoService.Getmatid(matrial))) 
		{
			int matrialid= Integer.parseInt(profileinfoService.Getmatid(matrial));			
			model.put("matrialdetails", prepareMatrimonyBean(profileinfoService.getMatrimonyvalue(matrialid)));
			
		}
		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));				
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));
			
    	}
			IndSignupModel individualname = new IndSignupModel();
			int name= Integer.parseInt(i);
			
			model.put("regname", prepareBean(signupService.getname(name)));
	         
		return new ModelAndView("myprofilecenter", model);
	}







	@RequestMapping(value = "/helpviewprofile", method = RequestMethod.GET)
	public ModelAndView getbodyview(HttpSession session,
			@ModelAttribute("command") EmployeeBean employeeBean,
			BindingResult result) {
	
		String ind_kyc_id=(String)session.getAttribute("Kyc_id");
		
		IndSignupModel individual = new IndSignupModel();
		individual.setKycid(ind_kyc_id);
		String i=signupService.getInd_id(individual);
		
		Summarydetail summary=new Summarydetail();
		summary.setInd_kyc_id(i);
		
		Matrial matrial=new Matrial();
		matrial.setInd_kyc_id(i);
		BasicDetail BasicDetail=new BasicDetail();
		BasicDetail.setInd_kyc_id(i);
		Map<String, Object> model = new HashMap<String, Object>();
			
		if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
		{			
			int fam=Integer.parseInt(profileinfoService.Getfamid(i));	
		    model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
		}
	
		if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
		{			
			int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));	
			model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));  		
		}
		
		if(!"failure".equals(profileinfoService.Getregid(summary))) 
		{
			int su= Integer.parseInt(profileinfoService.Getregid(summary));		
			model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su))); 		
		}
		
		if(!"failurematrimony".equals(profileinfoService.Getmatid(matrial))) 
		{
			int matrialid= Integer.parseInt(profileinfoService.Getmatid(matrial));
			model.put("matrialdetails", prepareMatrimonyBean(profileinfoService.getMatrimonyvalue(matrialid)));		
		}
			
		if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
		{
			int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));
			model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));		
		}
			IndSignupModel individualname = new IndSignupModel();
			int name= Integer.parseInt(i);
			model.put("regname", prepareBean(signupService.getname(name)));
	         
		return new ModelAndView("myprofilecenter", model);
	}






private List<DocumentDetailsBean> prepareDocumentDetail(List<DocumentDetailModel> docdetails) {
 	
 
 	List<DocumentDetailsBean> bean = null;
 	
	if (docdetails != null && !docdetails.isEmpty()) {
		bean = new ArrayList<DocumentDetailsBean>();
		DocumentDetailsBean beans = null;

		for (DocumentDetailModel docdetail : docdetails) {
         beans = new DocumentDetailsBean();
			
			beans.setDoc_name(docdetail.getDoc_name());
			beans.setDocs_data(docdetail.getDocs_data());
			bean.add(beans);
		}
	}
		return bean;
	} 
 
	
@RequestMapping(value = "/editprofile", method = RequestMethod.POST)
public ModelAndView getbody1(HttpSession session,
		@ModelAttribute("command") EmployeeBean employeeBean,
		BindingResult result) {

		String ind_kyc_id=(String)session.getAttribute("Kyc_id");
		IndSignupModel individual = new IndSignupModel();
		individual.setKycid(ind_kyc_id);
		String i=signupService.getInd_id(individual);
		
		Summarydetail summary=new Summarydetail();
		summary.setInd_kyc_id(i);
		
		Matrial matrial=new Matrial();
		matrial.setInd_kyc_id(i);
		BasicDetail BasicDetail=new BasicDetail();
		BasicDetail.setInd_kyc_id(i);
		Map<String, Object> model = new HashMap<String, Object>();
			
   
	if (!"failurefamily".equals(profileinfoService.Getfamid(i))) 
	{			
		int fam=Integer.parseInt(profileinfoService.Getfamid(i));
	    model.put("familyvalue", prepareEmployeeBean1(profileinfoService.getfamilyvalue(fam)));
	}
	
	if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
	{			
		int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));
	    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam))); 	
	}
	if(!"failure".equals(profileinfoService.Getregid(summary))) 
	{
		int su= Integer.parseInt(profileinfoService.Getregid(summary));
		model.put("Summarydetails", prepareSummaryBean(profileinfoService.getSummaryvalue(su)));		
	}	   
	if(!"failurematrimony".equals(profileinfoService.Getmatid(matrial))) 
	{
		int matrialid= Integer.parseInt(profileinfoService.Getmatid(matrial));
		model.put("matrialdetails", prepareMatrimonyBean(profileinfoService.getMatrimonyvalue(matrialid)));		
	}
	
	if(!"failurebasic".equals(profileinfoService.Getbasicid(BasicDetail))) 
	{
		int BasicDetailid= Integer.parseInt(profileinfoService.Getbasicid(BasicDetail));
		model.put("basicdetails", prepareBasicBean(profileinfoService.getBasicDetailvalue(BasicDetailid)));
    }
		IndSignupModel individualname = new IndSignupModel();
		int name= Integer.parseInt(i);		
		
		model.put("regname", prepareBean(signupService.getname(name)));
     	return new ModelAndView("editprofile", model);
}




public ModelAndView getsocialacc(HttpSession session,@ModelAttribute("command") SocialaccBean SocialaccBean,BindingResult result)
{
	String ind_kyc_id=(String)session.getAttribute("Kyc_id");
	
	IndSignupModel individual = new IndSignupModel();
	individual.setKycid(ind_kyc_id);
	String i=signupService.getInd_id(individual);
	
	
	if ("failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
	{
		return new ModelAndView("left_menu0");
	} 
	
	if (!"failuresocialacc".equals(profileinfoService.Getsocialaccid(i))) 
	{			
	int fam=Integer.parseInt(profileinfoService.Getsocialaccid(i));
	
	Map<String, Object> model = new HashMap<String, Object>();
    model.put("socialaccvalue", prepareSocialaccBean(profileinfoService.getsocialaccvalue(fam)));
    return new ModelAndView("left_menu0",model);
		
	}
	return new ModelAndView("left_menu0");
}



private IndSignupBean prepareIndSignupBean(IndSignupModel indSignupModel) {
	IndSignupBean bean = new IndSignupBean();
	  
	bean.setKycid(indSignupModel.getKycid());
	return bean;
}


@RequestMapping(value = "/changepassword1", method = RequestMethod.POST)
public @ResponseBody String saveFamilyDetails1111(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
	
	String OldPassword = request.getParameter("OldPassword");
	String Newpass = request.getParameter("newpassword");
	String conpass = request.getParameter("conpassword");
	String kycid=(String) session.getAttribute("Kyc_id");
	
	
	String i =(String)session.getAttribute("Ind_id");
	
	IndSignupModel indsignupmodel=new IndSignupModel();
	indsignupmodel.setPassword(OldPassword);
	indsignupmodel.setIndid((Integer.parseInt(i)));
	
	String checkpassword = signupService.getcheckpassword(indsignupmodel);
	
	if (PasswordEncryptionDecryption.getValidatePassword(OldPassword, checkpassword).equals("True")) {
		
		String chnagePassword = PasswordEncryptionDecryption.getEncryptPassword(Newpass);
		
		indsignupmodel.setPassword(chnagePassword);
		signupService.getchangepassword(indsignupmodel);
		
		    Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("password changing");
			Profileupdatemodel.setType_of_alert("notification update");
			Profileupdatemodel.setKyc_id(kycid);
			Profileupdatemodel.setMore_about_it("changed the password");
			Profileupdatemodel.setProfileupdatetime(new Date());
			profileinfoService.profileupdate(Profileupdatemodel); 
		
			return "successfull";	
	}
	
	else
	{			
		return "NotMatched";
	}

}

	



@RequestMapping(value = "/visibilitysetting", method = RequestMethod.POST)
@ResponseBody public String savesettings(HttpServletRequest request,HttpSession session) {
	
	
	String ind_kyc_id=(String)session.getAttribute("Kyc_id");
	
			
	String status=request.getParameter("visible");
	
	String basic_vis = request.getParameter("basic_vis");
	String family_vis = request.getParameter("family_vis");
	String social_vis = request.getParameter("social_vis");
	
	
	
	String i =(String)session.getAttribute("Ind_id");
	
	IndSignupModel indsignupmodel=new IndSignupModel();
	indsignupmodel.setVisibility(status);
	indsignupmodel.setIndid(Integer.parseInt(i));
			
	signupService.savestatusreg(indsignupmodel);
	
	profileinfoService.updateRegVisibility(Integer.parseInt(i),status);
	profileinfoService.setBasicVisibility(Integer.parseInt(i),basic_vis);
	profileinfoService.setFamilyVisibility(Integer.parseInt(i),family_vis);
	profileinfoService.setSocialVisibility(Integer.parseInt(i),social_vis);
	

    Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel();
	Profileupdatemodel.setProfileupdatedetail("Visibility setting has been updated.");
	Profileupdatemodel.setType_of_alert("Notification update");
	Profileupdatemodel.setKyc_id(ind_kyc_id);
	Profileupdatemodel.setMore_about_it("Private,Public setting has changed");
	Profileupdatemodel.setProfileupdatetime(new Date());
	profileinfoService.profileupdate(Profileupdatemodel); 
	
	
	
	return "Changed Successfully!";
}



@RequestMapping(value="/searchouterpage", method = RequestMethod.POST)
public @ResponseBody String searchouterpage(HttpServletRequest request, HttpServletResponse response){
	
	IndSignupModel searchhomepage=new IndSignupModel();
		
	
	String searchvalue=request.getParameter("searchvalue");
	
    searchhomepage.setFirstname(searchvalue);
    
    return prepareIndSignupModel(profileinfoService.searchhomepage(searchhomepage)).toString();
       		
	}

@RequestMapping(value="/searchouterpageUti", method = RequestMethod.POST)
public @ResponseBody String searchouterpageUti(HttpServletRequest request, HttpServletResponse response){
	
	IndSignupModel searchhomepage=new IndSignupModel();			
	String searchvalue=request.getParameter("searchvalue");
	
    searchhomepage.setFirstname(searchvalue);
    
    String uti_result =  profileinfoService.searchhomepageforUti(searchvalue);
    
    return uti_result;
       		
	}

		@RequestMapping(value="/newsLetter", method = RequestMethod.GET)
		public String newsLetter(){
						
			return "emailtemplate";
		}

		@RequestMapping(value="/search_Outerprofile1", method = RequestMethod.POST)
		public @ResponseBody String search_Outerprofile1(HttpServletRequest request, HttpSession session){
			
			IndSignupModel searchprofile=new IndSignupModel();	
			String searchedValue=request.getParameter("kycid");	
			searchprofile.setFirstname(searchedValue);	 						
			String searchedKYCInd = profileinfoService.getSearchKycInd(searchedValue);			
			
			return searchedKYCInd;
		}
		
		@RequestMapping(value="/search_Outerprofile", method = RequestMethod.POST)
		public ModelAndView search_Outerprofile(HttpServletRequest request, HttpSession session){
			
			IndSignupModel searchprofile=new IndSignupModel();	
			String searchedValue=request.getParameter("kycid");	
			searchprofile.setFirstname(searchedValue);	 						
			String searchedKYCInd = profileinfoService.getSearchKycInd(searchedValue);			
			
			request.setAttribute("searchedKYCInd", searchedKYCInd);
			
			return new ModelAndView("ind_search_result_before_login");
		}
		
		

		@RequestMapping(value="/indsearch_profile", method = RequestMethod.POST)
		public ModelAndView getSearchResultOfIndividual(HttpServletRequest request, HttpSession session){
			
			IndSignupModel searchprofile=new IndSignupModel();	
			String searchedValue=request.getParameter("kycid");	
			searchprofile.setFirstname(searchedValue);	 			
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			List<String> indSearchResult = prepareIndSignupModel(profileinfoService.searchprofile(searchprofile));
			
			model.put("indSearchResult", indSearchResult);
			
			request.setAttribute("indSearchResult", indSearchResult);
			
			return new ModelAndView("search_list_of_individual",model);			
		}
		
		@RequestMapping(value="/indsearchprofilefromUtilizer", method = RequestMethod.POST)
		public ModelAndView indSearchProfileFromUtilizer(HttpServletRequest request, HttpSession session){
			
			IndSignupModel searchprofile=new IndSignupModel();	
			String searchedValue=request.getParameter("kycid");	
			searchprofile.setFirstname(searchedValue);	 			
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			List<String> indSearchResult = prepareIndSignupModel(profileinfoService.searchprofile(searchprofile));
			
			model.put("indSearchResult", indSearchResult);
			
			request.setAttribute("indSearchResult", indSearchResult);
			
			return new ModelAndView("searchIndListFromUtilizer",model);			
		}
		
		
		@RequestMapping(value="/viewProfile", method = RequestMethod.POST)
		public @ResponseBody String add(HttpServletRequest request, HttpServletResponse response,HttpSession session){
			
			IndSignupModel viewmore=new IndSignupModel();						
			String kycid=request.getParameter("kycid");
			
			viewmore.setIndid(Integer.parseInt(kycid));
			
			int ind_kyc_id=Integer.parseInt(request.getParameter("kycid"));
			
			IndSignupModel getkycid=new IndSignupModel();
			getkycid.setIndid(ind_kyc_id);
			String ind_kyc_idvalue=profileinfoService.getkycid(ind_kyc_id);
					
			String ind_kyc_id1=(String)session.getAttribute("Kyc_id");
			    			    
			String firstname=profileinfoService.getfirstname(ind_kyc_id1);
			
			if(!ind_kyc_idvalue.equals(ind_kyc_id1))
			{
			    Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
				Profileupdatemodel.setProfileupdatedetail(firstname);
				Profileupdatemodel.setType_of_alert("visitor update");
				Profileupdatemodel.setKyc_id(ind_kyc_idvalue);
				Profileupdatemodel.setViewer_kyc_id(ind_kyc_id1);
				Profileupdatemodel.setMore_about_it(firstname+" has viewed your profile");
				Profileupdatemodel.setProfileupdatetime(new Date());
				profileinfoService.profileupdate(Profileupdatemodel); 
			}
			
			int ind_id = viewmore.getInd_id();
			String Basic = profileinfoService.getBasicDetail(ind_id);
		    String family = profileinfoService.getFamilyDetail(ind_id);
			String social = profileinfoService.getsocialdetails(ind_id);
			
			String reg_Visibility = profileinfoService.getRegVisisbility(ind_id);
			
			String basic_visibility = profileinfoService.getBasicVisisbility(ind_id);
			String family_visibility = profileinfoService.getFamilyVisisbility(ind_id);
			String social_visibility = profileinfoService.getSocialVisisbility(ind_id);
			
			if(!Basic.equals("NoData"))
			{
			     Basic = Basic.substring(1,Basic.length()-1);
			}
			if(!family.equals("NoData"))
			{
			    family = family.substring(1,family.length()-1);
			}
			if(!social.equals("NoData"))
			{
			    social = social.substring(1,social.length()-1);
			}
			
			//Basic = Basic.replaceAll(", ", ",");
			family = family.replaceAll(", ", ",");
			social = social.replaceAll(", ", ",");
			
			Basic = Basic.concat("$$"+basic_visibility);
			family = family.concat("$$"+family_visibility);
			social =  social.concat("$$"+social_visibility);
			
			String viewData = prepareIndSignupModelview(profileinfoService.viewmore(viewmore)).toString()+"#"+Basic+"#"+family+"#"+social;
			
			System.out.println("basic Detail with visibility "+viewData);
			
			//return prepareIndSignupModelview(profileinfoService.viewmore(viewmore)).toString();
			
			return viewData;
		}
			
    private List<String> preparereghistory(List<IndSignupModel> searchvalue) {
  	
	List<String> viewmore = new ArrayList<String>();
	
	
 	List<IndSignupBean> bean = null;
 	
	if (searchvalue != null && !searchvalue.isEmpty()) {
		bean = new ArrayList<IndSignupBean>();
		IndSignupBean beans = null;

		for (IndSignupModel  docdetail : searchvalue) {
            beans = new IndSignupBean();
            IndSignupModel searchpic=new IndSignupModel();
			beans.setFirstname(docdetail.getFirstname()); 
			 viewmore.add(beans.getFirstname());
			
			beans.setLastname(docdetail.getLastname());   
			 viewmore.add(beans.getLastname());
			 
			 beans.setMobileno(docdetail.getMobileno());   
			 viewmore.add(beans.getMobileno());
			
			beans.setCr_date(docdetail.getCr_date());
			 viewmore.add(beans.getCr_date().toString()
					 );
			beans.setVisibility(docdetail.getVisibility());
									
            searchpic.setIndid(beans.getIndid());
					          			
			bean.add(beans);
		}
	  }
	//return bean;
	return  viewmore;
	} 
							
private List<String> prepareIndSignupModelview(List<IndSignupModel> searchvalue) {
  	
	List<String> viewmore = new ArrayList<String>();
	
	
 	List<IndSignupBean> bean = null;
 	
	if (searchvalue != null && !searchvalue.isEmpty()) {
		bean = new ArrayList<IndSignupBean>();
		IndSignupBean beans = null;

		for (IndSignupModel  docdetail : searchvalue) {
            beans = new IndSignupBean();
            IndSignupModel searchpic=new IndSignupModel();
			beans.setIndid(docdetail.getIndid());
			viewmore.add(beans.getIndid().toString());
			
			beans.setFirstname(docdetail.getFirstname()); 
			viewmore.add(beans.getFirstname());
			
			beans.setKycid(docdetail.getKycid());   
			viewmore.add(beans.getKycid());
			
			beans.setCr_date(docdetail.getCr_date());
			beans.setVisibility(docdetail.getVisibility());
			
			beans.setMobileno(docdetail.getMobileno());
			viewmore.add(beans.getMobileno());
			
			beans.setEmailid(docdetail.getEmailid());
			viewmore.add(beans.getEmailid());
			
			if(beans.getCr_date()!=null) {
								   
				viewmore.add(beans.getCr_date().toString());
			} 
			if(beans.getCr_date()==null) { 
				
				   
				viewmore.add("");
			}
				
			
            searchpic.setIndid(beans.getIndid());
			
			if(!"noprofilepic".equals(profileinfoService.getprofilepic(searchpic)))
			{
			String profilepic=profileinfoService.getprofilepic(searchpic);
			
			beans.setPassword(profilepic);
			viewmore.add(beans.getPassword());
			}
			
			if("noprofilepic".equals(profileinfoService.getprofilepic(searchpic)))
			{
				viewmore.add("no image");				
			}
            
			
             
            beans.setVisibility(docdetail.getVisibility());
 			viewmore.add(beans.getVisibility());
             
			bean.add(beans);
		}
	  }
	//return bean;
	return  viewmore;
	}  

     @RequestMapping(value="/basicDetailHistory", method = RequestMethod.POST)
     public @ResponseBody String getBasicDetailHistory(HttpSession session)
     {   
    	 String kyc_id = (String)session.getAttribute("Kyc_id");
   		 int ind_id = profileinfoService.findInd_id(kyc_id);

    	 String basicHistory  =  profileinfoService.getBasicHistory(ind_id);
    	 return basicHistory;
     }
     
     @RequestMapping(value="/basicDetailHistoryfromUtilizer", method = RequestMethod.POST)
     public @ResponseBody String basicDetailHistoryfromUtilizer(HttpSession session,HttpServletRequest request)
     {   
    	 
   		 int ind_id = Integer.parseInt(request.getParameter("searchedIndId"));

    	 String basicHistory  =  profileinfoService.getBasicHistory(ind_id);
    	 return basicHistory;
     }
     
     @RequestMapping(value="/familyHistory", method = RequestMethod.POST)
     public @ResponseBody String getFamilyHistory(HttpSession session)
     {   
    	 String kyc_id = (String)session.getAttribute("Kyc_id");
   		 int ind_id = profileinfoService.findInd_id(kyc_id);

    	 String familyHistory  =  profileinfoService.getFamilyHistory(ind_id);
    	 return familyHistory;
     }
     
     
     @RequestMapping(value="/familyHistoryFromUtilizer", method = RequestMethod.POST)
     public @ResponseBody String familyHistoryFromUtilizer(HttpSession session,HttpServletRequest request)
     {   
    	
   		 int ind_id = Integer.parseInt(request.getParameter("searchedIndId"));

    	 String familyHistory  =  profileinfoService.getFamilyHistory(ind_id);
    	 return familyHistory;
     }
     
     
     @RequestMapping(value="/socialHistory", method = RequestMethod.POST)
     public @ResponseBody String getSocialHistory(HttpSession session)
     {   
    	 String kyc_id = (String)session.getAttribute("Kyc_id");
   		 int ind_id = profileinfoService.findInd_id(kyc_id);
   		 
    	 String socialHistory  =  profileinfoService.getsocialHistory(ind_id);
    	 
    	 return socialHistory;
     }
     
     @RequestMapping(value="/socialHistoryFromUtilizer", method = RequestMethod.POST)
     public @ResponseBody String socialHistoryFromUtilizer(HttpSession session,HttpServletRequest request)
     {   
    	 int ind_id = Integer.parseInt(request.getParameter("searchedIndId"));

    	 String socialHistory  =  profileinfoService.getsocialHistory(ind_id);
    	 
    	 return socialHistory;
     }
     
     @RequestMapping(value="/saveAboutUs", method = RequestMethod.POST)
     public @ResponseBody String saveAboutUs(HttpSession session,HttpServletRequest request) throws UnsupportedEncodingException
     {   
    	 String profile_data = "";
    	 
    	 String kyc_id = (String)session.getAttribute("Kyc_id");
   		 int ind_id = profileinfoService.findInd_id(kyc_id);
   		 String id = Integer.toString(ind_id);
   		
   		 String data = request.getParameter("textAreaContent");  	
	   	 	
   		 byte[] bytes = data.getBytes("UTF-8");	
		 double sizeOfTextInKB = bytes.length/1024.0;
		
		 System.out.println("Starting Data="+data);
		 
		 System.out.println("Compare Data="+data.contains("&lt;script")+"-"+data.replaceAll("&lt;script", " "));
		 
		 data = data.replaceAll("&lt;", ""); 
		 data = data.replaceAll("script", ""); 
		 data = data.replaceAll("&lt;script", ""); 		 
		 data = data.replaceAll("&lt;script&gt;", "");
		 data = data.replaceAll("&lt;/script&gt;", "");
		 
		 data = data.replaceAll("&gt;","");
		 
		 data = data.replaceAll("link", ""); 
		 data = data.replaceAll("&lt;link&gt;", " ");
		 data = data.replaceAll("&lt;/link&gt;", " ");		 		
		 data = data.replaceAll("&lt;link", " ");
				 
		 data = data.replaceAll("&lt;a&lt;", " ");
		 data = data.replaceAll("&lt;/a&lt;", " ");
		 data = data.replaceAll("resourceskyc_css", " ");
		 data = data.replaceAll("href", " ");
		 data = data.replaceAll("&lt;h2&lt;", " ");
		 data = data.replaceAll("&lt;/h2&lt;", " ");
		 data = data.replaceAll("src=resources/kyc_scripts/", " ");
		 data = data.replaceAll("type=text/javascript", " "); 
		 data = data.replaceAll("rel='stylesheet' href=resources/kyc_css/", " "); 
		 data = data.replaceAll("rel='stylesheet", " ");
		 data = data.replaceAll("Accordion.js", " ");
		 data = data.replaceAll("jquery-1.9.1.min.js", " ");
		 data = data.replaceAll("jquery-1.11.1.js", " ");
		 data = data.replaceAll("jquery.com.jqueryUi.js", " ");
		 data = data.replaceAll("resources/kycFileDirectory", " ");
		 data = data.replaceAll("resources/", " ");
		 data = data.replaceAll(".js", " ");
		 data = data.replaceAll(".css", " ");
		 
		 
		 System.out.println("Final Data="+data);
   		
   		 Summarydetail sumDetail = new Summarydetail();
   		
   		String sumId = profileinfoService.getSum_det_id(id);
   		
   		if(!"Not There".equals(sumId))
   		{   		   			
	   		int sum_id = Integer.parseInt(sumId);
	   		sumDetail.setSum_det_id(sum_id);	
	   		sumDetail.setInd_kyc_id(id);
	   		sumDetail.setLandpagedescription(data);
	   		sumDetail.setCr_date(new Date());
	   		
		    	if(profileinfoService.saveAboutUsInfo(sumDetail).equals("successeditfamily"))
		    	{
		    	    String Kyc_id= kyc_id;
					Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
					Profileupdatemodel.setProfileupdatedetail("About Us has been updated.");
					Profileupdatemodel.setProfileupdatetime(new Date());
					Profileupdatemodel.setType_of_alert("About us updated");
					Profileupdatemodel.setMore_about_it("About us updated");
				    Profileupdatemodel.setKyc_id(Kyc_id);
					profileinfoService.profileupdate(Profileupdatemodel);												
		    	}
			    	
	    	profile_data = profileinfoService.getAboutUsInfo(sumDetail);	
	    	
   		}
   		else
   		{   			   			
		   			sumDetail.setInd_kyc_id(id);
		   	   		sumDetail.setLandpagedescription(data);
		   	   		sumDetail.setCr_date(new Date());
		   	   		
			   	   	if(profileinfoService.saveAboutUsInfo(sumDetail).equals("successeditfamily"))
			    	{
			    	    String Kyc_id= Integer.toString(ind_id);
						Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
						Profileupdatemodel.setProfileupdatedetail("About Us has been updated.");
						Profileupdatemodel.setProfileupdatetime(new Date());
						Profileupdatemodel.setType_of_alert("About us updated");
						Profileupdatemodel.setMore_about_it("About us updated");
					    Profileupdatemodel.setKyc_id(Kyc_id);
						profileinfoService.profileupdate(Profileupdatemodel);															
			    	}
			   	   	
			   	 profile_data = profileinfoService.getAboutUsInfo(sumDetail);
			   	    	
		  }
		   		   		
    	 return profile_data;
     }
     
     @RequestMapping(value="/getAboutUsInfo", method = RequestMethod.POST)
     public @ResponseBody String getAboutUsInfo(HttpServletRequest request,HttpSession session)
     {
         String ind_id = (String)session.getAttribute("Ind_id");
         System.out.println("Own IndId"+ind_id);
         Summarydetail sumDetail = new Summarydetail();
         sumDetail.setInd_kyc_id(ind_id);
         String profile = profileinfoService.getAboutUsInfo(sumDetail);
                     			
    	 return profile;
     }
     
     @RequestMapping(value="/getOtherAboutUsInfo", method = RequestMethod.POST)
     public @ResponseBody String getOtherAboutUsInfo(HttpServletRequest request,HttpSession session)
     {
         String ind_id = request.getParameter("ind_kyc");
         Summarydetail sumDetail = new Summarydetail();
         sumDetail.setInd_kyc_id(ind_id);
         String profile = profileinfoService.getAboutUsInfo(sumDetail);
                     			
    	 return profile;
     }
     
     @RequestMapping(value="/profilePhoto",method=RequestMethod.POST)
     public ModelAndView getProfilePictures(HttpServletRequest request,HttpSession session)
     {
    	 String kyc_id = (String)session.getAttribute("Kyc_id");
   		 int ind_id = profileinfoService.findInd_id(kyc_id);   		 
    	 String profilePhoto = profileinfoService.getProfilePhoto(ind_id);
    	 String id = Integer.toString(ind_id);
    	 
    	 String historyDitails = id+"@@@"+profilePhoto;
    	 request.setAttribute("profilePhotoHistory", historyDitails);
    	   	 
    	 return new ModelAndView("ind_profile_photo_history");    	   	 
     }
     
     @RequestMapping(value="/updateProfilePhoto",method=RequestMethod.POST)
     public @ResponseBody String setProfilePicture(HttpServletRequest request,HttpSession session)
     {    	 
    	 String picName = request.getParameter("pictureName");
    	 String kyc_id = (String)session.getAttribute("Kyc_id");
   		 int ind_id = profileinfoService.findInd_id(kyc_id);   		 
   		
    	 String status = profileinfoService.updateProfilePic(picName,ind_id);
    	 if(status.equals("success"))
    	 {
    	    String Kyc_id= kyc_id;
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("uploaded a Profile Picture");
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("Profile Picture updated");
			Profileupdatemodel.setMore_about_it("Profile Pictur uploaded");
		    Profileupdatemodel.setKyc_id(Kyc_id);
			profileinfoService.profileupdate(Profileupdatemodel);
    	 }
    	 
    	 return status;
     }
     
     @RequestMapping(value="/profileDetail",method=RequestMethod.POST)
     public @ResponseBody String getViewedProfileDetail(HttpServletRequest request)
     {
    	 String kycId = request.getParameter("kycid");    	
    	 int ind_id = profileinfoService.findInd_id(kycId);
    	 
    	 String pictureName = profileinfoService.getLatestPicName(ind_id);
    	 String visibility = profileinfoService.getRegVisisbility(ind_id);
    	 String ind_Details = profileinfoService.getIndividualDetails(ind_id);
    	 int year = profileinfoService.getYear(ind_id);
    	 
    	 String details = "";
    	 details = year+"$"+ind_id+"$"+pictureName+"$"+visibility+"$"+ind_Details;
    	 
    	 return details;
     }
     
     @RequestMapping(value="/validateEmailId",method=RequestMethod.POST)
     public @ResponseBody String validateEmailId(HttpServletRequest request)
     {
    	 String emailId = request.getParameter("emailId");
    	 String emailStatus = profileinfoService.getValidateEmailId(emailId);
    	 
    	 return emailStatus;
     }
          
     @RequestMapping(value="/forgetpwValidateEmailId",method=RequestMethod.POST)
     public @ResponseBody String forgetpwValidateEmailId(HttpServletRequest request)
     {
    	 String emailId = request.getParameter("emailId");
    	 String emailStatus = profileinfoService.getValidateEmailIdForgotPassword(emailId);
    	 return emailStatus;
     }
     
     @RequestMapping(value="/validatemobileno",method=RequestMethod.POST)
     public @ResponseBody String validateMobileNo(HttpServletRequest request)
     {
    	 String mobileNo = request.getParameter("mobileno");
    	 String mobileNoStatus = profileinfoService.getValidateMobileNo(mobileNo);
    	 
    	 return mobileNoStatus;
     }
     
     
     /*@Autowired
     private JavaMailSender mailSender;
 	*/
     
     @RequestMapping(value="/formatPassword",method = RequestMethod.POST)
	 public @ResponseBody String resetPassword(HttpServletRequest request) throws IOException, IllegalWriteException, AuthenticationFailedException, SendFailedException
		{
		   String resetStatus = "";
		   String emailId = request.getParameter("emailsignup");
		   
		   String resetVariable = profileinfoService.getIndividualIdVaiEmailId(emailId);
		   		   
		   String changedUrl = PasswordEncryptionDecryption.getEncryptPassword(resetVariable);
		   String encryptedUrl = changedUrl.replaceAll("[^\\p{Alpha}\\p{Digit}]+","");
		   
		   System.out.println("Changed Url="+changedUrl);
		   System.out.println("Encrypted Url="+encryptedUrl);
		   
		   int ind_id = profileinfoService.getIndividualIdVaiVisibility(resetVariable);
		   profileinfoService.updateVisibilityUrl(ind_id,resetVariable,encryptedUrl);
		   
		    ResourceBundle loggerFileDirectory = ResourceBundle.getBundle("resources/kycFileDirectory");
		  
		    ResourceBundle resource = ResourceBundle.getBundle("resources/resetPasswordSendingMail");
		    		    
		     String headingImage=resource.getString("headingImage");
	    	 String belowleftImage=resource.getString("belowleftImage");
	    	 String belowrightImage=resource.getString("belowrightImage");
	    	 String mailicon=resource.getString("mailicon");
	    	 String contacticon=resource.getString("contacticon");
	    	 
	    	 String emailHeading=resource.getString("emailHeading");
	    	 String contactNo=resource.getString("contactNo");
	    	 String contactEmail=resource.getString("contactEmail");
	    	 
	    	 String subject=resource.getString("signupsubject");
			 String messageBody=resource.getString("signupmessage");		
			 String urlLinktoActivate = resource.getString("aftermessageextraline");
			 
	    	 String copyright=resource.getString("copyright");
	    	 String termscondition=resource.getString("termscondition");
	    	 String privacyPolicy=resource.getString("privacyPolicy");
	    	 
	    	 String facebookicon=resource.getString("facebookicon");
	    	 String twittericon=resource.getString("twittericon");
	    	 String googleicon=resource.getString("googleicon");
	    	 String linkedicon=resource.getString("linkedicon");
	    	 
	    	 String facebookLink=resource.getString("facebookLink");
	    	 String twitterLink=resource.getString("twitterLink");
	    	 String googlePlus=resource.getString("googlePlus");
	    	 String linkedIn=resource.getString("linkedIn");
			
	    	 String clenseeLogo = resource.getString("clenseeLogo");
			 String signupwelcome = resource.getString("signupwelcome");
			 String clickhere = resource.getString("clickhere");
			 
		    String commonUrl = resource.getString("commonUrl");
		  
	        String target = "_blank";
		    		   	        	      
			String url = ""+commonUrl+"resetPassword.html?wdvrgtqsusb="+encryptedUrl+"";
			    		       
		    StringBuilder text = new StringBuilder();
	    
		    /* Sending Emails to Register user */
		       
	        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
	        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
	       
	        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
	       
	        /* Header */
	        
	        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
	        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
	        text.append("</tr></table></td></tr></table>");
	       
	        /* End of Header */
	        
	        /* Header-2 */
	        
	        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
	        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
	        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
	        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
	        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
	        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
	        text.append("</p></td></tr></table></td></tr></table>");
	        
	        /* End of Header-2 */
	        
	        
	        /* BODY PART */
	        
	        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
	        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
	        text.append("<h3 style='color: #004879;'>Dear Clensee user,</h3>");
	        text.append("<p style='text-align: justify'>"+signupwelcome+"</p>");
	        text.append("<p style='text-align: justify'>"+messageBody+"</p><br><p>");
	        text.append(""+urlLinktoActivate+"<br></p><br>");
	        text.append("<a href="+url+" target="+target+" style='cursor:pointer;margin-left:224px;'><img src=" +clickhere+ " /></a>");
	        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
	        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
	        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
	        text.append("</tr></table></td></tr>");
	        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
	        text.append("</table> ");
	        
	        
	        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
	        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
	        
	        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
	        
	        text.append("<tr><td>");
	        
	        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
	        text.append("</td></tr></table>");
	       
	        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
	        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
	        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
	        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
	        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
	        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
	        
	        
	        text.append("</tr>");
	        text.append("</table></td></tr></table></td></tr></table>");
	        text.append("</body>");
	        text.append("</html>");
	       
	        /* END OF BODY PART */
	        
	        
	        /* End of Sending Emails to Register user */
		    
		  
	        SendEmailUtil obj = new SendEmailUtil();
	        
			resetStatus = obj.getEmailSent(emailId,subject,text.toString());
			
			request.setAttribute("contactNotifyMessage","You have successfully submited the request.");
	     
	    	EmailSentStatusModel emailReport = new EmailSentStatusModel();
	    	
	    	if(resetStatus.equals("success"))
	    	{		    		
	    		emailReport.setEmail_id(emailId);
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("0");
	    	}
	    	else
	    	{
	    		emailReport.setEmail_id(emailId);
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("1");	    			    		
	    	}
	    	
	    	profileinfoService.setEmailStatusReport(emailReport);
	    	
	    	// Store Reset password Info in DB
			
			 ResetPasswordModel resetPW = new ResetPasswordModel();
		        
		     resetPW.setInd_visibility_code(encryptedUrl);
		     resetPW.setInd_status("0");
		     resetPW.setRequested_date(new Date());
		       
		     profileinfoService.saveResetPasswordModel(resetPW);
		     
		    // EOC Store Reset password Info in Db
		     
		   return resetStatus;
		}
		
     
     @RequestMapping(value="/resetPassword",method=RequestMethod.GET)
     public ModelAndView resetPasdword(HttpServletRequest request)
     {   	
    	 String reserVariable = request.getParameter("wdvrgtqsusb");    	     	  	     	   			 
    	 request.setAttribute("uniqueVariable", reserVariable);   	 
    	 String checkStatus = signupService.getCheckStatus(reserVariable);    	 
    	 request.setAttribute("resetStatus", checkStatus);   	 
    	 return new ModelAndView("resetPassword");
     } 
     
     @RequestMapping(value="/indResetPassword",method=RequestMethod.POST)
     public @ResponseBody String indResetPassword(HttpServletRequest request,HttpSession session)
     {
    	 String uniqueVariable = request.getParameter("uniqueVariable");
    	 String password = request.getParameter("newPassword");
    	 String encryptedPassword = PasswordEncryptionDecryption.getEncryptPassword(password);
    	 
    	 IndSignupModel indsignupmodel=new IndSignupModel();
    	 indsignupmodel.setPassword(encryptedPassword);
    	 
    	 int Ind_id = signupService.getindividualIndId(uniqueVariable);
    	 indsignupmodel.setIndid(Ind_id);
    	 
    	 String status = signupService.getchangepassword(indsignupmodel);
    	  
    	  // Tracking Activity Details
	        
	        String Kyc_id= (String)session.getAttribute("Kyc_id");
			Profileupdatemodel  Profileupdatemodel= new Profileupdatemodel(); 
			Profileupdatemodel.setProfileupdatedetail("You have changed your password");
			Profileupdatemodel.setProfileupdatetime(new Date());
			Profileupdatemodel.setType_of_alert("Password Updated");
			Profileupdatemodel.setMore_about_it("Event done in Setting");
		    Profileupdatemodel.setKyc_id(Kyc_id);
			profileinfoService.profileupdate(Profileupdatemodel);

         // End Of Tracking Activity Details
			
					
    	 signupService.getDeleteResetPaswordInfo(uniqueVariable);
    	 
    	 return status;
     }
     
     @RequestMapping(value="/contactusbeforeLogin",method=RequestMethod.GET)
     public String getContactUsBeforeLogin(HttpServletRequest request,HttpSession session)
     {
    	
    	 return "contactUsBeforeLogin";
     }
     
        
     @RequestMapping(value="/contactus",method=RequestMethod.GET)
     public String getContactUs(HttpServletRequest request,HttpSession session)
     {
    	 int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
 		 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
 		 
    	 return "kyc_contact_us";
     }
     
     @RequestMapping(value="/getNotifyOfContactUs",method=RequestMethod.POST)
     public @ResponseBody String getNotifyOfContactUs(HttpServletRequest request,HttpSession session,@ModelAttribute("command")ContactUsBean contactBean, BindingResult result)throws Exception
     {   	

    	 String resultStatus = "";
    	 String target = "";
    	     	 
    	 ResourceBundle resource = ResourceBundle.getBundle("resources/ContactUsNotifyMessage");
		
    	 String headingImage=resource.getString("headingImage");
    	 String belowleftImage=resource.getString("belowleftImage");
    	 String belowrightImage=resource.getString("belowrightImage");
    	 String mailicon=resource.getString("mailicon");
    	 String contacticon=resource.getString("contacticon");
    	 
    	 String emailHeading=resource.getString("emailHeading");
    	 String contactNo=resource.getString("contactNo");
    	 String contactEmail=resource.getString("contactEmail");
    	 
    	 String subject=resource.getString("notifysubject");
	     String messageBody=resource.getString("contactNotifymessage");		
		
    	 String copyright=resource.getString("copyright");
    	 String termscondition=resource.getString("termscondition");
    	 String privacyPolicy=resource.getString("privacyPolicy");
    	 
    	 String facebookicon=resource.getString("facebookicon");
    	 String twittericon=resource.getString("twittericon");
    	 String googleicon=resource.getString("googleicon");
    	 String linkedicon=resource.getString("linkedicon");
    	 
    	 String facebookLink=resource.getString("facebookLink");
    	 String twitterLink=resource.getString("twitterLink");
    	 String googlePlus=resource.getString("googlePlus");
    	 String linkedIn=resource.getString("linkedIn");
    	
    	 String clenseeLogo = resource.getString("clenseeLogo");
		 String notifywelcome = resource.getString("notifywelcome");
		 String clickhere = resource.getString("clickhere");
		 String notifyreport = resource.getString("notifyreport");
     	 	     
	     String messagebody = request.getParameter("shareComment");	        	        
	    
	     String clenseeAdminEmailId =   resource.getString("clenseeAdminEmailId");
	     
	        StringBuilder text = new StringBuilder();
	        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
	        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
	       
	        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
	       
	        /* Header */
	        
	        text.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
	        text.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
	        text.append("</tr></table></td></tr></table>");
	       
	        /* End of Header */
	        
	        /* Header-2 */
	        
	        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
	        text.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
	        text.append("<td align='right' style='font-size: 14px; color: #fff;'>");
	        text.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
	        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
	        text.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
	        text.append("</p></td></tr></table></td></tr></table>");
	        
	        /* End of Header-2 */
	        
	        
	        /* BODY PART */
	        
	        text.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
	        text.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
	        text.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
	        text.append("<h3 style='color: #004879;'>Dear "+request.getParameter("name")+",</h3>");
	        text.append("<p style='text-align: justify'>"+notifywelcome+"</p>");
	        text.append("<p style='text-align: justify'>"+messageBody+"</p>");
	        text.append("<p style='text-align: justify'>"+notifyreport+"</p>");
	       
	        text.append("<table><tr><td width='300'><img src="+belowleftImage+" /></td>");
	        text.append("<td width='300' align='right'><img src="+belowrightImage+" /></td></tr>");
	        text.append("<tr><td></td><td align='right' style='color: #777; font-size: 12px; padding-right: 20px;'>Best Regards <br>Clensee</td>");
	        text.append("</tr></table></td></tr>");
	        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
	        text.append("</table> ");
	        
	        
	        text.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
	        text.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
	        
	        text.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
	        
	        text.append("<tr><td>");
	        
	        text.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
	        text.append("</td></tr></table>");
	        
	       
	        text.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
	        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
	        text.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
	        text.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
	        text.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
	        text.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
	        
	        
	        text.append("</tr>");
	        text.append("</table></td></tr></table></td></tr></table>");
	        text.append("</body>");
	        text.append("</html>");
	       
	        /* END OF BODY PART */
	       
	     SendEmailUtil obj = new SendEmailUtil();
		
	     resultStatus = obj.getEmailSent(request.getParameter("emailId"),subject,text.toString());
		
	     request.setAttribute("contactNotifyMessage","You have successfully submited the request.");	
		 target = "You have successfully submited the request.";
		
    	 
    	 EmailSentStatusModel emailReport = new EmailSentStatusModel();
    	
	    	if(resultStatus.equals("success"))
	    	{    		
	    		emailReport.setEmail_id(request.getParameter("emailId"));
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("0");
	    	}
	    	else
	    	{
	    		emailReport.setEmail_id(request.getParameter("emailId"));
	    		emailReport.setSubject(subject);
	    		emailReport.setMessage_body(text.toString());
	    		emailReport.setSent_date(new Date());
	    		emailReport.setSent_report("1");
	    	}
    	
    	profileinfoService.setEmailStatusReport(emailReport);
    	
    	
    	
    	// Track the contact us information 
    	
    	
    	
    	
    	
				    	// Mail To Clensee email Address
				    	
							    	    StringBuilder Clenseetext = new StringBuilder();
							    	    Clenseetext.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
							    	    Clenseetext.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
								       
								        text.append("<body bgcolor='#333' style='border: 1px solid #004879; margin: 0px auto; padding: 0px; width: 600px; font-size: 14px; font-family: Trebuchet MS, Verdana, Arial, sans-serif;'>");
								       
								        /* Header */
								        
								        Clenseetext.append("<table bgcolor='#009fe0' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
								        Clenseetext.append("<tr><td><table width='100%' height='45' border='0px' cellpadding='0px' cellspacing='0'><tr>");
								        Clenseetext.append("<tr> <td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>" +emailHeading+ "</td>");
								        Clenseetext.append("</tr></table></td></tr></table>");
								       
								        /* End of Header */
								        
								        /* Header-2 */
								        
								        Clenseetext.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'>");
								        Clenseetext.append("<tr><td><table width='100%' height='70' border='0px' cellpadding='0px' cellspacing='0'><tr>");
								        Clenseetext.append("<td align='left' style='font-size: 30px; color: #fffa00; padding-left: 15px; text-shadow: 1px 2px 0 #004879;'><img src=" +clenseeLogo+ " /></td>");
								        Clenseetext.append("<td align='right' style='font-size: 14px; color: #fff;'>");
								        Clenseetext.append("<p style='font-size: 14px; color: #fff; text-align: right;'>");
								        //text.append("<img src=" +contacticon+ " /> <strong style='font-size: 14px; color: #a3c9e1;'>"+contactNo+"</strong> <br>");
								        Clenseetext.append("<img src=" +mailicon+ " /> <strong><a style='font-size: 14px; color: #a3c9e1;'>"+contactEmail+"</a></strong>");
								        Clenseetext.append("</p></td></tr></table></td></tr></table>");
								        
								        /* End of Header-2 */
								        
								        
								        /* BODY PART */
								        
								        Clenseetext.append("<table bgcolor='#0087bf' width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr><td  bgcolor='#FFFFFF'>");
								        Clenseetext.append("<table width='600' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0'><tr>");
								        Clenseetext.append("<td><img src="+headingImage+" /></td></tr><tr><td bgcolor='#fff' style='padding-left: 15px; padding-right: 15px;'>");
								        Clenseetext.append("<h3 style='color: #004879;'>Dear Clensee Admin,</h3>");
								      
								        Clenseetext.append("<p style='text-align: justify'><b>Name:</b>"+request.getParameter("name")+"</p>");
								        Clenseetext.append("<p style='text-align: justify'><b>Email Id:</b>"+request.getParameter("emailId")+"</p>");
								        Clenseetext.append("<p style='text-align: justify'><b>Mode of Contact:</b>"+request.getParameter("modeOfContact")+"</p>");
								        Clenseetext.append("<p style='text-align: justify'><b>Contact No:</b>"+request.getParameter("mobileNo")+"</p>");
								        
								        Clenseetext.append("<p style='text-align: justify'><b>Message:</b></p>");
								        Clenseetext.append("<p style='text-align: justify'>"+request.getParameter("description")+"</p>");
								        
								        Clenseetext.append("<table><tr>");
								        Clenseetext.append("</tr>");
								        Clenseetext.append("<tr>");
								        Clenseetext.append("</tr></table></td></tr>");
								        Clenseetext.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px;background-color:#d3d3d3'>This email is auto generated Please do not reply.For futher queries please drop a mail to support@clensee.com</td></tr>");
								        Clenseetext.append("</table> ");
								        
								        Clenseetext.append("<table width='600' bgcolor='#0087bf' border='0px' bordercolordark='#fff' cellpadding='0px' cellspacing='0' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
								        Clenseetext.append("<tr><td> <table align='left' width='285' cellpadding='0px' cellspacing='0' style='color: #a3c9e1;'>");
								        
								        Clenseetext.append("<tr><h5 style='color: #a3c9e1; font-size: 12px;margin: 10px -104px 4px 0; padding-left:2px; width:448px;'>"+copyright+"</h5></tr>");
								        
								        Clenseetext.append("<tr><td>");
								        
								        Clenseetext.append("<a style='color: #a3c9e1;' href="+termscondition+" target='_blank'>Terms & Conditions</a> | <a style='color: #a3c9e1;' href="+privacyPolicy+" target='_blank'>Privacy Policy</a>");
								        Clenseetext.append("</td></tr></table>");
								        
								       
								        Clenseetext.append("<table align='right' cellpadding='0' cellspacing='0' style='color: #a3c9e1; width: 150px; text-align: right;'>");
								        Clenseetext.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
								        Clenseetext.append("<tr><td><a href="+facebookLink+" target='_blank'><img src="+facebookicon+" /></a></td>");
								        Clenseetext.append("<td><a href="+twitterLink+" target='_blank'><img src="+twittericon+" /></a></td>");
								        Clenseetext.append("<td><a href="+googlePlus+" target='_blank'><img src="+googleicon+" /></a></td>");
								        Clenseetext.append("<td><a href="+linkedIn+" target='_blank'><img src="+linkedicon+" /></a></td>");
								        
								        
								        Clenseetext.append("</tr>");
								        Clenseetext.append("</table></td></tr></table></td></tr></table>");
								        Clenseetext.append("</body>");
								        Clenseetext.append("</html>");
								       
								        /* END OF BODY PART  */
								    
								     String clenseeSubject = request.getParameter("name")+" Contact us for "+request.getParameter("modeOfContact");
								    		 
								     resultStatus = obj.getEmailSent(clenseeAdminEmailId,clenseeSubject,Clenseetext.toString());
									
								     request.setAttribute("contactNotifyMessage","You have successfully submited the request.");	
									 target = "You have successfully submited the request.";
									
								    	if(resultStatus.equals("success"))
								    	{    		
								    		emailReport.setEmail_id(clenseeAdminEmailId);
								    		emailReport.setSubject(clenseeSubject);
								    		emailReport.setMessage_body(Clenseetext.toString());
								    		emailReport.setSent_date(new Date());
								    		emailReport.setSent_report("0");
								    	}
								    	else
								    	{
								    		emailReport.setEmail_id(clenseeAdminEmailId);
								    		emailReport.setSubject(clenseeSubject);
								    		emailReport.setMessage_body(Clenseetext.toString());
								    		emailReport.setSent_date(new Date());
								    		emailReport.setSent_report("1");
								    	}
							 	
							 	profileinfoService.setEmailStatusReport(emailReport);
							 	
							 	
				    	
				    	// End of mail to clensee email address
				    	
    	
    	
    	
    	
    	
    	ContactUsModel contactus = new ContactUsModel();
    	
    	if (session.getAttribute("Ind_id") != null)
		{	
    		contactus.setContact_type("Individual");
    		contactus.setContact_portal_identity(Integer.parseInt((String)session.getAttribute("Ind_id")));
		}
    	else if (session.getAttribute("uti_Ind_id") != null)
		{	
    		contactus.setContact_type("Utilizer");
    		contactus.setContact_portal_identity((int)session.getAttribute("uti_Ind_id"));
		}
    	else
    	{
    		contactus.setContact_type("Non Clensee User");
    		contactus.setContact_portal_identity(0);
    	}
    	
    	
    	contactus.setContact_name(request.getParameter("name"));
    	contactus.setContact_emailid(request.getParameter("emailId"));
    	contactus.setContact_mobile_no(request.getParameter("mobileNo"));
    	contactus.setContact_for(request.getParameter("modeOfContact"));
    	contactus.setContact_message(request.getParameter("description"));
    	contactus.setContact_date(new Date());
    	
    	profileinfoService.saveClenseeContactDetails(contactus);
    	
    	
    	
    	
    	
    	
    	
    	
    	// End of Tracking contact us information
    	
    	
    	return target;
     }
     
        
     @RequestMapping(value="/registration_help",method=RequestMethod.GET)
     public String registration_help(HttpServletRequest request,HttpSession session)
     {
    	 int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
 		 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
 		 
    	 return "registration_help";
     }
     
     @RequestMapping(value="/basic_help",method=RequestMethod.GET)
     public String basic_help(HttpServletRequest request,HttpSession session)
     {
    	 int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
 		 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
 		 
    	 return "basicdetails_help";
     }
     
     @RequestMapping(value="/family_help",method=RequestMethod.GET)
     public String family_help(HttpServletRequest request,HttpSession session)
     {
    	 int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
 		 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
 		 
    	 return "family_details_help";
     }
     
     @RequestMapping(value="/social_help",method=RequestMethod.GET)
     public String social_help(HttpServletRequest request,HttpSession session)
     {
    	 int indId = Integer.parseInt((String)session.getAttribute("Ind_id"));		
 		 request.setAttribute("individualFirstName",signupService.getnamefromRegHistory(indId));
 		 
    	 return "social_account_details_help";
     }
     
     
     @RequestMapping(value="/previewProfilePhoto",method=RequestMethod.POST)
     public String previewProfilePhoto(HttpServletRequest request,HttpSession session)
     {   
    	 
    	 String latestProfilePhoto = request.getParameter("photoName");
    			 
    	 request.setAttribute("latestProfilePhoto", latestProfilePhoto);
    	
    	 return "previewProfilePhoto";
     }

}
