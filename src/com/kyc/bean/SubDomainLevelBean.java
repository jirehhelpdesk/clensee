package com.kyc.bean;

public class SubDomainLevelBean {

	private int sub_domain_id;
	private int parent_domain_id;
	private String sub_domain_name;
	
	
	public int getSub_domain_id() {
		return sub_domain_id;
	}
	public void setSub_domain_id(int sub_domain_id) {
		this.sub_domain_id = sub_domain_id;
	}
	public int getParent_domain_id() {
		return parent_domain_id;
	}
	public void setParent_domain_id(int parent_domain_id) {
		this.parent_domain_id = parent_domain_id;
	}
	public String getSub_domain_name() {
		return sub_domain_name;
	}
	public void setSub_domain_name(String sub_domain_name) {
		this.sub_domain_name = sub_domain_name;
	}
	
	
}
