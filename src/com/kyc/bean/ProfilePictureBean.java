package com.kyc.bean;

import java.util.Date;


public class ProfilePictureBean {
	
    private int pic_id;	
	private int kyc_ind_id;
	private String profile_picture_name;
	private Date cr_date;
	
	
	public int getPic_id() {
		return pic_id;
	}
	public void setPic_id(int pic_id) {
		this.pic_id = pic_id;
	}
	public int getKyc_ind_id() {
		return kyc_ind_id;
	}
	public void setKyc_ind_id(int kyc_ind_id) {
		this.kyc_ind_id = kyc_ind_id;
	}
	public String getProfile_picture_name() {
		return profile_picture_name;
	}
	public void setProfile_picture_name(String profile_picture_name) {
		this.profile_picture_name = profile_picture_name;
	}
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	
	
	
}
