package com.kyc.bean;

import java.util.Date;

public class PatternDetailsBean {

	private int pattern_id;
	private String pattern_name;
	private String pattern_logic;
	private String cr_by;
	private Date cr_date;
	private String mod_by;
	private Date mod_date;
	
	
	public int getPattern_id() {
		return pattern_id;
	}
	public void setPattern_id(int pattern_id) {
		this.pattern_id = pattern_id;
	}
	public String getPattern_name() {
		return pattern_name;
	}
	public void setPattern_name(String pattern_name) {
		this.pattern_name = pattern_name;
	}
	public String getPattern_logic() {
		return pattern_logic;
	}
	public void setPattern_logic(String pattern_logic) {
		this.pattern_logic = pattern_logic;
	}
	public String getCr_by() {
		return cr_by;
	}
	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public String getMod_by() {
		return mod_by;
	}
	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}
	public Date getMod_date() {
		return mod_date;
	}
	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

}
