package com.kyc.bean;

import java.util.Date;

public class KycNotificationBean {

	
	private int notification_id;
	private String notification_type;
	private String notification_to;
	private String notification_subject;
	private String notification_message;
	private Date notification_date;
	private String notification_status;
	
	
	public int getNotification_id() {
		return notification_id;
	}
	public void setNotification_id(int notification_id) {
		this.notification_id = notification_id;
	}
	public String getNotification_type() {
		return notification_type;
	}
	public void setNotification_type(String notification_type) {
		this.notification_type = notification_type;
	}
	public String getNotification_to() {
		return notification_to;
	}
	public void setNotification_to(String notification_to) {
		this.notification_to = notification_to;
	}
	public String getNotification_subject() {
		return notification_subject;
	}
	public void setNotification_subject(String notification_subject) {
		this.notification_subject = notification_subject;
	}
	public String getNotification_message() {
		return notification_message;
	}
	public void setNotification_message(String notification_message) {
		this.notification_message = notification_message;
	}
	public Date getNotification_date() {
		return notification_date;
	}
	public void setNotification_date(Date notification_date) {
		this.notification_date = notification_date;
	}
	public String getNotification_status() {
		return notification_status;
	}
	public void setNotification_status(String notification_status) {
		this.notification_status = notification_status;
	}
	
	
}
