package com.kyc.bean;
import java.util.Date;

public class SocialaccBean {
	private Integer idsocialaccountdetail;
	private String Socialwebsitename;
	private String Socialwebsitelink;
	private String ind_kyc_id;
	private String Socialtwitterlink;
	private String Socialtwittername;
	private String Sociallinkedlnlink;
	private String Sociallinkedlnname;
	private Date cr_date;
	private String cr_by;
	
	
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public String getCr_by() {
		return cr_by;
	}
	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}
	public String getSocialtwitterlink() {
		return Socialtwitterlink;
	}
	public void setSocialtwitterlink(String socialtwitterlink) {
		Socialtwitterlink = socialtwitterlink;
	}
	public String getSocialtwittername() {
		return Socialtwittername;
	}
	public void setSocialtwittername(String socialtwittername) {
		Socialtwittername = socialtwittername;
	}
	public String getSociallinkedlnlink() {
		return Sociallinkedlnlink;
	}
	public void setSociallinkedlnlink(String sociallinkedlnlink) {
		Sociallinkedlnlink = sociallinkedlnlink;
	}
	public String getSociallinkedlnname() {
		return Sociallinkedlnname;
	}
	public void setSociallinkedlnname(String sociallinkedlnname) {
		Sociallinkedlnname = sociallinkedlnname;
	}
	public String getInd_kyc_id() {
		return ind_kyc_id;
	}
	public void setInd_kyc_id(String ind_kyc_id) {
		this.ind_kyc_id = ind_kyc_id;
	}
	public Integer getIdsocialaccountdetail() {
		return idsocialaccountdetail;
	}
	public void setIdsocialaccountdetail(Integer idsocialaccountdetail) {
		this.idsocialaccountdetail = idsocialaccountdetail;
	}
	public String getSocialwebsitename() {
		return Socialwebsitename;
	}
	public void setSocialwebsitename(String socialwebsitename) {
		Socialwebsitename = socialwebsitename;
	}
	public String getSocialwebsitelink() {
		return Socialwebsitelink;
	}
	public void setSocialwebsitelink(String socialwebsitelink) {
		Socialwebsitelink = socialwebsitelink;
	}
}
