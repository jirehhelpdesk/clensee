package com.kyc.bean;

import java.util.Date;

public class DocumentHierarchyBean {

	
	private int doc_hir_id;
	
	private String doc_hir_name;
	
	private String doc_type;
	
	private String subdoc_type;
	
	private String doc_hierarchy;
     
	private String cr_by;
	
	private Date cr_date;
	
	private String mod_by;
	
	private Date mod_date;
    
	
	
	
	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public String getSubdoc_type() {
		return subdoc_type;
	}

	public void setSubdoc_type(String subdoc_type) {
		this.subdoc_type = subdoc_type;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

	public int getDoc_hir_id() {
		return doc_hir_id;
	}

	public void setDoc_hir_id(int doc_hir_id) {
		this.doc_hir_id = doc_hir_id;
	}

	public String getDoc_hir_name() {
		return doc_hir_name;
	}

	public void setDoc_hir_name(String doc_hir_name) {
		this.doc_hir_name = doc_hir_name;
	}

	public String getDoc_hierarchy() {
		return doc_hierarchy;
	}

	public void setDoc_hierarchy(String doc_hierarchy) {
		this.doc_hierarchy = doc_hierarchy;
	}
	
	
}
