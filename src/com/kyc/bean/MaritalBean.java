package com.kyc.bean;

public class MaritalBean {
	private String Martialstatus;
	private String SpousePOI;
	private String Spousename;
	private String Spousegender;
	private String ind_kyc_id;
	private String horoscope_info;
	private String food_preferred;
	private Integer idmatrialdetail;
	public Integer getIdmatrialdetail() {
		return idmatrialdetail;
	}
	public void setIdmatrialdetail(Integer idmatrialdetail) {
		this.idmatrialdetail = idmatrialdetail;
	}
	public String getMartialstatus() {
		return Martialstatus;
	}
	public void setMartialstatus(String martialstatus) {
		Martialstatus = martialstatus;
	}
	public String getSpousePOI() {
		return SpousePOI;
	}
	public void setSpousePOI(String spousePOI) {
		SpousePOI = spousePOI;
	}
	public String getSpousename() {
		return Spousename;
	}
	public void setSpousename(String spousename) {
		Spousename = spousename;
	}
	public String getSpousegender() {
		return Spousegender;
	}
	public void setSpousegender(String spousegender) {
		Spousegender = spousegender;
	}
	public String getInd_kyc_id() {
		return ind_kyc_id;
	}
	public void setInd_kyc_id(String ind_kyc_id) {
		this.ind_kyc_id = ind_kyc_id;
	}
	public String getHoroscope_info() {
		return horoscope_info;
	}
	public void setHoroscope_info(String horoscope_info) {
		this.horoscope_info = horoscope_info;
	}
	public String getFood_preferred() {
		return food_preferred;
	}
	public void setFood_preferred(String food_preferred) {
		this.food_preferred = food_preferred;
	}
	
	
}
