package com.kyc.bean;
import java.util.List;


import org.springframework.web.multipart.MultipartFile;

public class FileUploadForm {
		 
	private List<MultipartFile> payslip;

	
	public List<MultipartFile> getFiles() {
		return payslip;
	}

	public void setFiles(List<MultipartFile> files) {
		this.payslip = files;
	}
		
}
