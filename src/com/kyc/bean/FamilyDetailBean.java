package com.kyc.bean;

import java.util.Date;

public class FamilyDetailBean {
	public String getMaritalstatus() {
		return Maritalstatus;
	}
	public void setMaritalstatus(String maritalstatus) {
		Maritalstatus = maritalstatus;
	}
	private Integer fid;
	public Integer getFid() {
		return fid;
	}
	public void setFid(Integer fid) {
		this.fid = fid;
	}
	private String ind_kyc_id;
	private String Fathername;
	private String FatherPOI;
	private String Mothername;
	private String MotherPOI;
	private String Brothername;
	private String BrotherPOI;
	private String Spousename;
	private String SpousePOI;
	private String Maritalstatus;
	private String Spousegender;
	private String HoroscopeInformation;
	private String Foodpreferred;
	private Date cr_date;
	private String cr_by;
	
	
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public String getCr_by() {
		return cr_by;
	}
	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}
	public String getSpousegender() {
		return Spousegender;
	}
	public void setSpousegender(String spousegender) {
		Spousegender = spousegender;
	}
	public String getHoroscopeInformation() {
		return HoroscopeInformation;
	}
	public void setHoroscopeInformation(String horoscopeInformation) {
		HoroscopeInformation = horoscopeInformation;
	}
	public String getFoodpreferred() {
		return Foodpreferred;
	}
	public void setFoodpreferred(String foodpreferred) {
		Foodpreferred = foodpreferred;
	}
	public String getMotherPOI() {
		return MotherPOI;
	}
	public void setMotherPOI(String motherPOI) {
		MotherPOI = motherPOI;
	}
	public String getBrothername() {
		return Brothername;
	}
	public void setBrothername(String brothername) {
		Brothername = brothername;
	}
	
	public String getInd_kyc_id() {
		return ind_kyc_id;
	}
	public void setInd_kyc_id(String ind_kyc_id) {
		this.ind_kyc_id = ind_kyc_id;
	}
	public String getBrotherPOI() {
		return BrotherPOI;
	}
	public void setBrotherPOI(String brotherPOI) {
		BrotherPOI = brotherPOI;
	}
	
	public String getFathername() {
		return Fathername;
	}
	public void setFathername(String fathername) {
		Fathername = fathername;
	}
	public String getFatherPOI() {
		return FatherPOI;
	}
	public void setFatherPOI(String fatherPOI) {
		FatherPOI = fatherPOI;
	}
	public String getMothername() {
		return Mothername;
	}
	public void setMothername(String mothername) {
		Mothername = mothername;
	}
	public String getSpousename() {
		return Spousename;
	}
	public void setSpousename(String spousename) {
		Spousename = spousename;
	}
	public String getSpousePOI() {
		return SpousePOI;
	}
	public void setSpousePOI(String spousePOI) {
		SpousePOI = spousePOI;
	}
	
}
