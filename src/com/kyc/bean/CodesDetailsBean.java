package com.kyc.bean;

import java.util.Date;

public class CodesDetailsBean {
	
	private int code_id;
	
	private String code_generated;
	
	private String code_description;
	
	private String code_sender;
	
	private String code_receiver;
		
	private String  cr_by;
	
	private Date cr_date;
	
	private String mod_by;
	
	private Date mod_date;
	
	private String code_pattern;
	
	private String code_status;
	
	

	public String getCode_status() {
		return code_status;
	}

	public void setCode_status(String code_status) {
		this.code_status = code_status;
	}

	public int getCode_id() {
		return code_id;
	}

	public void setCode_id(int code_id) {
		this.code_id = code_id;
	}

	public String getCode_generated() {
		return code_generated;
	}

	public void setCode_generated(String code_generated) {
		this.code_generated = code_generated;
	}

	public String getCode_description() {
		return code_description;
	}

	public void setCode_description(String code_description) {
		this.code_description = code_description;
	}
	
	public String getCode_sender() {
		return code_sender;
	}

	public void setCode_sender(String code_sender) {
		this.code_sender = code_sender;
	}

	public String getCode_receiver() {
		return code_receiver;
	}

	public void setCode_receiver(String code_receiver) {
		this.code_receiver = code_receiver;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

	public String getCode_pattern() {
		return code_pattern;
	}

	public void setCode_pattern(String code_pattern) {
		this.code_pattern = code_pattern;
	}
	
	

}
