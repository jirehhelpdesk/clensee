package com.kyc.bean;

import java.util.Date;

public class UtiliserProfileBean {

	private int uti_ind_id;
	private String kyc_uti_id;
	private String password;
	private int pattern_id;
	private String uti_dom_hierarchy;
	private String uti_first_name;
	private String uti_middle_name;
	private String uti_last_name;
	private String uti_reg_kyc_id;
	private String uti_office_first_no;
	private String uti_office_second_no;
	private String uti_email_id;
	private String uti_office_emailId;
	private String uti_profile_pic;
	private String uti_plan_domain;
	private String utiliser_plan;
	private Date uti_cr_date;
    private String uti_gender;	
	private String uti_office_Address;
	private String uti_head_office_Address;
	
	
	
	public int getUti_ind_id() {
		return uti_ind_id;
	}
	public void setUti_ind_id(int uti_ind_id) {
		this.uti_ind_id = uti_ind_id;
	}
	public Date getUti_cr_date() {
		return uti_cr_date;
	}
	public void setUti_cr_date(Date uti_cr_date) {
		this.uti_cr_date = uti_cr_date;
	}	
	public String getUti_plan_domain() {
		return uti_plan_domain;
	}
	public void setUti_plan_domain(String uti_plan_domain) {
		this.uti_plan_domain = uti_plan_domain;
	}
	public String getUtiliser_plan() {
		return utiliser_plan;
	}
	public void setUtiliser_plan(String utiliser_plan) {
		this.utiliser_plan = utiliser_plan;
	}
	public String getKyc_uti_id() {
		return kyc_uti_id;
	}
	public void setKyc_uti_id(String kyc_uti_id) {
		this.kyc_uti_id = kyc_uti_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getPattern_id() {
		return pattern_id;
	}
	public void setPattern_id(int pattern_id) {
		this.pattern_id = pattern_id;
	}
	public String getUti_dom_hierarchy() {
		return uti_dom_hierarchy;
	}
	public void setUti_dom_hierarchy(String uti_dom_hierarchy) {
		this.uti_dom_hierarchy = uti_dom_hierarchy;
	}
	public String getUti_first_name() {
		return uti_first_name;
	}
	public void setUti_first_name(String uti_first_name) {
		this.uti_first_name = uti_first_name;
	}
	public String getUti_middle_name() {
		return uti_middle_name;
	}
	public void setUti_middle_name(String uti_middle_name) {
		this.uti_middle_name = uti_middle_name;
	}
	public String getUti_last_name() {
		return uti_last_name;
	}
	public void setUti_last_name(String uti_last_name) {
		this.uti_last_name = uti_last_name;
	}
	
	public String getUti_office_first_no() {
		return uti_office_first_no;
	}
	public void setUti_office_first_no(String uti_office_first_no) {
		this.uti_office_first_no = uti_office_first_no;
	}
	public String getUti_office_second_no() {
		return uti_office_second_no;
	}
	public void setUti_office_second_no(String uti_office_second_no) {
		this.uti_office_second_no = uti_office_second_no;
	}
	public String getUti_email_id() {
		return uti_email_id;
	}
	public void setUti_email_id(String uti_email_id) {
		this.uti_email_id = uti_email_id;
	}
	public String getUti_profile_pic() {
		return uti_profile_pic;
	}
	public void setUti_profile_pic(String uti_profile_pic) {
		this.uti_profile_pic = uti_profile_pic;
	}
	public String getUti_gender() {
		return uti_gender;
	}
	public void setUti_gender(String uti_gender) {
		this.uti_gender = uti_gender;
	}
	public String getUti_office_Address() {
		return uti_office_Address;
	}
	public void setUti_office_Address(String uti_office_Address) {
		this.uti_office_Address = uti_office_Address;
	}
	public String getUti_office_emailId() {
		return uti_office_emailId;
	}
	public void setUti_office_emailId(String uti_office_emailId) {
		this.uti_office_emailId = uti_office_emailId;
	}
	public String getUti_head_office_Address() {
		return uti_head_office_Address;
	}
	public void setUti_head_office_Address(String uti_head_office_Address) {
		this.uti_head_office_Address = uti_head_office_Address;
	}
	public String getUti_reg_kyc_id() {
		return uti_reg_kyc_id;
	}
	public void setUti_reg_kyc_id(String uti_reg_kyc_id) {
		this.uti_reg_kyc_id = uti_reg_kyc_id;
	}
	 
		
}
