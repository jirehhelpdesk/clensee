package com.kyc.bean;

import java.util.Date;

public class BasicDetailBean {
	private Integer idbasicdetailPassport;
	private String Passportsize;
	private String Matrialstatus;
	private String DOB;
	private String Nationality;
	private String Teloff;
	private String Telres;
	private String Emailalternative;
	private String Present_address;
	private String Permanent_address;
	private String ind_kyc_id;
	private String Languages;
	private String Hobbies;
	private String visibility;
	private Date cr_date;
	private String cr_by;
		
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public String getCr_by() {
		return cr_by;
	}
	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public String getPresent_address() {
		return Present_address;
	}
	public void setPresent_address(String present_address) {
		Present_address = present_address;
	}
	public String getPermanent_address() {
		return Permanent_address;
	}
	public void setPermanent_address(String permanent_address) {
		Permanent_address = permanent_address;
	}
	public String getInd_kyc_id() {
		return ind_kyc_id;
	}
	public void setInd_kyc_id(String ind_kyc_id) {
		this.ind_kyc_id = ind_kyc_id;
	}
	public Integer getIdbasicdetailPassport() {
		return idbasicdetailPassport;
	}
	public void setIdbasicdetailPassport(Integer idbasicdetailPassport) {
		this.idbasicdetailPassport = idbasicdetailPassport;
	}
	public String getPassportsize() {
		return Passportsize;
	}
	public void setPassportsize(String passportsize) {
		Passportsize = passportsize;
	}
	
	public String getMatrialstatus() {
		return Matrialstatus;
	}
	public void setMatrialstatus(String matrialstatus) {
		Matrialstatus = matrialstatus;
	}
	
	
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	public String getNationality() {
		return Nationality;
	}
	public void setNationality(String nationality) {
		Nationality = nationality;
	}
	public String getTeloff() {
		return Teloff;
	}
	public void setTeloff(String teloff) {
		Teloff = teloff;
	}
	public String getTelres() {
		return Telres;
	}
	public void setTelres(String telres) {
		Telres = telres;
	}
	public String getEmailalternative() {
		return Emailalternative;
	}
	public void setEmailalternative(String emailalternative) {
		Emailalternative = emailalternative;
	}
	
	
	public String getLanguages() {
		return Languages;
	}
	public void setLanguages(String languages) {
		Languages = languages;
	}
	public String getHobbies() {
		return Hobbies;
	}
	public void setHobbies(String hobbies) {
		Hobbies = hobbies;
	}
	
	
}
