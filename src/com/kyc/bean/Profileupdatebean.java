package com.kyc.bean;

import java.util.Date;

public class Profileupdatebean {

	
private Integer idprofileupdate;
private String profileupdatedetail;
private Date profileupdatetime;
private String type_of_alert;
private String more_about_it;
private String kyc_id;
private String viewer_kyc_id;


public String getViewer_kyc_id() {
	return viewer_kyc_id;
}
public void setViewer_kyc_id(String viewer_kyc_id) {
	this.viewer_kyc_id = viewer_kyc_id;
}
public String getType_of_alert() {
	return type_of_alert;
}
public void setType_of_alert(String type_of_alert) {
	this.type_of_alert = type_of_alert;
}
public String getMore_about_it() {
	return more_about_it;
}
public void setMore_about_it(String more_about_it) {
	this.more_about_it = more_about_it;
}
public String getKyc_id() {
	return kyc_id;
}
public void setKyc_id(String kyc_id) {
	this.kyc_id = kyc_id;
}


public Integer getIdprofileupdate() {
	return idprofileupdate;
}
public void setIdprofileupdate(Integer idprofileupdate) {
	this.idprofileupdate = idprofileupdate;
}
public String getProfileupdatedetail() {
	return profileupdatedetail;
}
public void setProfileupdatedetail(String profileupdatedetail) {
	this.profileupdatedetail = profileupdatedetail;
}
public Date getProfileupdatetime() {
	return profileupdatetime;
}
public void setProfileupdatetime(Date profileupdatetime) {
	this.profileupdatetime = profileupdatetime;
}
}
