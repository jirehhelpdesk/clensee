package com.kyc.bean;

import java.util.Date;

public class UtiliserDetailsBean

{
	
	private int details_id;
	private int uti_ind_id;
	private Date cr_date;	
	private String about_utiliser;
	private String about_company;
	
	
	public int getDetails_id() {
		return details_id;
	}
	public void setDetails_id(int details_id) {
		this.details_id = details_id;
	}
	public int getUti_ind_id() {
		return uti_ind_id;
	}
	public void setUti_ind_id(int uti_ind_id) {
		this.uti_ind_id = uti_ind_id;
	}	
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}	
	public String getAbout_utiliser() {
		return about_utiliser;
	}
	public void setAbout_utiliser(String about_utiliser) {
		this.about_utiliser = about_utiliser;
	}
	public String getAbout_company() {
		return about_company;
	}
	public void setAbout_company(String about_company) {
		this.about_company = about_company;
	}
	
		
}
