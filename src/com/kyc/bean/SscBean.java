package com.kyc.bean;

public class SscBean {
	
	private int docid;
	private String orgname;
	private String typeofcourse;
	private String yearofpass;
	private String percentage;
	private String state;
	private String certf_id;
	private String certf_Issue_auth;
	private String pincode;
	
	
	public int getDocid() {
		return docid;
	}
	public void setDocid(int docid) {
		this.docid = docid;
	}
	public String getOrgname() {
		return orgname;
	}
	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}
	public String getTypeofcourse() {
		return typeofcourse;
	}
	public void setTypeofcourse(String typeofcourse) {
		this.typeofcourse = typeofcourse;
	}
	public String getYearofpass() {
		return yearofpass;
	}
	public void setYearofpass(String yearofpass) {
		this.yearofpass = yearofpass;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCertf_id() {
		return certf_id;
	}
	public void setCertf_id(String certf_id) {
		this.certf_id = certf_id;
	}
	public String getCertf_Issue_auth() {
		return certf_Issue_auth;
	}
	public void setCertf_Issue_auth(String certf_Issue_auth) {
		this.certf_Issue_auth = certf_Issue_auth;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	
	
}
