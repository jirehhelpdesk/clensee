package com.kyc.bean;

import java.util.Date;

public class FeaturesDetailsBean {

	    private int feature_id;	
		private String feature_name;
		private String feature_value;
		private int feature_price;
		private String feature_description;
		private String feature_category;
		private String cr_by;
		private Date cr_date;
		private String mod_by;
		private Date mod_date;
		public int getFeature_id() {
			return feature_id;
		}
		public void setFeature_id(int feature_id) {
			this.feature_id = feature_id;
		}
		public String getFeature_name() {
			return feature_name;
		}
		public void setFeature_name(String feature_name) {
			this.feature_name = feature_name;
		}
		public String getFeature_value() {
			return feature_value;
		}
		public void setFeature_value(String feature_value) {
			this.feature_value = feature_value;
		}
		public int getFeature_price() {
			return feature_price;
		}
		public void setFeature_price(int feature_price) {
			this.feature_price = feature_price;
		}
		public String getFeature_description() {
			return feature_description;
		}
		public void setFeature_description(String feature_description) {
			this.feature_description = feature_description;
		}
		public String getFeature_category() {
			return feature_category;
		}
		public void setFeature_category(String feature_category) {
			this.feature_category = feature_category;
		}
		public String getCr_by() {
			return cr_by;
		}
		public void setCr_by(String cr_by) {
			this.cr_by = cr_by;
		}
		public Date getCr_date() {
			return cr_date;
		}
		public void setCr_date(Date cr_date) {
			this.cr_date = cr_date;
		}
		public String getMod_by() {
			return mod_by;
		}
		public void setMod_by(String mod_by) {
			this.mod_by = mod_by;
		}
		public Date getMod_date() {
			return mod_date;
		}
		public void setMod_date(Date mod_date) {
			this.mod_date = mod_date;
		}
		
}
