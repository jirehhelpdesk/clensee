package com.kyc.bean;

import java.util.Date;


public class IndSignupBean {
	
	private Integer indid;
	private String kycid;
	private String firstname;
	private String middlename;
	private String lastname;
	private String emailid;
	private String mobileno;
	private String password;
	private Date cr_date;
	private String visibility;
	private String status;
	private String plan_available;
	private String gender;
	private String individual_full_name;
		
	
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPlan_available() {
		return plan_available;
	}
	public void setPlan_available(String plan_available) {
		this.plan_available = plan_available;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public Integer getIndid() {
		return indid;
	}
	public void setIndid(Integer indid) {
		this.indid = indid;
	}
	public String getKycid() {
		return kycid;
	}
	public void setKycid(String kycid) {
		this.kycid = kycid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIndividual_full_name() {
		return individual_full_name;
	}
	public void setIndividual_full_name(String individual_full_name) {
		this.individual_full_name = individual_full_name;
	}
	
	
	
		
}
