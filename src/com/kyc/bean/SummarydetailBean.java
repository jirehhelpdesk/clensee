package com.kyc.bean;

import java.util.Date;

public class SummarydetailBean {
	
	private Integer idsummarydetailBean;
	private String kycid;
	private String landpagedescription;
	private Date cr_date;
	
	
	
	public String getLandpagedescription() {
		return landpagedescription;
	}
	public void setLandpagedescription(String landpagedescription) {
		this.landpagedescription = landpagedescription;
	}
	public String getKycid() {
		return kycid;
	}
	public void setKycid(String kycid) {
		this.kycid = kycid;
	}
	public Integer getIdsummarydetailBean() {
		return idsummarydetailBean;
	}
	public void setIdsummarydetailBean(Integer idsummarydetailBean) {
		this.idsummarydetailBean = idsummarydetailBean;
	}
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
		
}
