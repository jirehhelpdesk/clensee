package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="payment_records_for_transaction")
public class PaymentDetailsModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "payment_id")
	private Integer payment_id;
	
	@Column(name = "payment_from")
	private String payment_from;
	
	@Column(name = "payment_done_by")
	private String payment_done_by;
	
	@Column(name = "payment_amount")
	private String  payment_amount;
	
	@Column(name = "payment_date")
	private Date payment_date;
	
	@Column(name = "payment_status")
	private String payment_status;
	
	@Column(name = "payment_transaction_id")
	private String payment_transaction_id;
	
	@Column(name = "payment_info_emailId")
	private String payment_info_emailId;
	
	@Column(name = "payment_info_mobileno")
	private String payment_info_mobileno;
	
	@Column(name = "payment_info_ipaddress")
	private String payment_info_ipaddress;

	
	
	
	public Integer getPayment_id() {
		return payment_id;
	}

	public void setPayment_id(Integer payment_id) {
		this.payment_id = payment_id;
	}

	public String getPayment_from() {
		return payment_from;
	}

	public void setPayment_from(String payment_from) {
		this.payment_from = payment_from;
	}

	public String getPayment_done_by() {
		return payment_done_by;
	}

	public void setPayment_done_by(String payment_done_by) {
		this.payment_done_by = payment_done_by;
	}

	public String getPayment_amount() {
		return payment_amount;
	}

	public void setPayment_amount(String payment_amount) {
		this.payment_amount = payment_amount;
	}

	public Date getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(Date payment_date) {
		this.payment_date = payment_date;
	}

	public String getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}

	public String getPayment_transaction_id() {
		return payment_transaction_id;
	}

	public void setPayment_transaction_id(String payment_transaction_id) {
		this.payment_transaction_id = payment_transaction_id;
	}

	public String getPayment_info_emailId() {
		return payment_info_emailId;
	}

	public void setPayment_info_emailId(String payment_info_emailId) {
		this.payment_info_emailId = payment_info_emailId;
	}

	public String getPayment_info_mobileno() {
		return payment_info_mobileno;
	}

	public void setPayment_info_mobileno(String payment_info_mobileno) {
		this.payment_info_mobileno = payment_info_mobileno;
	}

	public String getPayment_info_ipaddress() {
		return payment_info_ipaddress;
	}

	public void setPayment_info_ipaddress(String payment_info_ipaddress) {
		this.payment_info_ipaddress = payment_info_ipaddress;
	}
	
	

}
