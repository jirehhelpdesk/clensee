package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contact_us_details")
public class ContactUsModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="contact_id")
	private int contact_id;
	
	@Column(name="contact_type")
	private String contact_type;
	
	@Column(name="contact_portal_identity")
	private int contact_portal_identity;
	
	@Column(name="contact_name")
	private String contact_name;
	
	@Column(name="contact_emailid")
	private String contact_emailid;
		
	@Column(name="contact_mobile_no")
	private String contact_mobile_no;
	
	@Column(name="contact_for")
	private String contact_for;
	
	@Column(name="contact_message")
	private String contact_message;
	
	@Column(name="contact_date")
	private Date contact_date;

	
	
	public int getContact_id() {
		return contact_id;
	}

	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}

	public String getContact_type() {
		return contact_type;
	}

	public void setContact_type(String contact_type) {
		this.contact_type = contact_type;
	}

	public int getContact_portal_identity() {
		return contact_portal_identity;
	}

	public void setContact_portal_identity(int contact_portal_identity) {
		this.contact_portal_identity = contact_portal_identity;
	}

	public String getContact_name() {
		return contact_name;
	}

	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}

	public String getContact_emailid() {
		return contact_emailid;
	}

	public void setContact_emailid(String contact_emailid) {
		this.contact_emailid = contact_emailid;
	}

	public String getContact_mobile_no() {
		return contact_mobile_no;
	}

	public void setContact_mobile_no(String contact_mobile_no) {
		this.contact_mobile_no = contact_mobile_no;
	}

	public String getContact_for() {
		return contact_for;
	}

	public void setContact_for(String contact_for) {
		this.contact_for = contact_for;
	}

	public String getContact_message() {
		return contact_message;
	}

	public void setContact_message(String contact_message) {
		this.contact_message = contact_message;
	}

	public Date getContact_date() {
		return contact_date;
	}

	public void setContact_date(Date contact_date) {
		this.contact_date = contact_date;
	}
		
}
