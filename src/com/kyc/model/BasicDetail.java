package com.kyc.model;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="basic_details")
public class BasicDetail implements Serializable{

	private static final long serialVersionUID = -723583058586873479L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)

	@Column(name = "basic_details_id")
	private Integer basic_details_id;

	@Column(name = "marital_status")
	private String marital_status;
	
	@Column(name = "profile_pic")
	private String profile_pic;
	
	@Column(name = "date_of_birth")
	private String date_of_birth;
	
	@Column(name = "nationality")
	private String nationality;
	
	@Column(name = "tell_office")
	private String tell_office;
	
	@Column(name = "tell_residence")
	private String tell_residence;
	
	@Column(name = "alternate_email")
	private String alternate_email;
	
	@Column(name = "present_address")
	private String present_address;
	
	@Column(name = "permanent_address")
	private String permanent_address;
	
	@Column(name = "languages_known")
	private String languages_known;
	
	@Column(name = "visibility")
	private String visibility;
	
	@Column(name = "hobbies")
	private String hobbies;
	
	@Column(name = "ind_kyc_id")
	private String ind_kyc_id;
	
	@Column(name = "cr_date")
	private Date cr_date;
	
	

	public String getProfile_pic() {
		return profile_pic;
	}
	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}
	
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	
	public Integer getBasic_details_id() {
		return basic_details_id;
	}
	public void setBasic_details_id(Integer basic_details_id) {
		this.basic_details_id = basic_details_id;
	}
	
		
	public String getMarital_status() {
		return marital_status;
	}
	public void setMarital_status(String marital_status) {
		this.marital_status = marital_status;
	}
	
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getTell_office() {
		return tell_office;
	}
	public void setTell_office(String tell_office) {
		this.tell_office = tell_office;
	}
	public String getTell_residence() {
		return tell_residence;
	}
	public void setTell_residence(String tell_residence) {
		this.tell_residence = tell_residence;
	}
	public String getAlternate_email() {
		return alternate_email;
	}
	public void setAlternate_email(String alternate_email) {
		this.alternate_email = alternate_email;
	}
	public String getPresent_address() {
		return present_address;
	}
	public void setPresent_address(String present_address) {
		this.present_address = present_address;
	}
	public String getPermanent_address() {
		return permanent_address;
	}
	public void setPermanent_address(String permanent_address) {
		this.permanent_address = permanent_address;
	}
	public String getLanguages_known() {
		return languages_known;
	}
	public void setLanguages_known(String languages_known) {
		this.languages_known = languages_known;
	}
	public String getHobbies() {
		return hobbies;
	}
	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}
	public String getInd_kyc_id() {
		return ind_kyc_id;
	}
	public void setInd_kyc_id(String ind_kyc_id) {
		this.ind_kyc_id = ind_kyc_id;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
		
}
