package com.kyc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="summary_details")
public class Summarydetail {

	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	
	@Column(name = "sum_det_id")
	private Integer sum_det_id;
	
	@Column(name = "ind_kyc_id")
	private String ind_kyc_id;
	
	@Column(name = "landpagedescription")
	private String landpagedescription;
	
    @Column(name = "cr_date")
    private Date cr_date;
    
    
	
	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getLandpagedescription() {
		return landpagedescription;
	}

	public void setLandpagedescription(String landpagedescription) {
		this.landpagedescription = landpagedescription;
	}

	public Integer getSum_det_id() {
		return sum_det_id;
	}

	public void setSum_det_id(Integer sum_det_id) {
		this.sum_det_id = sum_det_id;
	}
	
	public String getInd_kyc_id() {
		return ind_kyc_id;
	}

	public void setInd_kyc_id(String ind_kyc_id) {
		this.ind_kyc_id = ind_kyc_id;
	}
}