package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="documents")
public class DocumentModel {
	

	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ind_id")
	private int ind_id;
	
	@Column(name="kycid")
	private String kycid;
	
	@Column(name="orgname")
	private String orgname;
	
	@Column(name="typeofdoc")
	private String typeofdoc;
	
	@Column(name="docname")
	private String docname;
	
	@Column(name="state")
	private String state;
	
	
	@Column(name="docissuauth")
	private String docissuauth;
	
	@Column(name="pincode")
	private String pincode;
	
	@Column(name="date")
	private Date date;

	
	
	public int getInd_id() {
		return ind_id;
	}

	public void setInd_id(int ind_id) {
		this.ind_id = ind_id;
	}

	public String getKycid() {
		return kycid;
	}

	public void setKycid(String kycid) {
		this.kycid = kycid;
	}

	public String getOrgname() {
		return orgname;
	}

	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}

	public String getTypeofdoc() {
		return typeofdoc;
	}

	public void setTypeofdoc(String typeofdoc) {
		this.typeofdoc = typeofdoc;
	}

	public String getDocname() {
		return docname;
	}

	public void setDocname(String docname) {
		this.docname = docname;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	

	public String getDocissuauth() {
		return docissuauth;
	}

	public void setDocissuauth(String docissuauth) {
		this.docissuauth = docissuauth;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	

}
