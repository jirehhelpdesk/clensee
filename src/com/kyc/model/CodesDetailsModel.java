package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="codes_details")
public class CodesDetailsModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="code_id")
	private int code_id;
	
	@Column(name="code_generated")
	private String code_generated;
	
	@Column(name="code_description")
    private String code_description;
			
	@Column(name="code_sender")
	private String code_sender;
	
	@Column(name="code_receiver")
	private String code_receiver;
	
	@Column(name="cr_by")
	private String  cr_by;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="mod_by")
	private String mod_by;
	
	@Column(name="mod_date")
	private Date mod_date;
	
	@Column(name="code_pattern")
	private String code_pattern;
	
	@Column(name="code_status")
	private String code_status;
	
	@Column(name="apply_code_count")
	private String apply_code_count;

	public String getCode_status() {
		return code_status;
	}

	public void setCode_status(String code_status) {
		this.code_status = code_status;
	}

	public int getCode_id() {
		return code_id;
	}

	public void setCode_id(int code_id) {
		this.code_id = code_id;
	}

	public String getCode_generated() {
		return code_generated;
	}

	public void setCode_generated(String code_generated) {
		this.code_generated = code_generated;
	}

	public String getCode_description() {
		return code_description;
	}

	public void setCode_description(String code_description) {
		this.code_description = code_description;
	}

	

	public String getCode_sender() {
		return code_sender;
	}

	public void setCode_sender(String code_sender) {
		this.code_sender = code_sender;
	}

	public String getCode_receiver() {
		return code_receiver;
	}

	public void setCode_receiver(String code_receiver) {
		this.code_receiver = code_receiver;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

	public String getCode_pattern() {
		return code_pattern;
	}

	public void setCode_pattern(String code_pattern) {
		this.code_pattern = code_pattern;
	}

	public String getApply_code_count() {
		return apply_code_count;
	}

	public void setApply_code_count(String apply_code_count) {
		this.apply_code_count = apply_code_count;
	}

	
		
}
