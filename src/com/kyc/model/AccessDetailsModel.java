package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="access_details")	
public class AccessDetailsModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="access_id")
	private int access_id;
	
	@Column(name="access_pattern")
	private String access_pattern;
	
	@Column(name="access_giver")
	private String access_giver;
	
	@Column(name="giver_name")
	private String giver_name;
	
	@Column(name="access_taker")
	private String access_taker;
	
	@Column(name="access_given_date")
	private Date access_given_date;
	
	@Column(name="access_taken_date")
	private Date access_taken_date;
	
	@Column(name="access_description")
	private String access_description;

	@Column(name = "status")
	private String status;
	
	@Column(name = "uti_status")
	private String uti_status;
	
	@Column(name = "repeate_access_status")
	private String repeate_access_status;
	
	
	public String getGiver_name() {
		return giver_name;
	}

	public void setGiver_name(String giver_name) {
		this.giver_name = giver_name;
	}

	public String getUti_status() {
		return uti_status;
	}

	public void setUti_status(String uti_status) {
		this.uti_status = uti_status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getAccess_id() {
		return access_id;
	}

	public void setAccess_id(int access_id) {
		this.access_id = access_id;
	}

	public String getAccess_pattern() {
		return access_pattern;
	}

	public void setAccess_pattern(String access_pattern) {
		this.access_pattern = access_pattern;
	}

	public String getAccess_giver() {
		return access_giver;
	}

	public void setAccess_giver(String string) {
		this.access_giver = string;
	}

	public String getAccess_taker() {
		return access_taker;
	}

	public void setAccess_taker(String access_taker) {
		this.access_taker = access_taker;
	}

	public Date getAccess_given_date() {
		return access_given_date;
	}

	public void setAccess_given_date(Date access_given_date) {
		this.access_given_date = access_given_date;
	}

	public Date getAccess_taken_date() {
		return access_taken_date;
	}

	public void setAccess_taken_date(Date access_taken_date) {
		this.access_taken_date = access_taken_date;
	}

	public String getAccess_description() {
		return access_description;
	}

	public void setAccess_description(String access_description) {
		this.access_description = access_description;
	}

	
	public String getRepeate_access_status() {
		return repeate_access_status;
	}

	public void setRepeate_access_status(String repeate_access_status) {
		this.repeate_access_status = repeate_access_status;
	}
	

}
