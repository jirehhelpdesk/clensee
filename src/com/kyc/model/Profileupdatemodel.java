package com.kyc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="profileupdate")
public class Profileupdatemodel  implements Serializable{
	
	
	private static final long serialVersionUID = -723583058586873479L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)

	@Column(name = "idprofileupdate")
	private Integer idprofileupdate;
	
	
	@Column(name = "profileupdatedetail")
	private String profileupdatedetail;
	
	@Column(name = "profileupdatetime")
	private Date profileupdatetime;

	
	@Column(name = "type_of_alert")
	private String type_of_alert;

	
	@Column(name = "more_about_it")
	private String more_about_it;

	
	@Column(name = "kyc_id")
	private String kyc_id;

	@Column(name="viewer_kyc_id")
	private String viewer_kyc_id;
	
	
	


	public String getViewer_kyc_id() {
		return viewer_kyc_id;
	}


	public void setViewer_kyc_id(String viewer_kyc_id) {
		this.viewer_kyc_id = viewer_kyc_id;
	}


	public String getType_of_alert() {
		return type_of_alert;
	}


	public void setType_of_alert(String type_of_alert) {
		this.type_of_alert = type_of_alert;
	}


	public String getMore_about_it() {
		return more_about_it;
	}


	public void setMore_about_it(String more_about_it) {
		this.more_about_it = more_about_it;
	}


	public String getKyc_id() {
		return kyc_id;
	}


	public void setKyc_id(String kyc_id) {
		this.kyc_id = kyc_id;
	}


	public Integer getIdprofileupdate() {
		return idprofileupdate;
	}


	public void setIdprofileupdate(Integer idprofileupdate) {
		this.idprofileupdate = idprofileupdate;
	}


	public String getProfileupdatedetail() {
		return profileupdatedetail;
	}


	public void setProfileupdatedetail(String profileupdatedetail) {
		this.profileupdatedetail = profileupdatedetail;
	}


	public Date getProfileupdatetime() {
		return profileupdatetime;
	}


	public void setProfileupdatetime(Date profileupdatetime) {
		this.profileupdatetime = profileupdatetime;
	}
	
	
	
	

}
