package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sms_sent_status")
public class SmsSentStatusModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)

	@Column(name = "sms_unique_id")
	private Integer sms_unique_id;
	
	@Column(name = "unique_id")
	private Integer unique_id;
	
	@Column(name = "sms_belongs_to")
	private String sms_belongs_to;	
	
	@Column(name = "mobile_no")
	private String mobile_no;	
	
	@Column(name = "message_body")
	private String message_body;	
	
	@Column(name = "sent_date")
	private Date sent_date;	
	
	@Column(name = "sent_report")
	private String sent_report;

	public Integer getSms_unique_id() {
		
		return sms_unique_id;
		
	}

	public Integer getUnique_id() {
		return unique_id;
	}

	public void setUnique_id(Integer unique_id) {
		this.unique_id = unique_id;
	}

	public String getSms_belongs_to() {
		return sms_belongs_to;
	}

	public void setSms_belongs_to(String sms_belongs_to) {
		this.sms_belongs_to = sms_belongs_to;
	}

	public void setSms_unique_id(Integer sms_unique_id) {
		this.sms_unique_id = sms_unique_id;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getMessage_body() {
		return message_body;
	}

	public void setMessage_body(String message_body) {
		this.message_body = message_body;
	}

	public Date getSent_date() {
		return sent_date;
	}

	public void setSent_date(Date sent_date) {
		this.sent_date = sent_date;
	}

	public String getSent_report() {
		return sent_report;
	}

	public void setSent_report(String sent_report) {
		this.sent_report = sent_report;
	}	
	
	
}
