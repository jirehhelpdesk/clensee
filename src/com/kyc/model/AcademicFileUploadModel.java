package com.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="academic_file_details")
public class AcademicFileUploadModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "academic_file_upload_id")
	private Integer academic_file_upload_id;
	
	@Column(name = "kyc_ind_id")
	private int kyc_ind_id;
	
	@Column(name = "file_label_name")
	private String file_label_name;
	
	@Column(name = "file_name")
	private String file_name;
	
	@Column(name = "file_description")
	private String file_description;
	
	@Column(name = "file_size")
	private float file_size;

	
	
	
	public Integer getAcademic_file_upload_id() {
		return academic_file_upload_id;
	}
	public int getKyc_ind_id() {
		return kyc_ind_id;
	}

	public void setKyc_ind_id(int kyc_ind_id) {
		this.kyc_ind_id = kyc_ind_id;
	}

	public void setAcademic_file_upload_id(Integer academic_file_upload_id) {
		this.academic_file_upload_id = academic_file_upload_id;
	}

	public String getFile_label_name() {
		return file_label_name;
	}

	public void setFile_label_name(String file_label_name) {
		this.file_label_name = file_label_name;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_description() {
		return file_description;
	}

	public void setFile_description(String file_description) {
		this.file_description = file_description;
	}

	public float getFile_size() {
		return file_size;
	}

	public void setFile_size(float file_size) {
		this.file_size = file_size;
	}
	
	
	
}
