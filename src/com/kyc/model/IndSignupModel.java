package com.kyc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="in_registrationdetails")
public class IndSignupModel implements Serializable{

	private static final long serialVersionUID = -723583058586873479L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ind_id")
	private Integer ind_id;
	
	@Column(name = "kyc_id")
	private String kycid;
	
	@Column(name = "first_name")
	private String firstname;
	
	@Column(name = "middle_name")
	private String middlename;
	
	@Column(name = "last_name")
	private String lastname;
	
	@Column(name = "email_id")
	private String emailid;
	
	@Column(name = "mobile_no")
	private String mobileno;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "cr_date")
	private Date cr_date;
	
	@Column(name = "gender")
    private String gender;
			
	@Column(name = "visibility")
	private String visibility;
	
	@Column(name = "plan_available")
	private String plan_available;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "admin_control")
	private String admin_control;
		
	@Column(name = "individual_full_name")
	private String individual_full_name;
	
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Integer getInd_id() {
		return ind_id;
	}
	public void setInd_id(Integer ind_id) {
		this.ind_id = ind_id;
	}
	public String getPlan_available() {
		return plan_available;
	}
	public void setPlan_available(String plan_available) {
		this.plan_available = plan_available;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public Integer getIndid() {
		return ind_id;
	}
	public void setIndid(Integer ind_id) {
		this.ind_id = ind_id;
	}
	public String getKycid() {
		return kycid;
	}
	public void setKycid(String kycid) {
		this.kycid = kycid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAdmin_control() {
		return admin_control;
	}

	public void setAdmin_control(String admin_control) {
		this.admin_control = admin_control;
	}

	
	public String getIndividual_full_name() {
		return individual_full_name;
	}

	public void setIndividual_full_name(String individual_full_name) {
		this.individual_full_name = individual_full_name;
	}

}
