package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pattern_details")
public class PatternDetailsModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="pattern_id")
	private int pattern_id;
	
	@Column(name="pattern_name")
	private String pattern_name;
	
	@Column(name="pattern_logic")
	private String pattern_logic;
	
	@Column(name="cr_by")
	private String cr_by;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="mod_by")
	private String mod_by;
	
	@Column(name="mod_date")
	private Date mod_date;

	
	
	public int getPattern_id() {
		return pattern_id;
	}

	public void setPattern_id(int pattern_id) {
		this.pattern_id = pattern_id;
	}

	public String getPattern_name() {
		return pattern_name;
	}

	public void setPattern_name(String pattern_name) {
		this.pattern_name = pattern_name;
	}

	public String getPattern_logic() {
		return pattern_logic;
	}

	public void setPattern_logic(String pattern_logic) {
		this.pattern_logic = pattern_logic;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}
	
	
}
