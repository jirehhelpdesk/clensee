package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="utilizer_plan_details")
public class UtilizerPlanDetailsModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "uti_plan_id")
	private int uti_plan_id;
	
	@Column(name = "uti_ind_id")
	private int uti_ind_id;
	
	@Column(name = "uti_plan_domain")
	private String uti_plan_domain;
	
	@Column(name = "uti_activate_plan")
	private String uti_activate_plan;
	
	@Column(name = "cr_date")
	private Date cr_date;

		
	public int getUti_plan_id() {
		return uti_plan_id;
	}

	public void setUti_plan_id(int uti_plan_id) {
		this.uti_plan_id = uti_plan_id;
	}

	public int getUti_ind_id() {
		return uti_ind_id;
	}

	public void setUti_ind_id(int uti_ind_id) {
		this.uti_ind_id = uti_ind_id;
	}

	public String getUti_plan_domain() {
		return uti_plan_domain;
	}

	public void setUti_plan_domain(String uti_plan_domain) {
		this.uti_plan_domain = uti_plan_domain;
	}

	public String getUti_activate_plan() {
		return uti_activate_plan;
	}

	public void setUti_activate_plan(String uti_activate_plan) {
		this.uti_activate_plan = uti_activate_plan;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
}
