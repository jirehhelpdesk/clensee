package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="uti_reset_password")
public class UtilizerPasswordRequestModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "password_id")
	private int password_id;
		
	@Column(name = "uti_reference_id")
	private String uti_reference_id;
	
	@Column(name = "uti_status")
	private String uti_status;
	
	@Column(name = "requested_date")
	private Date requested_date;

	
	public int getPassword_id() {
		return password_id;
	}

	public void setPassword_id(int password_id) {
		this.password_id = password_id;
	}

	public String getUti_reference_id() {
		return uti_reference_id;
	}

	public void setUti_reference_id(String uti_reference_id) {
		this.uti_reference_id = uti_reference_id;
	}

	public String getUti_status() {
		return uti_status;
	}

	public void setUti_status(String uti_status) {
		this.uti_status = uti_status;
	}

	public Date getRequested_date() {
		return requested_date;
	}

	public void setRequested_date(Date requested_date) {
		this.requested_date = requested_date;
	}

}
