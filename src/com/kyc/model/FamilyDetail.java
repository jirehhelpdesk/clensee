package com.kyc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="family_details")
public class FamilyDetail implements Serializable{

	private static final long serialVersionUID = -723583058586873479L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "fam_details_id")
	private Integer fam_details_id;
	
	@Column(name = "ind_kyc_id")
	private String ind_kyc_id;
	
	@Column(name = "Spousegender")
	private String Spousegender;
	
	@Column(name = "HoroscopeInformation")
	private String HoroscopeInformation;
	
	@Column(name = "Foodpreferred")
	private String Foodpreferred;
	
	@Column(name = "Maritalstatus")
	private String Maritalstatus;
	
	@Column(name = "father_name")
	private String father_name;
	
	@Column(name = "father_POI")
	private String father_POI;
	
	@Column(name = "mother_name")
	private String mother_name;
	
	@Column(name = "mother_POI")
	private String mother_POI;
	
	@Column(name = "cr_date")
	private Date cr_date;
	
	@Column(name = "cr_by")
	private String cr_by;
	
	
	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Integer getFam_details_id() {
		return fam_details_id;
	}

	public void setFam_details_id(Integer fam_details_id) {
		this.fam_details_id = fam_details_id;
	}

	public String getFather_name() {
		return father_name;
	}

	public void setFather_name(String father_name) {
		this.father_name = father_name;
	}

	public String getFather_POI() {
		return father_POI;
	}

	public void setFather_POI(String father_POI) {
		this.father_POI = father_POI;
	}

	public String getMother_name() {
		return mother_name;
	}

	public void setMother_name(String mother_name) {
		this.mother_name = mother_name;
	}

	public String getMother_POI() {
		return mother_POI;
	}

	public void setMother_POI(String mother_POI) {
		this.mother_POI = mother_POI;
	}

	public String getSibling_name() {
		return sibling_name;
	}

	public void setSibling_name(String sibling_name) {
		this.sibling_name = sibling_name;
	}

	public String getSibling_POI() {
		return sibling_POI;
	}

	public void setSibling_POI(String sibling_POI) {
		this.sibling_POI = sibling_POI;
	}

	public String getSpouse_name() {
		return spouse_name;
	}

	public void setSpouse_name(String spouse_name) {
		this.spouse_name = spouse_name;
	}

	public String getSpouse_POI() {
		return spouse_POI;
	}

	public void setSpouse_POI(String spouse_POI) {
		this.spouse_POI = spouse_POI;
	}

	public String getInd_kyc_id() {
		return ind_kyc_id;
	}

	public void setInd_kyc_id(String ind_kyc_id) {
		this.ind_kyc_id = ind_kyc_id;
	}
	
	

	public String getSpousegender() {
		return Spousegender;
	}

	public void setSpousegender(String spousegender) {
		Spousegender = spousegender;
	}

	public String getHoroscopeInformation() {
		return HoroscopeInformation;
	}

	public void setHoroscopeInformation(String horoscopeInformation) {
		HoroscopeInformation = horoscopeInformation;
	}

	public String getFoodpreferred() {
		return Foodpreferred;
	}

	public void setFoodpreferred(String foodpreferred) {
		Foodpreferred = foodpreferred;
	}

	public String getMaritalstatus() {
		return Maritalstatus;
	}

	public void setMaritalstatus(String maritalstatus) {
		Maritalstatus = maritalstatus;
	}



	@Column(name="sibling_name")
	private String sibling_name;
	
	@Column(name="sibling_POI")
	private String sibling_POI;
	
	@Column(name="spouse_name")
	private String spouse_name;
	
	@Column(name="spouse_POI")
	private String spouse_POI;

	
	
	
	
}
