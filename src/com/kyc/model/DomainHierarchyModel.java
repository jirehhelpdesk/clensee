package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="domain_hierarchy")
public class DomainHierarchyModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="dom_hierarchy_id")
	private int dom_hierarchy_id;
	
	@Column(name="dom_name")
    private String dom_name;
	
	@Column(name="dom_hierarchy_data")
	private String dom_hierarchy_data;
	
	@Column(name="cr_by")
	private String cr_by;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="mod_by")
	private String mod_by;
	
	@Column(name="mod_date")
	private String mod_date;

	
	
	public int getDom_hierarchy_id() {
		return dom_hierarchy_id;
	}

	public void setDom_hierarchy_id(int dom_hierarchy_id) {
		this.dom_hierarchy_id = dom_hierarchy_id;
	}

	public String getDom_name() {
		return dom_name;
	}

	public void setDom_name(String dom_name) {
		this.dom_name = dom_name;
	}

	public String getDom_hierarchy_data() {
		return dom_hierarchy_data;
	}

	public void setDom_hierarchy_data(String dom_hierarchy_data) {
		this.dom_hierarchy_data = dom_hierarchy_data;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public String getMod_date() {
		return mod_date;
	}

	public void setMod_date(String mod_date) {
		this.mod_date = mod_date;
	}
	
	
}
