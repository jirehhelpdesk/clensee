package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="document_hierarchy")
public class DocumentHierarchyModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="doc_hir_id")
	private int doc_hir_id;
	
	@Column(name="doc_hir_name")
	private String doc_hir_name;
	
	@Column(name="doc_type")
	private String doc_type;
	
	@Column(name="subdoc_type")
	private String subdoc_type;
	
	@Column(name="doc_hierarchy")
	private String doc_hierarchy;

	@Column(name="cr_by")
    private String cr_by;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="mod_by")
	private String mod_by;
	
	@Column(name="mod_date")
	private Date mod_date;
	
	
	
	
	public String getSubdoc_type() {
		return subdoc_type;
	}

	public void setSubdoc_type(String subdoc_type) {
		this.subdoc_type = subdoc_type;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

	public int getDoc_hir_id() {
		return doc_hir_id;
	}

	public void setDoc_hir_id(int doc_hir_id) {
		this.doc_hir_id = doc_hir_id;
	}

	public String getDoc_hir_name() {
		return doc_hir_name;
	}

	public void setDoc_hir_name(String doc_hir_name) {
		this.doc_hir_name = doc_hir_name;
	}

	public String getDoc_hierarchy() {
		return doc_hierarchy;
	}

	public void setDoc_hierarchy(String doc_hierarchy) {
		this.doc_hierarchy = doc_hierarchy;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}
	
	

}
