package com.kyc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

	@Entity
	@Table(name="matrimony_details")
	public class Matrial  implements Serializable{

		private static final long serialVersionUID = -723583058586873479L;
		
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)

		@Column(name = "matrimony_details_id")
		private Integer matrimony_details_id;
		
		
		@Column(name = "marital_status")
		private String marital_status;
		
		@Column(name = "spouse_name")
		private String spouse_name;
		
		@Column(name = "spouse_gender")
		private String spouse_gender;	
		
		@Column(name = "spouse_POI")
		private String spouse_POI;
		
		@Column(name = "horoscope_info")
		private String horoscope_info;
		
		@Column(name = "food_preferred")
		private String food_preferred;
		
		@Column(name = "ind_kyc_id")
		private String ind_kyc_id;

		public Integer getMatrimony_details_id() {
			return matrimony_details_id;
		}

		public void setMatrimony_details_id(Integer matrimony_details_id) {
			this.matrimony_details_id = matrimony_details_id;
		}

		public String getMarital_status() {
			return marital_status;
		}

		public void setMarital_status(String marital_status) {
			this.marital_status = marital_status;
		}

		public String getSpouse_name() {
			return spouse_name;
		}

		public void setSpouse_name(String spouse_name) {
			this.spouse_name = spouse_name;
		}

		public String getSpouse_gender() {
			return spouse_gender;
		}

		public void setSpouse_gender(String spouse_gender) {
			this.spouse_gender = spouse_gender;
		}

		public String getSpouse_POI() {
			return spouse_POI;
		}

		public void setSpouse_POI(String spouse_POI) {
			this.spouse_POI = spouse_POI;
		}

		public String getHoroscope_info() {
			return horoscope_info;
		}

		public void setHoroscope_info(String horoscope_info) {
			this.horoscope_info = horoscope_info;
		}

		public String getFood_preferred() {
			return food_preferred;
		}

		public void setFood_preferred(String food_preferred) {
			this.food_preferred = food_preferred;
		}

		public String getInd_kyc_id() {
			return ind_kyc_id;
		}

		public void setInd_kyc_id(String ind_kyc_id) {
			this.ind_kyc_id = ind_kyc_id;
		}
		

		

}
