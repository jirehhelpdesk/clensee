package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="utilizer_activity_details")	
public class UtilizerActivityModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="activity_id")
	private int activity_id;
	
	@Column(name="uti_ind_id")
	private int uti_ind_id;
	
	@Column(name="activity_type")
	private String activity_type;
	
	@Column(name="activity_details")
	private String activity_details;
	
	@Column(name="activity_date")
	private Date activity_date;

	
	public int getActivity_id() {
		return activity_id;
	}

	public void setActivity_id(int activity_id) {
		this.activity_id = activity_id;
	}

	public int getUti_ind_id() {
		return uti_ind_id;
	}

	public void setUti_ind_id(int uti_ind_id) {
		this.uti_ind_id = uti_ind_id;
	}

	public String getActivity_details() {
		return activity_details;
	}

	public void setActivity_details(String activity_details) {
		this.activity_details = activity_details;
	}

	public Date getActivity_date() {
		return activity_date;
	}

	public void setActivity_date(Date activity_date) {
		this.activity_date = activity_date;
	}

	public String getActivity_type() {
		return activity_type;
	}

	public void setActivity_type(String activity_type) {
		this.activity_type = activity_type;
	}
	
	
	
}
