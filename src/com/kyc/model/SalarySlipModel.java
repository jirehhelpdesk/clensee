package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="salary_slip")
public class SalarySlipModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "salary_slip_id")
	private int salary_slip_id;
	
	@Column(name = "ind_id")
	private int ind_id;
	
	@Column(name = "company_name")
	private String company_name;
	
	@Column(name = "month_year")
	private String month_year;
	
	@Column(name = "slip_name")
	private String slip_name;
	
	@Column(name="doc_size")
	private float doc_size;
	
	@Column(name = "cr_date")
	private Date cr_date;

	
	
	public int getSalary_slip_id() {
		return salary_slip_id;
	}

	public void setSalary_slip_id(int salary_slip_id) {
		this.salary_slip_id = salary_slip_id;
	}

	public int getInd_id() {
		return ind_id;
	}

	public void setInd_id(int ind_id) {
		this.ind_id = ind_id;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
		
	public String getMonth_year() {
		return month_year;
	}

	public void setMonth_year(String month_year) {
		this.month_year = month_year;
	}

	public String getSlip_name() {
		return slip_name;
	}

	public void setSlip_name(String slip_name) {
		this.slip_name = slip_name;
	}
	
	public float getDoc_size() {
		return doc_size;
	}

	public void setDoc_size(float doc_size) {
		this.doc_size = doc_size;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	
	
	
}
