package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="document_details")
public class DocumentDetailModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="doc_id")
	private int doc_id;
    
	@Column(name="doc_name")
	private String doc_name;
	
	@Column(name="doc_size")
	private float doc_size;
	
	@Column(name="doc_type")
	private String doc_type;
	
	@Column(name="kyc_ind_id")
	private int kyc_ind_id;
	
	@Column(name="docs_data")
	private String docs_data;

	@Column(name="cr_by")
    private String cr_by;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="mod_by")
	private String mod_by;
	
	@Column(name="mod_date")
	private Date mod_date;
	
	@Column(name="doc_status")
	private String doc_status;
	
	@Column(name="des")
	private String des;
	
	@Column(name="doc_des")
	private String doc_des;
	
	@Column(name="panel_status")
	private String panel_status;
	
	
	public float getDoc_size() {
		return doc_size;
	}

	public void setDoc_size(float doc_size) {
		this.doc_size = doc_size;
	}

	public String getPanel_status() {
		return panel_status;
	}

	public void setPanel_status(String panel_status) {
		this.panel_status = panel_status;
	}

	public String getDoc_des() {
		return doc_des;
	}

	public void setDoc_des(String doc_des) {
		this.doc_des = doc_des;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

	public String getDoc_status() {
		return doc_status;
	}

	public void setDoc_status(String doc_status) {
		this.doc_status = doc_status;
	}

	public int getDoc_id() {
		return doc_id;
	}

	public void setDoc_id(int doc_id) {
		this.doc_id = doc_id;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public int getKyc_ind_id() {
		return kyc_ind_id;
	}

	public void setKyc_ind_id(int kyc_ind_id) {
		this.kyc_ind_id = kyc_ind_id;
	}

	public String getDocs_data() {
		return docs_data;
	}

	public void setDocs_data(String docs_data) {
		this.docs_data = docs_data;
	}
	
	
	
	
}
