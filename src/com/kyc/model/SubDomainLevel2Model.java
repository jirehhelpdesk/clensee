package com.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sub_domain_level_2")
public class SubDomainLevel2Model {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="sub_domain_level_2_id")
	private int sub_domain_level_2_id;
	
	@Column(name="parent_domain_id")
    private int parent_domain_id;
	
	@Column(name="sub_domain_name")
	private String sub_domain_name;

		
	public int getSub_domain_level_2_id() {
		return sub_domain_level_2_id;
	}

	public void setSub_domain_level_2_id(int sub_domain_level_2_id) {
		this.sub_domain_level_2_id = sub_domain_level_2_id;
	}

	public int getParent_domain_id() {
		return parent_domain_id;
	}

	public void setParent_domain_id(int parent_domain_id) {
		this.parent_domain_id = parent_domain_id;
	}

	public String getSub_domain_name() {
		return sub_domain_name;
	}

	public void setSub_domain_name(String sub_domain_name) {
		this.sub_domain_name = sub_domain_name;
	}
	
}
