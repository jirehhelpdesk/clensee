package com.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="domain_level")
public class DomainLevelModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="domain_id")
	private int domain_id;
	
	@Column(name="domain_name")
    private String domain_name;
	
	@Column(name="admin_id")
	private String admin_id;
	
	public int getDomain_id() {
		return domain_id;
	}

	public void setDomain_id(int domain_id) {
		this.domain_id = domain_id;
	}

	public String getDomain_name() {
		return domain_name;
	}

	public void setDomain_name(String domain_name) {
		this.domain_name = domain_name;
	}

	public String getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
		
}
