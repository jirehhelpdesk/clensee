package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="utilizer_profile_photo")
public class UtilizerProfilePhotoModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "photo_id")
	private int photo_id;
	
	@Column(name = "uti_ind_id")
	private int uti_ind_id;
	
	@Column(name = "uti_profile_photo")
	private String uti_profile_photo;
	
	@Column(name = "cr_date")
	private Date cr_date;

		
	public int getPhoto_id() {
		return photo_id;
	}

	public void setPhoto_id(int photo_id) {
		this.photo_id = photo_id;
	}

	public int getUti_ind_id() {
		return uti_ind_id;
	}

	public void setUti_ind_id(int uti_ind_id) {
		this.uti_ind_id = uti_ind_id;
	}

	public String getUti_profile_photo() {
		return uti_profile_photo;
	}

	public void setUti_profile_photo(String uti_profile_photo) {
		this.uti_profile_photo = uti_profile_photo;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

}
