package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="email_sent_status")
public class EmailSentStatusModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)

	@Column(name = "email_unique_id")
	private Integer email_unique_id;
	
	@Column(name = "email_id")
	private String email_id;	
	
	@Column(name = "subject")
	private String subject;	
	
	@Column(name = "message_body")
	private String message_body;	
		
	@Column(name = "sent_date")
	private Date sent_date;	
	
	@Column(name = "sent_report")
	private String sent_report;

	
	public Integer getEmail_unique_id() {
		return email_unique_id;
	}

	public void setEmail_unique_id(Integer email_unique_id) {
		this.email_unique_id = email_unique_id;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage_body() {
		return message_body;
	}

	public void setMessage_body(String message_body) {
		this.message_body = message_body;
	}

	public Date getSent_date() {
		return sent_date;
	}

	public void setSent_date(Date sent_date) {
		this.sent_date = sent_date;
	}

	public String getSent_report() {
		return sent_report;
	}

	public void setSent_report(String sent_report) {
		this.sent_report = sent_report;
	}	
	
	
	
}
