package com.kyc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="features_details")
public class FeaturesDetailsModel implements Serializable{
private static final long serialVersionUID = -723583058586873479L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "feature_id")
	private Integer feature_id;
	
	@Column(name = "feature_name")
	private String feature_name;
	
	@Column(name = "feature_value")
	private String feature_value;
	
	@Column(name = "feature_price")
	private Integer feature_price;
	
	
	private String feature_description;
	
	@Column(name = "feature_category")
	private String feature_category;
	
	@Column(name = "cr_by")
	private String cr_by;
	
	@Column(name = "cr_date")
	private Date cr_date;
	
	@Column(name = "mod_by")
	private String mod_by;
	
	@Column(name = "mod_date")
	private Date mod_date;
	
	public Integer getFeature_id() {
		return feature_id;
	}

	public void setFeature_id(Integer feature_id) {
		this.feature_id = feature_id;
	}

	public String getFeature_name() {
		return feature_name;
	}

	public void setFeature_name(String feature_name) {
		this.feature_name = feature_name;
	}

	public String getFeature_value() {
		return feature_value;
	}

	public void setFeature_value(String feature_value) {
		this.feature_value = feature_value;
	}

	public Integer getFeature_price() {
		return feature_price;
	}

	public void setFeature_price(Integer feature_price) {
		this.feature_price = feature_price;
	}

	public String getFeature_description() {
		return feature_description;
	}

	public void setFeature_description(String feature_description) {
		this.feature_description = feature_description;
	}

	public String getFeature_category() {
		return feature_category;
	}

	public void setFeature_category(String feature_category) {
		this.feature_category = feature_category;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date date) {
		this.cr_date = date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

	

	
}
