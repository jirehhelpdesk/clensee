package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="profile_picture")
public class ProfilePictureModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)

	@Column(name ="pic_id")
	private int pic_id;
	
	@Column(name="kyc_ind_id")
	private int kyc_ind_id;
	
	@Column(name="profile_picture_name")
	private String profile_picture_name;
	
	@Column(name="cr_date")
	private Date cr_date;

	public int getPic_id() {
		return pic_id;
	}

	public void setPic_id(int pic_id) {
		this.pic_id = pic_id;
	}

	public int getKyc_ind_id() {
		return kyc_ind_id;
	}

	public void setKyc_ind_id(int kyc_ind_id) {
		this.kyc_ind_id = kyc_ind_id;
	}

	public String getProfile_picture_name() {
		return profile_picture_name;
	}

	public void setProfile_picture_name(String profile_picture_name) {
		this.profile_picture_name = profile_picture_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	
		
}
