package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="plan_details")
public class PlanDetailsModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="plan_id")
	private int plan_id;
	
	@Column(name="plan_name")
	private String plan_name;
	
	@Column(name="plan_type")
	private String  plan_type;
	
	@Column(name="plan_domain")
	private String  plan_domain;
	
	@Column(name="plan_price")
	private float  plan_price;
	
	@Column(name="cr_by")
	private String cr_by;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="mod_by")
	private String mod_by;
	
	@Column(name="mod_date")
	private Date mod_date;

	
	@Column(name ="feature_ids")
	private String feature_ids;

	
	public int getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}

	public String getPlan_name() {
		return plan_name;
	}

	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}

	public String getPlan_type() {
		return plan_type;
	}

	public void setPlan_type(String plan_type) {
		this.plan_type = plan_type;
	}
	
	public float getPlan_price() {
		return plan_price;
	}
		
	public String getPlan_domain() {
		return plan_domain;
	}

	public void setPlan_domain(String plan_domain) {
		this.plan_domain = plan_domain;
	}

	public void setPlan_price(float plan_price) {
		this.plan_price = plan_price;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getMod_by() {
		return mod_by;
	}

	public void setMod_by(String mod_by) {
		this.mod_by = mod_by;
	}

	public Date getMod_date() {
		return mod_date;
	}

	public void setMod_date(Date mod_date) {
		this.mod_date = mod_date;
	}

	public String getFeature_ids() {
		return feature_ids;
	}

	public void setFeature_ids(String feature_ids) {
		this.feature_ids = feature_ids;
	}

	
	
	
	
	
}
