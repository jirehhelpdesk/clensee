package com.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="sscdoc")
public class SscDocModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "docid")
	private int docid;
	
	@Column(name="orgname")
	private String orgname;
	
	@Column(name="typeofcourse")
    private String typeofcourse;
	
	@Column(name="yearofpass")
	private String yearofpass;
	
	@Column(name="percentage")
	private String percentage;
	
	@Column(name="state")
	private String state;
	
	@Column(name="certf_id")
	private String certf_id;
	
	@Column(name="certf_Issue_auth")
	private String certf_Issue_auth;
	
	
	@Column(name="pincode")
	private String pincode;


	public int getDocid() {
		return docid;
	}


	public void setDocid(int docid) {
		this.docid = docid;
	}


	public String getOrgname() {
		return orgname;
	}


	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}


	public String getTypeofcourse() {
		return typeofcourse;
	}


	public void setTypeofcourse(String typeofcourse) {
		this.typeofcourse = typeofcourse;
	}


	public String getYearofpass() {
		return yearofpass;
	}


	public void setYearofpass(String yearofpass) {
		this.yearofpass = yearofpass;
	}


	public String getPercentage() {
		return percentage;
	}


	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getCertf_id() {
		return certf_id;
	}


	public void setCertf_id(String certf_id) {
		this.certf_id = certf_id;
	}


	public String getCertf_Issue_auth() {
		return certf_Issue_auth;
	}


	public void setCertf_Issue_auth(String certf_Issue_auth) {
		this.certf_Issue_auth = certf_Issue_auth;
	}


	public String getPincode() {
		return pincode;
	}


	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	
}
