package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ind_reset_password")	
public class ResetPasswordModel {

	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="password_id")
	private int password_id;
	
	@Column(name="ind_visibility_code")
	private String ind_visibility_code;
	
	@Column(name="ind_status")
	private String ind_status;
	
	@Column(name="requested_date")
	private Date requested_date;

		
	public int getPassword_id() {
		return password_id;
	}

	public void setPassword_id(int password_id) {
		this.password_id = password_id;
	}

	public String getInd_visibility_code() {
		return ind_visibility_code;
	}

	public void setInd_visibility_code(String ind_visibility_code) {
		this.ind_visibility_code = ind_visibility_code;
	}

	public String getInd_status() {
		return ind_status;
	}

	public void setInd_status(String ind_status) {
		this.ind_status = ind_status;
	}

	public Date getRequested_date() {
		return requested_date;
	}

	public void setRequested_date(Date requested_date) {
		this.requested_date = requested_date;
	}	
}
