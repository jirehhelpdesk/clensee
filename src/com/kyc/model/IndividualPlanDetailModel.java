package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="individual_plan_details")
public class IndividualPlanDetailModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ind_plan_id")
	private int ind_plan_id;
	
	@Column(name = "ind_id")
	private int ind_id;
	
	@Column(name = "plan_name")
	private String plan_name;
	
	@Column(name = "cr_date")
	private Date cr_date;

		
	public int getInd_plan_id() {
		return ind_plan_id;
	}

	public void setInd_plan_id(int ind_plan_id) {
		this.ind_plan_id = ind_plan_id;
	}

	public int getInd_id() {
		return ind_id;
	}

	public void setInd_id(int ind_id) {
		this.ind_id = ind_id;
	}

	public String getPlan_name() {
		return plan_name;
	}

	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	
	
}
