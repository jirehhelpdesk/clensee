package com.kyc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="visibility_setting")
public class VisibilityModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	
	@Column(name = "visibility_id")
	private int visibility_id;
		
	@Column(name = "kyc_ind_id")
	private int kyc_ind_id;

	@Column(name="registration_visibility")
	private String registration_visibility;
	
	@Column(name="basic_visibility")
	private String basic_visibility;
	
	@Column(name="family_visibility")
	private String family_visibility;
	
	@Column(name="social_account_visibility")
	private String social_account_visibility;

	
	
	public int getVisibility_id() {
		return visibility_id;
	}

	public void setVisibility_id(int visibility_id) {
		this.visibility_id = visibility_id;
	}

	public int getKyc_ind_id() {
		return kyc_ind_id;
	}

	public void setKyc_ind_id(int kyc_ind_id) {
		this.kyc_ind_id = kyc_ind_id;
	}

	public String getRegistration_visibility() {
		return registration_visibility;
	}

	public void setRegistration_visibility(String registration_visibility) {
		this.registration_visibility = registration_visibility;
	}

	public String getBasic_visibility() {
		return basic_visibility;
	}

	public void setBasic_visibility(String basic_visibility) {
		this.basic_visibility = basic_visibility;
	}

	public String getFamily_visibility() {
		return family_visibility;
	}

	public void setFamily_visibility(String family_visibility) {
		this.family_visibility = family_visibility;
	}

	public String getSocial_account_visibility() {
		return social_account_visibility;
	}

	public void setSocial_account_visibility(String social_account_visibility) {
		this.social_account_visibility = social_account_visibility;
	}
			
}
