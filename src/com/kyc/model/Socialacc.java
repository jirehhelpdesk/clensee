package com.kyc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="social_account_details")
public class Socialacc  implements Serializable{

	private static final long serialVersionUID = -723583058586873479L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)

	

	
	@Column(name = "social_det_id")
	private Integer social_det_id;
	
	@Column(name = "website_name")
	private String website_name;
	
	@Column(name = "website_link")
	private String website_link;
	

	@Column(name = "twitter_link")
	private String twitter_link;
	

	@Column(name = "twitter_name")
	private String twitter_name;
	

	@Column(name = "linkedln_link")
	private String linkedln_link;
	

	@Column(name = "linkedln_name")
	private String linkedln_name;
	
	
	@Column(name = "ind_kyc_id")
	private String ind_kyc_id;
	

	@Column(name = "cr_date")
	private Date cr_date;
	
	@Column(name = "cr_by")
	private String cr_by;
	
	
	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getCr_by() {
		return cr_by;
	}

	public void setCr_by(String cr_by) {
		this.cr_by = cr_by;
	}

	public String getTwitter_link() {
		return twitter_link;
	}

	public void setTwitter_link(String twitter_link) {
		this.twitter_link = twitter_link;
	}

	public String getTwitter_name() {
		return twitter_name;
	}

	public void setTwitter_name(String twitter_name) {
		this.twitter_name = twitter_name;
	}

	public String getLinkedln_link() {
		return linkedln_link;
	}

	public void setLinkedln_link(String linkedln_link) {
		this.linkedln_link = linkedln_link;
	}

	public String getLinkedln_name() {
		return linkedln_name;
	}

	public void setLinkedln_name(String linkedln_name) {
		this.linkedln_name = linkedln_name;
	}

	public Integer getSocial_det_id() {
		return social_det_id;
	}

	public void setSocial_det_id(Integer social_det_id) {
		this.social_det_id = social_det_id;
	}
	public String getInd_kyc_id() {
		return ind_kyc_id;
	}

	public void setInd_kyc_id(String ind_kyc_id) {
		this.ind_kyc_id = ind_kyc_id;
	}

	public String getWebsite_name() {
		return website_name;
	}

	public void setWebsite_name(String website_name) {
		this.website_name = website_name;
	}

	public String getWebsite_link() {
		return website_link;
	}

	public void setWebsite_link(String website_link) {
		this.website_link = website_link;
	}

	
	
}