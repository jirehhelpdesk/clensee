package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="non_kyc_user_code_details")
public class NonKycUserCodeDetailsModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "non_kyc_user_code_id")
	private int non_kyc_user_code_id;
		
	@Column(name = "sender_ind_id")
	private int sender_ind_id;
	
	@Column(name = "sender_name")
	private String sender_name;
	
	@Column(name = "reciver_email_id")
	private String reciver_email_id;
	
	@Column(name = "reciver_name")
	private String reciver_name;
	
	@Column(name = "code_details")
	private String code_details;
	
	@Column(name = "send_date")
	private Date send_date;

	@Column(name = "description")
	private String description;
	
	public Integer getNon_kyc_user_code_id() {
		return non_kyc_user_code_id;
	}

	public void setNon_kyc_user_code_id(Integer non_kyc_user_code_id) {
		this.non_kyc_user_code_id = non_kyc_user_code_id;
	}

	public int getSender_ind_id() {
		return sender_ind_id;
	}

	public void setSender_ind_id(int sender_ind_id) {
		this.sender_ind_id = sender_ind_id;
	}

	public String getSender_name() {
		return sender_name;
	}

	public void setSender_name(String sender_name) {
		this.sender_name = sender_name;
	}

	public String getReciver_email_id() {
		return reciver_email_id;
	}

	public void setReciver_email_id(String reciver_email_id) {
		this.reciver_email_id = reciver_email_id;
	}

	public String getReciver_name() {
		return reciver_name;
	}

	public void setReciver_name(String reciver_name) {
		this.reciver_name = reciver_name;
	}

	public String getCode_details() {
		return code_details;
	}

	public void setCode_details(String code_details) {
		this.code_details = code_details;
	}

	public Date getSend_date() {
		return send_date;
	}

	public void setSend_date(Date send_date) {
		this.send_date = send_date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
