package com.kyc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="clensee_payment_invoice_records")
public class ClenseeInvoiceModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "invoice_serial_no")
	private int invoice_serial_no;
	
	@Column(name = "invoice_id")
	private String invoice_id;
	
	@Column(name = "clensee_unique_id")
	private Integer clensee_unique_id;
	
	@Column(name = "invoice_category")
	private String invoice_category;
	
	@Column(name = "invoice_done_for")
	private String invoice_done_for;
	
	@Column(name = "invoice_generated_date")
	private Date invoice_generated_date;
	
	@Column(name = "invoice_payment_amount")
	private String invoice_payment_amount;
	
	@Column(name = "invoice_description")
	private String invoice_description;
	
	
	public int getInvoice_serial_no() {
		return invoice_serial_no;
	}

	public void setInvoice_serial_no(int invoice_serial_no) {
		this.invoice_serial_no = invoice_serial_no;
	}

	public String getInvoice_id() {
		return invoice_id;
	}

	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}

	public Integer getClensee_unique_id() {
		return clensee_unique_id;
	}

	public void setClensee_unique_id(Integer clensee_unique_id) {
		this.clensee_unique_id = clensee_unique_id;
	}

	public String getInvoice_category() {
		return invoice_category;
	}

	public void setInvoice_category(String invoice_category) {
		this.invoice_category = invoice_category;
	}

	public String getInvoice_done_for() {
		return invoice_done_for;
	}

	public void setInvoice_done_for(String invoice_done_for) {
		this.invoice_done_for = invoice_done_for;
	}

	public Date getInvoice_generated_date() {
		return invoice_generated_date;
	}

	public void setInvoice_generated_date(Date invoice_generated_date) {
		this.invoice_generated_date = invoice_generated_date;
	}

	public String getInvoice_payment_amount() {
		return invoice_payment_amount;
	}

	public void setInvoice_payment_amount(String invoice_payment_amount) {
		this.invoice_payment_amount = invoice_payment_amount;
	}

	public String getInvoice_description() {
		return invoice_description;
	}

	public void setInvoice_description(String invoice_description) {
		this.invoice_description = invoice_description;
	}
	
	
}
