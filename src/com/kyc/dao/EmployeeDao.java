package com.kyc.dao;

import java.util.List;

import com.kyc.model.Employee;

/**
 * @author Sharma Ragi
 *
 */
public interface EmployeeDao {
	
	public void addEmployee(Employee employee);

	public List<Employee> listEmployeess();
	
	public Employee getEmployee(int empid);
	
	public void deleteEmployee(Employee employee);
}
