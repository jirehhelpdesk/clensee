package com.kyc.dao;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.kyc.bean.DocumentHierarchyBean;
import com.kyc.model.AcademicFileUploadModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.DocumentModel;
import com.kyc.model.SalarySlipModel;
import com.kyc.model.SscDocModel;

public interface DocDao {

	public String insertKYCDocDetailsInRegn(DocumentDetailModel docdetailmodel);
	
	public int getIndIdviaDocId(int doc_id);
	
	public List<String> getKycRegistrationStatus(String kycId);
	
	public String getKycIdViaEmailId(String emailId);
	
	public String uploadSSC(SscDocModel sscdocmodel);
	
	public String uploadDocument(DocumentModel docmodel);
	
	public DocumentModel getDetail(String kycid);
	
	public List<DocumentModel> listDocuments();
	
	public String getAcademicComp();
	
	public String getAcademicComp2();
	
	public String getAcademicComp3();
	
	public String getFinancialComp();
	
	public String getEmployementComp();
	
	public String getBusinessComp();
	
	public String uploadDocumentDetail(DocumentDetailModel docdetailmodel);
	
	public String uploadotherDocumentDetail(DocumentDetailModel docdetailmodel);
	
	public String updateDocumentDetail(DocumentDetailModel docdetailmodel);
	
	public String getDocsDataSSC(DocumentDetailModel docdetailmodel);
	
	public String getDocsDataInter(DocumentDetailModel docdetailmodel);
	
	public String getDocsDataGraduate(DocumentDetailModel docdetailmodel);
	
	public String getDocsDataFinancial(DocumentDetailModel docdetailmodel);
	
	public String getDocsDataEmployement(DocumentDetailModel docdetailmodel);
	
	public String getDocsDataBusiness(DocumentDetailModel docdetailmodel);
	
	public List<String> getAcademicDocComp();
	
	public String getDocsVeiwOrNew(DocumentHierarchyBean docbean);
	
	public List<DocumentDetailModel> getHistory(DocumentDetailModel docdetails,HttpSession session);
	
	public String getViewMore(DocumentDetailModel docdetails);
	
	public String getDocName(DocumentDetailModel docdetails);
	
	public String getOtherDocsVeiwOrNew(DocumentHierarchyBean docbean);
	
	public List<Integer> getDocIds(DocumentDetailModel docdetails);
	
	public String getKYCComp();
	
    public String getstatus(DocumentDetailModel docdetails);
    
    public String getHierarchy(DocumentDetailModel docdetails);
    
    public String getfinDocName(DocumentDetailModel docdetailmodel);
    
	public List<DocumentDetailModel> getfinHistory(DocumentDetailModel docdetails,HttpSession session);
	
	public List<DocumentDetailModel> searchfinancial(String doc_year,String docType,String docMode,int user);
	
	public List<DocumentDetailModel> backFinancial(String backValue,int user); 
	
	public String getKYCdatas(DocumentDetailModel docdetails);
	
	public List<DocumentDetailModel> getEmpHistory(DocumentDetailModel docdetails);
	
	public String getrecentDocName(DocumentDetailModel docdetails);
	
	public String findLatestDocName(int id,String docname);
	
	public Integer findYear(String Ind_id);
    
	public String getAcademicStatus(int id);
	
	public String getOldComp(int indId);
	
	public String getIndName(int id);
	
	public String setPannelDocDetails(DocumentDetailModel docmodel);
	
	public String setKYCPannelDocDetails(DocumentDetailModel docmodel);
			
	public String getId(int id);
	
	public String updateKycId(String latest_kyc,int id);
	
	public String getFinancialDoc(int id);
	
	public String getEmpHierarchy();
	
	public String getEmpPanelStatusfromEmpDetails(int ind_id);
	
	public String getEmpPanelStatus(int ind_id);
	
	public String getEmpDetails(String panel,int ind_id);
	
	public String getEmpInfo(int ind_id);
	
	public String getEmpDocNames(int Individual_Id,String orgName);
	
	public String getEmpHistoryDetails(int doc_id);
	
	public String savePaySlips(SalarySlipModel salSlipModel);
	
	public List<String> getSalarySlipsNameAsPerUpload(SalarySlipModel salSlipModel);
	
	public List<String> getPaySlipsName(SalarySlipModel salSlipModel);
	
	public List<String> getPaySlipsMonthYear(SalarySlipModel salSlipModel);
	
	public List<String> getFinancialData(int id);
	
	public String getFinancialDataviaDocName(int i,String docName);
	
	public String uploadEmpDocumentDetail(DocumentDetailModel docdetailmodel);
	
	public String getFinancialDocName(int i,String finName);
	
	public List<DocumentDetailModel> getFinancialHistory(int i,String docName);
	
	public List<String> getMonthSalarySlip(int i1,String orgName);
	
	public List<String> getEmpDataofAllCompany(int Individual_Id);
	
	public String academicFileUpload(AcademicFileUploadModel academicFileDetails);
	
    public List<String> getAcademicUploadedDocName(int kyc_ind_id);
    
    public List<Float> getAcademicUploadedDocSize();
    
    public List<String> getAcademicUploadedDocDes();
    
    public String getDeleteAllFromUploadTableDetails(int kyc_ind_id);
    
    public String getoldDocumentList(int ind_id,String docType);
    
    public String getAcademicDataById(int doc_id);
    
	public String getAcademicDocNameById(int doc_id);
	
	public String getEmployementLatestData(int ind_id,String companyName);
	
	public String getKYCLatestDocsData(int ind_id);
	
	public String getKYCLatestDocName(int ind_id);
	
	public String uploadKYCDocumentDetail(DocumentDetailModel docdetailmodel);
	
	public String getLatestKYCPannel(int i);
	
	public List<String> getAllListKYCDocsData(int ownIndId);
	
	public List<String> getAcademicFaultFileName(int ownIndId);
	
	public String deleteAcademicFaultFileDetails(int ownIndId);
	
	public String getEmployeeDetailsEachCompnay(String panelNames,int individualId);
	
	public List<String> getKycIdFromDocumentDetails(String kycId);
	
	public String getCreatedDateOfPaySlips(String payslipsName,SalarySlipModel salSlipModel);
	
	public String getEmpDetailsExceptThisCompany(String panel1,int individualId,String companyName);
	
	public String checkOrgNameExistOrNot(int ind_id,String orgName);
	
	public String getEmpPanelStatusAsofNow(int i);
	
	public String getPannelWithId(int ind_id,String old_selComp);
	
	public String getKYCdataAsUtilizerDetails(DocumentDetailModel docdetails);
	
	public String getLatestKycBackSideDoc(String ind_id,String kycDocType);
	
	public String getFinancialTypeFromHierarchy();
	
	public  Date getHitoryDateOfThisId(int doc_id);
	
	//public Date getHistoryCreatedDatebyDocId(int doc_id);
}
