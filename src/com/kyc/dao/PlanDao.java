package com.kyc.dao;

import java.util.Date;
import java.util.List;

import com.kyc.model.ClenseeInvoiceModel;
import com.kyc.model.FeaturesDetailsModel;
import com.kyc.model.PaymentDetailsModel;
import com.kyc.model.PlanDetailsModel;

public interface PlanDao {
	public String addFeaturesDetail(FeaturesDetailsModel featuresDetailsModel);
	public String featurename();
	public String plannameview();
	
	public List<String> featurecategory(String featureCategory);
	
	public String getPlanFeatureId(String featureCategory,String planName);
	
	public List<FeaturesDetailsModel> getallfeatures(FeaturesDetailsModel FeaturesDetailsModel);
	public String addPlanDetail(PlanDetailsModel PlanDetailsModel);
	public String getplanname(FeaturesDetailsModel FeaturesDetailsModel);
	public List<FeaturesDetailsModel> getcurrentplan(FeaturesDetailsModel FeaturesDetailsModel);
	public List<FeaturesDetailsModel> selectedviewfeature(FeaturesDetailsModel FeaturesDetailsModel);
	public List<FeaturesDetailsModel> selectedviewplan(FeaturesDetailsModel FeaturesDetailsModel);
	
	public List<FeaturesDetailsModel> getAllPlanDetailsBytheDetails(String plan_category,String plan_domain,String plan_name);
	
	public List<PlanDetailsModel> getorganiseplandetail(PlanDetailsModel PlanDetailsModel);
	public String rangeselectedFeaturesDetail(FeaturesDetailsModel FeaturesDetailsModel);
	
	public String changeplan(int ind_id,String requestedPlanName);
	
	public List<String> getplanhasfeature(PlanDetailsModel planDetailsModel);
	public List<FeaturesDetailsModel> getplanhasfeatureorganise(PlanDetailsModel planDetailsModel);
	public String deleteplanhasfeature(FeaturesDetailsModel FeaturesDetailsModel);
	public String deletePlanDetails(String planType,String PlanName);
	public String getplannameview();
	
	public List<FeaturesDetailsModel> getFeatureDetailsByCategory(String featureCategory,String featureName);
	
	public List<FeaturesDetailsModel> getFeaturesThroughCategory(String featureCategory);
	
	public List<FeaturesDetailsModel> getallfeaturesbyCategoryAndName(String featureCategory,String featureName);
	
	public List<String> getPlanNameByCategory(String planCategory);
	
	public List<String> getPlanNameforUtilizer(String planCategory);
	
	public float getPlanPricAsPerPlanCategory(String plan_category,String plan_domain,String plan_name);
	
	public float getPlanPricAsPrCategory(String planCategory);
	
	public String getPlanFeatureIdDetails(String planName,String planType);
	
	public String getFeaturesForUtilizer(String planName,String planType,String planDomain);
	
	public int getPlanIdByNameAndType(String plan_type,String plan_name,String plan_domain);
	
	public String getPlanDetails(String basicFeatures_Id,String featureNames);
	
	public float  getPlanPrice(String featureCategory,String planName);
	
	public float getPlanPriceforUtilizer(String featureCategory,String planDomain,String planName);
	
	public String getKycId(int ind_id);
	
	public int getCountGeneratedCode(String kyc_id);
	
	public int getCountApplyCode(String kyc_id);
	
	public int getCountAllowAccess(String kyc_id);
	
	public int getCountRevokeAccess(String kyc_id);
	
	public float getTotalFileStorage(int ind_id);
	
	public String getCurrentPlanName(int ind_id);
	
	public String getFeatureNameById(int feature_id);
	
	public String getFeatureValue(int feature_id);
	
	public int noOfFullProfileAsOfNow(String kyc_id);
	
	public List<FeaturesDetailsModel> getUtilizerCurrentplan(FeaturesDetailsModel FeaturesDetailsModels);
	
	public String getUtilizerplanname(int uti_Ind_id);	
	
	public String getUtilizerPlanDomain(int uti_Ind_id);
	
	public String getUtilizerPlanFeatureId(String featureCategory,String planName,String planDomain);
	
	public String getUtilizerUniqueId(int uti_Ind_id);
	
	public String getUtilizerFeatureIdsAsPerPlan(String utilizerCategory,String planDomain,String planName);
	
	public int getCountAllowAccessByUtilizer(String utiUniqueId);
	
	public String migrateUtilizerPlan(String requestedPlanName,int uti_Ind_id);
	
	public int findUtiid(String uti_kycid);
	
	public String getCurrentPlanNameOfUtilizer(int uti_indid);
	
	public String getPlanFeatureIdDetailsForUtilizer(String planName,String domianName,String category);
	
	public int noOfFullProfileAsOfNowOfUtilizer(int uti_indid);
	
	public int noOfSMSAsOfNowAsOfNowOfUtilizer(int uti_Indid);
	
	public float getPlanPriceAsPerRequest(String requestedPlanName);
	
	public String getCurrentMobileNo(int ind_id);
	 
	public String getCurrentEmailId(int ind_id);
	
	public String getFullName(int ind_id);
	
	public String getCheckPlanExistOrNot4Ind(String planName,String planType);
	
	public String getCheckPlanExistOrNot4Uti(String planName,String planDomain,String planType);
	
	public String getCheckPlanExistOrNot4Cor(String planName,String planType);
	
	public List<String> getPlanNameForUti(String planCategory,String planDomain);
	
	public List<FeaturesDetailsModel> getAllFeatureDetailsByPlanType(String planType);
	
	public List<FeaturesDetailsModel> getAllfeatureDetailsByPlanDetails(String planName,String planType,String planDomain);
	
	public int noOfSMSAsOfNow(int ind_id);
	
	public Date getUtiActivatedPlanDate(int uti_Ind_id);
	
	public String savePaymentRecords(PaymentDetailsModel payu);
	
	public int getUniquePaymentId(PaymentDetailsModel payu);
	
	public String updatePaymentStatus(int paymentId,String paymentAmount,String paymentStatus,String paymentTranId);
	
	public String getFeatureIdByFeatureNames(String featureCategory,String featurecategory);
	
	
	public String saveInvoiceDetails(ClenseeInvoiceModel invoiceData);
	
	public int getLastInvoiceNumber();
	
}
    