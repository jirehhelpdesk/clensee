package com.kyc.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kyc.model.AccessDetailsModel;
import com.kyc.model.BasicDetail;
import com.kyc.model.CodesDetailsModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.IndSignupModel;
import com.kyc.model.NonKycUserCodeDetailsModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.UtiliserProfileModel;


@Repository("accdao")
public class AccessDaoImpl implements AccessDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public List<IndSignupModel> findUser(IndSignupModel codedetail)
	{
		
		List<IndSignupModel> lsdata = new ArrayList<IndSignupModel>();
				
		if("KYC_ID".equals(codedetail.getMobileno()))
		{
		String hql="from IndSignupModel where kycid like '"+codedetail.getPassword()+"'";
		lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		}
		
		if("Email_id".equals(codedetail.getMobileno()))
		{
			if(codedetail.getPassword().contains("@"))
			{
				if(codedetail.getPassword().startsWith("@"))
				{
					
				}
				else
				{
					int splitSearch = codedetail.getPassword().split("@").length;
					System.out.println("Length of String="+splitSearch);
					if(splitSearch==2)
					{
						String hqlViaemail = "from IndSignupModel where emailid like '%"+codedetail.getPassword()+"%'";					
						lsdata = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
					}
					if(splitSearch==1)
					{
						String hqlViaemail = "from IndSignupModel where emailid like '"+codedetail.getPassword()+"%'";					
						lsdata = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
					}
				}						
			}
			else
			{
				String hqlViaemail = "from IndSignupModel where emailid like '%"+codedetail.getPassword()+"%@%'";					
				lsdata = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();					
			}
		
		}
		
		if("Name".equals(codedetail.getMobileno()))
		{			
		   if(codedetail.getPassword().contains(" "))
			{			   
			      List<IndSignupModel> NameData2space = new ArrayList<IndSignupModel>();
				  String hqlForSpaceFN = "from IndSignupModel where individual_full_name like '%"+codedetail.getPassword()+"%'";			   
				  NameData2space = sessionFactory.getCurrentSession().createQuery(hqlForSpaceFN).list();	
				  
				  if(NameData2space.size()>0)
				  {				  
					  lsdata.addAll(NameData2space);			  
				  }		
				  				 
				  return lsdata;
			}
			else
			{
				String hql="from IndSignupModel where firstname like '%"+codedetail.getPassword()+"%'";
				lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				if(lsdata.size()>0)
				{
					List<IndSignupModel> middleNameData = new ArrayList<IndSignupModel>();
					
					String middleNameHQL ="from IndSignupModel where middlename like '%"+codedetail.getPassword()+"%'";
					middleNameData = sessionFactory.getCurrentSession().createQuery(middleNameHQL).list();
													  
			     	if(middleNameData.size()>0)
			     	{
			     		  /* Removing Duplicacy Values */
			     		
			     		 for(int i=0;i<lsdata.size();i++)
						  {							
							  if(middleNameData.contains(lsdata.get(i)))
							  {
								  middleNameData.remove(lsdata.get(i));
							  }																			
						  }
						 
						  /* End of Removing Duplicacy Values */
						  
						  lsdata.addAll(middleNameData);  						  
			     	}
					 
					   List<IndSignupModel> lastNameData = new ArrayList<IndSignupModel>();
						
					   String lastNameHQL ="from IndSignupModel where lastname like '%"+codedetail.getPassword()+"%'";
					   lastNameData = sessionFactory.getCurrentSession().createQuery(lastNameHQL).list();
						
					   if(lastNameData.size()>0)
					   {
						   /* Removing Duplicacy Values */
							  
					     	  System.out.println("check"+lsdata.size());
					  					
							  for(int i=0;i<lsdata.size();i++)
							  {							
								  if(lastNameData.contains(lsdata.get(i)))
								  {
									  lastNameData.remove(lsdata.get(i));
								  }																			
							  }
							 
						  /* End of Removing Duplicacy Values */
							  
							 lsdata.addAll(lastNameData);  							  
					   }
				}
				else
				{               			
					String middleNameHQL ="from IndSignupModel where middlename like '%"+codedetail.getPassword()+"%'";
					lsdata = sessionFactory.getCurrentSession().createQuery(middleNameHQL).list();
													  
			     	if(lsdata.size()>0)
			     	{
			     		List<IndSignupModel> lastNameData = new ArrayList<IndSignupModel>();
						
						   String lastNameHQL ="from IndSignupModel where lastname like '%"+codedetail.getPassword()+"%'";
						   lastNameData = sessionFactory.getCurrentSession().createQuery(lastNameHQL).list();
							
						   if(lastNameData.size()>0)
						   {
							   /* Removing Duplicacy Values */
								  
						     	  System.out.println("check"+lsdata.size());
						  					
								  for(int i=0;i<lsdata.size();i++)
								  {							
									  if(lastNameData.contains(lsdata.get(i)))
									  {
										  lastNameData.remove(lsdata.get(i));
									  }																			
								  }
								 
							  /* End of Removing Duplicacy Values */
								  
								 lsdata.addAll(lastNameData);  							  
						   }						 
			     	}
			     	else
			     	{		     		  
						   String lastNameHQL ="from IndSignupModel where lastname like '%"+codedetail.getPassword()+"%'";
						   lsdata = sessionFactory.getCurrentSession().createQuery(lastNameHQL).list();					
			     	}		     	
			     				     	
				}
			}
			
			
		}
		
		
		return lsdata;		
		
	}
	
	
	@SuppressWarnings("unchecked")
	public String getLatestProfilePicture(int ind_id)
	{		
		String hql  ="select profile_picture_name from ProfilePictureModel where kyc_ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from ProfilePictureModel where kyc_ind_id="+ind_id+") ";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Data";
		}		
	}
	
	
	@SuppressWarnings("unchecked")
	public String checkIndAccessStatus(String viewedkycid,String ownKycId)
	{
		String statusHql = "select apply_code_count from CodesDetailsModel where code_sender='"+viewedkycid+"' and code_receiver='"+ownKycId+"' and cr_date=(select MAX(cr_date) from CodesDetailsModel where code_sender='"+viewedkycid+"' and code_receiver='"+ownKycId+"')";
		List<String> lsd =  sessionFactory.getCurrentSession().createQuery(statusHql).list();
		if(lsd.size()>0)
		{
			return lsd.get(0);
		}
		else
		{
			return "No Data";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findDocumentforKYC(HttpServletRequest request)
	{

		String docname=(String)request.getAttribute("doctype");
		int kyc_ind_id = Integer.parseInt((String)request.getAttribute("kyc_ind_id"));		
		
		String sqlQuery = "select doc_name from(select * from document_details order by cr_date desc) a where a.doc_type like '"+docname+"' and doc_name IS NOT NULL and a.kyc_ind_id = "+kyc_ind_id+" group by substr(a.doc_name,1,9)";
			
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sqlQuery).addScalar("doc_name", Hibernate.STRING).list();		
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findDocumentforAcademic(HttpServletRequest request)
	{

		String docname=(String)request.getAttribute("doctype");
		int kyc_ind_id = Integer.parseInt((String)request.getAttribute("kyc_ind_id"));		
		
		String sqlQuery = "select doc_name from(select * from document_details order by cr_date desc) a where a.doc_type like '"+docname+"' and doc_name IS NOT NULL and a.kyc_ind_id = "+kyc_ind_id+" group by substr(a.doc_name,1,15)";
			
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sqlQuery).addScalar("doc_name", Hibernate.STRING).list();		
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findDocumentforEmployement(HttpServletRequest request)
	{
		String docname=(String)request.getAttribute("doctype");
		int kyc_ind_id = Integer.parseInt((String)request.getAttribute("kyc_ind_id"));		
		
		String sqlQuery = "select doc_name from(select * from document_details order by cr_date desc) a where a.doc_type like '"+docname+"' and doc_name IS NOT NULL and a.kyc_ind_id = "+kyc_ind_id+" group by substr(a.doc_name,1,19)";
			
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sqlQuery).addScalar("doc_name", Hibernate.STRING).list();		
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findDocument(HttpServletRequest request)
	{
		String docname=(String)request.getAttribute("doctype");
		int kyc_ind_id = Integer.parseInt((String)request.getAttribute("kyc_ind_id"));		
		
		String sqlQuery = "select doc_name from(select * from document_details order by cr_date desc) a where a.doc_type like '"+docname+"' and doc_name IS NOT NULL and a.kyc_ind_id = "+kyc_ind_id+" group by substr(a.doc_name,1,50)";
			
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sqlQuery).addScalar("doc_name", Hibernate.STRING).list();		
		
		return lsdata;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findRecipentDetail(HttpServletRequest request)
	{
		int indid= (Integer)request.getAttribute("indid");
		String hql  ="select kycid from IndSignupModel where ind_id='"+indid+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
		
	@SuppressWarnings("unchecked")
	public String findCrName(String kycid)
	{
		String hql="select ind_id from IndSignupModel where kycid='"+kycid+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(!lsdata.isEmpty())
		{
			int ind_id = lsdata.get(0);
			
			String nameHql="from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
			List<RegHistoryModel> name = sessionFactory.getCurrentSession().createQuery(nameHql).list();
			
			String fullName = name.get(0).getFirst_name()+" "+name.get(0).getMiddle_name()+" "+name.get(0).getLast_name();
					
			return fullName;
		}
		else
		{
			return "Not There";
		}
	}
	
	@SuppressWarnings("unchecked")
	public int findInd_Id(String kycid)
	{
		String hql = "select ind_id from IndSignupModel where kycid ='"+kycid+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String findDocView(String docname)
	{
		String hql = "select doc_name from DocumentDetailModel where doc_name like '%"+docname+"%'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);		
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findAsperType(IndSignupModel codedetail)
	{
         List<String> lsdata = new ArrayList<String>();
		
		System.out.println("  "+codedetail.getMobileno());
		System.out.println("  " +codedetail.getPassword());
		
		if("KYC_ID".equals(codedetail.getMobileno()))
		{
		String hql="select kycid from IndSignupModel where kycid like '%"+codedetail.getPassword()+"%'";
		lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		}
		
		if("Email_id".equals(codedetail.getMobileno()))
		{
			String hql="select emailid from IndSignupModel where emailid like '%"+codedetail.getPassword()+"%'";
			lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		}
		
		if("Name".equals(codedetail.getMobileno()))
		{
			String hql="select firstname from IndSignupModel where firstname like '%"+codedetail.getPassword()+"%' or middlename like '%"+codedetail.getPassword()+"%' or lastname like '%"+codedetail.getPassword()+"%'";
			lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		}
		
		return lsdata;
	}
	
	public String saveCodedetails(CodesDetailsModel codemodel)
	{
		
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(codemodel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}

	
	@SuppressWarnings("unchecked")
	public List<IndSignupModel> viewUser(IndSignupModel individual)
	{
		String hql = "from IndSignupModel where kycid='"+individual.getKycid()+"'";		
		List<IndSignupModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<CodesDetailsModel> findApplyedCode(String kyc_id)
	{
		String kid="";
		String query = "select kycid from IndSignupModel where ind_id = '"+kyc_id+"'";
		List<String> ls = sessionFactory.getCurrentSession().createQuery(query).list();
		if(!ls.isEmpty())
		{
		   kid = ls.get(0);	
		}
		
        List<CodesDetailsModel> lsdata1 = new ArrayList<CodesDetailsModel>();
		
		String sql = "select code_id from codes_details where cr_date >=now()-INTERVAL 1 DAY and code_receiver='"+kid+"' and code_status='1' ORDER BY code_id DESC";
		List<String> lsd =  sessionFactory.getCurrentSession().createSQLQuery(sql).list();
		
		if(lsd.size()>0)
		{
			String str= lsd.toString();	    
			String str1 = str.substring(1,str.length()-1);	
			String accID[] = str1.split(",");
	    
			for(int i=0;i<accID.length;i++)
			{				
				String hql = "from CodesDetailsModel where code_id="+accID[i]+"";
				lsdata1.addAll(sessionFactory.getCurrentSession().createQuery(hql).list());						
			}		
		    return lsdata1;		
		}
		else
		{
			return lsdata1;
		}				
	 }
	
	@SuppressWarnings("unchecked")
	public String findCodePatternForNonUser(int non_kyc_code)
	{
		String pattern="";
		String hql = "select code_details from NonKycUserCodeDetailsModel where non_kyc_user_code_id="+non_kyc_code+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(!lsdata.isEmpty())
		{
			return pattern=lsdata.get(0);
		}
		else
		{
			return pattern;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String findCodePattern(Integer code)
	{
		
		String pattern="";
		String hql = "select code_pattern from CodesDetailsModel where code_id="+code+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(!lsdata.isEmpty())
		{
			return pattern=lsdata.get(0);
		}
		else
		{
			return pattern;
		}
	}
	
	@SuppressWarnings("unchecked")
	public int findid(String kycid)
	{
		String hql="select ind_id from IndSignupModel where kycid ='"+kycid+"'";
		List<Integer> lsdata =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public int findyear(String docname,int ind_id)
	{
		String hql="select year(cr_date) from IndSignupModel where ind_id ="+ind_id+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	
	@SuppressWarnings("unchecked")
	public String getDomainHir(String domtype)
	{
		String hql = "select dom_hierarchy_data from DomainHierarchyModel where dom_name='"+domtype+"'";
		List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		System.out.println(ls.toString());
		return ls.toString();
	}
	
	@SuppressWarnings("unchecked")
	public String getDomain()
	{
		String domainData = "";
				
		String hql = "select domain_name from DomainLevelModel";
		List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();		
		if(ls.size()>0)
		{
			for(int i=0;i<ls.size();i++)
			{				
				domainData += ls.get(i) + ",";
			}
			domainData = domainData.substring(0, domainData.length()-1);
		}		
		return domainData;
	}
	
	public String saveAccessDetails(AccessDetailsModel accdetail)
	{
		AccessDetailsModel demo =accdetail;	
		
		String txStatus="";
		Session accessSession=sessionFactory.openSession();
		Transaction tx=null;
		try {		
		tx=accessSession.beginTransaction();
		accessSession.save(accdetail);
		//accessSession.flush();
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="ResultSuccess";
		} else {
			txStatus="failure";
		} 
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			accessSession.close();
		}
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UtiliserProfileModel> findUtiKycId(String ptname)
	{
		String hql = "from UtiliserProfileModel where uti_dom_hierarchy ='"+ptname+"'";
		List<UtiliserProfileModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
		return lsdata;
		
	}
	
	@SuppressWarnings("unchecked")
	public String findIndKycID(int acc_giver)
	{
		String hql = "select kycid from IndSignupModel where ind_id ="+acc_giver+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public int getPatternId(String acc_reciver)
	{
		
		String hql = "select pattern_id from UtiliserProfileModel where kyc_uti_id = '"+acc_reciver+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
		
	}
	
	@SuppressWarnings("unchecked")
	public String getPatternLogic(int prtn_id)
	{
		String hql = "select pattern_logic from PatternDetailsModel where pattern_id = '"+prtn_id+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<CodesDetailsModel> getAccessDetails(String ownId)
	{
				
		String hql_query1 = "select code_id  from codes_details where cr_date <= now() - INTERVAL 1 DAY and code_sender = '"+ownId+"'";
		List<AccessDetailsModel> accessData = sessionFactory.getCurrentSession().createSQLQuery(hql_query1).list();	
		
		Iterator iter = accessData.iterator();
		while(iter.hasNext())
		{
			org.hibernate.Query sub_hql = sessionFactory.getCurrentSession().createQuery("update CodesDetailsModel set code_status = :setstatus where code_id = :setId");
			sub_hql.setParameter("setstatus", "0");
			sub_hql.setParameter("setId", iter.next());			
		    int result  = sub_hql.executeUpdate();
		}
		
		String hql = "from CodesDetailsModel where code_sender='"+ownId+"' ORDER BY cr_date DESC";
		List<CodesDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
				
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<CodesDetailsModel> getIndAccessDetails(String ownId)
	{
		String hql = "from CodesDetailsModel where code_sender='"+ownId+"'";
		List<CodesDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<CodesDetailsModel> findHisInd(String kycid)
	{
		String hql = "from CodesDetailsModel where code_sender='"+kycid+"' ORDER BY cr_date DESC";
		List<CodesDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<NonKycUserCodeDetailsModel> getCodeDetailsforNonKycUser(int ind_Id)
	{
		String hql = "from NonKycUserCodeDetailsModel where sender_ind_id="+ind_Id+" ORDER BY send_date DESC";
		List<NonKycUserCodeDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<AccessDetailsModel> findHisUti(String kycid)
	{
		String hql = "from AccessDetailsModel where access_giver='"+kycid+"' ORDER BY access_given_date DESC";
		List<AccessDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		
       List<UtiliserProfileModel> utiFullName = new ArrayList<UtiliserProfileModel>();
		
		for(int i=0;i<lsdata.size();i++)
		{
			String utiFullNameHql = "from UtiliserProfileModel where kyc_uti_id='"+lsdata.get(i).getAccess_taker()+"'";
			List<UtiliserProfileModel>  utiName = sessionFactory.getCurrentSession().createQuery(utiFullNameHql).list();
			utiFullName.addAll(utiName);
		}
		
		for(int j=0;j<utiFullName.size();j++)
		{
			lsdata.get(j).setAccess_taker(utiFullName.get(j).getUti_first_name()+" "+utiFullName.get(j).getUti_middle_name()+" "+utiFullName.get(j).getUti_last_name());
		}
		
		return lsdata;
	}
	
	public String changeStatusOfUtiliser(String uti_unique_id,Date date,String access_status)
	{
		if(access_status.equals("Active"))
		{ 
		 	
		String kyc_id = uti_unique_id.replaceAll(" ","");
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update AccessDetailsModel set status = :setstatus  where access_taker = :settaker and  access_given_date = :setdate");
		hql.setParameter("setstatus", "0");
		hql.setParameter("settaker", kyc_id);
		hql.setParameter("setdate", date);		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "Revoked Your Access";
		}
		else
		{			
			return "Unable to do Try Again";
		}
		
		}
				
		else
		{
			
		String kyc_id = uti_unique_id.replaceAll(" ","");
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update AccessDetailsModel set uti_status = :setstatus  where access_taker = :settaker and  access_given_date = :setdate");
		hql.setParameter("setstatus", "1");
		hql.setParameter("settaker", kyc_id);
		hql.setParameter("setdate", date);		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "Access Allowed";
		}
		else
		{			
			return "Unable to do Try Again";
		}
		
		}
	}
	
	
	public String revokeUtiliser(String kycid,Date date,String access_status)
	{
		if(access_status.equals("Revoke"))
		{ 
		 	
			String kyc_id = kycid.replaceAll(" ","");
			org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update CodesDetailsModel set code_status = :setstatus  where code_receiver = :settaker and cr_date = :setdate");
			hql.setParameter("setstatus", "0");
			hql.setParameter("settaker", kyc_id);
			hql.setParameter("setdate", date);		
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "Successfully revoked access";
			   			
			}
			else
			{			
				return "Unable to do Try Again";
			}
		
		}
				
		else
		{
			
			String kyc_id = kycid.replaceAll(" ","");
			org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update CodesDetailsModel set code_status = :setstatus , cr_date = :setAccessDate  where code_receiver = :settaker and cr_date = :setdate");
			hql.setParameter("setstatus", "1");
			hql.setParameter("setAccessDate", new Date());
			hql.setParameter("settaker", kyc_id);
			hql.setParameter("setdate", date);		
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "Access Allowed";
			}
			else
			{			
				return "Unable to do Try Again";
			}
		
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String findName(String acc_sender)
	{
		
		String hql="from IndSignupModel where kycid='"+acc_sender+"'";
		List<IndSignupModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		String name  = lsdata.get(0).getFirstname()+" "+lsdata.get(0).getMiddlename()+" "+lsdata.get(0).getLastname();
	    
		return name;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findutiPattern(String kycid,String Date)
	{
		/*String hql  = "select access_pattern from AccessDetailsModel where access_taker = '"+kycid+"' and access_given_date ='"+Date+"'";*/
		String hql  = "select access_pattern from AccessDetailsModel where access_given_date ='"+Date+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public String findDocs(String docLike,int ind_id)
	{
        String hql = "select doc_name from DocumentDetailModel where doc_name like '%"+docLike+"%' and kyc_ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from DocumentDetailModel where doc_name like '%"+docLike+"%' and kyc_ind_id="+ind_id+")";
		
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>=1)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getRecevierStatus(String receiver,String sender)
	{
		String hql = "select code_receiver from CodesDetailsModel where code_sender='"+sender+"' and code_receiver='"+receiver+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>=1)
		{
			for(int i=0;i<lsdata.size();i++)
			{
				org.hibernate.Query hql1 = sessionFactory.getCurrentSession().createQuery("update CodesDetailsModel set code_status = :setstatus  where code_sender=:setSender and code_receiver=:setReceiver ");
				hql1.setParameter("setstatus", "0");
				hql1.setParameter("setSender", sender);
				hql1.setParameter("setReceiver", receiver);				
			    int result  = hql1.executeUpdate();			    
			}
			return "is There";
			
		}
		else
		{
			return "is not There"; 
		}
	}
	
	@SuppressWarnings("unchecked")
	public String findDocType(String docname,int ind_id)
	{
		String hql = "select doc_type from DocumentDetailModel where doc_name LIKE '%"+docname+"%' and kyc_ind_id="+ind_id+" ";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public String getProfilePic(int kycInd_id)
	{
		String hql = "select profile_picture_name from ProfilePictureModel where kyc_ind_id="+kycInd_id+" and cr_date=(select MAX(cr_date) from ProfilePictureModel where kyc_ind_id="+kycInd_id+")";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "no image";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getFirstName(int kycInd_id)
	{
		String hql = "select first_name from RegHistoryModel where ind_id="+kycInd_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+kycInd_id+")";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<AccessDetailsModel> getUtiAccessDetails(String ownId)
	{
		String hql  = "from AccessDetailsModel where access_giver='"+ownId+"' ORDER BY access_given_date DESC";
		List<AccessDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked") 
	public String getUtiliserName(String utiliser_unique_id)
	{
		String hql = "select uti_first_name from UtiliserProfileModel where kyc_uti_id='"+utiliser_unique_id+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
		
	}
	
	@SuppressWarnings("unchecked")
	public String getPatternfromId(int patternId)
	{
		String hql  = "select pattern_logic from PatternDetailsModel where pattern_id="+patternId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Data";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getUtiliserAccessId(int utiliserId)
	{
		String hql  = "select kyc_uti_id from UtiliserProfileModel where uti_ind_id="+utiliserId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
    @SuppressWarnings("unchecked")
	public List<String> getAllRegEmailId()
    {
    	String hql  = "select emailid from IndSignupModel";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
    }
	
	@SuppressWarnings("unchecked")
	public List<String> getAllRegHistoryEmailId()
	{
		String hql  = "select email_id from RegHistoryModel";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public int getLastGeneratedCodeId()
	{
		String hql  = "select code_id from CodesDetailsModel where cr_date=(select Max(cr_date) from CodesDetailsModel)";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public String getLastGeneratedCode()
	{
		String hql  = "select code_generated from CodesDetailsModel where cr_date=(select Max(cr_date) from CodesDetailsModel)";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Data";
		}
	}
		
	@SuppressWarnings("unchecked")
	public String getSenderName(int ind_id)
	{
		String hql  = "select first_name from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	public String getSaveNonKycCodeDetails(NonKycUserCodeDetailsModel nonUserModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(nonUserModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	public int orgApplyCodeCount(int code)
	{
		String hql1 = " UPDATE CodesDetailsModel SET apply_code_count='1' where code_id="+code+"";
		int status = sessionFactory.getCurrentSession().createQuery(hql1).executeUpdate();
		
		if(status>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public String getReciverName(int reciver_Ind_id)
	{
		String nameHql = "from RegHistoryModel where ind_id="+reciver_Ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+reciver_Ind_id+")";
		List<RegHistoryModel> nameData = sessionFactory.getCurrentSession().createQuery(nameHql).list();
		
		String fullName = nameData.get(0).getFirst_name()+" "+nameData.get(0).getMiddle_name()+" "+nameData.get(0).getLast_name();
		
		return fullName;
	}
	
	@SuppressWarnings("unchecked")
	public String getReciverEmailId(int reciver_Ind_id)
	{
		String emailHql = "from IndSignupModel where ind_id="+reciver_Ind_id+" and cr_date=(select MAX(cr_date) from IndSignupModel where ind_id="+reciver_Ind_id+")";
		List<IndSignupModel> emailData = sessionFactory.getCurrentSession().createQuery(emailHql).list();
		
		String emailId = emailData.get(0).getEmailid();
		
		return emailId;
	}
	
	@SuppressWarnings("unchecked")
	public String getReciverMobileNo(int reciver_Ind_id)
	{
		String mobilenoHql = "from RegHistoryModel where ind_id="+reciver_Ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+reciver_Ind_id+")";
		List<RegHistoryModel> mobileData = sessionFactory.getCurrentSession().createQuery(mobilenoHql).list();
		
		String  mobileNo = Long.toString(mobileData.get(0).getMobile_no());
		
		return mobileNo;
	}
	
    @SuppressWarnings("unchecked")
	public List<RegHistoryModel> getRegistrationDetails(int ind_id)
    {
    	String regHql = "from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
		List<RegHistoryModel> regData = sessionFactory.getCurrentSession().createQuery(regHql).list();
		
		return regData;
    }
	
	@SuppressWarnings("unchecked")
	public List<BasicDetail> getBasicDetailDetails(int ind_id)
	{
		String basicHql = "from BasicDetail where ind_kyc_id="+ind_id+" and cr_date=(select MAX(cr_date) from BasicDetail where ind_kyc_id="+ind_id+")";
		List<BasicDetail> basicData = sessionFactory.getCurrentSession().createQuery(basicHql).list();
		return basicData;
	}
	
	@SuppressWarnings("unchecked")
	public List<FamilyDetail> getFamilyDetailDetails(int ind_id)
	{
		String familyHql = "from FamilyDetail where ind_kyc_id="+ind_id+" and cr_date=(select MAX(cr_date) from FamilyDetail where ind_kyc_id="+ind_id+")";
		List<FamilyDetail> familyData = sessionFactory.getCurrentSession().createQuery(familyHql).list();
		
		return familyData;
	}
	
	@SuppressWarnings("unchecked")
	public List<UtiliserProfileModel> getUtilizerDetails(String utiId)
	{
		String utilizerDetailsHql = "from UtiliserProfileModel as I where I.kyc_uti_id='"+utiId+"'";
		List<UtiliserProfileModel> utilizerDetails = sessionFactory.getCurrentSession().createQuery(utilizerDetailsHql).list();
		return utilizerDetails;
	}
	
	@SuppressWarnings("unchecked")
	public String deactiveLastAccess(AccessDetailsModel accmodel)
	{
		String selectHql = "select access_id from AccessDetailsModel  where access_giver = '"+accmodel.getAccess_giver()+"' and access_taker = '"+accmodel.getAccess_taker()+"' and access_given_date=(select MAX(access_given_date) from AccessDetailsModel where access_giver = '"+accmodel.getAccess_giver()+"' and access_taker = '"+accmodel.getAccess_taker()+"')";
		List<Integer> utilizerDetails = sessionFactory.getCurrentSession().createQuery(selectHql).list();
		
		if(utilizerDetails.size()>0)
		{
			org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update AccessDetailsModel set status = :setstatus , uti_status = :setutiStatus , repeate_access_status = :setutirepeatStatus where access_id = :setaccessId");
			hql.setParameter("setstatus", "0");
			hql.setParameter("setutiStatus","1");
			hql.setParameter("setutirepeatStatus","Yes");
			hql.setParameter("setaccessId",utilizerDetails.get(0));
			
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "success";
			}
			else
			{			
				return "failed";
			}
		}
		else
		{
			return "success";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public String getPannelStatus(String documentType,int IndId)
	{
		String entireListOfDocName = "";
		
		if(documentType.equals("Financial_DOC"))
		{
			String hql = "select doc_name from DocumentDetailModel where doc_type='"+documentType+"' and kyc_ind_id="+IndId+" ORDER BY cr_date DESC";		
			List<String> ls =sessionFactory.getCurrentSession().createQuery(hql).list();
			if(ls.size()>0)
			{
				for(int i=0;i<ls.size();i++)
				{
					entireListOfDocName += ls.get(i)+",";
				}				
			}
			
			String finalFinancialDocuments = "";
			if(entireListOfDocName.length()>0)
			{
				entireListOfDocName = entireListOfDocName.substring(0, entireListOfDocName.length()-1);	
				String listOfDocArra[] = entireListOfDocName.split(",");
				for(int t=0;t<listOfDocArra.length;t++)
				{
					String checkCondition[] = listOfDocArra[t].split("-FINANCIAL-DOCUMENT");
					if(!finalFinancialDocuments.contains(checkCondition[0]))
					{
						finalFinancialDocuments += listOfDocArra[t] + ",";
					}
				}
				if(finalFinancialDocuments.length()>0)
				{
					finalFinancialDocuments = finalFinancialDocuments.substring(0, finalFinancialDocuments.length()-1);
				}
			}
			
			return finalFinancialDocuments;	
		}
		else
		{
			String hql = "select panel_status from DocumentDetailModel where doc_type='"+documentType+"' and kyc_ind_id="+IndId+" and cr_date=(select MAX(cr_date) from DocumentDetailModel where doc_type='"+documentType+"' and kyc_ind_id="+IndId+")";		
			List<String> ls =sessionFactory.getCurrentSession().createQuery(hql).list();
			if(ls.size()>0)
			{
				String panelStatus = ls.get(0);
				if(documentType.equals("Employment_DOC"))
				{
					String panelArray[] = panelStatus.split("/");
					for(int i=0;i<panelArray.length;i++)
					{
						 String docNameHql = "select doc_name from DocumentDetailModel where docs_data LIKE '"+panelArray[i]+"%' and doc_type='"+documentType+"' and kyc_ind_id="+IndId+" and cr_date=(select MAX(cr_date) from DocumentDetailModel where docs_data LIKE '"+panelArray[i]+"%' and doc_type='"+documentType+"' and kyc_ind_id="+IndId+")";		
					     List<String> docNames =sessionFactory.getCurrentSession().createQuery(docNameHql).list();
					     if(docNames.size()>0)
					     {
					    	 entireListOfDocName +=docNames.get(0)+",";
					     }
					}
				}
				else
				{
					String panelArray[] = panelStatus.split(",");				
					for(int i=0;i<panelArray.length;i++)
					{
						 String docNameHql = "select doc_name from DocumentDetailModel where docs_data LIKE '"+panelArray[i]+"%' and doc_type='"+documentType+"' and kyc_ind_id="+IndId+" and cr_date=(select MAX(cr_date) from DocumentDetailModel where docs_data LIKE '"+panelArray[i]+"%' and doc_type='"+documentType+"' and kyc_ind_id="+IndId+")";		
					     List<String> docNames =sessionFactory.getCurrentSession().createQuery(docNameHql).list();
					     if(docNames.size()>0)
					     {
					    	 entireListOfDocName +=docNames.get(0)+",";
					     }
					}
				}
				
				if(entireListOfDocName.length()>0)
				{
					entireListOfDocName = entireListOfDocName.substring(0, entireListOfDocName.length()-1);					
				}
				
				return entireListOfDocName;	
			}
			else
			{
				return "";
			}
		}
        
	}
	
	public List<UtiliserProfileModel> getUtilizerFullName(String Kyc_id)
	{
		Kyc_id = Kyc_id.replaceAll(" ", "");
		String utilizerDetailsHql = "from UtiliserProfileModel where kyc_uti_id='"+Kyc_id+"'";
		@SuppressWarnings("unchecked")
		List<UtiliserProfileModel> utilizerDetails = sessionFactory.getCurrentSession().createQuery(utilizerDetailsHql).list();
		return utilizerDetails;
	}
	
	public String getIndNameFromKycId(String Kyc_id)
	{
		Kyc_id = Kyc_id.replaceAll(" ", "");
		String hql = "from IndSignupModel where kycid = '"+Kyc_id+"'";
		@SuppressWarnings("unchecked")
		List<IndSignupModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		String fullName = "";
		fullName = lsdata.get(0).getFirstname()+" "+lsdata.get(0).getMiddlename()+" "+lsdata.get(0).getLastname();		
		return fullName;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getDocumentAvailablityViaThePattern(String documentArray,String docType,int ind_id)
	{
		 if(docType.equals("EMPLOYEMENT"))
		 {
			 String docNameHql = "";
			 if(documentArray.equals("PaySlip"))
			 {
				 docNameHql = "select doc_name from DocumentDetailModel where doc_name LIKE '%"+documentArray+"%' and kyc_ind_id="+ind_id+" ";		
			 }
			 else
			 {
				 docNameHql = "select doc_name from DocumentDetailModel where (doc_name LIKE '%Offer%' or doc_name LIKE '%Hike%' or doc_name LIKE '%Experience%' or doc_name LIKE '%Reliving%' or doc_name LIKE '%Form%') and kyc_ind_id="+ind_id+" ";						 
			 }
		     List<String> docNames =sessionFactory.getCurrentSession().createQuery(docNameHql).list();
		     if(docNames.size()>0)
		     {
		    	 return documentArray+"-Yes";
		     }
		     else
		     {
		    	 return documentArray+"-No";
		     }
		 }
		 else
		 {
			 String docNameHql = "select doc_name from DocumentDetailModel where doc_name LIKE '%"+documentArray+"%' and kyc_ind_id="+ind_id+" ";		
		     List<String> docNames = sessionFactory.getCurrentSession().createQuery(docNameHql).list();
		     if(docNames.size()>0)
		     {
		    	 return documentArray+"-Yes";
		     }
		     else
		     {
		    	 return documentArray+"-No";
		     }
		 }
		
	}
	
	
	@SuppressWarnings("unchecked")
	public String checkEmailIdExistOrNot(String SharedEmailId)
	{
		String hql = "select email_id from RegHistoryModel where email_id='"+SharedEmailId+"'";
		List<String> existData = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(existData.size()>0)
		{
			return "Exist";
		}
		else
		{
			return "Not Exist";
		}
	}
}
