package com.kyc.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kyc.model.AccessDetailsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.DocumentModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.ProfilePictureModel;
import com.kyc.model.UtiliserDetailsModel;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtiliserUpdateModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;
import com.kyc.util.PasswordEncryptionDecryption;

@Repository("utidao")
public class UtiliserDaoImpl implements UtiliserDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public String getUtilizerFullName(int uti_Indid)
	{
		String nameHql = "from UtiliserProfileModel where uti_ind_id="+uti_Indid+"";
		List<UtiliserProfileModel> nameData = sessionFactory.getCurrentSession().createQuery(nameHql).list();
		String utiFullName = "";
		if(nameData.size()>0)
		{
			utiFullName = nameData.get(0).getUti_first_name()+" "+nameData.get(0).getUti_middle_name()+" "+nameData.get(0).getUti_last_name();
		}
		return utiFullName;
	}
	
	@SuppressWarnings("unchecked")
	public String utiliserAuthenticate(String kycid,String password)
	{		
		if(kycid!=null && password!= null)
		 {			
			String passwordHqlViaKycId = "select password from UtiliserProfileModel as I where I.kyc_uti_id='"+kycid+"'";
			List<String> passwordDataViaKycId = sessionFactory.getCurrentSession().createQuery(passwordHqlViaKycId).list();
			
			if(passwordDataViaKycId.size()>0)
			{
				String validateStatus = PasswordEncryptionDecryption.getValidatePassword(password, passwordDataViaKycId.get(0));
				if(validateStatus.equals("True"))
				{
					return "success"; 
				}
				else
				{
					return "failure"; 
				}
			}
			else
			{
				String passwordHqlViaemailId = "select password from UtiliserProfileModel as I where  I.uti_email_id='"+kycid+"'";
				List<String> passwordDataViaemailId = sessionFactory.getCurrentSession().createQuery(passwordHqlViaemailId).list();
				if(passwordDataViaemailId.size()>0)
				{
					String validateStatus = PasswordEncryptionDecryption.getValidatePassword(password, passwordDataViaemailId.get(0));
					if(validateStatus.equals("True"))
					{
						return "success"; 
					}
					else
					{
						return "failure"; 
					}
				}
				else
				{
					return "failure"; 
				}
			}								 
		 }
		else
		{
			return "failure"; 
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AccessDetailsModel> findAwaitedDocDetails(String uti_kycid)
	{
		List<AccessDetailsModel> lsdata1 = new ArrayList<AccessDetailsModel>();
								
		String hql = "from AccessDetailsModel where access_taker='"+uti_kycid+"' and uti_status='0' and repeate_access_status='No' Order By access_given_date desc";
		lsdata1 = sessionFactory.getCurrentSession().createQuery(hql).list();						
			
		return lsdata1;		
	}
	
	@SuppressWarnings("unchecked")
	public List<AccessDetailsModel> findViewedDocDetails(String uti_kycid)
	{
		String hql = "from AccessDetailsModel where access_taker='"+uti_kycid+"' and status='1' and uti_status='1' and repeate_access_status='No' Order By access_taken_date desc";
		List<AccessDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
		
	
	@SuppressWarnings("unchecked")
	public List<AccessDetailsModel> viewedSearchedDocDetailsForUtilizer(String uti_kycid,String searchedIndex)
	{
		String hql = "from AccessDetailsModel where giver_name LIKE '%"+searchedIndex+"%' and access_taker='"+uti_kycid+"' and uti_status='1' and repeate_access_status='No' Order By access_taken_date desc";
		List<AccessDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	
	public String acceptDocs(String ind_Id,Date date)
	{
		String kycid = ind_Id.replaceAll(" ","");
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update AccessDetailsModel set uti_status = :setutistatus , access_taken_date = :settakenDate where access_giver = :setgiver and access_given_date = :setdate");
		hql.setParameter("setutistatus", "1");
		hql.setParameter("setgiver", kycid);
		hql.setParameter("setdate", date);
		hql.setParameter("settakenDate", new Date());
		int result  = hql.executeUpdate();
		
		if(result==1)
		{
		    return "Document Accepted";
		}
		else
		{
			return "Unable to do Try Again";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findAccessPattern(String kycid,String date)
	{
		
		String hql  = "select access_pattern from AccessDetailsModel where access_giver = '"+kycid+"' and access_given_date ='"+date+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public String findYear(String kycid)
	{
		String hql  = "select EXTRACT(YEAR from cr_date) from IndSignupModel where kyc_id = '"+kycid+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.toString().substring(1, lsdata.toString().length()-1);
	}
	
	
	@SuppressWarnings("unchecked")
	public String findDirName(String docname)
	{	
		String docType = "";
		String hql  = "select doc_type from DocumentDetailModel where doc_name like '%"+docname+"%'";		
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			docType = lsdata.get(0);
		}
		else
		{
			String emphql  = "select doc_type from DocumentDetailModel where docs_data like '%"+docname+"%'";			
			List<String> empData = sessionFactory.getCurrentSession().createQuery(emphql).list();
			if(empData.size()>0)
			{
				docType = empData.get(0);
			}
		}
		return docType;
	}
	
	
	@SuppressWarnings("unchecked")
	public int findid(String kycid)
	{
		String hql="select ind_id from IndSignupModel where kycid ='"+kycid+"'";
		List<Integer> lsdata =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<DocumentDetailModel> getHistory(String docname,int Ind_id)
	{
		String hql = "from DocumentDetailModel where kyc_ind_id="+Ind_id+" and doc_name like '%"+docname+"%' ORDER BY cr_date DESC";		
		List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		if(lsdata.size()>0)
		{
			return lsdata;
		}
		else
		{
			String historyhql = "from DocumentDetailModel where kyc_ind_id="+Ind_id+" and docs_data like '%"+docname+"%' ORDER BY cr_date DESC";		
			List<DocumentDetailModel> historydata = sessionFactory.getCurrentSession().createQuery(historyhql).list();
			return historydata;
		}
		
	}
		
	@SuppressWarnings("unchecked")
	public List<UtiliserProfileModel> getUtiBasicDetails(String utikycid)
	{
		String hql = "from UtiliserProfileModel where kyc_uti_id='"+utikycid+"'";		
		List<UtiliserProfileModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<UtiliserDetailsModel> getUtiProfDetails(String utikycid)
	{
		String hql = "from UtiliserDetailsModel where kyc_uti_id='"+utikycid+"'";		
		List<UtiliserDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public int findUtiid(String utiKycid)
	{
		String hql = "Select uti_ind_id from UtiliserProfileModel where kyc_uti_id='"+utiKycid+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		int id = lsdata.get(0);
		return id;
	}
		
	@SuppressWarnings("unchecked")
	public String updateProfile(UtiliserProfileModel profile)
	{		
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		sessionFactory
		.getCurrentSession()
		.createQuery("UPDATE UtiliserProfileModel SET kyc_uti_id = '"
						+ profile.getKyc_uti_id()
						+ "' , uti_first_name='"
						+ profile.getUti_first_name()
						+ "' , uti_middle_name='"
						+ profile.getUti_middle_name()
						+ "' , uti_last_name='"
						+ profile.getUti_last_name()
						+ "' ,uti_email_id='"
						+ profile.getUti_email_id() + "' where kyc_uti_id='"+profile.getKyc_uti_id()+"'")
						.executeUpdate();
		
						tx.commit();
						if(tx.wasCommitted()){
							txStatus="Updated";
						} else {
							txStatus="failure";
						}
						} catch (Exception he) {
							System.out.println(he.getMessage());
						} finally {
							docSession.close();
						}
						return txStatus;
	}
	
	public String updateProfDetails(UtiliserDetailsModel utiDetailsModel)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try 
		{
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(utiDetailsModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} 
		else {
			txStatus="failure";
		}
		}
		catch (Exception he) {
			System.out.println(he.getMessage());
		}
		finally {
			
			docSession.close();
			
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<UtiliserUpdateModel> getUtiNotifications(String uti_kycid)
	{
		String hql = "from UtiliserUpdateModel where uti_kyc_id='"+uti_kycid+"' ORDER BY uti_update_datetime DESC ";
		List<UtiliserUpdateModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
	}
	
	public String setNotificationdetails(UtiliserUpdateModel uti_update)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(uti_update);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	public String changePassword(String OldPassword, String Newpass, String kycid)
	{
		 String kyc_id = kycid.replaceAll(" ", "");
		 org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UtiliserProfileModel set password = :setnewPass  where kyc_uti_id = :setKYCid");
	        hql.setParameter("setnewPass", Newpass);	        
	        hql.setParameter("setKYCid", kyc_id);
	        int result = hql.executeUpdate();
	        if(result == 1)
	            return "Password Changed";
	        else
	            return "Unable to do Try Again";
	}
	
	@SuppressWarnings("unchecked")
	public String checkUtiPassword(String OldPassword,String newPassword,String kycid)
	{
		String hql ="select password from UtiliserProfileModel where kyc_uti_id='"+kycid+"'";
        List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
        return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String findDocs(String docLike,int ind_id)
	{
		
		String hql = "select doc_name from DocumentDetailModel where doc_name like '%"+docLike+"%' and kyc_ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from DocumentDetailModel where doc_name like '%"+docLike+"%' and kyc_ind_id="+ind_id+")";
		
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public int findUti_kycid(String utiKyc_id)
	{
		String hql = "select uti_ind_id from UtiliserDetailsModel where kyc_uti_id='"+utiKyc_id+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
		    return lsdata.get(0);
		}
		else
		{
			return 0;
		}
	}
	
	public String insertProfDetails(UtiliserDetailsModel Prof)
	{
		
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(Prof);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<DocumentModel> getDocActivity(String kycid)
	{
		String hql = "select access_giver from AccessDetailsModel where access_taker='"+kycid+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		List<DocumentModel> docdata = new ArrayList<DocumentModel>();
		if(lsdata.size()>0)
		{
		for(int i=0;i<lsdata.size();i++)
		{
			String hql1 = "from DocumentModel where kycid='"+lsdata.get(i)+"' and date=(select MAX(date) from DocumentModel where kycid='"+lsdata.get(i)+"')";
		    docdata = sessionFactory.getCurrentSession().createQuery(hql1).list();
		}
		}
		return docdata;
	}
	
	@SuppressWarnings("unchecked")
	public String getUtilizerKycId(String kycid)
	{
		String hql = "select kyc_uti_id from UtiliserProfileModel where uti_email_id='"+kycid+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public int getUtiIndId(String kycId)
	{
		String hql = "select uti_ind_id from UtiliserProfileModel where uti_email_id='"+kycId+"' or kyc_uti_id='"+kycId+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	public UtiliserProfileModel getLatestProfilePic(int uti_Indid)
	{		
		return (UtiliserProfileModel)sessionFactory.getCurrentSession().get(UtiliserProfileModel.class, uti_Indid);			  
	}
	
	
	public UtiliserDetailsModel getUtilizerDetail(int uti_Indid)
	{
		return (UtiliserDetailsModel)sessionFactory.getCurrentSession().get(UtiliserDetailsModel.class, uti_Indid);	
	}
	
	@SuppressWarnings("unchecked")
	public String getUtiAboutUsInfo(int uti_Indid)
	{
		String hql = "select about_utiliser from UtiliserDetailsModel where uti_ind_id="+uti_Indid+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Data";
		}
	}
	
	@SuppressWarnings("unchecked")
	public int getUtiDetailsId(int uti_Indid)
	{
		String hql = "select details_id from UtiliserDetailsModel where uti_ind_id="+uti_Indid+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String getRegistartionDetail(String column,String tableName,int ind_id)
	{
		if(column.equals("mobile_no"))
		{
			String hql = "select "+column+" from "+tableName+" where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from "+tableName+" where ind_id="+ind_id+")";
			List<Long> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			String mobileno =  Long.toString(lsdata.get(0));
			return mobileno;
		}
		else
		{
			String hql = "select "+column+" from "+tableName+" where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from "+tableName+" where ind_id="+ind_id+")";
			List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			return lsdata.get(0);
		}
		
	}
	
    @SuppressWarnings("unchecked")
	public String getBasicDetail(String column,String tableName,int ind_id)
    {   	
		String hql = "select "+column+" from "+tableName+" where ind_kyc_id='"+ind_id+"' and cr_date=(select MAX(cr_date) from "+tableName+" where ind_kyc_id='"+ind_id+"')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Not Filled Yet!";
		}
    }
	
	@SuppressWarnings("unchecked")
	public String getFamilyDetail(String column,String tableName,int ind_id)
	{		
		String hql = "select "+column+" from "+tableName+" where ind_kyc_id='"+ind_id+"' and cr_date=(select MAX(cr_date) from "+tableName+" where ind_kyc_id='"+ind_id+"')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Not Filled Yet!";
		}		
	}
	
	public String saveActivityDetails(UtilizerActivityModel actModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(actModel);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<UtilizerActivityModel> getUtilizerActivity(int uti_indid)
	{
		String hql = "from UtilizerActivityModel where uti_ind_id="+uti_indid+" Order By activity_date desc ";
		List<UtilizerActivityModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<KycNotificationModel> getNotificationDetails(int uti_indid)
	{
		String hql = "from KycNotificationModel where (notification_type='Utilizer' and notification_to='"+uti_indid+"' or notification_to='All Utilizer') or (notification_type='System' and notification_to='System') Order By notification_date desc ";
		List<KycNotificationModel> adminNotification = sessionFactory.getCurrentSession().createQuery(hql).list();
		return adminNotification;
	}
	
	@SuppressWarnings("unchecked")
	public String getMessagefromNotifyId(int notifyId)
	{
		String hql = "select notification_message from KycNotificationModel where notification_id="+notifyId+"";
		List<String> adminNotification = sessionFactory.getCurrentSession().createQuery(hql).list();
		return adminNotification.get(0);
	}
	
    @SuppressWarnings("unchecked")
	public int viewedDocumentinthisMonth(String uti_kycid)
    {
    	String hql = "select COUNT(*) from AccessDetailsModel where access_taker='"+uti_kycid+"' and uti_status='1' and MONTH(access_taken_date)=MONTH(sysdate())";
		List<Long> countCode  = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(countCode.size()>0)
		{
			 long count = countCode.get(0);
			 int countValue = (int)count;
			 return countValue;		
		}
		else
		{
			return 0;
		}	
    }
	
	@SuppressWarnings("unchecked")
	public int getviewedDocumentAsofNow(String uti_kycid)
	{
		String hql = "select COUNT(*) from AccessDetailsModel where access_taker='"+uti_kycid+"' and uti_status='0'";
		List<Long> countCode  = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(countCode.size()>0)
		{
			 long count = countCode.get(0);
			 int countValue = (int)count;
			 return countValue;		
		}
		else
		{
			return 0;
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AccessDetailsModel> getAccessNotificationDetails(String uti_kycid)
	{
		String hql = "from AccessDetailsModel where access_taker='"+uti_kycid+"' and status='1' and repeate_access_status='No' ORDER BY access_given_date DESC";
		List<AccessDetailsModel> accessDetails  = sessionFactory.getCurrentSession().createQuery(hql).list();
		return accessDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getDocumentComponentValue(String docname,int ind_id)
	{
		String hql = "select docs_data from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_name LIKE '%"+docname+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_name LIKE '%"+docname+"%')";
		List<String> documentComponentValue = sessionFactory.getCurrentSession().createQuery(hql).list();
		return documentComponentValue.get(0);
	}
	
	
	@SuppressWarnings("unchecked")
	public String checkEmailIdAvailabilty(String requestedEmailId)
	{
		String status = "";
		String checkHql = "from UtiliserProfileModel where uti_email_id='"+requestedEmailId+"'";
		List<String> utiData1 = sessionFactory.getCurrentSession().createQuery(checkHql).list();
		
		if(utiData1.size()>0)
		{
			status = "Yes";
		}
		else
		{
			String checkHql2 = "from UtiliserProfileModel where uti_office_emailId='"+requestedEmailId+"'";
			List<String> utiData2 = sessionFactory.getCurrentSession().createQuery(checkHql2).list();
			if(utiData2.size()>0)
			{
				status = "Yes";
			}
			else
			{
				status = "Please enter a registered Email Id !";
			}
		}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public String getRefIdViaEmailId(String emailId)
	{
		String status = "";
		String checkHql = "select uti_reference_id from UtiliserProfileModel where uti_email_id='"+emailId+"'";
		List<String> utiData1 = sessionFactory.getCurrentSession().createQuery(checkHql).list();
		
		if(utiData1.size()>0)
		{
			status = utiData1.get(0);
		}
		else
		{
			String checkHql2 = "select uti_reference_id from UtiliserProfileModel where uti_office_emailId='"+emailId+"'";
			List<String> utiData2 = sessionFactory.getCurrentSession().createQuery(checkHql2).list();
			if(utiData2.size()>0)
			{
				status = utiData2.get(0);
			}			
		}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public String checkPasswordStatus(String reserVariable)
	{
			String sql = "select uti_status from uti_reset_password where uti_reference_id='"+reserVariable+"' and requested_date >=now()-INTERVAL 1 DAY ";
			List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sql).list();
			
			if(lsdata.size()>0)
			{			
				return "Allow";						
			}
			else
			{
				return "Not Allow";						
			}
	}		
	
	@SuppressWarnings("unchecked")
	public int getindividualIndId(String uniqueVariable)
	{
		String checkHql2 = "select uti_ind_id from UtiliserProfileModel where uti_reference_id='"+uniqueVariable+"'";
		List<Integer> utiData2 = sessionFactory.getCurrentSession().createQuery(checkHql2).list();
		return utiData2.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String getDeleteUtiResetPaswordInfo(String uniqueVariable)
	{
		String hql = "select password_id FROM UtilizerPasswordRequestModel where uti_reference_id='"+uniqueVariable+"'";	             
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
		Transaction tx1=null;
		String status = "";
		for(int i=0;i<lsdata.size();i++)
		{
			String passwordHql = "delete from UtilizerPasswordRequestModel where password_id="+lsdata.get(i)+"";
			Query query = sessionFactory.getCurrentSession().createQuery(passwordHql);		
			
			int result = query.executeUpdate();		
			tx1=sessionFactory.getCurrentSession().beginTransaction();		
			tx1.commit();
			
			if(result>0)
			{
				status="success";
			}
			else
			{
				status="failure";
			}
		}
		
		return status;	
	}
	
	public String getChangeUtiPassword(UtiliserProfileModel utiModel)
	{
		String hql1 = " UPDATE UtiliserProfileModel SET password='"+utiModel.getPassword()+"' where uti_ind_id="+utiModel.getUti_ind_id()+"";
		int status = sessionFactory.getCurrentSession().createQuery(hql1).executeUpdate();
		
		if(status>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}	
	}

	
	public String migrateUtilizerPlanDetails(UtilizerPlanDetailsModel utiPlanModel)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UtilizerPlanDetailsModel set uti_activate_plan = :setplanName , cr_date = :setActiveDate  where uti_ind_id = :setUtiIndId");
		hql.setParameter("setplanName", utiPlanModel.getUti_activate_plan());
		hql.setParameter("setActiveDate", utiPlanModel.getCr_date());
		hql.setParameter("setUtiIndId", utiPlanModel.getUti_ind_id());
		
	    int status  = hql.executeUpdate();
		
		if(status>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}	
	}
	
	public String saveProfilePhotoFromUtilizer(UtilizerProfilePhotoModel profilePhotoModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(profilePhotoModel);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	public String updateProfPhotoDetails(String documentName,int utiIndId)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UtiliserProfileModel set uti_profile_pic = :setPhotoName where uti_ind_id = :setUtiIndId");
		hql.setParameter("setPhotoName", documentName);
		hql.setParameter("setUtiIndId", utiIndId);
		
	    int status  = hql.executeUpdate();
		
		if(status>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getProfilePhotoHistory(int uti_ind_id)
	{
		String profilePhotoList = "";
		String hql = "from UtilizerProfilePhotoModel where uti_ind_id="+uti_ind_id+"";
		List<UtilizerProfilePhotoModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			for(int i=0;i<lsdata.size();i++)
			{
				profilePhotoList += lsdata.get(i).getUti_profile_photo() + "&&" + lsdata.get(i).getCr_date() + ",";
				
			}
			
			profilePhotoList = profilePhotoList.substring(0, profilePhotoList.length()-1);
			
			return profilePhotoList;
		}
		else
		{
			return "NoData";
		}
	}
	
	public String updateProfilePic(String picName,int uti_ind_id)
	{		
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UtilizerProfilePhotoModel set cr_date = :setdate where uti_ind_id = :setIndId and uti_profile_photo = :setpicName");
		hql.setParameter("setdate", new Date());
		hql.setParameter("setIndId", uti_ind_id);
		hql.setParameter("setpicName", picName);
			
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{	
			org.hibernate.Query profilePhotHql = sessionFactory.getCurrentSession().createQuery("update UtiliserProfileModel set uti_profile_pic = :setPhotoName where uti_ind_id = :setUtiIndId");
			profilePhotHql.setParameter("setPhotoName", picName);
			profilePhotHql.setParameter("setUtiIndId", uti_ind_id);
			
		    int status  = profilePhotHql.executeUpdate();
			
			if(status>0)
			{
				return "success";
			}
			else
			{
				return "failure";
			}			    		   
		}
		else
		{			
			return "profileUpdatedFailed";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String> getListOfSenderKycId(String utiKycId)
	{
		String hql = "select access_giver from AccessDetailsModel where access_taker='"+utiKycId+"' and uti_status='1' and status='1' and repeate_access_status='No'";
		List<String> accessDetails  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return accessDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public int getUtilizerIdVaiUniqueId(String resetVariable)
	{
		String hql = "select uti_ind_id from UtiliserProfileModel where uti_reference_id='"+resetVariable+"'";		
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	public String updateVisibilityUrl(int uti_Ind_id,String encryptedUrl)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UtiliserProfileModel set uti_reference_id = :encryptedUrl  where uti_ind_id = :uti_ind_id");
		hql.setParameter("encryptedUrl", encryptedUrl);
		hql.setParameter("uti_ind_id", uti_Ind_id);
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "update";
		}
		else
		{			
			return "not update";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String getAccesPatternViaKycId(String uti_kycid,String indKycId)
	{
		String hql = "select access_pattern from AccessDetailsModel where access_taker='"+uti_kycid+"' and access_giver='"+indKycId+"' and uti_status='1' and status='1' and repeate_access_status='No'";
		List<String> accessDetails  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(accessDetails.size()>0)
		{
			return accessDetails.get(0);
		}
		else
		{
			return "No Data";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public String getdefaultKycDocuments(String docType)
	{		
	   String hql = "select subdoc_type from DocumentHierarchyModel where doc_hir_name like '"+docType+"%'";
	   List<String> accessDetails  = sessionFactory.getCurrentSession().createQuery(hql).list();
	   String hierarchyData = "";
	   
	   if(accessDetails.size()>0)
	   {
		   for(int i=0;i<accessDetails.size();i++)
		   {
			   hierarchyData  +=  accessDetails.get(i) + ",";
		   }
		   
		   if(hierarchyData.length()>0)
		   {
			   hierarchyData = hierarchyData.substring(0,hierarchyData.length()-1);
		   }
	   }
	   
	   return hierarchyData;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getDocumentNamesWithDetails(String indKycId,String academicAccordian,String extraAddedDocuments)
	{
		String accordianarray[] = academicAccordian.split(",");
		String nameWithData = "";
		for(int i=0;i<accordianarray.length;i++)
		{
			String detailhql = "select docs_data from DocumentDetailModel where kyc_ind_id="+indKycId+" and docs_data LIKE '"+accordianarray[i]+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+indKycId+" and docs_data LIKE '"+accordianarray[i]+"%')";
			List<String> accessDetails  = sessionFactory.getCurrentSession().createQuery(detailhql).list();
			if(accessDetails.size()>0)
			{
				nameWithData +=accessDetails.get(0);
				
				String docNamehql = "select doc_name from DocumentDetailModel where kyc_ind_id="+indKycId+" and doc_name LIKE '"+accordianarray[i]+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+indKycId+" and doc_name LIKE '"+accordianarray[i]+"%')";
				List<String> docNameDetails  = sessionFactory.getCurrentSession().createQuery(docNamehql).list();
				if(docNameDetails.size()>0)
				{
					if(extraAddedDocuments.length()>2)
					{						   
						if(docNameDetails.get(0).contains(extraAddedDocuments.split(",")[0]))
						{							
							nameWithData += "ACASEP"+extraAddedDocuments+"ENDSEP";
						}
						else
						{							
							nameWithData += "ACASEP"+docNameDetails.get(0)+"ENDSEP";
						}	
					}
					else
					{
						nameWithData += "ACASEP"+docNameDetails.get(0)+"ENDSEP";
					}
									
				}
				else
				{
					nameWithData += "ACASEP"+"No Doc"+"ENDSEP";
				}
			}
			else
			{
				nameWithData +="No Details"+"ENDSEP";
			}
			
		}
		
		if(nameWithData.length()>0)
		{
			nameWithData = nameWithData.substring(0,nameWithData.length()-6);
		}
		
		return nameWithData;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmployeeDetailsWithDocName(int ind_id,String getEmployeeStatus)
	{
		String employeeDetails = "";
		String accordianarray[] = getEmployeeStatus.split("/");
		
		if(!getEmployeeStatus.equals("Not There"))
		{
			for(int i=0;i<accordianarray.length;i++)
			{
				String detailhql = "select docs_data from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data LIKE '"+accordianarray[i]+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data LIKE '"+accordianarray[i]+"%')";
				List<String> accessDetails  = sessionFactory.getCurrentSession().createQuery(detailhql).list();
				if(!accessDetails.get(0).contains("No Data"))
				{
					employeeDetails +=accessDetails.get(0);
					
					String docNamehql = "select doc_name from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data LIKE '"+accordianarray[i]+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data LIKE '"+accordianarray[i]+"%')";
					List<String> docNameDetails  = sessionFactory.getCurrentSession().createQuery(docNamehql).list();
					if(docNameDetails.get(0).contains("_"))
					{
						employeeDetails += "EMPSEP"+docNameDetails.get(0)+"ENDSEP";
					}
					else
					{
						employeeDetails += "EMPSEP"+"No Doc"+"ENDSEP";
					}
				}
				else
				{
					employeeDetails +="No Details"+"ENDSEP";
				}
				
			}
		}				
		if(employeeDetails.length()>0)
		{
			employeeDetails = employeeDetails.substring(0, employeeDetails.length()-6);
		}
		
		return employeeDetails;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String getFinancialDetailsBythePattern(int ind_id,String getFinancialStatusAsPerPattern)
	{
		String financialDetails = "";
		String financialDoctype[] = getFinancialStatusAsPerPattern.split("FINSEP");
		String accordianarray[] = financialDoctype[0].split(",");

		
		
		for(int i=0;i<accordianarray.length;i++)
		{
			String detailhql = "select docs_data from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data LIKE '%"+accordianarray[i]+"%' ORDER BY cr_date DESC";
			List<String> accessDetails  = sessionFactory.getCurrentSession().createQuery(detailhql).list();
			if(accessDetails.size()>0)
			{
				for(int j=0;j<accessDetails.size();j++)
				{
					if(!accessDetails.get(j).contains("No Data"))
					{
						financialDetails +=accessDetails.get(j)+"ENDSEP";	
					}				
				}							
			}
			else
			{
				financialDetails +="No Details"+"ENDSEP";
			}			
		}
		
		String extraDocuments = "";
		if(financialDoctype.length==2)
		{
			extraDocuments +=financialDoctype[1];
			
			String extraAddedDocuments[] = extraDocuments.split(",");
			
			for(int j=0;j<extraAddedDocuments.length;j++)
			{
				String detailhql = "select docs_data from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data LIKE '%"+extraAddedDocuments[j]+"%' ORDER BY cr_date DESC";
				List<String> accessDetails  = sessionFactory.getCurrentSession().createQuery(detailhql).list();
				if(accessDetails.size()>0)
				{
					for(int k=0;k<accessDetails.size();k++)
					{
						if(!accessDetails.get(k).contains("No Data"))
						{
							financialDetails +=accessDetails.get(k)+"ENDSEP";	
						}				
					}							
				}
				else
				{
					financialDetails +="No Details"+"ENDSEP";
				}	
			}
			
		}
		
		
		String finalFinancialDocuments = "";
		
		if(financialDetails.length()>0)
		{
			financialDetails = financialDetails.substring(0, financialDetails.length()-6);
		    			
			String financialDetailsArray[] = financialDetails.split("ENDSEP");
			for(int f=0;f<financialDetailsArray.length;f++)
			{
				String eachFinancialArray[] = financialDetailsArray[f].split("&&");
				if(eachFinancialArray.length>2)
				{
					if(!finalFinancialDocuments.contains(eachFinancialArray[0]+"&&"+eachFinancialArray[1]+"&&"+eachFinancialArray[2]))
					{
						finalFinancialDocuments += financialDetailsArray[f] + "ENDSEP";
					}
				}				
			}
			
			if(finalFinancialDocuments.length()>0)
			{
				finalFinancialDocuments = finalFinancialDocuments.substring(0,finalFinancialDocuments.length()-6);
			}
		}
		
		return finalFinancialDocuments;
	}
	
    @SuppressWarnings("unchecked")
	public int getNoOfAwaitedRequets(String uti_kycid)
    {
    	String hql = "select COUNT(access_id) from AccessDetailsModel where access_taker='"+uti_kycid+"' and uti_status='0' and status='1' and repeate_access_status='No'";
		List<Long> awaitedCount  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(awaitedCount.size()>0)
		{
			 long count = awaitedCount.get(0);
			 int countValue = (int)count;
			 return countValue;
		}
		else
		{
			return 0;
		}		
    }
    
	@SuppressWarnings("unchecked")
	public int getNoOfRecivedDocuments(String uti_kycid)
	{
		String hql = "select COUNT(access_id) from AccessDetailsModel where access_taker='"+uti_kycid+"' and uti_status='1' and status='1' and repeate_access_status='No'";
		List<Long> receivedCount  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(receivedCount.size()>0)
		{
			 long count = receivedCount.get(0);
			 int countValue = (int)count;
			 return countValue;
		}
		else
		{
			return 0;
		}		
	}

	
}
