package com.kyc.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kyc.bean.SubDomainLevelBean;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentHierarchyModel;
import com.kyc.model.DomainHierarchyModel;
import com.kyc.model.DomainLevelModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.PatternDetailsModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.SubDomainLevel10Model;
import com.kyc.model.SubDomainLevel1Model;
import com.kyc.model.SubDomainLevel2Model;
import com.kyc.model.SubDomainLevel3Model;
import com.kyc.model.SubDomainLevel4Model;
import com.kyc.model.SubDomainLevel5Model;
import com.kyc.model.SubDomainLevel6Model;
import com.kyc.model.SubDomainLevel7Model;
import com.kyc.model.SubDomainLevel8Model;
import com.kyc.model.SubDomainLevel9Model;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;

@Repository("adminpaneldao")
public class AdminPanelDaoImpl implements AdminPanelDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<IndSignupModel> getAllUsersDetails()
	{
		
		String queryToken ="from IndSignupModel";
		 @SuppressWarnings({ "unchecked" })
		List<IndSignupModel> usersList =(List<IndSignupModel>)sessionFactory.getCurrentSession().createQuery(queryToken).list();
		
		return usersList; 
	}
	
	
	@SuppressWarnings("unchecked")
	public String getUtiRefId(int utiUnqId)
	{
		String profile_picHql = "select uti_reference_id from UtiliserProfileModel where uti_ind_id="+utiUnqId+"";
        List<String> lsdata = sessionFactory.getCurrentSession().createQuery(profile_picHql).list();
  		return lsdata.get(0);
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<String> findtypeGroup(String type)
	{
		String hql = "select subdoc_type from DocumentHierarchyModel where doc_hir_name LIKE '"+type+"%'";
		List<String> lst = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lst.size()>0)
		{
			return lst;
		}
		else
		{
			return lst;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public String find_acctypeGroup(String type)
	{
		String strarr[] = type.split("_");
		String hql ="select doc_type from DocumentHierarchyModel where doc_hir_name Like '"+strarr[0]+"%'";
		List<String> lst = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		String str = lst.toString();	
		String str2 = str.substring(1,str.length()-1);
		
		return str2.replaceAll(" ","");
	}
	
	@SuppressWarnings("unchecked")
	public List<IndSignupModel> searchindividualname(IndSignupModel searchprofile)
	{
		List<IndSignupModel> usersList = new ArrayList<IndSignupModel>();
		String queryToken ="from IndSignupModel where individual_full_name like '%"+ searchprofile.getKycid() + "%' ";										
		usersList = sessionFactory.getCurrentSession().createQuery(queryToken).list();
		
		return usersList;
	}
	
	
	public String searchindividualkycidpic(IndSignupModel searchprofile)
	{
		
		String profilepic ="select profile_picture_name from ProfilePictureModel where kyc_ind_id="+searchprofile.getIndid()+" and cr_date=(select MAX(cr_date) from ProfilePictureModel where kyc_ind_id="+searchprofile.getIndid()+")";
								
		 @SuppressWarnings({ "unchecked" })
		 List<String> lsdata =sessionFactory.getCurrentSession().createQuery(profilepic).list(); 
		
		if (lsdata.size() > 0) {

			return ""+lsdata.get(0);
		} 
		else 
		{
			return "No Data";
		}
	}
	
	
	
	public List<IndSignupModel> searchindividualplan(IndSignupModel searchprofile)
	{
		
		String queryToken ="from IndSignupModel as I where I.plan_available like '%"
								+ searchprofile.getKycid() + "%'";
		@SuppressWarnings({ "unchecked" })
		List<IndSignupModel> usersList =(List<IndSignupModel>)sessionFactory.getCurrentSession().createQuery(queryToken).list();
		
		return usersList; 
	}
	
	public List<IndSignupModel> searchindividualkycid(IndSignupModel searchprofile)
	{
		
		String queryToken ="from IndSignupModel as I where I.kycid like '%"
								+ searchprofile.getKycid() + "%'";
		@SuppressWarnings({ "unchecked" })
		List<IndSignupModel> usersList =(List<IndSignupModel>)sessionFactory.getCurrentSession().createQuery(queryToken).list();
		
		return usersList; 
	}
	
	
	public String changestatus(IndSignupModel changestatus){
		
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("update IndSignupModel set admin_control='"+changestatus.getStatus()+"' where ind_id='"+changestatus.getIndid()+"'").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public String findSub_category(String type)
	{
		String hql = "select subdoc_type from DocumentHierarchyModel where doc_type='"+type+"'";
		List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
		String str = lsd.toString();
		
		String str1 = str.substring(1,str.length()-1);
		return str1;
	}
	
	public String savePattern(PatternDetailsModel pattern)
	{
		
		System.out.println(pattern.getCr_by()+"/"+pattern.getPattern_logic()+"/"+pattern.getPattern_name()+"/"+pattern.getCr_date());
		
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(pattern);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}

	@SuppressWarnings("unchecked")
	public List<String> getPatternNames()
	{
		String hql = "select pattern_name from PatternDetailsModel";
		List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsd;
	}
	
	@SuppressWarnings("unchecked")
	public String findPattern(String Pattern_Name)
	{
		String hql = "select pattern_logic from PatternDetailsModel where pattern_name='"+Pattern_Name+"'";
		List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		String str = lsd.toString();
		
		String p_logic = str.substring(1,str.length()-1);
		
		System.out.println(p_logic);
		return p_logic;
	}
	
	@SuppressWarnings("unchecked")
	public List<PatternDetailsModel> patternforDelete()
	{
		String hql="from PatternDetailsModel";
		List<PatternDetailsModel> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsd;
	}
	
	
	public String deletePattern(int p_id)
	{
	  String hql = "DELETE FROM PatternDetailsModel WHERE pattern_id = :pat_id";	
	  org.hibernate.Query hql1 = sessionFactory.getCurrentSession().createQuery(hql);
	  hql1.setParameter("pat_id", p_id);
	  int result  = hql1.executeUpdate();
	  
	  if(result==1)
	  {
		  return "Pattern Deleted";
	  }
	  else
	  {
		  return "Problem arise try after Some Time";
	  }
	}
	
	@SuppressWarnings("unchecked")
	public List<UtiliserProfileModel> searchUtiliser(String searchData,String searchType)
	{		
		List<UtiliserProfileModel> lsdata = new ArrayList<UtiliserProfileModel>();
		
		if("Name".equals(searchType))
		{
			String hql="from UtiliserProfileModel where uti_first_name like '%"+searchData+"%'";
			lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(lsdata.size()>0)
			{
				List<UtiliserProfileModel> middleNamedata = new ArrayList<UtiliserProfileModel>();
				
				String middleNamehql="from UtiliserProfileModel where uti_middle_name like '%"+searchData+"%'";
				middleNamedata = sessionFactory.getCurrentSession().createQuery(middleNamehql).list();
				
				if(middleNamedata.size()>0)
				{
					/* Removing Duplicacy Values */
					 		
					  for(int i=0;i<lsdata.size();i++)
					  {							
						  if(middleNamedata.contains(lsdata.get(i)))
						  {
							  middleNamedata.remove(lsdata.get(i));
						  }																			
					  }
					 
				    /* End of Removing Duplicacy Values */
					  
					lsdata.addAll(middleNamedata);
				}
				
                List<UtiliserProfileModel> lastNamedata = new ArrayList<UtiliserProfileModel>();
				
				String lastNamehql="from UtiliserProfileModel where uti_last_name like '%"+searchData+"%'";
				lastNamedata = sessionFactory.getCurrentSession().createQuery(lastNamehql).list();
				
				if(lastNamedata.size()>0)
				{
					/* Removing Duplicacy Values */
					 		
					  for(int i=0;i<lsdata.size();i++)
					  {							
						  if(lastNamedata.contains(lsdata.get(i)))
						  {
							  lastNamedata.remove(lsdata.get(i));
						  }																			
					  }
					 
					  /* End of Removing Duplicacy Values */
					  lsdata.addAll(lastNamedata);
				}				
			}
			else
			{
				String middleNamehql="from UtiliserProfileModel where uti_middle_name like '%"+searchData+"%'";
				lsdata = sessionFactory.getCurrentSession().createQuery(middleNamehql).list();
				
				if(lsdata.size()>0)
				{
					List<UtiliserProfileModel> lastNamedata = new ArrayList<UtiliserProfileModel>();
					
					String lastNamehql="from UtiliserProfileModel where uti_last_name like '%"+searchData+"%'";
					lastNamedata = sessionFactory.getCurrentSession().createQuery(lastNamehql).list();
					
					if(lastNamedata.size()>0)
					{
						/* Removing Duplicacy Values */
						 		
						  for(int i=0;i<lsdata.size();i++)
						  {							
							  if(lastNamedata.contains(lsdata.get(i)))
							  {
								  lastNamedata.remove(lsdata.get(i));
							  }																			
						  }
						 
						  /* End of Removing Duplicacy Values */
						  lsdata.addAll(lastNamedata);
					}		
				}
				else
				{
					String lastNamehql="from UtiliserProfileModel where uti_last_name like '%"+searchData+"%'";
					lsdata = sessionFactory.getCurrentSession().createQuery(lastNamehql).list();					
				}
			}
		}
		
		if("KYCID".equals(searchType))
		{
			String hql="from UtiliserProfileModel where kyc_uti_id LIKE '%"+searchData+"%'";
			lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		}
		
		if("Plan".equals(searchType))
		{ 
			String hql="from UtiliserProfileModel where utiliser_plan LIKE '%"+searchData+"%'";
			lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		}
		
		if("Domain".equals(searchType))
		{
			String hql="from UtiliserProfileModel where uti_dom_hierarchy LIKE '%"+searchData+"%'";
			lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		}
				
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public String findLogic(String pattern_name)
	{
		String hql = "select pattern_logic from PatternDetailsModel where pattern_name='"+pattern_name+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	public String updatePattern(String pattern_name,String p_logic)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update PatternDetailsModel set pattern_logic = :setpattern_logic where pattern_name = :setpattern_name");
		
		hql.setParameter("setpattern_logic", p_logic);
		hql.setParameter("setpattern_name", pattern_name);
		
		int result  = hql.executeUpdate();
		
		if(result==1)
		{
		    return "Pattern Updated";
		}
		else
		{
			return "Unable to do Try Again";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getPlanName()
	{
		String hql = "select plan_name from PlanDetailsModel";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
	}
	
	public String saveUtiliserDetails(UtiliserProfileModel utiProfile)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.save(utiProfile);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	
	public String updateUtilizerDetails(UtiliserProfileModel utiProfile)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(utiProfile);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	@SuppressWarnings("unchecked")
	public int findPatternId(String pattern_name)
	{
		String hql ="select pattern_id from PatternDetailsModel where pattern_name='"+pattern_name+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<UtiliserProfileModel> UtiliserProfileDetails(String utiid)
	{
		
		String hql = "from UtiliserProfileModel where uti_ind_id="+utiid.replaceAll(" ","")+"";
		List<UtiliserProfileModel>  lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public int findUtiId(String utiid)
	{
		String hql = "select uti_ind_id from UtiliserProfileModel where kyc_uti_id='"+utiid+"'";
		List<Integer> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsd.get(0);
	}
	
    @SuppressWarnings("unchecked")
	public String findKycid(int id)
    {
    	String hql = "select kyc_uti_id from UtiliserProfileModel where uti_ind_id='"+id+"'";
    	List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
    	
    	return lsd.get(0);
    }
	
	@SuppressWarnings("unchecked")
	public String findPassword(int id)
	{
		String hql = "select password from UtiliserProfileModel where uti_ind_id='"+id+"'";
    	List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
    	
    	return lsd.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String findDomain(int id)
	{
		String hql = "select uti_dom_hierarchy from UtiliserProfileModel where uti_ind_id='"+id+"'";
    	List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
    	
    	return lsd.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getkycHierarchy()
	{
		String hql = "select doc_hierarchy from DocumentHierarchyModel where doc_type='KYC'";
    	List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
    	
    	return lsd;
	}
	
	@SuppressWarnings("unchecked")
	public int getHierId(String kycHierarchy)
	{
		String hql = "select doc_hir_id from DocumentHierarchyModel where doc_type='"+kycHierarchy+"'";
    	List<Integer> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
    	
    	return lsd.get(0);
	}
	
	 public String saveKycHierarchy(DocumentHierarchyModel kycHierarchy)
	 {
	    String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(kycHierarchy);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	 }
	 
	 @SuppressWarnings("unchecked")
	 public String getFinancialHierarchy()
	 {		 
		String hql = "select doc_hierarchy from DocumentHierarchyModel where doc_type='FINANCIAL'";
	    List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
	    
	    if(lsd.size()>0)
	    {
	    	return lsd.get(0);
	    }
	    else
	    {
	    	return "No Data";
	    }
	 }
	 
	 
	 @SuppressWarnings("unchecked")
	 public String getAcademicHierarchy(String acaDocType)
	 {
		 String hql = "select doc_hierarchy from DocumentHierarchyModel where doc_type='"+acaDocType+"'";
		 List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
		    
		    if(lsd.size()>0)
		    {
		    	if(lsd.contains(null))
		    	{
		    		return "No Data";
		    	}else
		    	{
		    		return lsd.get(0);
		    	}
		    	
		    }
		    else
		    {
		    	return "No Data";
		    }
	 }
	 
	 @SuppressWarnings("unchecked")
	 public List<String> getAcademicDocType()
	 {
			 String hql = "select doc_type from DocumentHierarchyModel where doc_hir_name like 'ACADEMIC%'";
			 List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list(); 
			 
			 return lsd;
	 }
	 
	 public String saveDomain(DomainLevelModel domainModel)
	 {
		    String txStatus="";
			Session docSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=docSession.beginTransaction();
			docSession.saveOrUpdate(domainModel);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				docSession.close();
			}
			return txStatus;
	 }
	 
	@SuppressWarnings("unchecked")
	public List<String> getDomainName()
	 {
		 String hql = "select domain_name from DomainLevelModel";
		 List<String> domainList = sessionFactory.getCurrentSession().createQuery(hql).list();
		 
		 return domainList;
	 }
	 
	 @SuppressWarnings("unchecked")
		public String getDomainHierarchy(String columnId,String domainTable)
		 {
			String hierarchyData = "";
		    
			String domain_name_hql = "select sub_domain_name from "+domainTable+" ";
			List<String> dom_name_data = sessionFactory.getCurrentSession().createQuery(domain_name_hql).list();	
			
			String domain_id_hql = "select "+columnId+" from "+domainTable+" ";
			List<Integer> dom_id_data = sessionFactory.getCurrentSession().createQuery(domain_id_hql).list();
			 			
				if(dom_name_data.size()>0)
				{
					for(int i=0;i<dom_name_data.size();i++)
					{
						hierarchyData += dom_id_data.get(i) + ":" + dom_name_data.get(i) + ",";						
					}
					hierarchyData = hierarchyData.substring(0, hierarchyData.length()-1);
				}
				else
				{
					hierarchyData = "No Data"; 
				}
						 			 
			 return hierarchyData;
		 }
	 
	 @SuppressWarnings("unchecked")
		public int getDomainId(String domainName)
		 {
			 String hql = "select dom_hierarchy_id from DomainHierarchyModel where dom_name='"+domainName+"'";
			 List<Integer> dom_hierarchy_data = sessionFactory.getCurrentSession().createQuery(hql).list();
			 
			 return dom_hierarchy_data.get(0);
		 }
	 
	 @SuppressWarnings("unchecked")
		public int getParentId(String parentIdColumn,String parentTypeColum,String domainType,String parentTableName)
		 {
			 String hql = "select "+parentIdColumn+" from "+parentTableName+" where "+parentTypeColum+"='"+domainType+"'";
			 List<Integer> domain_parent_id = sessionFactory.getCurrentSession().createQuery(hql).list();
			 
			 return domain_parent_id.get(0);
		 }
	 
	    public String saveSubDomainLevel1(SubDomainLevel1Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }
	    
	    public String saveSubDomainLevel2(SubDomainLevel2Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }
	   
	    public String saveSubDomainLevel3(SubDomainLevel3Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }
	   
	    public String saveSubDomainLevel4(SubDomainLevel4Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }
	   
	    public String saveSubDomainLevel5(SubDomainLevel5Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }
	    
	    public String saveSubDomainLevel6(SubDomainLevel6Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }
	    
	    public String saveSubDomainLevel7(SubDomainLevel7Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }
	   
	    public String saveSubDomainLevel8(SubDomainLevel8Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }
	   
	    public String saveSubDomainLevel9(SubDomainLevel9Model subDomainModel)
	    {
	    	 String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(subDomainModel);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
	    }

		public String saveSubDomainLevel10(SubDomainLevel10Model subDomainModel) {
			
				String txStatus = "";
				Session docSession = sessionFactory.openSession();
				Transaction tx = null;
				try {
					tx = docSession.beginTransaction();
					docSession.saveOrUpdate(subDomainModel);
					tx.commit();
					if (tx.wasCommitted()) {
						txStatus = "success";
					} else {
						txStatus = "failure";
					}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
		}
		
		@SuppressWarnings("unchecked")
		public String getEntireHierarchy(String domainLevel,String domainValue)
		{
			String entireDomainHierarchy = "";
			
			if(domainLevel.equals("subDomainlevel1"))
			{
				String child1sthql = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+domainValue+"";
				List<String> child1stData = sessionFactory.getCurrentSession().createQuery(child1sthql).list();
				
				String hql1 = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+domainValue+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata1.get(0)+"";
				List<String> lsdata2 = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
				
				entireDomainHierarchy += lsdata2.get(0)+","+child1stData.get(0);
			}
			
			if(domainLevel.equals("subDomainlevel2"))
			{
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+domainValue+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+domainValue+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2;
				
			}
			
			if(domainLevel.equals("subDomainlevel3"))
			{
				String childsthql3 = "select sub_domain_name from SubDomainLevel3Model where sub_domain_level_3_id="+domainValue+"";
				List<String> childstData3 = sessionFactory.getCurrentSession().createQuery(childsthql3).list();
				
				String child3 = childstData3.get(0);
				
				String hql2 = "select parent_domain_id from SubDomainLevel3Model where sub_domain_level_3_id="+domainValue+"";
				List<Integer> lsdata2 = sessionFactory.getCurrentSession().createQuery(hql2).list();
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				 
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2+","+child3;
				
			}
			
			if(domainLevel.equals("subDomainlevel4"))
			{
				String childsthql4 = "select sub_domain_name from SubDomainLevel4Model where sub_domain_level_4_id="+domainValue+"";
				List<String> childstData4 = sessionFactory.getCurrentSession().createQuery(childsthql4).list();
				
				String child4 = childstData4.get(0);
				
				String hql3 = "select parent_domain_id from SubDomainLevel4Model where sub_domain_level_4_id="+domainValue+"";
				List<Integer> lsdata3 = sessionFactory.getCurrentSession().createQuery(hql3).list();
				
				
				String childsthql3 = "select sub_domain_name from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<String> childstData3 = sessionFactory.getCurrentSession().createQuery(childsthql3).list();
				
				String child3 = childstData3.get(0);
				
				String hql2 = "select parent_domain_id from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<Integer> lsdata2 = sessionFactory.getCurrentSession().createQuery(hql2).list();
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				 
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2+","+child3+","+child4;
				
			}
			
			if(domainLevel.equals("subDomainlevel5"))
			{
				
				String childsthql5 = "select sub_domain_name from SubDomainLevel5Model where sub_domain_level_5_id="+domainValue+"";
				List<String> childstData5 = sessionFactory.getCurrentSession().createQuery(childsthql5).list();
				
				String child5 = childstData5.get(0);
				
				String hql4 = "select parent_domain_id from SubDomainLevel5Model where sub_domain_level_5_id="+domainValue+"";
				List<Integer> lsdata4 = sessionFactory.getCurrentSession().createQuery(hql4).list();
				
				String childsthql4 = "select sub_domain_name from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<String> childstData4 = sessionFactory.getCurrentSession().createQuery(childsthql4).list();
				
				String child4 = childstData4.get(0);
				
				String hql3 = "select parent_domain_id from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<Integer> lsdata3 = sessionFactory.getCurrentSession().createQuery(hql3).list();
				
				
				String childsthql3 = "select sub_domain_name from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<String> childstData3 = sessionFactory.getCurrentSession().createQuery(childsthql3).list();
				
				String child3 = childstData3.get(0);
				
				String hql2 = "select parent_domain_id from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<Integer> lsdata2 = sessionFactory.getCurrentSession().createQuery(hql2).list();
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				 
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2+","+child3+","+child4+","+child5;
				
			}
			
			if(domainLevel.equals("subDomainlevel6"))
			{
				
				String childsthql6 = "select sub_domain_name from SubDomainLevel6Model where sub_domain_level_6_id="+domainValue+"";
				List<String> childstData6 = sessionFactory.getCurrentSession().createQuery(childsthql6).list();
				
				String child6 = childstData6.get(0);
				
				String hql5 = "select parent_domain_id from SubDomainLevel6Model where sub_domain_level_6_id="+domainValue+"";
				List<Integer> lsdata5 = sessionFactory.getCurrentSession().createQuery(hql5).list();
								
				String childsthql5 = "select sub_domain_name from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<String> childstData5 = sessionFactory.getCurrentSession().createQuery(childsthql5).list();
				
				String child5 = childstData5.get(0);
				
				String hql4 = "select parent_domain_id from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<Integer> lsdata4 = sessionFactory.getCurrentSession().createQuery(hql4).list();
				
				String childsthql4 = "select sub_domain_name from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<String> childstData4 = sessionFactory.getCurrentSession().createQuery(childsthql4).list();
				
				String child4 = childstData4.get(0);
				
				String hql3 = "select parent_domain_id from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<Integer> lsdata3 = sessionFactory.getCurrentSession().createQuery(hql3).list();
					
				String childsthql3 = "select sub_domain_name from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<String> childstData3 = sessionFactory.getCurrentSession().createQuery(childsthql3).list();
				
				String child3 = childstData3.get(0);
				
				String hql2 = "select parent_domain_id from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<Integer> lsdata2 = sessionFactory.getCurrentSession().createQuery(hql2).list();
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				 
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2+","+child3+","+child4+","+child5+","+child6;
				
			}
			
			if(domainLevel.equals("subDomainlevel7"))
			{
				
				String childsthql7 = "select sub_domain_name from SubDomainLevel7Model where sub_domain_level_7_id="+domainValue+"";
				List<String> childstData7 = sessionFactory.getCurrentSession().createQuery(childsthql7).list();
				
				String child7 = childstData7.get(0);
				
				String hql6 = "select parent_domain_id from SubDomainLevel7Model where sub_domain_level_7_id="+domainValue+"";
				List<Integer> lsdata6 = sessionFactory.getCurrentSession().createQuery(hql6).list();
					
				
				String childsthql6 = "select sub_domain_name from SubDomainLevel6Model where sub_domain_level_6_id="+lsdata6.get(0)+"";
				List<String> childstData6 = sessionFactory.getCurrentSession().createQuery(childsthql6).list();
				
				String child6 = childstData6.get(0);
				
				String hql5 = "select parent_domain_id from SubDomainLevel6Model where sub_domain_level_6_id="+lsdata6.get(0)+"";
				List<Integer> lsdata5 = sessionFactory.getCurrentSession().createQuery(hql5).list();
								
				String childsthql5 = "select sub_domain_name from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<String> childstData5 = sessionFactory.getCurrentSession().createQuery(childsthql5).list();
				
				String child5 = childstData5.get(0);
				
				String hql4 = "select parent_domain_id from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<Integer> lsdata4 = sessionFactory.getCurrentSession().createQuery(hql4).list();
				
				String childsthql4 = "select sub_domain_name from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<String> childstData4 = sessionFactory.getCurrentSession().createQuery(childsthql4).list();
				
				String child4 = childstData4.get(0);
				
				String hql3 = "select parent_domain_id from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<Integer> lsdata3 = sessionFactory.getCurrentSession().createQuery(hql3).list();
					
				String childsthql3 = "select sub_domain_name from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<String> childstData3 = sessionFactory.getCurrentSession().createQuery(childsthql3).list();
				
				String child3 = childstData3.get(0);
				
				String hql2 = "select parent_domain_id from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<Integer> lsdata2 = sessionFactory.getCurrentSession().createQuery(hql2).list();
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				 
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2+","+child3+","+child4+","+child5+","+child6+","+child7;
				
			}
			
			if(domainLevel.equals("subDomainlevel8"))
			{
				
				String childsthql8 = "select sub_domain_name from SubDomainLevel8Model where sub_domain_level_8_id="+domainValue+"";
				List<String> childstData8 = sessionFactory.getCurrentSession().createQuery(childsthql8).list();
				
				String child8 = childstData8.get(0);
				
				String hql7 = "select parent_domain_id from SubDomainLevel8Model where sub_domain_level_8_id="+domainValue+"";
				List<Integer> lsdata7 = sessionFactory.getCurrentSession().createQuery(hql7).list();
				
				String childsthql7 = "select sub_domain_name from SubDomainLevel7Model where sub_domain_level_7_id="+lsdata7.get(0)+"";
				List<String> childstData7 = sessionFactory.getCurrentSession().createQuery(childsthql7).list();
				
				String child7 = childstData7.get(0);
				
				String hql6 = "select parent_domain_id from SubDomainLevel7Model where sub_domain_level_7_id="+lsdata7.get(0)+"";
				List<Integer> lsdata6 = sessionFactory.getCurrentSession().createQuery(hql6).list();
					
				
				String childsthql6 = "select sub_domain_name from SubDomainLevel6Model where sub_domain_level_6_id="+lsdata6.get(0)+"";
				List<String> childstData6 = sessionFactory.getCurrentSession().createQuery(childsthql6).list();
				
				String child6 = childstData6.get(0);
				
				String hql5 = "select parent_domain_id from SubDomainLevel6Model where sub_domain_level_6_id="+lsdata6.get(0)+"";
				List<Integer> lsdata5 = sessionFactory.getCurrentSession().createQuery(hql5).list();
								
				String childsthql5 = "select sub_domain_name from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<String> childstData5 = sessionFactory.getCurrentSession().createQuery(childsthql5).list();
				
				String child5 = childstData5.get(0);
				
				String hql4 = "select parent_domain_id from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<Integer> lsdata4 = sessionFactory.getCurrentSession().createQuery(hql4).list();
				
				String childsthql4 = "select sub_domain_name from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<String> childstData4 = sessionFactory.getCurrentSession().createQuery(childsthql4).list();
				
				String child4 = childstData4.get(0);
				
				String hql3 = "select parent_domain_id from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<Integer> lsdata3 = sessionFactory.getCurrentSession().createQuery(hql3).list();
					
				String childsthql3 = "select sub_domain_name from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<String> childstData3 = sessionFactory.getCurrentSession().createQuery(childsthql3).list();
				
				String child3 = childstData3.get(0);
				
				String hql2 = "select parent_domain_id from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<Integer> lsdata2 = sessionFactory.getCurrentSession().createQuery(hql2).list();
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				 
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2+","+child3+","+child4+","+child5+","+child6+","+child7+","+child8;
				
			}
			
			if(domainLevel.equals("subDomainlevel9"))
			{
				
				String childsthql9 = "select sub_domain_name from SubDomainLevel9Model where sub_domain_level_9_id="+domainValue+"";
				List<String> childstData9 = sessionFactory.getCurrentSession().createQuery(childsthql9).list();
				
				String child9 = childstData9.get(0);
				
				String hql8 = "select parent_domain_id from SubDomainLevel9Model where sub_domain_level_9_id="+domainValue+"";
				List<Integer> lsdata8 = sessionFactory.getCurrentSession().createQuery(hql8).list();
				
				String childsthql8 = "select sub_domain_name from SubDomainLevel8Model where sub_domain_level_8_id="+lsdata8.get(0)+"";
				List<String> childstData8 = sessionFactory.getCurrentSession().createQuery(childsthql8).list();
				
				String child8 = childstData8.get(0);
				
				String hql7 = "select parent_domain_id from SubDomainLevel8Model where sub_domain_level_8_id="+lsdata8.get(0)+"";
				List<Integer> lsdata7 = sessionFactory.getCurrentSession().createQuery(hql7).list();
				
				String childsthql7 = "select sub_domain_name from SubDomainLevel7Model where sub_domain_level_7_id="+lsdata7.get(0)+"";
				List<String> childstData7 = sessionFactory.getCurrentSession().createQuery(childsthql7).list();
				
				String child7 = childstData7.get(0);
				
				String hql6 = "select parent_domain_id from SubDomainLevel7Model where sub_domain_level_7_id="+lsdata7.get(0)+"";
				List<Integer> lsdata6 = sessionFactory.getCurrentSession().createQuery(hql6).list();
					
				
				String childsthql6 = "select sub_domain_name from SubDomainLevel6Model where sub_domain_level_6_id="+lsdata6.get(0)+"";
				List<String> childstData6 = sessionFactory.getCurrentSession().createQuery(childsthql6).list();
				
				String child6 = childstData6.get(0);
				
				String hql5 = "select parent_domain_id from SubDomainLevel6Model where sub_domain_level_6_id="+lsdata6.get(0)+"";
				List<Integer> lsdata5 = sessionFactory.getCurrentSession().createQuery(hql5).list();
								
				String childsthql5 = "select sub_domain_name from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<String> childstData5 = sessionFactory.getCurrentSession().createQuery(childsthql5).list();
				
				String child5 = childstData5.get(0);
				
				String hql4 = "select parent_domain_id from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<Integer> lsdata4 = sessionFactory.getCurrentSession().createQuery(hql4).list();
				
				String childsthql4 = "select sub_domain_name from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<String> childstData4 = sessionFactory.getCurrentSession().createQuery(childsthql4).list();
				
				String child4 = childstData4.get(0);
				
				String hql3 = "select parent_domain_id from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<Integer> lsdata3 = sessionFactory.getCurrentSession().createQuery(hql3).list();
					
				String childsthql3 = "select sub_domain_name from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<String> childstData3 = sessionFactory.getCurrentSession().createQuery(childsthql3).list();
				
				String child3 = childstData3.get(0);
				
				String hql2 = "select parent_domain_id from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<Integer> lsdata2 = sessionFactory.getCurrentSession().createQuery(hql2).list();
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				 
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2+","+child3+","+child4+","+child5+","+child6+","+child7+","+child8+","+child9;
				
			}
			
			if(domainLevel.equals("subDomainlevel10"))
			{
				String childsthql10 = "select sub_domain_name from SubDomainLevel10Model where sub_domain_level_10_id="+domainValue+"";
				List<String> childstData10 = sessionFactory.getCurrentSession().createQuery(childsthql10).list();
				
				String child10 = childstData10.get(0);
				
				String hql9 = "select parent_domain_id from SubDomainLevel10Model where sub_domain_level_10_id="+domainValue+"";
				List<Integer> lsdata9 = sessionFactory.getCurrentSession().createQuery(hql9).list();
				
				String childsthql9 = "select sub_domain_name from SubDomainLevel9Model where sub_domain_level_9_id="+lsdata9.get(0)+"";
				List<String> childstData9 = sessionFactory.getCurrentSession().createQuery(childsthql9).list();
				
				String child9 = childstData9.get(0);
				
				String hql8 = "select parent_domain_id from SubDomainLevel9Model where sub_domain_level_9_id="+lsdata9.get(0)+"";
				List<Integer> lsdata8 = sessionFactory.getCurrentSession().createQuery(hql8).list();
				
				String childsthql8 = "select sub_domain_name from SubDomainLevel8Model where sub_domain_level_8_id="+lsdata8.get(0)+"";
				List<String> childstData8 = sessionFactory.getCurrentSession().createQuery(childsthql8).list();
				
				String child8 = childstData8.get(0);
				
				String hql7 = "select parent_domain_id from SubDomainLevel8Model where sub_domain_level_8_id="+lsdata8.get(0)+"";
				List<Integer> lsdata7 = sessionFactory.getCurrentSession().createQuery(hql7).list();
				
				String childsthql7 = "select sub_domain_name from SubDomainLevel7Model where sub_domain_level_7_id="+lsdata7.get(0)+"";
				List<String> childstData7 = sessionFactory.getCurrentSession().createQuery(childsthql7).list();
				
				String child7 = childstData7.get(0);
				
				String hql6 = "select parent_domain_id from SubDomainLevel7Model where sub_domain_level_7_id="+lsdata7.get(0)+"";
				List<Integer> lsdata6 = sessionFactory.getCurrentSession().createQuery(hql6).list();
					
				
				String childsthql6 = "select sub_domain_name from SubDomainLevel6Model where sub_domain_level_6_id="+lsdata6.get(0)+"";
				List<String> childstData6 = sessionFactory.getCurrentSession().createQuery(childsthql6).list();
				
				String child6 = childstData6.get(0);
				
				String hql5 = "select parent_domain_id from SubDomainLevel6Model where sub_domain_level_6_id="+lsdata6.get(0)+"";
				List<Integer> lsdata5 = sessionFactory.getCurrentSession().createQuery(hql5).list();
								
				String childsthql5 = "select sub_domain_name from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<String> childstData5 = sessionFactory.getCurrentSession().createQuery(childsthql5).list();
				
				String child5 = childstData5.get(0);
				
				String hql4 = "select parent_domain_id from SubDomainLevel5Model where sub_domain_level_5_id="+lsdata5.get(0)+"";
				List<Integer> lsdata4 = sessionFactory.getCurrentSession().createQuery(hql4).list();
				
				String childsthql4 = "select sub_domain_name from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<String> childstData4 = sessionFactory.getCurrentSession().createQuery(childsthql4).list();
				
				String child4 = childstData4.get(0);
				
				String hql3 = "select parent_domain_id from SubDomainLevel4Model where sub_domain_level_4_id="+lsdata4.get(0)+"";
				List<Integer> lsdata3 = sessionFactory.getCurrentSession().createQuery(hql3).list();
					
				String childsthql3 = "select sub_domain_name from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<String> childstData3 = sessionFactory.getCurrentSession().createQuery(childsthql3).list();
				
				String child3 = childstData3.get(0);
				
				String hql2 = "select parent_domain_id from SubDomainLevel3Model where sub_domain_level_3_id="+lsdata3.get(0)+"";
				List<Integer> lsdata2 = sessionFactory.getCurrentSession().createQuery(hql2).list();
				
				String childsthql2 = "select sub_domain_name from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<String> childstData2 = sessionFactory.getCurrentSession().createQuery(childsthql2).list();
				
				String child2 = childstData2.get(0);
				
				String hql1 = "select parent_domain_id from SubDomainLevel2Model where sub_domain_level_2_id="+lsdata2.get(0)+"";
				List<Integer> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
				 
				String childsthql1 = "select sub_domain_name from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<String> childstData1 = sessionFactory.getCurrentSession().createQuery(childsthql1).list();
				
				String child1 = childstData1.get(0);
				
				String hql = "select parent_domain_id from SubDomainLevel1Model where sub_domain_level_1_id="+lsdata1.get(0)+"";
				List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String parentDomain = "select domain_name from DomainLevelModel where domain_id="+lsdata.get(0)+"";
				List<String> rootDomain = sessionFactory.getCurrentSession().createQuery(parentDomain).list();
										
				String parent = rootDomain.get(0);
				
				entireDomainHierarchy += parent +","+child1+","+child2+","+child3+","+child4+","+child5+","+child6+","+child7+","+child8+","+child9+","+child10;
				
			}
			
			return entireDomainHierarchy;
		}
		
		@SuppressWarnings("unchecked")
		public String getMainDomainStatus(String domainValue)
		 {
			
			String domainHql  = "select domain_id from DomainLevelModel where domain_name='"+domainValue+"'";
			List<Integer> domainId = sessionFactory.getCurrentSession().createQuery(domainHql).list();
			 
			String hql = "select sub_domain_name from SubDomainLevel1Model where parent_domain_id="+domainId.get(0)+"";
			List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(lsdata.size()>0)
			{
				String childData = "";
				for(int i=0;i<lsdata.size();i++)
				{
					childData += lsdata.get(i)+",";
				}
				childData = childData.substring(0,childData.length()-1);
				return childData;
			}
			else
			{
				return "NoChild";
			}
			
		 }
		
		@SuppressWarnings("unchecked")
		public String getSubDomainStatus(String domainValue,String child_table_name)
		{
			
			String hql = "select sub_domain_name from "+child_table_name+" where parent_domain_id="+domainValue+"";
			List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(lsdata.size()>0)
			{
				String childData = "";
				for(int i=0;i<lsdata.size();i++)
				{
					childData += lsdata.get(i)+",";
				}
				childData = childData.substring(0,childData.length()-1);
				return childData;
			}
			else
			{
				return "NoChild";
			}
		}
		
		public int getDeleteDomain(String tableName,String primaryColumn,String domainValue)
		{
			int result = 0;			
			if(tableName.equals("DomainLevelModel"))
			{
				String hql = "DELETE FROM "+tableName+" WHERE "+primaryColumn+" = :domainName";
			    Query query = sessionFactory.getCurrentSession().createQuery(hql);
			    query.setParameter("domainName", domainValue);
			    result = query.executeUpdate();
			}
			else
			{
				Integer id = Integer.parseInt(domainValue);
				String hql = "DELETE FROM "+tableName+" WHERE "+primaryColumn+" = :domainId";
			    Query query = sessionFactory.getCurrentSession().createQuery(hql);
			    query.setParameter("domainId", id);
			    result = query.executeUpdate();
			}
		    
		    return result;
		}
		
		@SuppressWarnings("unchecked")
		public int getDomainIdbyDomainName(String domainName)
		{
			String domainId = "select domain_id from DomainLevelModel where domain_name='"+domainName+"'";
			List<Integer> subDomainIdList = sessionFactory.getCurrentSession().createQuery(domainId).list();
			
			return subDomainIdList.get(0);
		}
	    
		
		@SuppressWarnings("unchecked")
		public String subDomainByDomainId(int domainId)
		{
			String searchResult = "";
			
			String subDomainId = "select sub_domain_level_1_id from SubDomainLevel1Model where parent_domain_id="+domainId+"";
			String subDomainName = "select sub_domain_name from SubDomainLevel1Model where parent_domain_id="+domainId+"";
			
			List<Integer> subDomainIdList = sessionFactory.getCurrentSession().createQuery(subDomainId).list();
			List<String> subDomainNameList = sessionFactory.getCurrentSession().createQuery(subDomainName).list();
			
			if(subDomainIdList.size()>0)
			{
				for(int i=0;i<subDomainIdList.size();i++)
				{
					searchResult += subDomainIdList.get(i)+":"+subDomainNameList.get(i)+",";
				}
				
				searchResult = searchResult.substring(0, searchResult.length()-1);
			}
			else
			{
				searchResult += "NoChild";
			}
			
			return searchResult;
		}
		
		@SuppressWarnings("unchecked")
		public String getParentNameById(String parentTableName,String parentIdColumn,int domainId)
		 {
			 
			 String hql = "select sub_domain_name from "+parentTableName+" where "+parentIdColumn+"="+domainId+"";
			 List<String> subDomainName = sessionFactory.getCurrentSession().createQuery(hql).list();
			 return subDomainName.get(0);
					 
		 }
		
		@SuppressWarnings("unchecked")
		public String getHierarchyByParentId(String childTablesName,String childIdColumn,int domainId)
		{
			String searchResult = "";
			
			String subDomainId = "select "+childIdColumn+" from "+childTablesName+" where parent_domain_id="+domainId+"";
			String subDomainName = "select sub_domain_name from "+childTablesName+" where parent_domain_id="+domainId+"";
			
			List<Integer> subDomainIdList = sessionFactory.getCurrentSession().createQuery(subDomainId).list();
			List<String> subDomainNameList = sessionFactory.getCurrentSession().createQuery(subDomainName).list();
			
			if(subDomainIdList.size()>0)
			{
				for(int i=0;i<subDomainIdList.size();i++)
				{
					searchResult += subDomainIdList.get(i)+":"+subDomainNameList.get(i)+",";
				}
				
				searchResult = searchResult.substring(0, searchResult.length()-1);
			}
			else
			{
				searchResult += "NoChild";
			}
			return searchResult;
		}
		
		@SuppressWarnings("unchecked")
		public String getdomainName(String tablesName,String columnName,String selectId)
		{
			String domainName = "select sub_domain_name from "+tablesName+" where "+columnName+"="+selectId+"";
			List<String> subDomainNameList = sessionFactory.getCurrentSession().createQuery(domainName).list();
			
			return subDomainNameList.get(0);
		}
		
	   @SuppressWarnings("unchecked")
		public int getUtiliserId(UtiliserProfileModel utiProfile)
		 {
			 String hql = "select uti_ind_id  from UtiliserProfileModel where kyc_uti_id='"+utiProfile.getKyc_uti_id()+"' and uti_email_id='"+utiProfile.getUti_email_id()+"' and uti_office_first_no='"+utiProfile.getUti_office_first_no()+"'";
			 List<Integer> utiId = sessionFactory.getCurrentSession().createQuery(hql).list();
				
			 return utiId.get(0);
		 }
	   
	@SuppressWarnings("unchecked")
	public String getfinancialSubType()
	{
		   String hql = "select subdoc_type from DocumentHierarchyModel where doc_type='FINANCIAL'";
		   List<String> subdoc_type = sessionFactory.getCurrentSession().createQuery(hql).list();
		   
		   if(subdoc_type.size()>0)
		   {
			   return subdoc_type.get(0);
		   }
		   else
		   {
			   return "No Data";
		   }
    }
	
	@SuppressWarnings("unchecked")
	public String getHierarchyComponents(String hierarchyType)
	 {
		 String hql = "select doc_hierarchy from DocumentHierarchyModel where doc_type='"+hierarchyType+"'";
		   List<String> doc_hierarchy = sessionFactory.getCurrentSession().createQuery(hql).list();
			
		   if(doc_hierarchy.get(0)==null)
		   {
			   return "No Data";
		   }
		   else
		   {
			   return doc_hierarchy.get(0);
		   }
	 }
	
	@SuppressWarnings("unchecked")
	public  List<String> getPlanNameAsPerPlanDomain(String planDomain)
	{
		String hql = "select plan_name from PlanDetailsModel where plan_domain='"+planDomain+"' and plan_type='Utilizer'";
		List<String> planName = sessionFactory.getCurrentSession().createQuery(hql).list();
		return planName;
	}
	
	public String saveNotifyDetails(KycNotificationModel notifyModel)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(notifyModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getIndRegisteredYear(int ind_id)
	{
		String hql="select year(cr_date) from IndSignupModel where ind_id ="+ind_id+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	
	 @SuppressWarnings("unchecked")
	 public List<IndSignupModel> getIndividualDetailsViaKycid(String searchedValue)
	 {		
		 List<IndSignupModel> lsdata = new ArrayList<IndSignupModel>();
		 
		 String indIdhql = "from IndSignupModel where kycid='"+searchedValue+"'";
		 lsdata = sessionFactory.getCurrentSession().createQuery(indIdhql).list();
		 
		 return lsdata;		 
	 }
	    
	 @SuppressWarnings("unchecked")
	 public List<IndSignupModel> getIndividualDetailsViaPlan(String searchedValue)
	 {
         List<IndSignupModel> indDetails = new ArrayList<IndSignupModel>();		 
		 String indIdhql = "from IndSignupModel where plan_available LIKE '%"+searchedValue+"%'";
		 indDetails= sessionFactory.getCurrentSession().createQuery(indIdhql).list();
				 
		 return indDetails;
	 }
	
	 
	 @SuppressWarnings("unchecked")
	 public List<IndSignupModel> getIndividualDetailsViaName(String searchedValue)
	 {
         List<IndSignupModel> indDetails = new ArrayList<IndSignupModel>();
		       
		 String hql = "from IndSignupModel where individual_full_name LIKE '%"+searchedValue+"%'";
		 indDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
		 
		 return indDetails;
	 }
	 	
	 
	 @SuppressWarnings("unchecked")
	 public List<EmailSentStatusModel> getPendingEmail()
	 {
		 String hql = "from EmailSentStatusModel where sent_report='1'";
		 List<EmailSentStatusModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		 
		 return lsdata;
	 }
	    
	 
	 @SuppressWarnings("unchecked")
	 public List<SmsSentStatusModel> getPendingSMS()
	 {
		 String hql = "from SmsSentStatusModel where sent_report='1'";
		 List<SmsSentStatusModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		 
		 return lsdata;
	 }
	 
	 @SuppressWarnings("unchecked")
	 public List<EmailSentStatusModel> getPendingEmailDetails(int email_uniqueId)
	 {
		 String hql = "from EmailSentStatusModel where email_unique_id='"+email_uniqueId+"'";
		 List<EmailSentStatusModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		 
		 return lsdata;
	 }
		
	 @SuppressWarnings("unchecked")
	 public List<SmsSentStatusModel> getPendingSMSDetails(int sms_uniqueId)
	 {
		 String hql = "from SmsSentStatusModel where sms_unique_id='"+sms_uniqueId+"'";
		 List<SmsSentStatusModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		 
		 return lsdata;
	 }
	 
	 public String getUpdateEmailDetails(int email_uniqueId)
	 {
		    org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update EmailSentStatusModel set sent_report = :setsentreport  where email_unique_id = :setemailUniqueId");
			hql.setParameter("setemailUniqueId", email_uniqueId);
			hql.setParameter("setsentreport", "0");
			
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "Done";
			}
			else
			{			
				return "failed";
			}
	 }
	    
    public String getUpdateSmSDetails(int sms_uniqueId)
    {
    	org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update SmsSentStatusModel set sent_report = :setsentreport  where sms_unique_id = :setsmsUniqueId");
    	hql.setParameter("setsmsUniqueId", sms_uniqueId);
		hql.setParameter("setsentreport", "0");
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "Done";
		}
		else
		{			
			return "failed";
		}
    }
    
    @SuppressWarnings("unchecked")
	public List<RegHistoryModel> getdtailsviakycId(String kycId)
    {
        List<RegHistoryModel> regData = new ArrayList<RegHistoryModel>();
        
        String indHql = "select ind_id from IndSignupModel where kycid='"+kycId+"'";
        List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(indHql).list();
		
        if(lsdata.size()>0)
		{
			String regDetailHql = "from RegHistoryModel where ind_id="+lsdata.get(0)+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+lsdata.get(0)+")";
			regData = sessionFactory.getCurrentSession().createQuery(regDetailHql).list();
			
			return regData;
		}
        else
        {
        	return regData;
        }            	
    }
    
    @SuppressWarnings("unchecked")
	public String getPatternName(int patternId)
    {
    	String patternHql = "select pattern_name from PatternDetailsModel where pattern_id="+patternId+"";
        List<String> lsdata = sessionFactory.getCurrentSession().createQuery(patternHql).list();
		return lsdata.get(0);
    }
    
    @SuppressWarnings("unchecked")
	public String getPassword(int utiUnqId)
    {
    	String passwordHql = "select password from UtiliserProfileModel where uti_ind_id="+utiUnqId+"";
        List<String> lsdata = sessionFactory.getCurrentSession().createQuery(passwordHql).list();
		return lsdata.get(0);
    }
    
    @SuppressWarnings("unchecked")
	public String getPlanDomain(int utiUnqId)
    {
    	String planDomainHql = "select uti_plan_domain from UtiliserProfileModel where uti_ind_id="+utiUnqId+"";
        List<String> lsdata = sessionFactory.getCurrentSession().createQuery(planDomainHql).list();
		return lsdata.get(0);
    }
    
    @SuppressWarnings("unchecked")
   	public String getUtilizerPlanName(int utiUnqId)
    {
       	 String planNameHql = "select utiliser_plan from UtiliserProfileModel where uti_ind_id="+utiUnqId+"";
         List<String> lsdata = sessionFactory.getCurrentSession().createQuery(planNameHql).list();
   		 return lsdata.get(0);
    }
    
    @SuppressWarnings("unchecked")
   	public String getUtilizerDomain(int utiUnqId)
    {
       	 String domainHql = "select uti_dom_hierarchy from UtiliserProfileModel where uti_ind_id="+utiUnqId+"";
         List<String> lsdata = sessionFactory.getCurrentSession().createQuery(domainHql).list();
   		 return lsdata.get(0);
    }
    
    @SuppressWarnings("unchecked")
	public Date getUtiRegDate(int utiUnqId)
    {
    	String cr_dateHql = "select uti_cr_date from UtiliserProfileModel where uti_ind_id="+utiUnqId+"";
        List<Date> lsdata = sessionFactory.getCurrentSession().createQuery(cr_dateHql).list();
  		return lsdata.get(0);
    }
   	
	@SuppressWarnings("unchecked")
	public String getUtiProfilePic(int utiUnqId)
	{
		String profile_picHql = "select uti_profile_pic from UtiliserProfileModel where uti_ind_id="+utiUnqId+"";
        List<String> lsdata = sessionFactory.getCurrentSession().createQuery(profile_picHql).list();
  		return lsdata.get(0);
	}
	
	
	public String saveUtilizerPlanDetails(UtilizerPlanDetailsModel utiPlanModel)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(utiPlanModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	public String saveUtilizerProfilePhoto(UtilizerProfilePhotoModel utiProfilePhoto)
	{
		
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(utiProfilePhoto);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public String getPatternDetailAsperPatterName(String patternName)
	{
		String patternHql = "select pattern_logic from PatternDetailsModel where pattern_name='"+patternName+"'";
		List<String> patternData = sessionFactory.getCurrentSession().createQuery(patternHql).list();
  		return patternData.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public int getLastUtilizerIndId()
	{
		String LastUtilizerIndIdHql = "select uti_ind_id from UtiliserProfileModel where uti_cr_date=(select MAX(uti_cr_date) from UtiliserProfileModel)";
        List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(LastUtilizerIndIdHql).list();
        
        if(lsdata.size()>0)
        {
        	return lsdata.get(0);
        }
        else
        {
        	return 0;
        }
  		
	}
	
	public String deleteAcademicHierarchy(String hierarchyName)
	{
		String hql = "DELETE FROM DocumentHierarchyModel WHERE doc_type = :hierarchyName";
	    Query query = sessionFactory.getCurrentSession().createQuery(hql);
	    query.setParameter("hierarchyName", hierarchyName);
	    int result = query.executeUpdate();
	    if(result>0)
	    {
	    	return "success";
	    }
	    else
	    {
	    	return "failure";
	    }
	}
	
	public String updateUtiliserProfilePhoto(UtiliserProfileModel utiProfile)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UtiliserProfileModel set uti_profile_pic = :utiProfilePhoto  where uti_ind_id = :utiUniqueId");
		hql.setParameter("utiUniqueId", utiProfile.getUti_ind_id());
		hql.setParameter("utiProfilePhoto", utiProfile.getUti_profile_pic());
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failure";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Integer> getClenseeYearList()
	{
		String hql = "select distinct(EXTRACT(YEAR from cr_date)) from IndSignupModel";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactUsModel> getAllContactUsDetails(String todaysDateFormat)
	{
		String hql = "from ContactUsModel where contact_date LIKE '%"+todaysDateFormat+"%' ORDER BY contact_date DESC";
		List<ContactUsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public String getContactMssageByConId(int contactId)
	{
		String hql = "select contact_message from ContactUsModel where contact_id="+contactId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Nothing Mentioned.";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactUsModel> getSearchContactUsDetails(String searchType,String searchValue)
	{
		if(searchType.equals("Email Id"))
		{
			String hql = "from ContactUsModel where contact_emailid LIKE '%"+searchValue+"%' ORDER BY contact_date DESC";
			List<ContactUsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			return lsdata;
		}
		else if(searchType.equals("Name"))
		{
			String hql = "from ContactUsModel where contact_name LIKE '%"+searchValue+"%' ORDER BY contact_date DESC";
			List<ContactUsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			return lsdata;
		}
		else if(searchType.equals("Mode of Contact"))
		{
			String hql = "from ContactUsModel where contact_for LIKE '%"+searchValue+"%' ORDER BY contact_date DESC";
			List<ContactUsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			return lsdata;
		}
		else
		{
			String hql = "from ContactUsModel where contact_date LIKE '%"+searchValue+"%' ORDER BY contact_date DESC";
			List<ContactUsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			return lsdata;
		}		
	}
}

