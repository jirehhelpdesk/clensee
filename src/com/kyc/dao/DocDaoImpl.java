package com.kyc.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kyc.bean.DocumentHierarchyBean;
import com.kyc.model.AcademicFileUploadModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.DocumentModel;
import com.kyc.model.SalarySlipModel;
import com.kyc.model.SscDocModel;

@Repository("docdao")
public class DocDaoImpl implements DocDao{

	
	@SuppressWarnings("unchecked")
	public List<String> getKycRegistrationStatus(String kycId)
	{
		String hql = "select kycid from IndSignupModel where kyc_id='"+kycId+"'";
		List<String> kycList = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return kycList;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getKycIdFromDocumentDetails(String kycId)
	{
		String hql = "select docs_data from DocumentDetailModel where docs_data LIKE '%"+kycId+"%'";
		List<String> kycList = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return kycList;
	}
	
	@SuppressWarnings("unchecked")
	public int getIndIdviaDocId(int doc_id)
	{
		String hql = "select kyc_ind_id from DocumentDetailModel where doc_id="+doc_id+"";
		List<Integer> kycList = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return kycList.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String getKycIdViaEmailId(String emailId)
	{
		String hql = "select kycid from IndSignupModel where emailid='"+emailId+"'";
        List<String> kycList = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return kycList.get(0);
	}
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public String insertKYCDocDetailsInRegn(DocumentDetailModel docdetailmodel)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(docdetailmodel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	public String uploadSSC(SscDocModel sscdocmodel)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(sscdocmodel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	
	public String uploadDocument(DocumentModel docmodel)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.save(docmodel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	public DocumentModel getDetail(String kycid) {
		return (DocumentModel) sessionFactory.getCurrentSession().get(DocumentModel.class, kycid);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<DocumentModel> listDocuments() {
		return (List<DocumentModel>) sessionFactory.getCurrentSession().createCriteria(DocumentModel.class).list();
	}
	
	public String getAcademicComp()
	{
			
		String q1 ="select doc_hierarchy from DocumentHierarchyModel where doc_hir_name='ACADEMIC_SSC_DOC'";
		 @SuppressWarnings("rawtypes")
		List ls =sessionFactory.getCurrentSession().createQuery(q1).list();
		
		return ""+ls.get(0); 
	}
	
	public String getAcademicComp2()
	{
	
		
		String q1 ="select doc_hierarchy from DocumentHierarchyModel where doc_hir_name='ACADEMIC_INTERMEDIATE_DOC'";
		 @SuppressWarnings("rawtypes")
		List ls =sessionFactory.getCurrentSession().createQuery(q1).list();
		
		return ""+ls.get(0); 
	}
	
	public String getAcademicComp3()
	{
	
		
		String q1 ="select doc_hierarchy from DocumentHierarchyModel where doc_hir_name='ACADEMIC_GRADUATE_DOC'";
		 @SuppressWarnings("rawtypes")
		List ls =sessionFactory.getCurrentSession().createQuery(q1).list();
		
		return ""+ls.get(0); 
	}
	
	
	public String getFinancialComp()
	{
			
		String q1 ="select doc_hierarchy from DocumentHierarchyModel where doc_hir_name='FINANCIAL_DOC'";
		@SuppressWarnings("rawtypes")
		List ls =sessionFactory.getCurrentSession().createQuery(q1).list();
		
		if(ls.size()>0)
		{
			return ""+ls.get(0); 
		}
		else
		{
			return "";
		}
		
	}
	
	public String getEmployementComp()
	{
	
		
		String q1 ="select doc_hierarchy from DocumentHierarchyModel where doc_hir_name='EMPLOYEMENT_DOC'";
		@SuppressWarnings("rawtypes")
		List ls =sessionFactory.getCurrentSession().createQuery(q1).list();
		
		return ""+ls.get(0); 
	}
	
	public String getBusinessComp()
	{			
		String q1 ="select doc_hierarchy from DocumentHierarchyModel where doc_hir_name='BUSINESS_DOC'";
		@SuppressWarnings("rawtypes")
		List ls =sessionFactory.getCurrentSession().createQuery(q1).list();
		
		return ""+ls.get(0); 
	}
	
	@SuppressWarnings("unchecked")
	public String getLatestKYCPannel(int i)
	{
		String hqlQuery = "select panel_status from DocumentDetailModel where kyc_ind_id="+i+" and doc_type='KYC_DOCUMENT' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+i+" and doc_type='KYC_DOCUMENT')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hqlQuery).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String setKYCPannelDocDetails(DocumentDetailModel docmodel)
	{	
		String hql = "select doc_id from DocumentDetailModel where kyc_ind_id="+docmodel.getKyc_ind_id()+" and doc_type='"+docmodel.getDoc_type()+"' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+docmodel.getKyc_ind_id()+" and doc_type='"+docmodel.getDoc_type()+"')";
		List<Integer> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		docmodel.setDoc_id(lsd.get(0));
		
		String txStatus="";
		Session docdetSession=sessionFactory.openSession();
		Transaction tx1=null;
		try {
		tx1=docdetSession.beginTransaction();
		docdetSession.saveOrUpdate(docmodel);
		tx1.commit();
		if(tx1.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docdetSession.close();
		}
		return txStatus;		
	}
	
	@SuppressWarnings("unchecked")
	public String setPannelDocDetails(DocumentDetailModel docmodel)
	{		
		String hql = "select doc_id from DocumentDetailModel where kyc_ind_id="+docmodel.getKyc_ind_id()+" and doc_type='"+docmodel.getDoc_type()+"' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+docmodel.getKyc_ind_id()+" and doc_type='"+docmodel.getDoc_type()+"')";
		List<Integer> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
						
		if(lsd.size()==0)
		{

		String txStatus="";
		Session docdetSession=sessionFactory.openSession();
		Transaction tx1=null;
		try {
		tx1=docdetSession.beginTransaction();
		docdetSession.saveOrUpdate(docmodel);
		tx1.commit();
		if(tx1.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docdetSession.close();
		}
		return txStatus;

		}

		else
		{
			String hqlQuery = "select doc_name from DocumentDetailModel where kyc_ind_id="+docmodel.getKyc_ind_id()+" and doc_type='"+docmodel.getDoc_type()+"' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+docmodel.getKyc_ind_id()+" and doc_type='"+docmodel.getDoc_type()+"')";
			List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hqlQuery).list();
			
			if(lsdata.get(0)!=null)
			{				
				String txStatus="";
				Session docdetSession=sessionFactory.openSession();
				Transaction tx1=null;
				try {
				tx1=docdetSession.beginTransaction();
				docdetSession.saveOrUpdate(docmodel);
				tx1.commit();
				if(tx1.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docdetSession.close();
				}
				return txStatus;
			}
			
			else
			{
				docmodel.setDoc_id(lsd.get(0));
				String txStatus="";
				Session docdetSession=sessionFactory.openSession();
				Transaction tx1=null;
				try {
				tx1=docdetSession.beginTransaction();
				docdetSession.saveOrUpdate(docmodel);
				tx1.commit();
				if(tx1.wasCommitted()){
					txStatus="success";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docdetSession.close();
				}
				return txStatus;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String uploadotherDocumentDetail(DocumentDetailModel docdetailmodel)
	{
		
		String txStatus="";
		Session docdetSession=sessionFactory.openSession();
		Transaction tx1=null;
		try {
		tx1=docdetSession.beginTransaction();
		docdetSession.saveOrUpdate(docdetailmodel);
		tx1.commit();
		if(tx1.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docdetSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String uploadKYCDocumentDetail(DocumentDetailModel docdetailmodel)
	{		
			String txStatus="";
			Session docdetSession=sessionFactory.openSession();
			Transaction tx1=null;
			try {
			tx1=docdetSession.beginTransaction();
			docdetSession.saveOrUpdate(docdetailmodel);
			tx1.commit();
			if(tx1.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he)
			{
				System.out.println(he.getMessage());
				
			} finally {
				docdetSession.close();
			}
			return txStatus;		
	}
	
	
	@SuppressWarnings("unchecked")
	public String uploadDocumentDetail(DocumentDetailModel docdetailmodel)
	{
		String hql = "select doc_id from DocumentDetailModel where kyc_ind_id="+docdetailmodel.getKyc_ind_id()+" and doc_type='"+docdetailmodel.getDoc_type()+"' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+docdetailmodel.getKyc_ind_id()+" and doc_type='"+docdetailmodel.getDoc_type()+"')";
		List<Integer> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
				
		String hqlQuery = "select doc_name from DocumentDetailModel where kyc_ind_id="+docdetailmodel.getKyc_ind_id()+" and doc_type='"+docdetailmodel.getDoc_type()+"' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+docdetailmodel.getKyc_ind_id()+" and doc_type='"+docdetailmodel.getDoc_type()+"')";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hqlQuery).list();
				
		if(lsdata.get(0)!=null)
		{
			String txStatus="";
			Session docdetSession=sessionFactory.openSession();
			Transaction tx1=null;
			try {
			tx1=docdetSession.beginTransaction();
			docdetSession.saveOrUpdate(docdetailmodel);
			tx1.commit();
			if(tx1.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he)
			{
				System.out.println(he.getMessage());
				
			} finally {
				docdetSession.close();
			}
			return txStatus;
		}
		
		else
		{
																	
		docdetailmodel.setDoc_id(lsd.get(0));	
			
		String txStatus="";
		Session docdetSession=sessionFactory.openSession();
		Transaction tx1=null;
		try {
		tx1=docdetSession.beginTransaction();
		docdetSession.saveOrUpdate(docdetailmodel);
		tx1.commit();
		if(tx1.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docdetSession.close();
		}
		return txStatus;
		}
		
		
	}
	
	
	
	public String updateDocumentDetail(DocumentDetailModel docdetailmodel)
	{
		String txStatus="";
		Session docdetSession=sessionFactory.openSession();
		Transaction tx1=null;
		try {
					
		tx1=docdetSession.beginTransaction();		
	    Query query = docdetSession.createQuery("update DocumentDetailModel set docs_data = :data" +" where doc_name = :criteria1 and kyc_ind_id= :criteria2");
        query.setParameter("data", docdetailmodel.getDocs_data());
        query.setParameter("criteria1", docdetailmodel.getDoc_name());
        query.setParameter("criteria2", docdetailmodel.getKyc_ind_id());
        int result = query.executeUpdate();
		tx1.commit();
		if(tx1.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docdetSession.close();
		}
		return txStatus;
	}
	
	
	public String getDocsDataSSC(DocumentDetailModel docdetailmodel)
	{
		
		int k = docdetailmodel.getKyc_ind_id();		
		String hql = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.doc_name='SSC-MarkSheet'";		
		List ls =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(ls.isEmpty())
		{
			return "value Not Found";
		}
		else
		{
			return ""+ls.get(0);
		}
		
	}
	
	public String getDocsDataInter(DocumentDetailModel docdetailmodel)
	{
		
		int k = docdetailmodel.getKyc_ind_id();
		
		String hql = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.doc_name='Inter-MarkSheet'";
		
		List ls =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(ls.isEmpty())
		{
			return "value Not Found";
		}
		else
		{
			return ""+ls.get(0);
		}
		
	}
	
	public String getDocsDataGraduate(DocumentDetailModel docdetailmodel)
	{
		
		int k = docdetailmodel.getKyc_ind_id();
		
		String hql = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.doc_name='Graduate-MarkSheet'";
		
		List ls =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(ls.isEmpty())
		{
			return "value Not Found";
		}
		else
		{
			return ""+ls.get(0);
		}
		
	}
	
	public String getDocsDataFinancial(DocumentDetailModel docdetailmodel)
	{
		
		int k = docdetailmodel.getKyc_ind_id();
		
		String hql = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.doc_type='Financial'";
		
		List ls =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(ls.isEmpty())
		{
			return "value Not Found";
		}
		else
		{
			return ""+ls.get(0);
		}
		
	}
	
	public String getDocsDataEmployement(DocumentDetailModel docdetailmodel)
	{
		
		int k = docdetailmodel.getKyc_ind_id();
		
		/*String DocView = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.doc_type='Employement' and dd.cr_date=(select Max(dd.cr_date) from DocumentDetailModel dd where where dd.kyc_ind_id="+k+" and and dd.doc_type='Employement')";
		List lsview =sessionFactory.getCurrentSession().createQuery(DocView).list();
		
		if(lsview.isEmpty())
		{*/
			String DocNew = "select dd.doc_hierarchy from DocumentHierarchyModel dd where dd.doc_type='EMPLOYEMENT'";
			List lsnew =sessionFactory.getCurrentSession().createQuery(DocNew).list();
			return ""+lsnew.get(0);
		/*}
		else
		{			
			return ""+lsview.get(0);
		}*/		
	}
	
	public String getDocsDataBusiness(DocumentDetailModel docdetailmodel)
	{		
		int k = docdetailmodel.getKyc_ind_id();
		
		String hql = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.doc_type='Business'";
		
		List ls =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(ls.isEmpty())
		{
			return "value Not Found";
		}
		else
		{
			return ""+ls.get(0);
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String> getAcademicDocComp()
	{
		String hql = "Select dh.doc_type from DocumentHierarchyModel dh where dh.doc_hir_name LIKE 'ACADEMIC%'";
		List<String> ls= sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return ls;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getDocsVeiwOrNew(DocumentHierarchyBean docbean)
	{
		String pannelArray[] = docbean.getDoc_hierarchy().split(",");		
		int ind_id = docbean.getDoc_hir_id();
		
		StringBuilder docHierAndData= new StringBuilder();
		
		for(int i=0;i<pannelArray.length;i++)
		{
			String hierarchyHql = "select doc_hierarchy from DocumentHierarchyModel where doc_type='"+pannelArray[i]+"'";
			List<String> hierarchyList = sessionFactory.getCurrentSession().createQuery(hierarchyHql).list();
			
			docHierAndData.append(hierarchyList.get(0));
			docHierAndData.append("/");
			
			String academicDataHql = "select docs_data from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data like '"+pannelArray[i]+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data like '"+pannelArray[i]+"%')";
			List<String> panelData = sessionFactory.getCurrentSession().createQuery(academicDataHql).list();
			
			if(panelData.size()>0)
			{
				docHierAndData.append(panelData.get(0));
				docHierAndData.append("/");								
			}
			else
			{
				docHierAndData.append("No Data");
				docHierAndData.append("/");
			}
			
			String academicdocNameHql = "select doc_name from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data like '"+pannelArray[i]+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data like '"+pannelArray[i]+"%')";
			List<String> docName = sessionFactory.getCurrentSession().createQuery(academicdocNameHql).list();
			
			if(docName.size()>0)
			{
				docHierAndData.append(docName.get(0));				
				docHierAndData.append("#");
			}
			else
			{
				docHierAndData.append("No Document");
				docHierAndData.append("#");
			}
			
		}
		
		return docHierAndData.toString();
	}
	
	@SuppressWarnings("unchecked")
	public String getOtherDocsVeiwOrNew(DocumentHierarchyBean docbean)
	{
		String hqlForDocHierarchy="";
		String hqlForDocData="";
		StringBuilder docHierAndData= new StringBuilder();
		int k = docbean.getDoc_hir_id();
				
			hqlForDocData = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.doc_name like 'FINANCIAL%' and dd.cr_date = (select Max(cr_date) from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.doc_name like 'FINANCIAL%')";
			List<String> lsData= sessionFactory.getCurrentSession().createQuery(hqlForDocData).list();
			if(!lsData.isEmpty()) {
				System.out.println("Not Empty");
				
				docHierAndData.append(lsData.get(0)+"/");
			} else {
				System.out.println("Empty");
				hqlForDocHierarchy = "select dh.doc_hierarchy from DocumentHierarchyModel dh where dh.doc_type='FINANCIAL'";
				List<String> lsHier= sessionFactory.getCurrentSession().createQuery(hqlForDocHierarchy).list();
				if(!lsHier.isEmpty()) {
					docHierAndData.append(lsHier.get(0)+"/");
				}				
			}							
		return docHierAndData.toString();		
	}
	
	@SuppressWarnings("unchecked")
	public List<DocumentDetailModel> getHistory(DocumentDetailModel docdetails,HttpSession session)
	{
		int k = docdetails.getKyc_ind_id();		
		String docName=docdetails.getDoc_name();		
		String hql = "from DocumentDetailModel where kyc_ind_id="+k+" and docs_data like '"+docName+"%' ORDER BY cr_date DESC";		
		List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		
		return lsdata;
	}
	
	
	public String getViewMore(DocumentDetailModel docdetails)
	{
        String hql = "select dd.docs_data from DocumentDetailModel dd where dd.doc_id="+docdetails.getDoc_id()+"";		
		List ls =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(ls.isEmpty())
		{
			return "value Not Found";
		}
		else
		{
			return ""+ls.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getDocName(DocumentDetailModel docdetails)
	{
		
		String hql = "select doc_name from DocumentDetailModel where doc_id="+docdetails.getDoc_id()+"";
		
		List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return ""+ls.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String getrecentDocName(DocumentDetailModel docdetails)
	{
		
		String hql = "select doc_name from DocumentDetailModel where kyc_ind_id="+docdetails.getDoc_id()+" and doc_name like '"+docdetails.getDoc_name()+"' and cr_date=(select cr_date=Max(cr_date) from DocumentDetailsModel where kyc_ind_id="+docdetails.getDoc_id()+" and doc_name like '"+docdetails.getDoc_name()+"')";
		
		List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return ""+ls.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> getDocIds(DocumentDetailModel docdetails)
	{
		String hql = "select dd.doc_id from DocumentDetailModel dd where dd.kyc_ind_id="+docdetails.getKyc_ind_id()+" and dd.doc_name like '"+docdetails.getDoc_name()+"%'";
		
		List<Integer> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return ls;
	} 
	
	
	@SuppressWarnings("unchecked")
	public String getKYCComp()
	{
		String hql = "Select dh.doc_hierarchy from DocumentHierarchyModel dh where dh.doc_hir_name LIKE 'KYC%'";
		List<String> ls= sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return ""+ls.get(0);		
	}
	
	@SuppressWarnings("unchecked")
	public String getstatus(DocumentDetailModel docdetails)
	{
	   String hql="select dd.panel_status from DocumentDetailModel dd where dd.kyc_ind_id="+docdetails.getKyc_ind_id()+" and dd.doc_type like '"+docdetails.getDoc_type()+"%' and dd.cr_date=(select Max(dd.cr_date) from DocumentDetailModel dd where dd.kyc_ind_id="+docdetails.getKyc_ind_id()+" and dd.doc_type like '"+docdetails.getDoc_type()+"%')"; 			         
	   List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
	   
	   if(ls.isEmpty())
	   {
	       return "Not There";
	   }
	   else
	   {
		   return ""+ls.get(0);
	   }
	}
	
	@SuppressWarnings("unchecked")
	public String getHierarchy(DocumentDetailModel docdetails)
	{
		String hql = "select dd.doc_hierarchy from DocumentHierarchyModel dd where dd.doc_type='"+docdetails.getDoc_type()+"'";
		List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
		return ""+ls.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String getfinDocName(DocumentDetailModel docdetailmodel)
	{
		String hql = "select dd.doc_name from DocumentDetailModel dd where dd.doc_type='"+docdetailmodel.getDoc_type()+"' and dd.kyc_ind_id="+docdetailmodel.getKyc_ind_id()+" and dd.cr_date=(select Max(dd.cr_date) from DocumentDetailModel dd where dd.doc_type='"+docdetailmodel.getDoc_type()+"' and dd.kyc_ind_id="+docdetailmodel.getKyc_ind_id()+")";
		List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
		return ""+ls.get(0);
		
	}
	
	@SuppressWarnings("unchecked")
	public List<DocumentDetailModel> getfinHistory(DocumentDetailModel docdetails,HttpSession session)
	{
		int k = docdetails.getKyc_ind_id();		
		String docName=docdetails.getDoc_name();		
		List<DocumentDetailModel> lsdata = new ArrayList<DocumentDetailModel>();		
		String hql = "from DocumentDetailModel where kyc_ind_id="+k+" and doc_name like '"+docName+"%' ORDER BY cr_date DESC";		
		lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		
		return lsdata;		
	}
	
	@SuppressWarnings("unchecked")
	public List<DocumentDetailModel> searchfinancial(String doc_year,String docType,String docMode,int user)
	{
		String hql = "from DocumentDetailModel where kyc_ind_id="+user+" and doc_type='Financial_DOC' and (doc_name like '%"+doc_year+"%' or doc_name like '%"+docType+"%' or doc_name like '%"+docMode+"%')";		
		List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public List<DocumentDetailModel> backFinancial(String backValue,int user)
	{
		String hql = "from DocumentDetailModel where kyc_ind_id="+user+" and doc_type='Financial_DOC' and doc_name like '%"+backValue+"%'";		
		List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;	
	}
	
	@SuppressWarnings("unchecked")
	public String getKYCdatas(DocumentDetailModel docdetails)
	{
		String complist[] = docdetails.getDoc_des().split(",");
		int k = docdetails.getKyc_ind_id();
		StringBuilder kycdatas= new StringBuilder();
		
		for(int i=0;i<complist.length;i++)
		{			
			String hql = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.docs_data like '"+complist[i]+"%' and dd.cr_date=(select Max(dd.cr_date) from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.docs_data like '"+complist[i]+"%')";
			List<String> lsData= sessionFactory.getCurrentSession().createQuery(hql).list();			
			if(!lsData.isEmpty())
			{
				kycdatas.append(complist[i]+"$"+lsData.get(0)+"/");
			}				
		}	
		
		return kycdatas.toString();
	}
	
	
	@SuppressWarnings("unchecked")
	public String getKYCdataAsUtilizerDetails(DocumentDetailModel docdetails)
	{
		String complist[] = docdetails.getDoc_des().split(",");
		int k = docdetails.getKyc_ind_id();
		StringBuilder kycdatas= new StringBuilder();
		
		for(int i=0;i<complist.length;i++)
		{			
			String hql = "select dd.docs_data from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.docs_data like '"+complist[i]+"%' and dd.cr_date=(select Max(dd.cr_date) from DocumentDetailModel dd where dd.kyc_ind_id="+k+" and dd.docs_data like '"+complist[i]+"%')";
			List<String> lsData= sessionFactory.getCurrentSession().createQuery(hql).list();			
			if(!lsData.isEmpty())
			{
				kycdatas.append(complist[i]+"$"+lsData.get(0)+"/");
			}				
		}	
		
		return kycdatas.toString();
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<DocumentDetailModel> getEmpHistory(DocumentDetailModel docdetails)
	{
		int k = docdetails.getKyc_ind_id();		
		String docName=docdetails.getDocs_data();		
		
		String hql = "from DocumentDetailModel where kyc_ind_id="+k+" and docs_data like '"+docName+"%' and doc_type='Employment_DOC' ORDER BY cr_date DESC";		
		List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public String findLatestDocName(int id,String docname)
	{
		String hql = "select doc_name from DocumentDetailModel where kyc_ind_id = "+id+" and docs_data like '"+docname+"%' and cr_date=(select Max(cr_date) from DocumentDetailModel where kyc_ind_id = "+id+" and docs_data like '"+docname+"%')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		
		return lsdata.toString();
	}
	
	
	@SuppressWarnings("unchecked")
	public Integer findYear(String Ind_id)
	{
		String hql = "select EXTRACT(YEAR from cr_date) from IndSignupModel where ind_id = "+Ind_id+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String getAcademicStatus(int id)
	{	
		String hql = "select panel_status from DocumentDetailModel where kyc_ind_id="+id+" and doc_type='ACADEMIC_DOC' and cr_date=(select Max(cr_date) from DocumentDetailModel where kyc_ind_id = "+id+" and doc_type='ACADEMIC_DOC')";
        List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();			
		if(lsdata.size()>0)
		{
            return lsdata.get(0);
		}
		else
		{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getOldComp(int indId)
	{
		String hql = "select panel_status from DocumentDetailModel where Kyc_ind_id="+indId+" and doc_type Like 'KYC_DOCUMENT%' and cr_date=(select Max(cr_date) from DocumentDetailModel where kyc_ind_id ="+indId+" and doc_type like 'KYC_DOCUMENT%')";
	    List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
	    if(lsdata.size()>0)
	    {	
	    return lsdata.get(0);
	    }
	    else
	    {
	    	return "";
	    }
	}
	
	@SuppressWarnings("unchecked")
	public String getIndName(int id)
	{
		String hql = "select firstname from IndSignupModel where ind_id="+id+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String getId(int id)
	{
		String hql = "select kycid from IndSignupModel where ind_id="+id+"";
        List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	public String updateKycId(String latest_kyc,int id)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update IndSignupModel set kycid = :setKycid where ind_id = :setId");
		hql.setParameter("setKycid",latest_kyc);
		hql.setParameter("setId",id);
				
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "KycId Upadted";
		}
		else
		{			
			return "Unable to do Try Again";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getFinancialDoc(int id)
	{
		String hql = "select doc_name  from DocumentDetailModel where kyc_ind_id="+id+" and doc_type='Financial_DOC'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return "doc is There";
		}
		else
		{
		   return "doc is not There";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getEmpHierarchy()
	{
		String hql = "select doc_hierarchy from DocumentHierarchyModel where doc_type='EMPLOYEMENT'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String getEmpPanelStatusfromEmpDetails(int ind_id)
	{
		String hql = "select panel_status from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}
	}
	
	
	
	// Under Construction
	
	@SuppressWarnings("unchecked")
	public String getEmpPanelStatusAsofNow(int i)
	{
		String hql = "select panel_status from DocumentDetailModel where kyc_ind_id="+i+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+i+" and doc_type='Employment_DOC')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmpPanelStatus(int ind_id)
	{
		String pannel_pattern = "";
		
		String sorted_Panel = "";
		
		String temp_panel = "";
		
		//  Here we got all the panel names from the DB
		
		String hql = "select panel_status from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{				
			String PanelStringArray[]  = lsdata.get(0).split("/");
			
			for(int i=0;i<PanelStringArray.length;i++)
			{	
				List<String> latestDocData = new ArrayList<String>();
				if(i==0)
				{
					String latestDocsDataHQL = "select docs_data from  DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type like 'Employment%' and docs_data like '"+PanelStringArray[i]+"%' and cr_date =(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type like 'Employment%' and docs_data like '"+PanelStringArray[i]+"%')";						
					latestDocData = sessionFactory.getCurrentSession().createQuery(latestDocsDataHQL).list();						
				}
				else
				{
					String latestDocsDataHQL = "select docs_data from  DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type like 'Employment%' and docs_data like '"+PanelStringArray[i]+"%' and docs_data not like '%No Data%' and cr_date =(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type like 'Employment%' and docs_data like '"+PanelStringArray[i]+"%' and docs_data not like '%No Data%')";						
					latestDocData = sessionFactory.getCurrentSession().createQuery(latestDocsDataHQL).list();						
				}
				
				
				// As per the panel we will get the latest company details
				String emp_docs_data = "";
				if(latestDocData.size()>0)
				{
					 emp_docs_data = latestDocData.get(0);
				}
				
				
				// Each data split by && separator
				
				String emp_docs_data_array[] = emp_docs_data.split("&&");
				
				// set the end date data in panel_pattern string  again by && separator
				
				pannel_pattern += emp_docs_data_array[4] + "&&"; 
			}
			
			pannel_pattern = pannel_pattern.substring(0,pannel_pattern.length()-2);
			 
			// Now again split the panel_pattern string in a single array 
			
			String pannel_patternArray[] = pannel_pattern.split("&&");
			
			List<Date> listDates = new ArrayList<Date>();		
			DateFormat dateFormatter = new SimpleDateFormat("d/M/yy");
			
			
			for(int j=0;j<pannel_patternArray.length;j++)
			{
				if(!pannel_patternArray[j].equals("No Data") && !pannel_patternArray[j].equals("Presently Working"))
				{					
					try 
					{			
				       listDates.add(dateFormatter.parse(pannel_patternArray[j]));		// In a list we placed all job end date								    										    
					} 					
				    catch (ParseException ex) 
					{
						System.err.print(ex);
					}	
				}
			}
			
			if(listDates.size()>0)
			{				
				Collections.sort(listDates);	// Now it is a sorted List			 
				
				for(int d=0;d<listDates.size();d++)
				{					
					Date latestDate = listDates.get(d);						
					DateFormat df = new SimpleDateFormat("d/M/yy");				   		
					String reportDate = df.format(latestDate);
						
					System.out.println("Reports Date are="+reportDate);
					
					String temp[] = reportDate.split("/");
					String dd = "";  String MM = "";  String yy = "";
					
					String arracngedDate = "";
					
					if(temp[0].startsWith("0"))
					{
						dd += temp[0].replaceAll("0","");
					}
					
					if(!dd.equals(""))
					{
						arracngedDate +=dd+"/";
					}
					
					if(dd.equals(""))
					{
						arracngedDate +=temp[0]+"/";
					}
					
					if(temp[1].startsWith("0"))
					{
						MM += temp[1].replaceAll("0","");
					}
					
					if(!MM.equals(""))
					{
						arracngedDate +=MM+"/";
					}
					
					if(MM.equals(""))
					{
						arracngedDate +=temp[1]+"/";
					}
																									
					if(yy.equals(""))
					{
						arracngedDate +=temp[2];
					}
									
					System.out.println("All the String are"+arracngedDate);
					
					temp_panel += arracngedDate + "&&";
				}
												
			}
			
			if(temp_panel.contains("&&"))
			{
				temp_panel = temp_panel.substring(0,temp_panel.length()-2);
			}
			
			
			if(pannel_pattern.contains("Presently Working"))
			{
				if(temp_panel.equals(""))
				{
					temp_panel = "Presently Working";
				}
				else
				{
					temp_panel = temp_panel+"&&Presently Working";
				}				
			}
			
			if(pannel_pattern.contains("No Data"))
			{
				if(temp_panel.equals(""))
				{
					temp_panel = "No Data";
				}
				else
				{
					temp_panel = "No Data&&"+temp_panel;
				}					
			}
			
			System.out.println("Final Panel"+temp_panel);
			
			String temp_panelArray[] = temp_panel.split("&&");
			
			for(int p=temp_panelArray.length-1;p>=0;p--)
			{
				
				List<String> latestDocData = new ArrayList<String>();
				if(p==(temp_panelArray.length-1))
				{
					String latestDocsDataHQL = "select docs_data from  DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type like 'Employment%'  and docs_data like '%"+temp_panelArray[p]+"' and cr_date =(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type like 'Employment%'  and docs_data like '%"+temp_panelArray[p]+"')";						
					latestDocData = sessionFactory.getCurrentSession().createQuery(latestDocsDataHQL).list();													
				}
				else
				{
					String latestDocsDataHQL = "select docs_data from  DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type like 'Employment%'  and docs_data like '%"+temp_panelArray[p]+"' and cr_date =(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type like 'Employment%'  and docs_data like '%"+temp_panelArray[p]+"')";						
					latestDocData = sessionFactory.getCurrentSession().createQuery(latestDocsDataHQL).list();													
				}
				
				if(latestDocData.size()>0)
				{
					String docsData[] = latestDocData.get(0).split("&&");
					sorted_Panel +=docsData[0] + "/";
				}
								
			}
			
			if(sorted_Panel.length()>1)
			{
				sorted_Panel = sorted_Panel.substring(0,sorted_Panel.length()-1);
			}
				
			System.out.println("Final sorted Panels data="+sorted_Panel);
			
			return sorted_Panel;
		}
		else
		{
			return "";
		}
	}
	
	// Under Construction
	@SuppressWarnings("unchecked")
	public String getEmpDetails(String panel,int ind_id)
	{
		String empDetails = "";
		
		String panel_ary[] = panel.split("/");
		
		System.out.println("Same Panel Name="+panel);
		
		for(int i=0;i<panel_ary.length;i++)
		{
			
		  String hql = "select docs_data from DocumentDetailModel where docs_data like '"+panel_ary[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where docs_data like '"+panel_ary[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC')";
		  List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		  
		  if(lsdata.size()>0)
		  {
			  String hql1 = "select doc_name from DocumentDetailModel where docs_data like '"+panel_ary[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where docs_data like '"+panel_ary[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC')";
			  List<String> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
			  
			  if(lsdata1.size()>0)
			  {
				  String sql = "select month_year from (select * from salary_slip order by salary_slip_id desc) d where ind_id="+ind_id+" and company_name='"+panel_ary[i]+"' GROUP BY month_year ";
			      List<String> month_year = sessionFactory.getCurrentSession().createSQLQuery(sql).addScalar("month_year", Hibernate.STRING).list();
				  String monthNames="";
				  
				  if(month_year.size()>0)
				  {
				  for(int j=0;j<month_year.size();j++)
				  {
					 monthNames = monthNames + month_year.get(j) + ",";    
				  }
				  System.out.println("Salary slip and month "+monthNames);
				  
				  monthNames = monthNames.substring(0, monthNames.length()-1);
			      
				  }
				  empDetails = lsdata.get(0) + "@@@" + lsdata1.get(0) + "@@@" + monthNames + "#" + empDetails ;
			     
			  }
			  else
			  {
				  empDetails = lsdata.get(0) + "#" + empDetails;
			  }
		  }
		}
		
		return empDetails;
	}
	
	@SuppressWarnings("unchecked")
	public String getEmpInfo(int ind_id)
	{
		String hql = "select docs_data from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		  
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getEmpDocNames(int ind_id,String orgName)
	{
		String hql = "select doc_name from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and docs_data like '"+orgName+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and docs_data like '"+orgName+"%')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		  
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmpHistoryDetails(int doc_id)
	{
		String empDetails = "";
				
		  String hql = "select docs_data from DocumentDetailModel where doc_id="+doc_id+"";
		  List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		  
		  if(lsdata.size()>0)
		  {
			  String hql1 = "select doc_name from DocumentDetailModel where doc_id="+doc_id+"";
			  List<String> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
			  
			  if(lsdata1.size()>0)
			  {
			     empDetails = lsdata.get(0) + "@@@" + lsdata1.get(0);			     
			  }
			  else
			  {
				  empDetails = lsdata.get(0);				  
			  }
		  }
		  return empDetails;
		  
	}
	
	public String savePaySlips(SalarySlipModel salSlipModel)
	{
		String txStatus="";
		Session docSession=sessionFactory.getCurrentSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.save(salSlipModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getSalarySlipsNameAsPerUpload(SalarySlipModel salSlipModel)
	{
		String sql = "select slip_name from (select * from salary_slip  order by salary_slip_id desc) d  where ind_id="+salSlipModel.getInd_id()+" and company_name='"+salSlipModel.getCompany_name()+"' GROUP BY month_year ";
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sql).addScalar("slip_name", Hibernate.STRING).list();
				
	    return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getPaySlipsName(SalarySlipModel salSlipModel)
	{
		
		String sql = "select slip_name from (select * from salary_slip  order by salary_slip_id desc) d  where ind_id="+salSlipModel.getInd_id()+" and company_name='"+salSlipModel.getCompany_name()+"' GROUP BY month_year ";
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sql).addScalar("slip_name", Hibernate.STRING).list();
				
	    return lsdata;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getPaySlipsMonthYear(SalarySlipModel salSlipModel)
	{
		String hql = "select month_year from SalarySlipModel where ind_id="+salSlipModel.getInd_id()+" and company_name='"+salSlipModel.getCompany_name()+"' ";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
	    return lsdata;		
	}
	
	@SuppressWarnings("unchecked")
	public String getCreatedDateOfPaySlips(String payslipsName,SalarySlipModel salSlipModel)
	{
		String cr_dateString = "";
		String payslipsNameArray[] = payslipsName.split(",");
		for(int i=0;i<payslipsNameArray.length;i++)
		{
			String hql = "select cr_date from SalarySlipModel where ind_id="+salSlipModel.getInd_id()+" and slip_name='"+payslipsNameArray[i]+"'";
			List<Date> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			Date today = lsdata.get(0);
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");		        
			cr_dateString = df.format(today) + ",";
		}
		
		cr_dateString = cr_dateString.substring(0, cr_dateString.length()-1);
		
		return cr_dateString;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getFinancialData(int id)
	{				
		String sql = "select docs_data from(select * from document_details order by cr_date desc) a where a.doc_type like 'Financial%' and a.kyc_ind_id ="+id+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sql).addScalar("docs_data", Hibernate.STRING).list();
		return lsdata;
		
	}
	
	@SuppressWarnings("unchecked")
	public String getFinancialDataviaDocName(int i,String docName)
	{
		String hql = "select docs_data from DocumentDetailModel where kyc_ind_id="+i+" and doc_name LIKE '%"+docName+"%' ";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
	    return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String uploadEmpDocumentDetail(DocumentDetailModel docdetailmodel)
	{
						
		String hql = "select doc_id from DocumentDetailModel where kyc_ind_id="+docdetailmodel.getKyc_ind_id()+" and docs_data like '"+docdetailmodel.getDocs_data().split("&&")[0]+"%' and docs_data like '%No Data' ";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			docdetailmodel.setDoc_id(lsdata.get(0));
		}
		
		String txStatus="";
		Session docdetSession=sessionFactory.openSession();
		Transaction tx1=null;
		try {
		tx1=docdetSession.beginTransaction();
		docdetSession.saveOrUpdate(docdetailmodel);
		tx1.commit();
		if(tx1.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docdetSession.close();
		}
		
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String getFinancialDocName(int i,String finName)
	{
		String hql = "select doc_name from DocumentDetailModel where kyc_ind_id="+i+" and doc_name like '"+finName+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+i+" and doc_name like '"+finName+"%')";
		List<String> lsd = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsd.size()>0)
		{
			return lsd.get(0);
		}
		else
		{
			return "No Data";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<DocumentDetailModel> getFinancialHistory(int i,String docName)
	{
		String hql = "from DocumentDetailModel where kyc_ind_id="+i+" and doc_name like '"+docName+"%' ORDER BY cr_date DESC";
		List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getMonthSalarySlip(int i1,String orgName)
	{
		String hql = "select month_year from (select * from salary_slip order by salary_slip_id desc) d where ind_id="+i1+" and company_name='"+orgName+"' GROUP BY month_year ";
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(hql).addScalar("month_year", Hibernate.STRING).list();
				
	    return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getEmpDataofAllCompany(int Individual_Id)
	{
		List<String> lsdata = new ArrayList<String>();
		
		String pannelhql = "select panel_status from DocumentDetailModel where kyc_ind_id="+Individual_Id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+Individual_Id+" and doc_type='Employment_DOC')";
		List<String> panelData = sessionFactory.getCurrentSession().createQuery(pannelhql).list();
		
		String panelArray[] = panelData.get(0).split("/");
		
		for(int i=0;i<panelArray.length;i++)
		{
			String docsDatahql = "select docs_data from DocumentDetailModel where kyc_ind_id="+Individual_Id+" and doc_type='Employment_DOC' and docs_data LIKE '"+panelArray[i]+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+Individual_Id+" and doc_type='Employment_DOC' and docs_data LIKE '"+panelArray[i]+"%') ";
			List<String> docsData = sessionFactory.getCurrentSession().createQuery(docsDatahql).list();
			lsdata.add(docsData.get(0));
		}
		
		return lsdata;
	}
	
	public String academicFileUpload(AcademicFileUploadModel academicFileDetails)
	{
		String txStatus="";
		Session docdetSession=sessionFactory.openSession();
		Transaction tx1=null;
		try {
		tx1=docdetSession.beginTransaction();
		docdetSession.saveOrUpdate(academicFileDetails);
		tx1.commit();
		if(tx1.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docdetSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getAcademicUploadedDocName(int kyc_ind_id)
	{
		String hql = "SELECT file_name FROM AcademicFileUploadModel where kyc_ind_id="+kyc_ind_id+"";
		List<String> filenameList = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return filenameList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Float> getAcademicUploadedDocSize()
	{
		String hql = "SELECT file_size FROM AcademicFileUploadModel";
		List<Float> filenameList = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return filenameList;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getAcademicUploadedDocDes()
	{
		String hql = "SELECT file_description FROM AcademicFileUploadModel";
		List<String> fileDesList = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return fileDesList;
	}
	
	public String getDeleteAllFromUploadTableDetails(int kyc_ind_id)
	{
		String hql = "DELETE FROM AcademicFileUploadModel where kyc_ind_id="+kyc_ind_id+"";	             
		Transaction tx1=null;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);		
		
		int result = query.executeUpdate();		
		tx1=sessionFactory.getCurrentSession().beginTransaction();		
		tx1.commit();
		
		if(result>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public String getoldDocumentList(int ind_id,String docType)
	{
		String hql = "select doc_name from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data like '"+docType+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data like '"+docType+"%')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Data";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public String getAcademicDataById(int doc_id)
    {
		 String hql = "select dd.docs_data from DocumentDetailModel dd where dd.doc_id="+doc_id+"";
		 List<String> ls =sessionFactory.getCurrentSession().createQuery(hql).list();
			
		 return ls.get(0);			
    }
	
	@SuppressWarnings("unchecked")
	public String getAcademicDocNameById(int doc_id)
	{
		String hql = "select dd.doc_name from DocumentDetailModel dd where dd.doc_id="+doc_id+"";
		List<String> ls =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(ls.size()>0)
		{
			return ls.get(0);	
		}
		else
		{
			return "No Data";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmployementLatestData(int ind_id,String companyName)
	{
		 String hql = "select docs_data from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data LIKE '"+companyName+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and docs_data LIKE '"+companyName+"%' and doc_type='Employment_DOC')";
		 List<String> ls =sessionFactory.getCurrentSession().createQuery(hql).list();	
		 if(ls.size()>0)
		 {
			 return ls.get(0);	
		 }
		 else
		 {
			 return "No Record";	
		 }		 	
	}
		
	@SuppressWarnings("unchecked")
	public String getKYCLatestDocsData(int ind_id)
	{
		String kycHql = "select docs_data from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='KYC_DOCUMENT' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='KYC_DOCUMENT')";
		List<String> ls = sessionFactory.getCurrentSession().createQuery(kycHql).list();	
		return ls.get(0);	
	}
	
	@SuppressWarnings("unchecked")
	public String getKYCLatestDocName(int ind_id)
	{
		String kycHql = "select doc_name from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='KYC_DOCUMENT' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='KYC_DOCUMENT')";
		List<String> ls = sessionFactory.getCurrentSession().createQuery(kycHql).list();
		
		if(ls.size()>0)
		{
			return ls.get(0);
		}
		else
		{
			return "";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getAllListKYCDocsData(int ownIndId)
	{
		String kycHql = "select docs_data from DocumentDetailModel where doc_type = 'KYC_DOCUMENT' and kyc_ind_id != "+ownIndId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(kycHql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getAcademicFaultFileName(int ownIndId)
	{
		String hql = "select file_name from AcademicFileUploadModel where kyc_ind_id="+ownIndId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public String deleteAcademicFaultFileDetails(int ownIndId)
	{
		String hql = "DELETE FROM AcademicFileUploadModel where kyc_ind_id="+ownIndId+"";	             
		Transaction tx1=null;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);		
		
		int result = query.executeUpdate();		
		tx1=sessionFactory.getCurrentSession().beginTransaction();		
		tx1.commit();
		
		if(result>0)
		{
			return "success";
		}
		else
		{
			return "failure";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getEmployeeDetailsEachCompnay(String panelNames,int individualId)
	{
		String pannelArray[] = panelNames.split("/");
		String docsData = "";
		
		for(int i=0;i<pannelArray.length;i++)
		{
			String hql = "select docs_data from DocumentDetailModel where kyc_ind_id="+individualId+" and docs_data like '"+pannelArray[i]+"%' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+individualId+" and docs_data like '"+pannelArray[i]+"%' )";
			List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(!lsdata.get(0).contains("No Data"))
			{
				docsData +=  lsdata.get(0) + "##";
			}
		}
		
		docsData = docsData.substring(0,docsData.length()-2);
		
		return docsData;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String getEmpDetailsExceptThisCompany(String panel,int ind_id,String companyName)
	{
        String empDetails = "";
		
		String panel_ary[] = panel.split("/");
		
		for(int i=0;i<panel_ary.length;i++)
		{		
			if(!companyName.contains(panel_ary[i]))
			{
				String hql = "select docs_data from DocumentDetailModel where docs_data like '"+panel_ary[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where docs_data like '"+panel_ary[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC')";
				  List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				  
				  if(lsdata.size()>0)
				  {
					  String hql1 = "select doc_name from DocumentDetailModel where docs_data like '"+panel_ary[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where docs_data like '"+panel_ary[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC')";
					  List<String> lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
					  
					  if(lsdata1.size()>0)
					  {
						  String sql = "select month_year from (select * from salary_slip order by salary_slip_id desc) d where ind_id="+ind_id+" and company_name='"+panel_ary[i]+"' GROUP BY month_year ";
					      List<String> month_year = sessionFactory.getCurrentSession().createSQLQuery(sql).addScalar("month_year", Hibernate.STRING).list();
						  String monthNames="";
						  
						  if(month_year.size()>0)
						  {
						  for(int j=0;j<month_year.size();j++)
						  {
							 monthNames = monthNames + month_year.get(j) + ",";    
						  }
						  System.out.println("Salary slip and month "+monthNames);
						  
						  monthNames = monthNames.substring(0, monthNames.length()-1);
					      
						  }
						  empDetails = lsdata.get(0) + "@@@" + lsdata1.get(0) + "@@@" + monthNames + "#" + empDetails ;
					     
					  }
					  else
					  {
						  empDetails = lsdata.get(0) + "#" + empDetails;
					  }
				  }
			}
			
		  
		}
		
		return empDetails;
	}
	
	@SuppressWarnings("unchecked")
	public String checkOrgNameExistOrNot(int ind_id,String orgName)
	{
		String resultStatus = "";
		
		String hql = "select docs_data from DocumentDetailModel where docs_data like '"+orgName+"%' and kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' ";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		 
		if(lsdata.size()>0)
		{
			resultStatus = "Exist";
		}
		else
		{
			String panelHql = "select panel_status from DocumentDetailModel where kyc_ind_id="+ind_id+" and doc_type='Employment_DOC' and cr_date=(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_id+")";
			List<String> panelLsdata = sessionFactory.getCurrentSession().createQuery(panelHql).list();
			if(panelLsdata.size()>0)
			{
				String cmyName = panelLsdata.get(0);
				String cmyNameArray[] = cmyName.split("/");
				for(int i=0;i<cmyNameArray.length;i++)
				{
					if(orgName.startsWith(cmyNameArray[i]))
					{
						resultStatus = "Exist";						
						break;
					}
					else
					{
						resultStatus = "Not Exist";
					}
				}
				
			}
			else
			{
				resultStatus = "Not Exist";
			}			
		}
		
		return resultStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getPannelWithId(int ind_id,String old_selComp)
	{
		String panelArray[] = old_selComp.split(",");
		String data = "";
		for(int i=0;i<panelArray.length;i++)
		{
			String hql = "select docs_data from DocumentDetailModel where docs_data like '"+panelArray[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='KYC_DOCUMENT' and cr_date=(select MAX(cr_date) from DocumentDetailModel where docs_data like '"+panelArray[i]+"%' and kyc_ind_id="+ind_id+" and doc_type='KYC_DOCUMENT')";
			List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(lsdata.size()>0)
			{
				String docData[] = lsdata.get(0).split(",");
				data += docData[0]+"-"+docData[2]+",";
			}
		}
		
		if(data.length()>0)
		{
			data.substring(0, data.length()-1);
		}
		
		return data;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getLatestKycBackSideDoc(String ind_id,String kycDocType)
	{
		String kycDocStatus = "";		
		String hql = "select docs_data from DocumentDetailModel where docs_data like '"+kycDocType+"%' and kyc_ind_id="+ind_id+" and doc_type='KYC_DOCUMENT' and cr_date=(select MAX(cr_date) from DocumentDetailModel where docs_data like '"+kycDocType+"%' and kyc_ind_id="+ind_id+" and doc_type='KYC_DOCUMENT')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{			
			String kycDocArray[] = lsdata.get(0).split(",");
			kycDocStatus = kycDocArray[1];
		}
		else
		{
			kycDocStatus = "Not Uploaded";
		}		
		return kycDocStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getFinancialTypeFromHierarchy()
	{
		String hql ="select subdoc_type from DocumentHierarchyModel where doc_hir_name LIKE 'FINANCIAL%'";		
		List<String> ls =sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return ls.get(0); 
	}
	
	
	@SuppressWarnings("unchecked")
	public Date getHitoryDateOfThisId(int doc_id)
	{		
		String hql = "select cr_date from DocumentDetailModel where doc_id = "+doc_id+"";
		List<Date> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
}

