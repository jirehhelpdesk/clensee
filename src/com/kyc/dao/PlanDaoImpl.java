package com.kyc.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kyc.model.ClenseeInvoiceModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.FeaturesDetailsModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.PaymentDetailsModel;
import com.kyc.model.PlanDetailsModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.Summarydetail;

@Repository("PlanDao")
public class PlanDaoImpl implements PlanDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public String addFeaturesDetail(FeaturesDetailsModel featuresDetailsModel) {

		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(featuresDetailsModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
				
	}
	
	
	public String addPlanDetail(PlanDetailsModel PlanDetailsModel) {

		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(PlanDetailsModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
				
	}
	
	public String getplanname(FeaturesDetailsModel FeaturesDetailsModel) {

		String hql = "select plan_available from IndSignupModel where ind_id='"+FeaturesDetailsModel.getFeature_id()+"' ";
		@SuppressWarnings("unchecked")
		List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
		return ls.toString();
				
	}
	
	
	
	public String getplanname(PlanDetailsModel PlanDetailsModel) {

		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(PlanDetailsModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successsaveplandetail";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
				
	}
	
	
	public String featurename(){
	
	String hql = "select distinct(feature_name) from FeaturesDetailsModel";
	@SuppressWarnings("unchecked")
	List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
	return ls.toString();
	
	}
	public String getplannameview(){
		
		String hql = "select distinct(plan_name) from PlanDetailsModel";
		@SuppressWarnings("unchecked")
		List<String> ls = sessionFactory.getCurrentSession().createQuery(hql).list();
		return ls.toString();
		
		} 
	
	public List<String> featurecategory(String featureCategory)
	{		
		String hql = "select distinct(feature_name) from FeaturesDetailsModel where feature_category='"+featureCategory+"'";
		@SuppressWarnings("unchecked")
		List<String> featureName = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return featureName;		 
	}
	
		
		public List<FeaturesDetailsModel> getallfeatures(FeaturesDetailsModel FeaturesDetailsModel)
			{
				@SuppressWarnings("unchecked")
				List<FeaturesDetailsModel> lst = sessionFactory.getCurrentSession().createQuery("from FeaturesDetailsModel").list();
				return lst;				
			}
		
		
		public List<FeaturesDetailsModel> selectedviewfeature(FeaturesDetailsModel FeaturesDetailsModel)
		{
				@SuppressWarnings("unchecked")
				List<FeaturesDetailsModel> lst = sessionFactory
						.getCurrentSession()
						.createQuery(
								" from FeaturesDetailsModel where feature_name='"+FeaturesDetailsModel.getFeature_name()+"'").list();
				return lst;
			
		}
		
		
		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel> getAllPlanDetailsBytheDetails(String plan_category,String plan_domain,String plan_name)
		{
			List<String> values = new ArrayList<String>();
			
			List<FeaturesDetailsModel> lst =new ArrayList<FeaturesDetailsModel>();
			List<FeaturesDetailsModel> listdata = new ArrayList<FeaturesDetailsModel>();
			
			if(plan_category.equals("Individual"))
			{
				values = sessionFactory.getCurrentSession().createQuery("select feature_ids from PlanDetailsModel where plan_name='"+plan_name+"' and plan_domain='Default' and plan_type='"+plan_category+"'").list();				
			}
			if(plan_category.equals("Utilizer"))
			{
				values = sessionFactory.getCurrentSession().createQuery("select feature_ids from PlanDetailsModel where plan_name='"+plan_name+"' and plan_domain='"+plan_domain+"' and plan_type='"+plan_category+"'").list();				
			}
			if(plan_category.equals("Corporate"))
			{
				values = sessionFactory.getCurrentSession().createQuery("select feature_ids from PlanDetailsModel where plan_name='"+plan_name+"' and plan_domain='Default' and plan_type='"+plan_category+"'").list();				
			}
			
			if(values.size()>0)
			{					
				String lst1=values.toString();
				lst1=lst1.substring(1,lst1.length()-1);
				String[] ary=lst1.split(",");
		          						
				for(int i=0;i<ary.length;i++)
				{			        	   
	        	   String hql1 = "from FeaturesDetailsModel where feature_id='"+ary[i]+"'";		                    
	        	   listdata  = sessionFactory.getCurrentSession().createQuery(hql1).list();      	             
                   lst.addAll(listdata);
			    }				
			}	
			
			return lst;		
		}
		
		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel> selectedviewplan(FeaturesDetailsModel FeaturesDetailsModel)
		{
				List<FeaturesDetailsModel> lst =new ArrayList<FeaturesDetailsModel>();
				List<FeaturesDetailsModel> listdata = new ArrayList<FeaturesDetailsModel>();
				List<String> values = sessionFactory.getCurrentSession().createQuery("select feature_ids from PlanDetailsModel where plan_name='"+FeaturesDetailsModel.getFeature_name()+"'").list();
				
				if(values.size()>0)
				{					
					String lst1=values.toString();
					lst1=lst1.substring(1,lst1.length()-1);
					String[] ary=lst1.split(",");
			          						
					for(int i=0;i<ary.length;i++)
					{			        	   
		        	   String hql1 = "from FeaturesDetailsModel where feature_id='"+ary[i]+"'";		                    
		        	   listdata  = sessionFactory.getCurrentSession().createQuery(hql1).list();      	             
	                   lst.addAll(listdata);
				    }				
				}	
				
				return lst;					
		}
		
		@SuppressWarnings("unchecked")
		public float getPlanPricAsPerPlanCategory(String plan_category,String plan_domain,String plan_name)
		{
			float planPrice = (float)0.0;
			if(plan_category.equals("Individual"))
			{
				String hql = "select plan_price from PlanDetailsModel where plan_name='"+plan_name+"' and plan_domain='Default' and plan_type='"+plan_category+"'";
			    List<Float> planPriceData = sessionFactory.getCurrentSession().createQuery(hql).list();
			    
			    planPrice += planPriceData.get(0);
			}
			if(plan_category.equals("Utilizer"))
			{
				String hql = "select plan_price from PlanDetailsModel where plan_name='"+plan_name+"' and plan_domain='"+plan_domain+"' and plan_type='"+plan_category+"'";
			    List<Float> planPriceData = sessionFactory.getCurrentSession().createQuery(hql).list();
			    
			    planPrice += planPriceData.get(0);
			}
			if(plan_category.equals("Corporate"))
			{
				String hql = "select plan_price from PlanDetailsModel where plan_name='"+plan_name+"' and plan_domain='Default' and plan_type='"+plan_category+"'";
			    List<Float> planPriceData = sessionFactory.getCurrentSession().createQuery(hql).list();
			    
			    planPrice += planPriceData.get(0);
			}
		    
			return planPrice;
		}
		
		
		@SuppressWarnings("unchecked")
		public float getPlanPricAsPrCategory(String planCategory)
		{			
			String hql = "select plan_price from PlanDetailsModel where plan_name='"+planCategory+"'";
		    List<Float> planPrice = sessionFactory.getCurrentSession().createQuery(hql).list();
		    
		    return planPrice.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public List<PlanDetailsModel> getorganiseplandetail(PlanDetailsModel PlanDetailsModel)
		{
				
			
				@SuppressWarnings("unchecked")
				List<PlanDetailsModel> values = sessionFactory
						.getCurrentSession()
						.createQuery(
								"from PlanDetailsModel").list();
				
				System.out.println("daoimpl");
				return values;
		
			
		}
			
@SuppressWarnings("unchecked")
public String rangeselectedFeaturesDetail(FeaturesDetailsModel FeaturesDetailsModel)
{
	int	start=0;int	end=0;
	int	startprice=0;int	endprice=0;
	StringBuilder finalvalue= new StringBuilder();
	StringBuilder finalvaluepre= new StringBuilder();
	String checked=FeaturesDetailsModel.getFeature_name();
	String selectedfeature=FeaturesDetailsModel.getMod_by();
	String testing=FeaturesDetailsModel.getFeature_category();
	String testingprice=FeaturesDetailsModel.getCr_by();
	
	if(!testingprice.isEmpty() && testingprice!="" )
	{
	 startprice=Integer.parseInt(FeaturesDetailsModel.getCr_by());
	 endprice=Integer.parseInt(FeaturesDetailsModel.getFeature_description());
	}
	
	
	if(!testing.isEmpty() && testing!="" )
	{
	 start=Integer.parseInt(FeaturesDetailsModel.getFeature_category());
	 end=FeaturesDetailsModel.getFeature_id();
	System.out.println("checked$$$$$$$$$$$$$$"+checked);
	System.out.println("selectedfeature$$$$$$$$$$$$$$"+selectedfeature+"start"+start+"end"+end);}
	
	List<String> stringvalue=new ArrayList<String>();
	List<String> stringvalue1=new ArrayList<String>();
	List<String> lstcategory2=new ArrayList<String>();
	List<String> lstprice=new ArrayList<String>();
	List<FeaturesDetailsModel> listdata1 = new ArrayList<FeaturesDetailsModel>();
	@SuppressWarnings("unchecked")
	
	
	List<PlanDetailsModel> lst=sessionFactory
			.getCurrentSession()
			.createQuery(
					"from PlanDetailsModel").list();
	System.out.println("lst"+lst);
	
	for(int i=0;i<lst.size();i++)
	
	{
	stringvalue.add(lst.get(i).getPlan_name());
	System.out.println("stringvalue"+stringvalue);
	
	///////////////////////
	
	
	if(!testing.isEmpty() && testing!="" && !testingprice.isEmpty() && testingprice!="")
	{
		
		System.out.println("entering both");
		
		
	  String hql1 = "select sum(feature_value) as feature_value from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+") and feature_category='"+checked+"'";
	  List<FeaturesDetailsModel> lstcategory  = sessionFactory.getCurrentSession().createQuery(hql1).list();
	  System.out.println("lstcategory"+lstcategory);
	
	  
	  String hql2 = "select sum(feature_price) as feature_price from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")";
	   lstprice= sessionFactory.getCurrentSession().createQuery(hql2).list();
	  System.out.println("lstprice"+lstprice);
	  
	  		  
	  String pricevalues=lstprice.toString();
		 pricevalues=pricevalues.substring(1,pricevalues.length()-1);
		 System.out.println("pricevalues"+pricevalues);
		 int valuerange=Integer.parseInt(pricevalues);
		 System.out.println("valuerange"+valuerange);
	
	 if(lstcategory.get(0) != null)
	  {
		
	  System.out.println("not empty");
	  String lstcategory1=lstcategory.toString();
	  lstcategory1=lstcategory1.substring(1,lstcategory1.length()-1);
		
	 int value=Integer.parseInt(lstcategory1);
	
	 if(value>start && value<end && valuerange>startprice && valuerange<endprice){
		 
		 finalvalue.append(lst.get(i).getPlan_name()+","); 
		 String hql = "select distinct(feature_category) from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")";         
	   	   List<String> feature  = sessionFactory.getCurrentSession().createQuery(hql).list(); 
	   	 String distinctfeature=feature.toString();
	   	distinctfeature=distinctfeature.substring(1,distinctfeature.length()-1);
	   	System.out.println("distinctfeature"+distinctfeature);
			
	   	String[] ary=distinctfeature.split(", ");
		System.out.println("ary"+ary);
		
		for(int j=0;j<ary.length;j++) {
			 String hql3 = "select sum(feature_value) as feature_value from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+") and feature_category='"+ary[j]+"'";
			   lstcategory2  = sessionFactory.getCurrentSession().createQuery(hql3).list();
			  System.out.println("lstcategory2"+lstcategory2);	
			  String values=lstcategory2.toString();
				 values=values.substring(1,values.length()-1);
				 finalvalue.append(values+","); 
			
		} 
	 
		 finalvalue.append(pricevalues+","); 	 
			
				//finalvalue.append(value+","); 
				
				System.out.println("finalvalue"+finalvalue);
			 
			
			
		  }
	  
	 else{System.out.println("not tested");}	  
	  
	  
	}
	
	  else{
		  
		  System.out.println("empty");
	  }
	// return finalvalue.toString(); 
	}
	
	
	
	///////////////////////////
	
	
	
	else 
		if(!testing.isEmpty() && testing!="" )
	{
		
		System.out.println("entering feature");
	 String hql1 = "select sum(feature_value) as feature_value from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+") and feature_category='"+checked+"'";
	  List<FeaturesDetailsModel> lstcategory  = sessionFactory.getCurrentSession().createQuery(hql1).list();
	  System.out.println("lstcategory"+lstcategory);
	
	 
	  
	  if(lstcategory.get(0) != null)
	  {
		  
		 	System.out.println("not empty");
	  String lstcategory1=lstcategory.toString();
	  lstcategory1=lstcategory1.substring(1,lstcategory1.length()-1);
		
	 int value=Integer.parseInt(lstcategory1);
	
	 if(value>start && value<end){
		 finalvalue.append(lst.get(i).getPlan_name()+","); 
		 System.out.println("testing");
		
		 String hql = "select distinct(feature_category) from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")";         
   	   List<String> feature  = sessionFactory.getCurrentSession().createQuery(hql).list(); 
   	 String distinctfeature=feature.toString();
   	distinctfeature=distinctfeature.substring(1,distinctfeature.length()-1);
   	System.out.println("distinctfeature"+distinctfeature);
		
   	String[] ary=distinctfeature.split(", ");
	System.out.println("ary"+ary);
	
	for(int j=0;j<ary.length;j++) {
		 String hql2 = "select sum(feature_value) as feature_value from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+") and feature_category='"+ary[j]+"'";
		   lstcategory2  = sessionFactory.getCurrentSession().createQuery(hql2).list();
		  System.out.println("lstcategory2"+lstcategory2);	
		  String values=lstcategory2.toString();
			 values=values.substring(1,values.length()-1);
			
			 finalvalue.append(values+","); 
			
	}
		
	
	
		 String hql2 = "select sum(feature_price) as feature_price from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")";
		   lstcategory2  = sessionFactory.getCurrentSession().createQuery(hql2).list();
		  System.out.println("lstcategory2"+lstcategory2);	
		  String values=lstcategory2.toString();
			 values=values.substring(1,values.length()-1);
			
			 finalvalue.append(values+","); 
	
	
		//finalvalue.append(value+","); 
		
		System.out.println("finalvalue"+finalvalue);
		System.out.println("finalvaluepre"+finalvaluepre);
	  }
	  
	 else{System.out.println("not tested");}	  
	  
	  
	}
	
	  else{
		  
		  System.out.println("empty");
	  }
	  
	}
	
	////////////////////
	
	
	else if(!testingprice.isEmpty() && testingprice!=""  )
	{
		
		System.out.println("testingprice");
	 String hql1 = "select sum(feature_price) as feature_price from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")";
	  List<FeaturesDetailsModel> lstcategory  = sessionFactory.getCurrentSession().createQuery(hql1).list();
	  System.out.println("lstcategory"+lstcategory);
	  
	  if(lstcategory.get(0) != null)
	  {
		  
		 	System.out.println("not empty");
	  String lstcategory1=lstcategory.toString();
	  lstcategory1=lstcategory1.substring(1,lstcategory1.length()-1);
		
	 int value=Integer.parseInt(lstcategory1);
	
	 if(value>startprice && value<endprice){
		 finalvalue.append(lst.get(i).getPlan_name()+","); 
		 System.out.println("testing");
		  String hql = "select distinct(feature_category) from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")";         
   	   List<String> feature  = sessionFactory.getCurrentSession().createQuery(hql).list(); 
   	 String distinctfeature=feature.toString();
   	distinctfeature=distinctfeature.substring(1,distinctfeature.length()-1);
   	System.out.println("distinctfeature"+distinctfeature);
		
   	String[] ary=distinctfeature.split(", ");
	System.out.println("ary"+ary);
	
	for(int j=0;j<ary.length;j++) {
		 String hql2 = "select sum(feature_value) as feature_value from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")  and feature_category='"+ary[j]+"'";
		   lstcategory2  = sessionFactory.getCurrentSession().createQuery(hql2).list();
		  System.out.println("lstcategory2"+lstcategory2);	
		  String values=lstcategory2.toString();
			 values=values.substring(1,values.length()-1);
			
			 finalvalue.append(values+","); 
			
	}
		 
	 
	
		//finalvalue.append(value+","); 
	 finalvalue.append(value+",");
		System.out.println("finalvalue"+finalvalue);
		System.out.println("finalvaluepre"+finalvaluepre);
	  }
	  
	 else{System.out.println("not tested");}	  
	  
	  
	}
	
	  else{
		  
		  System.out.println("empty");
	  }
	  
	}

	
	////////////////
	else{
		  System.out.println("outer empty");
		
	
		  finalvalue.append(lst.get(i).getPlan_name()+",");
	
	  String hql = "select distinct(feature_category) from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")";         
  	   List<String> feature  = sessionFactory.getCurrentSession().createQuery(hql).list(); 
  	 String distinctfeature=feature.toString();
  	distinctfeature=distinctfeature.substring(1,distinctfeature.length()-1);
  	System.out.println("distinctfeature"+distinctfeature);
		
  	String[] ary=distinctfeature.split(", ");
	System.out.println("ary"+ary);
	
	for(int j=0;j<ary.length;j++) {
		
		 String hql2 = "select sum(feature_value) as feature_value from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+") and feature_category='"+ary[j]+"'";
		   lstcategory2  = sessionFactory.getCurrentSession().createQuery(hql2).list();
		  System.out.println("lstcategory2"+lstcategory2);	
		  String values=lstcategory2.toString();
			 values=values.substring(1,values.length()-1);
			
			 
			 finalvalue.append(values+","); 
			
	}
		 
	 
	
		//finalvalue.append(value+","); 
	String hql2 = "select sum(feature_price) as feature_price from FeaturesDetailsModel where feature_id in ("+lst.get(i).getFeature_ids()+")";
	   lstcategory2  = sessionFactory.getCurrentSession().createQuery(hql2).list();
	  System.out.println("lstcategory2"+lstcategory2);	
	  String values=lstcategory2.toString();
		 values=values.substring(1,values.length()-1);
		
		 finalvalue.append(values+","); 
		System.out.println("finalvalue"+finalvalue);
		System.out.println("finalvaluepre"+finalvaluepre);
	
	
	}
	
	
	}
	
	
	
	

	
	return finalvalue.toString();
	
}





	
	
//////////////////
@SuppressWarnings("unchecked")
public List<String> getplanhasfeature(PlanDetailsModel planDetailsModel)
{
	List<String> lst = sessionFactory
			.getCurrentSession()
			.createQuery(
					"select plan_name from PlanDetailsModel where feature_ids like '%"+planDetailsModel.getFeature_ids()+"%'").list();
	System.out.println("daoimpl lst########"+lst);
	return lst;

}

public List<FeaturesDetailsModel> getplanhasfeatureorganise(PlanDetailsModel planDetailsModel)
{
	@SuppressWarnings("unchecked")
	List<String> lstfinal=new ArrayList<String>();
	List<FeaturesDetailsModel> listdata = new ArrayList<FeaturesDetailsModel>();
	List<FeaturesDetailsModel> listdata1 = new ArrayList<FeaturesDetailsModel>();
	@SuppressWarnings("unchecked")
	List<String> lst = sessionFactory
			.getCurrentSession()
			.createQuery(
					"select feature_ids from PlanDetailsModel where plan_id='"+planDetailsModel.getPlan_id()+"'").list();
	System.out.println("daoimpl lst########"+lst);
	
	if(lst.size()>0)
	{
		String lst1=lst.toString();
		lst1=lst1.substring(1,lst1.length()-1);
		String[] ary=lst1.split(",");
          
			
		for(int i=0;i<ary.length;i++) {
        	   System.out.println("ary"+ary[i]);
        	
        	   
        	   String hql1 = "from FeaturesDetailsModel where feature_id='"+ary[i]+"'";	
            
        	   listdata  = sessionFactory.getCurrentSession().createQuery(hql1).list();      	   
  System.out.println("listdata"+listdata);
  listdata1.addAll(listdata);
           }
	}
	return listdata1;

}

		@SuppressWarnings("unused")
		public String  deleteplanhasfeature(FeaturesDetailsModel FeaturesDetailsModel)
		{
			
			String hql = "delete from FeaturesDetailsModel where feature_id="+FeaturesDetailsModel.getFeature_id()+" ";	
			int ls = sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
			
			/*String hql1 = " UPDATE PlanDetailsModel SET feature_ids=replace(feature_ids,"+FeaturesDetailsModel.getFeature_id()+",','') ";	
			int ls1 = sessionFactory.getCurrentSession().createQuery(hql1).executeUpdate();
			
			String hql2 = " UPDATE PlanDetailsModel SET feature_ids=replace(feature_ids,"+FeaturesDetailsModel.getFeature_id()+",'') ";	
			int ls2 = sessionFactory.getCurrentSession().createQuery(hql2).executeUpdate();
			
			String hql3 = " UPDATE PlanDetailsModel SET feature_ids=replace(feature_ids,"+FeaturesDetailsModel.getFeature_id()+",'') ";	
			int ls3 = sessionFactory.getCurrentSession().createQuery(hql3).executeUpdate();*/
			
			return "success";
			
		}	
	
		@SuppressWarnings("unused")
		public String deletePlanDetails(String planType,String PlanName)
		{
			String hql = "delete from PlanDetailsModel where plan_type='"+planType+"' and plan_name='"+PlanName+"'";	
			int ls = sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
				
			return "success";	
		}		

		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel>  getcurrentplan(FeaturesDetailsModel FeaturesDetailsModel)
		{
		
			String hql = "select feature_ids from PlanDetailsModel where plan_domain='Default' and plan_name=(select plan_available from IndSignupModel where ind_id='"+FeaturesDetailsModel.getFeature_id()+"')";			
			List<String> lst = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			List<FeaturesDetailsModel> listdata1 = new ArrayList<FeaturesDetailsModel>();
			List<FeaturesDetailsModel> listdata = new ArrayList<FeaturesDetailsModel>();
			
			if(lst.size()>0)
			{
				String lst1=lst.toString();
				lst1=lst1.substring(1,lst1.length()-1);
				String[] ary=lst1.split(",");
		          					
			      for(int i=0;i<ary.length;i++) {
		        	   	        	   
		        	   String hql1 = "from FeaturesDetailsModel where feature_id='"+ary[i]+"'";	                   
		        	   listdata  = sessionFactory.getCurrentSession().createQuery(hql1).list();      	                         
                       listdata1.addAll(listdata);
		           }
				
			return  listdata1;
					
			}
			else{
				
				return  listdata1;
			     }
			}


		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel> getUtilizerCurrentplan(FeaturesDetailsModel FeaturesDetailsModels)
		{
			
			String planDomain = "select uti_plan_domain from UtiliserProfileModel where uti_ind_id="+FeaturesDetailsModels.getFeature_id()+"";
			List<String> planDomainName = sessionFactory.getCurrentSession().createQuery(planDomain).list();
			
			String hql = "select feature_ids from PlanDetailsModel where plan_domain='"+planDomainName.get(0)+"' and plan_name=(select utiliser_plan from UtiliserProfileModel where uti_ind_id='"+FeaturesDetailsModels.getFeature_id()+"')";				
			List<String> lst = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			List<FeaturesDetailsModel> listdata1 = new ArrayList<FeaturesDetailsModel>();
			List<FeaturesDetailsModel> listdata = new ArrayList<FeaturesDetailsModel>();
			
			if(lst.size()>0)
			{
				String lst1=lst.toString();
				lst1=lst1.substring(1,lst1.length()-1);
				String[] ary=lst1.split(",");
		          					
			      for(int i=0;i<ary.length;i++) {
		        	   	        	   
		        	   String hql1 = "from FeaturesDetailsModel where feature_id='"+ary[i]+"'";	                   
		        	   listdata  = sessionFactory.getCurrentSession().createQuery(hql1).list();      	                         
                       listdata1.addAll(listdata);
		            }
				
			        return  listdata1;					
			}
			else{			
				    return  listdata1;
			    }
		}
		
		@SuppressWarnings("unchecked")
		public String getUtilizerplanname(int uti_Ind_id)
		{
			String planHQL = "select utiliser_plan from UtiliserProfileModel where uti_ind_id="+uti_Ind_id+"";
			List<String> planName = sessionFactory.getCurrentSession().createQuery(planHQL).list();			
			return planName.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getUtilizerPlanDomain(int uti_Ind_id)
		{
			String planDomain = "select uti_plan_domain from UtiliserProfileModel where uti_ind_id="+uti_Ind_id+"";
			List<String> planDomainName = sessionFactory.getCurrentSession().createQuery(planDomain).list();
			return planDomainName.get(0);
		}
		
		@Override
		public String plannameview() {
			// TODO Auto-generated method stub
			return null;
		}


		@Override
		public String changeplan(int ind_id,String requestedPlanName) {
			
			org.hibernate.Query sub_hql = sessionFactory.getCurrentSession().createQuery("update IndividualPlanDetailModel set plan_name = :setplanName, cr_date = :setDate where ind_id = :ind_id");
			sub_hql.setParameter("setplanName", requestedPlanName);
			sub_hql.setParameter("setDate", new Date());
			sub_hql.setParameter("ind_id", ind_id);
		    int result  = sub_hql.executeUpdate();
		  
					if(result>0)
					{
						String hql1 = " UPDATE IndSignupModel SET plan_available='"+requestedPlanName+"' where ind_id="+ind_id+"";
						int status = sessionFactory.getCurrentSession().createQuery(hql1).executeUpdate();
						
						if(status>0)
						{
							return "success";
						}
						else
						{
							return "failure";
						}
					}
					else
					{
						return "failure";
					}				
							
		}

		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel> getFeatureDetailsByCategory(String featureCategory,String featureName)
		{
			String hql = "from FeaturesDetailsModel where feature_category='"+featureCategory+"' and feature_name='"+featureName+"'";
			List<FeaturesDetailsModel> lstcategory  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return lstcategory;
		}
		
		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel> getFeaturesThroughCategory(String featureCategory)
		{
			String hql = "from FeaturesDetailsModel where feature_category='"+featureCategory+"' ";
			List<FeaturesDetailsModel> lstcategory  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return lstcategory;
		}
				
		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel> getallfeaturesbyCategoryAndName(String featureCategory,String featureName)
		{
			String hql = "from FeaturesDetailsModel where feature_category='"+featureCategory+"' and feature_name='"+featureName+"'";
			List<FeaturesDetailsModel> lstcategory  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return lstcategory;
		}
		
		@SuppressWarnings("unchecked")
		public List<String> getPlanNameByCategory(String planCategory)
		{
			
			String hql = "select plan_name from PlanDetailsModel where plan_type='"+planCategory+"'";
			List<String> planNameList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planNameList;
		}
		
		@SuppressWarnings("unchecked")
		public List<String> getPlanNameforUtilizer(String planDomain)
		{
			String hql = "select plan_name from PlanDetailsModel where plan_type='Utilizer' and plan_domain='"+planDomain+"'";
			List<String> planNameList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planNameList;
		}
		
		
		@SuppressWarnings("unchecked")
		public String getPlanFeatureIdDetails(String plan_type,String plan_name)
		{
			String hql = "select feature_ids from PlanDetailsModel where plan_type='"+plan_name+"' and plan_name='"+plan_type+"'";
			List<String> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planFeatureList.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getFeaturesForUtilizer(String planName,String planType,String planDomain)
		{
			String hql = "select feature_ids from PlanDetailsModel where plan_type='"+planType+"' and plan_name='"+planName+"' and plan_domain='"+planDomain+"'";
			List<String> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planFeatureList.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public int getPlanIdByNameAndType(String plan_name,String plan_type,String plan_domain)
		{
			String hql = "select plan_id from PlanDetailsModel where plan_type='"+plan_type+"' and plan_name='"+plan_name+"' and plan_domain='"+plan_domain+"'";
			List<Integer> planId  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planId.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getPlanFeatureId(String featureCategory,String planName)
		{
			String hql = "select feature_ids from PlanDetailsModel where plan_type='"+featureCategory+"' and plan_name='"+planName+"'";
			List<String> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planFeatureList.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getPlanDetails(String basicFeatures_Id,String featureNames)
		{
			String searchResult = "";
			
			String featureId[] =  basicFeatures_Id.split(",");
			String featureNameArray[] = featureNames.split(",");
			
			for(int i=0;i<featureId.length;i++)
			{		
				String hql = "select feature_name from FeaturesDetailsModel where feature_id="+featureId[i]+"";
				List<String> featureName  = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				String featurehql = "select feature_value from FeaturesDetailsModel where feature_id="+featureId[i]+"";
				List<String> featureValue  = sessionFactory.getCurrentSession().createQuery(featurehql).list();			
				searchResult += featureName.get(0) + "##" + featureValue.get(0) + ",";					
			}			
			
			searchResult = searchResult.substring(0, searchResult.length()-1);
			
			return searchResult;
		}
		
		@SuppressWarnings("unchecked")
		public float  getPlanPrice(String featureCategory,String planName)
		{
			String hql = "select plan_price from PlanDetailsModel where plan_type='"+featureCategory+"' and plan_name='"+planName+"'";
			List<Float> planPrice  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			return planPrice.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public float getPlanPriceforUtilizer(String featureCategory,String planDomain,String planName)
		{
			String hql = "select plan_price from PlanDetailsModel where plan_type='"+featureCategory+"' and plan_domain='"+planDomain+"' and plan_name='"+planName+"'";
			List<Float> planPrice  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			return planPrice.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getKycId(int ind_id)
		{
			String hql = "Select kycid from IndSignupModel where ind_id="+ind_id+"";
			List<String> kyc_id  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return kyc_id.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public int getCountGeneratedCode(String kyc_id)
		{
			String hql = "select COUNT(code_id) from CodesDetailsModel where code_sender='"+kyc_id+"' and MONTH(cr_date)=MONTH(sysdate())";
			List<Long> countCode  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(countCode.size()>0)
			{				
				 long count = countCode.get(0);
				 int countValue = (int)count;
				 
				 String kyc_idhql = "Select ind_id from IndSignupModel where kycid='"+kyc_id+"'";
				 List<Integer> indId  = sessionFactory.getCurrentSession().createQuery(kyc_idhql).list();
				 if(indId.size()>0)
				 {
					 String countHql = "select COUNT(non_kyc_user_code_id) from NonKycUserCodeDetailsModel where sender_ind_id="+indId.get(0)+" and MONTH(send_date)=MONTH(sysdate())";
					 List<Long> countCodeForNonKycUser  = sessionFactory.getCurrentSession().createQuery(countHql).list();
					 if(countCodeForNonKycUser.size()>0)
					 {
						 long countMore = countCodeForNonKycUser.get(0);
						 countValue += (int)countMore;
					 }
				 }
				 return countValue;				
			}
			else
			{
				return 0;
			}			
		}
		
		@SuppressWarnings("unchecked")
		public int getCountApplyCode(String kyc_id)
		{
			String hql = "select COUNT(*) from CodesDetailsModel where code_receiver='"+kyc_id+"' and MONTH(cr_date)=MONTH(sysdate()) and apply_code_count='1'";
			List<Long> countCode  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(countCode.size()>0)
			{
				 long count = countCode.get(0);
				 int countValue = (int)count;
				 return countValue;		
			}
			else
			{
				return 0;
			}				
		}
		
		@SuppressWarnings("unchecked")
		public int getCountAllowAccess(String kyc_id)
		{
			String hql = "select COUNT(*) from AccessDetailsModel where access_giver='"+kyc_id+"' and MONTH(access_given_date)=MONTH(sysdate())";
			List<Long> countCode  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(countCode.size()>0)
			{
				 long count = countCode.get(0);
				 int countValue = (int)count;
				 return countValue;		
			}
			else
			{
				return 0;
			}	
		}
		
		public int getCountRevokeAccess(String kyc_id)
		{
			String hql = "select COUNT(*) from AccessDetailsModel where access_giver='"+kyc_id+"' and MONTH(access_given_date)=MONTH(sysdate()) and uti_status='0'";
			@SuppressWarnings("unchecked")
			List<Long> countCode  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(countCode.size()>0)
			{
				 long count = countCode.get(0);
				 int countValue = (int)count;
				 return countValue;		
			}
			else
			{
				return 0;
			}	
		}
		
		@SuppressWarnings("unchecked")
		public float getTotalFileStorage(int ind_id)
		{
			String hql = "select SUM(doc_size) from DocumentDetailModel where kyc_ind_id="+ind_id+"";			
			List<Double> totalSize  = sessionFactory.getCurrentSession().createQuery(hql).list();			
			if(totalSize.size()>0)
			{
				double size = totalSize.get(0);
				float totalStorage = (float)size;				
				return totalStorage;
			}
			else
			{
				return 0;
			}			
		}
		
		@SuppressWarnings("unchecked")
		public String getCurrentPlanName(int ind_id)
		{
			String hql = "Select plan_available from IndSignupModel where ind_id="+ind_id+"";
			List<String> plan_available  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return plan_available.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getFeatureNameById(int feature_id)
		{
			String hql = "Select feature_name from FeaturesDetailsModel where feature_id="+feature_id+"";
			List<String> featureName  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return featureName.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getFeatureValue(int feature_id)
		{
			String hql = "Select feature_value from FeaturesDetailsModel where feature_id="+feature_id+"";
			List<String> featureName  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return featureName.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public int noOfFullProfileAsOfNow(String kyc_id)
		{
			String hql = "select COUNT(idprofileupdate) from Profileupdatemodel where viewer_kyc_id='"+kyc_id+"' and MONTH(profileupdatetime)=MONTH(sysdate()) and type_of_alert='visitor update'";
			List<Long> count  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(count.size()>0)
			{
				 long realcount = count.get(0);
				 int countValue = (int)realcount;
				 return countValue;		
			}
			else
			{
				return 0;
			}	
		}
		
		@SuppressWarnings("unchecked")
		public String getUtilizerPlanFeatureId(String featureCategory,String planName,String planDomain)
		{
			String hql = "select feature_ids from PlanDetailsModel where plan_type='"+featureCategory+"' and plan_name='"+planName+"' and plan_domain='"+planDomain+"'";
			List<String> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planFeatureList.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getUtilizerUniqueId(int uti_Ind_id)
		{
			String hql = "select kyc_uti_id from UtiliserProfileModel where uti_Ind_id='"+uti_Ind_id+"'";
			List<String> utilizerUniqueId  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return utilizerUniqueId.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getUtilizerFeatureIdsAsPerPlan(String utilizerCategory,String planDomain,String planName)
		{
			String hql = "select feature_ids from PlanDetailsModel where plan_type='"+utilizerCategory+"' and plan_name='"+planName+"' and plan_domain='"+planDomain+"'";
			List<String> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planFeatureList.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public int getCountAllowAccessByUtilizer(String utiUniqueId)
		{
			String hql = "select COUNT(*) from AccessDetailsModel where access_taker='"+utiUniqueId+"' and MONTH(access_taken_date)=MONTH(sysdate())";
			List<Long> countCode  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(countCode.size()>0)
			{
				 long count = countCode.get(0);
				 int countValue = (int)count;
				 return countValue;		
			}
			else
			{
				return 0;
			}	
		}
		
		public String migrateUtilizerPlan(String requestedPlanName,int uti_Ind_id)
		{
			org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UtiliserProfileModel set utiliser_plan = :setrequestedPlanName  where uti_ind_id = :setuti_Ind_id");
			hql.setParameter("setuti_Ind_id", uti_Ind_id);
			hql.setParameter("setrequestedPlanName", requestedPlanName);				
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "success";
			}
			else
			{			
				return "failure";
			}
		}
		
		@SuppressWarnings("unchecked")
		public int findUtiid(String uti_kycid)
		{
			String hql = "Select uti_ind_id from UtiliserProfileModel where kyc_uti_id='"+uti_kycid+"'";
			List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();			
			int id = lsdata.get(0);
			return id;
		}
		
		@SuppressWarnings("unchecked")
		public String getCurrentPlanNameOfUtilizer(int uti_indid)
		{
			String planhql ="select utiliser_plan from UtiliserProfileModel where uti_ind_id="+uti_indid+"";
			List<String> plan_Name  = sessionFactory.getCurrentSession().createQuery(planhql).list();
			
			String domainHql ="select uti_plan_domain from UtiliserProfileModel where uti_ind_id="+uti_indid+"";
			List<String> domain_Name  = sessionFactory.getCurrentSession().createQuery(domainHql).list();
			
			String domainWithPlan = plan_Name.get(0)+","+domain_Name.get(0);
			
			return domainWithPlan;
		}
		
		
		@SuppressWarnings("unchecked")
		public String getPlanFeatureIdDetailsForUtilizer(String planName,String domainName,String category)
		{
			String hql = "select feature_ids from PlanDetailsModel where plan_type='"+category+"' and plan_domain='"+domainName+"' and plan_name='"+planName+"'";
			List<String> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planFeatureList.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public int noOfFullProfileAsOfNowOfUtilizer(int uti_indid)
		{
			String hql = "select COUNT(activity_id) from UtilizerActivityModel where uti_ind_id="+uti_indid+" and MONTH(activity_date)=MONTH(sysdate()) and activity_type='Profile Viewing'";
			List<Long> count  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(count.size()>0)
			{
				 long realcount = count.get(0);
				 int countValue = (int)realcount;
				 return countValue;		
			}
			else
			{
				return 0;
			}	
		}
		
		@SuppressWarnings("unchecked")
		public int noOfSMSAsOfNowAsOfNowOfUtilizer(int uti_Indid)
		{
			String hql = "select COUNT(sms_unique_id) from SmsSentStatusModel where sms_belongs_to='Utilizer' and unique_id="+uti_Indid+" and MONTH(sent_date)=MONTH(sysdate())";
			List<Long> count  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(count.size()>0)
			{
				 long realcount = count.get(0);
				 int countValue = (int)realcount;
				 return countValue;		
			}
			else
			{
				return 0;
			}
		}
		
		@SuppressWarnings("unchecked")
		public float getPlanPriceAsPerRequest(String requestedPlanName)
		{
			String hql = "select plan_price from PlanDetailsModel where plan_type='Individual' and plan_name='"+requestedPlanName+"'";
			List<Float> planPrice  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			return planPrice.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getCurrentMobileNo(int ind_id)
		{
			String hql = "select mobile_no from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
			List<Long> mobileNo  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			return ""+mobileNo.get(0);
		}
		 
		@SuppressWarnings("unchecked")
		public String getCurrentEmailId(int ind_id)
		{
			String hql = "select email_id from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
			List<String> emailId  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			return emailId.get(0);
		}
		
		@SuppressWarnings("unchecked")
		public String getFullName(int ind_id)
		{
			String hql = "from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
			List<RegHistoryModel> nameList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			String fullName = nameList.get(0).getFirst_name()+" "+nameList.get(0).getLast_name();
					
			return fullName;
		}
		
		@SuppressWarnings("unchecked")
		public String getCheckPlanExistOrNot4Ind(String planName,String planType)
		{
			String hql = "from PlanDetailsModel where plan_type='"+planType+"' and plan_domain='Default' and plan_name='"+planName+"'";
			List<PlanDetailsModel> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(planFeatureList.size()>0)
			{
				return "Yes";
			}
			else
			{
				return "No";
			}
			
		}
		
		@SuppressWarnings("unchecked")
		public String getCheckPlanExistOrNot4Uti(String planName,String planDomain,String planType)
		{
			String hql = "from PlanDetailsModel where plan_type='"+planType+"' and plan_domain='"+planDomain+"' and plan_name='"+planName+"'";
			List<PlanDetailsModel> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(planFeatureList.size()>0)
			{
				return "Yes";
			}
			else
			{
				return "No";
			}
		}
		
		@SuppressWarnings("unchecked")
		public String getCheckPlanExistOrNot4Cor(String planName,String planType)
		{
			String hql = "from PlanDetailsModel where plan_type='"+planType+"' and plan_domain='Default' and plan_name='"+planName+"'";
			List<PlanDetailsModel> planFeatureList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(planFeatureList.size()>0)
			{
				return "Yes";
			}
			else
			{
				return "No";
			}
		}
		
		@SuppressWarnings("unchecked")
		public List<String> getPlanNameForUti(String planCategory,String planDomain)
		{
			String hql = "select plan_name from PlanDetailsModel where plan_type='"+planCategory+"' and plan_domain='"+planDomain+"'";
			List<String> planNameList  = sessionFactory.getCurrentSession().createQuery(hql).list();
			return planNameList;
		}
		
		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel> getAllFeatureDetailsByPlanType(String planType)
		{
			 String featureHql = "from FeaturesDetailsModel where feature_category='"+planType+"'";		                    
			 List<FeaturesDetailsModel> listdata  = sessionFactory.getCurrentSession().createQuery(featureHql).list(); 
			 
			 return listdata;
		}
		
		@SuppressWarnings("unchecked")
		public List<FeaturesDetailsModel> getAllfeatureDetailsByPlanDetails(String planName,String planType,String planDomain)
		{
			String featureHql = "from FeaturesDetailsModel where feature_category='"+planType+"'";		                    
			 List<FeaturesDetailsModel> listdata  = sessionFactory.getCurrentSession().createQuery(featureHql).list(); 
			 
			 return listdata;
		}
		
		
		@SuppressWarnings("unchecked")
		public int noOfSMSAsOfNow(int ind_id)
		{
			String hql = "select COUNT(sms_unique_id) from SmsSentStatusModel where sms_belongs_to='Individual' and unique_id="+ind_id+" and MONTH(sent_date)=MONTH(sysdate())";
			List<Long> count  = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(count.size()>0)
			{
				 long realcount = count.get(0);
				 int countValue = (int)realcount;
				 return countValue;		
			}
			else
			{
				return 0;
			}
		}
		
		
		@SuppressWarnings("unchecked")
		public Date getUtiActivatedPlanDate(int uti_Ind_id)
		{
			 String dateHql = "select cr_date from UtilizerPlanDetailsModel where uti_ind_id='"+uti_Ind_id+"'";		                    
			 List<Date> listdata  = sessionFactory.getCurrentSession().createQuery(dateHql).list(); 
			 
			 return listdata.get(0);
		}
		
		
		public String savePaymentRecords(PaymentDetailsModel payu)		
		{
			String txStatus="";
			Session docSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=docSession.beginTransaction();
			docSession.saveOrUpdate(payu);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				docSession.close();
			}
			return txStatus;
		}
		
		
		@SuppressWarnings("unchecked")
		public int getUniquePaymentId(PaymentDetailsModel payu)
		{
			 String payment_idHql = "select payment_id from PaymentDetailsModel where payment_from='"+payu.getPayment_from()+"' and payment_done_by='"+payu.getPayment_done_by()+"' and payment_date=(select MAX(payment_date) from PaymentDetailsModel where payment_from='"+payu.getPayment_from()+"' and payment_done_by='"+payu.getPayment_done_by()+"')";		                    
			 List<Integer> paymentId  = sessionFactory.getCurrentSession().createQuery(payment_idHql).list(); 
			 
			 return paymentId.get(0);
		}
		
		
		public String updatePaymentStatus(int paymentId,String paymentAmount,String paymentStatus,String paymentTranId)
		{
			org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update PaymentDetailsModel set payment_transaction_id = :setPaymentTranId , payment_status = :setPaymentStatus ,payment_amount = :setPaymentAmount where payment_id = :setPaymentId");
			hql.setParameter("setPaymentId", paymentId);
			hql.setParameter("setPaymentAmount", paymentAmount);	
			hql.setParameter("setPaymentStatus", paymentStatus);
			hql.setParameter("setPaymentTranId", paymentTranId);
			
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "success";
			}
			else
			{			
				return "failure";
			}
		}
		
		
		@SuppressWarnings("unchecked")
		public String getFeatureIdByFeatureNames(String featureCategory,String featurecategory)
		{
			String featureArray[] = featurecategory.split(",");
			String featureId = "";
			for(int i=0;i<featureArray.length;i++)
			{
				String featureHql = "select feature_id from FeaturesDetailsModel where feature_category='"+featureCategory+"' and feature_name='"+featureArray[i]+"'";		                    
				List<Integer> listdata  = sessionFactory.getCurrentSession().createQuery(featureHql).list(); 
				if(listdata.size()>0)
				{
					for(int j=0;j<listdata.size();j++)
					{
						featureId += listdata.get(j) + ",";
					}
				}			
			}
			if(featureId.length()>0)
			{
				featureId = featureId.substring(0, featureId.length()-1);
			}
			return featureId;
		}
		
		
		
		
		public String saveInvoiceDetails(ClenseeInvoiceModel invoiceData)
		{
			String txStatus="";
			Session docSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=docSession.beginTransaction();
			docSession.saveOrUpdate(invoiceData);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				docSession.close();
			}
			return txStatus;
		}
		
	
		@SuppressWarnings("unchecked")
		public int getLastInvoiceNumber()
		{
			String hql = "select invoice_serial_no from ClenseeInvoiceModel where invoice_generated_date=(select MAX(invoice_generated_date) from ClenseeInvoiceModel)";
			List<Integer> lastData = sessionFactory.getCurrentSession().createQuery(hql).list();
			if(lastData.size()>0)
			{
				return lastData.get(0);
			}
			else
			{
				return 1;
			}
		}
		
}
