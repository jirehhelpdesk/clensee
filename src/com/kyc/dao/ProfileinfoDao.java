package com.kyc.dao;

import java.util.Date;
import java.util.List;

import com.kyc.model.AccessDetailsModel;
import com.kyc.model.BasicDetail;
import com.kyc.model.CodesDetailsModel;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.IndSignupModel;
import com.kyc.model.IndividualPlanDetailModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.Matrial;
import com.kyc.model.ProfilePictureModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.ResetPasswordModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.Socialacc;
import com.kyc.model.Summarydetail;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPasswordRequestModel;

public interface ProfileinfoDao
{
	public String addFamilyDetail(FamilyDetail FamilyDetail);
	public String profileupdate(Profileupdatemodel Profileupdatemodel);
	public String addBasicDetail(BasicDetail BasicDetail);
	public String visibilityBasicDetail(BasicDetail BasicDetail);
	public String getkycid(int ind_kyc_id);
	public String getfirstname(String ind_kyc_id);
	public String addMatrial(Matrial matrial);
	public String addSocialacc(Socialacc socailacc);
	public String addSummarydetail(Summarydetail summarydetail);
	public  FamilyDetail getfamilyvalue(int fam_details_id);
	public  Summarydetail getSummaryvalue(String ind_kyc_id);
	public  Matrial getMatrimonyvalue(int matrimony_details_id);
	public   BasicDetail getBasicDetailvalue(int BasicDetailid);
	public  Socialacc getsocialdetails(String ind_kyc_id);
	public  IndSignupModel getregistrationdetails(String ind_kyc_id);
	public  String Getregid(Summarydetail summary);
	public void setregid(String ind_kyc_id);
	public Summarydetail getSummaryvalue(int sum_det_id);
	public  String Getsocialaccid(String ind_kyc_id);
	public  String Getfamid(String ind_kyc_id);
	
	public  String GetSocialaccid(String ind_kyc_id);
	public String geteditfamilyvalue(FamilyDetail familyDetail);
	public String editmatrial(Matrial matrial);
	public String editsummary(Summarydetail summary);
	public  String getdescription(String ind_kyc_id);
	public  List<Summarydetail> getsummarydetail(Summarydetail summarydetail);
	public List<IndSignupModel> searchprofile(IndSignupModel searchprofile);
	public List<IndSignupModel> searchhomepage(IndSignupModel searchhomepage);
	public List<IndSignupModel> searchprofilekycid(IndSignupModel searchprofile);
	public List<IndSignupModel> viewmore(IndSignupModel viewmore);
	
	public String getprofilepic(IndSignupModel searchprofile);
	
	public  String getsearchedPresentAddress(IndSignupModel searchprofile);
	
	public List<Profileupdatemodel> profilenotification(Profileupdatemodel profilenotification);
	public List<Profileupdatemodel> profilevisitors(Profileupdatemodel profilenotification);
	public List<Profileupdatemodel> profileactivities(Profileupdatemodel profilenotification);
	public String editSocialacc(Socialacc socialacc);
	public String Getacademicdetail(DocumentDetailModel docdetails); 
	public List<DocumentDetailModel> Getemployeedetail(DocumentDetailModel docdetails);
	public String editBasicDetail(BasicDetail BasicDetail);
	public Socialacc socialaccvalue(int social_det_id);
	public String Getmatid(Matrial matrial);
	public String Getbasicid(BasicDetail BasicDetail);
		
	public List<UtiliserProfileModel> searchhomepageforUti(String searchvalue);
	
	public int findInd_id(String ind_kyc_id);
	
	public List<BasicDetail> getBasicHistory(int ind_id);
	
	public List<FamilyDetail> getFamilyHistory(int ind_id);
	
	public List<Socialacc> getsocialHistory(int ind_id);
	
	public List<String> getIndNotify(String kyc_id);
	
	public List<Date> getSharedIndDetails(String kyc_id);
	
	public String getCreatedBy(String kycId);
	
	public String getSum_det_id(String id);
	
	public String saveAboutUsInfo(Summarydetail sumDetail);
	
	public String getAboutUsInfo(Summarydetail sumDetail);
	
	public String getPicName(int ind_id);
	
	public String saveProfilePicInfo(ProfilePictureModel profileModel);
	
	public String getLatestPicName(int ind_id);
	
	public ProfilePictureModel getPrifilePicInfo(int ind_id);
	
	public int getPrifilePicId(String picName);
	
	public String GetacademicPostDate(DocumentDetailModel docdetails);
		
	public String GetEmpPostDate(DocumentDetailModel docdetails);
	
	public String getPermanentAddress(int id);
	 		
	public String getPresentAddress(int id);	

	public String getRegVisisbility(int ind_id);
	
	public String getBasicVisisbility(int ind_id);
	
	public String getFamilyVisisbility(int ind_id);
	
	public String getSocialVisisbility(int ind_id);
	
	public String updateRegVisibility(int ind_id,String val);
	
    public String setBasicVisibility(int ind_id,String val);
	
	public String setFamilyVisibility(int ind_id,String val);
	
	public String setSocialVisibility(int ind_id,String val);
	
	public List<BasicDetail> getBasicDetail(int ind_id);
	
	public List<FamilyDetail> getFamilyDetail(int ind_id);
	
	public List<Socialacc> getsocialdetails(int ind_id);
	
	public String getProfilePhoto(int ind_id);
	
	public String updateProfilePic(String picName,int ind_id);
	
	public List<RegHistoryModel> getIndividualDetails(int ind_id);
	
	public int getYear(int ind_id);
	
	public List<IndSignupModel> getSearchKycInd(String searchedValue);
	
	public String getCityName(int indId);
	
	public String getSearchedAboutUsInfo(int ind_id);
	
	public List<String> getValidateEmailId(String emailId);
	
	public String getViewedStatus(String kycId,String ownKycId);
	
	public String getIndividualIdVaiEmailId(String emailId);
	
	public List<KycNotificationModel> getNotificationDetails(int ind_id);
	
	public String getMessagefromNotifyId(int notifyId);
	
	public int findUtiid(String uti_kycid);
	
	public String saveActivityDetails(UtilizerActivityModel actModel);
	
	public List<AccessDetailsModel> getuti2IndNotification(String kyc_id);
	
	public List<String> getValidateMobileNo(String mobileNo);
	
	public String individualIdThroughEmailId(String emailId);
	
	public String getUpdateEmailId(int ind_id,String EmailId);
	
	public List<String> getKycIdOfAccessGiven(String IndKycId);
	
	public String getIndividualIdViaKycId(String KycId);
	
	public String getYearFromIndividualId(String indIndArray);
	
	public String saveResetPasswordModel(ResetPasswordModel resetPW);
	
	public String getCurrentPlanName(int ownInd_id);
	
	public Date getRegisterDate(int ownInd_id);
	
	public String getEntryInPlanDetails(IndividualPlanDetailModel indPlanModel);
	
	public String getvalidateRegistrationEmailId(String emailId,int ind_id);
	
	public String getvalidateRegistrationMobileNo(String mobileNo,int ind_id);
	
	public String setEmailStatusReport(EmailSentStatusModel emailReport);
	
	public String setSmsStatusReport(SmsSentStatusModel smsReport);
	
	public String migratePlanAfterOneYear(int ownInd_id);
	
    public String saveUtiResetPasswordModel(UtilizerPasswordRequestModel resetPW);
    
    public  List<RegHistoryModel>  getIndividualDetailsviaIndId(int indId);
    
    public String getUpdateRegTableWithLastRecord(RegHistoryModel indHistory,String fulleName);
    
    public String getUtilizerAccessForIndividual(String individualKycId,String utilizerKycId);
    
    public List<BasicDetail> getLatestBasicDetailsInfo(int basicId);
    
    public int getIndividualIdVaiVisibility(String resetVariable);
    
    public String updateVisibilityUrl(int ind_id,String resetVariable,String encryptedUrl);
    
    public String getLatestProfilePitureName(int individualId);
    
    public String saveClenseeContactDetails(ContactUsModel contactus);
    
    public int getIndIdViaEmailId(String emailId);
}
   