package com.kyc.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kyc.model.AccessDetailsModel;
import com.kyc.model.BasicDetail;
import com.kyc.model.CodesDetailsModel;
import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentDetailModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.FamilyDetail;
import com.kyc.model.IndSignupModel;
import com.kyc.model.IndividualPlanDetailModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.Matrial;
import com.kyc.model.ProfilePictureModel;
import com.kyc.model.Profileupdatemodel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.ResetPasswordModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.Socialacc;
import com.kyc.model.Summarydetail;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerActivityModel;
import com.kyc.model.UtilizerPasswordRequestModel;

@Repository("ProfileinfoDao")
public class ProfileinfoDaoImpl implements ProfileinfoDao {

	@Autowired
	private SessionFactory sessionFactory;
	


	public String addFamilyDetail(FamilyDetail FamilyDetail) {

		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(FamilyDetail);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successsavefamily";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
				
	}
	
	
	
	
	
	
	public String profileupdate(Profileupdatemodel Profileupdatemodel){
		
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(Profileupdatemodel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
		
	}

	
	public String addBasicDetail(BasicDetail BasicDetail){
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(BasicDetail);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successsavebasic";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
		
	}
	
	
	public String getkycid(int ind_kyc_id){
				List lst = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select kycid from IndSignupModel where  ind_id= '"+ind_kyc_id+"'").list();

		if (lst.size() > 0) {
			return "" + lst.get(0);
		} else {
			return "failure";
		}
	}

	@SuppressWarnings("unchecked")
	public String getfirstname(String ind_kyc_id1){
		
		List<IndSignupModel> lst = sessionFactory.getCurrentSession().createQuery("from IndSignupModel where  kycid= '"+ind_kyc_id1+"'").list();
		
		if (lst.size() > 0) {
			return "" + lst.get(0).getFirstname()+" "+lst.get(0).getMiddlename()+" "+lst.get(0).getLastname();
		} else {
			return "failure";
				
        }
      }
	
	
	
	public String visibilityBasicDetail(BasicDetail BasicDetail){
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("UPDATE BasicDetail SET visibility = '"+BasicDetail.getVisibility() +"' where ind_kyc_id='"+BasicDetail.getInd_kyc_id()+"'").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successeditpassword";
		} else {
			txStatus="failurepassword";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}

	
	@SuppressWarnings("unchecked")
	public Socialacc getsocialaccvalue(int sum_det_id) {
		
		/*String hql = "from Socialacc where social_det_id="+sum_det_id+"";
		
		List<Socialacc> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();*/
		return (Socialacc) sessionFactory.getCurrentSession().get(
				Socialacc.class, sum_det_id);
	}

	
	
	
	public String addMatrial(Matrial matrial){
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(matrial);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successsavematrimony";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
		
	}
	
	public String addSocialacc(Socialacc socialacc) {

		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(socialacc);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successsavesocialacc";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
				
	}
	
	
	public String addSummarydetail(Summarydetail summarydetail) {

		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(summarydetail);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successsavesummarydetail";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
				
	}

	
	public FamilyDetail getfamilyvalue(int fam_details_id) {
		
		
		return (FamilyDetail) sessionFactory.getCurrentSession().get(
				FamilyDetail.class, fam_details_id);
	}

	
	public Summarydetail getSummaryvalues(String ind_kyc_id) {
		return (Summarydetail) sessionFactory.getCurrentSession().get(
				Summarydetail.class, ind_kyc_id);
	}

	public Matrial getMatrimonyvalue(String ind_kyc_id) {
		return (Matrial) sessionFactory.getCurrentSession().get(Matrial.class,
				ind_kyc_id);

	}

	@Override
	public Socialacc getsocialdetails(String ind_kyc_id) {
		// TODO Auto-generated method stub
		return (Socialacc) sessionFactory.getCurrentSession().get(
				Socialacc.class, ind_kyc_id);
	}

	@Override
	public IndSignupModel getregistrationdetails(String ind_kyc_id) {

		return (IndSignupModel) sessionFactory.getCurrentSession().get(
				IndSignupModel.class, ind_kyc_id);
	}

	public String Getregid(Summarydetail summary) {
		String kycid = summary.getInd_kyc_id();
		List lst = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select sum_det_id from Summarydetail where ind_kyc_id='"
								+ kycid + "' ").list();

		if (lst.size() > 0) {
			return "" + lst.get(0);
		} else {
			return "failure";
		}

	}

	public Summarydetail getSummaryvalue(int sum_det_id) {
		return (Summarydetail) sessionFactory.getCurrentSession().get(
				Summarydetail.class, sum_det_id);
	}

	@SuppressWarnings("unchecked")
	public String getSearchedAboutUsInfo(int ind_id)
	{
	   String hql = "select landpagedescription from Summarydetail where ind_kyc_id="+ind_id+"";
	   List<String> searchedUserInfo = sessionFactory.getCurrentSession().createQuery(hql).list();
	   
	   if(searchedUserInfo.size()>0)
	   {
		   return searchedUserInfo.get(0);
	   }
	   else
	   {
		   return "No Data";
	   }
	   
	}
	
	
	public Matrial getMatrimonyvalue(int matrimony_details_id) {
		return (Matrial) sessionFactory.getCurrentSession().get(Matrial.class,
				matrimony_details_id);
	}

	public   BasicDetail getBasicDetailvalue(int BasicDetailid) {
		return (BasicDetail) sessionFactory.getCurrentSession().get(
				BasicDetail.class, BasicDetailid);
	}

	@Override
	public Summarydetail getSummaryvalue(String ind_kyc_id) {
		// TODO Auto-generated method stub
		return null;
	}

	public String Getfamid(String ind_kyc_id) {
		String kycid = ind_kyc_id;
		List lst = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select fam_details_id from FamilyDetail where ind_kyc_id='"
								+ kycid + "'  and cr_date=(select max(cr_date) from FamilyDetail where ind_kyc_id='"
								+ kycid + "')").list();
		if (lst.size() > 0) {
			System.out.println("success");
			return "" + lst.get(0);
		} else {
			System.out.println("failure");
			return "failurefamily";
		}
	}

	public String Getsocialaccid(String ind_kyc_id) {
		String kycid = ind_kyc_id;
		List lst = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select social_det_id from Socialacc where ind_kyc_id='"
								+ kycid + "' and cr_date=(select max(cr_date) from Socialacc where ind_kyc_id='"
								+ kycid + "') ").list();
		if (lst.size() > 0) {

			return "" + lst.get(0);
		} else {
			return "failuresocialacc";
		}
	}

	public String GetSocialaccid(String ind_kyc_id) {
		String kycid = ind_kyc_id;
		List lst = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select social_det_id from Socialacc where ind_kyc_id='"
								+ kycid + "' ").list();
		System.out.println("daoimpli" + lst.get(0));
		return "" + lst.get(0);
	}

	
	@SuppressWarnings("unchecked")
	public String geteditfamilyvalue(FamilyDetail FamilyDetail){
		
		String hql = "from FamilyDetail where ind_kyc_id="+FamilyDetail.getInd_kyc_id()+" and cr_date=(select MAX(cr_date) from FamilyDetail where ind_kyc_id="+FamilyDetail.getInd_kyc_id()+")";		
		List<FamilyDetail> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			if(FamilyDetail.getFather_name().equals(lsdata.get(0).getFather_name()) && FamilyDetail.getFather_POI().equals(lsdata.get(0).getFather_POI()) && FamilyDetail.getMother_name().equals(lsdata.get(0).getMother_name()) && FamilyDetail.getMother_POI().equals(lsdata.get(0).getMother_POI()) && FamilyDetail.getSibling_name().equals(lsdata.get(0).getSibling_name()) && FamilyDetail.getSibling_POI().equals(lsdata.get(0).getSibling_POI()) && FamilyDetail.getSpouse_name().equals(lsdata.get(0).getSpouse_name()) && FamilyDetail.getSpouse_POI().equals(lsdata.get(0).getSpouse_POI()) && FamilyDetail.getHoroscopeInformation().equals(lsdata.get(0).getHoroscopeInformation()) && FamilyDetail.getFoodpreferred().equals(lsdata.get(0).getFoodpreferred()))
			{
				return "successeditfamily";
			}
			else
			{
				String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(FamilyDetail);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="successeditfamily";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
			}
		}
		else
		{
			String txStatus="";
			Session docSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=docSession.beginTransaction();
			docSession.saveOrUpdate(FamilyDetail);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="successeditfamily";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				docSession.close();
			}
			return txStatus;
		}
		
	
	}
	
	@SuppressWarnings("unchecked")
	public String editSocialacc(Socialacc socialacc){
		
		String hql = "from Socialacc where ind_kyc_id="+socialacc.getInd_kyc_id()+" and cr_date=(select MAX(cr_date) from Socialacc where ind_kyc_id="+socialacc.getInd_kyc_id()+")";		
		List<Socialacc> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();

		if(lsdata.size()>0)
		{
			if(socialacc.getLinkedln_link().equals(lsdata.get(0).getLinkedln_link()) && socialacc.getTwitter_link().equals(lsdata.get(0).getTwitter_link()) && socialacc.getWebsite_link().equals(lsdata.get(0).getWebsite_link()) )
			{
				return "successeditsocialacc";
			}
			else
			{
				String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(socialacc);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="successeditsocialacc";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
			}
		}
		else
		{
			String txStatus="";
			Session docSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=docSession.beginTransaction();
			docSession.saveOrUpdate(socialacc);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="successeditsocialacc";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				docSession.close();
			}
			return txStatus;
		}
		
						
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String editBasicDetail(BasicDetail BasicDetail){
		
		String hql = "from BasicDetail where ind_kyc_id="+BasicDetail.getInd_kyc_id()+" and cr_date=(select MAX(cr_date) from BasicDetail where ind_kyc_id="+BasicDetail.getInd_kyc_id()+")";		
		List<BasicDetail> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();

		if(lsdata.size()>0)
		{
			if(BasicDetail.getAlternate_email().equals(lsdata.get(0).getAlternate_email()) && BasicDetail.getDate_of_birth().equals(lsdata.get(0).getDate_of_birth()) && BasicDetail.getTell_office().equals(lsdata.get(0).getTell_office()) && BasicDetail.getTell_residence().equals(lsdata.get(0).getTell_residence()) && BasicDetail.getMarital_status().equals(lsdata.get(0).getMarital_status()) && BasicDetail.getLanguages_known().equals(lsdata.get(0).getLanguages_known()) && BasicDetail.getHobbies().equals(lsdata.get(0).getHobbies()) && BasicDetail.getPresent_address().equals(lsdata.get(0).getPresent_address()) && BasicDetail.getPermanent_address().equals(lsdata.get(0).getPermanent_address()) )
			{
				return "successeditbasic";
			}
			else
			{
				String txStatus="";
				Session docSession=sessionFactory.openSession();
				Transaction tx=null;
				try {
				tx=docSession.beginTransaction();
				docSession.saveOrUpdate(BasicDetail);
				tx.commit();
				if(tx.wasCommitted()){
					txStatus="successeditbasic";
				} else {
					txStatus="failure";
				}
				} catch (Exception he) {
					System.out.println(he.getMessage());
				} finally {
					docSession.close();
				}
				return txStatus;
			}
		}
		else
		{
			String txStatus="";
			Session docSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=docSession.beginTransaction();
			docSession.saveOrUpdate(BasicDetail);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="successeditbasic";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				docSession.close();
			}
			return txStatus;
		}								
	}

	
	
	public String editmatrial(Matrial matrial){
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		sessionFactory
		.getCurrentSession()
		.createQuery(
				"UPDATE Matrial SET spouse_name = '"
						+ matrial.getSpouse_name()
						+ "' , spouse_gender='"
						+ matrial.getSpouse_gender()
						+ "' ,horoscope_info='"
						+ matrial.getHoroscope_info()
						+ "' ,food_preferred='"
						+ matrial.getFood_preferred()
						+ "' WHERE ind_kyc_id = '"
						+ matrial.getInd_kyc_id() + "'")
		.executeUpdate();
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successeditmatrimony";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
		
	}
	
	public String editsummary(Summarydetail summary){
		String txStatus="";
		summary.getLandpagedescription();
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		sessionFactory
		.getCurrentSession()
		.createQuery(
				"UPDATE Summarydetail SET about_individual = '"																								
						+ summary.getLandpagedescription()
						+ "' WHERE ind_kyc_id = '"
						+ summary.getInd_kyc_id() + "'")
		.executeUpdate();
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successeditsummarydetail";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
		
	}
	
	
	public String getdescription(String ind_kyc_id) {
		
		String kycid = ind_kyc_id;
		List lst = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select landpagedescription from Summarydetail where  ind_kyc_id = '"
								+ kycid + "'").list();
		return "" + lst.get(0);

	}

	@Override
	public Socialacc socialaccvalue(int social_det_id) {
		return (Socialacc) sessionFactory.getCurrentSession().get(
				Socialacc.class, social_det_id);

	}

	@Override
	public String Getmatid(Matrial matrial) {
		// TODO Auto-generated method stub

		String kycid = matrial.getInd_kyc_id();
		List lst = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select matrimony_details_id from Matrial where ind_kyc_id='"
								+ kycid + "' ").list();
		if (lst.size() > 0) {
			return "" + lst.get(0);
		} else {
			return "failurematrimony";
		}
	}

	@SuppressWarnings("unchecked")
	public String Getbasicid(BasicDetail BasicDetail) {

		String kycid = BasicDetail.getInd_kyc_id();
		List lst = sessionFactory.getCurrentSession().createQuery("select basic_details_id from BasicDetail where ind_kyc_id='"
								+ kycid + "' and cr_date=(select max(cr_date) from BasicDetail where ind_kyc_id='"
								+ kycid + "') ").list();
		
		if (lst.size() > 0) {
				
			
			
			return "" + lst.get(0);
			
		} else {
			return "failurebasic";
		}
	}

	@Override
	public void setregid(String ind_kyc_id) {
		sessionFactory
				.getCurrentSession() 
				.createQuery(
						"insert into Summarydetail(ind_kyc_id) values ('"
								+ ind_kyc_id + "') ").executeUpdate();

	}

	
	@SuppressWarnings("unchecked")
	public String getprofilepic(IndSignupModel searchprofile) {
		
		int ind_kyc_id=searchprofile.getIndid();
		
		String hql = "select profile_picture_name from ProfilePictureModel where kyc_ind_id="+ind_kyc_id+" and cr_date=(select MAX(cr_date) from ProfilePictureModel where kyc_ind_id="+ind_kyc_id+")";
				
				List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
					
					if (lsdata.size() > 0) {

						return "" + lsdata.get(0);
					} else {
						return "noprofilepic";
					}
			}
	
	
	@SuppressWarnings("unchecked")
	public  String getsearchedPresentAddress(IndSignupModel searchprofile)
	{
		int ind_kyc_id=searchprofile.getIndid();
		String hql = "select present_address from BasicDetail where ind_kyc_id='"+ind_kyc_id+"' and cr_date=(select MAX(cr_date) from BasicDetail where ind_kyc_id='"+ind_kyc_id+"')";
		
		List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if (lsdata.size() > 0) {

				
				String address = ""+lsdata.get(0);
				if(address.contains("##"))
				{
					String addressEach[] = address.split("##");								
					return addressEach[5]+"-"+addressEach[6];
				}
				else
				{
					return "NA";
				}
				
			} else {
				return "NA";
			}	
	}
	
	@SuppressWarnings("unchecked")
	public List<Profileupdatemodel> profilenotification(Profileupdatemodel profilenotification) {

		String hql = "from Profileupdatemodel where kyc_id='"
								+ profilenotification.getKyc_id() + "'";
		
		List<Profileupdatemodel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			System.out.println("dao impl search" + lsdata.size());
			return lsdata;
		}
	
	
	@SuppressWarnings("unchecked")
	public List<Profileupdatemodel> profilevisitors(Profileupdatemodel profilenotification) {

		String hql = "from Profileupdatemodel where kyc_id='"
								+ profilenotification.getKyc_id() + "' and type_of_alert='visitor update' order by profileupdatetime desc";
		
		List<Profileupdatemodel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			return lsdata;
		}

	@SuppressWarnings("unchecked")
	public List<Profileupdatemodel> profileactivities(Profileupdatemodel profilenotification) {
        
		
		
		String hql = "from Profileupdatemodel where kyc_id='"
								+ profilenotification.getKyc_id() + "' and type_of_alert<>'visitor update' order by profileupdatetime desc";
		
		List<Profileupdatemodel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
		}
	
	@Override
	public List<Summarydetail> getsummarydetail(Summarydetail summarydetail) {
		@SuppressWarnings("unchecked")
		List<Summarydetail> lst = sessionFactory
				.getCurrentSession()
				.createQuery(
						" from Summarydetail where ind_kyc_id='"
								+ summarydetail.getInd_kyc_id() + "'").list();
		return lst;

	}
	
			
	@SuppressWarnings("unchecked")
	public String Getacademicdetail(DocumentDetailModel docdetails) {
	
		
		StringBuilder academicdata= new StringBuilder();
		int ind_kyc_id=docdetails.getKyc_ind_id();
		
		 String sqlQuery  = "select docs_data from(select * from document_details order by cr_date desc) a where a.doc_type like 'ACADEMIC%' and a.kyc_ind_id = "+ind_kyc_id+" group by substr(a.docs_data,1,9)";
		
		 List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sqlQuery).addScalar("docs_data", Hibernate.STRING).list();
		 
		 for(int i=0;i<lsdata.size();i++)
		 {			 
			 academicdata.append(lsdata.get(i)+"/");			 
		 }
				 		 
		 String hql = "select MAX(cr_date) from DocumentDetailModel where a.doc_type like 'ACADEMIC%' and a.kyc_ind_id = "+ind_kyc_id+"";
				 		 
		 return academicdata.toString();
			
		
		}
	
	
	@SuppressWarnings("unchecked")
	public List<IndSignupModel> searchprofile(IndSignupModel searchprofile) {

		
		List<IndSignupModel> searchedProfileData = new ArrayList<IndSignupModel>();		
						
		if(searchprofile.getFirstname().contains(" "))
		{			 
			  List<IndSignupModel> NameData2space = new ArrayList<IndSignupModel>();
			  String hqlForSpaceFN = "from IndSignupModel where individual_full_name like '%"+searchprofile.getFirstname()+"%'";				   
			  NameData2space = sessionFactory.getCurrentSession().createQuery(hqlForSpaceFN).list();	
			  
			  if(NameData2space.size()>0)
			  {				  
				  searchedProfileData.addAll(NameData2space);			  
			  }	
			  			
			  return searchedProfileData;
		}
		else
		{
			String hqlViaFN = "from IndSignupModel where firstname like '%"+searchprofile.getFirstname()+"%'";										
			searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaFN).list();
						
			if(searchedProfileData.size()>0)
			{
				List<IndSignupModel> middleNameData = new ArrayList<IndSignupModel>();	
				
				String hqlViaMN = "from IndSignupModel where middlename like '%"+searchprofile.getFirstname()+"%'";										
				middleNameData = sessionFactory.getCurrentSession().createQuery(hqlViaMN).list();			
				if(middleNameData.size()>0)
				{
					  /* Removing Duplicacy Values */
					  
			     	  System.out.println("check"+searchedProfileData.size());
			  					
					  for(int i=0;i<searchedProfileData.size();i++)
					  {							
						  if(middleNameData.contains(searchedProfileData.get(i)))
						  {
							  middleNameData.remove(searchedProfileData.get(i));
						  }																			
					  }
					 
					  /* End of Removing Duplicacy Values */
					  
					searchedProfileData.addAll(middleNameData);								
				}	
				
				List<IndSignupModel> lastNameData = new ArrayList<IndSignupModel>();
				String hqlViaLN = "from IndSignupModel where lastname like '%"+searchprofile.getFirstname()+"%'";										
				lastNameData = sessionFactory.getCurrentSession().createQuery(hqlViaLN).list();	
				
				if(lastNameData.size()>0)
				{
					 /* Removing Duplicacy Values */
					  
			     	  System.out.println("check"+searchedProfileData.size());
			  					
					  for(int i=0;i<searchedProfileData.size();i++)
					  {							
						  if(lastNameData.contains(searchedProfileData.get(i)))
						  {
							  lastNameData.remove(searchedProfileData.get(i));
						  }																			
					  }
					 
					  /* End of Removing Duplicacy Values */
					  
					  searchedProfileData.addAll(lastNameData);						
				}
				
				List<IndSignupModel> emailData = new ArrayList<IndSignupModel>();
				
				if(searchprofile.getFirstname().contains("@"))
				{
					if(searchprofile.getFirstname().startsWith("@"))
					{
						
					}
					else
					{
						int splitSearch = searchprofile.getFirstname().split("@").length;
						System.out.println("Length of String="+splitSearch);
						if(splitSearch==2)
						{
							String hqlViaemail = "from IndSignupModel where emailid like '%"+searchprofile.getFirstname()+"%'";					
							searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
						}
						if(splitSearch==1)
						{
							String hqlViaemail = "from IndSignupModel where emailid like '"+searchprofile.getFirstname()+"%'";					
							searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
						}
					}	
				}
				else
				{
					String hqlViaemail = "from IndSignupModel where emailid like '%"+searchprofile.getFirstname()+"%@%'";					
					emailData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();					
				}
									
				if(emailData.size()>0)
				{
					 /* Removing Duplicacy Values */
					  
			     	  System.out.println("check"+searchedProfileData.size());
			  					
					  for(int i=0;i<searchedProfileData.size();i++)
					  {							
						  if(emailData.contains(searchedProfileData.get(i)))
						  {
							  emailData.remove(searchedProfileData.get(i));
						  }																			
					  }
					 
					  /* End of Removing Duplicacy Values */
					  
					searchedProfileData.addAll(emailData);
				}							
				return searchedProfileData;			
			}
			else
			{
				String hqlViaMN = "from IndSignupModel where middlename like '%"+searchprofile.getFirstname()+"%'";										
				searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaMN).list();			
				if(searchedProfileData.size()>0)
				{               
					List<IndSignupModel> lastNameData = new ArrayList<IndSignupModel>();
					String hqlViaLN = "from IndSignupModel where lastname like '%"+searchprofile.getFirstname()+"%'";										
					lastNameData = sessionFactory.getCurrentSession().createQuery(hqlViaLN).list();	
					
					if(lastNameData.size()>0)
					{
						 /* Removing Duplicacy Values */
						  
				     	  System.out.println("check"+searchedProfileData.size());
				  					
						  for(int i=0;i<searchedProfileData.size();i++)
						  {							
							  if(lastNameData.contains(searchedProfileData.get(i)))
							  {
								  lastNameData.remove(searchedProfileData.get(i));
							  }																			
						  }
						 
						  /* End of Removing Duplicacy Values */
						  
						searchedProfileData.addAll(lastNameData);					
					}
					
					List<IndSignupModel> emailData = new ArrayList<IndSignupModel>();
					
					if(searchprofile.getFirstname().contains("@"))
					{
						if(searchprofile.getFirstname().startsWith("@"))
						{
							
						}
						else
						{
							int splitSearch = searchprofile.getFirstname().split("@").length;
							System.out.println("Length of String="+splitSearch);
							if(splitSearch==2)
							{
								String hqlViaemail = "from IndSignupModel where emailid like '%"+searchprofile.getFirstname()+"%'";					
								searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
							}
							if(splitSearch==1)
							{
								String hqlViaemail = "from IndSignupModel where emailid like '"+searchprofile.getFirstname()+"%'";					
								searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
							}
						}	
					}
					else
					{
						String hqlViaemail = "from IndSignupModel where emailid like '%"+searchprofile.getFirstname()+"%@%'";					
						emailData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();					
					}
					
					if(emailData.size()>0)
					{
						 /* Removing Duplicacy Values */
						  
				     	  System.out.println("check"+searchedProfileData.size());
				  					
						  for(int i=0;i<searchedProfileData.size();i++)
						  {							
							  if(emailData.contains(searchedProfileData.get(i)))
							  {
								  emailData.remove(searchedProfileData.get(i));
							  }																			
						  }
						 
						  /* End of Removing Duplicacy Values */
						  
						searchedProfileData.addAll(emailData);
					}	
					
					return searchedProfileData;
				}
				else
				{
					String hqlViaLN = "from IndSignupModel where lastname like '%"+searchprofile.getFirstname()+"%'";										
					searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaLN).list();	
					
					if(searchedProfileData.size()>0)
					{
						List<IndSignupModel> emailData = new ArrayList<IndSignupModel>();
						
						if(searchprofile.getFirstname().contains("@"))
						{
							if(searchprofile.getFirstname().startsWith("@"))
							{
								
							}
							else
							{
								int splitSearch = searchprofile.getFirstname().split("@").length;
								System.out.println("Length of String="+splitSearch);
								if(splitSearch==2)
								{
									String hqlViaemail = "from IndSignupModel where emailid like '%"+searchprofile.getFirstname()+"%'";					
									searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
								}
								if(splitSearch==1)
								{
									String hqlViaemail = "from IndSignupModel where emailid like '"+searchprofile.getFirstname()+"%'";					
									searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
								}
							}	
						}
						else
						{
							String hqlViaemail = "from IndSignupModel where emailid like '%"+searchprofile.getFirstname()+"%@%'";					
							emailData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();					
						}
						
						if(emailData.size()>0)
						{
							 /* Removing Duplicacy Values */
							  
					     	  System.out.println("check"+searchedProfileData.size());
					  					
							  for(int i=0;i<searchedProfileData.size();i++)
							  {							
								  if(emailData.contains(searchedProfileData.get(i)))
								  {
									  emailData.remove(searchedProfileData.get(i));
								  }																			
							  }
							 
							  /* End of Removing Duplicacy Values */
							  
							searchedProfileData.addAll(emailData);
						}	
						
						return searchedProfileData;
					}
					else
					{
						if(searchprofile.getFirstname().contains("@"))
						{
							if(searchprofile.getFirstname().startsWith("@"))
							{
								
							}
							else
							{
								int splitSearch = searchprofile.getFirstname().split("@").length;
								System.out.println("Length of String="+splitSearch);
								if(splitSearch==2)
								{
									String hqlViaemail = "from IndSignupModel where emailid like '%"+searchprofile.getFirstname()+"%'";					
									searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
								}
								if(splitSearch==1)
								{
									String hqlViaemail = "from IndSignupModel where emailid like '"+searchprofile.getFirstname()+"%'";					
									searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();	
								}
							}						
						}
						else
						{
							String hqlViaemail = "from IndSignupModel where emailid like '%"+searchprofile.getFirstname()+"%@%'";					
							searchedProfileData = sessionFactory.getCurrentSession().createQuery(hqlViaemail).list();					
						}
						
						if(searchedProfileData.size()>0)
						{
							return searchedProfileData;
						}
						else
						{
							return searchedProfileData;
						}
					}
				}
			}
		}
		
															
	}
	
	@SuppressWarnings("unchecked")
	public List<IndSignupModel> searchhomepage(IndSignupModel searchprofile)
	{
		
			String hql = "from IndSignupModel as I where I.firstname like '%"+searchprofile.getFirstname()+"%' or I.middlename like '%"+searchprofile.getFirstname()+"%' or I.lastname like '%"+searchprofile.getFirstname()+"%' or I.kycid like '%"+searchprofile.getFirstname()+"%'";					
			List<IndSignupModel> Ind_lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
					
		    return Ind_lsdata;			
							  
	}
		
		
	@SuppressWarnings("unchecked")
	public List<IndSignupModel> searchprofilekycid(IndSignupModel searchprofile) {

		String hql = "from IndSignupModel as I where I.kycid like '%"
								+ searchprofile.getKycid() + "%' and ind_id=(select max(ind_id) from IndSignupModel where kyc_id like '%"
								+ searchprofile.getKycid() + "%') ";
				
				List<IndSignupModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				return lsdata;
				
		}
	
		@SuppressWarnings("unchecked")
		public List<IndSignupModel> viewmore(IndSignupModel viewmore) {

			String hql = "from IndSignupModel as I where I.ind_id='"+ viewmore.getIndid() + "'";
					
					List<IndSignupModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
					return lsdata;		
			}
		
	//Under Construction
	
	
	@SuppressWarnings("unchecked")
	public List<DocumentDetailModel> Getemployeedetail(DocumentDetailModel docdetails) {
		
 		
        List<Date> listDates = new ArrayList<Date>();		
		DateFormat dateFormatter = new SimpleDateFormat("d/M/yy");
		
		String flagReport = "";
		
		int ind_kyc_id=docdetails.getKyc_ind_id();
		
		List<DocumentDetailModel> lsdata = new ArrayList<DocumentDetailModel>();
		
		String panel = "select panel_status from  DocumentDetailModel where kyc_ind_id="+ind_kyc_id+" and doc_type like 'Employment%' and cr_date =(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_kyc_id+" and doc_type like 'Employment%')";
		List<String> panelStatus = sessionFactory.getCurrentSession().createQuery(panel).list();
		
		
		
		if(!panelStatus.isEmpty())
		{		
			System.out.println("Current Pannels"+panelStatus.get(0));
			
			String panelString = panelStatus.get(0);
			String PanelStringArray[]  = panelString.split("/");
			
				for(int i=0;i<PanelStringArray.length;i++)
				{
					String latestDocsDataHQL = "select docs_data from  DocumentDetailModel where kyc_ind_id="+ind_kyc_id+" and doc_type like 'Employment%' and docs_data like '"+PanelStringArray[i]+"%' and docs_data not like '%No Data%' and cr_date =(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_kyc_id+" and doc_type like 'Employment%' and docs_data like '"+PanelStringArray[i]+"%' and docs_data not like '%No Data%')";						
					List<String> latestDocData = sessionFactory.getCurrentSession().createQuery(latestDocsDataHQL).list();	
					
					if(latestDocData.size()>0)
					{
						if(!latestDocData.get(0).contains("No Data"))
						{
							System.out.println("Latest Docs Data "+latestDocData.get(0));
							
							if(latestDocData.get(0).contains("Presently Working"))
							{
								flagReport = "True";
								String hql = "from DocumentDetailModel where kyc_ind_id="+ind_kyc_id+" and doc_type like 'Employment%' and docs_data like '"+PanelStringArray[i]+"%' and cr_date =(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_kyc_id+" and doc_type like 'Employment%' and docs_data like '"+PanelStringArray[i]+"%')";							
								lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
								break;
							}
							else
							{
								String emp_docs_data = latestDocData.get(0);
								String emp_docs_data_array[] = emp_docs_data.split("&&");
																								
								try 
									{			
								       listDates.add(dateFormatter.parse(emp_docs_data_array[4]));		// In a list we placed all job end date								    										    
									} 
									
								catch (ParseException ex) 
									{
										System.err.print(ex);
									}																		
							}
						}	
					}
															
				}	
				
				// Now we will sort these dates 
				
				if(flagReport.equals("True"))
				{
					return lsdata;
				}
				else
				{
						if(listDates.size()>0)
						{
							Collections.sort(listDates);				 
							Date latestDate = listDates.get(listDates.size()-1);						
							DateFormat df = new SimpleDateFormat("d/M/yy");				   		
							String reportDate = df.format(latestDate);
							
							System.out.println("The Sorted Date"+reportDate);
							
							String temp[] = reportDate.split("/");
							
							String dd = "";  String MM = "";  String yy = "";
							String arracngedDate = "";
										
							if(temp[0].startsWith("0"))
							{
								dd += temp[0].replaceAll("0","");
							}
							
							if(!dd.equals(""))
							{
								arracngedDate +=dd+"/";
							}
							
							if(dd.equals(""))
							{
								arracngedDate +=temp[0]+"/";
							}
							
							if(temp[1].startsWith("0"))
							{
								MM += temp[1].replaceAll("0","");
							}
							
							if(!MM.equals(""))
							{
								arracngedDate +=MM+"/";
							}
							
							if(MM.equals(""))
							{
								arracngedDate +=temp[1]+"/";
							}
							
							/*if(temp[2].startsWith("0"))
							{
								yy += temp[2].replaceAll("0","");
							}*/
							
							if(!yy.equals(""))
							{
								arracngedDate +=yy;
							}
							
							if(yy.equals(""))
							{
								arracngedDate +=temp[2];
							}
							
							System.out.println("The Sorted String"+arracngedDate);
							
							for(int i=0;i<PanelStringArray.length;i++)
							{
								String hql = "from DocumentDetailModel where kyc_ind_id="+ind_kyc_id+" and doc_type like 'Employment%' and docs_data LIKE '"+PanelStringArray[i]+"%' and docs_data LIKE '%"+arracngedDate+"' and cr_date =(select MAX(cr_date) from DocumentDetailModel where kyc_ind_id="+ind_kyc_id+" and doc_type like 'Employment%' and docs_data LIKE '"+PanelStringArray[i]+"%' and docs_data LIKE '%"+arracngedDate+"')";							
								lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
								if(lsdata.size()>0)
								{							
									break;
								}
							}														
						}
						return lsdata;
				}				
		}
		else
		{
			return lsdata;	
		}
		
	}
	
	
	//Under Construction
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<UtiliserProfileModel> searchhomepageforUti(String searchvalue)
	{
		String hql1 = "from UtiliserProfileModel where kyc_uti_id like '%"+searchvalue+"%' or uti_first_name like '%"+searchvalue+"%' or uti_middle_name like '%"+searchvalue+"%' or uti_last_name like '%"+searchvalue+"%'";
		List<UtiliserProfileModel> uti_lsdata1 = sessionFactory.getCurrentSession().createQuery(hql1).list();
		
		return uti_lsdata1;
	}

	@SuppressWarnings("unchecked")
	public int findInd_id(String ind_kyc_id)
	{
		String hql = "select ind_id from IndSignupModel where kycid='"+ind_kyc_id+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<BasicDetail> getBasicHistory(int ind_id)
	{
		String hql = "from BasicDetail where ind_kyc_id="+ind_id+"";
		List<BasicDetail> basicData = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return basicData;
	}
	
	@SuppressWarnings("unchecked")
	public List<FamilyDetail> getFamilyHistory(int ind_id)
	{
		String hql = "from FamilyDetail where ind_kyc_id="+ind_id+"";
		List<FamilyDetail> fmdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return fmdata;
	}
		
	@SuppressWarnings("unchecked")
	public List<Socialacc> getsocialHistory(int ind_id)
	{
		String hql = "from Socialacc where ind_kyc_id="+ind_id+"";
		List<Socialacc> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String> getIndNotify(String kyc_id)
	{
		
		String sql = "select code_sender from codes_details where cr_date >=now()-INTERVAL 1 DAY and code_receiver='"+kyc_id+"'";
		List<String> lsd =  sessionFactory.getCurrentSession().createSQLQuery(sql).list();
		
		return lsd;
	}
	
	@SuppressWarnings("unchecked")
	public List<Date> getSharedIndDetails(String kyc_id)
	{
		String sql = "select cr_date from codes_details where cr_date >=now()-INTERVAL 1 DAY and code_receiver='"+kyc_id+"'";
		List<Date> lsd =  sessionFactory.getCurrentSession().createSQLQuery(sql).list();		
		return lsd;
	}
	
	@SuppressWarnings("unchecked")
	public String getCreatedBy(String kycId)
	{
		String sql = "from IndSignupModel where kycid='"+kycId+"'";
		List<IndSignupModel> lsd =  sessionFactory.getCurrentSession().createQuery(sql).list();
		
		return lsd.get(0).getFirstname()+" "+lsd.get(0).getMiddlename()+" "+lsd.get(0).getLastname();
	}
	
	@SuppressWarnings("unchecked")
	public String getSum_det_id(String id)
	{
		String hql = "select sum_det_id from Summarydetail where ind_kyc_id='"+id+"'";
		List<Integer> lsd =  sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsd.size()>0)
		{
			int sum_id= lsd.get(0);
			String str = Integer.toString(sum_id);
			return str;
		}
		else
		{
			return "Not There";
		}
	}
	
	public String saveAboutUsInfo(Summarydetail sumDetail)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(sumDetail);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successeditfamily";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String getAboutUsInfo(Summarydetail sumDetail)
	{
		String hql = "select landpagedescription from Summarydetail where ind_kyc_id='"+sumDetail.getInd_kyc_id()+"'";
		List<String> lsd =  sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsd.size()>0)
		{
		  String dateQuery = "select cr_date from Summarydetail where ind_kyc_id='"+sumDetail.getInd_kyc_id()+"'";	
		  List<String> lsd1 =  sessionFactory.getCurrentSession().createQuery(dateQuery).list();		  
		  String date = lsd1.toString();		  
		  return lsd.get(0)+"KYCSEPERATORKYC"+date;
		}
		else
		{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getPicName(int ind_id)
	{
		String hql = "select profile_pic from BasicDetail where ind_kyc_id="+ind_id+" and cr_date=(select MAX(cr_date) from BasicDetail where ind_kyc_id="+ind_id+")";
		List<String> lsdata  = sessionFactory.getCurrentSession().createQuery(hql).list();		
		if(lsdata.size()>0)
		{
			if(lsdata.get(0)!=null)
			{
			    return lsdata.get(0);
			}
			else
			{
				return "no image"; 
			}
		}
		else
		{
			return "no image"; 
		}
	}
	
	public String saveProfilePicInfo(ProfilePictureModel profileModel)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try 
		{
		tx=docSession.beginTransaction();
		docSession.saveOrUpdate(profileModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} 
		else {
			txStatus="failure";
		}
		}
		catch (Exception he) {
			System.out.println(he.getMessage());
		}
		finally {
			
			docSession.close();
			
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String getLatestPicName(int ind_id)
	{
		String hql = "select profile_picture_name from ProfilePictureModel where kyc_ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from ProfilePictureModel where kyc_ind_id="+ind_id+")";
		List<String> lsdata  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
		    return lsdata.get(0);
		}
		else
		{
			return "no image";
		}
	}
	
	@SuppressWarnings("unchecked")
	public int getPrifilePicId(String picName)
	{
		String hql = "select pic_id from ProfilePictureModel where profile_picture_name='"+picName+"'";
		List<Integer> lsdata  = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public ProfilePictureModel getPrifilePicInfo(int pic_id)
	{
		return (ProfilePictureModel)sessionFactory.getCurrentSession().get(
				ProfilePictureModel.class, pic_id);
	}
	
	@SuppressWarnings("unchecked")
	public String GetacademicPostDate(DocumentDetailModel docdetails)
	{
		String hql = "select cr_date from DocumentDetailModel where cr_date=(select MAX(cr_date) from DocumentDetailModel where doc_type like 'ACADEMIC%' and kyc_ind_id="+docdetails.getKyc_ind_id()+")";
		List<Date> lsdata  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		SimpleDateFormat dateformatJava = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String cr_date = dateformatJava.format(lsdata.get(0));
		
		return cr_date;
	}
	
	@SuppressWarnings("unchecked")
	public String GetEmpPostDate(DocumentDetailModel docdetails)
	{
		String hql = "select cr_date from DocumentDetailModel where cr_date=(select MAX(cr_date) from DocumentDetailModel where doc_type like 'Employment%' and kyc_ind_id="+docdetails.getKyc_ind_id()+")";
		List<Date> lsdata  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		SimpleDateFormat dateformatJava = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String cr_date = dateformatJava.format(lsdata.get(0));
		
		return cr_date;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getPermanentAddress(int id)
	 {
		String ind_id = Integer.toString(id);
				
		String hql = "select permanent_address from BasicDetail where ind_kyc_id='"+ind_id+"' and cr_date=(select MAX(cr_date) from BasicDetail where ind_kyc_id='"+ind_id+"')";
		List<String> lsdata  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
		   return lsdata.get(0);
		}
		else
		{
			return "nothing";
		}
	 }
		
	@SuppressWarnings("unchecked")
	public String getPresentAddress(int id)
	{
		String ind_id = Integer.toString(id);
		
		String hql = "select present_address from BasicDetail where ind_kyc_id='"+ind_id+"' and cr_date=(select MAX(cr_date) from BasicDetail where ind_kyc_id='"+ind_id+"')";
		List<String> lsdata  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
		   return lsdata.get(0);
		}
		else
		{
		   return "nothing";
		}
	}
	

	@SuppressWarnings("unchecked")
	public String getRegVisisbility(int ind_id)
	{
		String hql = "select registration_visibility from VisibilityModel where kyc_ind_id="+ind_id+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
		   return lsdata.get(0);
		}
		else
		{
			return "nothing";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getBasicVisisbility(int ind_id)
	{
		String hql = "select basic_visibility from VisibilityModel where kyc_ind_id="+ind_id+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);		
	}
	
	@SuppressWarnings("unchecked")
	public String getFamilyVisisbility(int ind_id)
	{
		String hql = "select family_visibility from VisibilityModel where kyc_ind_id="+ind_id+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		 
		return lsdata.get(0);		
	}
	
	@SuppressWarnings("unchecked")
	public String getSocialVisisbility(int ind_id)
	{
		String hql = "select social_account_visibility from VisibilityModel where kyc_ind_id="+ind_id+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
		
	}
	
	public String updateRegVisibility(int ind_id,String val)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.getCurrentSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("UPDATE VisibilityModel SET registration_visibility = '"+val+"' where kyc_ind_id="+ind_id+"").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="visibiltyUpdated";
		} else {
			txStatus="visibiltyFailed";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
    public String setBasicVisibility(int ind_id,String val)
    {
    	String txStatus="";
		Session signUpSession=sessionFactory.getCurrentSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("UPDATE VisibilityModel SET basic_visibility = '"+val+"' where kyc_ind_id="+ind_id+"").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="visibiltyUpdated";
		} else {
			txStatus="visibiltyFailed";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
    }
	
	public String setFamilyVisibility(int ind_id,String val)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.getCurrentSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("UPDATE VisibilityModel SET family_visibility = '"+val+"' where kyc_ind_id="+ind_id+"").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="visibiltyUpdated";
		} else {
			txStatus="visibiltyFailed";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	public String setSocialVisibility(int ind_id,String val)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.getCurrentSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("UPDATE VisibilityModel SET social_account_visibility = '"+val+"' where kyc_ind_id="+ind_id+"").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="visibiltyUpdated";
		} else {
			txStatus="visibiltyFailed";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<BasicDetail> getBasicDetail(int ind_id)
	{
		String individual_Id = Integer.toString(ind_id);
		String hql = "from BasicDetail where ind_kyc_id='"+individual_Id+"' and cr_date=(select MAX(cr_date) from BasicDetail where ind_kyc_id='"+individual_Id+"')";
		List<BasicDetail> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
	        return lsdata;
		
		
	}
	
	@SuppressWarnings("unchecked")
	public List<FamilyDetail> getFamilyDetail(int ind_id)
	{
		String individual_Id = Integer.toString(ind_id);
		String hql = "from FamilyDetail where ind_kyc_id='"+individual_Id+"' and cr_date=(select MAX(cr_date) from FamilyDetail where ind_kyc_id='"+individual_Id+"')";
		List<FamilyDetail> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<Socialacc> getsocialdetails(int ind_id)
	{
		String individual_Id = Integer.toString(ind_id);
		String hql = "from Socialacc where ind_kyc_id='"+individual_Id+"' and cr_date=(select MAX(cr_date) from Socialacc where ind_kyc_id='"+individual_Id+"')";
		List<Socialacc> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public String getProfilePhoto(int ind_id)
	{
		String profilePhotoList = "";
		String hql = "from ProfilePictureModel where kyc_ind_id="+ind_id+"";
		List<ProfilePictureModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			for(int i=0;i<lsdata.size();i++)
			{
				profilePhotoList += lsdata.get(i).getProfile_picture_name() + "&&" + lsdata.get(i).getCr_date() + ",";
				
			}
			
			profilePhotoList = profilePhotoList.substring(0, profilePhotoList.length()-1);
			
			return profilePhotoList;
		}
		else
		{
			return "NoData";
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String getLatestProfilePitureName(int individualId)
	{
		String profilePhoto = "";
		String hql = "select profile_picture_name from ProfilePictureModel where kyc_ind_id="+individualId+" and cr_date=(select MAX(cr_date) from ProfilePictureModel where kyc_ind_id="+individualId+")";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			profilePhoto = lsdata.get(0); 
		}
		else
		{
			profilePhoto = "No Data";
		}
		return profilePhoto;
	}
	
	
	
	
	public String updateProfilePic(String picName,int ind_id)
	{		
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update ProfilePictureModel set cr_date = :setdate where kyc_ind_id = :setIndId and profile_picture_name = :setpicName");
		hql.setParameter("setdate", new Date());
		hql.setParameter("setIndId", ind_id);
		hql.setParameter("setpicName", picName);
			
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "profileUpdated";
		}
		else
		{			
			return "profileUpdatedFailed";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<RegHistoryModel> getIndividualDetails(int ind_id)
	{
		String hql = "from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
		List<RegHistoryModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public int getYear(int ind_id)
	{
		String hql="select year(cr_date) from IndSignupModel where ind_id ="+ind_id+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<IndSignupModel> getSearchKycInd(String searchedValue)
	{
	 
	  List<IndSignupModel> searchedData = new ArrayList<IndSignupModel>();	  
	  searchedData.clear();
	  	 	  
	  List<IndSignupModel> tempData = new ArrayList<IndSignupModel>();	  
	  	  
	  if(searchedValue.contains(" "))
	  {
		  List<IndSignupModel> NameData2space = new ArrayList<IndSignupModel>();
		  
		  String hqlForSpaceFN = "from IndSignupModel where individual_full_name like '%"+searchedValue+"%'";			   
		  NameData2space = sessionFactory.getCurrentSession().createQuery(hqlForSpaceFN).list();	
		  
		  if(NameData2space.size()>0)
		  {				  
			  searchedData.addAll(NameData2space);			  
		  }	
		  		
		  return searchedData;	
	  }
	  else
	  {
		  String hqlFN = "from IndSignupModel where firstname like '%"+searchedValue+"%'";			   
		  searchedData = sessionFactory.getCurrentSession().createQuery(hqlFN).list();
		 		  
		  if(searchedData.size()>0)
		  {
			  List<IndSignupModel> midlNamData = new ArrayList<IndSignupModel>();
			  String hqlMN = "from IndSignupModel where middlename like '%"+searchedValue+"%'";			   
			  midlNamData = sessionFactory.getCurrentSession().createQuery(hqlMN).list();
			  
			  if(midlNamData.size()>0)
			  {		
						  /* Removing Duplicacy Values */
						 		
						  for(int i=0;i<searchedData.size();i++)
						  {							
							  if(midlNamData.contains(searchedData.get(i)))
							  {
								  midlNamData.remove(searchedData.get(i));
							  }																			
						  }
						  
						  searchedData.addAll(midlNamData);
						 
						  /* End of Removing Duplicacy Values */			  			
			  }		  		
			 	
			  List<IndSignupModel> lasNamData = new ArrayList<IndSignupModel>();
			  String hqlLN = "from IndSignupModel where lastname like '%"+searchedValue+"%'";			   
			  lasNamData = sessionFactory.getCurrentSession().createQuery(hqlLN).list();
			  
			  if(lasNamData.size()>0)
			  {
				     /* Removing Duplicacy Values */
				  
			     	  System.out.println("check"+searchedData.size());
			  					
					  for(int i=0;i<searchedData.size();i++)
					  {							
						  if(lasNamData.contains(searchedData.get(i)))
						  {
							  lasNamData.remove(searchedData.get(i));
						  }																			
					  }
					  
					  searchedData.addAll(lasNamData);
					 
					  /* End of Removing Duplicacy Values */
			  }
			  
			  return searchedData;		  		  
		  }
		  else
		  {
			  String hqlMN = "from IndSignupModel where middlename like '%"+searchedValue+"%'";			   
			  searchedData = sessionFactory.getCurrentSession().createQuery(hqlMN).list();
			  System.out.println("IntialSize-"+searchedData.size());
			
			  if(searchedData.size()>0)
			  {
				  List<IndSignupModel> lasNamData = new ArrayList<IndSignupModel>();			  
				  String hqlLN = "from IndSignupModel where lastname like '%"+searchedValue+"%'";			   
				  lasNamData = sessionFactory.getCurrentSession().createQuery(hqlLN).list();
				  
				  if(lasNamData.size()>0)
				  {
					  /* Removing Duplicacy Values */
					  		     	 		
					  for(int i=0;i<searchedData.size();i++)
					  {							
						  if(lasNamData.contains(searchedData.get(i)))
						  {
							  lasNamData.remove(searchedData.get(i));
						  }																			
					  }
					  
					  searchedData.addAll(lasNamData);
					 
					  /* End of Removing Duplicacy Values */			
				  }
				  			 			  			 		  
				  return searchedData;
				
			  }
			  else
			  {
				  String hqlLN = "from IndSignupModel where lastname like '%"+searchedValue+"%'";			   
				  searchedData = sessionFactory.getCurrentSession().createQuery(hqlLN).list();
				  if(searchedData.size()>0)
				  {
					  return searchedData;
				  }
				  else
				  {
					  return searchedData;
				  }
			  }
		  }
	  }
	}
	
	@SuppressWarnings("unchecked")
	public String getCityName(int indId)
	{
		int ind_kyc_id=indId;
		String hql = "select present_address from BasicDetail where ind_kyc_id='"+ind_kyc_id+"' and cr_date=(select MAX(cr_date) from BasicDetail where ind_kyc_id='"+ind_kyc_id+"')";
		
		List<DocumentDetailModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
			if (lsdata.size() > 0) {

				String address = ""+lsdata.get(0);	
				if(address.contains("##"))
				{
					String addressEach[] = address.split("##");								
					return addressEach[5]+"-"+addressEach[6];	
				}
				else
				{
					return "No Data";
				}
			} 
			else
			{
				return "No Data";
			}	   
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getValidateEmailId(String emailId)
	{
		String hql = "from RegHistoryModel where email_id='"+emailId+"'";
		List<String> emailList = sessionFactory.getCurrentSession().createQuery(hql).list();
	    
		return emailList;
	}
	
	@SuppressWarnings("unchecked")
	public String getViewedStatus(String kycId,String ownKycId)
	{
		String hql = "select idprofileupdate from Profileupdatemodel where kyc_id='"+kycId+"' and viewer_kyc_id='"+ownKycId+"'";
		List<Integer> statusData= sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(statusData.size()>0)
		{
			String id = Integer.toString(statusData.get(0));
			return id;
		}
		else
		{
			return "No Data";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getIndividualIdVaiEmailId(String emailId)
	{
		String reghql = "select visibility from IndSignupModel where emailid='"+emailId+"'";
		List<String> statusData= sessionFactory.getCurrentSession().createQuery(reghql).list();
		
		if(statusData.size()==0)
		{
			String reghistoryHql = "select ind_id from RegHistoryModel where email_id='"+emailId+"'";
			List<Integer> indData= sessionFactory.getCurrentSession().createQuery(reghistoryHql).list();
			
			String indHql = "select visibility from IndSignupModel where ind_id="+indData.get(0)+"";
			List<String> visibilityData = sessionFactory.getCurrentSession().createQuery(reghql).list();
			
			return visibilityData.get(0);
		}
		else
		{
			return statusData.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<KycNotificationModel> getNotificationDetails(int ind_id)
	{
		String hql = "from KycNotificationModel where (notification_type='Individual' and notification_to='"+ind_id+"' or notification_to='All Individual') or (notification_type='System' and notification_to='System')";
		List<KycNotificationModel> adminNotification = sessionFactory.getCurrentSession().createQuery(hql).list();
		return adminNotification;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getMessagefromNotifyId(int notifyId)
	{
		String hql = "select notification_message from KycNotificationModel where notification_id="+notifyId+"";
		List<String> adminNotification = sessionFactory.getCurrentSession().createQuery(hql).list();
		return adminNotification.get(0);
	}
	
	
	@SuppressWarnings("unchecked")
	public int findUtiid(String uti_kycid)
	{
		String hql = "Select uti_ind_id from UtiliserProfileModel where kyc_uti_id='"+uti_kycid+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		int id = lsdata.get(0);
		return id;
	}
	
	public String saveActivityDetails(UtilizerActivityModel actModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(actModel);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} 
		else 
		{
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<AccessDetailsModel> getuti2IndNotification(String kyc_id)
	{
		List<AccessDetailsModel> accessDetails = new ArrayList<AccessDetailsModel>();
		List<IndSignupModel> nameList =  new ArrayList<IndSignupModel>();
		
		String hql = "from AccessDetailsModel where access_giver='"+kyc_id+"' and uti_status='1' and repeate_access_status='No' ORDER BY access_given_date DESC";
		accessDetails  = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		for(int i=0;i<accessDetails.size();i++)
		{
			String nameHql = "from IndSignupModel where kycid='"+accessDetails.get(i).getAccess_taker()+"'";
			List<IndSignupModel> fullName = sessionFactory.getCurrentSession().createQuery(nameHql).list();
			nameList.addAll(fullName);
		}
		
		for(int j=0;j<nameList.size();j++)
		{
			accessDetails.get(j).setAccess_taker(nameList.get(j).getFirstname()+" "+nameList.get(j).getMiddlename()+" "+nameList.get(j).getLastname());
		}
		
		return accessDetails;
	}
		
	@SuppressWarnings("unchecked")
	public List<String> getValidateMobileNo(String mobileNo)
	{
		String hql = "select mobile_no from RegHistoryModel where mobile_no="+mobileNo+""; 
		List<String> mobileNoDetails  = sessionFactory.getCurrentSession().createQuery(hql).list();
		return mobileNoDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public String individualIdThroughEmailId(String emailId)
	{
		String reghql = "select ind_id from IndSignupModel where emailid='"+emailId+"'";
		List<Integer> statusData= sessionFactory.getCurrentSession().createQuery(reghql).list();
		
		if(statusData.size()>0)
		{
			String validateStatus = "select status from IndSignupModel where ind_id="+statusData.get(0)+"";
			List<String> validateStatusHql = sessionFactory.getCurrentSession().createQuery(validateStatus).list();
			
			if(validateStatusHql.get(0).equals("1"))
			{
				return "Yes";
			}
			else
			{
				return "Please activate the registration link which has sent to your mail id .";
			}
		}
		else
		{
			String reghistory = "select ind_id from RegHistoryModel where email_id='"+emailId+"'";
			List<Integer> indRegHistory = sessionFactory.getCurrentSession().createQuery(reghistory).list();
			
			if(indRegHistory.size()>0)
			{
				String validateStatus = "select status from IndSignupModel where ind_id="+indRegHistory.get(0)+"";
				List<String> validateStatusHql = sessionFactory.getCurrentSession().createQuery(validateStatus).list();
				
				if(validateStatusHql.get(0).equals("1"))
				{
					return "Yes";
				}
				else
				{
					return "Please activate the registration link which has sent to your mail id .";
				}
				
			}
			else
			{
				return "This Email id is not registered with us.";
			}
			
		}
		 
	}
	
	public String getUpdateEmailId(int ind_id,String EmailId)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("UPDATE IndSignupModel SET email_id = '"+EmailId+"' where ind_id="+ind_id+"").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String> getKycIdOfAccessGiven(String IndKycId)
	{
		/*String individualKycId = "select code_sender from CodesDetailsModel where code_receiver='"+IndKycId+"' ";
		List<String> individualKycIdHql = sessionFactory.getCurrentSession().createQuery(individualKycId).list();		
		*/
		String sql = "select code_sender from codes_details where cr_date >=now()-INTERVAL 1 DAY and code_receiver='"+IndKycId+"' and code_status='1' ORDER BY code_id DESC";
		List<String> individualKycIdHql =  sessionFactory.getCurrentSession().createSQLQuery(sql).list();
		
		
		return individualKycIdHql;
	}
	
	@SuppressWarnings("unchecked")
	public String getIndividualIdViaKycId(String KycId)
	{
		String indIdQuery = "select ind_id from IndSignupModel where kyc_id='"+KycId+"'";
		List<Integer> indIdQueryResult = sessionFactory.getCurrentSession().createQuery(indIdQuery).list();
		
		return indIdQueryResult.get(0)+"";
	}
	
	
	@SuppressWarnings("unchecked")
	public String getYearFromIndividualId(String indIndArray)
	{
		String hql = "select EXTRACT(YEAR from cr_date) from IndSignupModel where ind_id ="+indIndArray+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		String year = Integer.toString(lsdata.get(0));		
		return year;
	}
	
	public String saveResetPasswordModel(ResetPasswordModel resetPW)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(resetPW);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String getCurrentPlanName(int ownInd_id)
	{
		String indIdQuery = "select plan_available from IndSignupModel where ind_id="+ownInd_id+"";
		List<String> indIdQueryResult = sessionFactory.getCurrentSession().createQuery(indIdQuery).list();		
		return indIdQueryResult.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public Date getRegisterDate(int ownInd_id)
	{
		String indIdQuery = "select cr_date from IndividualPlanDetailModel where ind_id="+ownInd_id+"";
		List<Date> indIdQueryResult = sessionFactory.getCurrentSession().createQuery(indIdQuery).list();		
		return indIdQueryResult.get(0);
	}
	
	public String getEntryInPlanDetails(IndividualPlanDetailModel indPlanModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(indPlanModel);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public String getvalidateRegistrationEmailId(String emailId,int ind_id)
	{
		String emailIdHql = "from RegHistoryModel where email_id='"+emailId+"' and ind_id!="+ind_id+"";
		List<RegHistoryModel> emailIdResult = sessionFactory.getCurrentSession().createQuery(emailIdHql).list();
		
		if(emailIdResult.size()>0)
		{
			return "Exist";
		}
		else
		{
			String alterNativeEmailIdHql = "from BasicDetail where alternate_email='"+emailId+"' and ind_kyc_id!='"+ind_id+"'";
			List<BasicDetail> alterNativeEmailIdResult = sessionFactory.getCurrentSession().createQuery(alterNativeEmailIdHql).list();
			
			if(alterNativeEmailIdResult.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}			
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getvalidateRegistrationMobileNo(String mobileNo,int ind_id)
	{
		String emailIdHql = "from RegHistoryModel where mobile_no='"+mobileNo+"' and ind_id!="+ind_id+"";
		List<RegHistoryModel> emailIdResult = sessionFactory.getCurrentSession().createQuery(emailIdHql).list();
		
		if(emailIdResult.size()>0)
		{
			return "Exist";
		}
		else
		{
			String tell_officeHql = "from BasicDetail where tell_office='"+mobileNo+"' and ind_kyc_id!='"+ind_id+"'";
			List<BasicDetail> tell_officeResult = sessionFactory.getCurrentSession().createQuery(tell_officeHql).list();
			
			if(tell_officeResult.size()>0)
			{
				return "Exist";
			}
			else
			{
				String tell_residenceHql = "from BasicDetail where tell_residence='"+mobileNo+"' and ind_kyc_id!='"+ind_id+"'";
				List<BasicDetail> tell_residenceResult = sessionFactory.getCurrentSession().createQuery(tell_residenceHql).list();
				
				if(tell_residenceResult.size()>0)
				{
					return "Exist";
				}
				else
				{
					return "Not Exist";
				}	
			}	
		}
	}
	
	public String setEmailStatusReport(EmailSentStatusModel emailReport)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(emailReport);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	public String setSmsStatusReport(SmsSentStatusModel smsReport)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(smsReport);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	public String migratePlanAfterOneYear(int ownInd_id)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update IndividualPlanDetailModel set plan_name = :setplanname , cr_date = :setdate where ind_id = :sentIndId");
    	hql.setParameter("setplanname", "Basic");
    	hql.setParameter("setdate", new Date());
		hql.setParameter("sentIndId", ownInd_id);
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{	
			String hql1 = " UPDATE IndSignupModel SET plan_available='Basic' where ind_id="+ownInd_id+"";
			int status = sessionFactory.getCurrentSession().createQuery(hql1).executeUpdate();
			
			if(status>0)
			{
				return "success";
			}
			else
			{
				return "failure";
			}	    
		}
		else
		{			
			return "failure";
		}
	}
	
	public String saveUtiResetPasswordModel(UtilizerPasswordRequestModel resetPW)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(resetPW);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public  List<RegHistoryModel>  getIndividualDetailsviaIndId(int indId)
	{
		String hql = "from RegHistoryModel where ind_id="+indId+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+indId+")";
		List<RegHistoryModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;		
	}
	
	public String getUpdateRegTableWithLastRecord(RegHistoryModel indHistory,String fulleName)
	{		
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update IndSignupModel set firstname = :setFirstName , middlename = :setSecondName , lastname = :setLastName , individual_full_name = :setFullName where ind_id = :setIndId");
		hql.setParameter("setFirstName", indHistory.getFirst_name());
		hql.setParameter("setSecondName", indHistory.getMiddle_name());
		hql.setParameter("setLastName", indHistory.getLast_name());
		hql.setParameter("setIndId", indHistory.getInd_id());
		hql.setParameter("setFullName", fulleName);
		
		int result  = hql.executeUpdate();
		
		if(result==1)
		{
		    return "success";
		}
		else
		{
			return "failure";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getUtilizerAccessForIndividual(String individualKycId,String utilizerKycId)
	{
		String hql = "select access_pattern from AccessDetailsModel where access_giver='"+individualKycId+"' and access_taker='"+utilizerKycId+"' and status='1' and access_taken_date=(select MAX(access_taken_date) from AccessDetailsModel where access_giver='"+individualKycId+"' and access_taker='"+utilizerKycId+"' and status='1')";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<BasicDetail> getLatestBasicDetailsInfo(int basicId)
	{
		String basicHql = "from BasicDetail where basic_details_id="+basicId+"";
		List<BasicDetail> lsdata = sessionFactory.getCurrentSession().createQuery(basicHql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public int getIndividualIdVaiVisibility(String resetVariable)
	{
		String indIdQuery = "select ind_id from IndSignupModel where visibility='"+resetVariable+"'";
		List<Integer> indIdQueryResult = sessionFactory.getCurrentSession().createQuery(indIdQuery).list();		
		return indIdQueryResult.get(0);
	}
	
	public String updateVisibilityUrl(int ind_id,String resetVariable,String encryptedUrl)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update IndSignupModel set visibility = :encryptedUrl  where ind_id = :ind_id");
		hql.setParameter("encryptedUrl", encryptedUrl);
		hql.setParameter("ind_id", ind_id);
		//hql.setParameter("setvisibility", resetVariable);		
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "update";
		}
		else
		{			
			return "not update";
		}
	}
	
	
	public String saveClenseeContactDetails(ContactUsModel contactus)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(contactus);
		tx.commit();		
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getIndIdViaEmailId(String emailId)
	{
		String indIdQuery = "select ind_id from IndSignupModel where emailid='"+emailId+"'";
		List<Integer> indIdQueryResult = sessionFactory.getCurrentSession().createQuery(indIdQuery).list();		
		if(indIdQueryResult.size()>0)
		{
			return indIdQueryResult.get(0);
		}
		else
		{
			return 0;
		}
	}
	
}
