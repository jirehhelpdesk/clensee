package com.kyc.dao;


import java.util.Date;
import java.util.List;





























import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kyc.model.CodesDetailsModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.NonKycUserCodeDetailsModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.VisibilityModel;
import com.kyc.util.PasswordEncryptionDecryption;



@Repository("indSignupDao")
public class IndSignupDaoImpl implements IndSignupDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public int getLastIndId()
	{
		String hql = "select ind_id from IndSignupModel where cr_date=(select MAX(cr_date) from IndSignupModel)";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 1;
		}
	}
	
	public String addIndividual(IndSignupModel individual) {

		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(individual);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}

	
	@SuppressWarnings("unchecked")
	public String checkIndividual(IndSignupModel individual) throws Exception
	{
		String un = individual.getKycid();
		String pw = individual.getPassword();
	    
		String emailId = un;
		
		if(un!=null && pw!= null)
			 {			
			     String hql= "select password from IndSignupModel as I where I.kycid='"+un+"' and status='1' and admin_control='1'";		// and I.password='"+pw+"'		
				 List<String> lst=sessionFactory.getCurrentSession().createQuery(hql).list();
					 if(lst.size()>0)
					 {		
						 String validateStatus = PasswordEncryptionDecryption.getValidatePassword(pw, lst.get(0));
						 if(validateStatus.equals("True"))
						 {
							 return lst.get(0);	
						 }
						 else
						 {
							 return "failure";
						 }				         			        
					 } 				 
					 else if(lst.size()==0)
					 {
						 String Emailhql= "select password from IndSignupModel as I where I.emailid='"+emailId+"' and status='1' and admin_control='1'";   // and I.password='"+pw+"'
						 List<String> Emaillst=sessionFactory.getCurrentSession().createQuery(Emailhql).list();
							 
						     if(Emaillst.size()>0)
								 {								    	 
							    	 String validateStatus = PasswordEncryptionDecryption.getValidatePassword(pw, Emaillst.get(0));
									 if(validateStatus.equals("True"))
									 {
										 return Emaillst.get(0);	
									 }
									 else
									 {
										 return "failure";
									 }							       						         
								 }
							 else
								 {
									 return "failure";
								 }					 
					 }
					 else 
					 {
						 return "failure";
					 }				 
			 }
		else
		{
			return "failure";
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<IndSignupModel> getInd_idmax(IndSignupModel individual)
	{
		String un = individual.getKycid();		
		String hql = "from IndSignupModel D WHERE D.kycid='"+un+"' and D.ind_id=(select max(D.ind_id) from IndSignupModel D WHERE D.kycid='"+un+"')";		
		List<IndSignupModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;
	}
	
	public String getInd_id(IndSignupModel individual)
	{
		String hql = "" ;
		String un = individual.getKycid();		
		if(un.contains("@") || un.contains(".com"))
		{
			hql="SELECT D.ind_id FROM IndSignupModel D WHERE D.emailid='"+un+"' and D.ind_id=(select max(D.ind_id) from IndSignupModel D WHERE D.emailid='"+un+"')";			
		}
		else
		{
		    hql="SELECT D.ind_id FROM IndSignupModel D WHERE D.kycid='"+un+"' and D.ind_id=(select max(D.ind_id) from IndSignupModel D WHERE D.kycid='"+un+"')";
		}
		
		@SuppressWarnings("rawtypes")
		List lst=sessionFactory.getCurrentSession().createQuery(hql).list();
		return ""+lst.get(0); 
	}
	
	public String getcheckpassword(IndSignupModel indsignupmodel)
	{
		String un = indsignupmodel.getPassword();		
		String hql="SELECT D.password FROM IndSignupModel D WHERE D.ind_id='"+indsignupmodel.getIndid()+"'";
		@SuppressWarnings("rawtypes")
		List lst=sessionFactory.getCurrentSession().createQuery(hql).list();
	    if(lst.size()>0)
	    {
	    	return ""+lst.get(0); 
	    }
	    else return "";
	}
	
	
	public String savestatusreg(IndSignupModel indsignupmodel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("UPDATE IndSignupModel SET visibility = '"+indsignupmodel.getVisibility() +"' where ind_id='"+indsignupmodel.getIndid()+"'").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="successeditpassword";
		} else {
			txStatus="failurepassword";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		} 
		return txStatus;
	}
	
	
	public String getchangepassword(IndSignupModel indsignupmodel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		sessionFactory.getCurrentSession().createQuery("UPDATE IndSignupModel SET password = '"+indsignupmodel.getPassword() +"' where ind_id='"+indsignupmodel.getIndid()+"'").executeUpdate();				
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}	
	
	@SuppressWarnings("unchecked")
	public List<RegHistoryModel> reghistory(int ind_id) {

		String hql = "from RegHistoryModel where ind_id="+ind_id+"";
		List<RegHistoryModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata;

	}

	public IndSignupModel getname(int reg_id) {
		
		return (IndSignupModel)sessionFactory.getCurrentSession().get(IndSignupModel.class, reg_id);
	}

	@SuppressWarnings("unchecked")
	public String getUtilizerName(int utiIndId)
	{
		String hql = "select uti_first_name from UtiliserProfileModel where uti_ind_id="+utiIndId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();

		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public RegHistoryModel getRegistrationDetails(int ind_id)
	{
		String hql = "select reg_id from RegHistoryModel where ind_id = "+ind_id+" and cr_date =(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
		List<Integer> reg_id = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return (RegHistoryModel)sessionFactory.getCurrentSession().get(RegHistoryModel.class,reg_id.get(0));
	}
	
	
	@SuppressWarnings("unchecked")
	public String editIndividual(RegHistoryModel indHistory) {

		String hql = "from RegHistoryModel where ind_id="+indHistory.getInd_id()+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+indHistory.getInd_id()+")";
		List<RegHistoryModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
				
		if(indHistory.getFirst_name().equals(lsdata.get(0).getFirst_name()) && indHistory.getMiddle_name().equals(lsdata.get(0).getMiddle_name()) && indHistory.getLast_name().equals(lsdata.get(0).getLast_name()) && indHistory.getEmail_id().equals(lsdata.get(0).getEmail_id()) && indHistory.getMobile_no()==lsdata.get(0).getMobile_no() && indHistory.getGender().equals(lsdata.get(0).getGender()) )
		{
			return "success";
		}
		else
		{
			String txStatus="";
			Session docSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=docSession.beginTransaction();
			docSession.save(indHistory);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				docSession.close();
			}
			return txStatus;
		}
		
						
	}	
	
	public String saveRegHistory(RegHistoryModel regmodel)
	{
		String txStatus="";
		Session docSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=docSession.beginTransaction();
		docSession.save(regmodel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			docSession.close();
		}
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String findyear(String kycId)
	{
		String hql = "select EXTRACT(YEAR from cr_date) from IndSignupModel where kycid = '"+kycId+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		String year = Integer.toString(lsdata.get(0));		
		return year;				
	}

	
	@SuppressWarnings("unchecked")
	public String getVisibility(int ind_id)
	{
		String hql = "select visibility from IndSignupModel where ind_id="+ind_id+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	public String saveVisibilitySetting(VisibilityModel visibility)
	{
		String txStatus="";
		Session visSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=visSession.beginTransaction();
		visSession.save(visibility);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			visSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String getActiveStatus(String uniqueId)
	{
		String controlhql = "select admin_control from IndSignupModel where visibility='"+uniqueId+"'";
		List<String> admin_control = sessionFactory.getCurrentSession().createQuery(controlhql).list();
		
		if(admin_control.get(0).equals("1"))
		{
			String activation_validity = "select status from in_registrationdetails where visibility='"+uniqueId+"' and cr_date >=now()-INTERVAL 7 DAY";
			List<String> activation_validityData = sessionFactory.getCurrentSession().createSQLQuery(activation_validity).list();
			
			if(activation_validityData.size()>0)
			{
				org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update IndSignupModel set status = :setstatus  where visibility = :setvisibility");
				hql.setParameter("setstatus", "1");
				hql.setParameter("setvisibility", uniqueId);		
			    int result  = hql.executeUpdate();
				
				if(result==1)
				{			
				    return "successfully activated";
				}
				else
				{			
					return "failed to activate";
				}
			}
			else
			{
				return "Expired";
			}			
		}
		else
		{
			return "Prohibited";
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public String getActiveValidityStatus(String uniqueVariable)
	{
		String activation_validity = "select status from in_registrationdetails where visibility='"+uniqueVariable+"' and cr_date >=now()-INTERVAL 7 DAY";
		List<String> activation_validityData = sessionFactory.getCurrentSession().createSQLQuery(activation_validity).list();
		if(activation_validityData.size()>0)
		{
			
			String controlhql = "select admin_control from IndSignupModel where visibility='"+uniqueVariable+"'";
			List<String> admin_control = sessionFactory.getCurrentSession().createQuery(controlhql).list();
			
			if(admin_control.get(0).equals("1"))
			{
				return "failed to activate";
			}
			else
			{
				return "Prohibited";
			}
			
		}
		else
		{
			return "Expired";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String getnamefromRegHistory(int ind_id)
	{
		String hql = "select first_name from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
		List<String> firstName = sessionFactory.getCurrentSession().createQuery(hql).list();
		return firstName.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String checkStatus(String kycId)
	{
		String hql = "select status from IndSignupModel where visibility='"+kycId+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public int getindividualIndId(String uniqueVariable)
	{
		String hql = "select ind_id from IndSignupModel where visibility='"+uniqueVariable+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return lsdata.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public RegHistoryModel getLatestRegData(int ind_id)
	{
		String hql = "from RegHistoryModel where ind_id="+ind_id+" and cr_date=(select MAX(cr_date) from RegHistoryModel where ind_id="+ind_id+")";
		List<RegHistoryModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		return lsdata.get(0);
	}
	
	
	@SuppressWarnings("unchecked")
	public String getCheckStatus(String reserVariable)
	{
		String sql = "select ind_status from ind_reset_password where ind_visibility_code='"+reserVariable+"' and requested_date >=now()-INTERVAL 1 DAY ";
		List<String> lsdata = sessionFactory.getCurrentSession().createSQLQuery(sql).list();
		
		if(lsdata.size()>0)
		{
			if(lsdata.get(0).equals("0"))
			{
				String hql = "select status from IndSignupModel where visibility='"+reserVariable+"'";
				List<String> ind_status = sessionFactory.getCurrentSession().createQuery(hql).list();
				
				if(ind_status.size()>0)
				{
					if(ind_status.get(0).equals("1"))
					{
						return "Allow";
					}
					else
					{
						return "Prohibited";
					}
				}
				else
				{
					return "Not Allow";
				}
				
			}
			else
			{
				return "Not Allow";
			}			
		}
		else
		{
			return "Not Allow";					
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getDeleteResetPaswordInfo(String uniqueVariable)
	{
		String hql = "select password_id FROM ResetPasswordModel where ind_visibility_code='"+uniqueVariable+"'";	             
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
				
		Transaction tx1=null;
		String status = "";
		for(int i=0;i<lsdata.size();i++)
		{
			String passwordHql = "delete from ResetPasswordModel where password_id="+lsdata.get(i)+"";
			Query query = sessionFactory.getCurrentSession().createQuery(passwordHql);		
			
			int result = query.executeUpdate();		
			tx1=sessionFactory.getCurrentSession().beginTransaction();		
			tx1.commit();
			
			if(result>0)
			{
				status="success";
			}
			else
			{
				status="failure";
			}
		}
		
		return status;		
	}
	
	@SuppressWarnings("unchecked")
	public String getUpdateAppliedCodeStatus(String uniqueVariable)
	{
		String hqlForEmailId = "select emailid from IndSignupModel where visibility='"+uniqueVariable+"'";
		List<String> emailId = sessionFactory.getCurrentSession().createQuery(hqlForEmailId).list();
		
		String status = "";
		
		if(emailId.size()>0)
		{
			status = "Available";
			
			String hqlForCodeId= "from NonKycUserCodeDetailsModel where reciver_email_id='"+emailId.get(0)+"'";
			List<NonKycUserCodeDetailsModel> nonKycUserCodeDetails = sessionFactory.getCurrentSession().createQuery(hqlForCodeId).list();
			
			if(nonKycUserCodeDetails.size()>0)
			{
				String hqlForKycId = "select kycid from IndSignupModel where visibility='"+uniqueVariable+"'";
				List<String> kycId = sessionFactory.getCurrentSession().createQuery(hqlForKycId).list();
				
				for(int i=0;i<nonKycUserCodeDetails.size();i++)
				{
					CodesDetailsModel codeDetails = new CodesDetailsModel();
					
					String codeGenerate = "@kycc2o3de1000";
					
					String code_GeneratedHql  = "select code_generated from CodesDetailsModel where cr_date=(select Max(cr_date) from CodesDetailsModel)";
					List<String> code_generated = sessionFactory.getCurrentSession().createQuery(code_GeneratedHql).list();
					
					if(code_generated.size()>0)
					{
                        String lastCode = code_generated.get(0);			        	
			        	String coseAry[] = lastCode.split("e");			        	
			        	int nextIndex = Integer.parseInt(coseAry[1]);
			        	nextIndex = nextIndex + 1;
			        	String nextGeneratedCode = coseAry[0] + "e" + nextIndex;			        	
			        	
			        	codeDetails.setCode_generated(nextGeneratedCode);
					}
					else
					{
						codeDetails.setCode_generated(codeGenerate);
					}
					
					String senderDetailsHql = "from IndSignupModel where ind_id="+nonKycUserCodeDetails.get(i).getSender_ind_id()+"";
					List<IndSignupModel> senderDetails = sessionFactory.getCurrentSession().createQuery(senderDetailsHql).list();
					
					
					codeDetails.setCode_description("Sent Before Registration.");
					codeDetails.setCode_sender(senderDetails.get(0).getKycid());
					codeDetails.setCode_receiver(kycId.get(0));
					codeDetails.setCr_by(senderDetails.get(0).getFirstname()+" "+senderDetails.get(0).getMiddlename()+" "+senderDetails.get(0).getLastname());
					codeDetails.setCr_date(new Date());
					codeDetails.setCode_pattern(nonKycUserCodeDetails.get(i).getCode_details());
					codeDetails.setCode_status("1");
					codeDetails.setApply_code_count("0");
						
					// All before registration Code Inserted to the code Details Table
					
					String txStatus="";
					Session visSession=sessionFactory.openSession();
					Transaction tx=null;
					try {
					tx=visSession.beginTransaction();
					visSession.save(codeDetails);
					tx.commit();
					if(tx.wasCommitted()){
						status="success";
					} else {
						status="failure";
					}
					} catch (Exception he) {
						System.out.println(he.getMessage());
					} finally {
						visSession.close();
					}
																			
				}			
				
			}
			
			// After Insert of all code deleted from the existence one.
			
			Transaction tx1=null;
			String nonKycCodeHql = "delete from NonKycUserCodeDetailsModel where reciver_email_id='"+emailId.get(0)+"'";
			Query query = sessionFactory.getCurrentSession().createQuery(nonKycCodeHql);					
			int result = query.executeUpdate();		
			tx1=sessionFactory.getCurrentSession().beginTransaction();		
			tx1.commit();
			
		}
		else
		{
			status = "Not Available";
		}
		
		return status;		
	}
	
	@SuppressWarnings("unchecked")
	public String getKYCId(String uniqueId)
	{
		String hqlForkycid = "select kycid from IndSignupModel where visibility='"+uniqueId+"'";
		List<String> kycid = sessionFactory.getCurrentSession().createQuery(hqlForkycid).list();
		
		return kycid.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public String statusOfIndividualForLogin(IndSignupModel individual)
	{
		String un = individual.getKycid();
		String pw = individual.getPassword();
	    
		String emailId = un;
		
		if(un!=null && pw!= null)
			 {	

				String validatehql= "select password from IndSignupModel as I where I.kycid='"+un+"' and status='0' and admin_control='1'";		// and I.password='"+pw+"'		
				List<String> validatelst=sessionFactory.getCurrentSession().createQuery(validatehql).list();
				if(validatelst.size()>0)
			    {
			    	return "Please activate the link,Which has sent to your emailId to active your account  !";
			    }
				else
				{
					String validateemailIdhql= "select password from IndSignupModel as I where I.emailid='"+un+"' and status='0' and admin_control='1'";		// and I.password='"+pw+"'		
					List<String> validateemailIdlst=sessionFactory.getCurrentSession().createQuery(validateemailIdhql).list();
					if(validateemailIdlst.size()>0)
					{
						return "Please activate the link,Which has sent to your emailId to active your account  !";
					}
					else
					{
						String blockhql= "select password from IndSignupModel as I where I.kycid='"+un+"' and status='1' and admin_control='0'";		// and I.password='"+pw+"'		
						List<String> blocklst=sessionFactory.getCurrentSession().createQuery(blockhql).list();
					    if(blocklst.size()>0)
					    {
					    	return "Your account has blocked ,To activate the account please contact to Admin !";
					    }
					    else
					    {
					    	String blockemailhql= "select password from IndSignupModel as I where I.emailid='"+un+"' and status='1' and admin_control='0'";		// and I.password='"+pw+"'		
							List<String> blockemaillst=sessionFactory.getCurrentSession().createQuery(blockemailhql).list();
						    if(blockemaillst.size()>0)
						    {
						    	return "Your account has blocked ,To activate the account please contact to Admin !";
						    }
						    else
						    {
						    	String hql= "select password from IndSignupModel as I where I.kycid='"+un+"' and status='1' and admin_control='1'";		// and I.password='"+pw+"'		
								 List<String> lst=sessionFactory.getCurrentSession().createQuery(hql).list();
									 if(lst.size()>0)
									 {		
										 String validateStatus = PasswordEncryptionDecryption.getValidatePassword(pw, lst.get(0));
										 if(validateStatus.equals("True"))
										 {
											 return lst.get(0);	
										 }
										 else
										 {
											 return "Access is denied due to invalid credentials.";
										 }				         			        
									 } 				 
									 else if(lst.size()==0)
									 {
										 String Emailhql= "select password from IndSignupModel as I where I.emailid='"+emailId+"' and status='1' and admin_control='1'";   // and I.password='"+pw+"'
										 List<String> Emaillst=sessionFactory.getCurrentSession().createQuery(Emailhql).list();
											 
										     if(Emaillst.size()>0)
												 {								    	 
											    	 String validateStatus = PasswordEncryptionDecryption.getValidatePassword(pw, Emaillst.get(0));
													 if(validateStatus.equals("True"))
													 {
														 return Emaillst.get(0);	
													 }
													 else
													 {
														 return "Access is denied due to invalid credentials.";
													 }							       						         
												 }
											 else
												 {
													 return "Access is denied due to invalid credentials.";
												 }					 
									 }
									 else 
									 {
										 return "Access is denied due to invalid credentials.";
									 }			
						        }
					      }
					}
					
				}							    
			 }
		else
		{
			return "Access is denied due to invalid credentials.";
		}		
	}
	
	@SuppressWarnings("unchecked")
	public String getKYCIDFromCookiesValue(String indId)
	{
		String hql = "select kycid from IndSignupModel WHERE ind_id="+indId+"";
		List<String> Emaillst=sessionFactory.getCurrentSession().createQuery(hql).list();
		return Emaillst.get(0);
	}
}
