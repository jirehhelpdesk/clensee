package com.kyc.dao;

import java.util.Date;
import java.util.List;

import com.kyc.model.ContactUsModel;
import com.kyc.model.DocumentHierarchyModel;
import com.kyc.model.DomainHierarchyModel;
import com.kyc.model.DomainLevelModel;
import com.kyc.model.EmailSentStatusModel;
import com.kyc.model.IndSignupModel;
import com.kyc.model.KycNotificationModel;
import com.kyc.model.PatternDetailsModel;
import com.kyc.model.RegHistoryModel;
import com.kyc.model.SmsSentStatusModel;
import com.kyc.model.SubDomainLevel10Model;
import com.kyc.model.SubDomainLevel1Model;
import com.kyc.model.SubDomainLevel2Model;
import com.kyc.model.SubDomainLevel3Model;
import com.kyc.model.SubDomainLevel4Model;
import com.kyc.model.SubDomainLevel5Model;
import com.kyc.model.SubDomainLevel6Model;
import com.kyc.model.SubDomainLevel7Model;
import com.kyc.model.SubDomainLevel8Model;
import com.kyc.model.SubDomainLevel9Model;
import com.kyc.model.UtiliserProfileModel;
import com.kyc.model.UtilizerPlanDetailsModel;
import com.kyc.model.UtilizerProfilePhotoModel;

public interface AdminPanelDao {

	public List<IndSignupModel> getAllUsersDetails();
	
	public String getUtiRefId(int utiUnqId);
	
	public List<String> findtypeGroup(String type);
	
	public String find_acctypeGroup(String type);
	
	public String findSub_category(String type);
	
	public String savePattern(PatternDetailsModel pattern);
		
	public List<String> getPatternNames();
	
	public List<String> getPlanName();
	
	public String findPattern(String Pattern_Name);
	
	public List<PatternDetailsModel> patternforDelete();
	
	public String deletePattern(int p_id);
	
	public List<UtiliserProfileModel> searchUtiliser(String searchData,String searchType);
	
	public String findLogic(String pattern_name);
	
	public String updatePattern(String pattern_name,String p_logic);
	
	public String saveUtiliserDetails(UtiliserProfileModel utiProfile);
	
	public int findPatternId(String pattern_name);
	
	public List<UtiliserProfileModel> UtiliserProfileDetails(String utiid);
	
	public int findUtiId(String utiid);
	
    public String findKycid(int id);
	
	public String findPassword(int id);
	
	public String findDomain(int id);
	
	public List<IndSignupModel> searchindividualname(IndSignupModel searchprofile);

    public List<IndSignupModel> searchindividualplan(IndSignupModel searchprofile);

    public List<IndSignupModel> searchindividualkycid(IndSignupModel searchprofile);

    public String searchindividualkycidpic(IndSignupModel searchprofile); 
    
    public String changestatus(IndSignupModel changestatus);
    
    public List<String> getkycHierarchy();
    
    public String saveKycHierarchy(DocumentHierarchyModel kycHierarchy);
    
    public int getHierId(String kycHierarchy);
    
    public String getFinancialHierarchy();
    
    public String getAcademicHierarchy(String acaDocType);
    
    public List<String> getAcademicDocType();
    
    public String saveDomain(DomainLevelModel domainModel);
    
    public List<String> getDomainName();
    
    public String getDomainHierarchy(String columnId,String domainTable);
    
    public int getDomainId(String domainName);
    
    public int getParentId(String parentIdColumn,String parentTypeColum,String domainType,String parentTableName);
    
    public String saveSubDomainLevel1(SubDomainLevel1Model subDomainModel);    
    public String saveSubDomainLevel2(SubDomainLevel2Model subDomainModel);
    public String saveSubDomainLevel3(SubDomainLevel3Model subDomainModel);
    public String saveSubDomainLevel4(SubDomainLevel4Model subDomainModel);
    public String saveSubDomainLevel5(SubDomainLevel5Model subDomainModel);
    public String saveSubDomainLevel6(SubDomainLevel6Model subDomainModel);
    public String saveSubDomainLevel7(SubDomainLevel7Model subDomainModel);
    public String saveSubDomainLevel8(SubDomainLevel8Model subDomainModel);
    public String saveSubDomainLevel9(SubDomainLevel9Model subDomainModel);
    public String saveSubDomainLevel10(SubDomainLevel10Model subDomainModel);
    
    
    public String getEntireHierarchy(String domainLevel,String domainValue);
   
    public String getMainDomainStatus(String domainValue);
    
    public String getSubDomainStatus(String domainValue,String child_table_name); 
    
    public int getDomainIdbyDomainName(String domainName);
    
    public int getDeleteDomain(String tableName,String primaryColumn,String domainValue);
    
    public String subDomainByDomainId(int domainId);
    
    public String getParentNameById(String parentTableName,String parentIdColumn,int domainId);
    
    public String getHierarchyByParentId(String childTablesName,String childIdColumn,int domainId);
    
    public String getdomainName(String tablesName,String columnName,String selectId);
    
    public int getUtiliserId(UtiliserProfileModel utiProfile);
    
    public String getfinancialSubType();
    
    public String getHierarchyComponents(String hierarchyType);
    
    public  List<String> getPlanNameAsPerPlanDomain(String planDomain);
    
    public String saveNotifyDetails(KycNotificationModel notifyModel);
    
    public int getIndRegisteredYear(int ind_id);
    
    public List<IndSignupModel> getIndividualDetailsViaKycid(String searchedValue);
    
    public List<IndSignupModel> getIndividualDetailsViaPlan(String searchedValue);
    
    public List<IndSignupModel> getIndividualDetailsViaName(String searchedValue);
    
    public List<EmailSentStatusModel> getPendingEmail(); 
    
    public List<SmsSentStatusModel> getPendingSMS(); 
    
    public List<EmailSentStatusModel> getPendingEmailDetails(int email_uniqueId); 
	
    public List<SmsSentStatusModel> getPendingSMSDetails(int sms_uniqueId);
    
    public String getUpdateEmailDetails(int email_uniqueId);
    
    public String getUpdateSmSDetails(int sms_uniqueId);
    
    public List<RegHistoryModel> getdtailsviakycId(String kycId);
    
    public String getPatternName(int patternId);
    
    public String getPassword(int utiUnqId);
    
    public String getPlanDomain(int utiUnqId);
    
    public String getUtilizerPlanName(int utiUnqId);
    
   	public String getUtilizerDomain(int utiUnqId);
   	
    public Date getUtiRegDate(int utiUnqId);
   	
	public String getUtiProfilePic(int utiUnqId); 
	
	public String saveUtilizerPlanDetails(UtilizerPlanDetailsModel utiPlanModel);
	
	public String saveUtilizerProfilePhoto(UtilizerProfilePhotoModel utiProfilePhoto);
	
	public String getPatternDetailAsperPatterName(String patternName);
	
	public int getLastUtilizerIndId();
	
	public String deleteAcademicHierarchy(String hierarchyName);  
	
	public String updateUtilizerDetails(UtiliserProfileModel utiProfile);
	
	public String updateUtiliserProfilePhoto(UtiliserProfileModel utiProfile);
	
	public List<Integer> getClenseeYearList();
	
	public List<ContactUsModel> getAllContactUsDetails(String todaysDateFormat);
	
	public String getContactMssageByConId(int contactId);
	
	public List<ContactUsModel> getSearchContactUsDetails(String searchType,String searchValue);
	
}
